package com.example.besafepost.task;

import java.io.File;

import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.utils.Utils;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import android.os.AsyncTask;

public class NewTweetTask extends AsyncTask<Void, Void, Boolean> {

	private Twitter twitter;
	private File file;
	private String message;
	private IActivityDelegate mIDelegate;

	public NewTweetTask(Twitter twitter, File file, String message,
			IActivityDelegate mIDelegate) {
		this.twitter = twitter;
		this.file = file;
		this.message = message;
		this.mIDelegate = mIDelegate;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		StatusUpdate status = new StatusUpdate(message);
		if (file != null) {
			status.setMedia(file);
		}
		try {
			twitter.updateStatus(status);
			return true;
		} catch (TwitterException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if (result) {
			mIDelegate.onGetSuccess(Utils.REQUEST_POST_NEW_TWEET);
		} else {
			mIDelegate.onGetFailure(Utils.REQUEST_POST_NEW_TWEET);
		}

	}
}
