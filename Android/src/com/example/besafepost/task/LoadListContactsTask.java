package com.example.besafepost.task;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import com.example.besafepost.interfaces.IDelegateLoadListContactsTask;
import com.example.besafepost.model.Contact;

public class LoadListContactsTask extends AsyncTask<Void, Void, List<Contact>> {

	private static List<Contact> listContactsProfile = null;
	private IDelegateLoadListContactsTask iDelegate;
	private Activity activity;

	public LoadListContactsTask(IDelegateLoadListContactsTask idDelegate, Activity mActivity) {
		this.iDelegate = idDelegate;
		this.activity = mActivity;
	}

	@Override
	protected void onPreExecute() {

		iDelegate.onGetStart(this);
		super.onPreExecute();
	}

	@Override
	protected List<Contact> doInBackground(Void... params) {
		if (listContactsProfile == null) {

			listContactsProfile = new ArrayList<Contact>();

			String phoneNumber = null;
			Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
			String _ID = BaseColumns._ID;
			String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
			String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

			Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
			String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
			String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

			ContentResolver contentResolver = activity.getContentResolver();

			Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, DISPLAY_NAME
					+ " ASC");

			// Loop for every contact in the phone
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				do {

					String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
					String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));

					int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor
							.getColumnIndex(HAS_PHONE_NUMBER)));
					Contact mContact = new Contact();
					List<String> listPhoneNumber = new ArrayList<String>();
					if (hasPhoneNumber > 0) {
						mContact.setNameContact(name);

						// Query and loop for every phone number of the contact
						Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null,
								Phone_CONTACT_ID + " = ?", new String[] { contact_id }, null);
						listPhoneNumber.clear();
						while (phoneCursor.moveToNext()) {
							phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
							listPhoneNumber.add(phoneNumber);
							mContact.setPhoneNumber(listPhoneNumber);

						}

						phoneCursor.close();
					}

					listContactsProfile.add(mContact);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}
		return listContactsProfile;
	}

	@Override
	protected void onPostExecute(List<Contact> result) {

		iDelegate.onGetSuccess(this);

		super.onPostExecute(result);
	}

	public List<Contact> getNameContact() {

		return listContactsProfile;
	}
}
