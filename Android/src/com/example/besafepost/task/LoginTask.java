package com.example.besafepost.task;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;

import com.example.besafepost.interfaces.IAsyncTaskDelegate;
import com.example.besafepost.singleton.LoginObject;
import com.example.besafepost.utils.Log;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class LoginTask extends AbstractTask {
	private String mEmail;
	private String mPassword;
	private LoginTaskObject loginTaskObject;

	public LoginTask(IAsyncTaskDelegate<AbstractTask> delegate, String loginEmail, String password) {
		mDelegate = delegate;
		mEmail = loginEmail;
		mPassword = password;
	}

	@Override
	protected Void doInBackground(Void... params) {
		HttpPost httpPost = new HttpPost("http://bio.vmodev.com:7200/besafe/api.php?m=login");
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		if (mEmail != null) {
			postParameters.add(new BasicNameValuePair("email", mEmail));
		}
		if (mPassword != null) {
			postParameters.add(new BasicNameValuePair("password", mPassword));
		}
		StringBuilder builder = new StringBuilder();
		try {
			BufferedReader br = null;
			String output = null;
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 10000);
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
			httpPost.setEntity(formEntity);
			HttpResponse response = httpClient.execute(httpPost);
			br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			while ((output = br.readLine()) != null) {
				builder.append(output);
			}

			httpClient.getConnectionManager().shutdown();
			strResult = builder.toString();

			Log.i(Log.TAG, strResult);
			
			loginTaskObject = new Gson().fromJson(strResult, LoginTaskObject.class);
		} catch (Exception e) {
			this.e = e;
		}
		return null;
	}

	public LoginTaskObject getLoginTaskObject() {
		return loginTaskObject;
	}

	public class LoginTaskObject {
		@SerializedName("msg")
		public String msg;
		@SerializedName("returnCode")
		public String returnCode;
		@SerializedName("data")
		public LoginObject data;
	}
}