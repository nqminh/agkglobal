package com.example.besafepost.task;

import com.example.besafepost.interfaces.IAsyncTaskDelegate;
import com.example.besafepost.utils.Const;

public class CheckAccount extends AbstractPostMethodTask {

	public CheckAccount(IAsyncTaskDelegate<AbstractTask> delegate, String socialType, String friendIds) {
		// TODO Auto-generated constructor stub
		mDelegate = delegate;
		url = Const.URL_CHECK_ACCOUNT;
		addParam("social_type", socialType);
		addParam("friend_list", friendIds);
	}
}
