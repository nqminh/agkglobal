package com.example.besafepost.task;

import com.example.besafepost.interfaces.IAsyncTaskDelegate;
import com.example.besafepost.utils.Const;

public class ForgotPassTask extends AbstractPostMethodTask{
	public ForgotPassTask(IAsyncTaskDelegate<AbstractTask> delegate, String email) {
		this.mDelegate = delegate;
		this.url = Const.URL_FORGOT;
		addParam("email", email);
	}
}
