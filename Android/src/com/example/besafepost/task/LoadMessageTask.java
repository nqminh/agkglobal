package com.example.besafepost.task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.model.Sms;
import com.example.besafepost.utils.Log;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

public class LoadMessageTask extends AsyncTask<Void, Void, List<Sms>> {
	Activity mActivity;
	IActivityDelegate mDelegate;
	List<Sms> lstSms = new ArrayList<Sms>();
	
	public LoadMessageTask(Activity activity, IActivityDelegate activityDelegate) {
		mActivity = activity;
		mDelegate = activityDelegate;
	}
	
	@Override
	protected List<Sms> doInBackground(Void... params) {
	    Sms objSms = new Sms();
	    Uri message = Uri.parse("content://sms/");
	    ContentResolver cr = mActivity.getContentResolver();

	    Cursor c = cr.query(message, null, null, null, null);
	    int totalSMS = c.getCount();

	    if (c.moveToFirst()) {
	        for (int i = 0; i < totalSMS; i++) {
	            objSms = new Sms();
	            objSms.setId(c.getInt(c.getColumnIndexOrThrow("_id")));
	            objSms.setAddress(c.getString(c
	                    .getColumnIndexOrThrow("address")));
	            objSms.set_threadId(c.getInt(c.getColumnIndexOrThrow("thread_id")));
	            objSms.setMsg(c.getString(c.getColumnIndexOrThrow("body")));
	            objSms.setReadState(c.getInt(c.getColumnIndex("read")));
	            objSms.setTime(c.getLong(c.getColumnIndexOrThrow("date")));
	            objSms.setType(c.getInt(c.getColumnIndexOrThrow("type")));

	            lstSms.add(objSms);
	        
	        	if (i==0) Log.d("Besafe", objSms.toString());
	            c.moveToNext();
	        }
	    }
	     else {
	    	Log.e(Log.TAG, "this app have no sms");
	     }
	    c.close();
	    if (lstSms.size() > 0) {
	    	Collections.sort(lstSms, new Comparator<Sms>() {

				@Override
				public int compare(Sms lhs, Sms rhs) {
					return (lhs.getId() > rhs.getId()) ? 1 : 0 ;
				}
			});
	    }
		return lstSms;
	}
	
	@Override
	protected void onPostExecute(List<Sms> result) {
		mDelegate.onGetSuccess("result_ok");
	}
	
	public List<Sms> getListSMS(){
		return lstSms;
	}
}
