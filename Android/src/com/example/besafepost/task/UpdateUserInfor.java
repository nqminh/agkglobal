package com.example.besafepost.task;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;

import com.example.besafepost.interfaces.IAsyncTaskDelegate;
import com.example.besafepost.singleton.LoginObject;
import com.example.besafepost.utils.Log;

public class UpdateUserInfor extends AbstractTask {
	public UpdateUserInfor(IAsyncTaskDelegate<AbstractTask> delegate) {
		mDelegate = delegate;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		HttpPost httpPost = new HttpPost(
				"http://besafeapps.com/api/api.php?m=updateContact");
		ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
		LoginObject mLoginObject = LoginObject.get_instance();
		postParameters.add(new BasicNameValuePair("full_name", mLoginObject.getFullName()));
		postParameters.add(new BasicNameValuePair("age", mLoginObject.getAge()));
		postParameters.add(new BasicNameValuePair("sex", mLoginObject.getSex()));
		postParameters.add(new BasicNameValuePair("email", mLoginObject.getEmail()));
		postParameters.add(new BasicNameValuePair("phone", mLoginObject.getPhone()));
		postParameters.add(new BasicNameValuePair("facebook_id", mLoginObject.getFacebookId()));
		postParameters.add(new BasicNameValuePair("twitter_id", mLoginObject.getTwitterId()));
		
		StringBuilder builder = new StringBuilder();
		try {

			BufferedReader br = null;
			String output = null;
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpConnectionParams.setConnectionTimeout(
					httpClient.getParams(), 10000);
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(
					postParameters);
			httpPost.setEntity(formEntity);
			HttpResponse response = httpClient.execute(httpPost);
			br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			while ((output = br.readLine()) != null) {
				builder.append(output);
			}

			httpClient.getConnectionManager().shutdown();
			strResult = builder.toString();
			Log.d(Log.TAG, strResult);
		} catch (Exception e) {
			this.e = e;
		}
		return null;
	}
}