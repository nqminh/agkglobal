package com.example.besafepost.task;

import com.example.besafepost.interfaces.IAsyncTaskDelegate;
import com.example.besafepost.singleton.LoginObject;
import com.example.besafepost.task.LoginTask.LoginTaskObject;
import com.example.besafepost.utils.Const;
import com.google.gson.Gson;

public class RegisterTask extends AbstractPostMethodTask {
	public LoginTaskObject resultObject;
	public RegisterTask(IAsyncTaskDelegate<AbstractTask> delegate, LoginObject object) {
		mDelegate = delegate;
		url = Const.URL_REGISTER;
		addParam("email", object.getEmail());
		addParam("password",object.getPassword());
		addParam("zip_code", object.getZipCode());
		addParam("birthday", object.getBirthYear());
		addParam("sex", object.getSex());
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		super.doInBackground(params);
		try {
			resultObject = new Gson().fromJson(strResult, LoginTaskObject.class);
		} catch (Exception e) {
			this.e = e;
		}
		return null;
	}
}
