package com.example.besafepost.task;

import com.example.besafepost.interfaces.IAsyncTaskDelegate;
import com.example.besafepost.singleton.LoginObject;
import com.example.besafepost.utils.Const;

public class updateBuddy extends AbstractPostMethodTask {
	
	public updateBuddy(IAsyncTaskDelegate<AbstractTask> delegate, String buddyId, String socialType) {
		mDelegate = delegate;
		url = Const.URL_UPDATE_BUDDY;
		addParam("user_id", LoginObject.get_instance().getId());
		addParam("buddy_id", buddyId);
		addParam("social_type", socialType);
	}
}
