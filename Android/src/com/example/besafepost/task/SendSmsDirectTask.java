package com.example.besafepost.task;

import twitter4j.Twitter;

import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.utils.Utils;

import android.os.AsyncTask;

public class SendSmsDirectTask extends AsyncTask<Void, Void, Boolean> {

	private Twitter twitter;
	private String scrName;
	private String message;
	private IActivityDelegate mIDelegate;

	public SendSmsDirectTask(Twitter twitter, String scrName, String message,
			IActivityDelegate mIDelegate) {
		this.twitter = twitter;
		this.scrName = scrName;
		this.message = message;
		this.mIDelegate = mIDelegate;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		try {
			twitter.sendDirectMessage(scrName, message);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if (result) {
			mIDelegate.onGetSuccess(Utils.REQUEST_SEND_DIRECT_SMS);
		} else {
			mIDelegate.onGetFailure(Utils.REQUEST_SEND_DIRECT_SMS);
		}

	}

}
