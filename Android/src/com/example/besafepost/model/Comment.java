package com.example.besafepost.model;

public class Comment {

	public static String NAME_ACTOR = "<!-- ACTOR'S NAME GOES HERE -->";
	public static String COMMENT = "<!-- COMMENT GOES HERE -->";
	public static String LIKE_OR_UNLIKE = "<!-- LIKE_OR_UNLIKE -->";
	public static String LIKE_COUNT = "<!-- LIKE COUNT GOES HERE -->";
	public static String TIME = "<!-- TIME GOES HERE -->";
	public static String ID_LIKE_OR_UNLIKE = "idLikeButton";
	public static String HREF_LIKE_COMMEN = "http://null/hrefLikeComment";
	public static String ID_LIKE_COUNT = "idLikeCountComment";
	public static String SRC_PROFILE = "<!--SRC_PROFILE -->";
	private String nameActor;
	private String textComment;
	private String time;
	private int likeCount;
	private boolean user_likes;
	private String postId;
	private String profileIcon = "";
	private boolean canLike = false;

	public boolean isCanLike() {
		return canLike;
	}

	public void setCanLike(boolean canLike) {
		this.canLike = canLike;
	}

	public String getProfileIcon() {
		return profileIcon;
	}

	public void setProfileIcon(String profileIcon) {
		this.profileIcon = profileIcon;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public String getNameActor() {
		return nameActor;
	}

	public void setNameActor(String nameActor) {
		this.nameActor = nameActor;
	}

	public String getTextComment() {
		return textComment;
	}

	public void setTextComment(String textComment) {
		this.textComment = textComment;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public boolean isUser_likes() {
		return user_likes;
	}

	public void setUser_likes(boolean user_likes) {
		this.user_likes = user_likes;
	}

}
