package com.example.besafepost.model;

public class PostObject {
	public static final String SOCIAL_FACEBOOK = "facebook";
	public static final String SOCIAL_TWITTER = "twitter";
	
	public static final String TYPE_STATUS = "status";
	public static final String TYPE_PHOTO = "photo";
	public static final String TYPE_CHECK_IN = "checkin";
	public static final String TYPE_NEW_TWEET = "newtweet";
	public static final String TYPE_RETWEET = "retweet";
	public static final String TYPE_REPLY = "reply";
	public static final String TYPE_QUOTE = "quote";
	
	
	private int postId;
	private String status;
	private double lat;
	private double lon;
	private String address;
	private String imgLink;
	private String audience;
	private String type;
	private String link;
	private String socialType;
	
	public int getPostId() {
		return postId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getImgLink() {
		return imgLink;
	}
	public void setImgLink(String imgLink) {
		this.imgLink = imgLink;
	}
	public String getAudience() {
		return audience;
	}
	public void setAudience(String audience) {
		this.audience = audience;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getSocialType() {
		return socialType;
	}
	public void setSocialType(String socialType) {
		this.socialType = socialType;
	}
}
