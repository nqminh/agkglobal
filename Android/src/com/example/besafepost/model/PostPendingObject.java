package com.example.besafepost.model;

import com.google.gson.annotations.SerializedName;

public class PostPendingObject{
	@SerializedName("id")
	private int id;
	@SerializedName("user_id")
	private int userId;
	@SerializedName("post_id")
	private int postId;
	@SerializedName("social_type")
	private String socialType;
	@SerializedName("media_file")
	private String mediaLink;
	@SerializedName("status")
	private String status;
	@SerializedName("data")
	private DataObject data;
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public int getPostId() {
		return postId;
	}


	public void setPostId(int postId) {
		this.postId = postId;
	}


	public String getSocialType() {
		return socialType;
	}


	public void setSocialType(String socialType) {
		this.socialType = socialType;
	}


	public String getMediaLink() {
		return mediaLink;
	}


	public void setMediaLink(String mediaLink) {
		this.mediaLink = mediaLink;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public DataObject getData() {
		return data;
	}


	public void setData(DataObject data) {
		this.data = data;
	}


	public class DataObject{
		@SerializedName("lat")
		private double lat;
		@SerializedName("long")
		private double lon;
		@SerializedName("address")
		private String address;
		@SerializedName("status")
		private String status;
		@SerializedName("audience")
		private String audience;
		@SerializedName("share_post_id")
		private String sharePostId;
		@SerializedName("post_type")
		private String postType;
		
		public double getLat() {
			return lat;
		}
		public void setLat(double lat) {
			this.lat = lat;
		}
		public double getLon() {
			return lon;
		}
		public void setLon(double lon) {
			this.lon = lon;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getAudience() {
			return audience;
		}
		public void setAudience(String audience) {
			this.audience = audience;
		}
		public String getSharePostId() {
			return sharePostId;
		}
		public void setSharePostId(String sharePostId) {
			this.sharePostId = sharePostId;
		}
		public String getPostType() {
			return postType;
		}
		public void setPostType(String postType) {
			this.postType = postType;
		}
	}
}
