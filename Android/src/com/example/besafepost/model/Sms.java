package com.example.besafepost.model;

public class Sms {
	private int _id;
	private String _address;
	private String _msg;
	private int _threadId;
	private int _readState;
	private long _time;
	private int _type;

	public int getId() {
		return _id;
	}

	public String getAddress() {
		return _address;
	}

	public String getMsg() {
		return _msg;
	}

	public int getReadState() {
		return _readState;
	}

	public long getTime() {
		return _time;
	}

	public int getType() {
		return _type;
	}

	public void setId(int id) {
		_id = id;
	}

	public void setAddress(String address) {
		_address = address;
	}

	public void setMsg(String msg) {
		_msg = msg;
	}

	public void setReadState(int readState) {
		_readState = readState;
	}

	public void setTime(long time) {
		_time = time;
	}

	public void setType(int type) {
		_type = type;
	}

	@Override
	public String toString() {
		return "id : " + _id + ", thread_id : " + _threadId + ", address : " + _address
				+ ", type : " + _type + ", read state : " + _readState + ", time : " + _time
				+ ", message : " + _msg;
	}

	public int get_threadId() {
		return _threadId;
	}

	public void set_threadId(int _threadId) {
		this._threadId = _threadId;
	}
}
