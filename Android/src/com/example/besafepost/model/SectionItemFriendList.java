package com.example.besafepost.model;

import com.example.besafepost.interfaces.IDeItemFriendList;

public class SectionItemFriendList implements IDeItemFriendList {
	private final String title;

	public SectionItemFriendList(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public boolean isSection() {
		return true;
	}
}
