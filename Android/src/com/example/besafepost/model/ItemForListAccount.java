package com.example.besafepost.model;

public class ItemForListAccount {
	private String accountName;
	private String accountType;
	private int iconFromSrc;
	private int colorAccountNameFromSrc;

	public int getColorAccountNameFromSrc() {
		return colorAccountNameFromSrc;
	}

	public void setColorAccountNameFromSrc(int colorAccountNameFromSrc) {
		this.colorAccountNameFromSrc = colorAccountNameFromSrc;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public int getIconFromSrc() {
		return iconFromSrc;
	}

	public void setIconFromSrc(int iconFromSrc) {
		this.iconFromSrc = iconFromSrc;
	}

}
