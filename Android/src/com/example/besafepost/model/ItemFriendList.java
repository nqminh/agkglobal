package com.example.besafepost.model;

public class ItemFriendList {
	private Long idFriendList;
	private String nameFriendList;

	public void setIdFriendList(Long idFriendList) {
		this.idFriendList = idFriendList;
	}

	public Long getIdFriendList() {
		return idFriendList;
	}

	public void setNameFriendList(String nameFriendList) {
		this.nameFriendList = nameFriendList;
	}

	public String getNameFriendList() {
		return nameFriendList;
	}
}
