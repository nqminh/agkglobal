package com.example.besafepost.model;

import com.google.gson.annotations.SerializedName;

public class TaskResultObject {
	@SerializedName("returnCode")
	String returnCode;
	@SerializedName("msg")
	String message;
}
