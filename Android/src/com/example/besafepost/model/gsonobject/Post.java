package com.example.besafepost.model.gsonobject;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Post {
	@SerializedName("comment_info")
	public CommentInfo commentInfo;
	@SerializedName("actor_id")
	public String actorId;
	@SerializedName("like_info")
	public LikeInfo likeInfo;
	@SerializedName("source_id")
	public String sourceId;
	@SerializedName("share_info")
	public ShareInfo shareInfo;
	@SerializedName("attachment")
	public Attachment attachment;
	@SerializedName("type")
	public int type;
	@SerializedName("post_id")
	public String postId;
	@SerializedName("updated_time")
	public long updateTime;
	// @SerializedName("action_links")
	// public String actionLinks;
	@SerializedName("target_id")
	public String targetId;
	@SerializedName("message")
	public String message;
	@SerializedName("permalink")
	public String permalink;
	@SerializedName("likes")
	public Likes likes;
	@SerializedName("app_id")
	public String appId;
	@SerializedName("place")
	public String place;
	@SerializedName("created_time")
	public long createdTime;
	// @SerializedName("attribution")
	// public String attribution;

	public Profile profile;
	public Place placeObject;
	public Profile target;

	public Post() {
		commentInfo = new CommentInfo();
		actorId = "";
		likeInfo = new LikeInfo();
		sourceId = "";
		shareInfo = new ShareInfo();
		attachment = new Attachment();
		type = -1;
		postId = "";
		updateTime = 0;
		targetId = "";
		message = "";
		permalink = "";
		likes = new Likes();
		appId = "";
		place = "";
		createdTime = 0;
		profile = new Profile();
		placeObject = new Place();
		target = new Profile();
	}

	public class CommentInfo {
		@SerializedName("comment_count")
		public int count;
		@SerializedName("comment_order")
		public String order;
		@SerializedName("can_comment")
		public boolean canComment;

		public CommentInfo() {
			count = 0;
			order = "";
			canComment = false;
		}
	}

	public class LikeInfo {
		@SerializedName("user_likes")
		public boolean userLike;
		@SerializedName("can_like")
		public boolean canLike;
		@SerializedName("like_count")
		public int likeCount;

		public LikeInfo() {
			userLike = false;
			canLike = false;
			likeCount = 0;
		}
	}

	public class ShareInfo {
		@SerializedName("can_share")
		public boolean canShare;
		@SerializedName("share_count")
		public int shareCount;
		
		public ShareInfo(){
			canShare = false;
			shareCount = 0;
		}
	}

	public class Attachment {
		@SerializedName("icon")
		public String icon;
		@SerializedName("fb_object_type")
		public String fbObjectType;
		@SerializedName("description")
		public String description;
		@SerializedName("fb_object_id")
		public String fbObjectId;
		@SerializedName("name")
		public String name;
		@SerializedName("caption")
		public String caption;
		@SerializedName("href")
		public String href;
		@SerializedName("properties")
		public List<Property> properties;
		@SerializedName("media")
		public List<MediaItem> media;
		
		public Attachment(){
			icon = "";
			fbObjectType = "";
			description = "";
			fbObjectId = "";
			name = "";
			caption = "";
			href = "";
			properties = new ArrayList<Post.Attachment.Property>();
			media = new ArrayList<Post.Attachment.MediaItem>();
		}

		public class Property {
			@SerializedName("text")
			public String text;
			
			public Property() {
				text = "";
			}
		}

		public class MediaItem {
			@SerializedName("src")
			public String src;
			@SerializedName("type")
			public String type;
			@SerializedName("photo")
			public PhotoItem photo;
			@SerializedName("video")
			public VideoItem video;
			@SerializedName("href")
			public String href;
			@SerializedName("alt")
			public String alt;
			
			public MediaItem() {
				src = "";
				type = "";
				photo = new PhotoItem();
				video = new VideoItem();
				href = "";
				alt = "";
			}

			public class PhotoItem {
				@SerializedName("index")
				public int index;
				@SerializedName("height")
				public int height;
				@SerializedName("width")
				public int width;
				@SerializedName("owner")
				public String owner;
				@SerializedName("images")
				public List<Image> images;
				@SerializedName("fbid")
				public String fbid;
				@SerializedName("pid")
				public String pid;
				@SerializedName("aid")
				public String aid;
				
				public PhotoItem() {
					index = 0;
					height = 0;
					width = 0;
					owner = "";
					images = new ArrayList<Post.Attachment.MediaItem.PhotoItem.Image>();
					fbid = "";
					pid = "";
					aid = "";
				}

				public class Image {
					@SerializedName("height")
					public int height;
					@SerializedName("width")
					public int width;
					@SerializedName("src")
					public String src;
					
					public Image() {
						height = 0;
						width = 0;
						src = "";
					}
				}
			}

			public class VideoItem {
				@SerializedName("source_url")
				public String sourceUrl;
				
				public VideoItem() {
					sourceUrl = "";
				}
			}
		}
	}

	public class Likes {
		@SerializedName("can_like")
		public boolean canLike;
		@SerializedName("user_likes")
		public boolean userLike;
		// @SerializedName("friends")
		// public List<String> friends;
		@SerializedName("count")
		public int count;
		@SerializedName("href")
		public String href;
		@SerializedName("sample")
		public List<String> sample;
		
		public Likes() {
			canLike = false;
			userLike = false;
			count = 0;
			href = "";
			sample = new ArrayList<String>();
		}
	}

}
