package com.example.besafepost.model.gsonobject;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Place {
	// TODO
	@SerializedName("content_age")
	public int contentAge;
	@SerializedName("pic_small")
	public String picSmall;
	@SerializedName("description")
	public String description;
	@SerializedName("name")
	public String name;
	@SerializedName("page_id")
	public String pageId;
	@SerializedName("pic_crop")
	public PicCrop picCrop;
	@SerializedName("is_unclaimed")
	public boolean isUnclaimed;
	@SerializedName("display_subtext")
	public String displaySubtext;
	@SerializedName("pic")
	public String pic;
	@SerializedName("pic_large")
	public String picLarge;
	@SerializedName("geometry")
	public Geometry geometry;
	
	public Place() {
		// TODO Auto-generated constructor stub
		contentAge = -1;
		picSmall = "";
		picCrop = new PicCrop();
		pic = "";
		picLarge = "";
		description = "";
		name = "";
		pageId = "";
		isUnclaimed = false;
		displaySubtext = "";
		geometry = new Geometry();
	}
	
	public class PicCrop {
		@SerializedName("bottom")
		public int bottom;
		@SerializedName("top")
		public int top;
		@SerializedName("left")
		public int left;
		@SerializedName("right")
		public int right;
		@SerializedName("width")
		public int width;
		@SerializedName("height")
		public int height;
		@SerializedName("uri")
		public String uri;
		
		public PicCrop() {
			// TODO Auto-generated constructor stub
			bottom = 0;
			top = 0;
			left = 0;
			right = 0;
			width = 0;
			height = 0;
			uri = "";
		}
	}
	
	public class Geometry {
		@SerializedName("type")
		public String type;
		@SerializedName("coordinates")
		public List<String> coordinates;
		public Geometry() {
			// TODO Auto-generated constructor stub
			type = "";
			coordinates = new ArrayList<String>();
			coordinates.add("");
			coordinates.add("");
		}
	}
}