package com.example.besafepost.model.gsonobject;

import com.google.gson.annotations.SerializedName;

public class Profile {
	@SerializedName("pic_square")
	public String picSquare;
	@SerializedName("pic_big")
	public String picBig;
	@SerializedName("id")
	public String id;
	@SerializedName("name")
	public String name;
	
	public Profile() {
		picSquare = "";
		picBig = "";
		id = "";
		name = "";
	}
}