package com.example.besafepost.model;

import com.example.besafepost.interfaces.IDeItemFriendList;

public class EntryItemFriendList implements IDeItemFriendList {

	public final String title;
	public final int imgSrc;

	public EntryItemFriendList(String title, int imgSrc) {
		this.title = title;
		this.imgSrc = imgSrc;
	}

	@Override
	public boolean isSection() {
		return false;
	}
}
