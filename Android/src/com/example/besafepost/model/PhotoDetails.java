package com.example.besafepost.model;

public class PhotoDetails {
	String src, objectId;
	int likeCount, commentCount;
	Boolean isLike;
	private String description;

	public void setIsLike(Boolean isLike) {
		this.isLike = isLike;
	}

	public Boolean getIsLike() {
		return isLike;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getSrc() {
		return src;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public int getCommentCount() {
		return commentCount;
	}

	public String getDescription() {
		if (description != null)
			return description;
		else
			return "";
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
