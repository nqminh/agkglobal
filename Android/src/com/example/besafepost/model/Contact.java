package com.example.besafepost.model;

import java.util.List;

public class Contact {

	private String nameContact;
	private List<String> phoneNumber;

	public String getNameContact() {
		return (nameContact != null)?nameContact:"";
	}

	public void setNameContact(String nameContact) {
		this.nameContact = nameContact;
	}

	public List<String> getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(List<String> phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Override
	public String toString() {
		return ((phoneNumber != null && phoneNumber.size() > 0) ? phoneNumber.get(0) : "");
	}
	
	public String getNameAndAddr(){
		return getNameContact() + "\n" + ((phoneNumber != null && phoneNumber.size() > 0) ? phoneNumber.get(0) : "");
	}
}
