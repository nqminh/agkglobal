package com.example.besafepost.model;

import java.util.ArrayList;

import android.text.TextUtils;

import com.example.besafepost.model.gsonobject.Post;
import com.example.besafepost.model.gsonobject.Post.Attachment.MediaItem;
import com.example.besafepost.model.gsonobject.Post.Attachment.Property;

public class ArticleItem {

	public static String PROFILE_ICON = "&quot;%@&quot;";
	public static String PROFILE_NAME = "<!-- PROFILE NAME GOES HERE -->%@";
	public static String TIME_STAMP = "<!-- TIMESTAMP GOES HERE -->%@";
	public static String MESSAGE = "<!-- MESSAGE GOES HERE -->%@";
	public static String LIKE_COUNT = "<!-- LIKE COUNT GOES HERE -->%@";
	public static String COMMENT_COUNT = "<!-- COMMENT COUNT GOES HERE -->%@";
	public static String PHOTO_ATTACH = "%@";
	public static String SHARE_LINK = "<!-- THIS IS SHARE LINK -->%@";
	public static String SHARE_CONTENT = "<!-- THIS IS CONTENT SHARE -->%@";
	public static String SHARE_VIA = "<!-- THIS IS VIA -->%@";
	public static String SHARE_TITLE = "<!-- THIS IS SHARE TITLE -->%@";
	public static String SRC_ICON_IMAGE = "<!-- THIS IS SRC ICON IMAGE -->%@";
	public static String LIKE_LINK = "http://null/like/%@";
	public static String LINK_HIDE_DELELE_POST = "http://onclick/hide_delele";
	public static String LIKE_ON_PHOTO_LINK = "http://null/like/photo/%@";
	public static String SRC_FULL_IMG = "<!-- THIS IS SRC FULL IMG -->%@";
	public static String COMMENT_LINK = "http://null/comment/%@";
	public static String LOAD_MORE_LINK = "http://null/loadmore=";
	public static String USER_LIKE = "<!-- THIS IS USER LIKE-->%@";
	public static String TYPE_OF_LIKED = "<!-- THIS IS TYPE OF LIKED -->%@";
	public static String TIMESTAMP_USER_LIKE = "<!-- TIMESTAMP USER LIKE -->%@";
	public static String ICON_LIKED_USER = "<!-- THIS IS ICON LIKED USER -->%@";
	public static String LIKED_USER = "<!-- THIS IS LIKED USER -->%@";
	public static String TIMESTAMP_LIKED_USER = "<!-- TIMESTAMP LIKED USER -->%@";
	public static String TYPE_OF_ACTION = "<!-- THIS IS TYPE OF ACTION -->%@";
	public static String ACTION_ENTER_HERE = "<!-- ACTION ENTER HERE -->";
	public static String LIKE_OR_UNLIKE = "<!-- THIS IS LIKE OR UNLIKE -->%@";
	public static String ID_LIKE_BUTTON = "<!-- THIS IS ID OF LIKE BUTTON -->%@";
	public static String ID_LIKE_COUNT = "<!-- THIS IS ID OF LIKE COUNT -->%@";
	public static String ID_CMT_COUNT = "<!-- THIS IS ID OF COMMENT COUNT -->%@";
	public static String LINK_UPLOAD_VIDEO = "http://null/video/%@";
	public static String LINK_SHARE = "http://null/sharelink/%@";
	public static String CLICK_ALBUM = "<!-- CLICK ALBUM -->";
	public static String TARGET = "<!-- TARGET GOES HERE -->";
	public static String LINE_1_TEXT_UNDO = "<!-- LINE_1_TEXT_UNDO -->";
	public static String LINE_2_TEXT_UNDO = "<!-- LINE_2_TEXT_UNDO -->";
	public static String LINK_UNDO = "http://null/Undo/%@";
	public static String LINK_UNDO_HIDE = "http://null/hideUndo/%@";
	public static String LINK_UNDO_DELETE = "http://null/deleteUndo/%@";
	public static String ID_OF_ITEM = "idItem:";
	public static String ICON_USER_SHARED = "<!-- THIS IS ICON USER SHARED -->%@";
	public static String USER_SHARE = "<!-- THIS IS USER SHARE-->%@";
	public static String USER_SHARED = "<!-- THIS IS USER SHARED -->%@";
	public static String TIME_USER_SHARE = "<!-- TIMESTAMP USER SHARE -->%@";
	public static String MESSAGE_USER_SHARE = "<!-- MESSAGE USER SHARE -->%@";
	public static String MESSAGE_USER_SHARED = "<!-- MESSAGE USER SHARED -->%@";
	public static String SHARE_BUTTON = "<!-- THIS IS SHARE BUTTON -->";
	public static String SHARE_SPACE = "<!-- THIS IS SHARE SPACE -->";
	public static String SHARE_POST_LINK = "http://null/shared/%@";
	public static String HEIGHT_VIDEO = "<!-- HEIGHT_VIDEO -->";

	private String timeStamp;
	private ArrayList<String> listFbidPhoto = new ArrayList<String>();
	private String iconUserShared = "";

	private Post post;

	public ArticleItem() {
		post = new Post();
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Post getPost() {
		if (post == null)
			post = new Post();
		return post;
	}

	public void setFbObjectId(String fbObjectId) {
		post.attachment.fbObjectId = fbObjectId;
	}

	public String getFbObjectId() {
		return post.attachment.fbObjectId;
	}

	public void setIconUserShared(String iconUserShared) {

		this.iconUserShared = iconUserShared;
	}

	public String getIconUserShared() {
		return iconUserShared;
	}

	public void setPermalink(String permalink) {
		post.permalink = permalink;
	}

	public String getPermalink() {
		if (post.attachment != null && post.attachment.media != null
				&& post.attachment.media.size() > 0 ){
			MediaItem item = post.attachment.media.get(0);
			if (item != null && !TextUtils.isEmpty(item.href)) {
				return item.href;
			}
		}
		return post.permalink;
	}

	public void setCanShare(boolean canShare) {
		post.shareInfo.canShare = canShare;
	}

	public boolean getCanShare() {
		return post.shareInfo.canShare;
	}

	public void setSharePhotoOf(String sharePhotoOf) {
		if (post.attachment.properties.size() == 0) {
			Property property = post.attachment.new Property();
			property.text = sharePhotoOf;
			post.attachment.properties.add(property);
		} else {
			post.attachment.properties.get(0).text = sharePhotoOf;
		}
	}

	public String getSharePhotoOf() {
		if (post.attachment.properties.size() == 0) {
			return "";
		} else {
			return post.attachment.properties.get(0).text;
		}
	}

	public void setSourceId(String sourceId) {
		post.sourceId = sourceId;
	}

	public String getSourceId() {
		return post.sourceId;
	}

	public void setAppId(String appId) {
		post.appId = appId;
	}

	public String getAppId() {
		return post.appId;
	}

	public void setNameTarget(String nameTarget) {
		post.target.name = nameTarget;
	}

	public String getNameTarget() {
		return post.target.name;
	}

	public ArrayList<String> getListFbidPhoto() {
		return listFbidPhoto;
	}

	public void setListFbidPhoto(ArrayList<String> listFbidPhoto) {
		this.listFbidPhoto = listFbidPhoto;
	}

	public String getPlace() {
		return post.placeObject.name;
	}

	public void setPlace(String place) {
		post.placeObject.name = place;
	}

	public boolean isUserLiked() {
		return post.likeInfo.userLike;
	}

	public void setUserLiked(boolean userLiked) {
		post.likeInfo.userLike = userLiked;
	}

	public String getLat() {
		if (post.placeObject.geometry.coordinates.size() > 1)
			return post.placeObject.geometry.coordinates.get(0);
		else
			return "";
	}

	public String getLon() {
		if (post.placeObject.geometry.coordinates.size() > 1)
			return post.placeObject.geometry.coordinates.get(1);
		else
			return "";
	}

	public void setLat(String lat) {
		if (post.placeObject.geometry.coordinates.size() > 1) {
			post.placeObject.geometry.coordinates.remove(0);
			post.placeObject.geometry.coordinates.add(0, lat);
		} else {
			post.placeObject.geometry.coordinates = new ArrayList<String>();
			post.placeObject.geometry.coordinates.add(lat);
			post.placeObject.geometry.coordinates.add("");
		}

	}

	public void setLon(String lon) {
		if (post.placeObject.geometry.coordinates.size() > 1) {
			post.placeObject.geometry.coordinates.remove(1);
			post.placeObject.geometry.coordinates.add(lon);
		} else {
			post.placeObject.geometry.coordinates = new ArrayList<String>();
			post.placeObject.geometry.coordinates.add("");
			post.placeObject.geometry.coordinates.add(lon);
		}

	}

	public String getPlaceId() {
		return post.placeObject.pageId;
	}

	public void setPlaceId(String placeId) {
		post.placeObject.pageId = placeId;
	}

	public String getTypeAttachment() {
		if (post.attachment != null && post.attachment.media != null && post.attachment.media.size() > 0) {
			return post.attachment.media.get(0).type;
		} else {
			return "";
		}
	}

	public void setTypeAttachment(String typeAttachment) {
		if (post.attachment.media.size() > 0) {
			post.attachment.media.get(0).type = typeAttachment;
		} else {
			MediaItem item = post.attachment.new MediaItem();
			item.type = typeAttachment;
			post.attachment.media.add(item);
		}
	}

	public int getType() {
		return post.type;
	}

	public void setType(int type) {
		post.type = type;
	}

	public String getShareLink() {
		return post.attachment.href;
	}

	public void setShareLink(String shareLink) {
		post.attachment.href = shareLink;
	}

	public String getShareContent() {
		return post.attachment.description;
	}

	public void setShareContent(String shareContent) {
		post.attachment.description = shareContent;
	}

	public String getShareVia() {
		return post.attachment.caption;
	}

	public void setShareVia(String shareVia) {
		post.attachment.caption = shareVia;
	}

	public String getShareTitle() {
		return post.attachment.name;
	}

	public void setShareTitle(String shareTitle) {
		post.attachment.name = shareTitle;
	}

	public String getSrcIconImage() {
		if (post.attachment.media != null && (post.attachment.media.size() > 0)) {
			return post.attachment.media.get(0).src.replace("_s.", "_n.");
		} else {
			return "";
		}
	}

	public void setSrcIconImage(String srcIconImage) {
		if (post.attachment.media.size() == 0) {
			MediaItem item = post.attachment.new MediaItem();
			item.src = srcIconImage;
			post.attachment.media.add(item);
		} else {
			post.attachment.media.get(0).src = srcIconImage;
		}
	}

	public String getPostId() {
		return post.postId;
	}

	public void setPostId(String postId) {
		post.postId = postId;
	}

	public String getProfileIcon() {
		return post.profile.picBig;
	}

	public void setProfileIcon(String profileIcon) {
		post.profile.picBig = profileIcon;
	}

	public String getProfileName() {
		return post.profile.name;
	}

	public void setProfileName(String profileName) {
		post.profile.name = profileName;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getMessage() {
		return post.message;
	}

	public void setMessage(String message) {
		post.message = message;
	}

	public int getLikeCount() {
		return post.likeInfo.likeCount;
	}

	public void setLikeCount(int likeCount) {
		post.likeInfo.likeCount = likeCount;
	}

	public int getCommentCount() {
		return post.commentInfo.count;
	}

	public void setCommentCount(int commentCount) {
		post.commentInfo.count = commentCount;
	}

	public String getSource_url() {
		if (post.attachment.media.size() > 0 && post.attachment.media.get(0).video != null) {
			return post.attachment.media.get(0).video.sourceUrl;
		} else {
			return "";
		}
	}

	public void setSource_url(String source_url) {
		if (post.attachment.media.size() > 0) {
			post.attachment.media.get(0).video.sourceUrl = source_url;
		} else {
			MediaItem item = post.attachment.new MediaItem();
			item.video.sourceUrl = source_url;
			post.attachment.media.add(item);
		}
	}

	public String getActorId() {
		return post.actorId;
	}

	public void setActorId(String actorId) {
		post.actorId = actorId;
	}

}
