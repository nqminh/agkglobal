package com.example.besafepost;

import com.example.besafepost.utils.Utils;

import android.app.Application;
import android.content.Context;

public class BesafePostApplication extends Application {
	private static Context context;
	

	synchronized public static Context getContext() {
		return context;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		context = getBaseContext();
		if (Utils.isFirstInstall()) createDefaultWords();
	}

	//What mean? (Khi bam Pause/Resume thi gia tri ham nay bang false)
	public static boolean isActivityVisible() {
		return activityVisible;
	}

	public static void activityResumed() {
		activityVisible = true;
	}

	public static void activityPaused() {
		activityVisible = false;
	}

	private static boolean activityVisible;

	private void createDefaultWords(){
		Utils.putStringIntoListStringPreference(Utils.keyBannedWord, "Retarded");
		Utils.putStringIntoListStringPreference(Utils.keyBannedWord, "Faggot");
		Utils.putStringIntoListStringPreference(Utils.keyBannedWord, "Gay");
		Utils.putStringIntoListStringPreference(Utils.keyBannedWord, "Nigga");
		Utils.putStringIntoListStringPreference(Utils.keyBannedWord, "Bipolar");
	}
}
