package com.example.besafepost.receiver;

import com.example.besafepost.interfaces.IReiceiverCallback;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class SmsReceiver extends BroadcastReceiver {
	public static IReiceiverCallback callback = null;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (callback != null) callback.onReceiveMessage(); 
	}
}