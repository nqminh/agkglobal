package com.example.besafepost.fragment;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.besafepost.activity.LogInAppActivity;
import com.example.besafepost.activity.MainActivity;
import com.example.besafepost.activity.R;
import com.example.besafepost.adapter.ListAccountAdapter;
import com.example.besafepost.interfaces.IAsyncTaskDelegate;
import com.example.besafepost.model.ItemForListAccount;
import com.example.besafepost.singleton.LoginObject;
import com.example.besafepost.task.AbstractTask;
import com.example.besafepost.task.UpdateUserInfor;
import com.example.besafepost.utils.Const;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.StringUtils;
import com.example.besafepost.utils.Utils;
import com.facebook.Session;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class FragmentSetting extends BaseFragment implements OnClickListener,
		android.widget.RadioGroup.OnCheckedChangeListener, IAsyncTaskDelegate<AbstractTask> {
	private Switch cdSwitch, tipSwitch, tipSwitchTwitter;
	private SharedPreferences twitterPrefs, tipPrefs, cdPrefs, delayPrefs;
	private Editor twitterEditor, tipEditor, cdEditor, delayEditor;
	private RadioGroup radioGroup;
	private EditText edtFullName, edtAge, edtSex, edtEmailAddress,
			edtPhoneNumber;
	private Button btnSendInfo;
	private Button btnLogOut;
	private RadioButton btnNoDelay, btn10Sec, btn20Sec, btn30Sec, btn60Sec;
	private View viewSocialNetwork, viewBannedWord, viewBannerAddress;
	private Button btnBackBlockWord, btnBackAddBlockWord, btnBackBlockAddress,
			btnBackAddBlockAddress, btnBackSocialNetwork, btnAddBlockWord,
			btnAddBlockAddress, btnSaveBlockWord, btnSaveBlockAddress;
	private ListView lvBannedWord, lvBannedAddress, lvAccount;
	private EditText edtEnterBlockWord, edtEnterBlockAddress;
	View view;
	private static final int STEP_BLOCK_WORD = 1, STEP_BLOCK_ADDRESS = 2,
			STEP_ENTER_BLOCK_WORD = 3, STEP_ENTER_BLOCK_ADDRESS = 4,
			STEP_SOCIAL_NETWORK = 5, STEP_GANERAL = 6;
	private View fragmentSettingGaneral, fragmentSettingBlockWord,
			fragmentSettingBlockAddress, fragmentSettingAddBlockWord,
			fragmentSettingAddBlockAddress, fragmentSettingSocialNetwork;
	private List<String> listBannedWord;
	private List<String> listBannedAddress;
	private List<ItemForListAccount> listAccount;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.setting_fragment, null);
		findAllViewById();
		gotoBlockedWords(STEP_GANERAL);
		cdPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		tipPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		twitterPrefs = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		delayPrefs = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		setBesafeTwitterSwitch();
		setBesafeTipSwitch();
		setCountDownSwitch();
		cdEditor = cdPrefs.edit();
		tipEditor = tipPrefs.edit();
		twitterEditor = twitterPrefs.edit();
		delayEditor = delayPrefs.edit();
		radioGroup.setOnCheckedChangeListener(this);
		onCheckedChanged(radioGroup, R.id.btn10Sec);
		setRadioGroupDelay();
		// attach a listener to check for changes in state
		cdSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					cdEditor.putBoolean(StringUtils.KEY_COUNT_DOWN, true);
				} else {
					cdEditor.putBoolean(StringUtils.KEY_COUNT_DOWN, false);
				}
				cdEditor.commit();
			}
		});

		tipSwitchTwitter
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							twitterEditor.putBoolean(StringUtils.KEY_TWEET_TIP,
									true);
						} else {
							twitterEditor.putBoolean(StringUtils.KEY_TWEET_TIP,
									false);
						}
						twitterEditor.commit();
					}
				});

		tipSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					tipEditor.putBoolean(StringUtils.KEY_POST_TIP, true);
				} else {
					tipEditor.putBoolean(StringUtils.KEY_POST_TIP, false);
				}
				tipEditor.commit();
			}
		});
		// don't get info. update only
		// DialogUtils
		// .showProgressDialog(getActivity(), "", "Loading UserInfo...");
		// new getUserInfo().execute(userId);
		return view;
	}

	private void findAllViewById() {
		btnAddBlockWord = (Button) view.findViewById(R.id.btnAddBlockWord);
		btnAddBlockWord.setOnClickListener(this);
		btnAddBlockAddress = (Button) view
				.findViewById(R.id.btnAddBlockAddress);
		btnAddBlockAddress.setOnClickListener(this);
		btnBackBlockWord = (Button) view.findViewById(R.id.btnBackBlockWord);
		btnBackBlockWord.setOnClickListener(this);
		btnBackBlockAddress = (Button) view
				.findViewById(R.id.btnBackBlockAddAddress);
		btnBackBlockAddress.setOnClickListener(this);
		btnBackAddBlockWord = (Button) view
				.findViewById(R.id.btnBackAddBlockWord);
		btnBackAddBlockWord.setOnClickListener(this);
		btnBackAddBlockAddress = (Button) view
				.findViewById(R.id.btnBackAddBlockAddress);
		btnBackAddBlockAddress.setOnClickListener(this);
		btnBackSocialNetwork = (Button) view
				.findViewById(R.id.btnBackSocialNetwork);
		btnBackSocialNetwork.setOnClickListener(this);
		btnSaveBlockAddress = (Button) view
				.findViewById(R.id.btnSaveBlockAddress);
		btnSaveBlockAddress.setOnClickListener(this);
		btnSaveBlockWord = (Button) view.findViewById(R.id.btnSaveBlockWord);
		btnSaveBlockWord.setOnClickListener(this);
		edtEnterBlockAddress = (EditText) view
				.findViewById(R.id.edtEnterBlockAddress);
		edtEnterBlockWord = (EditText) view
				.findViewById(R.id.edtEnterBlockWord);
		radioGroup = (RadioGroup) view.findViewById(R.id.radioGroupMenu);
		edtFullName = (EditText) view.findViewById(R.id.edtFullName);
		btnLogOut = (Button) view.findViewById(R.id.btnLogOutApp);
		viewBannedWord = view.findViewById(R.id.viewBannerWork);
		viewBannerAddress = view.findViewById(R.id.viewBannerAddresses);
		viewSocialNetwork = view.findViewById(R.id.viewSocialNetwork);
		viewBannedWord.setOnClickListener(this);
		viewBannerAddress.setOnClickListener(this);
		viewSocialNetwork.setOnClickListener(this);
		btnLogOut.setOnClickListener(this);
		edtAge = (EditText) view.findViewById(R.id.edtAge);
		edtSex = (EditText) view.findViewById(R.id.edtSex);
		edtEmailAddress = (EditText) view.findViewById(R.id.edtEmailAddress);
		edtPhoneNumber = (EditText) view.findViewById(R.id.edtPhoneNumber);
		btnSendInfo = (Button) view.findViewById(R.id.btnSend);
		btnSendInfo.setOnClickListener(this);
		cdSwitch = (Switch) view.findViewById(R.id.mySwitch);
		tipSwitchTwitter = (Switch) view.findViewById(R.id.tipSwitchTwitter);
		tipSwitch = (Switch) view.findViewById(R.id.tipSwitch);

		btnNoDelay = (RadioButton) view.findViewById(R.id.btnNoDelay);
		btn10Sec = (RadioButton) view.findViewById(R.id.btn10Sec);
		btn20Sec = (RadioButton) view.findViewById(R.id.btn20Sec);
		btn30Sec = (RadioButton) view.findViewById(R.id.btn30Sec);
		btn60Sec = (RadioButton) view.findViewById(R.id.btn60Sec);
		fragmentSettingAddBlockAddress = view
				.findViewById(R.id.fragment_setting_add_block_address);
		fragmentSettingAddBlockWord = view
				.findViewById(R.id.fragment_setting_add_block_word);
		fragmentSettingBlockAddress = view
				.findViewById(R.id.fragment_setting_block_address);
		fragmentSettingBlockWord = view
				.findViewById(R.id.fragment_setting_block_word);
		fragmentSettingGaneral = view
				.findViewById(R.id.fragment_setting_ganeral);
		fragmentSettingSocialNetwork = view
				.findViewById(R.id.fragment_setting_social_network);
		lvBannedAddress = (ListView) view.findViewById(R.id.lvBannedAddress);
		lvBannedWord = (ListView) view.findViewById(R.id.lvBannedWord);
		lvAccount = (ListView) view.findViewById(R.id.lvAccount);
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.btnNoDelay:
			delayEditor.putInt(StringUtils.KEY_DELAY, 0);
			break;
		case R.id.btn10Sec:
			delayEditor.putInt(StringUtils.KEY_DELAY, 10);
			break;
		case R.id.btn20Sec:
			delayEditor.putInt(StringUtils.KEY_DELAY, 20);
			break;
		case R.id.btn30Sec:
			delayEditor.putInt(StringUtils.KEY_DELAY, 30);
			break;
		case R.id.btn60Sec:
			delayEditor.putInt(StringUtils.KEY_DELAY, 60);
			break;
		default:
			break;
		}
		delayEditor.commit();
	}

	@Override
	public void onResume() {
		// Utils.showKeyboard(getActivity(), edtFullName);
		super.onResume();
	}

	public void setRadioGroupDelay() {
		int delay;
		delay = delayPrefs.getInt(StringUtils.KEY_DELAY, 10);
		switch (delay) {
		case 0:
			btnNoDelay.setChecked(true);
			break;
		case 10:
			btn10Sec.setChecked(true);
			break;
		case 20:
			btn20Sec.setChecked(true);
			break;
		case 30:
			btn30Sec.setChecked(true);
			break;
		case 60:
			btn60Sec.setChecked(true);
			break;
		default:
			break;
		}

	}

	public void setBesafeTwitterSwitch() {
		boolean tipSwitchEnable;
		if (twitterPrefs != null) {
			tipSwitchEnable = twitterPrefs.getBoolean(
					StringUtils.KEY_TWEET_TIP, true);
			tipSwitchTwitter.setChecked(tipSwitchEnable);
		}
	}

	public void setBesafeTipSwitch() {
		boolean tipSwitchEnable;
		if (tipPrefs != null) {
			tipSwitchEnable = tipPrefs.getBoolean(StringUtils.KEY_POST_TIP,
					true);
			tipSwitch.setChecked(tipSwitchEnable);
		}
	}

	public void setCountDownSwitch() {
		boolean cdSwitchEnable;
		cdSwitchEnable = cdPrefs.getBoolean(StringUtils.KEY_COUNT_DOWN, true);
		cdSwitch.setChecked(cdSwitchEnable);
	}

	public boolean checkInfoNotEmpty() {
		if (edtFullName.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "Please enter your full name",
					Toast.LENGTH_LONG).show();
			Utils.showKeyboard(getActivity(), edtFullName);
			return false;
		} else if (edtAge.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "Please enter your age",
					Toast.LENGTH_LONG).show();
			Utils.showKeyboard(getActivity(), edtAge);
			return false;

		} else if (edtSex.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "Please enter your sex",
					Toast.LENGTH_LONG).show();
			Utils.showKeyboard(getActivity(), edtSex);
			return false;

		} else if (edtEmailAddress.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "Please enter your email address",
					Toast.LENGTH_LONG).show();
			Utils.showKeyboard(getActivity(), edtEmailAddress);
			return false;

		} else if (edtPhoneNumber.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "Please enter your phone number",
					Toast.LENGTH_LONG).show();
			Utils.showKeyboard(getActivity(), edtPhoneNumber);
			return false;

		} else {
			return true;
		}
	}

	@Override
	public void onClick(View v) {
		if (v == btnSendInfo) {
			if (checkInfoNotEmpty()) {
				DialogUtils.showProgressDialog(getActivity(), "",
						"Updating...", false);
				LoginObject object = LoginObject.get_instance();
				object.setFullName(edtFullName.getText().toString());
				object.setAge(edtAge.getText().toString());
				object.setEmail(edtEmailAddress.getText().toString());
				object.setPhone(edtPhoneNumber.getText().toString());
				new UpdateUserInfor(this).execute();
			}
		}

		if (v == btnLogOut) {
			final Dialog dialog = new Dialog(getActivity());
			dialog.setContentView(R.layout.custom_dialog_logout);
			Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
			Button btnOK = (Button) dialog.findViewById(R.id.btnOK);
			btnCancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			btnOK.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					LoginObject object = new LoginObject();
					LoginObject.set_instance(object);
					LoginObject.get_instance().saveToCache();
					// remove tab,after log out,run again,fragment post called
					SharedPreferences prefs = getActivity()
							.getSharedPreferences("myShared", 0);
					Editor editorTab = prefs.edit();
					editorTab.remove(MainActivity.TAB);
					editorTab.commit();
					Session session = Session.getActiveSession();
					if (session != null && session.isOpened()) {
						session.closeAndClearTokenInformation();
					}
					
					SharedPreferences.Editor editor2 = getActivity().getSharedPreferences(Const.PREF_NAME, 0).edit();
					editor2.remove(Const.IEXTRA_OAUTH_TOKEN);
					editor2.remove(Const.PREF_KEY_OAUTH_SECRET);
					editor2.remove(Const.PREF_KEY_TWITTER_LOGIN);
					editor2.commit();
					CookieManager cookieManager = CookieManager.getInstance();
					cookieManager.removeAllCookie();
					Intent intent = new Intent(getActivity(),
							LogInAppActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(intent);
					dialog.dismiss();
					getActivity().finish();
				}
			});
			dialog.show();
		}
		if (v == viewBannedWord) {
			gotoBlockedWords(STEP_BLOCK_WORD);
			Utils.hideKeyboard(getActivity());
		}
		if (v == viewBannerAddress) {
			gotoBlockedWords(STEP_BLOCK_ADDRESS);
			Utils.hideKeyboard(getActivity());
		}
		if (v == viewSocialNetwork) {
			gotoBlockedWords(STEP_SOCIAL_NETWORK);
			Utils.hideKeyboard(getActivity());
			refreshListAccount();
		}
		if (v == btnBackAddBlockAddress) {
			Utils.hideKeyboard(getActivity());
			gotoBlockedWords(STEP_BLOCK_ADDRESS);
		}
		if (v == btnBackAddBlockWord) {
			Utils.hideKeyboard(getActivity());
			gotoBlockedWords(STEP_BLOCK_WORD);
		}
		if (v == btnBackBlockAddress) {
			gotoBlockedWords(STEP_GANERAL);
		}
		if (v == btnBackBlockWord) {
			gotoBlockedWords(STEP_GANERAL);
		}
		if (v == btnBackSocialNetwork) {
			gotoBlockedWords(STEP_GANERAL);
		}
		if (v == btnAddBlockAddress) {
			gotoBlockedWords(STEP_ENTER_BLOCK_ADDRESS);
			Utils.showKeyboard(getActivity(), edtEnterBlockAddress);
			edtEnterBlockAddress.setText("");
		}
		if (v == btnAddBlockWord) {
			gotoBlockedWords(STEP_ENTER_BLOCK_WORD);
			Utils.showKeyboard(getActivity(), edtEnterBlockWord);
			edtEnterBlockWord.setText("");
		}
		if (v == btnSaveBlockWord) {
			if (edtEnterBlockWord.getText().toString().isEmpty()) {
				Toast.makeText(getActivity(), "Please enter block word",
						Toast.LENGTH_LONG).show();
			} else {
				Utils.putStringIntoListStringPreference(Utils.keyBannedWord,
						edtEnterBlockWord.getText().toString());
				gotoBlockedWords(STEP_BLOCK_WORD);
				Utils.hideKeyboard(getActivity());

			}
		}
		if (v == btnSaveBlockAddress) {
			if (edtEnterBlockAddress.getText().toString().isEmpty()) {
				Toast.makeText(getActivity(), "Please enter block address",
						Toast.LENGTH_LONG).show();
			} else {
				Utils.putStringIntoListStringPreference(Utils.keyBannedAddress,
						edtEnterBlockAddress.getText().toString());
				gotoBlockedWords(STEP_BLOCK_ADDRESS);
				Utils.hideKeyboard(getActivity());

			}
		}
	}

	private void refreshListBannedWord() {
		listBannedWord = Utils.getListStringFromPreference(Utils.keyBannedWord);
		ListAdapter adapter = new AdapterListBanned(getActivity(),
				R.layout.row_banned, listBannedWord, Utils.keyBannedWord);
		lvBannedWord.setAdapter(adapter);
	}

	public void refreshListAccount() {
		String accFb = Utils.getStringFromFreference(Utils.keyAcccountFb);
		String screenName = Utils
				.getStringFromFreference(Utils.keyAcccountTwitter);
		listAccount = new ArrayList<ItemForListAccount>();
		if (!StringUtils.isEmptyString(accFb)) {
			ItemForListAccount temp = new ItemForListAccount();
			temp.setAccountName(accFb);
			temp.setAccountType("Facebook");
			temp.setIconFromSrc(R.drawable.icon_facebook);
			temp.setColorAccountNameFromSrc(R.color.blue_fb);
			listAccount.add(temp);
		}
		if (!StringUtils.isEmptyString(screenName)) {
			ItemForListAccount temp = new ItemForListAccount();
			temp.setAccountName("@" + screenName);
			temp.setAccountType("Twitter");
			temp.setIconFromSrc(R.drawable.icon_twitter);
			temp.setColorAccountNameFromSrc(R.color.blue_twitter);
			listAccount.add(temp);
		}
		if (lvAccount != null) {
			ListAccountAdapter adapter = new ListAccountAdapter(getActivity(),
					R.layout.list_account_row, listAccount);
			lvAccount.setAdapter(adapter);
		}
	}

	private void refreshListBannedAddress() {
		listBannedAddress = Utils
				.getListStringFromPreference(Utils.keyBannedAddress);
		ListAdapter adapter = new AdapterListBanned(getActivity(),
				R.layout.row_banned, listBannedAddress, Utils.keyBannedAddress);
		lvBannedAddress.setAdapter(adapter);
	}

	private void gotoBlockedWords(int step) {
		fragmentSettingGaneral
				.setVisibility(step == STEP_GANERAL ? View.VISIBLE : View.GONE);
		fragmentSettingAddBlockAddress
				.setVisibility(step == STEP_ENTER_BLOCK_ADDRESS ? View.VISIBLE
						: View.GONE);
		fragmentSettingAddBlockWord
				.setVisibility(step == STEP_ENTER_BLOCK_WORD ? View.VISIBLE
						: View.GONE);
		fragmentSettingBlockAddress
				.setVisibility(step == STEP_BLOCK_ADDRESS ? View.VISIBLE
						: View.GONE);
		fragmentSettingBlockWord
				.setVisibility(step == STEP_BLOCK_WORD ? View.VISIBLE
						: View.GONE);
		fragmentSettingSocialNetwork
				.setVisibility(step == STEP_SOCIAL_NETWORK ? View.VISIBLE
						: View.GONE);
		if (step == STEP_BLOCK_WORD) {
			refreshListBannedWord();
		}
		if (step == STEP_BLOCK_ADDRESS) {
			refreshListBannedAddress();
		}

	}

	public class AdapterListBanned extends ArrayAdapter<String> {
		private List<String> listBanned;
		private LayoutInflater inflater;
		private int LayoutResourceId;
		static final int MIN_DISTANCE = 20;
		private float downX, upX;
		private String key;
		private List<Integer> listPositionVisibleDel = new ArrayList<Integer>();

		public AdapterListBanned(Context context, int LayoutResourceId,
				List<String> listBanned, String key) {
			super(context, LayoutResourceId, listBanned);
			this.key = key;
			this.listBanned = listBanned;
			this.LayoutResourceId = LayoutResourceId;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View row = null;
			row = inflater.inflate(LayoutResourceId, parent, false);
			final Button btnDel = (Button) row.findViewById(R.id.btn_del);
			TextView textBannded = (TextView) row.findViewById(R.id.tv_banned);
			textBannded.setText(listBanned.get(position));
			if (listPositionVisibleDel.contains(position)) {
				btnDel.setVisibility(View.VISIBLE);
			}
			row.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: {
						downX = event.getX();
						return true;
					}
					case MotionEvent.ACTION_UP: {
						upX = event.getX();
						float deltaX = downX - upX;

						// swipe horizontal?
						if (Math.abs(deltaX) > MIN_DISTANCE) {
							// left or right
							if (deltaX < 0) {
								//left to right
								btnDel.setVisibility(View.VISIBLE);
								listPositionVisibleDel.add(position);
								return false;
							}
							if (deltaX > 0) {
								//right to left
								return false;
							}
						}
						return false;
					}
					}
					return false;
				}
			});
			btnDel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					Utils.deleteStringInListStringPreference(key, position);
					listBanned.remove(position);
					listPositionVisibleDel.remove((Integer) position);
					for (int i = 0; i < listPositionVisibleDel.size(); i++) {
						if (listPositionVisibleDel.get(i) > position) {
							listPositionVisibleDel.add(i,
									listPositionVisibleDel.get(i) - 1);
							listPositionVisibleDel.remove(i + 1);
						}
					}
					notifyDataSetChanged();
				}
			});
			return row;
		}
	}

	@Override
	public void onGetStart(AbstractTask task) {
		
	}

	@Override
	public void onGetSuccess(AbstractTask task) {
		if (task instanceof UpdateUserInfor) {
			DialogUtils.dismissDialog();
			JSONObject jsonResult = null;
			boolean isSuccess = false;
			try {
				jsonResult = new JSONObject(task.getResult());
				if (jsonResult.getString("returnCode").equalsIgnoreCase(
						"SUCCESS")) {
					isSuccess = true;
				}
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			if (isSuccess) {
				Toast.makeText(getActivity(), "Update contact successful",
						Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getActivity(), "Update contact not successful",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void onGetFailure(AbstractTask task, Exception exception) {
		DialogUtils.dismissDialog();	
		exception.printStackTrace();
	}
}
