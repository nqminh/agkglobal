package com.example.besafepost.fragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.besafepost.activity.AdjustPhotoActivity;
import com.example.besafepost.activity.CheckInActivity;
import com.example.besafepost.activity.CommentActivity;
import com.example.besafepost.activity.MainActivity;
import com.example.besafepost.activity.OpenLinkActivity;
import com.example.besafepost.activity.OpenLinkAndShareAtivity;
import com.example.besafepost.activity.PlayVideoActivity;
import com.example.besafepost.activity.R;
import com.example.besafepost.activity.ShareActivity;
import com.example.besafepost.activity.ShowAlbumActivity;
import com.example.besafepost.activity.StatusActivity;
import com.example.besafepost.activity.TipActivity;
import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.interfaces.ISwipeDetector;
import com.example.besafepost.model.ArticleItem;
import com.example.besafepost.singleton.ManagerFacebookApp;
import com.example.besafepost.task.UpdateUserInfor;
import com.example.besafepost.utils.ActivitySwipeDetector;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.FacebookUtils;
import com.example.besafepost.utils.FileUtils;
import com.example.besafepost.utils.Log;
import com.example.besafepost.utils.ObjectSerializer;
import com.example.besafepost.utils.StringUtils;
import com.example.besafepost.utils.Utils;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshWebView;

public class FragmentPost extends BaseFragment implements OnRefreshListener2<WebView>,
		IActivityDelegate, OnClickListener, ISwipeDetector {

	PullToRefreshWebView mPullRefreshWebView;
	private WebView webView;
	public static boolean loadPageFinish = false;
	private View refreshLoading;
	private static String postId;
	private static String photoId = "";
	private MyWebViewClient webViewClient;
	private boolean isLike;
	private int likeCount;
	HashMap<String, String> hMapItemHided = new HashMap<String, String>();
	HashMap<String, String> hMapItemDeleted = new HashMap<String, String>();
	public static ArrayList<String> listPostIdHide = new ArrayList<String>();
	public static ArrayList<String> listPostIdDel = new ArrayList<String>();
	View imgStatus, imgPhoto, imgCheckin;
	View toolbar;
	private ArticleItem currentItem;

	private ImageView imgNewStory;
	private View view;
	private static final int START_FOR_NOTHING = 4582;
	// , loginLayout;
	/*
	 * facebook login values
	 */
	public static final List<String> FB_PERMISSIONS_READ = Arrays.asList("email", "user_about_me",
	 "user_photos",
			"user_birthday", "user_likes", "read_stream",
			 "friends_photos",
			"read_insights", "read_friendlists");
	public static final List<String> FB_PERMISSIONS_PUBLISH = Arrays.asList("publish_actions",
	// "photo_upload",
	// "video_upload",
			"publish_stream");

	// end facebook login values

	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.post_fragment, null);
		findAllViewById();
		// get list post id hided
		SharedPreferences prefs = getActivity().getSharedPreferences(
				MainActivity.SHARED_PREFS_FILE, Context.MODE_PRIVATE);
		try {
			listPostIdHide = (ArrayList<String>) ObjectSerializer.deserialize(prefs.getString(
					MainActivity.LIST_POST_ID_HIDED,
					ObjectSerializer.serialize(new ArrayList<String>())));
		} catch (IOException e) {
			e.printStackTrace();
		}

		// handle event pull down or up for hide or show toolbar and taskbar
		ActivitySwipeDetector swipeDetector = new ActivitySwipeDetector(this);
		webView = mPullRefreshWebView.getRefreshableView();
		mPullRefreshWebView.setOnTouchListener(swipeDetector);
		webView.setOnTouchListener(swipeDetector);
		webView.setHorizontalScrollbarOverlay(false);

		// add webview clien and java sccript interface
		webViewClient = new MyWebViewClient();
		webView.setWebViewClient(webViewClient);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.addJavascriptInterface(new MyJavaScriptInterface(), "Android");
		loginFacebook();

		// end facebook login
		return view;
	}

	private void findAllViewById() {
		refreshLoading = view.findViewById(R.id.refresh_header);
		imgStatus = view.findViewById(R.id.viewStatus);
		imgPhoto = view.findViewById(R.id.viewPhoto);
		imgCheckin = view.findViewById(R.id.viewCheckIn);
		imgNewStory = (ImageView) view.findViewById(R.id.imgNewStory);
		imgNewStory.setOnClickListener(this);
		imgStatus.setOnClickListener(this);
		imgPhoto.setOnClickListener(this);
		imgCheckin.setOnClickListener(this);
		toolbar = view.findViewById(R.id.toolbar);
		mPullRefreshWebView = (PullToRefreshWebView) view.findViewById(R.id.pull_refresh_webview);
		// set listtenner pull to refersh
		mPullRefreshWebView.setOnRefreshListener(this);
	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			FacebookUtils.getNewFeeds(getActivity(), webView, StringUtils.LIMIT, FragmentPost.this,
					true);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Log.i(Log.TAG, url);
			if (url.startsWith(ArticleItem.COMMENT_LINK)) {
				postId = url.substring(ArticleItem.COMMENT_LINK.length());
				String getContentForComment = "javascript:getContentForComment('"
						+ ArticleItem.ID_OF_ITEM + postId + "');";
				webView.loadUrl(getContentForComment);
			} else if (url.startsWith(ArticleItem.LOAD_MORE_LINK)) {
				postId = url.substring(ArticleItem.LOAD_MORE_LINK.length());
				String getContentForComment = "javascript:getContentForComment('"
						+ ArticleItem.ID_OF_ITEM + postId + "');";
				webView.loadUrl(getContentForComment);
			} else if (url.startsWith(ArticleItem.LIKE_LINK)) {
				postId = url.substring(ArticleItem.LIKE_LINK.length());
				String getText = "javascript:getTextLikeOrUnlike('" + "postId:" + postId + "');";
				webView.loadUrl(getText);

			} else if (url.startsWith(ArticleItem.SHARE_POST_LINK)) {
				postId = url.substring(ArticleItem.SHARE_POST_LINK.length());
				Intent i = new Intent(getActivity(), ShareActivity.class);
				i.putExtra("postID", postId);
				startActivityForResult(i, Utils.AFTER_SHARE_FACEBOOK);

			} else if (url.startsWith(ArticleItem.LIKE_ON_PHOTO_LINK)) {
				String[] listId = url.split(ArticleItem.LIKE_ON_PHOTO_LINK);
				photoId = listId[1];
				postId = listId[2];
				Log.i("PostID", postId);
				Log.i("PhotoID", photoId);
				String getText = "javascript:getTextLikeOrUnlike('" + "postId:" + postId + "');";
				webView.loadUrl(getText);

			} else if (url.startsWith(ArticleItem.LINK_SHARE)) {

				String postId = url.substring(ArticleItem.LINK_SHARE.length());
				Intent i = new Intent(getActivity(), OpenLinkAndShareAtivity.class);
				i.putExtra("postId", postId);
				startActivityForResult(i, START_FOR_NOTHING);

			} else if (url.startsWith(ArticleItem.LINK_UPLOAD_VIDEO)) {

				String uploadvideolink = url.substring(ArticleItem.LINK_UPLOAD_VIDEO.length());
				Intent i = new Intent(getActivity(), PlayVideoActivity.class);
				i.putExtra("uploadvideolink", uploadvideolink);
				startActivityForResult(i, START_FOR_NOTHING);
			} else if (url.startsWith("http://onclick/album")) {
				postId = url.substring(20);
				Intent i = new Intent(getActivity(), ShowAlbumActivity.class);
				i.putExtra("postId", postId);
				startActivityForResult(i, Utils.REQUEST_CODE_SHOW_ALBUM);
			} else if (url.startsWith(ArticleItem.LINK_HIDE_DELELE_POST)) {

				String postIdHideOrDelete = url.substring(ArticleItem.LINK_HIDE_DELELE_POST
						.length());
				boolean canDel = false;
				ArticleItem articleItemHideOrDelete = new ArticleItem();
				articleItemHideOrDelete = FacebookUtils
						.getCurrentItemFromListArticleItem(postIdHideOrDelete);
				if (articleItemHideOrDelete.getAppId().equals(FacebookUtils.APP_ID)
						&& articleItemHideOrDelete.getActorId().equals(
								ManagerFacebookApp.getInstance().getUid())) {
					canDel = true;
				}
				customDialogHide(postIdHideOrDelete, canDel);

			} else if (url.startsWith(ArticleItem.LINK_UNDO_HIDE)) {
				String key = url.substring(ArticleItem.LINK_UNDO_HIDE.length());
				String oldItemText = hMapItemHided.get(key);
				oldItemText = oldItemText.replace("\n", "<br>");
				oldItemText = oldItemText.replace("\t", "");
				oldItemText = oldItemText.replace("\r", "");
				oldItemText = oldItemText.replace("'", "&#39;");
				changeTextInHtml(key, oldItemText);
				hMapItemHided.remove(key);
				String postIdHide = key.substring(ArticleItem.ID_OF_ITEM.length());
				listPostIdHide.remove(postIdHide);

			} else if (url.startsWith(ArticleItem.LINK_UNDO_DELETE)) {
				String key = url.substring(ArticleItem.LINK_UNDO_DELETE.length());
				String oldItemText = hMapItemDeleted.get(key);
				oldItemText = oldItemText.replace("\n", "<br>");
				oldItemText = oldItemText.replace("\t", "");
				oldItemText = oldItemText.replace("\r", "");
				oldItemText = oldItemText.replace("'", "&#39;");
				changeTextInHtml(key, oldItemText);
				hMapItemDeleted.remove(key);
				String postIdDel = key.substring(ArticleItem.ID_OF_ITEM.length());
				listPostIdDel.remove(postIdDel);
			} else {
				Intent i = new Intent(getActivity(), OpenLinkActivity.class);
				i.putExtra("url", url);
				startActivityForResult(i, START_FOR_NOTHING);
			}
			return true;
		}
	}

	@Override
	public void onPullDownToRefresh(PullToRefreshBase<WebView> refreshView) {
		StringUtils.LIMIT = StringUtils.LIMIT_ADVANCE;
		toolbar.setVisibility(View.VISIBLE);
		for (int i = 0; i < FragmentPost.listPostIdDel.size(); i++) {
			FacebookUtils.delete(FragmentPost.listPostIdDel.get(i));
		}
		FacebookUtils.getNewFeeds(getActivity(), webView, StringUtils.LIMIT, this, true);
	}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase<WebView> refreshView) {
		StringUtils.LIMIT += StringUtils.LIMIT_ADVANCE;
		FacebookUtils.getNewFeeds(getActivity(), webView, StringUtils.LIMIT, this, false);
		webView.setWebViewClient(webViewClient);
	}

	private void actionLike() {

		String textOnLikeButton;
		int cmtCount = currentItem.getCommentCount();
		if (isLike) {
			isLike = false;
			likeCount -= 1;
			textOnLikeButton = "Like";

			if (StringUtils.isEmptyString(photoId)) {
				FacebookUtils.unlike(postId);
			} else {
				FacebookUtils.unlike(photoId);
				photoId = "";
			}

		} else {
			isLike = true;
			likeCount += 1;
			textOnLikeButton = "Unlike";
			if (StringUtils.isEmptyString(photoId)) {
				FacebookUtils.like(postId);
			} else {
				FacebookUtils.like(photoId);
				photoId = "";
			}
		}
		changeTextInHtml("postId:" + postId, textOnLikeButton);
		if (likeCount > 0) {
			if (cmtCount > 0) {
				changeTextInHtml(StringUtils.ID_IMG_4 + postId, StringUtils.IMG_LIKE);
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId,
						StringUtils.rountOffNumber(likeCount));
			} else {
				changeTextInHtml(StringUtils.ID_IMG_2 + postId, StringUtils.IMG_LIKE);
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId,
						StringUtils.rountOffNumber(likeCount));
			}
		} else {
			if (cmtCount > 0) {
				changeTextInHtml(StringUtils.ID_IMG_4 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId, "");
			} else {
				changeTextInHtml(StringUtils.ID_IMG_2 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId, "");
			}
		}
		currentItem.setLikeCount(likeCount);
		currentItem.setUserLiked(isLike);
	}

	final class MyJavaScriptInterface {
		public void functionComment(String contentForCommentAct) {
			Intent i = new Intent(getActivity(), CommentActivity.class);
			// i.putExtra("contentForCommentAct", contentForCommentAct);
			i.putExtra("postID", postId);
			startActivityForResult(i, Utils.CALL_COMMENT_ACTIVITY);
		}

		public void functionHide(String contentItem, String idTag) {
			hMapItemHided.put(idTag, contentItem);
			String postIdHide = idTag.substring(ArticleItem.ID_OF_ITEM.length());
			listPostIdHide.add(postIdHide);
			String showUndoButton = FileUtils.readFileFromAssets(FileUtils.ARTICLE_FOR_UNDO,
					getActivity());
			showUndoButton = showUndoButton.replace(ArticleItem.LINE_1_TEXT_UNDO,
					"This post will no longer appear in your");
			showUndoButton = showUndoButton.replace(ArticleItem.LINE_2_TEXT_UNDO, "News Feed.");
			showUndoButton = showUndoButton.replace(ArticleItem.LINK_UNDO,
					ArticleItem.LINK_UNDO_HIDE + idTag);
			changeTextInHtml(idTag, showUndoButton);
		}

		public void functionDelete(String contentItem, String idTag) {
			hMapItemDeleted.put(idTag, contentItem);
			String postIdDel = idTag.substring(ArticleItem.ID_OF_ITEM.length());
			listPostIdDel.add(postIdDel);
			String showUndoButton = FileUtils.readFileFromAssets(FileUtils.ARTICLE_FOR_UNDO,
					getActivity());
			showUndoButton = showUndoButton.replace(ArticleItem.LINE_1_TEXT_UNDO,
					"This post will be deleted");
			showUndoButton = showUndoButton.replace(ArticleItem.LINE_2_TEXT_UNDO, "");
			showUndoButton = showUndoButton.replace(ArticleItem.LINK_UNDO,
					ArticleItem.LINK_UNDO_DELETE + idTag);
			changeTextInHtml(idTag, showUndoButton);
		}

		public void functionLike(String likeOrUnlike) {
			currentItem = FacebookUtils.getCurrentItemFromListArticleItem(postId);
			likeCount = currentItem.getLikeCount();

			if (likeOrUnlike.equalsIgnoreCase("like")) {
				isLike = false;
			} else {
				isLike = true;
			}
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
			if (prefs.getBoolean("Besafe Tip", true) && !isLike) {
				Intent intent = new Intent(getActivity(), TipActivity.class);
				intent.putExtra("isTipOfFb", true);
				intent.putExtra("iseExecuteWhenTurnOff", true);
				intent.putExtra("action", "Like");
				startActivityForResult(intent, Utils.CALL_TIP_FOR_LIKE_POST);
			} else {
				actionLike();

			}
		}

	}

	@Override
	public void onGetSuccess(String requestCode) {
		if (requestCode.equals(Utils.LOGIN_FACEBOOK)) {
			doneLogin();
			new UpdateUserInfor(null).execute();
		} else {
			mPullRefreshWebView.onRefreshComplete();
			refreshLoading.setVisibility(View.GONE);
			if (FacebookUtils.isHasNewStory()) {
				imgNewStory.setVisibility(View.VISIBLE);
			} else {
				imgNewStory.setVisibility(View.GONE);
			}
		}
	}

	private void setLikeCountAndCmtCountWithIntent(Intent data) {
		String postId = data.getStringExtra("postId");
		boolean isLike = data.getBooleanExtra("isLike", false);
		int likeCount = data.getIntExtra("likeCount", 0);
		int cmtCount = data.getIntExtra("cmtCount", 0);
		if (isLike) {
			changeTextInHtml("postId:" + postId, "Unlike");
		} else {
			changeTextInHtml("postId:" + postId, "Like");
		}
		// khong set like count and cmt count cho current item vi da set o
		// comment activity
		if (likeCount > 0) {
			if (cmtCount > 0) {
				changeTextInHtml(StringUtils.ID_IMG_4 + postId, StringUtils.IMG_LIKE);
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId,
						StringUtils.rountOffNumber(likeCount));
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId,
						StringUtils.rountOffNumber(cmtCount));
				changeTextInHtml(StringUtils.ID_IMG_2 + postId, StringUtils.IMG_COMMENT);
			} else {
				changeTextInHtml(StringUtils.ID_IMG_2 + postId, StringUtils.IMG_LIKE);
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId,
						StringUtils.rountOffNumber(likeCount));
				changeTextInHtml(StringUtils.ID_IMG_4 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId, "");
			}
		} else {
			if (cmtCount > 0) {
				changeTextInHtml(StringUtils.ID_IMG_4 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId, "");
				changeTextInHtml(StringUtils.ID_IMG_2 + postId, StringUtils.IMG_COMMENT);
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId,
						StringUtils.rountOffNumber(cmtCount));
			} else {
				changeTextInHtml(StringUtils.ID_IMG_2 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId, "");
				changeTextInHtml(StringUtils.ID_IMG_4 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId, "");
			}
		}
	}

	boolean backFromOther = false;

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Utils.REQUEST_CODE_SHOW_ALBUM) {
			backFromOther = true;
			if (resultCode == Activity.RESULT_OK)
				setLikeCountAndCmtCountWithIntent(data);
		}
		if (requestCode == Utils.CALL_COMMENT_ACTIVITY) {
			if (resultCode == Activity.RESULT_OK)
				setLikeCountAndCmtCountWithIntent(data);
			backFromOther = true;
		}
		if (requestCode == Utils.REQUEST_CODE_POST_STATUS) {
			if (resultCode == Activity.RESULT_OK)
				FacebookUtils.getNewFeeds(getActivity(), webView, StringUtils.LIMIT,
						FragmentPost.this, true);
			backFromOther = true;
		}
		if (requestCode == Utils.REQUEST_CODE_POST_PHOTO) {
			if (resultCode == Activity.RESULT_OK)
				FacebookUtils.getNewFeeds(getActivity(), webView, StringUtils.LIMIT,
						FragmentPost.this, true);
			backFromOther = true;
		}
		if (requestCode == Utils.CALL_TIP_FOR_LIKE_POST) {
			if (resultCode == Activity.RESULT_OK)
				actionLike();
			backFromOther = true;
		}
		if (requestCode == Utils.AFTER_SHARE_FACEBOOK) {
			if (resultCode == Activity.RESULT_OK)
				FacebookUtils.getNewFeeds(getActivity(), webView, StringUtils.LIMIT,
						FragmentPost.this, true);
			backFromOther = true;
		}
		if (requestCode == START_FOR_NOTHING) {
			backFromOther = true;
		}
	}

	public void doOnClick() {
	}

	public void doTopToBottom() {
		toolbar.setVisibility(View.VISIBLE);
		((MainActivity) getActivity()).hideTaskBar();
		mPullRefreshWebView.onRefreshComplete();
	}

	public void doBottomToTop() {
		toolbar.setVisibility(View.GONE);
		((MainActivity) getActivity()).showTaskBar();
		mPullRefreshWebView.onRefreshComplete();
	}

	public void changeTextInHtml(String idTag, String text) {
		String changeText = "javascript:changeTextInTag('" + idTag + "','" + text + "');";
		webView.loadUrl(changeText);
	}

	@Override
	public void onClick(View v) {
		if (v == imgStatus) {
			Intent i = new Intent(getActivity(), StatusActivity.class);
			startActivityForResult(i, Utils.REQUEST_CODE_POST_STATUS);
		}
		if (v == imgPhoto) {
			Intent i = new Intent(getActivity(), AdjustPhotoActivity.class);
			i.putExtra("firstCall", true);
			startActivityForResult(i, Utils.REQUEST_CODE_POST_PHOTO);
		}
		if (v == imgCheckin) {
			Intent i = new Intent(getActivity(), CheckInActivity.class);
			startActivityForResult(i, Utils.REQUEST_CODE_POST_STATUS);
		}
		if (v == imgNewStory) {
			FacebookUtils.getNewFeeds(getActivity(), webView, StringUtils.LIMIT, FragmentPost.this,
					true);
			imgNewStory.setVisibility(View.GONE);
			refreshLoading.setVisibility(View.VISIBLE);
		}
	}

	private void customDialogHide(final String postIdHideOrDelete, final boolean canDel) {
		final Dialog hidePostDialog = new Dialog(getActivity(),
				android.R.style.Theme_Translucent_NoTitleBar);
		hidePostDialog.setContentView(R.layout.hide_post_dialog);
		Button btnDel = (Button) hidePostDialog.findViewById(R.id.btn_del);
		Button btnHide = (Button) hidePostDialog.findViewById(R.id.btn_hide);
		Button btnCancel = (Button) hidePostDialog.findViewById(R.id.btn_cancel);
		LayoutParams lp = hidePostDialog.getWindow().getAttributes();
		lp.width = android.view.ViewGroup.LayoutParams.MATCH_PARENT;
		lp.height = android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
		if (canDel) {
			btnDel.setVisibility(View.VISIBLE);
		} else {
			btnDel.setVisibility(View.GONE);
		}

		lp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
		hidePostDialog.getWindow().setAttributes(lp);
		hidePostDialog.show();

		btnDel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hidePostDialog.dismiss();
				String getText = "javascript:getContentForDelete('" + ArticleItem.ID_OF_ITEM
						+ postIdHideOrDelete + "');";
				webView.loadUrl(getText);
			}
		});
		btnHide.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hidePostDialog.dismiss();
				String getText = "javascript:getContentForHide('" + ArticleItem.ID_OF_ITEM
						+ postIdHideOrDelete + "');";
				webView.loadUrl(getText);
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				hidePostDialog.dismiss();

			}
		});
	}

	@Override
	public void onGetFailure(String requestCode) {
		if (requestCode.equals(Utils.LOGIN_FACEBOOK)) {
			DialogUtils.dismissDialog();
			Toast.makeText(this.getActivity(), "Please check network", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	private void loginFacebook() {
		Session session = Session.getActiveSession();
		if (!session.isOpened() && !session.isClosed()) {
			session.openForRead(new Session.OpenRequest(this).setPermissions(FB_PERMISSIONS_READ)
					.setCallback(statusCallback).setRequestCode(100));
		} else {
			Session.openActiveSession(getActivity(), this, true, statusCallback);
		}

	}

	private Session.StatusCallback statusCallback = new SessionStatusCallback();

	private class SessionStatusCallback implements Session.StatusCallback {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	}

	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (state.isOpened()) {
			Log.i(Log.TAG, "Logged in...");
			updateView();
		} else if (state.isClosed()) {
			Log.i(Log.TAG, "Logged out...");
		} else {
			Log.i(Log.TAG, state.name());
		}
		if (exception != null)
			exception.printStackTrace();
	}

	private UiLifecycleHelper uiHelper;

	private void doneLogin() {
		DialogUtils.dismissDialog();
		webView.loadUrl("file:///android_asset/initWebview.html");
		refreshLoading.setVisibility(View.VISIBLE);

	}

	private void updateView() {
		DialogUtils.showProgressDialog(this.getActivity(), "", "Loading...", false);
		FacebookUtils.getUser(this);
	}

	@Override
	public void onResume() {
		super.onResume();

		if (!backFromOther) {
			// For scenarios where the main activity is launched and user
			// session is not null, the session state change notification
			// may not be triggered. Trigger it if it's open/closed.
			Session session = Session.getActiveSession();
			if (session != null && (session.isOpened() || session.isClosed())) {
				onSessionStateChange(session, session.getState(), null);
			}
		} else {
			backFromOther = false;
		}
		uiHelper.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		uiHelper = new UiLifecycleHelper(getActivity(), statusCallback);
		uiHelper.onCreate(savedInstanceState);
	}
}
