/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.besafepost.fragment;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase.DisplayType;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.besafepost.activity.R;
import com.example.besafepost.activity.ShowAlbumActivity;
import com.example.besafepost.model.PhotoDetails;

@SuppressLint("ValidFragment")
public class ScreenSlidePageFragment extends Fragment {

	private PhotoDetails photoDetails;
	private Context context;

	public ScreenSlidePageFragment(Context context, PhotoDetails photoDetails) {
		this.photoDetails = photoDetails;
		this.context = context;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout containing a title and body text.
		ViewGroup rootView = (ViewGroup) inflater.inflate(
				R.layout.fragment_screen_slide_page, container, false);
		ImageViewTouch imgAlbum = (ImageViewTouch) rootView
				.findViewById(R.id.imgAlbum);
		imgAlbum.setDisplayType(DisplayType.FIT_IF_BIGGER);
		new myAssynTask(imgAlbum).execute(photoDetails.getSrc());
		return rootView;
	}

	private class myAssynTask extends AsyncTask<String, Void, Drawable> {

		ImageViewTouch img;

		public myAssynTask(ImageViewTouch img) {
			this.img = img;
		}

		@Override
		protected Drawable doInBackground(String... urls) {
			Drawable draw = null;
			String src = urls[0];
			InputStream is;
			URL url;
			try {
				url = new URL(src);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setConnectTimeout(5000);
				conn.setInstanceFollowRedirects(false);
				conn.setDoInput(true);
				conn.connect();
				is = conn.getInputStream();
				draw = Drawable.createFromStream(is, "");
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return draw;
		}

		@Override
		protected void onPostExecute(Drawable result) {
			img.setImageDrawable(result);
			((ShowAlbumActivity) context).setDrawCurrent(result);
			super.onPostExecute(result);
		}
	}

}
