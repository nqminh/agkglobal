package com.example.besafepost.fragment;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.besafepost.activity.R;
import com.example.besafepost.adapter.ListTemplateSMSAdapter;
import com.example.besafepost.interfaces.IDelegateLoadListContactsTask;
import com.example.besafepost.model.Contact;
import com.example.besafepost.task.LoadListContactsTask;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.StringUtils;
import com.example.besafepost.utils.Utils;

@SuppressLint("DefaultLocale")
public class FragmentText extends BaseFragment implements
		IDelegateLoadListContactsTask {

	private LinearLayout fragment1, fragment2;
	private ListView lvTemplateSMS, lvContacts;
	private EditText inputSearch;
	private Button btnAdd, btnBack;
	private TextView tvTitle;
	private SharedPreferences pref;
	private String[] templateSMS = { "Away from my desk.", "Be home soon.",
			"Be right back.", "Call you in a minute", "Can't talk now",
			"I love you", "In a meeting", "Just a minute", "No", "OK", "OMG",
			"Thank you", "Yes" };
	private List<String> listSMS = new ArrayList<String>();
	private List<Contact> listContact = new ArrayList<Contact>();
	private List<Contact> listContactDisplay = new ArrayList<Contact>();
	private List<Contact> listContactFull = new ArrayList<Contact>();
	private List<String> listNumber = new ArrayList<String>();
	private List<String> listNameContactFull = new ArrayList<String>();
	private List<String> listNameContactDisplay = new ArrayList<String>();
	private ListTemplateSMSAdapter adapter = null;
	private ArrayAdapter<String> adapter1 = null;
	private OnClickListener onClick_AddButton = null;
	private OnClickListener onClick_BackButton = null;
	private Dialog dialogAdd;
	private ProgressDialog progressDialog;
	private Dialog dialogNumberPhone;
	private LoadListContactsTask task;
	private String numberPhone, bodyMessage;
	private String search = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		pref = PreferenceManager.getDefaultSharedPreferences(getActivity());

		if (StringUtils.isEmptyString(pref.getString(Utils.keyPrefSMS, ""))) {
			JSONObject jsonObject = new JSONObject();
			for (int i = 0; i < templateSMS.length; i++) {
				try {
					jsonObject.put(i + "", templateSMS[i]);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			SharedPreferences.Editor editor = pref.edit();
			editor.putString(Utils.keyPrefSMS, jsonObject.toString());
			editor.commit();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_text, null);

		fragment1 = (LinearLayout) view.findViewById(R.id.message_history);
		fragment2 = (LinearLayout) view.findViewById(R.id.mesage_list);
		lvTemplateSMS = (ListView) view.findViewById(R.id.listTemplateSMS);
		lvContacts = (ListView) view.findViewById(R.id.listContacts);
		inputSearch = (EditText) view.findViewById(R.id.inputSearch);
		tvTitle = (TextView) view.findViewById(R.id.tvTitle);
		btnAdd = (Button) view.findViewById(R.id.btnAdd);
		btnBack = (Button) view.findViewById(R.id.btnBack);

		hideButton(false, true);

		listSMS = Utils.getListStringFromPreference(Utils.keyPrefSMS);
		adapter = new ListTemplateSMSAdapter(getActivity(),
				R.layout.templatesms_item, listSMS);
		lvTemplateSMS.setAdapter(adapter);

		lvTemplateSMS.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				task = new LoadListContactsTask(FragmentText.this,
						getActivity());
				task.execute();
				bodyMessage = listSMS.get(arg2);
			}
		});

		lvContacts.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				dialogNumberPhone = new Dialog(getActivity());
				dialogNumberPhone
						.setContentView(R.layout.list_phonenumber_dialog);
				dialogNumberPhone.setTitle(listContactDisplay.get(arg2)
						.getNameContact());

				ListView listPhoneNumber = (ListView) dialogNumberPhone

				.findViewById(R.id.listPhoneNumber);
				listNumber = listContactDisplay.get(arg2).getPhoneNumber();
				listPhoneNumber.setAdapter(new ArrayAdapter<String>(
						getActivity(), android.R.layout.simple_list_item_1,
						listNumber));

				listPhoneNumber
						.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {

								numberPhone = listNumber.get(arg2);

								Intent sendIntent = new Intent(
										Intent.ACTION_VIEW);
								sendIntent.putExtra("sms_body", bodyMessage);
								sendIntent.putExtra("address", numberPhone);
								sendIntent.setType("vnd.android-dir/mms-sms");
								startActivity(sendIntent);
								dialogNumberPhone.dismiss();
							}
						});

				Button btnCancel = (Button) dialogNumberPhone
						.findViewById(R.id.btnCancel);

				// if button is clicked, close the custom dialog
				btnCancel.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialogNumberPhone.dismiss();
					}
				});

				dialogNumberPhone.show();

			}
		});

		inputSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				listNameContactDisplay.clear();
				listContactDisplay.clear();
				search = s.toString().toLowerCase();
				for (int i = 0; i < listNameContactFull.size(); i++) {
					if (listNameContactFull.get(i) != null
							&& listNameContactFull.get(i).toLowerCase()
									.contains(search)) {
						listNameContactDisplay.add(listNameContactFull.get(i));
						listContactDisplay.add(listContactFull.get(i));
					}
				}
				adapter1.notifyDataSetChanged();

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable arg0) {

			}
		});

		onClick_AddButton = new OnClickListener() {

			@Override
			public void onClick(View v) {

				dialogAdd = new Dialog(getActivity());
				dialogAdd.setContentView(R.layout.add_template_sms_dialog);
				dialogAdd.setCancelable(true);
				dialogAdd.setTitle("Add Template SMS");

				final EditText edtSMS = (EditText) dialogAdd

				.findViewById(R.id.edtSMS);
				Button btnOK = (Button) dialogAdd.findViewById(R.id.btnOK);
				Button btnCancel = (Button) dialogAdd
						.findViewById(R.id.btnCancel);

				btnOK.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (StringUtils.isEmptyString(edtSMS.getText()
								.toString())) {
							Toast.makeText(getActivity(),
									"Template SMS is Empty", Toast.LENGTH_SHORT)
									.show();
						} else if (Utils.isBlockedAddress(edtSMS.getText()
								.toString())
								|| Utils.isBlockedWord(edtSMS.getText()
										.toString())) {
							DialogUtils.showDialogBannedWord(getActivity(),
									Utils.isBlockedWord(edtSMS.getText()
											.toString()), Utils
											.isBlockedAddress(edtSMS.getText()
													.toString()));

						} else {
							Utils.putStringIntoListStringPreference(
									Utils.keyPrefSMS, edtSMS.getText()
											.toString());
							listSMS = Utils
									.getListStringFromPreference(Utils.keyPrefSMS);
							refreshList();
							dialogAdd.dismiss();
						}
					}
				});

				btnCancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialogAdd.dismiss();
					}
				});

				dialogAdd.show();

			}
		};

		onClick_BackButton = new OnClickListener() {

			@Override
			public void onClick(View v) {
				fragment1.setVisibility(View.VISIBLE);
				fragment2.setVisibility(View.GONE);
				hideButton(false, true);
				Utils.hideKeyboard(getActivity());
				// ((MainActivity) getActivity()).hideButton(false, true);
				// ((MainActivity) getActivity()).setIsListContact(false);
			}
		};

		btnAdd.setOnClickListener(onClick_AddButton);
		btnBack.setOnClickListener(onClick_BackButton);

		return view;
	}

	private void refreshList() {
		adapter = new ListTemplateSMSAdapter(getActivity(),
				R.layout.templatesms_item, listSMS);
		lvTemplateSMS.setAdapter(adapter);
	}

	public void hideButton(boolean add, boolean back) {
		if (add) {
			tvTitle.setText("Contacts");
			btnAdd.setVisibility(View.GONE);
		} else {
			tvTitle.setText("Besafe Text");
			btnAdd.setVisibility(View.VISIBLE);
		}
		if (back) {
			tvTitle.setText("Besafe Text");
			btnBack.setVisibility(View.GONE);
		} else {
			tvTitle.setText("Contacts");
			btnBack.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onGetStart(LoadListContactsTask task) {
		// TODO Auto-generated method stub
		progressDialog = new ProgressDialog(getActivity());
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.setMessage("Loading! Please wait...!");
		progressDialog.show();
	}

	@Override
	public void onGetProgress(LoadListContactsTask task) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGetSuccess(LoadListContactsTask task) {

		progressDialog.dismiss();
		listContact = task.getNameContact();
		fragment1.setVisibility(View.GONE);
		fragment2.setVisibility(View.VISIBLE);
		listNameContactFull.clear();
		listNameContactDisplay.clear();
		listContactDisplay.clear();
		listContactFull.clear();
		hideButton(true, false);
		// ((MainActivity) getActivity()).hideButton(true, false);
		// ((MainActivity) getActivity()).setIsListContact(true);

		for (int i = 0; i < listContact.size(); i++) {

			if (!StringUtils.isEmptyString(listContact.get(i).getNameContact())) {
				listNameContactDisplay.add(listContact.get(i).getNameContact());
				listNameContactFull.add(listContact.get(i).getNameContact());
				listContactDisplay.add(listContact.get(i));
				listContactFull.add(listContact.get(i));
			}

		}
		adapter1 = new ArrayAdapter<String>(getActivity(),
				R.layout.contact_item, R.id.numberPhone, listNameContactDisplay);
		lvContacts.setAdapter(adapter1);

	}

	@Override
	public void onGetFailure(LoadListContactsTask task) {
		// TODO Auto-generated method stub

	}

}
