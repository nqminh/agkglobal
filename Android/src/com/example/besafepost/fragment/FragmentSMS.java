package com.example.besafepost.fragment;

import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.besafepost.activity.MainActivity;
import com.example.besafepost.activity.R;
import com.example.besafepost.utils.StringUtils;

public class FragmentSMS extends BaseFragment {

	private EditText edtPhoneNumber, edtMessage;
	private Button btnSend;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_send_sms, null);

		edtMessage = (EditText) view.findViewById(R.id.edtMessage);
		edtPhoneNumber = (EditText) view.findViewById(R.id.edtPhoneNumber);
		btnSend = (Button) view.findViewById(R.id.btnSend);

		edtPhoneNumber.setText(MainActivity.fragmentCurrent.getArguments()
				.getString("Phone Number", ""));
		edtMessage.setText(MainActivity.fragmentCurrent.getArguments()
				.getString("templateSMS", ""));

		btnSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (StringUtils.isEmptyString(edtPhoneNumber.getText()
						.toString())
						|| StringUtils.isEmptyString(edtMessage.getText()
								.toString())) {
					Toast.makeText(getActivity(),
							"Phone Number or Message is empty !",
							Toast.LENGTH_SHORT).show();
				} else {
					sendSMS(edtPhoneNumber.getText().toString(), edtMessage
							.getText().toString());
				}
			}
		});

		return view;
	}

	private void sendSMS(String phoneNumber, String message) {
		try {
			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(phoneNumber, null, message, null, null);
			Toast.makeText(getActivity(), "SMS Sent!", Toast.LENGTH_LONG)
					.show();
		} catch (Exception e) {
			Toast.makeText(getActivity(), "SMS faild, please try again later!",
					Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}

	}

}
