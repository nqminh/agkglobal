package com.example.besafepost.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.besafepost.activity.MainActivity;
import com.example.besafepost.activity.R;
import com.example.besafepost.adapter.ListContactAdapter;
import com.example.besafepost.adapter.ListSmsSubjectAdapter;
import com.example.besafepost.adapter.MessageDetailAdapter;
import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.interfaces.IDelegateLoadListContactsTask;
import com.example.besafepost.interfaces.IReiceiverCallback;
import com.example.besafepost.interfaces.ISwipeDetector;
import com.example.besafepost.model.Contact;
import com.example.besafepost.model.Sms;
import com.example.besafepost.receiver.SmsReceiver;
import com.example.besafepost.task.LoadListContactsTask;
import com.example.besafepost.task.LoadMessageTask;
import com.example.besafepost.utils.ActivitySwipeDetector;
import com.example.besafepost.utils.Log;

public class FragmentMesageHistory extends BaseFragment implements IActivityDelegate,
		IDelegateLoadListContactsTask, IReiceiverCallback, ISwipeDetector {
	private static final String SENT = "SMS_SENT";
	private static final String DELIVERED = "SMS_DELIVERED";
	LoadMessageTask loadMessageTask;
	ListView listSubject, listSmsDetail;
	List<Sms> lstSms, lstSmsSubject;
	ListSmsSubjectAdapter adapter;
	Button backButton, btnAdd, sendButton;
	LinearLayout fragment1;
	RelativeLayout fragment2;
	AutoCompleteTextView contactAutoComplete;
	EditText messageText;
	Sms currentDetail = null;
	private ProgressDialog progressDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_message_history, null);

		btnAdd = (Button) view.findViewById(R.id.btnAdd);
		btnAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showConversation(null);
			}
		});

		listSubject = (ListView) view.findViewById(R.id.listSMSSubject);
		registerForContextMenu(listSubject);
		listSubject.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Sms sms = lstSmsSubject.get(arg2);
				showConversation(sms);
			}
		});

		messageText = (EditText) view.findViewById(R.id.inputMessage);
		sendButton = (Button) view.findViewById(R.id.send);
		sendButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String phoneNo = (currentDetail != null) ? currentDetail.getAddress()
						: contactAutoComplete.getText().toString();
				String sms = messageText.getText().toString();
				messageText.setText("");
				if (TextUtils.isEmpty(phoneNo)) {
					Toast.makeText(getActivity(), "please give me an address?", Toast.LENGTH_LONG)
							.show();
				} else if (TextUtils.isEmpty(sms)) {
					Toast.makeText(getActivity(), "please enter sms.", Toast.LENGTH_LONG).show();
				} else {
					sendSMS(phoneNo, sms);
				}
			}
		});

		listSmsDetail = (ListView) view.findViewById(R.id.listSmsDetail);
		registerForContextMenu(listSmsDetail);
		backButton = (Button) view.findViewById(R.id.btnBack);
		fragment1 = (LinearLayout) view.findViewById(R.id.message_history);
		fragment2 = (RelativeLayout) view.findViewById(R.id.mesage_list);
		contactAutoComplete = (AutoCompleteTextView) view.findViewById(R.id.contact);

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showDetail(false);
				currentDetail = null;
				currentContact = null;
				contactAutoComplete.setText("");
			}
		});
		ActivitySwipeDetector swipeDetector = new ActivitySwipeDetector(this);
		listSubject.setOnTouchListener(swipeDetector);
		
		new LoadListContactsTask(this, getActivity()).execute();
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		SmsReceiver.callback = this;
		loadMoreMessage();
	}

	@Override
	public void onPause() {
		SmsReceiver.callback = null;
		super.onPause();
	}

	private void loadMoreMessage() {
		loadMessageTask = new LoadMessageTask(getActivity(), FragmentMesageHistory.this);
		loadMessageTask.execute();
	};

	@Override
	public void onGetSuccess(String requestCode) {
		lstSms = loadMessageTask.getListSMS();
		if (lstSms.size() > 0) fragment1.setBackgroundColor(getResources().getColor(R.color.bck_msg));
		else fragment1.setBackgroundResource(R.drawable.sms_history_bck);
		lstSmsSubject = getSubject(lstSms);
		adapter = new ListSmsSubjectAdapter(getActivity(), R.layout.sms_subject, lstSmsSubject);
		listSubject.setAdapter(adapter);
		if (currentDetail != null) {
			showConversation(currentDetail);
		} else if (!TextUtils.isEmpty(contactAutoComplete.getText().toString())) {
			updateConversation();
		} else {

		}
	}

	private void updateConversation() {
		String current = contactAutoComplete.getText().toString();
		for (Sms sms : lstSms) {
			String addr = sms.getAddress();
			if (addr != null && addr.contains(current)) {
				currentDetail = sms;
				showConversation(currentDetail);
				break;
			}
		}
	}

	@Override
	public void onGetFailure(String requestCode) {

	}

	public List<Sms> getSubject(List<Sms> listSms) {
		List<Sms> myList = new ArrayList<Sms>();

		for (int i = 0; i < listSms.size(); i++) {
			Sms sms = listSms.get(i);
			boolean check = false;
			for (int j = 0; j < myList.size(); j++) {
				if (myList.get(j).get_threadId() == sms.get_threadId()) {
					check = true;
					break;
				}
			}
			if (!check) {
				myList.add(sms);
			}
		}

		return myList;
	}

	List<Sms> myList;
	MessageDetailAdapter detailAdapter;

	private void showConversation(Sms sms) {
		myList = new ArrayList<Sms>();
		if (sms != null) {
			currentDetail = sms;
			for (int i = 0; i < lstSms.size(); i++) {
				Sms sms2 = lstSms.get(i);
				if (sms2.get_threadId() == sms.get_threadId()) {
					myList.add(0, sms2);
				}
			}
		}
		if (myList.size() > 0) fragment2.setBackgroundColor(getResources().getColor(R.color.bck_msg));
		else fragment2.setBackgroundResource(R.drawable.sms_history_bck);
		detailAdapter = new MessageDetailAdapter(getActivity(), R.layout.sms_detail, myList);
		listSmsDetail.setAdapter(detailAdapter);
		listSmsDetail.post(new Runnable() {

			@Override
			public void run() {
				listSmsDetail.setSelection(detailAdapter.getCount() - 1);
			}
		});
		new LoadListContactsTask(this, getActivity()).execute();
		showDetail(true);
	}

	private void showDetail(boolean show) {
		if (show) {
			backButton.setVisibility(View.VISIBLE);
			fragment2.setVisibility(View.VISIBLE);
			fragment1.setVisibility(View.GONE);
			btnAdd.setVisibility(View.GONE);
			((MainActivity)getActivity()).hideTaskBar();
		} else {
			backButton.setVisibility(View.GONE);
			fragment2.setVisibility(View.GONE);
			fragment1.setVisibility(View.VISIBLE);
			btnAdd.setVisibility(View.VISIBLE);
			((MainActivity)getActivity()).showTaskBar();
		}
	}

	private List<Contact> listContact = new ArrayList<Contact>();
	ListContactAdapter contactAdapter;
	Contact currentContact = null;

	@Override
	public void onGetStart(LoadListContactsTask task) {
		progressDialog = new ProgressDialog(getActivity());
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.setMessage("Loading! Please wait...!");
		progressDialog.show();
	}

	@Override
	public void onGetProgress(LoadListContactsTask task) {

	}

	@Override
	public void onGetSuccess(LoadListContactsTask task) {
		progressDialog.dismiss();
		listContact = task.getNameContact();
		if (currentDetail == null) {
			contactAdapter = new ListContactAdapter(getActivity(), R.layout.contact_item,
					listContact);
			contactAutoComplete.setAdapter(contactAdapter);
		} else {
			contactAutoComplete.setAdapter(null);
			setContactTitle(currentDetail);
		}
		Log.d("besafe", "load contact done : " + listContact.size());
	}

	@Override
	public void onGetFailure(LoadListContactsTask task) {

	}

	private void setContactTitle(Sms sms) {
		boolean check = false;
		String smsAddress = sms.getAddress();
		if (!TextUtils.isEmpty(smsAddress)) {
			smsAddress.replace("+84", "");
			smsAddress.replace(" ", "");
			for (int i = 0; i < listContact.size(); i++) {
				Contact contact = listContact.get(i);
				check = false;
				if (contact.getPhoneNumber() != null)
					for (String address : contact.getPhoneNumber()) {
						if (sms.getAddress() != null && sms.getAddress().contains(address)) {
							contactAutoComplete.setText(contact.toString());
							currentContact = contact;
							check = true;
							break;
						}
					}
				if (check)
					break;
			}
			if (!check) {
				contactAutoComplete.setText(sms.getAddress());
			}
		}
	}

	private void sendSMS(final String phoneNumber, final String smsBody) {
		try {
			int id = smsBody.length() + (new Random().nextInt(1000000));

			PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(SENT
					+ id), 0);
			PendingIntent deliveredPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(
					DELIVERED + id), 0);
			BroadcastReceiver sendReceiver = new BroadcastReceiver() {
				@Override
				public void onReceive(Context arg0, Intent arg1) {
					switch (getResultCode()) {
					case Activity.RESULT_OK:

						ContentValues values = new ContentValues();
						values.put("address", phoneNumber);
						values.put("body", smsBody);
						getActivity().getContentResolver().insert(Uri.parse("content://sms/sent"),
								values);
						onReceiveMessage();

						Toast.makeText(getActivity().getBaseContext(), "SMS sent",
								Toast.LENGTH_SHORT).show();
						break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
						Toast.makeText(getActivity().getBaseContext(), "Generic failure",
								Toast.LENGTH_SHORT).show();
						break;
					case SmsManager.RESULT_ERROR_NO_SERVICE:
						Toast.makeText(getActivity().getBaseContext(), "No service",
								Toast.LENGTH_SHORT).show();
						break;
					case SmsManager.RESULT_ERROR_NULL_PDU:
						Toast.makeText(getActivity().getBaseContext(), "Null PDU",
								Toast.LENGTH_SHORT).show();
						break;
					case SmsManager.RESULT_ERROR_RADIO_OFF:
						Toast.makeText(getActivity().getBaseContext(), "Radio off",
								Toast.LENGTH_SHORT).show();
						break;
					}
					getActivity().unregisterReceiver(this);
				}
			};
			BroadcastReceiver deliverReceiver = new BroadcastReceiver() {
				@Override
				public void onReceive(Context arg0, Intent arg1) {
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						Toast.makeText(getActivity().getBaseContext(), "SMS delivered",
								Toast.LENGTH_SHORT).show();
						break;
					case Activity.RESULT_CANCELED:
						Toast.makeText(getActivity().getBaseContext(), "SMS not delivered",
								Toast.LENGTH_SHORT).show();
						break;
					}
					getActivity().unregisterReceiver(this);
				}
			};
			// ---when the SMS has been sent---
			getActivity().registerReceiver(sendReceiver, new IntentFilter(SENT + id));

			// ---when the SMS has been delivered---
			getActivity().registerReceiver(deliverReceiver, new IntentFilter(DELIVERED + id));

			SmsManager smsManager = SmsManager.getDefault();
			smsManager.sendTextMessage(phoneNumber, null, smsBody, sentPI, deliveredPI);

		} catch (Exception e) {
			Toast.makeText(getActivity(), "SMS faild, please try again later!", Toast.LENGTH_LONG)
					.show();
			e.printStackTrace();
		}

	}

	@Override
	public void onReceiveMessage() {
		h.postDelayed(r1, 2000);
	}

	final Handler h = new Handler();

	Runnable r1 = new Runnable() {
		@Override
		public void run() {

			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					loadMoreMessage();
				}
			});
		}
	};

	@Override
	public void onCreateContextMenu(android.view.ContextMenu menu, View v,
			android.view.ContextMenu.ContextMenuInfo menuInfo) {
		MenuInflater inflater;
		switch (v.getId()) {
		case R.id.listSmsDetail:
			 inflater = getActivity().getMenuInflater();
	          inflater.inflate(R.menu.menu_list, menu);
			break;
		case R.id.listSMSSubject:
			 inflater = getActivity().getMenuInflater();
	          inflater.inflate(R.menu.menu_list_2, menu);
			break;
		}
	}
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	      switch(item.getItemId()) {
	          case R.id.delete:
	        // remove stuff here
	        	  Sms sms = myList.get(info.position);
	        	  Uri thread = Uri.parse( "content://sms");
	        	  int deleted = getActivity().getContentResolver().delete( thread, "thread_id=? and _id=?", 
	        			  new String[]{String.valueOf(sms.get_threadId()), String.valueOf(sms.getId())} );
	        	  Log.i(Log.TAG, "delete message : " + deleted);
	        	  loadMoreMessage();
	                return true;
	          case R.id.delete_list_sms:
	        	  Sms sms2 = lstSmsSubject.get(info.position);
	        	  Uri thread2 = Uri.parse( "content://sms");
	        	  int deleted2 = getActivity().getContentResolver().delete( thread2, "thread_id=?", 
	        			  new String[]{String.valueOf(sms2.get_threadId())} );
	        	  Log.i(Log.TAG, "delete message : " + deleted2);
	        	  loadMoreMessage();
	                return true;
	          default:
	                return super.onContextItemSelected(item);
	      }
	}

	@Override
	public void doOnClick() {
		
	}

	@Override
	public void doTopToBottom() {
		((MainActivity) getActivity()).hideTaskBar();
	}

	@Override
	public void doBottomToTop() {
		((MainActivity) getActivity()).showTaskBar();
	}
}
