package com.example.besafepost.fragment;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.besafepost.activity.CountDownActivity;
import com.example.besafepost.activity.DirectMessageActivity;
import com.example.besafepost.activity.MainActivity;
import com.example.besafepost.activity.NewTweetActivity;
import com.example.besafepost.activity.OpenLinkActivity;
import com.example.besafepost.activity.PlayVideoYoutubeActivity;
import com.example.besafepost.activity.R;
import com.example.besafepost.activity.TipActivity;
import com.example.besafepost.activity.TwitterLogin;
import com.example.besafepost.interfaces.IDeSkipBanned;
import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.interfaces.ISwipeDetector;
import com.example.besafepost.singleton.LoginObject;
import com.example.besafepost.singleton.ManagerTwitterApp;
import com.example.besafepost.task.UpdateUserInfor;
import com.example.besafepost.utils.ActivitySwipeDetector;
import com.example.besafepost.utils.ConnectionDetector;
import com.example.besafepost.utils.Const;
import com.example.besafepost.utils.CountDownTimerWithPause;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.StringUtils;
import com.example.besafepost.utils.TwitterUtils;
import com.example.besafepost.utils.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshWebView;

@SuppressLint("NewApi")
public class FragmentTwitter extends BaseFragment implements IActivityDelegate,
		OnClickListener, OnRefreshListener2<WebView>, IDeSkipBanned, ISwipeDetector {
	/**
	 * Register your here app https://dev.twitter.com/apps/new and get your
	 * consumer key and secret
	 * */
	// All view
	private TextView tvMyScreenName;
	private ImageView ivDirectMsg;
	private ImageView ivNewTweet;

	// Twitter
	public static Twitter twitter;
	private static RequestToken requestToken;
	private String verifier;

	// Shared Preferences
	public static SharedPreferences mSharedPreferences;

	// Internet Connection detector
	private ConnectionDetector cd;
	private PullToRefreshWebView pullToRefreshWebView;

	// List of twitter timeline
	private String myScreenName = "";
	private WebView webViewTwitter;
	private MyWebViewClient webViewClient;
	private int LIMIT_TWITTER;
	private View refreshHeader;
	public static String GET_FEED_TWITTER = "get feed twitter";
	boolean isClickLink = false;
	private Button btnBackDetails;
	private WebView webViewDetailsTweet;
	private Button btnReply;
	public static EditText edtReply;
	private SharedPreferences delayPrefs;
	private Long idTweet;
	private String screenName;
	private String avatarSrc;
	private String msgContent;

	private Button btnCallRetweet;
	private Button btnCallQuoteTweet;
	private Button btnCancel;
	private LinearLayout lnDialogTip;

	private SharedPreferences prefs;
	private CountDownTimerWithPause waitTimer;
	private MediaPlayer mp;
	private Boolean isNotFavoriting = true;
	//because data is wrong,isFavorite alway true,when call show tweet details isFavorite is true,but when destroy favorite,have to set isFavorite = false
	private boolean isFavorite;
	private boolean isShowHome = true;

	private Status status;
	private Status retweetStatus;

	private boolean isPause = true;
	private int seek;
	private static final int STEP_HOME = 1, STEP_DETAILS = 2;
	private View layoutHome, layoutDetails, homeTwitterBar;
	private boolean isloadedwebViewDetailsTweet = false;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_twitter, null);
		// find by id all view
		layoutHome = view.findViewById(R.id.home_timeline);
		layoutDetails = view.findViewById(R.id.details_tweet);
		refreshHeader = view.findViewById(R.id.refresh_header);
		homeTwitterBar = view.findViewById(R.id.home_twitter_bar);
		pullToRefreshWebView = (PullToRefreshWebView) view
				.findViewById(R.id.pull_refresh_webview_twitter);
		tvMyScreenName = (TextView) view.findViewById(R.id.tvMyScreenName);
		ivDirectMsg = (ImageView) view.findViewById(R.id.ivDirectMsg);
		ivNewTweet = (ImageView) view.findViewById(R.id.ivNewTweet);
		pullToRefreshWebView.setOnRefreshListener(this);
		webViewTwitter = pullToRefreshWebView.getRefreshableView();
		ActivitySwipeDetector swipeDetector = new ActivitySwipeDetector(this);
		pullToRefreshWebView.setOnTouchListener(swipeDetector);
		webViewTwitter.setOnTouchListener(swipeDetector);
		// add webview clien and java sccript interface
		webViewClient = new MyWebViewClient();
		webViewTwitter.setWebViewClient(webViewClient);
		webViewTwitter.getSettings().setJavaScriptEnabled(true);
		webViewTwitter.setHorizontalScrollbarOverlay(false);
		webViewTwitter.addJavascriptInterface(new MyJavaScriptInterface(),
				"Android");
		webViewTwitter.loadUrl("file:///android_asset/initWebviewTwitter.html");
		

		webViewDetailsTweet = (WebView) view
				.findViewById(R.id.webViewDetailsTweet);
		webViewDetailsTweet.getSettings().setJavaScriptEnabled(true);
		webViewDetailsTweet
				.setWebViewClient(new MyWebViewClientForDetailsWebview());
		//		webViewDetailsTweet.setWebChromeClient(new WebChromeClient());
		webViewDetailsTweet.addJavascriptInterface(
				new MyJavaScriptInterfaceForDetailsTweet(), "Android");
		webViewDetailsTweet
				.loadUrl("file:///android_asset/initWebviewDetailsTweet.html");
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		// set limmit default when get feed twiter
		LIMIT_TWITTER = StringUtils.LIMIT_TWITTER_DEFAULT;
		// Check Internet Connection
		cd = new ConnectionDetector(getActivity());
		if (!cd.isConnectingToInternet()) {
			DialogUtils.showAlertDialog(getActivity(),
					"Internet Connection Error",
					"Please connect to working Internet connection", false);
		}

		// Check if twitter keys are setchoose_button.png
		if (Const.CONSUMER_KEY.trim().length() == 0
				|| Const.CONSUMER_SECRET.trim().length() == 0) {
			// Internet Connection is not present
			DialogUtils.showAlertDialog(getActivity(), "Twitter oAuth tokens",
					"Please set your twitter oauth tokens first!", false);
		}

		// Shared Preferences
		mSharedPreferences = getActivity().getSharedPreferences(
				Const.PREF_NAME, 0);

		//details
		btnBackDetails = (Button) view.findViewById(R.id.btnBack);
		btnReply = (Button) view.findViewById(R.id.btnReply);
		edtReply = (EditText) view.findViewById(R.id.edtReply);
		btnBackDetails.setOnClickListener(this);
		btnReply.setOnClickListener(this);
		delayPrefs = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		gotoStep(STEP_HOME);
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		isClickLink = false;
	}

	private class MyWebViewClient extends WebViewClient {
		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			// get list status,show home timeline
			if (isTwitterLoggedInAlready()) {
				tvMyScreenName.setVisibility(View.VISIBLE);
				ivDirectMsg.setVisibility(View.VISIBLE);
				ivNewTweet.setVisibility(View.VISIBLE);
				refreshHeader.setVisibility(View.VISIBLE);
				AccessToken accessToken;
				String oauthAccessToken = mSharedPreferences.getString(
						Const.IEXTRA_OAUTH_TOKEN, "");
				String oAuthAccessTokenSecret = mSharedPreferences.getString(
						Const.PREF_KEY_OAUTH_SECRET, "");
				long userId = mSharedPreferences.getLong(
						Const.PREF_KEY_USER_ID, 0);
				ManagerTwitterApp.getInstance().setUserId(userId);

				accessToken = new AccessToken(oauthAccessToken,
						oAuthAccessTokenSecret, userId);
				ConfigurationBuilder builder = new ConfigurationBuilder();
				builder.setOAuthConsumerKey(Const.CONSUMER_KEY);
				builder.setOAuthConsumerSecret(Const.CONSUMER_SECRET);
				Configuration configuration = builder.build();
				TwitterFactory factory = new TwitterFactory(configuration);
				twitter = factory.getInstance();
				twitter.setOAuthAccessToken(accessToken);
				User user;
				try {
					user = twitter.showUser(userId);// hoi lau
					myScreenName = "@" + user.getScreenName();
					tvMyScreenName.setText(myScreenName);
					ManagerTwitterApp.getInstance().setScreenName(
							user.getScreenName());
					Utils.saveStringIntoFreference(Utils.keyAcccountTwitter,
							user.getScreenName());
				} catch (TwitterException e1) {
					e1.printStackTrace();
				}
				TwitterUtils.getFeedTwitter(getActivity(),
						StringUtils.LIMIT_TWITTER_DEFAULT, twitter,
						FragmentTwitter.this, webViewTwitter);
			} else {
				loginToTwitter();
			}
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.startsWith(TwitterUtils.CLICK_LINK_ON_TWEET)) {
				isClickLink = true;
				String urlClick = url
						.substring(TwitterUtils.CLICK_LINK_ON_TWEET.length());
				Intent i = new Intent(getActivity(), OpenLinkActivity.class);
				i.putExtra("url", urlClick);
				startActivity(i);
			}
			return true;
		}

	}

	final class MyJavaScriptInterface {

		public void clickTweet(String url) {
			if (!isClickLink && isShowHome && isloadedwebViewDetailsTweet) {
				idTweet = Long.valueOf(url
						.substring(TwitterUtils.CLICK_TWITTER_ROW.length()));
				for (int i = 0; i < TwitterUtils.listStatus.size(); i++) {
					if (idTweet == TwitterUtils.listStatus.get(i).getId()) {
						status = TwitterUtils.listStatus.get(i);
						retweetStatus = status.getRetweetedStatus();
						break;
					}
				}
				if (retweetStatus == null) {
					screenName = status.getUser().getScreenName();
					avatarSrc = status.getUser()
							.getOriginalProfileImageURLHttps();
					msgContent = status.getText();
				} else {
					screenName = retweetStatus.getUser().getScreenName();
					avatarSrc = retweetStatus.getUser()
							.getOriginalProfileImageURLHttps();
					msgContent = retweetStatus.getText();
				}
				isShowHome = false;
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						edtReply.setHint("Reply to " + screenName);
						TwitterUtils.showTweetDetails(getActivity(), status,
								retweetStatus, webViewDetailsTweet);
						gotoStep(STEP_DETAILS);
					}
				});
			}
		}
	}

	final class MyJavaScriptInterfaceForDetailsTweet {

		public void clickVideoOnDetailTweet(final String src) {
			getActivity().runOnUiThread(new Runnable() {

				@Override
				public void run() {
					String reloadVideo = "javascript:stop();";
					webViewDetailsTweet.loadUrl(reloadVideo);
					Intent i = new Intent(getActivity(),
							PlayVideoYoutubeActivity.class);
					i.putExtra("uploadvideolink", src);
					startActivity(i);

				}
			});

		}

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		ivDirectMsg.setOnClickListener(this);
		ivNewTweet.setOnClickListener(this);
		pullToRefreshWebView.setOnRefreshListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ivDirectMsg:
			Intent iDrMsg = new Intent(this.getActivity(),
					DirectMessageActivity.class);
			startActivity(iDrMsg);
			break;
		case R.id.ivNewTweet:
			Intent iNewTweet = new Intent(this.getActivity(),
					NewTweetActivity.class);
			iNewTweet.putExtra("sendTo", "Public Tweet");
			iNewTweet.putExtra("title", "New Tweet");
			iNewTweet.putExtra("size", 18);
			try {
				User user = twitter.showUser(twitter.getId());
				iNewTweet.putExtra("avatar", user.getOriginalProfileImageURLHttps());
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			startActivityForResult(iNewTweet, Utils.START_NEW_TWEET_ACT);
			break;

		default:
			break;
		}
		if (v == btnBackDetails) {
			String reloadVideo = "javascript:stop();";
			webViewDetailsTweet.loadUrl(reloadVideo);
			isShowHome = true;
			gotoStep(STEP_HOME);

		}
		if (v == btnReply) {
			if (Utils.isBlockedAddress(edtReply.getText().toString())
					|| Utils.isBlockedWord(edtReply.getText().toString())) {
				DialogUtils.showDialogBannedWord(getActivity(),
						Utils.isBlockedWord(edtReply.getText().toString()),
						Utils.isBlockedAddress(edtReply.getText().toString()));
			} else {
				if (!edtReply.getText().toString().equals("")) {
					btnReply.setEnabled(false);
					if (delayPrefs.getInt("Delay", 10) == 0) {
						actionReply();
					} else {
						Intent intent = new Intent(getActivity(),
								CountDownActivity.class);
						intent.putExtra("scrName", "@" + screenName);
						intent.putExtra("avatarSrc", avatarSrc);
						intent.putExtra("msgContent", edtReply.getText()
								.toString());
						intent.putExtra("FromFB", false);
						startActivityForResult(intent,
								Utils.START_COUNT_DOWN_REPLY);
						// finish();
					}
				} else {
					Toast.makeText(getActivity(), "Please enter your reply",
							Toast.LENGTH_LONG).show();
				}
			}
		}
		if (v == lnDialogTip) {
			if (isPause) {
				if (waitTimer != null && mp != null) {
					mp.pause();
					seek = mp.getCurrentPosition();
					waitTimer.pause();
				}
			} else {
				if (waitTimer != null && mp != null) {
					mp.seekTo(seek);
					mp.start();
					waitTimer.resume();
				}
			}
			isPause = !isPause;
		}
	}

	private void loginToTwitter() {
		ConfigurationBuilder builder = new ConfigurationBuilder();
		builder.setOAuthConsumerKey(Const.CONSUMER_KEY);
		builder.setOAuthConsumerSecret(Const.CONSUMER_SECRET);
		builder.setUseSSL(true);
		Configuration configuration = builder.build();

		twitter = new TwitterFactory(configuration).getInstance();
		twitter.setOAuthAccessToken(null);

		try {
			requestToken = twitter.getOAuthRequestToken(Const.CALLBACK_URL); // hoi
																				// lau
			Intent intent = new Intent(this.getActivity(), TwitterLogin.class);
			intent.putExtra(Const.IEXTRA_AUTH_URL,
					requestToken.getAuthorizationURL());
			startActivityForResult(intent,
					Utils.START_ACT_FOR_GET_TOKEN_TWITTER);
		} catch (TwitterException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Utils.START_ACT_FOR_GET_TOKEN_TWITTER) {
			if (resultCode == Activity.RESULT_OK) {
				tvMyScreenName.setVisibility(View.VISIBLE);
				ivDirectMsg.setVisibility(View.VISIBLE);
				ivNewTweet.setVisibility(View.VISIBLE);
				refreshHeader.setVisibility(View.VISIBLE);
				String oauthVerifier = data.getExtras().getString("URI");
				Uri uri = Uri.parse(oauthVerifier);
				if (uri != null
						&& uri.toString().startsWith(Const.CALLBACK_URL)) {
					// oAuth verifier
					verifier = uri
							.getQueryParameter(Const.IEXTRA_OAUTH_VERIFIER);
					try {
						// Get the access token
						AccessToken accessToken;
						accessToken = twitter.getOAuthAccessToken(requestToken,
								verifier);
						// Shared Preferences
						Editor e = mSharedPreferences.edit();
						// After getting access token, access token secret
						// store them in application preferences
						e.putString(Const.IEXTRA_OAUTH_TOKEN,
								accessToken.getToken());
						e.putString(Const.PREF_KEY_OAUTH_SECRET,
								accessToken.getTokenSecret());
						e.putLong(Const.PREF_KEY_USER_ID,
								accessToken.getUserId());
						// Store login status - true
						e.putBoolean(Const.PREF_KEY_TWITTER_LOGIN, true);
						e.commit(); // save changes

						long userId = accessToken.getUserId();
						ManagerTwitterApp.getInstance().setUserId(userId);
						User user = twitter.showUser(userId);
						myScreenName = "@" + user.getScreenName();
						tvMyScreenName.setText(myScreenName);
						ManagerTwitterApp.getInstance().setScreenName(
								user.getScreenName());
						Utils.saveStringIntoFreference(
								Utils.keyAcccountTwitter, user.getScreenName());
						// show icon new tweet and new msg
						TwitterUtils.getFeedTwitter(getActivity(),
								StringUtils.LIMIT_TWITTER_DEFAULT, twitter,
								this, webViewTwitter);

						LoginObject.get_instance().setTwitterId(String.valueOf(twitter.getId()));
						new UpdateUserInfor(null).execute();
					} catch (Exception e) {
						// Check log for login errors
						Log.e("Twitter Login Error", "> " + e.getMessage());
					}
				}
			} else if (resultCode == Activity.RESULT_CANCELED) {
				Log.i("CANCEL", "Twitter auth canceled.");
				tvMyScreenName.setVisibility(View.GONE);
				ivDirectMsg.setVisibility(View.GONE);
				ivNewTweet.setVisibility(View.GONE);
				refreshHeader.setVisibility(View.GONE);
			} else {
				Log.i("Exception", "Cannot receive intent");
			}
		}
		if (requestCode == Utils.START_NEW_TWEET_ACT) {
			if (resultCode == Activity.RESULT_OK) {
				TwitterUtils.getFeedTwitter(getActivity(),
						StringUtils.LIMIT_TWITTER_DEFAULT, twitter, this,
						webViewTwitter);
				Toast.makeText(getActivity(), "Tweet Successful",
						Toast.LENGTH_SHORT).show();
			}
		}
		if (requestCode == Utils.START_COUNT_DOWN_FOR_RETWEET) {
			if (resultCode == Activity.RESULT_OK) {
				actionRetweet();
			}
		}
		if (requestCode == Utils.START_COUNT_DOWN_REPLY) {
			if (resultCode == Activity.RESULT_OK) {
				actionReply();
			}
			btnReply.setEnabled(true);
		}
		if (requestCode == Utils.CALL_TIP_FOR_FAVORITE) {
			isNotFavoriting = true;
			if (resultCode == Activity.RESULT_OK) {
				functionFavorite(status, retweetStatus);
			}
		}
		if (requestCode == Utils.CALL_TIP_FOR_RETWEET) {
			if (resultCode == Activity.RESULT_OK) {
				actionRetweet();
			}
		}
	}

	/**
	 * Check user already logged in your application using twitter Login flag is
	 * fetched from Shared Preferences
	 * */
	private boolean isTwitterLoggedInAlready() {
		// return twitter login status from Shared Preferences
		return mSharedPreferences.getBoolean(Const.PREF_KEY_TWITTER_LOGIN,
				false);
	}

	@Override
	public void onPullDownToRefresh(PullToRefreshBase<WebView> refreshView) {
		if (isTwitterLoggedInAlready()) {
			refreshView.onRefreshComplete();
			TwitterUtils.getFeedTwitter(getActivity(),
					StringUtils.LIMIT_TWITTER_DEFAULT, twitter, this,
					webViewTwitter);
		} else {
			loginToTwitter();
			refreshView.onRefreshComplete();

		}

	}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase<WebView> refreshView) {
		if (isTwitterLoggedInAlready()) {
			refreshView.onRefreshComplete();
			LIMIT_TWITTER += StringUtils.LIMIT_TWITTER_ADVANCE;
			TwitterUtils.getFeedTwitter(getActivity(), LIMIT_TWITTER, twitter,
					this, webViewTwitter);
		} else {
			loginToTwitter();
			refreshView.onRefreshComplete();
		}
	}

	@Override
	public void onGetSuccess(String requestCode) {
		if (requestCode.equalsIgnoreCase(GET_FEED_TWITTER)) {
			refreshHeader.setVisibility(View.GONE);
		}
	}

	@Override
	public void onGetFailure(String requestCode) {
		if (requestCode.equalsIgnoreCase(GET_FEED_TWITTER)) {
			refreshHeader.setVisibility(View.GONE);
			Toast.makeText(getActivity(), "Can't load data twitter",
					Toast.LENGTH_LONG).show();
		}
	}

	private void gotoStep(int step) {
		layoutHome.setVisibility(step == STEP_HOME ? View.VISIBLE : View.GONE);
		layoutDetails.setVisibility(step == STEP_DETAILS ? View.VISIBLE
				: View.GONE);

	}

	private class MyWebViewClientForDetailsWebview extends WebViewClient {
		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			isloadedwebViewDetailsTweet = true;
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Log.i("", "aaaaaaaaa" + url);
			if (url.startsWith(TwitterUtils.CLICK_NAME_OR_API_SEARCH)) {
				String urlOpenLink = url
						.substring(TwitterUtils.CLICK_NAME_OR_API_SEARCH
								.length());
				Intent i = new Intent(getActivity(), OpenLinkActivity.class);
				i.putExtra("url", urlOpenLink);
				startActivity(i);
			}
			if (url.startsWith(TwitterUtils.CLICK_LINK_ON_TWEET)) {
				String urlClick = url
						.substring(TwitterUtils.CLICK_LINK_ON_TWEET.length());
				Intent i = new Intent(getActivity(), OpenLinkActivity.class);
				i.putExtra("url", urlClick);
				startActivity(i);
			}
			if (url.startsWith(TwitterUtils.CLICK_REPLY)) {
//				String replyTo;
//				if (retweetStatus == null) {
//					replyTo = status.getUser().getScreenName();
//				} else {
//					replyTo = retweetStatus.getUser().getScreenName();
//				}
//				Intent iNewTweet = new Intent(getActivity(),
//						NewTweetActivity.class);
//				iNewTweet.putExtra("sendTo", "Public Tweet");
//				iNewTweet.putExtra("title", "New Tweet");
//				iNewTweet.putExtra("avatar", avatarSrc);
//				iNewTweet.putExtra("size", 18);
//				iNewTweet.putExtra("replyTo", replyTo);
//				startActivity(iNewTweet);
				edtReply.post(new Runnable() {
				    @Override
					public void run() {
				    	edtReply.requestFocusFromTouch();
				        InputMethodManager lManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE); 
				        lManager.showSoftInput(edtReply, 0);
				    }
				});
			} else if (url.startsWith(TwitterUtils.CLICK_FAVORITE)) {
				if (isNotFavoriting) {
					isNotFavoriting = false;
					if (retweetStatus == null) {
						isFavorite = TwitterUtils.getIsFavorite(status);
					} else {
						isFavorite = TwitterUtils.getIsFavorite(retweetStatus);
					}
					if (!isFavorite) {
						if (prefs.getBoolean(StringUtils.KEY_TWEET_TIP, true)) {
							Intent intent = new Intent(getActivity(),
									TipActivity.class);
							intent.putExtra("isTipOfFb", false);
							intent.putExtra("iseExecuteWhenTurnOff", true);
							intent.putExtra("action", "Favorite");
							startActivityForResult(intent,
									Utils.CALL_TIP_FOR_FAVORITE);
						} else {
							functionFavorite(status, retweetStatus);
						}
					} else {
						functionFavorite(status, retweetStatus);
						isFavorite = false;
					}
				}
			} else if (url.startsWith(TwitterUtils.CLICK_RETWEET)) {
				final Dialog dialog = new Dialog(getActivity(),
						android.R.style.Theme_Translucent_NoTitleBar);
				dialog.setContentView(R.layout.custom_dialog_re_quote_tweet);

				btnCallRetweet = (Button) dialog
						.findViewById(R.id.btnCallRetweet);
				btnCallQuoteTweet = (Button) dialog
						.findViewById(R.id.btnCallQuoteTweet);
				btnCancel = (Button) dialog.findViewById(R.id.btnCancel);

				LayoutParams lp = dialog.getWindow().getAttributes();
				lp.width = android.view.ViewGroup.LayoutParams.MATCH_PARENT;
				lp.height = android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

				lp.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
				dialog.getWindow().setAttributes(lp);
				dialog.show();

				btnCallQuoteTweet
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								dialog.dismiss();
								Intent intent = new Intent(getActivity(),
										NewTweetActivity.class);
								intent.putExtra("title", "Quote Tweet");
								intent.putExtra("size", 15);
								intent.putExtra("sendTo", getResources()
										.getString(R.string.txt_publictweet));
								intent.putExtra("scrName", screenName);
								intent.putExtra("msgContent", msgContent);
								intent.putExtra("avatar", avatarSrc);
								intent.putExtra("QUOTETWEET", true);
								startActivityForResult(intent, Utils.START_NEW_TWEET_ACT);
								dialog.dismiss();
							}
						});

				// Chu y change trang thai retweeted
				btnCallRetweet.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
//						if (prefs.getBoolean(StringUtils.KEY_TWEET_TIP, true)) {
//							Intent intent = new Intent(getActivity(),
//									TipActivity.class);
//							intent.putExtra("isTipOfFb", false);
//							intent.putExtra("action", "Retweet");
//							startActivityForResult(intent,
//									Utils.CALL_TIP_FOR_RETWEET);
//						} else {
							actionRetweetCheckedBlockAndDelay();
//						}
					}
				});

				btnCancel.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
			} else if (url.startsWith(TwitterUtils.CLICK_MORE)) {
				customDialogMore();
			}
			return true;
		}
	}

	private boolean isBlockWordAndAddress(String string) {
		if (Utils.isBlockedAddress(string) || Utils.isBlockedWord(string)) {
			DialogUtils.showDialogBannedWordEnableSendAnyway(getActivity(),
					Utils.isBlockedWord(string),
					Utils.isBlockedAddress(string), FragmentTwitter.this);
			return true;
		}
		return false;
	}

	private void startCountdownForRetweet() {
		Intent intent = new Intent(getActivity(), CountDownActivity.class);
		intent.putExtra("scrName", "Retweet of @" + screenName);
		intent.putExtra("avatarSrc", avatarSrc);
		intent.putExtra("msgContent", msgContent);
		intent.putExtra("FromFB", false);
		intent.putExtra("isRetweet", true);
		startActivityForResult(intent, Utils.START_COUNT_DOWN_FOR_RETWEET);
	}

	private void functionFavorite(Status status, Status retweetStatus) {

		int favoritesCount;
		int retweetCount;
		Long idTweet;

		if (retweetStatus == null) {
			idTweet = this.idTweet;
			favoritesCount = TwitterUtils.getFavoriteCount(status);
			retweetCount = TwitterUtils.getRetweetCount(status);
			isFavorite = TwitterUtils.getIsFavorite(status);

		} else {
			idTweet = retweetStatus.getId();
			favoritesCount = TwitterUtils.getFavoriteCount(retweetStatus);
			retweetCount = TwitterUtils.getRetweetCount(retweetStatus);
			isFavorite = TwitterUtils.getIsFavorite(retweetStatus);
		}

		if (isFavorite) {
			try {
				FragmentTwitter.twitter.destroyFavorite(idTweet);
				TwitterUtils.setFavoriteCount(idTweet, favoritesCount - 1);
				TwitterUtils.setRetweetCount(idTweet, retweetCount);
				TwitterUtils.setIsFavorite(idTweet, false);
			} catch (TwitterException e) {
				e.printStackTrace();
			}

		} else {
			try {
				TwitterUtils.setFavoriteCount(idTweet, favoritesCount + 1);
				TwitterUtils.setRetweetCount(idTweet, retweetCount);
				TwitterUtils.setIsFavorite(idTweet, true);
				FragmentTwitter.twitter.createFavorite(idTweet);
				Toast.makeText(getActivity(), "Successful", Toast.LENGTH_LONG).show();
			} catch (TwitterException e) {
				e.printStackTrace();
			}
		}
		TwitterUtils.showTweetDetails(getActivity(), status, retweetStatus,
				webViewDetailsTweet);
		isNotFavoriting = true;
	}

	private void actionReply() {
		long idTweet;
		String replyTo;
		if (retweetStatus == null) {
			idTweet = status.getId();
			replyTo = status.getUser().getScreenName();

		} else {
			idTweet = retweetStatus.getId();
			replyTo = retweetStatus.getUser().getScreenName();
		}
		TwitterUtils.replyTweet("@" + replyTo + " "
				+ edtReply.getText().toString(), idTweet);
		Toast.makeText(getActivity(), "Successful", Toast.LENGTH_LONG).show();
		edtReply.setText("");
		btnReply.setEnabled(true);
		Utils.hideKeyboard(getActivity());
	}

	public void actionRetweetCheckedBlockAndDelay() {
		if (!isBlockWordAndAddress(msgContent + screenName)) {
			if (prefs.getInt("Delay", 10) == 0) {
				actionRetweet();
			} else {
				startCountdownForRetweet();
			}
		}
	}

	public void actionRetweet() {
		int favoritesCount;
		int retweetCount;
		boolean isRetweetedByMe;
		Long idTweet;

		if (retweetStatus == null) {
			idTweet = this.idTweet;
			favoritesCount = TwitterUtils.getFavoriteCount(status);
			retweetCount = TwitterUtils.getRetweetCount(status);
			isRetweetedByMe = TwitterUtils.getIsReTweet(status);
		} else {
			idTweet = retweetStatus.getId();
			favoritesCount = TwitterUtils.getFavoriteCount(retweetStatus);
			retweetCount = TwitterUtils.getRetweetCount(retweetStatus);
			isRetweetedByMe = TwitterUtils.getIsReTweet(retweetStatus);
		}
		if (!isRetweetedByMe) {
			try {
				FragmentTwitter.twitter.retweetStatus(idTweet);
				TwitterUtils.setFavoriteCount(idTweet, favoritesCount);
				TwitterUtils.setRetweetCount(idTweet, retweetCount + 1);
				TwitterUtils.setIsRetweet(idTweet, true);
				Toast.makeText(getActivity(), "Retweet successfully!",
						Toast.LENGTH_LONG).show();
				TwitterUtils.showTweetDetails(getActivity(), status,
						retweetStatus, webViewDetailsTweet);
			} catch (TwitterException e) {
				e.printStackTrace();
				Toast.makeText(getActivity(), "Retweet not successfully!",
						Toast.LENGTH_LONG).show();
			}
		}

	}

	String htmlSendMail;

	private void customDialogMore() {
		final Dialog dialog = new Dialog(getActivity(),
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.more_tweet_dialog);
		Button btnMail = (Button) dialog.findViewById(R.id.btn_mail);
		Button btnCopyLink = (Button) dialog.findViewById(R.id.btn_copy_link);
		Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
		LayoutParams lp = dialog.getWindow().getAttributes();
		lp.width = android.view.ViewGroup.LayoutParams.MATCH_PARENT;
		lp.height = 300;
		lp.gravity = Gravity.BOTTOM;
		dialog.getWindow().setAttributes(lp);
		dialog.show();
		status = null;
		for (int i = 0; i < TwitterUtils.listStatus.size(); i++) {
			if (idTweet == TwitterUtils.listStatus.get(i).getId()) {
				status = TwitterUtils.listStatus.get(i);
				if (status.getRetweetedStatus() != null) {
					status = status.getRetweetedStatus();
				}
				break;
			}
		}
		final String screenName;
		final String name;
		final Long idTweet;
		htmlSendMail = TwitterUtils.getHtmlForSendMail(status, getActivity());
		//		replace('\n', (char) 32).replace((char) 160, (char) 32)
		//		.replace((char) 65532, (char) 32).replace('\uFFFC', (char) 32).trim();
		Log.i("", htmlSendMail);
		screenName = status.getUser().getScreenName();
		idTweet = status.getId();
		name = status.getUser().getName();

		btnMail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent emailIntent = new Intent(Intent.ACTION_SEND);
				emailIntent.setType("text/html");
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Tweet from " + name
						+ "(@" + screenName + ")");
				emailIntent.putExtra(Intent.EXTRA_TEXT,
						Html.fromHtml(htmlSendMail));
				startActivity(Intent.createChooser(emailIntent,
						"Send mail via..."));
				dialog.dismiss();
			}
		});
		btnCopyLink.setOnClickListener(new OnClickListener() {
			@SuppressLint("NewApi")
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				@SuppressWarnings("static-access")
				ClipboardManager clipboard = (ClipboardManager) getActivity()
						.getSystemService(getActivity().CLIPBOARD_SERVICE);
				clipboard.setText("https://twitter.com/" + screenName
						+ "/status/" + idTweet);
				dialog.dismiss();
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});
	}

	@Override
	public void sendAnyway() {
		startCountdownForRetweet();

	}

	@Override
	public void doOnClick() {
	}

	@Override
	public void doTopToBottom() {
		((MainActivity) getActivity()).hideTaskBar();
		homeTwitterBar.setVisibility(View.VISIBLE);
		pullToRefreshWebView.onRefreshComplete();
	}

	@Override
	public void doBottomToTop() {
		((MainActivity) getActivity()).showTaskBar();
		homeTwitterBar.setVisibility(View.GONE);
		pullToRefreshWebView.onRefreshComplete();
	}
}
