package com.example.besafepost.singleton;

public class ManagerFacebookApp {
	private static ManagerFacebookApp uniqInstance;
	private String uid;
	private String srcImgprofile;
	private String accFacebook;

	private ManagerFacebookApp() {
	}

	public static synchronized ManagerFacebookApp getInstance() {
		if (uniqInstance == null) {
			uniqInstance = new ManagerFacebookApp();
		}
		return uniqInstance;
	}

	public String getAccFacebook() {
		return accFacebook;
	}

	public void setAccFacebook(String accFacebook) {
		this.accFacebook = accFacebook;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getSrcImgprofile() {
		return srcImgprofile;
	}

	public void setSrcImgprofile(String srcImgprofile) {
		this.srcImgprofile = srcImgprofile;
	}
}
