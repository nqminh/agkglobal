package com.example.besafepost.singleton;

import java.io.Serializable;

import android.content.Context;

import com.example.besafepost.BesafePostApplication;
import com.example.besafepost.utils.Utils;
import com.google.gson.annotations.SerializedName;

public class LoginObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9196318204661278410L;
	@SerializedName("id")
	private String id;
	@SerializedName("full_name")
	private String fullName;
	@SerializedName("birthday")
	private String birthYear;
	@SerializedName("sex")
	private String sex;
	@SerializedName("email")
	private String email;
	@SerializedName("age")
	private String age;
	@SerializedName("phone")
	private String phone;
	@SerializedName("facebook_id")
	private String facebookId;
	@SerializedName("twitter_id")
	private String twitterId;
	@SerializedName("password")
	private String password;
	@SerializedName("zip_code")
	private String zipCode;
	
	private static LoginObject _instance;
	
	public static LoginObject get_instance() {
		if (_instance == null) {
			_instance = new LoginObject();
		}
		return _instance;
	}

	public static void set_instance(LoginObject _instance) {
		LoginObject._instance = _instance;
	}
	
	public LoginObject() {
		id = "";
		fullName = "";
		birthYear = "";
		sex = "";
		email = "";
		age = "";
		phone = "";
		facebookId = "";
		twitterId = "";
		password = "";
		zipCode = "";
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getBirthYear() {
		return birthYear;
	}
	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFacebookId() {
		return facebookId;
	}
	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}
	public String getTwitterId() {
		return twitterId;
	}
	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public void saveToCache() {
		Utils.writeObject2File(BesafePostApplication.getContext(), this, "data.dat");
	}

	public void loadFromCache(Context context) {
		Object obj = Utils.loadObjectFromFile(context, "data.dat");
		if (obj != null && obj instanceof LoginObject) {
			_instance = (LoginObject) obj;
		}
	}
}
