package com.example.besafepost.singleton;

public class ManagerTwitterApp {
	private static ManagerTwitterApp uniqInstance;
	private long userId;
	private String screenName;

	private ManagerTwitterApp() {
	}

	public static synchronized ManagerTwitterApp getInstance() {
		if (uniqInstance == null) {
			uniqInstance = new ManagerTwitterApp();
		}
		return uniqInstance;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
