package com.example.besafepost.activity;

import org.json.JSONException;
import org.json.JSONObject;

import com.example.besafepost.interfaces.IAsyncTaskDelegate;
import com.example.besafepost.singleton.LoginObject;
import com.example.besafepost.task.AbstractTask;
import com.example.besafepost.task.LoginTask;
import com.example.besafepost.task.LoginTask.LoginTaskObject;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.Utils;
import com.example.besafepost.task.ForgotPassTask;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LogInAppActivity extends Activity implements OnClickListener,
		IAsyncTaskDelegate<AbstractTask> {
	private EditText edtLogInEmail;
	private EditText edtLogInPw;
	private Button btnLogIn;
	private ImageView ivWarn1;
	private ImageView ivWarn2;
	private TextView tvSignUpApp;
	private TextView tvHelp, tvPatent, tvForgot;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_app);

		edtLogInEmail = (EditText) findViewById(R.id.edtLogInEmail);
		edtLogInPw = (EditText) findViewById(R.id.edtLogInPw);
		btnLogIn = (Button) findViewById(R.id.btnLogInApp);
		ivWarn1 = (ImageView) findViewById(R.id.iv_login_warning_1);
		ivWarn2 = (ImageView) findViewById(R.id.iv_login_warning_2);
		tvSignUpApp = (TextView) findViewById(R.id.tvSignupApp);
		tvHelp = (TextView) findViewById(R.id.btnHelp);
		tvPatent = (TextView) findViewById(R.id.btnPatentPending);
		tvForgot = (TextView) findViewById(R.id.tvforgotpass);

		tvSignUpApp.setOnClickListener(this);
		btnLogIn.setOnClickListener(this);
		tvHelp.setOnClickListener(this);
		tvPatent.setOnClickListener(this);
		tvForgot.setOnClickListener(this);
		setupUI(findViewById(R.id.main_view));
	}

	private void setupUI(View view) {

		// Set up touch listener for non-text box views to hide keyboard.
		if (!(view instanceof EditText)) {

			view.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					Utils.hideKeyboard(LogInAppActivity.this);
					return false;
				}

			});
		}

		// If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {

			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

				View innerView = ((ViewGroup) view).getChildAt(i);

				setupUI(innerView);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvSignupApp:
			Intent intent = new Intent(LogInAppActivity.this, SignUpAppActivity.class);
			startActivity(intent);
			break;
		case R.id.btnLogInApp:
			if (edtLogInEmail.getText().toString().trim().equals("")
					&& edtLogInPw.getText().toString().equals("")) {
				ivWarn1.setVisibility(View.VISIBLE);
				ivWarn2.setVisibility(View.VISIBLE);
			} else if (edtLogInEmail.getText().toString().trim().equals("")) {
				ivWarn1.setVisibility(View.VISIBLE);
				ivWarn2.setVisibility(View.INVISIBLE);
			} else if (edtLogInPw.getText().toString().equals("")) {
				ivWarn1.setVisibility(View.INVISIBLE);
				ivWarn2.setVisibility(View.VISIBLE);
			} else {
				ivWarn1.setVisibility(View.INVISIBLE);
				ivWarn2.setVisibility(View.INVISIBLE);
				new LoginTask(this, edtLogInEmail.getText().toString(), edtLogInPw.getText()
						.toString()).execute();
			}
			break;
		case R.id.btnHelp:
			String url = "http://besafeapps.com/";
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(url));
			startActivity(i);
			break;
		case R.id.btnPatentPending:
			Intent intent2 = new Intent(Intent.ACTION_VIEW);
			intent2.setData(Uri.parse("http://besafeapps.com/"));
			startActivity(intent2);
			break;
		case R.id.tvforgotpass:
			showDialogForgot();
			break;
		default:
			break;
		}
	}

	Dialog dialog;

	private void showDialogForgot() {
		// TODO
		LayoutInflater factory = LayoutInflater.from(this);
		final View dialogView = factory.inflate(R.layout.dialog_forgot, null);
		dialog = new Dialog(LogInAppActivity.this, R.style.CustomAlertDialog);
		dialog.setContentView(dialogView);

		dialogView.findViewById(R.id.btnSend).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!TextUtils.isEmpty(((EditText) dialogView.findViewById(R.id.edtMail)).getText()
						.toString())) {
					new ForgotPassTask(LogInAppActivity.this, ((EditText) dialogView
							.findViewById(R.id.edtMail)).getText().toString()).execute();
					dialog.dismiss();
					Utils.hideKeyboard(LogInAppActivity.this);
				} else {
					Toast.makeText(getApplicationContext(), "Please enter email", Toast.LENGTH_LONG)
							.show();
				}
			}
		});

		dialogView.findViewById(R.id.btnCancel).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				Utils.hideKeyboard(LogInAppActivity.this);
			}
		});
		dialog.show();
	}

	@Override
	public void onGetStart(AbstractTask task) {
		if (task instanceof LoginTask) {
			DialogUtils.showProgressDialog(LogInAppActivity.this, "", "Signing in...", false);
		} else {
			DialogUtils.showProgressDialog(LogInAppActivity.this, "", "Sending ...", false);
		}
	}

	@Override
	public void onGetSuccess(AbstractTask task) {
		if (task instanceof LoginTask) {
			DialogUtils.dismissDialog();
			LoginTaskObject taskObject = ((LoginTask) task).getLoginTaskObject();
			if (taskObject.returnCode.equalsIgnoreCase("SUCCESS")) {
				LoginObject.set_instance(taskObject.data);
				LoginObject.get_instance().saveToCache();
				Intent intent = new Intent(LogInAppActivity.this, MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			} else {
				Toast.makeText(LogInAppActivity.this,
						"Wrong email or password.Please try again!!!", Toast.LENGTH_LONG).show();
			}
		} else if (task instanceof ForgotPassTask) {
			DialogUtils.dismissDialog();
			try {
				JSONObject result = new JSONObject(task.getResult());
				if (result.has("returnCode") && result.getString("returnCode").equals("SUCCESS")) {
					Toast.makeText(LogInAppActivity.this,
							result.getString("msg"), Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(LogInAppActivity.this,
							result.getString("msg"), Toast.LENGTH_LONG).show();
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onGetFailure(AbstractTask task, Exception exception) {
		DialogUtils.dismissDialog();
		exception.printStackTrace();
	}
}
