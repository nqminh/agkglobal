package com.example.besafepost.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.besafepost.model.AlbumStorageDirFactory;
import com.example.besafepost.model.BaseAlbumDirFactory;
import com.example.besafepost.model.FroyoAlbumDirFactory;
import com.example.besafepost.model.Touch;
import com.example.besafepost.utils.Utils;

public class AdjustPhotoActivity extends Activity implements OnClickListener {


	private Button btnCancel, btnChoose;
	private ImageView ivAdjust;
	private Bitmap bitmap;
	private Uri photoUri;

	private AlertDialog.Builder builder;
	private final String items[] = { "Choose Existing", "Take Photo" };

	private String path;

	private String mCurrentPhotoPath;
	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;

	private Bitmap capturedBitmap;
	public static boolean isTakePhoto;

	private ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.adjust_photo);

		btnCancel = (Button) findViewById(R.id.bt_Canc);
		btnChoose = (Button) findViewById(R.id.bt_Choose);
		ivAdjust = (ImageView) findViewById(R.id.ivAdjust);

		btnCancel.setOnClickListener(this);
		btnChoose.setOnClickListener(this);

		Intent intent = getIntent();
		boolean isFirstCall = intent.getBooleanExtra("firstCall", false);
		Log.i("FIRST CALL", "" + isFirstCall);
		if (isFirstCall) {
			callDialog();
		}

		ivAdjust.setOnTouchListener(new Touch());

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			mAlbumStorageDirFactory = new BaseAlbumDirFactory();
		}
	}

	private void callDialog() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.select_dialog_item, items);
		builder = new AlertDialog.Builder(AdjustPhotoActivity.this);
		builder.setTitle("Photo source");
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (item == 0) { // Choose Existing
					if (!Environment.getExternalStorageState().equals(
							Environment.MEDIA_MOUNTED)) {
						Toast.makeText(getApplicationContext(),
								"SDCard is not mounted!", Toast.LENGTH_SHORT)
								.show();
					} else {
						Intent intent = new Intent(AdjustPhotoActivity.this,
								SavedPhotosActivity.class);
						startActivityForResult(intent,
								Utils.PICK_EXISTING_PHOTO_RESULT_CODE);
					}

					pd = ProgressDialog.show(AdjustPhotoActivity.this, "",
							"Please wait...");
					new Thread() {
						@Override
						public void run() {
							try {
								sleep(1000);
							} catch (Exception e) {
								Log.e("Exception:", e.getMessage());
							}
							pd.dismiss();
						}
					}.start();

				} else { // Take Photo
					Intent intent = new Intent(
							android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
					File f = null;
					try {
						f = setUpPhotoFile();
						mCurrentPhotoPath = f.getAbsolutePath();
						intent.putExtra(MediaStore.EXTRA_OUTPUT,
								Uri.fromFile(f));
					} catch (IOException e) {
						e.printStackTrace();
						f = null;
						mCurrentPhotoPath = null;
					}

					startActivityForResult(intent, Utils.TAKE_PICTURE);
				}
			}
		});
		builder.setCancelable(true);
		builder.create().show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Utils.PICK_EXISTING_PHOTO_RESULT_CODE) {
			if (resultCode == Utils.PHOTO_RESULT_CODE) {
				path = data.getStringExtra("bitmap");
				Log.i("PATH", path);
				new ImageLoaderTask().execute();

				photoUri = Uri.parse(path);
				ivAdjust.setImageURI(photoUri);

				btnChoose.setEnabled(true);
				isTakePhoto = false;
			} else {
				btnChoose.setEnabled(true);
				btnChoose.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Toast.makeText(getApplicationContext(),
								"You have not chosen a photo",
								Toast.LENGTH_LONG).show();
					}
				});
			}
		} else if (requestCode == Utils.TAKE_PICTURE) {
			if (resultCode == Activity.RESULT_OK) {
				handleBigCameraPhoto();

				new ImageCompressTask().execute();
				isTakePhoto = true;
				// btnChoose.setEnabled(true);
			} else {
				Toast.makeText(getApplicationContext(), "Photo not taken",
						Toast.LENGTH_LONG).show();
			}
		} else if (requestCode == Utils.REQUEST_CALL_PHOTO_ACT) {
			if (resultCode == RESULT_OK) {
				setResult(RESULT_OK);
			} else {
				setResult(RESULT_CANCELED);
			}
			finish();
		}
	}

	@SuppressLint("SimpleDateFormat")
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = "IMG_" + timeStamp + "_";
		File albumF = getAlbumDir();
		File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
		return imageF;
	}

	private File setUpPhotoFile() throws IOException {

		File f = createImageFile();
		mCurrentPhotoPath = f.getAbsolutePath();

		return f;
	}

	private File getAlbumDir() {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {

			storageDir = mAlbumStorageDirFactory
					.getAlbumStorageDir(getAlbumName());

			if (storageDir != null) {
				if (!storageDir.mkdirs()) {
					if (!storageDir.exists()) {
						Log.d("CameraSample", "failed to create directory");
						return null;
					}
				}
			}

		} else {
			Log.v(getString(R.string.app_name),
					"External storage is not mounted READ/WRITE.");
		}

		return storageDir;
	}

	/* Photo album for this application */
	private String getAlbumName() {
		return getString(R.string.album_name);
	}

	private void handleBigCameraPhoto() {

		if (mCurrentPhotoPath != null) {
			setPic();
			galleryAddPic();
			mCurrentPhotoPath = null;
		}

	}

	private void setPic() {

		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
		int targetW = 300;
		int targetH = 400;

		/* Get the size of the image */
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
		int photoW = Math.min(bmOptions.outWidth, 2048);
		int photoH = Math.min(bmOptions.outHeight, 2048);

		/* Figure out which way needs to be reduced less */
		int scaleFactor = 1;
		if ((targetW > 0) || (targetH > 0)) {
			scaleFactor = Math.min(photoW / targetW, photoH / targetH);
		}

		/* Set bitmap options to scale the image decode target */
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
		Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

		/* Associate the Bitmap to the ImageView */
		// ivAdjust.setImageBitmap(bitmap);

		/* Get the captured image */
		capturedBitmap = bitmap;
	}

	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent(
				"android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

	public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight,
			Context context) {
		final float densityMultiplier = context.getResources()
				.getDisplayMetrics().density;
		int h = (int) (newHeight * densityMultiplier);
		int w = (int) (h * photo.getWidth() / ((double) photo.getHeight()));

		photo = Bitmap.createScaledBitmap(photo, w, h, true);

		return photo;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_Canc:
			finish();
			break;
		case R.id.bt_Choose:
			Intent intent = new Intent(AdjustPhotoActivity.this,
					PhotoActivity.class);
			ivAdjust.setDrawingCacheEnabled(true);
			ivAdjust.buildDrawingCache(true);

			bitmap = scaleDownBitmap(ivAdjust.getDrawingCache(true), 300, this);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			byte bytes[] = baos.toByteArray();

			intent.putExtra("bitmap_image", bytes);
			ivAdjust.setDrawingCacheEnabled(false);

			startActivityForResult(intent, Utils.REQUEST_CALL_PHOTO_ACT);
			break;

		default:
			break;
		}
	}

	private class ImageLoaderTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... voids) {
			/* There isn't enough memory to open up more than a couple camera photos */
			/* So pre-scale the target bitmap into which the file is decoded */

			/* Get the size of the ImageView */
			int targetW = 300;
			int targetH = 400;

			/* Get the size of the image */
			BitmapFactory.Options bmOptions = new BitmapFactory.Options();
			bmOptions.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path, bmOptions);
			int photoW = Math.min(bmOptions.outWidth, 1024);
			int photoH = Math.min(bmOptions.outHeight, 1024);

			/* Figure out which way needs to be reduced less */
			int scaleFactor = 1;
			if ((targetW > 0) || (targetH > 0)) {
				scaleFactor = Math.min(photoW / targetW, photoH / targetH);
			}

			/* Set bitmap options to scale the image decode target */
			bmOptions.inJustDecodeBounds = false;
			bmOptions.inSampleSize = scaleFactor;
			bmOptions.inPurgeable = true;

			/* Decode the JPEG file into a Bitmap */
			bitmap = BitmapFactory.decodeFile(path, bmOptions);
			return true;
		}
	}

	private class ImageCompressTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			Intent intent = new Intent(AdjustPhotoActivity.this,
					PhotoActivity.class);

			if (capturedBitmap.getHeight() > 500) {
				Log.i("IMG CAPTURED HEIGHT, WIDTH",
						"" + capturedBitmap.getHeight() + ","
								+ capturedBitmap.getWidth());
				capturedBitmap = scaleDownBitmap(capturedBitmap, 300,
						getApplicationContext());
				Log.i("HEIGHT, WIDTH", "" + capturedBitmap.getHeight() + ","
						+ capturedBitmap.getWidth());
			}

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			capturedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			byte bytes[] = baos.toByteArray();
			intent.putExtra("bitmap_image", bytes);
			startActivityForResult(intent, Utils.REQUEST_CALL_PHOTO_ACT);
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

			} else {
				Toast.makeText(getApplicationContext(),
						"Sorry! Please try again", Toast.LENGTH_LONG).show();
			}
		}

	}
}
