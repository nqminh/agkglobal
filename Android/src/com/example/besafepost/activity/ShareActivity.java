package com.example.besafepost.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.besafepost.adapter.EntryAdapter;
import com.example.besafepost.interfaces.IDeItemFriendList;
import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.model.ArticleItem;
import com.example.besafepost.model.EntryItemFriendList;
import com.example.besafepost.model.ItemFriendList;
import com.example.besafepost.model.SectionItemFriendList;
import com.example.besafepost.model.gsonobject.Post;
import com.example.besafepost.task.DownloadImageTask;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.FacebookUtils;
import com.example.besafepost.utils.StringUtils;
import com.example.besafepost.utils.Utils;

public class ShareActivity extends Activity implements IActivityDelegate, OnClickListener,
		OnItemClickListener, OnTouchListener {
	String postId;
	Button btnCancel, btnShare;
	ArticleItem item = new ArticleItem();
	private ImageView imgIcon, imgIconPrivacy, imgShowListFriend;
	private ListView lvFriendList;
	private EditText edtComment;
	private TextView tvTitleShare, tvProfileName, tvDistance, tvDetail;
	private ArrayList<IDeItemFriendList> items = new ArrayList<IDeItemFriendList>();
	private ArrayList<ItemFriendList> listFriendList = new ArrayList<ItemFriendList>();
	private long friendListId = 1;
	private SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share_activity);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnCancel.setOnClickListener(this);
		btnShare = (Button) findViewById(R.id.btnShare);
		btnShare.setOnClickListener(this);
		edtComment = (EditText) findViewById(R.id.edtComment);
		imgIcon = (ImageView) findViewById(R.id.imgIcon);
		tvTitleShare = (TextView) findViewById(R.id.tvTitleShare);
		tvProfileName = (TextView) findViewById(R.id.tvBottom);
		imgIconPrivacy = (ImageView) findViewById(R.id.imgIconPrivacy);
		imgShowListFriend = (ImageView) findViewById(R.id.imgShowListFriend);
		imgShowListFriend.setOnClickListener(this);
		lvFriendList = (ListView) findViewById(R.id.lvFriendList);
		tvDistance = (TextView) findViewById(R.id.tvDistance);
		tvDetail = (TextView) findViewById(R.id.tvDetail);
		edtComment.setOnClickListener(this);
		prefs = PreferenceManager.getDefaultSharedPreferences(this);

		// get postId
		postId = getIntent().getStringExtra("postID");
		item = FacebookUtils.getCurrentItemFromListArticleItem(postId);

		initView();

		FacebookUtils.getListFriendList(this);
		DialogUtils.dismissDialog();
		Utils.showKeyboard(this, edtComment);
	}

	private void initView() {
		// TODO Auto-generated method stub
		Post post = item.getPost();
		String strUrl;
		if (post.type == 46) {
			strUrl = post.profile.picSquare;
			tvDetail.setText(item.getProfileName());
			tvTitleShare.setText("Status Update");
			tvProfileName.setVisibility(View.GONE);
		} else if (post.type == 80) {
			tvTitleShare.setText(post.attachment.name);
			tvDetail.setText(post.attachment.caption);
			strUrl = post.attachment.media.get(0).src;
			tvProfileName.setVisibility(View.GONE);
		} else {
			if (!TextUtils.isEmpty(item.getSrcIconImage())) {
				strUrl = post.attachment.media.get(0).src;
			} else {
				strUrl = post.profile.picSquare;
			}
			tvProfileName.setText(item.getProfileName());
			if (StringUtils.isEmptyString(item.getSrcIconImage())) {
				tvDetail.setText("Share Post");
			} else {
				tvDetail.setText("Share Photo");
			}
			if (!TextUtils.isEmpty(item.getMessage())) {
				tvTitleShare.setText(item.getMessage());
			} else {
				tvTitleShare.setVisibility(View.GONE);
			}
		}

		DialogUtils.showProgressDialog(this, "", "Loading...", false);

		new DownloadImageTask(imgIcon).execute(strUrl);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return false;
	}

	@Override
	public void onClick(View v) {
		if (v == btnCancel) {
			finish();
		}
		if (v == btnShare) {
			btnShare.setEnabled(false);
			if (Utils.isBlockedAddress(edtComment.getText().toString())
					|| Utils.isBlockedWord(edtComment.getText().toString())) {
				DialogUtils.showDialogBannedWord(ShareActivity.this,
						Utils.isBlockedWord(edtComment.getText().toString()),
						Utils.isBlockedAddress(edtComment.getText().toString()));
			} else if (prefs.getBoolean(StringUtils.KEY_POST_TIP, true)) {
				Intent intent = new Intent(ShareActivity.this, TipActivity.class);
				intent.putExtra("isTipOfFb", true);
				intent.putExtra("action", "Share");
				startActivityForResult(intent, Utils.CALL_TIP_FOR_SHARE_POST);
			} else {
				functionShare(item, friendListId);
			}
		}
		if (v == imgShowListFriend) {
			lvFriendList.setVisibility(View.VISIBLE);
			tvDistance.setVisibility(View.GONE);
			Utils.hideKeyboard(ShareActivity.this);
		}
		if (v == edtComment) {
			lvFriendList.setVisibility(View.GONE);
			tvDistance.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onGetSuccess(String requestCode) {
		if (requestCode.equalsIgnoreCase(Utils.REQUEST_SHARE_POST)) {
			Toast.makeText(ShareActivity.this, "Successful", Toast.LENGTH_SHORT).show();
			setResult(RESULT_OK);
			finish();
		}
		if (requestCode.equalsIgnoreCase(Utils.REQUEST_GET_LIST_FRIENDLIST)) {
			listFriendList = FacebookUtils.getListFriendList();
			setListFriendList();
		}
	}

	@Override
	public void onGetFailure(String requestCode) {
		if (requestCode.equalsIgnoreCase(Utils.REQUEST_SHARE_POST)) {
			// DialogUtils.dismissDialog();
			Toast.makeText(ShareActivity.this, "Unsuccessful", Toast.LENGTH_SHORT).show();
		}
		if (requestCode.equalsIgnoreCase(Utils.REQUEST_GET_LIST_FRIENDLIST)) {
			// DialogUtils.dismissDialog();
		}
	}

	private void setListFriendList() {

		items.add(new SectionItemFriendList("Audience"));
		items.add(new EntryItemFriendList("Public", R.drawable.icon_public));
		items.add(new EntryItemFriendList("Friends", R.drawable.icon_friend));
		items.add(new EntryItemFriendList("Only Me", R.drawable.icon_only_me));

		items.add(new SectionItemFriendList("Friend Lists"));
		for (int i = 0; i < listFriendList.size(); i++) {
			items.add(new EntryItemFriendList(listFriendList.get(i).getNameFriendList(),
					R.drawable.icon_friend_list));
		}
		EntryAdapter adapter = new EntryAdapter(this, 0, items);
		adapter.setIndexSelected(1);
		lvFriendList.setAdapter(adapter);
		lvFriendList.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> lv, View arg1, int position, long arg3) {
		if (lv == lvFriendList) {
			lvFriendList.setVisibility(View.GONE);
			tvDistance.setVisibility(View.VISIBLE);
			((EntryAdapter) lvFriendList.getAdapter()).setIndexSelected(position);
			((EntryAdapter) lvFriendList.getAdapter()).notifyDataSetChanged();
			Utils.showKeyboard(ShareActivity.this, edtComment);
			friendListId = position;
			if (position == 1) {
				imgIconPrivacy.setImageResource(R.drawable.icon_public);
			}
			if (position == 2) {
				imgIconPrivacy.setImageResource(R.drawable.icon_friend);
			}
			if (position == 3) {
				imgIconPrivacy.setImageResource(R.drawable.icon_only_me);
			}
			if (position > 4) {
				friendListId = listFriendList.get(position - 5).getIdFriendList();
				imgIconPrivacy.setImageResource(R.drawable.icon_friend_list);
			}
		}

	}

	private void functionShare(ArticleItem item, long friendListId) {
		btnShare.setEnabled(true);
		if (!StringUtils.isEmptyString(item.getPermalink())) {
			Toast.makeText(ShareActivity.this, "Sharing...", Toast.LENGTH_SHORT).show();
			FacebookUtils.sharePost(edtComment.getText().toString(), item.getPermalink(),
					friendListId, ShareActivity.this);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Utils.CALL_TIP_FOR_SHARE_POST)
			if (resultCode == RESULT_OK) {
				functionShare(item, friendListId);
			}
		btnShare.setEnabled(true);
	}
}
