package com.example.besafepost.activity;

import java.io.IOException;
import java.lang.ref.WeakReference;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.example.besafepost.fragment.FragmentMesageHistory;
import com.example.besafepost.fragment.FragmentPost;
import com.example.besafepost.fragment.FragmentSMS;
import com.example.besafepost.fragment.FragmentSetting;
import com.example.besafepost.fragment.FragmentTwitter;
import com.example.besafepost.utils.FacebookUtils;
import com.example.besafepost.utils.ObjectSerializer;
import com.example.besafepost.utils.Utils;

public class MainActivity extends FragmentActivity implements
		OnCheckedChangeListener {
	@SuppressWarnings("unused")
	private FrameLayout frameContent;
	private FragmentPost fragmentPost;
	private FragmentSetting fragmentSetting;
	public static FragmentMesageHistory mesageHistory;
	public static FragmentTwitter fragmentTwitter;
	public static Fragment fragmentCurrent;
	public static FragmentSMS fragmentSMS;
	RadioGroup radioGroup;
	// Shared Preferences
	private static SharedPreferences mSharedPreferences;
	public final static String TAB = "tab";
	private final static int POST_TAB = 1;
	private final static int TWITTER_TAB = 2;
	private final static int TEXT_TAB = 3;
	private final static int SETTING_TAB = 4;
	public final static String SHARED_PREFS_FILE = "save list post id hided";
	public final static String LIST_POST_ID_HIDED = "listPostIdHide";

	// private Button btnAdd, btnBack;
	// private boolean isListContact = false;
	private static WeakReference<MainActivity> wrActivity = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		wrActivity = new WeakReference<MainActivity>(this);
		setContentView(R.layout.activity_main);
		radioGroup = (RadioGroup) findViewById(R.id.radioGroupMenu);
		radioGroup.setOnCheckedChangeListener(this);
		frameContent = (FrameLayout) findViewById(R.id.frlContent);
		mSharedPreferences = getSharedPreferences("myShared", 0);

		switch (mSharedPreferences.getInt(TAB, 1)) {
		case POST_TAB:
			onCheckedChanged(radioGroup, R.id.btnPostTab);
			break;
		case TWITTER_TAB:
			onCheckedChanged(radioGroup, R.id.btnTweetTab);
			break;
		case TEXT_TAB:
			onCheckedChanged(radioGroup, R.id.btnTextTab);
			break;
		case SETTING_TAB:
			onCheckedChanged(radioGroup, R.id.btnSettingTab);
			break;
		default:
			break;
		}
	}

	public void hideTaskBar() {
		radioGroup.setVisibility(View.GONE);
	}

	public void showTaskBar() {
		radioGroup.setVisibility(View.VISIBLE);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Editor e = mSharedPreferences.edit();
		e.remove(TAB);
		e.commit();
		super.onBackPressed();
	}

	@Override
	protected void onResume() {
		Utils.hideKeyboard(this);
		super.onResume();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		Utils.hideKeyboard(this);
		FragmentTransaction fragTransaction;
		if ((wrActivity.get() != null)
				&& (wrActivity.get().isFinishing() != true)) {
			fragTransaction = wrActivity.get().getSupportFragmentManager()
					.beginTransaction();
		} else {
			fragTransaction = getSupportFragmentManager().beginTransaction();
		}
		Editor e = mSharedPreferences.edit();
		if (fragmentCurrent != null) {
			fragTransaction.hide(fragmentCurrent);
		}
		switch (checkedId) {
		case R.id.btnPostTab:
			e.putInt(TAB, POST_TAB);
			if (fragmentPost == null) {
				fragmentPost = new FragmentPost();
				fragTransaction.add(R.id.frlContent, fragmentPost, "facebook");
			} else {
				fragTransaction.show(fragmentPost);
			}
			fragmentCurrent = fragmentPost;
			RadioButton btnPostTab = (RadioButton) findViewById(R.id.btnPostTab);
			btnPostTab.setChecked(true);
			break;
		case R.id.btnSettingTab:
			e.putInt(TAB, SETTING_TAB);
			if (fragmentSetting == null) {
				fragmentSetting = new FragmentSetting();
				fragTransaction.add(R.id.frlContent, fragmentSetting, "setting");
			} else {
				fragmentSetting.setBesafeTipSwitch();
				fragmentSetting.setBesafeTwitterSwitch();
				fragmentSetting.refreshListAccount();
				fragTransaction.show(fragmentSetting);
			}
			fragmentCurrent = fragmentSetting;
			RadioButton btnSettingTab = (RadioButton) findViewById(R.id.btnSettingTab);
			btnSettingTab.setChecked(true);
			showTaskBar();
			break;
		case R.id.btnTextTab:
			e.putInt(TAB, TEXT_TAB);
//			if (fragmentText == null) {
//				fragmentText = new FragmentText();
//				fragTransaction.add(R.id.frlContent, fragmentText, "home");
//			} else {
//				fragTransaction.show(fragmentText);
//			}
//			fragmentCurrent = fragmentText;
			if (mesageHistory == null) {
				mesageHistory = new FragmentMesageHistory();
				fragTransaction.add(R.id.frlContent, mesageHistory, "message");
			} else {
				fragTransaction.show(mesageHistory);
			}
			fragmentCurrent = mesageHistory;
			RadioButton btnTextTab = (RadioButton) findViewById(R.id.btnTextTab);
			btnTextTab.setChecked(true);
			break;
		case R.id.btnTweetTab:
			e.putInt(TAB, TWITTER_TAB);
			if (fragmentTwitter == null) {
				fragmentTwitter = new FragmentTwitter();
				fragTransaction.add(R.id.frlContent, fragmentTwitter, "twitter");
			} else {
				fragTransaction.show(fragmentTwitter);
			}
			fragmentCurrent = fragmentTwitter;

			RadioButton btnTweetTab = (RadioButton) findViewById(R.id.btnTweetTab);
			btnTweetTab.setChecked(true);
			break;

		default:
			break;
		}
		e.commit();
		fragTransaction.commit();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		fragmentPost = null;
		fragmentSetting = null;
		mesageHistory = null;
		fragmentSMS = null;
		fragmentTwitter = null;
		// save the task list to preference
		
		if (FragmentPost.listPostIdHide != null) {

			SharedPreferences prefs = getSharedPreferences(SHARED_PREFS_FILE,
					Context.MODE_PRIVATE);
			Editor editor = prefs.edit();
			try {
				editor.putString(LIST_POST_ID_HIDED,
						ObjectSerializer.serialize(FragmentPost.listPostIdHide));
			} catch (IOException e) {
				e.printStackTrace();
			}
			editor.commit();
		}

		// delete the post
		for (int i = 0; i < FragmentPost.listPostIdDel.size(); i++) {
			FacebookUtils.delete(FragmentPost.listPostIdDel.get(i));
		}
		super.onDestroy();
	}

}
