package com.example.besafepost.activity;

import java.util.ArrayList;
import java.util.List;

import twitter4j.IDs;
import twitter4j.TwitterException;
import twitter4j.User;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.besafepost.fragment.FragmentTwitter;
import com.example.besafepost.task.DownloadImageTask;
import com.example.besafepost.utils.DialogUtils;

public class DirectMessageActivity extends Activity {
	private Button btnCancel;
	private ListView lvFollow;

	private ListFollow mListFollowAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twitter_direct_msg);

		btnCancel = (Button) findViewById(R.id.btnCancelDirectMsg);
		lvFollow = (ListView) findViewById(R.id.lvFollow);
		new GetFollow().execute();

		btnCancel.setOnClickListener(onClick);
		lvFollow.setOnItemClickListener(onItemClick);
	}

	OnClickListener onClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			finish();
		}
	};

	OnItemClickListener onItemClick = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> view, View arg1, int position,
				long arg3) {
			User user = (User) view.getAdapter().getItem(position);
			Intent intent = new Intent(DirectMessageActivity.this,
					NewTweetActivity.class);
			intent.putExtra("sendTo", user.getScreenName());
			intent.putExtra("title", "New Message");
			intent.putExtra("size", 15);
			intent.putExtra("scrName", user.getScreenName());
			intent.putExtra("avatar", user.getOriginalProfileImageURLHttps());
			intent.putExtra("drMsg", true);
			startActivity(intent);
			finish();
		}
	};

	private class GetFollow extends AsyncTask<Void, Void, List<User>> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			DialogUtils.showProgressDialog(DirectMessageActivity.this, "",
					"Loading...", false);
		}

		@Override
		protected List<User> doInBackground(Void... params) {
			List<User> listFollow = new ArrayList<User>();
			try {
				// Get the follower list
				IDs followerIds = FragmentTwitter.twitter.getFollowersIDs(-1);
				// IDs followingIds = FragmentTwitter.twitter.getFriendsIDs(-1);
				long[] ids = followerIds.getIDs();
				// long [] ids = followingIds.getIDs();
				for (int i = 0; i < ids.length; i++) {
					listFollow.add(FragmentTwitter.twitter.showUser(ids[i]));
				}
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return listFollow;
		}

		@Override
		protected void onPostExecute(List<User> result) {
			super.onPostExecute(result);
			DialogUtils.dismissDialog();
			mListFollowAdapter = new ListFollow(getApplicationContext(),
					R.layout.row_list_follow, result);
			lvFollow.setAdapter(mListFollowAdapter);
		}
	}

	private class ListFollow extends ArrayAdapter<User> {
		LayoutInflater inflater;
		private int textViewResourceId;
		private List<User> rows;

		public ListFollow(Context context, int textViewResourceId,
				List<User> followers) {
			super(context, textViewResourceId, followers);
			// TODO Auto-generated constructor stub
			this.textViewResourceId = textViewResourceId;
			this.rows = followers;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View row;
			row = inflater.inflate(textViewResourceId, parent, false);

			ImageView iconFollow = (ImageView) row
					.findViewById(R.id.iconFollow);
			TextView nameFollow = (TextView) row.findViewById(R.id.nameFollow);

			User data = rows.get(position);
			// Display followers' image profile
			new DownloadImageTask(iconFollow).execute(data.getProfileImageURL().toString());
//			
//			Object content = null;
//			try {
//				URL url = new URL(data.getProfileImageURL().toString());
//				content = url.getContent();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			InputStream is = (InputStream) content;
//			Drawable image = Drawable.createFromStream(is, "src");
//			iconFollow.setImageDrawable(image);

			nameFollow.setText(data.getName() + " (@" + data.getScreenName()
					+ ")");

			return row;
		}

	}
}
