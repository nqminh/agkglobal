package com.example.besafepost.activity;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

public class PlayVideoYoutubeActivity extends Activity {
	private VideoView video;
	private ProgressBar prog;
	private MediaController mediaController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		setContentView(R.layout.activity_play_video);

		video = (VideoView) findViewById(R.id.video);
		prog = (ProgressBar) findViewById(R.id.prog);
		findViewById(R.id.btnDone).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		String video_url = getIntent().getExtras().getString("uploadvideolink");
		Log.i("url", "aaaaaaa" + video_url);
		//		video_url = "rtsp://r6---sn-a5m7zu7d.c.youtube.com/CiILENy73wIaGQkSo7tyw70wHhMYDSANFEgGUgZ2aWRlb3MM/0/0/0/video.3gp";
		new getVideoUrlStream().execute(video_url);
		video.setOnErrorListener(new OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Error occured", 500)
						.show();
				return false;
			}
		});

		video.setOnPreparedListener(new OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer arg0) {
				prog.setVisibility(View.GONE);
				video.start();
			}
		});

	}

	@Override
	protected void onDestroy() {
		try {
			video.stopPlayback();
		} catch (Exception e) {
			//
		}
		super.onDestroy();
	}

	class getVideoUrlStream extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			String videoUrlStream = "";
			videoUrlStream = getUrlVideoRTSP(params[0]);
			return videoUrlStream;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			String video_url = result;
			Log.i("rtsp", "aaaaaaaa" + video_url);
			Uri video_uri = Uri.parse(video_url);
			mediaController = new MediaController(PlayVideoYoutubeActivity.this);
			mediaController.setAnchorView(video);
			video.setMediaController(mediaController);
			video.setVideoURI(video_uri);

		}

	}

	public String getUrlVideoRTSP(String urlYoutube) {
		try {
			String gdy = "http://gdata.youtube.com/feeds/api/videos/";
			DocumentBuilder documentBuilder = DocumentBuilderFactory
					.newInstance().newDocumentBuilder();
			String id = extractYoutubeId(urlYoutube);
			URL url = new URL(gdy + id);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			Document doc = documentBuilder.parse(connection.getInputStream());
			Element el = doc.getDocumentElement();
			NodeList list = el.getElementsByTagName("media:content");///media:content
			String cursor = urlYoutube;
			for (int i = 0; i < list.getLength(); i++) {
				Node node = list.item(i);
				if (node != null) {
					NamedNodeMap nodeMap = node.getAttributes();
					HashMap<String, String> maps = new HashMap<String, String>();
					for (int j = 0; j < nodeMap.getLength(); j++) {
						Attr att = (Attr) nodeMap.item(j);
						maps.put(att.getName(), att.getValue());
					}
					if (maps.containsKey("yt:format")) {
						String f = maps.get("yt:format");
						if (maps.containsKey("url")) {
							cursor = maps.get("url");
						}
						if (f.equals("1"))
							return cursor;
					}
				}
			}
			return cursor;
		} catch (Exception ex) {
			Log.e("Get Url Video RTSP Exception======>>", ex.toString());
		}
		return urlYoutube;

	}

	protected String extractYoutubeId(String url) throws MalformedURLException {
		url = url.replace("embed/", "watch?v=");
		String id = null;
		try {
			String query = new URL(url).getQuery();
			if (query != null) {
				String[] param = query.split("&");
				for (String row : param) {
					String[] param1 = row.split("=");
					if (param1[0].equals("v")) {
						id = param1[1];
					}
				}
			} else {
				if (url.contains("embed")) {
					id = url.substring(url.lastIndexOf("/") + 1);
				}
			}
		} catch (Exception ex) {
			Log.e("Exception", ex.toString());
		}
		return id;
	}
}
