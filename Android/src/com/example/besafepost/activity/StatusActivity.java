package com.example.besafepost.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.besafepost.adapter.EntryAdapter;
import com.example.besafepost.interfaces.IDeItemFriendList;
import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.model.AlbumStorageDirFactory;
import com.example.besafepost.model.BaseAlbumDirFactory;
import com.example.besafepost.model.EntryItemFriendList;
import com.example.besafepost.model.FroyoAlbumDirFactory;
import com.example.besafepost.model.ItemFriendList;
import com.example.besafepost.model.SectionItemFriendList;
import com.example.besafepost.utils.ConnectionDetector;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.FacebookUtils;
import com.example.besafepost.utils.Log;
import com.example.besafepost.utils.StringUtils;
import com.example.besafepost.utils.Utils;
import com.facebook.android.Facebook;

public class StatusActivity extends Activity implements OnClickListener, IActivityDelegate,
		OnItemClickListener {

	private Button btnCancel, btnPost;
	private EditText edtStatus;
	Facebook mFacebook;
	// private static final String APP_ID = "328711320597060";
	String[] permission = { "publish_stream", "offline_access", "user_photos", "photo_upload" };
	private ConnectionDetector cd;
	private Boolean hasNetwork = false;
	private long flId = 1;// friend list id
	private SharedPreferences delayPrefs;
	private View imgShowListFriend, tvDistance, imgCheckIn;
	private ImageView imgIconPrivacy, imgPhoto;
	private ListView lvFriendList;
	private ArrayList<ItemFriendList> listFriendList = new ArrayList<ItemFriendList>();
	private ArrayList<IDeItemFriendList> items = new ArrayList<IDeItemFriendList>();
	private final String photoItems[] = { "Choose Existing", "Take Photo" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_status);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnPost = (Button) findViewById(R.id.btnPost);
		edtStatus = (EditText) findViewById(R.id.edtStatus);
		Utils.showKeyboard(this, edtStatus);

		imgCheckIn = findViewById(R.id.imgIconAddress);
		imgCheckIn.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		btnPost.setOnClickListener(this);
		imgIconPrivacy = (ImageView) findViewById(R.id.imgIconPrivacy);
		imgPhoto = (ImageView) findViewById(R.id.ivChooseCam);
		imgPhoto.setOnClickListener(this);
		imgShowListFriend = findViewById(R.id.imgShowListFriend);
		imgShowListFriend.setOnClickListener(this);
		lvFriendList = (ListView) findViewById(R.id.lvFriendList);
		tvDistance = findViewById(R.id.tvDistance);
		edtStatus.setOnClickListener(this);
		delayPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		FacebookUtils.getListFriendList(this);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			mAlbumStorageDirFactory = new BaseAlbumDirFactory();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgIconAddress:
			Intent i = new Intent(StatusActivity.this, CheckInActivity.class);
			i.putExtra("text_post", edtStatus.getText().toString());
			startActivity(i);
			finish();
			break;
		case R.id.btnCancel:
			finish();
			break;

		case R.id.btnPost:
			cd = new ConnectionDetector(getApplicationContext());
			hasNetwork = cd.isConnectingToInternet();
			if (Utils.isBlockedAddress(edtStatus.getText().toString())
					|| Utils.isBlockedWord(edtStatus.getText().toString())) {
				DialogUtils.showDialogBannedWord(StatusActivity.this,
						Utils.isBlockedWord(edtStatus.getText().toString()),
						Utils.isBlockedAddress(edtStatus.getText().toString()));
			} else {
				if (hasNetwork) {
					if (StringUtils.isEmptyString(edtStatus.getText().toString())) {
						Toast.makeText(getApplicationContext(), "Status field is empty!",
								Toast.LENGTH_SHORT).show();
					} else {
						if (delayPrefs.getInt("Delay", 10) == 0) {
							updateStatus();
						} else {
							Intent intent = new Intent(StatusActivity.this, CountDownActivity.class);
							intent.putExtra("scrName", "Timeline");
							intent.putExtra("msgContent", edtStatus.getText().toString());
							intent.putExtra("isUseMyProfileFb", true);
							intent.putExtra("FromFB", true);
							startActivityForResult(intent, Utils.START_COUNTDOWN_FOR_POST_FB_STATUS);
						}
					}
				} else {
					Toast.makeText(getApplicationContext(), "Network problem!", Toast.LENGTH_LONG)
							.show();
				}
			}
			break;
		case R.id.imgShowListFriend:
			lvFriendList.setVisibility(View.VISIBLE);
			tvDistance.setVisibility(View.GONE);
			Utils.hideKeyboard(StatusActivity.this);
			break;
		case R.id.edtStatus:
			lvFriendList.setVisibility(View.GONE);
			tvDistance.setVisibility(View.VISIBLE);
			break;
		case R.id.ivChooseCam:
			callDialog();
			break;
		default:
			break;
		}
	}

	private void updateStatus() {
		DialogUtils.showProgressDialog(StatusActivity.this, "", "Updating status...", false);
		FacebookUtils.postToWall(edtStatus.getText().toString(), StatusActivity.this, flId);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Utils.showKeyboard(StatusActivity.this, edtStatus);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == Utils.START_COUNTDOWN_FOR_POST_FB_STATUS) {
			if (resultCode == RESULT_OK) {
				updateStatus();
			}
		} else if (requestCode == Utils.PICK_EXISTING_PHOTO_RESULT_CODE) {
			if (resultCode == Utils.PHOTO_RESULT_CODE) {
				path = data.getStringExtra("bitmap");
				new ImageLoaderTask().execute();
			}
		} else if (requestCode == Utils.TAKE_PICTURE) {
			if (resultCode == Activity.RESULT_OK) {
				handleBigCameraPhoto();
				new ImageCompressTask().execute();
			} else {
				Toast.makeText(getApplicationContext(), "Photo not taken", Toast.LENGTH_LONG)
						.show();
			}
		} else if (requestCode == Utils.REQUEST_CALL_PHOTO_ACT) {
			if (resultCode == RESULT_OK) {
				setResult(RESULT_OK);
			} else {
				setResult(RESULT_CANCELED);
			}
			finish();
		}
	}

	@Override
	public void onGetSuccess(String requestCode) {
		if (requestCode.equalsIgnoreCase(Utils.REQUEST_GET_LIST_FRIENDLIST)) {
			listFriendList = FacebookUtils.getListFriendList();
			setListFriendList();
			// get my profile
			FacebookUtils.getSrcImgProfile();
		}
		if (requestCode == Utils.REQUEST_POST_STATUS) {
			setResult(RESULT_OK);
			DialogUtils.dismissDialog();
			finish();
		}
	}

	@Override
	public void onGetFailure(String requestCode) {
		if (requestCode == Utils.REQUEST_POST_STATUS) {
			DialogUtils.dismissDialog();
			Toast.makeText(this, "post status not successful", Toast.LENGTH_LONG).show();
		}
	}

	private void setListFriendList() {

		items.add(new SectionItemFriendList("Audience"));
		items.add(new EntryItemFriendList("Public", R.drawable.icon_public));
		items.add(new EntryItemFriendList("Friends", R.drawable.icon_friend));
		items.add(new EntryItemFriendList("Only Me", R.drawable.icon_only_me));

		items.add(new SectionItemFriendList("Friend Lists"));
		for (int i = 0; i < listFriendList.size(); i++) {
			items.add(new EntryItemFriendList(listFriendList.get(i).getNameFriendList(),
					R.drawable.icon_friend_list));
		}
		EntryAdapter adapter = new EntryAdapter(this, 0, items);
		adapter.setIndexSelected(1);
		lvFriendList.setAdapter(adapter);
		lvFriendList.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> lv, View arg1, int position, long arg3) {
		if (lv == lvFriendList) {
			lvFriendList.setVisibility(View.GONE);
			tvDistance.setVisibility(View.VISIBLE);
			((EntryAdapter) lvFriendList.getAdapter()).setIndexSelected(position);
			((EntryAdapter) lvFriendList.getAdapter()).notifyDataSetChanged();
			Utils.showKeyboard(StatusActivity.this, edtStatus);
			flId = position;
			if (position == 1) {
				imgIconPrivacy.setImageResource(R.drawable.icon_public);
			}
			if (position == 2) {
				imgIconPrivacy.setImageResource(R.drawable.icon_friend);
			}
			if (position == 3) {
				imgIconPrivacy.setImageResource(R.drawable.icon_only_me);
			}
			if (position > 4) {
				flId = listFriendList.get(position - 5).getIdFriendList();
				imgIconPrivacy.setImageResource(R.drawable.icon_friend_list);
			}
		}
	}

	private AlertDialog.Builder builder;
	private String mCurrentPhotoPath;
	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
	private String path;
	private Bitmap capturedBitmap;
	private Bitmap bitmap;

	private void callDialog() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.select_dialog_item, photoItems);
		builder = new AlertDialog.Builder(StatusActivity.this);
		builder.setTitle("Photo source");
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (item == 0) { // Choose Existing
					if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
						Toast.makeText(getApplicationContext(), "SDCard is not mounted!",
								Toast.LENGTH_SHORT).show();
					} else {
						Intent intent = new Intent(StatusActivity.this, SavedPhotosActivity.class);
						startActivityForResult(intent, Utils.PICK_EXISTING_PHOTO_RESULT_CODE);
					}

				} else { // Take Photo
					Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
					File f = null;
					try {
						f = setUpPhotoFile();
						mCurrentPhotoPath = f.getAbsolutePath();
						intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
					} catch (IOException e) {
						e.printStackTrace();
						f = null;
						mCurrentPhotoPath = null;
					}

					startActivityForResult(intent, Utils.TAKE_PICTURE);
				}
			}
		});
		builder.setCancelable(true);
		builder.create().show();
	}

	private File setUpPhotoFile() throws IOException {

		File f = createImageFile();
		mCurrentPhotoPath = f.getAbsolutePath();

		return f;
	}

	@SuppressLint("SimpleDateFormat")
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "IMG_" + timeStamp + "_";
		File albumF = getAlbumDir();
		File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
		return imageF;
	}

	private File getAlbumDir() {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

			storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

			if (storageDir != null) {
				if (!storageDir.mkdirs()) {
					if (!storageDir.exists()) {
						Log.d("CameraSample", "failed to create directory");
						return null;
					}
				}
			}

		} else {
			Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
		}

		return storageDir;
	}

	/* Photo album for this application */
	private String getAlbumName() {
		return getString(R.string.album_name);
	}

	private void handleBigCameraPhoto() {

		if (mCurrentPhotoPath != null) {
			setPic();
			galleryAddPic();
			mCurrentPhotoPath = null;
		}

	}

	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

	public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight, Context context) {
		final float densityMultiplier = context.getResources().getDisplayMetrics().density;
		int h = (int) (newHeight * densityMultiplier);
		int w = (int) (h * photo.getWidth() / ((double) photo.getHeight()));

		photo = Bitmap.createScaledBitmap(photo, w, h, true);

		return photo;
	}

	private void setPic() {

		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
		int targetW = 300;
		int targetH = 400;

		/* Get the size of the image */
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
		int photoW = Math.min(bmOptions.outWidth, 2048);
		int photoH = Math.min(bmOptions.outHeight, 2048);

		/* Figure out which way needs to be reduced less */
		int scaleFactor = 1;
		if ((targetW > 0) || (targetH > 0)) {
			scaleFactor = Math.min(photoW / targetW, photoH / targetH);
		}

		/* Set bitmap options to scale the image decode target */
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
		Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

		/* Associate the Bitmap to the ImageView */
		// ivAdjust.setImageBitmap(bitmap);

		/* Get the captured image */
		capturedBitmap = bitmap;
	}

	private class ImageLoaderTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			DialogUtils.showProgressDialog(StatusActivity.this, "", "Please wait...", false);
		}

		@Override
		protected Boolean doInBackground(Void... voids) {
			/*
			 * There isn't enough memory to open up more than a couple camera
			 * photos
			 */
			/* So pre-scale the target bitmap into which the file is decoded */

			/* Get the size of the ImageView */
			int targetW = 300;
			int targetH = 400;

			/* Get the size of the image */
			BitmapFactory.Options bmOptions = new BitmapFactory.Options();
			bmOptions.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path, bmOptions);
			int photoW = Math.min(bmOptions.outWidth, 1024);
			int photoH = Math.min(bmOptions.outHeight, 1024);

			/* Figure out which way needs to be reduced less */
			int scaleFactor = 1;
			if ((targetW > 0) || (targetH > 0)) {
				scaleFactor = Math.min(photoW / targetW, photoH / targetH);
			}

			/* Set bitmap options to scale the image decode target */
			bmOptions.inJustDecodeBounds = false;
			bmOptions.inSampleSize = scaleFactor;
			bmOptions.inPurgeable = true;

			/* Decode the JPEG file into a Bitmap */
			bitmap = BitmapFactory.decodeFile(path, bmOptions);
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			DialogUtils.dismissDialog();
			doneLoadImage(bitmap);
		}
	}

	private class ImageCompressTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			DialogUtils.showProgressDialog(StatusActivity.this, "", "Please wait...", false);
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			if (capturedBitmap.getHeight() > 500) {
				// Log.i("IMG CAPTURED HEIGHT, WIDTH",
				// "" + capturedBitmap.getHeight() + ","
				// + capturedBitmap.getWidth());
				capturedBitmap = scaleDownBitmap(capturedBitmap, 300, getApplicationContext());
				// Log.i("HEIGHT, WIDTH", "" + capturedBitmap.getHeight() + ","
				// + capturedBitmap.getWidth());
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			DialogUtils.dismissDialog();
			if (result) {
				doneLoadImage(capturedBitmap);
			} else {
				Toast.makeText(getApplicationContext(), "Sorry! Please try again",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	private void doneLoadImage(Bitmap bitmap) {
		Intent intent = new Intent(StatusActivity.this, PhotoActivity.class);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte bytes[] = baos.toByteArray();
		String status = edtStatus.getText().toString();
		intent.putExtra("text_post", status);
		intent.putExtra("bitmap_image", bytes);
		startActivityForResult(intent, Utils.REQUEST_CALL_PHOTO_ACT);
	}
}
