package com.example.besafepost.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.besafepost.BesafePostApplication;
import com.example.besafepost.interfaces.IdeResumeFromHome;
import com.example.besafepost.task.DownloadImageTask;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.FacebookUtils;
import com.example.besafepost.utils.StringUtils;

public class CountDownActivity extends Activity implements OnClickListener,
		IdeResumeFromHome {
	private TextView tvPreSendTo;
	private TextView tvCntDwnTime;
	private TextView tvMsgContent;
	private Button btnStopSending;
	private ImageView imgCont;

	private int startCountDownFrom;
	private String scrName;
	private String avatarSrc;
	private String msgContent;
	private MediaPlayer mp;
	private CountDownTimer waitTimer;
	private Bitmap bitmap;
	private SharedPreferences delayPrefs;
	private boolean isCallFromHomeScreen = false;
	private boolean isUseMyProfileFb;
	private boolean isFromFB;
	private boolean isRetweet;
	private String THIS_IS_RECEIVER = "THIS_IS_RECEIVER";
	private String THIS_IS_ACTION = "THIS_IS_ACTION";
	private String action;
	private byte[] bytes;
	Vibrator v;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		setContentView(R.layout.activity_twitter_prepare_msg);

		tvPreSendTo = (TextView) findViewById(R.id.tvPreSendTo);
		tvCntDwnTime = (TextView) findViewById(R.id.tvCntDwnTime);
		tvMsgContent = (TextView) findViewById(R.id.tvMsgContent);
		btnStopSending = (Button) findViewById(R.id.btnStopSending);
		imgCont = (ImageView) findViewById(R.id.imgCont);

		getAllIntent();
		if (scrName.equalsIgnoreCase("Timeline")) {
			tvPreSendTo.setText("Post to wall:");
		} else {
			tvPreSendTo.setText(scrName);
		}
		tvMsgContent.setText(msgContent);
		if (isUseMyProfileFb) {
			if (FacebookUtils.bmp != null) {
				imgCont.setImageBitmap(FacebookUtils.bmp);
			}
		} else if (bytes != null) {
			bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
			imgCont.setImageBitmap(bitmap);
		} else if (avatarSrc != null) {
			new DownloadImageTask(imgCont).execute(avatarSrc);
		}
		delayPrefs = PreferenceManager.getDefaultSharedPreferences(this);

		if (isFromFB) {
			btnStopSending.setBackgroundResource(R.drawable.btn_stop_posting);
		} else {
			btnStopSending.setBackgroundResource(R.drawable.btn_stop_sending);
		}
		btnStopSending.setOnClickListener(this);
		mp = MediaPlayer.create(this, R.raw.full10);
		if (delayPrefs.getInt(StringUtils.KEY_DELAY, 10) == 10) {
			startCountDownFrom = 10;
			setCountDownTime(11000, 1000);

		} else if (delayPrefs.getInt(StringUtils.KEY_DELAY, 10) == 20) {
			startCountDownFrom = 20;
			setCountDownTime(21000, 1000);

		} else if (delayPrefs.getInt(StringUtils.KEY_DELAY, 10) == 30) {
			startCountDownFrom = 30;
			setCountDownTime(31000, 1000);

		} else if (delayPrefs.getInt(StringUtils.KEY_DELAY, 10) == 60) {
			startCountDownFrom = 60;
			setCountDownTime(61000, 1000);
		}
	}

	private void getAllIntent() {
		Intent intent = getIntent();
		scrName = intent.getStringExtra("scrName");
		//default,send a string src, set is image icon
		avatarSrc = intent.getStringExtra("avatarSrc");
		//use my profile facebook is image icon
		isUseMyProfileFb = intent.getBooleanExtra("isUseMyProfileFb", false);
		msgContent = intent.getStringExtra("msgContent");
		//set image icon is image taken
		bytes = intent.getByteArrayExtra("image taken");
		//use to set button "STOP" background
		isFromFB = intent.getBooleanExtra("FromFB", false);
		isRetweet = intent.getBooleanExtra("isRetweet", false);
	}

	@Override
	public void onClick(View v) {
		if (v == btnStopSending) {
			setResult(RESULT_CANCELED);
			actionFinish();
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		setResult(RESULT_CANCELED);
		actionFinish();
	}

	private void actionFinish() {
		if (waitTimer != null) {
			waitTimer.cancel();
		}
		if (mp != null && mp.isPlaying()) {
			mp.release();
		}
		finish();
	}

	private void setCountDownTime(long sumTime, long elementTime) {
		waitTimer = new CountDownTimer(sumTime, elementTime) {

			@Override
			public void onTick(long millisUntilFinished) {
				if (delayPrefs.getBoolean(StringUtils.KEY_COUNT_DOWN, true)) {
					if (startCountDownFrom == 10 && waitTimer != null) {
						mp.start();
					}
				} 
				v.vibrate(500);
				tvCntDwnTime.setText(String.valueOf(startCountDownFrom--));
			}

			@Override
			public void onFinish() {
				if (BesafePostApplication.isActivityVisible()) {
					setResult(RESULT_OK);
					actionFinish();
				} else {
					isCallFromHomeScreen = true;
					Intent i = new Intent(CountDownActivity.this,
							DialogActivity.class);
					i.putExtra("isCalledFromCountdown", true);
					if (isRetweet) {
						action = "Retweet";
					} else if (isFromFB) {
						action = "Post";
					} else {
						action = "Tweet";
					}
					i.putExtra("action", action);
					i.putExtra("isFromFb", isFromFB);
					startActivity(i);
				}

			}
		}.start();
	}

	@Override
	protected void onResume() {
		super.onResume();
		BesafePostApplication.activityResumed();
		if (isCallFromHomeScreen) {

			String notify = getResources().getString(
					R.string.notice_app_restarted_on_countdown);
			notify = notify.replace(THIS_IS_ACTION, action);
			notify = notify.replace(THIS_IS_RECEIVER, scrName);
			DialogUtils.showDialogWhenResumeFromHome(CountDownActivity.this,
					CountDownActivity.this, isFromFB, "", notify);
		}
		isCallFromHomeScreen = false;
	}

	@Override
	protected void onPause() {
		super.onPause();
		BesafePostApplication.activityPaused();
	}

	@Override
	public void sendNow() {
		setResult(RESULT_OK);
		actionFinish();
	}

	@Override
	public void stop() {
		actionFinish();
	}

}
