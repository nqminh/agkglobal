package com.example.besafepost.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.besafepost.utils.DialogUtils;

public class OpenLinkActivity extends Activity {
	private WebView webView;
	String url;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.open_link_activity);
		findViewById(R.id.btnBack).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		DialogUtils.showProgressDialog(this, "", "Loading...", false);
		url = getIntent().getStringExtra("url");
		webView = (WebView) findViewById(R.id.webViewOpenLinkOnTweet);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebChromeClient(new WebChromeClient());
		webView.setWebViewClient(new MyWebViewClient());

		webView.loadUrl(url);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		webView.destroy();
	}

	private class MyWebViewClient extends WebViewClient {

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			DialogUtils.dismissDialog();
		}
	}
}
