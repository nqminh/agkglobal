package com.example.besafepost.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.besafepost.BesafePostApplication;
import com.example.besafepost.interfaces.IdeResumeFromHome;
import com.example.besafepost.utils.CountDownTimerWithPause;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.StringUtils;

public class TipActivity extends Activity implements OnClickListener, IdeResumeFromHome {

	private TextView tvNotice;
	private Button actionAnyway;
	private Button cancelBesafeTip;
	private Button moreInfo;
	private Button hideWarningDialog;
	private TextView tvCntDwn;
	private ImageView ivPauseResume;
	private boolean isPlaying = true;
	private int seek;
	private MediaPlayer mp;
	private CountDownTimerWithPause waitTimer;
	private String action;
	private boolean isTipOfFb;
	private Editor editor;
	private int startCountDownFrom;
	private SharedPreferences pref;
	private boolean iseExecuteWhenTurnOff;
	private boolean isCallFromHomeScreen = false;
	private String THIS_IS_ACTION = "THIS_IS_ACTION";
	Vibrator v;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tip_activity);
		getAllIntent();
		if (action != null) {
			initView();
			pref = PreferenceManager.getDefaultSharedPreferences(this);
			editor = pref.edit();
			startCountDownFrom = pref.getInt(StringUtils.KEY_DELAY, 10);
			setCountDownTime((startCountDownFrom + 1) * 1000, 1000);
		} else {
			finish();
		}
	}

	private void getAllIntent() {
		Intent intent = getIntent();
		action = intent.getStringExtra("action");
		isTipOfFb = intent.getBooleanExtra("isTipOfFb", true);
		iseExecuteWhenTurnOff = intent.getBooleanExtra("iseExecuteWhenTurnOff", false);
	}

	private void initView() {
		tvNotice = (TextView) findViewById(R.id.tv_notice);
		if (action.equalsIgnoreCase("like")) {
			tvNotice.setText(R.string.like_share_warning);
		} else if (action.equalsIgnoreCase("check In")) {
			tvNotice.setText(R.string.checkIn_warning);
		} else if (action.equalsIgnoreCase("share")) {
			tvNotice.setText(R.string.share_warning);
		} else if (action.equalsIgnoreCase("favorite")) {
			tvNotice.setText(R.string.favorite_warning);
		} else if (action.equalsIgnoreCase("retweet")) {
			tvNotice.setText(R.string.retweet_warning);
		}
		tvCntDwn = (TextView) findViewById(R.id.tvCntDwn);
		ivPauseResume = (ImageView) findViewById(R.id.ivPauseResume);
		ivPauseResume.setOnClickListener(this);
		actionAnyway = (Button) findViewById(R.id.btn_action);
		actionAnyway.setText(action + " Anyway");
		actionAnyway.setOnClickListener(this);
		cancelBesafeTip = (Button) findViewById(R.id.btn_cancel);
		cancelBesafeTip.setOnClickListener(this);
		moreInfo = (Button) findViewById(R.id.btn_more_info);
		moreInfo.setOnClickListener(this);
		hideWarningDialog = (Button) findViewById(R.id.btn_hide);
		hideWarningDialog.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v == ivPauseResume) {
			if (isPlaying) {
				if (mp != null && mp.isPlaying()) {
					mp.pause();
					seek = mp.getCurrentPosition();
				}
				if (waitTimer != null) {
					waitTimer.pause();
					ivPauseResume.setBackgroundResource(R.drawable.play_icon);
				}
			} else {
				if (mp != null && startCountDownFrom < 10) {
					mp.seekTo(seek);
					mp.start();
				}
				if (waitTimer != null) {
					waitTimer.resume();
					ivPauseResume.setBackgroundResource(R.drawable.pause_icon);
				}
			}
			isPlaying = !isPlaying;
		}
		if (v == cancelBesafeTip) {
			actionFinish();
		}
		if (v == moreInfo) {
			Intent myIntent = new Intent(TipActivity.this, MoreInfoActivity.class);
			if (isTipOfFb) {
				if (action.equalsIgnoreCase("like")) {
					myIntent.putExtra("MoreInfo", "BeSafe Post");
				} else if (action.equalsIgnoreCase("share")) {
					myIntent.putExtra("MoreInfo", "Share");
				} else if (action.equalsIgnoreCase("check in")) {
					myIntent.putExtra("MoreInfo", "Status");
				}
			} else {
				myIntent.putExtra("MoreInfo", "Tweet");
			}
			startActivity(myIntent);
			actionFinish();

		}
		if (v == actionAnyway) {
			setResult(RESULT_OK);
			actionFinish();
		}
		if (v == hideWarningDialog) {
			if (isTipOfFb) {
				editor.putBoolean(StringUtils.KEY_POST_TIP, false);
			} else {
				editor.putBoolean(StringUtils.KEY_TWEET_TIP, false);
			}
			if (iseExecuteWhenTurnOff) {
				setResult(RESULT_OK);
			}
			editor.commit();
			actionFinish();

		}
	}

	private void actionFinish() {
		if (mp != null && mp.isPlaying()) {
			mp.release();
		}
		if (waitTimer != null) {
			waitTimer.cancel();
		}
		waitTimer = null;
		finish();
	}

	private void setCountDownTime(long sumTime, long elementTime) {
		mp = MediaPlayer.create(this, R.raw.full10);
		waitTimer = new CountDownTimerWithPause(sumTime, elementTime, true) {
			@Override
			public void onTick(long millisUntilFinished) {
				if (startCountDownFrom == 0) {
					tvCntDwn.setVisibility(View.INVISIBLE);
				}
				tvCntDwn.setText(String.valueOf(startCountDownFrom--));
				if (pref.getBoolean(StringUtils.KEY_COUNT_DOWN, true)) {
					if (startCountDownFrom == 9) {
						mp.start();
					}
				}
				v.vibrate(500);
			}

			@Override
			public void onFinish() {
				if (BesafePostApplication.isActivityVisible()) {
					setResult(RESULT_OK);
					actionFinish();
				} else {
					isCallFromHomeScreen = true;
					Intent i = new Intent(TipActivity.this, DialogActivity.class);
					i.putExtra("action", action);
					i.putExtra("isFromFb", isTipOfFb);
					startActivity(i);
				}
			}
		};
		waitTimer.create();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		actionFinish();
	}

	@Override
	protected void onResume() {
		super.onResume();
		BesafePostApplication.activityResumed();
		if (isCallFromHomeScreen) {
			String notify = getResources().getString(R.string.notice_app_restarted_on_tip).replace(
					THIS_IS_ACTION, action);
			DialogUtils.showDialogWhenResumeFromHome(TipActivity.this, TipActivity.this, isTipOfFb,
					action, notify);
		}
		isCallFromHomeScreen = false;
	}

	@Override
	protected void onPause() {
		super.onPause();
		BesafePostApplication.activityPaused();
	}

	@Override
	public void sendNow() {
		setResult(RESULT_OK);
		actionFinish();
	}

	@Override
	public void stop() {
		actionFinish();
	}

}
