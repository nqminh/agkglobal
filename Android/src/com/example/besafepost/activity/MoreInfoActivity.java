package com.example.besafepost.activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.example.besafepost.utils.DialogUtils;

public class MoreInfoActivity extends Activity {
	private Button btnBack;
	private WebView wvMoreInfo;
	private String whichCall;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_more_info);

		btnBack = (Button) findViewById(R.id.btnBack);
		wvMoreInfo = (WebView) findViewById(R.id.wvMoreInfo);

		getAllIntent();
		btnBack.setOnClickListener(onClick);

		wvMoreInfo.getSettings().setJavaScriptEnabled(true);
		wvMoreInfo.setWebChromeClient(new WebChromeClient());
		//		wvMoreInfo.getSettings().setLoadsImagesAutomatically(true);
		//		wvMoreInfo.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		//		wvMoreInfo.getSettings().setDomStorageEnabled(true);
		//		wvMoreInfo
		//				.getSettings()
		//				.setUserAgentString(
		//						"Mozilla/5.0 (Linux; U; Android 2.0; en-us; "
		//								+ "Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
		//		try {
		//			Method m = WebSettings.class.getMethod("setDomStorageEnabled",
		//					new Class[] { boolean.class });
		//			m.invoke(wvMoreInfo.getSettings(), true);
		//		} catch (SecurityException e) {
		//		} catch (NoSuchMethodException e) {
		//		} catch (IllegalArgumentException e) {
		//		} catch (IllegalAccessException e) {
		//		} catch (InvocationTargetException e) {
		//		}

		wvMoreInfo.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				DialogUtils.dismissDialog();
				super.onPageFinished(view, url);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (!MoreInfoActivity.this.isFinishing())
					DialogUtils.showProgressDialog(MoreInfoActivity.this, "",
							"Loading...", true);
				return true;
			}
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				// TODO Auto-generated method stub

				DialogUtils.showProgressDialog(MoreInfoActivity.this, "", "Loading...", true);
				super.onPageStarted(view, url, favicon);
			}
		});
		if (whichCall.equals("Tweet")) {
			wvMoreInfo.loadUrl("https://twitter.com/privacy");
		} else {
			wvMoreInfo.loadUrl("https://m.facebook.com/legal/terms");
		}
	}

	private OnClickListener onClick = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			finish();
		}
	};

	private void getAllIntent() {
		Intent intent = getIntent();
		whichCall = intent.getStringExtra("MoreInfo");
		btnBack.setText(whichCall);
	}
}
