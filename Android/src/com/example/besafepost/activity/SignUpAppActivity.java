package com.example.besafepost.activity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.besafepost.interfaces.IAsyncTaskDelegate;
import com.example.besafepost.singleton.LoginObject;
import com.example.besafepost.task.AbstractTask;
import com.example.besafepost.task.LoginTask.LoginTaskObject;
import com.example.besafepost.task.RegisterTask;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.Utils;

public class SignUpAppActivity extends Activity implements OnClickListener,
		IAsyncTaskDelegate<AbstractTask> {
	private EditText edtSignUpEmail;
	private EditText edtSignUpPw;
	private EditText edtZipCode;
	private EditText edtBirthyear;
	private Spinner spnGender;
	private ImageView ivWarn1;
	private ImageView ivWarn2;
	private ImageView ivWarn3;
	private ImageView ivWarn4;
	private ImageView ivWarn5;
	private CheckBox cbReadAgree;
	private TextView tvTerm, tvPatent;
	private Button btnRegister;
	private Button btnGoBack;

	private String listSex[] = { "Gender", "Male", "Female" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_app);

		edtSignUpEmail = (EditText) findViewById(R.id.edtSignUpEmail);
		edtSignUpPw = (EditText) findViewById(R.id.edtSignUpPw);
		edtZipCode = (EditText) findViewById(R.id.edtZipCode);
		edtBirthyear = (EditText) findViewById(R.id.edtBirthyear);
		spnGender = (Spinner) findViewById(R.id.spnGender);
		ivWarn1 = (ImageView) findViewById(R.id.iv_warning_1);
		ivWarn2 = (ImageView) findViewById(R.id.iv_warning_2);
		ivWarn3 = (ImageView) findViewById(R.id.iv_warning_3);
		ivWarn4 = (ImageView) findViewById(R.id.iv_warning_4);
		ivWarn5 = (ImageView) findViewById(R.id.iv_warning_5);
		cbReadAgree = (CheckBox) findViewById(R.id.checkBox1);
		tvTerm = (TextView) findViewById(R.id.tvTerm);
		btnRegister = (Button) findViewById(R.id.btnRegister);
		btnGoBack = (Button) findViewById(R.id.btnGoBack);
		tvPatent = (TextView) findViewById(R.id.btnPatentPending);

		edtZipCode.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		edtZipCode.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
		edtBirthyear.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
		edtBirthyear.setKeyListener(DigitsKeyListener.getInstance("0123456789"));

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, listSex);
		adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
		spnGender.setOnItemSelectedListener(onItemSelected);
		spnGender.setAdapter(adapter);

		tvTerm.setOnClickListener(this);
		btnRegister.setOnClickListener(this);
		btnGoBack.setOnClickListener(this);
		tvPatent.setOnClickListener(this);
		setupUI(findViewById(R.id.main_view));
	}

	private void setupUI(View view) {

		// Set up touch listener for non-text box views to hide keyboard.
		if (!(view instanceof EditText)) {

			view.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					Utils.hideKeyboard(SignUpAppActivity.this);
					return false;
				}

			});
		}

		// If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {

			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

				View innerView = ((ViewGroup) view).getChildAt(i);

				setupUI(innerView);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvTerm:
			AlertDialog.Builder builder = new AlertDialog.Builder(SignUpAppActivity.this);
			builder.setTitle("Term of Use");
			builder.setMessage(getResources().getString(R.string.term));
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					cbReadAgree.setChecked(false);
					dialog.dismiss();
				}
			});
			builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					cbReadAgree.setChecked(true);
					dialog.dismiss();
				}
			});
			builder.create().show();
			break;

		case R.id.btnRegister:
			String email = edtSignUpEmail.getText().toString();
			String passWord = edtSignUpPw.getText().toString();
			String zipCode = edtZipCode.getText().toString();
			String birthYear = edtBirthyear.getText().toString();
			String gender = spnGender.getSelectedItem().toString();

			String regExEmail = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
					+ "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
					+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
					+ "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
					+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
					+ "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
			String regExZipCode = "^[0-9]{5}(?:-[0-9]{4})?$";

			Matcher matcherObjEmail = Pattern.compile(regExEmail).matcher(email);
			Matcher matcherObjZipCode = Pattern.compile(regExZipCode).matcher(zipCode);

			if (email.equals("") || !matcherObjEmail.matches()) {
				Toast.makeText(SignUpAppActivity.this, "Please enter correct email.",
						Toast.LENGTH_LONG).show();
				ivWarn1.setVisibility(View.VISIBLE);
				ivWarn2.setVisibility(View.INVISIBLE);
				ivWarn3.setVisibility(View.INVISIBLE);
				ivWarn4.setVisibility(View.INVISIBLE);
				ivWarn5.setVisibility(View.INVISIBLE);
			} else if (passWord.equals("") || passWord.length() < 6) {
				Toast.makeText(SignUpAppActivity.this, "Password should be at least 6 characters.",
						Toast.LENGTH_LONG).show();
				ivWarn1.setVisibility(View.INVISIBLE);
				ivWarn2.setVisibility(View.VISIBLE);
				ivWarn3.setVisibility(View.INVISIBLE);
				ivWarn4.setVisibility(View.INVISIBLE);
				ivWarn5.setVisibility(View.INVISIBLE);
			} else if (zipCode.equals("") || !matcherObjZipCode.matches()) {
				Toast.makeText(SignUpAppActivity.this, "Invalid ZIP Code.", Toast.LENGTH_LONG)
						.show();
				ivWarn1.setVisibility(View.INVISIBLE);
				ivWarn2.setVisibility(View.INVISIBLE);
				ivWarn3.setVisibility(View.VISIBLE);
				ivWarn4.setVisibility(View.INVISIBLE);
				ivWarn5.setVisibility(View.INVISIBLE);
			} else if (birthYear.equals("")) {
				ivWarn1.setVisibility(View.INVISIBLE);
				ivWarn2.setVisibility(View.INVISIBLE);
				ivWarn3.setVisibility(View.INVISIBLE);
				ivWarn4.setVisibility(View.VISIBLE);
				ivWarn5.setVisibility(View.INVISIBLE);
			} else {
				if (gender.equals("Gender")) {
					ivWarn1.setVisibility(View.INVISIBLE);
					ivWarn2.setVisibility(View.INVISIBLE);
					ivWarn3.setVisibility(View.INVISIBLE);
					ivWarn4.setVisibility(View.INVISIBLE);
					ivWarn5.setVisibility(View.VISIBLE);
				} else if (!gender.equals("Gender") && !gender.equals("Male")
						&& !gender.equals("Female")) {
					ivWarn1.setVisibility(View.INVISIBLE);
					ivWarn2.setVisibility(View.INVISIBLE);
					ivWarn3.setVisibility(View.INVISIBLE);
					ivWarn4.setVisibility(View.INVISIBLE);
					ivWarn5.setVisibility(View.VISIBLE);
				} else if (gender.equals("Male") || gender.equals("Female")) {
					ivWarn1.setVisibility(View.INVISIBLE);
					ivWarn2.setVisibility(View.INVISIBLE);
					ivWarn3.setVisibility(View.INVISIBLE);
					ivWarn4.setVisibility(View.INVISIBLE);
					ivWarn5.setVisibility(View.INVISIBLE);
					if (!cbReadAgree.isChecked()) {
						Toast.makeText(SignUpAppActivity.this,
								"Please click to checkbox to register", Toast.LENGTH_LONG).show();
					} else {
						LoginObject loginObject = new LoginObject();
						loginObject.setEmail(edtSignUpEmail.getText().toString());
						loginObject.setPassword(edtSignUpPw.getText().toString());
						loginObject.setZipCode(edtZipCode.getText().toString());
						loginObject.setBirthYear(edtBirthyear.getText().toString());
						loginObject.setSex(spnGender.getSelectedItem().toString());
						new RegisterTask(this, loginObject).execute();
					}
				}
			}
			break;

		case R.id.btnGoBack:
			finish();
			break;

		case R.id.btnPatentPending:
			Intent intent2 = new Intent(Intent.ACTION_VIEW);
			intent2.setData(Uri.parse("http://besafeapps.com/"));
			startActivity(intent2);
			break;
		default:
			break;
		}
	}

	private OnItemSelectedListener onItemSelected = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
			((TextView) arg0.getChildAt(0)).setTextColor(Color.WHITE);
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {

		}
	};

	@Override
	public void onGetStart(AbstractTask task) {
		if (task instanceof RegisterTask) {
			DialogUtils.showProgressDialog(SignUpAppActivity.this, "", getResources().getString(R.string.loading),
					false);
		}
	}

	@Override
	public void onGetSuccess(AbstractTask task) {
		if (task instanceof RegisterTask) {
			DialogUtils.dismissDialog();

			LoginTaskObject object = ((RegisterTask) task).resultObject;

			if (object != null) {
				if (!TextUtils.isEmpty(object.returnCode) && object.returnCode.equals("SUCCESS")) {
					LoginObject.set_instance(object.data);
					LoginObject.get_instance().saveToCache();
					Intent intent = new Intent(SignUpAppActivity.this, MainActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					finish();
				} else {
					Toast.makeText(SignUpAppActivity.this, object.msg, Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(SignUpAppActivity.this, "server problem. please report to us!",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void onGetFailure(AbstractTask task, Exception exception) {
		if (task instanceof RegisterTask) {
			DialogUtils.dismissDialog();
			exception.printStackTrace();
		}
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		DialogUtils.dismissDialog();
	}
}
