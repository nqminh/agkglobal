package com.example.besafepost.activity;

import com.example.besafepost.utils.Const;
import com.example.besafepost.utils.Log;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class TwitterLogin extends Activity {
    public static final String TAG = TwitterLogin.class.getSimpleName();

    @Override
	@SuppressLint("SetJavaScriptEnabled")
	protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_twitter_login);
       
        WebView webView = (WebView) findViewById(R.id.twitterlogin);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
                boolean result = true;
                if (url != null && url.startsWith(Const.CALLBACK_URL)) {
                    Uri uri = Uri.parse(url);
                    Log.v(Log.TAG + " " + TAG, url);
                    if (uri.getQueryParameter("denied") != null) {
                        setResult(RESULT_CANCELED);
                        finish();
                    } else {
//                        String oauthToken = uri.getQueryParameter("oauth_token");
//                        String oauthVerifier = uri.getQueryParameter("oauth_verifier");

                        Intent intent = getIntent();
                        intent.putExtra("URI", uri.toString());
//                        intent.putExtra(Const.IEXTRA_OAUTH_TOKEN, oauthToken);
//                        intent.putExtra(Const.IEXTRA_OAUTH_VERIFIER, oauthVerifier);

                        setResult(RESULT_OK, intent);
                        finish();
                    }
                } else {
                    result = super.shouldOverrideUrlLoading(view, url);
                }
                return result;
            }
        });
        webView.loadUrl(this.getIntent().getExtras().getString("auth_url"));
    }
}