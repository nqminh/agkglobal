package com.example.besafepost.activity;

import com.example.besafepost.model.ArticleItem;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.FacebookUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class OpenLinkAndShareAtivity extends Activity {
	private ArticleItem currentItem;
	private WebView webView;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.open_link_and_share_activity);
		findViewById(R.id.btnBack).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		DialogUtils.showProgressDialog(this, "", "Loading...", true);
		final String postId = getIntent().getStringExtra("postId");
		currentItem = FacebookUtils.getCurrentItemFromListArticleItem(postId);
		webView = (WebView) findViewById(R.id.webViewOpenLinkOnTweet);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebChromeClient(new WebChromeClient());
		webView.setWebViewClient(new MyWebViewClient());
		webView.loadUrl(currentItem.getShareLink());
		findViewById(R.id.btnShare).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent i = new Intent(OpenLinkAndShareAtivity.this,
						ShareActivity.class);
				i.putExtra("postID", postId);
				startActivity(i);
				finish();

			}
		});
	}

	private class MyWebViewClient extends WebViewClient {

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			DialogUtils.dismissDialog();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		webView.destroy();
	}

}
