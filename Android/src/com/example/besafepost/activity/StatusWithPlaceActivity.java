package com.example.besafepost.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.task.DownloadImageTask;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.FacebookUtils;
import com.example.besafepost.utils.StringUtils;
import com.example.besafepost.utils.Utils;
import com.facebook.android.Facebook;

public class StatusWithPlaceActivity extends Activity implements
		OnClickListener, IActivityDelegate {
	private ImageView imgIcon;
	private Button btnCancel, btnPost;
	private EditText edtStatus;
	private TextView tvPlace;
	Facebook mFacebook;
	private String placeId;
	String[] permission = { "publish_stream", "offline_access", "user_photos",
			"photo_upload" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_status_place);
		Intent i = getIntent();
		String place = i.getStringExtra("place");
		String iconLink = i.getStringExtra("icon_link");
		String textPost = i.getStringExtra("text_post");

		placeId = i.getStringExtra("placeId");

		tvPlace = (TextView) findViewById(R.id.name);
		tvPlace.setText(Html.fromHtml("-At " + "<b><FONT COLOR=\"#3399ff\" >"
				+ place + "</Font></b>"));
		imgIcon = (ImageView) findViewById(R.id.icon);
		new DownloadImageTask(imgIcon).execute(iconLink);

		btnCancel = (Button) findViewById(R.id.btnCancel);
		btnPost = (Button) findViewById(R.id.btnPost);
		edtStatus = (EditText) findViewById(R.id.edtStatus);
		edtStatus.setText(textPost);
		edtStatus.requestFocus();

		btnCancel.setOnClickListener(this);
		btnPost.setOnClickListener(this);
		Utils.showKeyboard(this, edtStatus);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnCancel:
			finish();
			break;

		case R.id.btnPost:
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(this);
			if (Utils.isBlockedAddress(edtStatus.getText().toString())
					|| Utils.isBlockedWord(edtStatus.getText().toString())) {
				DialogUtils.showDialogBannedWord(StatusWithPlaceActivity.this,
						Utils.isBlockedWord(edtStatus.getText().toString()),
						Utils.isBlockedAddress(edtStatus.getText().toString()));
			} else if (prefs.getBoolean(StringUtils.KEY_POST_TIP, true)) {
				Intent intent = new Intent(StatusWithPlaceActivity.this,
						TipActivity.class);
				intent.putExtra("isTipOfFb", true);
				intent.putExtra("action", "Check In");
				startActivityForResult(intent, Utils.CALL_TIP_FOR_CHECK_IN);
			} else {
				updateStatusWithPlace();
			}
			btnPost.setEnabled(false);
			break;

		default:
			break;
		}
	}

	@Override
	public void onGetSuccess(String requestCode) {
		setResult(RESULT_OK);
		DialogUtils.dismissDialog();
		finish();
	}

	@Override
	public void onGetFailure(String requestCode) {
		setResult(RESULT_CANCELED);
		DialogUtils.dismissDialog();
		finish();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Utils.CALL_TIP_FOR_CHECK_IN) {
			btnPost.setEnabled(true);
			if (resultCode == RESULT_OK) {
				updateStatusWithPlace();
			}
		}
	}

	private void updateStatusWithPlace() {

		DialogUtils.showProgressDialog(this, "", "Updating status...", false);
		String post = edtStatus.getText().toString();
		FacebookUtils.postSttPlace(post, placeId, this);

	}
}
