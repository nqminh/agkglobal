package com.example.besafepost.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class DialogActivity extends Activity implements OnClickListener {
	private Button btnClose;
	private Button btnActionAnyway;
	private TextView tvNotice;
	private TextView tvTitle;
	private boolean isCalledFromCountdown;
	private String action;
	private String THIS_IS_ACTION = "THIS_IS_ACTION";
	private String THIS_IS_TWEET_OR_POST = "THIS_IS_TWEET_OR_POST";
	private boolean isFromFb;
	private static final int MY_NOTIFICATION_ID = 1;
	private String notice;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_activity);
		getAllIntent();
		initView();
		notifyUser();
		//		displayBadge();
	}

	private void getAllIntent() {
		Intent intent = getIntent();
		isCalledFromCountdown = intent.getBooleanExtra("isCalledFromCountdown",
				false);
		action = intent.getStringExtra("action");
		isFromFb = intent.getBooleanExtra("isFromFb", true);
	}

	private void initView() {
		btnClose = (Button) findViewById(R.id.btnClose);
		btnActionAnyway = (Button) findViewById(R.id.btnActionAnyway);
		tvNotice = (TextView) findViewById(R.id.tvNotice);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
		tvTitle.setText("BeSafe Apps");
		btnClose.setOnClickListener(this);
		btnActionAnyway.setOnClickListener(this);
		btnActionAnyway.setText(action + " Anyway");

		String post_or_tweet;
		if (isFromFb) {
			post_or_tweet = "Post";
		} else {
			post_or_tweet = "Tweet";
		}
		notice = getString(R.string.notice_dialog_on_home_screen).replace(
				THIS_IS_ACTION, action).replace(THIS_IS_TWEET_OR_POST,
				post_or_tweet);
		tvNotice.setText(notice);
	}

	@Override
	public void onClick(View v) {
		if (v == btnClose) {
			finish();
		}
		if (v == btnActionAnyway) {
			if (isCalledFromCountdown) {
				startActivity(new Intent(DialogActivity.this,
						CountDownActivity.class));
			} else {
				startActivity(new Intent(DialogActivity.this, TipActivity.class));
			}
			finish();
		}
	}

	@SuppressWarnings("deprecation")
	private void notifyUser() {
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Notification myNotification = new Notification(R.drawable.icon,
				"New Notification!", System.currentTimeMillis());
		Context context = getApplicationContext();
		String notificationTitle = "New Notification!";
		String notificationText = notice;
		Intent myIntent = new Intent(this, TipActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				myIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
		myNotification.defaults |= Notification.DEFAULT_SOUND;
		myNotification.flags |= Notification.FLAG_AUTO_CANCEL;
		myNotification.setLatestEventInfo(context, notificationTitle,
				notificationText, pendingIntent);
		notificationManager.notify(MY_NOTIFICATION_ID, myNotification);
	}

	//	private void displayBadge() {
	//		// TODO Auto-generated method stub
	//		Context context = getApplicationContext();
	//		if (Badge.isBadgingSupported(context)) {
	//			Badge badge = Badge.getBadge(context);
	//
	//			// This indicates there is no badge record for your app yet
	//			if (badge == null) {
	//				return;
	//			} else {
	//				Log.d("Badge 1", badge.toString());
	//			}
	//
	//			badge = new Badge();
	//			badge.mPackage = context.getPackageName();
	//			badge.mClass = getClass().getName(); // This should point to
	//													// Activity declared as
	//													// android.intent.action.MAIN
	//			badge.mBadgeCount = 1;
	//			badge.save(context);
	//		}
	//	}

}
