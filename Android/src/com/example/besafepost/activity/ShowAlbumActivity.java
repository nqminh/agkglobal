package com.example.besafepost.activity;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.besafepost.fragment.ScreenSlidePageFragment;
import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.model.ArticleItem;
import com.example.besafepost.model.PhotoDetails;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.FacebookUtils;
import com.example.besafepost.utils.Log;
import com.example.besafepost.utils.StringUtils;
import com.example.besafepost.utils.Utils;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ShowAlbumActivity extends FragmentActivity implements IActivityDelegate,
		OnClickListener {

	private ViewPager mPager;
	private PagerAdapter mPagerAdapter;
	private String postId;
	private View viewShare;
	ImageView imgShow, imgLike, imgComment;
	TextView tvLikeCount, tvCmtCount, tvDes;
	ArrayList<PhotoDetails> listPhotDetails;
	int currentImage;
	int likeCount, cmtCount;
	boolean isLike;
	Intent intent;
	Button btBack;
	private Drawable drawCurrent;
	ArrayList<String> listFbidPhoto = new ArrayList<String>();
	ArticleItem currenItem;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_album_activity);
		DialogUtils.showProgressDialog(this, "", "Loading...", false);
//		if (android.os.Build.VERSION.SDK_INT > 9) {
//			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll()
//					.build();
//			StrictMode.setThreadPolicy(policy);
//		}

		// get list fb id photo
		intent = getIntent();
		postId = intent.getStringExtra("postId");
		currenItem = FacebookUtils.getCurrentItemFromListArticleItem(postId);
		listFbidPhoto = currenItem.getListFbidPhoto();
		initAllView();
		FacebookUtils.getDetailsPhoto(this, listFbidPhoto, this);

	}

	private void initAllView() {
		mPager = (ViewPager) findViewById(R.id.pager);
		btBack = (Button) findViewById(R.id.btnBack);
		btBack.setOnClickListener(this);
		imgLike = (ImageView) findViewById(R.id.imgLike);
		imgLike.setOnClickListener(this);
		imgComment = (ImageView) findViewById(R.id.imgCmt);
		imgComment.setOnClickListener(this);
		tvLikeCount = (TextView) findViewById(R.id.tvLikeCount);
		tvCmtCount = (TextView) findViewById(R.id.tvCmtCount);
		tvDes = (TextView) findViewById(R.id.txt_des);
		viewShare = findViewById(R.id.viewShare);
		viewShare.setOnClickListener(this);
	}

	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
		public ScreenSlidePagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return new ScreenSlidePageFragment(ShowAlbumActivity.this,
					listPhotDetails.get(position));
		}

		@Override
		public int getCount() {
			return listFbidPhoto.size();
		}
	}

	private void setLikeAndCmtCount(PhotoDetails details) {

		tvLikeCount.setText(StringUtils.rountOffNumber(likeCount) + " Likes");
		tvCmtCount.setText(StringUtils.rountOffNumber(cmtCount) + " Comments");
		if (isLike) {
			imgLike.setImageResource(R.drawable.fb_unlike);
		} else {
			imgLike.setImageResource(R.drawable.fb_like);
		}
		tvDes.setText(details.getDescription());
	}

	@Override
	public void onGetSuccess(String requestCode) {
		if (requestCode.equalsIgnoreCase(Utils.GET_DETAILS_PHOTO)) {
			DialogUtils.dismissDialog();
			listPhotDetails = FacebookUtils.getListPhotoDetails();
			if (listPhotDetails.size() > 0) {
				mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
				mPager.setAdapter(mPagerAdapter);
				currentImage = 0;
				likeCount = listPhotDetails.get(currentImage).getLikeCount();
				cmtCount = listPhotDetails.get(currentImage).getCommentCount();
				isLike = listPhotDetails.get(currentImage).getIsLike();
				setLikeAndCmtCount(listPhotDetails.get(currentImage));
			}
			mPager.setOnPageChangeListener(new OnPageChangeListener() {

				@Override
				public void onPageSelected(int arg0) {
					currentImage = arg0;
					likeCount = listPhotDetails.get(currentImage).getLikeCount();
					cmtCount = listPhotDetails.get(currentImage).getCommentCount();
					isLike = listPhotDetails.get(currentImage).getIsLike();
					setLikeAndCmtCount(listPhotDetails.get(currentImage));
				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {

				}

				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub

				}
			});
		}
	}

	@Override
	public void onBackPressed() {
		if (listFbidPhoto.size() == 1 && currenItem.getType() != 80) {
			intent.putExtra("isLike", isLike);
			intent.putExtra("postId", postId);
			intent.putExtra("likeCount", StringUtils.rountOffNumber(likeCount));
			intent.putExtra("cmtCount", StringUtils.rountOffNumber(cmtCount));
			// reset like count for this item (item have a photo)
			currenItem.setLikeCount(likeCount);
			currenItem.setCommentCount(cmtCount);
			currenItem.setUserLiked(isLike);
			setResult(RESULT_OK, intent);
		} else {
			setResult(RESULT_CANCELED, intent);
		}
		super.onBackPressed();
	}

	@Override
	public void onClick(View v) {
		if (v == viewShare) {
			Intent i = new Intent(this, ShareActivity.class);
			i.putExtra("postID", postId);
			startActivity(i);
		}
		if (v == btBack) {
			if (listFbidPhoto.size() == 1 && currenItem.getType() != 80) {
				intent.putExtra("isLike", isLike);
				intent.putExtra("postId", postId);
				intent.putExtra("likeCount", likeCount);
				intent.putExtra("cmtCount", cmtCount);
				currenItem.setLikeCount(likeCount);
				currenItem.setCommentCount(cmtCount);
				currenItem.setUserLiked(isLike);
				setResult(RESULT_OK, intent);
			} else {
				setResult(RESULT_CANCELED, intent);
			}

			finish();
		}
		if (v == imgLike) {
			if (isLike) {
				FacebookUtils.unlike(listPhotDetails.get(currentImage).getObjectId());
				isLike = false;
				likeCount--;
				setLikeAndCmtCount(listPhotDetails.get(currentImage));
			} else {
				functionLikePost();
			}
		}
		if (v == imgComment) {
			imgComment.setEnabled(false);
			Intent i = new Intent(ShowAlbumActivity.this, CommentOnPhotoActivity.class);
			/*
			 * Bitmap bitmap = ((BitmapDrawable) drawCurrent).getBitmap();
			 * ByteArrayOutputStream baos = new ByteArrayOutputStream();
			 * bitmap.compress(Bitmap.CompressFormat.PNG, 50, baos); byte[] b =
			 * baos.toByteArray();
			 */
			i.putExtra("srcImgCurrent", listPhotDetails.get(currentImage).getSrc());
			i.putExtra("photoId", listPhotDetails.get(currentImage).getObjectId());
			i.putExtra("cmtCount", cmtCount);
			i.putExtra("Post profileName", currenItem.getProfileName());
			i.putExtra("Post profileIcon", currenItem.getProfileIcon());
			startActivityForResult(i, Utils.CALL_CMT_ON_PHOTO_ACT);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Utils.CALL_CMT_ON_PHOTO_ACT) {
			if (resultCode == RESULT_OK) {
				cmtCount = data.getIntExtra("cmtCount", cmtCount);
				tvCmtCount.setText(StringUtils.rountOffNumber(cmtCount) + " Comments");
			}
			// enable onclick imgcommet
			imgComment.setEnabled(true);
		} else if (requestCode == Utils.CALL_TIP_FOR_LIKE_POST_IN_COMMENT_ACTIVITY) {
			if (resultCode == RESULT_OK) {
				likeAPost();
			}
		}
	}

	public void setDrawCurrent(Drawable drawCurrent) {
		this.drawCurrent = drawCurrent;

	}

	@Override
	public void onGetFailure(String requestCode) {
//		Log.d(Log.TAG, "false");
		if (requestCode.equalsIgnoreCase(Utils.GET_DETAILS_PHOTO)) {
			DialogUtils.dismissDialog();
		}

	}

	private void likeAPost() {
		FacebookUtils.like(listPhotDetails.get(currentImage).getObjectId());
		isLike = true;
		likeCount++;
		setLikeAndCmtCount(listPhotDetails.get(currentImage));
	}

	private void functionLikePost() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(ShowAlbumActivity.this);
		isLike = false;
		if (prefs.getBoolean("Besafe Tip", true)) {
			Intent i = new Intent(ShowAlbumActivity.this, TipActivity.class);
			i.putExtra("action", "Like");
			i.putExtra("iseExecuteWhenTurnOff", true);
			startActivityForResult(i, Utils.CALL_TIP_FOR_LIKE_POST_IN_COMMENT_ACTIVITY);

		} else {
			likeAPost();
		}

	}
}
