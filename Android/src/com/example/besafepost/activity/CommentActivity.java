package com.example.besafepost.activity;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.model.ArticleItem;
import com.example.besafepost.model.Comment;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.FacebookUtils;
import com.example.besafepost.utils.Log;
import com.example.besafepost.utils.StringUtils;
import com.example.besafepost.utils.Utils;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshWebView;

@SuppressLint("JavascriptInterface")
public class CommentActivity extends Activity implements OnClickListener,
		OnRefreshListener2<WebView>, IActivityDelegate {

	public static EditText edtComment;
	private Button btnComment, btnCancel;
	private WebView webviewComment;
	private String postId, photoId = "";
	private String profileIcon;
	private PullToRefreshWebView mPullToRefreshWebView;
	private static String postIdLikeComment;
	private static int LIMIT;
	private View refreshLoading, loadMoreLoading;
	String likeCountComment;
	private int likeCount;
	private int cmtCount;
	private boolean isLike;
	private SharedPreferences delayPrefs;
	private ArticleItem currentItem;

	private boolean isFocused_edtComment;
	private String thePost = "";

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comment);
		Intent intent = getIntent();
		postId = intent.getStringExtra("postID");
		thePost = intent.getStringExtra("contentForCommentAct");

		// show loading when load comment
		refreshLoading = findViewById(R.id.refresh_header);
		refreshLoading.setVisibility(View.VISIBLE);
		loadMoreLoading = findViewById(R.id.loadmore_header);

		edtComment = (EditText) findViewById(R.id.et_cmt);
		edtComment.requestFocus();
		isFocused_edtComment = true;
		btnComment = (Button) findViewById(R.id.bt_cmt);
		btnCancel = (Button) findViewById(R.id.bt_Cancel);

		mPullToRefreshWebView = (PullToRefreshWebView) findViewById(R.id.webviewComment);
		mPullToRefreshWebView.setOnRefreshListener(this);
		webviewComment = mPullToRefreshWebView.getRefreshableView();
		webviewComment.addJavascriptInterface(
				new MyJavaScriptInterfaceWebviewDialog(), "Android");
		webviewComment.getSettings().setJavaScriptEnabled(true);
		webviewComment.setWebViewClient(new WebViewDialogClient());
		webviewComment.setHorizontalScrollBarEnabled(false);
		webviewComment
				.loadUrl("file:///android_asset/initWebviewCommentFb.html");

		btnComment.setOnClickListener(this);
		btnCancel.setOnClickListener(this);

		delayPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		currentItem = FacebookUtils.getCurrentItemFromListArticleItem(postId);
		profileIcon = currentItem.getProfileIcon();
		cmtCount = currentItem.getCommentCount();
		likeCount = currentItem.getLikeCount();
		isLike = currentItem.isUserLiked();
//		FacebookUtils.getBitmapFromSrc(profileIcon);
//		Log.i(Log.TAG, "actor : " + currentItem.getActorId());
//		Log.i(Log.TAG, "link : " + profileIcon);

	}

	@Override
	public void onBackPressed() {
		setResult();
		finish();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.bt_Cancel:
			setResult();
			finish();
			break;
		case R.id.bt_cmt:
			if (Utils.isBlockedAddress(edtComment.getText().toString())
					|| Utils.isBlockedWord(edtComment.getText().toString())) {
				DialogUtils
						.showDialogBannedWord(CommentActivity.this,
								Utils.isBlockedWord(edtComment.getText()
										.toString()), Utils
										.isBlockedAddress(edtComment.getText()
												.toString()));
			} else {
				if (StringUtils.isEmptyString(edtComment.getText().toString())) {
					Toast.makeText(this, "Please enter your comment!",
							Toast.LENGTH_SHORT).show();
				} else {
					if (delayPrefs.getInt("Delay", 10) == 0) {
						count = 10;
						postComment();
					} else {
						Intent intent = new Intent(CommentActivity.this,
								CountDownActivity.class);
						intent.putExtra("scrName", "Timeline");
						intent.putExtra("msgContent", edtComment.getText()
								.toString());
						intent.putExtra("avatarSrc", profileIcon);
						intent.putExtra("FromFB", true);
						startActivityForResult(intent,
								Utils.START_COUNTDOWN_FOR_POST_COMMENT);
					}
				}
			}
			break;
		default:
			break;
		}
	}

	private void postComment() {
		FacebookUtils
				.postComment(edtComment.getText().toString(), postId, this);
		Utils.hideKeyboard(this);
	}

	public void changeTextInHtml(String idTag, String text) {
		String changeText = "javascript:changeTextInTag('" + idTag + "','"
				+ text + "');";
		webviewComment.loadUrl(changeText);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Utils.START_COUNTDOWN_FOR_POST_COMMENT) {
			if (resultCode == RESULT_OK) {
				count = 10;
				postComment();
			}
		}
		if ((resultCode == Activity.RESULT_OK)
				&& requestCode == Utils.REQUEST_CODE_SHOW_ALBUM) {
			setLikeCountAndCmtCountWithIntent(data);
		}
		if (requestCode == Utils.CALL_TIP_FOR_LIKE_POST_IN_COMMENT_ACTIVITY) {
			if (resultCode == RESULT_OK) {
				likeAPost();
			}
		}
		if (requestCode == Utils.CALL_TIP_FOR_LIKE_COMMENT) {
			if (resultCode == RESULT_OK) {
				actionLikeComment();
			}
		}
	}

	private void setLikeCountAndCmtCountWithIntent(Intent data) {
		String postId = data.getStringExtra("postId");
		isLike = data.getBooleanExtra("isLike", false);
		likeCount = data.getIntExtra("likeCount", 0);
		cmtCount = data.getIntExtra("cmtCount", 0);
		if (isLike) {
			changeTextInHtml("postId:" + postId, "Unlike");
		} else {
			changeTextInHtml("postId:" + postId, "Like");
		}
		if (likeCount > 0) {
			if (cmtCount > 0) {
				changeTextInHtml(StringUtils.ID_IMG_4 + postId,
						StringUtils.IMG_LIKE);
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId,
						StringUtils.rountOffNumber(likeCount));
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId,
						StringUtils.rountOffNumber(cmtCount));
				changeTextInHtml(StringUtils.ID_IMG_2 + postId,
						StringUtils.IMG_COMMENT);
			} else {
				changeTextInHtml(StringUtils.ID_IMG_2 + postId,
						StringUtils.IMG_LIKE);
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId,
						StringUtils.rountOffNumber(likeCount));
				changeTextInHtml(StringUtils.ID_IMG_4 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId, "");
			}
		} else {
			if (cmtCount > 0) {
				changeTextInHtml(StringUtils.ID_IMG_4 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId, "");
				changeTextInHtml(StringUtils.ID_IMG_2 + postId,
						StringUtils.IMG_COMMENT);
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId,
						StringUtils.rountOffNumber(cmtCount));
			} else {
				changeTextInHtml(StringUtils.ID_IMG_2 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId, "");
				changeTextInHtml(StringUtils.ID_IMG_4 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId, "");
			}
		}
	}

	final class MyJavaScriptInterfaceWebviewDialog {
		public void functionLikeComment(String likeOrUnlike, String likeCountCmt) {
			likeCountComment = likeCountCmt;
			// text equals like,user not like
			if (likeOrUnlike.equalsIgnoreCase("like")) {

				if (delayPrefs.getBoolean("Besafe Tip", true)) {
					Intent i = new Intent(CommentActivity.this,
							TipActivity.class);
					i.putExtra("action", "Like");
					i.putExtra("iseExecuteWhenTurnOff", true);
					startActivityForResult(i, Utils.CALL_TIP_FOR_LIKE_COMMENT);

				} else {
					actionLikeComment();
				}

			} else {
				String changeText = null;
				String changeLikeCount = null;
				int intLikeCount;
				intLikeCount = Integer.valueOf(likeCountCmt);
				intLikeCount -= 1;
				changeText = "javascript:changeTextInTag('"
						+ Comment.ID_LIKE_OR_UNLIKE + postIdLikeComment
						+ "','Like');";
				likeCountCmt = String.valueOf(intLikeCount);
				changeLikeCount = "javascript:changeTextInTag('"
						+ Comment.ID_LIKE_COUNT + postIdLikeComment + "','"
						+ likeCountCmt + "');";
				FacebookUtils.unlike(postIdLikeComment);
				webviewComment.loadUrl(changeText);
				webviewComment.loadUrl(changeLikeCount);
			}
		}
	}

	private void actionLikeComment() {
		String changeText = null;
		String changeLikeCount = null;
		int intLikeCount;
		intLikeCount = Integer.valueOf(likeCountComment);
		intLikeCount += 1;
		changeText = "javascript:changeTextInTag('" + Comment.ID_LIKE_OR_UNLIKE
				+ postIdLikeComment + "','Unlike');";
		likeCountComment = String.valueOf(intLikeCount);
		changeLikeCount = "javascript:changeTextInTag('"
				+ Comment.ID_LIKE_COUNT + postIdLikeComment + "','"
				+ likeCountComment + "');";
		FacebookUtils.like(postIdLikeComment);
		webviewComment.loadUrl(changeText);
		webviewComment.loadUrl(changeLikeCount);
	}

	private void changeTextAfterLikePost() {
		if (likeCount > 0) {
			if (cmtCount > 0) {
				changeTextInHtml(StringUtils.ID_IMG_4 + postId,
						StringUtils.IMG_LIKE);
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId,
						StringUtils.rountOffNumber(likeCount));
			} else {
				changeTextInHtml(StringUtils.ID_IMG_2 + postId,
						StringUtils.IMG_LIKE);
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId,
						StringUtils.rountOffNumber(likeCount));
			}
		} else {
			if (cmtCount > 0) {
				changeTextInHtml(StringUtils.ID_IMG_4 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_3 + postId, "");
			} else {
				changeTextInHtml(StringUtils.ID_IMG_2 + postId, "");
				changeTextInHtml(StringUtils.ID_COUNT_1 + postId, "");
			}
		}

	}

	public void functionLikePost() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(CommentActivity.this);
		isLike = false;
		if (prefs.getBoolean("Besafe Tip", true)) {
			Intent i = new Intent(CommentActivity.this, TipActivity.class);
			i.putExtra("action", "Like");
			i.putExtra("iseExecuteWhenTurnOff", true);
			startActivityForResult(i,
					Utils.CALL_TIP_FOR_LIKE_POST_IN_COMMENT_ACTIVITY);

		} else {
			likeAPost();
		}

	}

	private void likeAPost() {
		likeCount++;
		isLike = true;
		if (photoId.isEmpty()) {
			FacebookUtils.like(postId);
		} else {
			FacebookUtils.like(photoId);
		}
		changeTextInHtml("postId:" + postId, "Unlike");

		changeTextAfterLikePost();
	}

	private class WebViewDialogClient extends WebViewClient {
		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			if (thePost == null) {
				List<ArticleItem> currentItemList = new ArrayList<ArticleItem>();
				currentItemList.add(currentItem);
				thePost = StringUtils.dataNewFeeds(currentItemList,
						CommentActivity.this, true);
			}
			thePost = thePost.replace("\n", "<br>");
			thePost = thePost.replace("\t", "");
			thePost = thePost.replace("\r", "");
			thePost = thePost.replace("'", "&#39;");
			String insertThePost = "javascript:changeTextInTag('"
					+ "thePost','" + thePost + "');";
			String hideIconHideDelete = "javascript:changeTextInTag('icon_hide_delete','');";
			webviewComment.loadUrl(insertThePost);
			webviewComment.loadUrl(hideIconHideDelete);
			// in first,get 20 comment
			LIMIT = StringUtils.LIMIT_COMMENT;
			FacebookUtils.getComment(postId, webviewComment,
					getApplicationContext(), LIMIT, CommentActivity.this);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.startsWith(Comment.HREF_LIKE_COMMEN)) {
				postIdLikeComment = url.substring(Comment.HREF_LIKE_COMMEN
						.length());
				String getTextLikeOrUnlike = "javascript:getTextLikeOrUnlike('"
						+ Comment.ID_LIKE_OR_UNLIKE + postIdLikeComment + "','"
						+ Comment.ID_LIKE_COUNT + postIdLikeComment + "')";
				webviewComment.loadUrl(getTextLikeOrUnlike);
			}
			if (url.startsWith("http://onclick/album")) {
				postId = url.substring(20);
				Intent i = new Intent(CommentActivity.this,
						ShowAlbumActivity.class);
				i.putExtra("postId", postId);
				startActivityForResult(i, Utils.REQUEST_CODE_SHOW_ALBUM);
			}
			if (url.startsWith(ArticleItem.LIKE_LINK)
					|| url.startsWith(ArticleItem.LIKE_ON_PHOTO_LINK)) {
				String[] listId = url.split(ArticleItem.LIKE_ON_PHOTO_LINK);
				if (listId.length > 1) {
					photoId = listId[1];
				}
				if (isLike) {
					isLike = false;
					likeCount--;
					if (photoId.isEmpty()) {
						FacebookUtils.unlike(postId);
					} else {
						FacebookUtils.unlike(photoId);
					}
					changeTextAfterLikePost();
					changeTextInHtml("postId:" + postId, "Like");
				} else {
					functionLikePost();
				}
			}
			if (url.startsWith(ArticleItem.SHARE_POST_LINK)) {
				Intent i = new Intent(CommentActivity.this, ShareActivity.class);
				i.putExtra("postID", postId);
				startActivity(i);

			}
			if (url.startsWith(ArticleItem.COMMENT_LINK)) {
				if (isFocused_edtComment) {
					Utils.hideKeyboard(CommentActivity.this);
				} else {
					Utils.showKeyboard(CommentActivity.this, edtComment);
				}
				isFocused_edtComment = !isFocused_edtComment;
			}
			return true;

		}
	}

	@Override
	public void onPullDownToRefresh(PullToRefreshBase<WebView> refreshView) {

		// get 20 comment
		LIMIT = StringUtils.LIMIT_COMMENT;
		refreshLoading.setVisibility(View.VISIBLE);
		FacebookUtils.getComment(postId, webviewComment,
				getApplicationContext(), LIMIT, this);
		mPullToRefreshWebView.onRefreshComplete();
	}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase<WebView> refreshView) {
		// load more 10 comment
		LIMIT += StringUtils.LIMIT_ADVANCE;
		loadMoreLoading.setVisibility(View.VISIBLE);
		FacebookUtils.getComment(postId, webviewComment,
				getApplicationContext(), LIMIT, this);
		mPullToRefreshWebView.onRefreshComplete();

	}

	@Override
	public void onGetSuccess(String requestCode) {
		if (requestCode.equalsIgnoreCase(Utils.REQUEST_GET_COMMENT)) {
			loadMoreLoading.setVisibility(View.GONE);
			refreshLoading.setVisibility(View.GONE);
			webviewComment.pageDown(true);
		}
		if (requestCode.equals(Utils.REQUEST_POST_COMMENT)) {
			postCommentSuccess();

			LIMIT += StringUtils.LIMIT_ADVANCE;
			FacebookUtils.getComment(postId, webviewComment,
					getApplicationContext(), LIMIT, this);
		}

	}

	private void postCommentSuccess() {
		edtComment.setText("");
		Toast.makeText(CommentActivity.this, "Post comment successful",
				Toast.LENGTH_LONG).show();
		// change comment count
		cmtCount = cmtCount + 1;
		changeTextInHtml(StringUtils.ID_COUNT_1 + postId,
				StringUtils.rountOffNumber(cmtCount));
		changeTextInHtml(StringUtils.ID_IMG_2 + postId, StringUtils.IMG_COMMENT);
		if (likeCount > 0) {
			changeTextInHtml(StringUtils.ID_COUNT_3 + postId,
					StringUtils.rountOffNumber(likeCount));
			changeTextInHtml(StringUtils.ID_IMG_4 + postId,
					StringUtils.IMG_LIKE);
		}
	}

	private void setResult() {
		Intent intent = new Intent();
		intent.putExtra("isLike", isLike);
		intent.putExtra("postId", postId);
		intent.putExtra("likeCount", likeCount);
		intent.putExtra("cmtCount", cmtCount);
		// reset like count for this item (item have a photo)
		currentItem.setLikeCount(likeCount);
		currentItem.setCommentCount(cmtCount);
		currentItem.setUserLiked(isLike);
		setResult(RESULT_OK, intent);
	}

	private int count = 10;
	@Override
	public void onGetFailure(String requestCode) {
		if (requestCode.equalsIgnoreCase(Utils.REQUEST_GET_COMMENT)) {
			loadMoreLoading.setVisibility(View.GONE);
			refreshLoading.setVisibility(View.GONE);
		}
		if (requestCode.equals(Utils.REQUEST_POST_COMMENT)) {
			if (count > 0) {
				count--;
				postComment();
			} else {
				edtComment.setText("");
				Toast.makeText(CommentActivity.this, "Post comment not successful, some problem happen",
						Toast.LENGTH_LONG).show();
			}
		}
	}
};
