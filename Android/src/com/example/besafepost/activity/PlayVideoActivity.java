package com.example.besafepost.activity;

import android.app.Activity;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

public class PlayVideoActivity extends Activity {
	private VideoView video;
	private ProgressBar prog;
	private MediaController mediaController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		setContentView(R.layout.activity_play_video);

		video = (VideoView) findViewById(R.id.video);
		prog = (ProgressBar) findViewById(R.id.prog);
		findViewById(R.id.btnDone).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		String video_url = getIntent().getExtras().getString("uploadvideolink");
		Log.i("Upload Video Link", video_url);
		Uri video_uri = Uri.parse(video_url);

		mediaController = new MediaController(this);
		mediaController.setAnchorView(video);
		video.setMediaController(mediaController);
		video.setVideoURI(video_uri);

		video.setOnErrorListener(new OnErrorListener() {

			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				// TODO Auto-generated method stub
				Toast.makeText(getApplicationContext(), "Error occured", 500)
						.show();
				return false;
			}
		});

		video.setOnPreparedListener(new OnPreparedListener() {

			@Override
			public void onPrepared(MediaPlayer arg0) {
				prog.setVisibility(View.GONE);
				video.start();
			}
		});

	}

	@Override
	protected void onDestroy() {
		try {
			video.stopPlayback();
		} catch (Exception e) {
			//
		}
		super.onDestroy();
	}
}
