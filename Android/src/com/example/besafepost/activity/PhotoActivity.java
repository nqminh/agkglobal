package com.example.besafepost.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.utils.ConnectionDetector;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.FacebookUtils;
import com.example.besafepost.utils.Utils;

public class PhotoActivity extends Activity implements OnClickListener,
		IActivityDelegate {

	private Button btnPost, btnCancel;
	private ImageView ivShowImage;
	private EditText edtStatus;
	private Bitmap bitmap;
	private ConnectionDetector cd;
	private Boolean hasNetwork = false;
	private SharedPreferences delayPrefs;
	private byte[] bytes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo);
		btnPost = (Button) findViewById(R.id.btnPost);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		ivShowImage = (ImageView) findViewById(R.id.ivShowImg);
		edtStatus = (EditText) findViewById(R.id.edtStatus);
		edtStatus.requestFocus();

		btnCancel.setOnClickListener(this);
		btnPost.setOnClickListener(this);

		Intent intent = getIntent();
		bytes = intent.getByteArrayExtra("bitmap_image");
		bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
		edtStatus.setText(intent.getStringExtra("text_post"));
		ivShowImage.setImageBitmap(bitmap);
		delayPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		FacebookUtils.getSrcImgProfile();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnCancel:
			Utils.hideKeyboard(PhotoActivity.this);
			finish();
			break;
		case R.id.btnPost:
			cd = new ConnectionDetector(getApplicationContext());
			hasNetwork = cd.isConnectingToInternet();
			if (Utils.isBlockedAddress(edtStatus.getText().toString())
					|| Utils.isBlockedWord(edtStatus.getText().toString())) {
				DialogUtils.showDialogBannedWord(PhotoActivity.this,
						Utils.isBlockedWord(edtStatus.getText().toString()),
						Utils.isBlockedAddress(edtStatus.getText().toString()));
			} else {
				if (hasNetwork) {
					if (delayPrefs.getInt("Delay", 10) == 0) {
						postPhotoToWall();
					} else {
						Intent intent = new Intent(PhotoActivity.this,
								CountDownActivity.class);
						intent.putExtra("scrName", "Timeline");
						intent.putExtra("msgContent", edtStatus.getText()
								.toString());
						intent.putExtra("FromFB", true);
						if (!AdjustPhotoActivity.isTakePhoto) {
							//image post is chosen from memory,not take
							//							image taken
							intent.putExtra("isUseMyProfileFb", true);
						} else {
							intent.putExtra("image taken", bytes);
						}
						startActivityForResult(intent,
								Utils.START_COUNTDOWN_FOR_POST_PHOTO_FB);
					}
				} else {
					Toast.makeText(getApplicationContext(), "Network problem!",
							Toast.LENGTH_LONG).show();
				}
			}
			break;
		default:
			break;
		}
	}

	private void postPhotoToWall() {
		DialogUtils.showProgressDialog(PhotoActivity.this, "",
				"Posting photo...", false);
		FacebookUtils.postPhotoToWall(this, bitmap, edtStatus.getText()
				.toString());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Utils.START_COUNTDOWN_FOR_POST_PHOTO_FB) {
			if (resultCode == RESULT_OK) {
				postPhotoToWall();
			}
		}
	}

	@Override
	public void onGetSuccess(String requestCode) {
		if (requestCode.equalsIgnoreCase(Utils.REQUEST_POST_PHOTO)) {
			Toast.makeText(PhotoActivity.this, "Post photo successful",
					Toast.LENGTH_LONG).show();
			DialogUtils.dismissDialog();
			setResult(RESULT_OK);
			finish();
		}
	}

	@Override
	public void onGetFailure(String requestCode) {
		if (requestCode.equalsIgnoreCase(Utils.REQUEST_POST_PHOTO)) {
			Toast.makeText(PhotoActivity.this, "Post photo not successful",
					Toast.LENGTH_LONG).show();
			DialogUtils.dismissDialog();
		}
	}

}
