package com.example.besafepost.activity;

import java.io.File;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.besafepost.utils.Utils;

public class TwitterSavedPhoto extends Activity implements OnItemClickListener {

	private GridView mPhotosGrid;
	private ListView mFolderList;
	private Button btnCancel;
	private ImageAdapter mImageAdapter;
	private FolderAdapter mFolderAdapter;
	private ArrayList<String> itemList = new ArrayList<String>();
	private ArrayList<String> itemListFolder = new ArrayList<String>();
	private ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twitter_saved_photo);

		btnCancel = (Button) findViewById(R.id.cancel_bt_twitter);
		mPhotosGrid = (GridView) findViewById(R.id.gridView_photos_twitter);
		mFolderList = (ListView) findViewById(R.id.listView_photos_twitter);

		mImageAdapter = new ImageAdapter(this);
		mPhotosGrid.setAdapter(mImageAdapter);

		mFolderAdapter = new FolderAdapter(getApplicationContext(),
				R.layout.row_list_folder, itemListFolder);

		new LoadFolderTask().execute();

		mFolderList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						// TODO Auto-generated method stub
						mFolderList.setVisibility(View.GONE);
						mPhotosGrid.setVisibility(View.VISIBLE);
						String path = itemListFolder.get(position);
						File targetDir = new File(path);
						File files[] = targetDir.listFiles();
						for (File file : files) {
							/*
							 * There are many kinds of file, but we only add jpg
							 * and png image
							 */
							if (!file.isDirectory()
									&& (file.toString().endsWith("jpg") || file
											.toString().endsWith("png"))) {
								mImageAdapter.add(file.getAbsolutePath());
								mImageAdapter.notifyDataSetChanged();
							}
						}
					}
				});

		mPhotosGrid.setOnItemClickListener(this);

		btnCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mPhotosGrid.isShown()) {
					mPhotosGrid.setVisibility(View.GONE);
					mFolderList.setVisibility(View.VISIBLE);
					mImageAdapter.clearData();
					mImageAdapter.notifyDataSetChanged();
				} else {
					finish();
				}
			}
		});
	}

	public void getDir(String dirPath) {
		String fileParent = "";
		File file = new File(dirPath);
		File list[] = file.listFiles();
		if (list.length != 0) {
			for (int i = 0; i < list.length; i++) {
				if (list[i].isDirectory() && hasPhoto(list[i])
						&& !list[i].isHidden()) {
					getDir(list[i].getAbsolutePath());
				} else {
					if (list[i].getName().endsWith("jpg")
							|| list[i].getName().endsWith("png")) {
						fileParent = list[i].getParent();
						break;
					}
				}
			}
			if (!fileParent.equals("")) {
				mFolderAdapter.add(fileParent);
				mFolderAdapter.notifyDataSetChanged();
			}
		}
	}

	@SuppressLint("DefaultLocale")
	private boolean hasPhoto(File tempFile) {
		if (tempFile.listFiles() == null) {
			if (tempFile.getName().toLowerCase().endsWith(("jpg"))
					|| tempFile.getName().toLowerCase().endsWith("png")) {
				return true;
			} else
				return false;
		} else {
			File list[] = tempFile.listFiles();
			for (int i = 0; i < list.length; i++) {
				if (hasPhoto(list[i]))
					return true;
			}
		}
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		String path = itemList.get(position);
		sendToNewTweetActivity(Utils.PHOTO_RESULT_CODE, path);
	}

	private void sendToNewTweetActivity(int resultCode, String photoPath) {
		Intent intent = getIntent();
		intent.putExtra("bitmap", photoPath);
		setResult(resultCode, intent);
		finish();
	}

	private class ImageAdapter extends BaseAdapter {
		private Context mContext;

		public ImageAdapter(Context context) {
			mContext = context;
		}

		public void add(String path) {
			itemList.add(path);
		}

		public void clearData() {
			itemList.clear();
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return itemList.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ImageView iv;
			if (convertView == null) { // if it's not recycled, initialize some
										// attributes
				iv = new ImageView(mContext);
				iv.setLayoutParams(new GridView.LayoutParams(100, 100));
				iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
				iv.setPadding(6, 6, 6, 6);
			} else {
				iv = (ImageView) convertView;
			}

			Bitmap bmp = decodeSampleBitmapFromUri(itemList.get(position), 100,
					100);
			iv.setImageBitmap(bmp);
			return iv;
		}

		public Bitmap decodeSampleBitmapFromUri(String path, int reqWidth,
				int reqHeight) {
			// TODO Auto-generated method stub
			Bitmap bmp = null;
			// First decode with inJustDecodeBounds = true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			bmp = BitmapFactory.decodeFile(path, options);
			return bmp;
		}

		private int calculateInSampleSize(BitmapFactory.Options options,
				int reqWidth, int reqHeight) {
			// Raw height and width of image
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {
				if (width > height) {
					inSampleSize = Math.round((float) height
							/ (float) reqHeight);
				} else {
					inSampleSize = Math.round((float) width / (float) reqWidth);
				}
			}
			return inSampleSize;
		}
	}

	private class FolderAdapter extends ArrayAdapter<String> {
		LayoutInflater inflater;
		private int textViewResourceId;
		private ArrayList<String> rows;

		public FolderAdapter(Context context, int textViewResourceId,
				ArrayList<String> rows) {
			super(context, textViewResourceId, rows);
			// TODO Auto-generated constructor stub
			this.textViewResourceId = textViewResourceId;
			this.rows = rows;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public void add(String path) {
			itemListFolder.add(path);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View row;
			row = inflater.inflate(textViewResourceId, parent, false);
			TextView tvFolderName = (TextView) row
					.findViewById(R.id.tvFolderName);
			String folderName = rows.get(position);

			/*
			 * The rows.get(position) returns the path, not name, so we must
			 * process to get the true name
			 */
			int index = 0;
			for (int i = folderName.length() - 1; i >= 0; i--) {
				if (folderName.charAt(i) == '/') {
					index = i;
					break;
				}
			}
			folderName = folderName.substring(index + 1);
			tvFolderName.setText(folderName);
			return row;
		}
	}

	private class LoadFolderTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd = ProgressDialog.show(TwitterSavedPhoto.this, "",
					"Please wait...");
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			getDir(Environment.getExternalStorageDirectory().getAbsolutePath());
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (result) {
				mFolderList.setAdapter(mFolderAdapter);
			} else {
				Toast.makeText(getApplicationContext(), "Can't load folder!",
						Toast.LENGTH_LONG).show();
			}
			pd.dismiss();
		}
	}
}
