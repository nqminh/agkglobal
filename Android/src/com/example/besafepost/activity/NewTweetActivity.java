package com.example.besafepost.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.besafepost.fragment.FragmentTwitter;
import com.example.besafepost.interfaces.IAddLocation;
import com.example.besafepost.interfaces.IDeSkipBanned;
import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.model.AlbumStorageDirFactory;
import com.example.besafepost.model.BaseAlbumDirFactory;
import com.example.besafepost.model.FroyoAlbumDirFactory;
import com.example.besafepost.task.NewTweetTask;
import com.example.besafepost.task.SendSmsDirectTask;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.GpsHelper;
import com.example.besafepost.utils.StringUtils;
import com.example.besafepost.utils.Utils;

public class NewTweetActivity extends Activity implements OnClickListener,
		IDeSkipBanned, IActivityDelegate, IAddLocation {
	private Button btnCancelNewTweet;
	private Button btnTweet;
	public static EditText edtNewTweet;
	private TextView numOfChar;
	private TextView tvSendTo;
	private TextView tvNewTweet;
	public ImageView ivAttach;
	private LinearLayout ivChooseCam;
	private LinearLayout ivChooseLoc;
	private LinearLayout ivChoosePhoto;

	private ProgressDialog pd;
	private Bitmap capturedBitmap;
	private Uri photoUri;
	private String path;
	private static String mCurrentPhotoPath;
	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
	public static boolean isTakePhoto;
	private String location;

	private double lat, lng;

	private int n = 140;
	public static boolean isDrMsg;
	private String scrName;
	private String avatarSrc;
	private String msgContent;
	private SharedPreferences delayPrefs;
	private String sendTo;
	private String replyTo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twitter_new_tweet);
		btnCancelNewTweet = (Button) findViewById(R.id.btnCancelNewTweet);
		btnTweet = (Button) findViewById(R.id.btnTweet);
		edtNewTweet = (EditText) findViewById(R.id.edtNewTweet);
		numOfChar = (TextView) findViewById(R.id.numOfChar);
		tvSendTo = (TextView) findViewById(R.id.tvSendTo);
		tvNewTweet = (TextView) findViewById(R.id.tvNewTweet);
		ivAttach = (ImageView) findViewById(R.id.ivAttach);
		ivChooseCam = (LinearLayout) findViewById(R.id.ivChooseCam);
		ivChooseLoc = (LinearLayout) findViewById(R.id.ivChooseLoc);
		ivChoosePhoto = (LinearLayout) findViewById(R.id.ivChoosePhoto);

		btnCancelNewTweet.setOnClickListener(this);
		btnTweet.setOnClickListener(this);
		ivChooseCam.setOnClickListener(this);
		ivChooseLoc.setOnClickListener(this);
		ivChoosePhoto.setOnClickListener(this);

		delayPrefs = PreferenceManager.getDefaultSharedPreferences(this);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			mAlbumStorageDirFactory = new BaseAlbumDirFactory();
		}

		edtNewTweet.addTextChangedListener(watcher);
		Intent intent = getIntent();
		sendTo = intent.getStringExtra("sendTo");
		String title = intent.getStringExtra("title");
		int size = intent.getIntExtra("size", 0);
		isDrMsg = intent.getBooleanExtra("drMsg", false);
		msgContent = intent.getStringExtra("msgContent");
		scrName = intent.getStringExtra("scrName");
		avatarSrc = intent.getStringExtra("avatar");
		replyTo = intent.getStringExtra("replyTo");
//				if (avatarSrc == null) {
//					try {
//						avatarSrc = FragmentTwitter.twitter.showUser(
//								ManagerTwitterApp.getInstance().getUserId())
//								.getOriginalProfileImageURLHttps();
//					} catch (TwitterException e1) {
//						e1.printStackTrace();
//					}
//				}
		tvSendTo.setText("To: @" + sendTo);
		tvNewTweet.setText(title);
		tvNewTweet.setTextSize(size);
		if (!StringUtils.isEmptyString(replyTo)) {
			edtNewTweet.append("@" + replyTo + " ");
		}
		if (msgContent != null) {
			edtNewTweet.append("RT @" + scrName + ":" + msgContent);
		}

	}

	TextWatcher watcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			count = edtNewTweet.getText().length();
			numOfChar.setText(String.valueOf(n - count));
			if (n - count < 0) {
				numOfChar.setTextColor(getResources()
						.getColor(R.color.red_text));
				btnTweet.setEnabled(false);
			} else {
				numOfChar.setTextColor(getResources().getColor(
						R.color.black_text));
				btnTweet.setEnabled(true);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
		}
	};

	public void sendSmsDirect() {
		DialogUtils.showProgressDialog(this, "", "Sending...", false);
		new SendSmsDirectTask(FragmentTwitter.twitter, scrName, edtNewTweet
				.getText().toString(), this).execute();

	}

	Handler myHandler = new Handler();

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnCancelNewTweet:
			finish();
			break;

		case R.id.btnTweet:
			if (isDrMsg) {
				if (Utils.isBlockedAddress(sendTo)
						|| Utils.isBlockedWord(sendTo)) {
					DialogUtils.showDialogBannedWordEnableSendAnyway(
							NewTweetActivity.this, Utils.isBlockedWord(sendTo),
							Utils.isBlockedAddress(sendTo),
							NewTweetActivity.this);
				} else if (Utils.isBlockedAddress(edtNewTweet.getText()
						.toString())
						|| Utils.isBlockedWord(edtNewTweet.getText().toString())) {
					DialogUtils.showDialogBannedWord(NewTweetActivity.this,
							Utils.isBlockedWord(edtNewTweet.getText()
									.toString()), Utils
									.isBlockedAddress(edtNewTweet.getText()
											.toString()));
				} else if (edtNewTweet.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(),
							"Please enter your message", Toast.LENGTH_LONG)
							.show();
				} else {
					functionSendSms();
				}
			} else {
				if (Utils.isBlockedAddress(edtNewTweet.getText().toString())
						|| Utils.isBlockedWord(edtNewTweet.getText().toString())) {
					DialogUtils.showDialogBannedWord(NewTweetActivity.this,
							Utils.isBlockedWord(edtNewTweet.getText()
									.toString()), Utils
									.isBlockedAddress(edtNewTweet.getText()
											.toString()));
				} else if (edtNewTweet.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(),
							"Please enter your message", Toast.LENGTH_LONG)
							.show();
				} else {
					if (delayPrefs.getInt(StringUtils.KEY_DELAY, 10) == 0) {
						updateNewTweet();

					} else {
						btnTweet.setEnabled(false);
						Intent intent = new Intent(NewTweetActivity.this,
								CountDownActivity.class);
						intent.putExtra("scrName", "@Public Tweet");
						intent.putExtra("avatarSrc", avatarSrc);
						intent.putExtra("msgContent", edtNewTweet.getText()
								.toString());
						intent.putExtra("FromFB", false);
						startActivityForResult(intent,
								Utils.START_COUNT_DOWN_FOR_NEWTWEET);
					}
				}
			}
			break;

		case R.id.ivChooseCam:
			Intent iChooseCam = new Intent(
					android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			File f = null;
			try {
				f = setUpPhotoFile();
				mCurrentPhotoPath = f.getAbsolutePath();
				iChooseCam.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
			} catch (IOException e) {
				e.printStackTrace();
				f = null;
				mCurrentPhotoPath = null;
			}
			startActivityForResult(iChooseCam, Utils.TAKE_PICTURE);
			break;
		case R.id.ivChooseLoc:
			pd = ProgressDialog.show(NewTweetActivity.this, "",
					"Getting your location...");
			final GpsHelper gpsHelper = new GpsHelper(NewTweetActivity.this);
			final Geocoder gCoder = new Geocoder(NewTweetActivity.this);
			new Thread() {
				@Override
				public void run() {

					lat = gpsHelper.getLocation().getLatitude();
					lng = gpsHelper.getLocation().getLongitude();
					Log.i("LAT LONG", lat + ", " + lng);

					ArrayList<Address> addresses = null;
					try {
						addresses = (ArrayList<Address>) gCoder
								.getFromLocation(lat, lng, 1);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						myHandler.post(new Runnable() {

							@Override
							public void run() {
								Toast.makeText(NewTweetActivity.this,
										"Can't get location", Toast.LENGTH_LONG)
										.show();

							}
						});
					}
					if (pd != null && pd.isShowing()) {
						pd.dismiss();
					}
					if (addresses != null && addresses.size() > 0) {
						location = (" -at "
								+ addresses.get(0).getThoroughfare() + ", "
								+ addresses.get(0).getSubAdminArea() + ", "
								+ addresses.get(0).getAdminArea() + ", " + addresses
								.get(0).getCountryName());
						myHandler.post(new Runnable() {

							@Override
							public void run() {
								DialogUtils.showDialogWhenGetLocation(
										NewTweetActivity.this,
										NewTweetActivity.this);
							}
						});
					} else {
						myHandler.post(new Runnable() {

							@Override
							public void run() {
								Toast.makeText(NewTweetActivity.this,
										"Can't get location", Toast.LENGTH_LONG)
										.show();
							}
						});
					}

				}
			}.start();

			break;

		case R.id.ivChoosePhoto:
			if (!Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED)) {
				Toast.makeText(getApplicationContext(),
						"SDCard is not mounted!", Toast.LENGTH_SHORT).show();
			} else {
				Intent iChoosePhoto = new Intent(NewTweetActivity.this,
						TwitterSavedPhoto.class);
				startActivityForResult(iChoosePhoto,
						Utils.PICK_EXISTING_PHOTO_RESULT_CODE);
			}
			break;

		default:
			break;
		}
	}

	private File setUpPhotoFile() throws IOException {
		File f = createImageFile();
		mCurrentPhotoPath = f.getAbsolutePath();

		return f;
	}

	@SuppressLint("SimpleDateFormat")
	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = "IMG_" + timeStamp + "_";
		File albumF = getAlbumDir();
		File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
		return imageF;
	}

	private File getAlbumDir() {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {

			storageDir = mAlbumStorageDirFactory
					.getAlbumStorageDir(getAlbumName());

			if (storageDir != null) {
				if (!storageDir.mkdirs()) {
					if (!storageDir.exists()) {
						Log.d("CameraSample", "failed to create directory");
						return null;
					}
				}
			}

		} else {
			Log.v(getString(R.string.app_name),
					"External storage is not mounted READ/WRITE.");
		}

		return storageDir;
	}

	/* Photo album for this application */
	private String getAlbumName() {
		return getString(R.string.album_name);
	}

	private void handleBigCameraPhoto() {
		if (mCurrentPhotoPath != null) {
			setPic();
			galleryAddPic();
		}
	}

	private void setPic() {

		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		int targetW = 300;
		int targetH = 300;

		/* Get the size of the image */
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
		int photoW = Math.min(bmOptions.outWidth, 2048);
		int photoH = Math.min(bmOptions.outHeight, 2048);

		/* Figure out which way needs to be reduced less */
		int scaleFactor = 1;
		if ((targetW > 0) || (targetH > 0)) {
			scaleFactor = Math.min(photoW / targetW, photoH / targetH);
		}

		/* Set bitmap options to scale the image decode target */
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
		Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

		/* Associate the Bitmap to the ImageView */
		ivAttach.setImageBitmap(bitmap);
		/* Get the captured image */
		capturedBitmap = bitmap;
	}

	// -Trong-

	private void galleryAddPic() {
		Intent mediaScanIntent = new Intent(
				"android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		File f = new File(mCurrentPhotoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Utils.TAKE_PICTURE) {
			if (resultCode == Activity.RESULT_OK) {
				// Trong
				handleBigCameraPhoto();

				new ImageCompressTask().execute();
				isTakePhoto = true;
			} else {
				Toast.makeText(getApplicationContext(), "Photo not taken",
						Toast.LENGTH_LONG).show();
			}
		}

		if (requestCode == Utils.PICK_EXISTING_PHOTO_RESULT_CODE) {
			if (resultCode == Utils.PHOTO_RESULT_CODE) {
				path = data.getStringExtra("bitmap");
				Log.i("PATH", path);
				//				new ImageLoaderTask().execute();
				photoUri = Uri.parse(path);
				ivAttach.setImageURI(photoUri);

				btnTweet.setEnabled(true);
				isTakePhoto = false;
			} else {
				btnTweet.setEnabled(true);
				Toast.makeText(getApplicationContext(),
						"You have not chosen a photo", Toast.LENGTH_LONG)
						.show();
			}
		}
		if (requestCode == Utils.START_COUNT_DOWN_FOR_SEND_SMS) {
			btnTweet.setEnabled(true);
			DialogUtils.dismissDialog();
			if (resultCode == RESULT_OK) {
				sendSmsDirect();
			}
		}
		if (requestCode == Utils.START_COUNT_DOWN_FOR_NEWTWEET) {
			btnTweet.setEnabled(true);
			DialogUtils.dismissDialog();
			if (resultCode == RESULT_OK) {
				updateNewTweet();
			}
		}
	}

	private void updateNewTweet() {
		DialogUtils.dismissDialog();
		String status = edtNewTweet.getText().toString();
		if (status.trim().length() > 0 && ivAttach.getDrawable() == null) {
			// Update status only
			DialogUtils.showProgressDialog(NewTweetActivity.this, "",
					"Updating status", false);
			new NewTweetTask(FragmentTwitter.twitter, null, status, this)
					.execute();
		} else if (ivAttach.getDrawable() != null) {
			// Post image
			DialogUtils.showProgressDialog(NewTweetActivity.this, "",
					"Updating status", false);
			if (isTakePhoto) {
				new NewTweetTask(FragmentTwitter.twitter, new File(
						mCurrentPhotoPath), status, this).execute();
			} else {
				new NewTweetTask(FragmentTwitter.twitter, new File(
						String.valueOf(photoUri)), status, this).execute();
			}

		} else if (status.trim().length() == 0
				&& ivAttach.getDrawable() == null) {
			Toast.makeText(getApplicationContext(),
					"Please enter status message", Toast.LENGTH_SHORT).show();
		}
	}

	private class ImageCompressTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			if (capturedBitmap.getHeight() > 600) {
				Log.i("IMG CAPTURED HEIGHT, WIDTH",
						"" + capturedBitmap.getHeight() + ","
								+ capturedBitmap.getWidth());
				capturedBitmap = AdjustPhotoActivity.scaleDownBitmap(
						capturedBitmap, 300, getApplicationContext());
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				capturedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
				Log.i("HEIGHT, WIDTH", "" + capturedBitmap.getHeight() + ","
						+ capturedBitmap.getWidth());
			} else {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				capturedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
				Log.i("HEIGHT 1, WIDTH 1", "" + capturedBitmap.getHeight()
						+ "," + capturedBitmap.getWidth());
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void sendAnyway() {
		functionSendSms();
	}

	private void functionSendSms() {
		if (delayPrefs.getInt("Delay", 10) == 0) {
			sendSmsDirect();
		} else {
			Intent intent = new Intent(NewTweetActivity.this,
					CountDownActivity.class);
			intent.putExtra("scrName", "@" + scrName);
			intent.putExtra("avatarSrc", avatarSrc);
			intent.putExtra("msgContent", edtNewTweet.getText().toString());
			intent.putExtra("FromFB", false);
			startActivityForResult(intent, Utils.START_COUNT_DOWN_FOR_SEND_SMS);
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

	}

	@Override
	public void onGetSuccess(String requestCode) {
		if (requestCode.equals(Utils.REQUEST_POST_NEW_TWEET)) {
			setResult(RESULT_OK);
			finish();
		}
		if (requestCode.equals(Utils.REQUEST_SEND_DIRECT_SMS)) {
			setResult(RESULT_OK);
			finish();
		}
		DialogUtils.dismissDialog();
	}

	@Override
	public void onGetFailure(String requestCode) {
		if (requestCode.equals(Utils.REQUEST_POST_NEW_TWEET)) {
			Toast.makeText(this, "Tweet Failure", Toast.LENGTH_SHORT).show();
		}
		if (requestCode.equals(Utils.REQUEST_SEND_DIRECT_SMS)) {
			Toast.makeText(this, "Tweet Failure", Toast.LENGTH_SHORT).show();
		}
		DialogUtils.dismissDialog();
	}

	@Override
	public void addLocation() {
		edtNewTweet.append(location);
	}
}
