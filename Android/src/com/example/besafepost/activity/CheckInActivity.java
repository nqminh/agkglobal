package com.example.besafepost.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.utils.FacebookUtils;
import com.example.besafepost.utils.GpsHelper;
import com.example.besafepost.utils.StringUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class CheckInActivity extends Activity implements OnItemClickListener,
		IActivityDelegate {
	private GoogleMap mapView;
	private double lat, lon;
	private ListView listPlace;
	private ProgressDialog dialog;
	public static final int REQUEST_CODE_STATUS_WITH_PLACE = 0;
	private String textPost = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkin_act);
		listPlace = (ListView) findViewById(R.id.listPlace);
		textPost = getIntent().getStringExtra("text_post");
		// get current lat long

		GpsHelper gpsHelper = new GpsHelper(this);
		lat = gpsHelper.getLocation().getLatitude();
		lon = gpsHelper.getLocation().getLongitude();
		setMapview();

		dialog = ProgressDialog.show(CheckInActivity.this, "", "Loading...",
				true, false);
//		dialog.show();
		FacebookUtils.getPlace(this, String.valueOf(lat), String.valueOf(lon),
				listPlace, StringUtils.LIMIT_PLACE, this);
		listPlace.setOnItemClickListener(this);
		findViewById(R.id.btnCancel).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	@SuppressLint("NewApi")
	private void setMapview() {
		mapView = ((MapFragment) getFragmentManager()
				.findFragmentById(R.id.map)).getMap();
		// LatLng point = new LatLng((int) (lat * 1E6), (int) (lon * 1E6));
		LatLng point = new LatLng(lat, lon);
		mapView.addMarker(new MarkerOptions().position(point)

		.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.pin_location_green)));

		// Move the camera instantly to hamburg with a zoom of 15.
		mapView.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 15));

		// Zoom in, animating the camera.
		// mapView.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// TODO Auto-generated method stub
		Intent i = new Intent(this, StatusWithPlaceActivity.class);
		i.putExtra("place", FacebookUtils.listItemPlace.get(position).getName());
		i.putExtra("placeId", FacebookUtils.listItemPlace.get(position)
				.getPlaceId());
		i.putExtra("icon_link", FacebookUtils.listItemPlace.get(position)
				.getIconLink());
		i.putExtra("text_post", textPost);
		startActivityForResult(i, REQUEST_CODE_STATUS_WITH_PLACE);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK
				&& requestCode == REQUEST_CODE_STATUS_WITH_PLACE) {
			setResult(RESULT_OK);
			finish();
		}
		if (resultCode == RESULT_CANCELED
				&& requestCode == REQUEST_CODE_STATUS_WITH_PLACE) {
			finish();
		}
	}

	@Override
	public void onGetSuccess(String requestCode) {
		// TODO Auto-generated method stub
		dialog.dismiss();
	}

	@Override
	public void onGetFailure(String requestCode) {
		// TODO Auto-generated method stub

	}
}
