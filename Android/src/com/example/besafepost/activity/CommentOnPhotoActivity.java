package com.example.besafepost.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.task.DownloadImageTask;
import com.example.besafepost.utils.DialogUtils;
import com.example.besafepost.utils.FacebookUtils;
import com.example.besafepost.utils.StringUtils;
import com.example.besafepost.utils.Utils;

public class CommentOnPhotoActivity extends Activity implements
		OnClickListener, IActivityDelegate {
	Button btCancel, btCmt;
	ImageView image;
	String photoId = "";
	EditText edtCmt;

	private SharedPreferences delayPrefs;

//	private String profileName;
	private String profileIcon;
	private int cmtCount;
	private String srcImgCurrent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.comment_photo_activity);
		image = (ImageView) findViewById(R.id.image);
		btCancel = (Button) findViewById(R.id.btnCancel);
		btCmt = (Button) findViewById(R.id.btnCmt);
		edtCmt = (EditText) findViewById(R.id.edtCmt);
		// edtCmt.requestFocus();
		btCancel.setOnClickListener(this);
		btCmt.setOnClickListener(this);
		Bundle extras = getIntent().getExtras();
		srcImgCurrent = extras.getString("srcImgCurrent");
//		profileName = extras.getString("Post profileName");
		profileIcon =extras.getString("Post profileIcon");
		photoId = extras.getString("photoId");
		cmtCount = extras.getInt("cmtCount");
//		 image.setImageBitmap(bmp);
		new DownloadImageTask(image).execute(srcImgCurrent);
		delayPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		// get profile of user
//		FacebookUtils.getBitmapFromSrc(profileIcon);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent i = new Intent();
		i.putExtra("cmtCount", cmtCount);
		setResult(RESULT_OK, i);
		finish();
	}

	@Override
	public void onClick(View v) {
		if (v == btCancel) {
			Intent i = new Intent();
			i.putExtra("cmtCount", cmtCount);
			setResult(RESULT_OK, i);
			finish();
		}
		if (v == btCmt) {
			if (Utils.isBlockedAddress(edtCmt.getText().toString())
					|| Utils.isBlockedWord(edtCmt.getText().toString())) {
				DialogUtils.showDialogBannedWord(CommentOnPhotoActivity.this,
						Utils.isBlockedWord(edtCmt.getText().toString()),
						Utils.isBlockedAddress(edtCmt.getText().toString()));
			} else {
				if (!StringUtils.isEmptyString(edtCmt.getText().toString())) {

					if (delayPrefs.getInt("Delay", 10) == 0) {
						postCommentOnPhoto();
					} else {
						Intent intent = new Intent(CommentOnPhotoActivity.this,
								CountDownActivity.class);
						intent.putExtra("scrName","Timeline");
						intent.putExtra("msgContent", edtCmt.getText()
								.toString());
						intent.putExtra("avatarSrc", profileIcon);
						intent.putExtra("FromFB", true);
						startActivityForResult(intent,
								Utils.START_COUNTDOWN_FOR_POST_COMMENT_ON_PHOTO);
					}
				} else {
					Toast.makeText(this, "Please enter your comment",
							Toast.LENGTH_LONG).show();
				}
			}
		}
	}

	private void postCommentOnPhoto() {
		FacebookUtils.postComment(edtCmt.getText().toString(), photoId, this);
		edtCmt.setText("");
		Utils.hideKeyboard(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == Utils.START_COUNTDOWN_FOR_POST_COMMENT_ON_PHOTO) {
			if (resultCode == RESULT_OK) {
				postCommentOnPhoto();
			}
		}
	}

	@Override
	public void onGetSuccess(String requestCode) {
		if (requestCode.equalsIgnoreCase(Utils.REQUEST_POST_COMMENT)) {
			Toast.makeText(this, "Post comment successful", Toast.LENGTH_LONG)
					.show();
			cmtCount++;
			Intent i = new Intent();
			i.putExtra("cmtCount", cmtCount);
			setResult(RESULT_OK, i);
			finish();
		}
	}

	@Override
	public void onGetFailure(String requestCode) {
		if (requestCode.equalsIgnoreCase(Utils.REQUEST_POST_COMMENT)) {
			Toast.makeText(this, "Post comment not successful",
					Toast.LENGTH_LONG).show();
		}

	}
}
