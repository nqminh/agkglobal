package com.example.besafepost.interfaces;

import com.example.besafepost.task.LoadListContactsTask;

public interface IDelegateLoadListContactsTask {

	void onGetStart(LoadListContactsTask task);

	void onGetProgress(LoadListContactsTask task);

	void onGetSuccess(LoadListContactsTask task);

	void onGetFailure(LoadListContactsTask task);
}
