package com.example.besafepost.interfaces;

public abstract interface IActivityDelegate {

	public abstract void onGetSuccess(String requestCode);

	public abstract void onGetFailure(String requestCode);

}
