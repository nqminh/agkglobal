package com.example.besafepost.interfaces;

public interface IdeResumeFromHome {
	public void sendNow();

	public void stop();
}
