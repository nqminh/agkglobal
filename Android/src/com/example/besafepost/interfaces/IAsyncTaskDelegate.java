package com.example.besafepost.interfaces;

import com.example.besafepost.task.AbstractTask;

public interface IAsyncTaskDelegate<T extends AbstractTask>{
	void onGetStart(T task);

	void onGetSuccess(T task);

	void onGetFailure(T task, Exception exception);
}
