package com.example.besafepost.utils;

import java.io.InputStream;

import android.content.Context;

public class FileUtils {

	public static String ARTICLE_BEGIN_LIKES = "templateArticleBeginLikes.txt";
	public static String ARTICLE_BEGIN_NORMAL = "templateArticleBeginNormal.txt";
	public static String ARTICLE_BEGIN_TAGGED = "templateArticleBeginTagged.txt";
	public static String ARTICLE_BODY_CHECKIN = "templateArticleBodyCheckin.txt";
	public static String ARTICLE_BODY_PHOTO = "templateArticleBodyPhoto.txt";
	public static String ARTICLE_BODY_SHARE_LINK = "templateArticleBodySharedLink.txt";
	public static String ARTICLE_BODY_SHARE_LINK_NO_ICON = "templateArticleBodySharedLinkNoIcon.txt";
	public static String ARTICLE_BODY_STATUS = "templateArticleBodyStatus.txt";
	public static String ARTICLE_BODY_VIDEO = "templateArticleBodyVideo.txt";
	public static String ARTICLE_END_0 = "templateArticleEnd0.txt";
	public static String ARTICLE_END_1 = "templateArticleEnd1.txt";
	public static String ARTICLE_END_2 = "templateArticleEnd2.txt";
	public static String ARTICLE_END_3 = "templateArticleEnd3.txt";
	public static String ARTICLE_END_COMMENT_0 = "templateArticleEndComment0.txt";
	public static String ARTICLE_END_COMMENT_1 = "templateArticleEndComment1.txt";
	public static String ARTICLE_END_COMMENT_2 = "templateArticleEndComment2.txt";
	public static String ARTICLE_END_COMMENT_3 = "templateArticleEndComment3.txt";
	public static String HTML_BEGIN_COMMENT = "templateHTMLBeginComment.txt";
	public static String HTML_COMMENT_OTHER = "templateHTMLCommentOther.txt";
	public static String HTML_CONTENT_STATUS = "templateHTMLContentStatus.txt";
	public static String TAG_SHOW_LINK = "templateShowLink.txt";
	public static String ARTICLE_TWEET = "templateArticleTweet.txt";
	public static String ARTICLE_TWEET_IMG = "templateArticleTweetPhoto.txt";
	public static String ARTICLE_TWEET_VIDEO = "templateArticleTweetVideo.txt";
	public static String ARTICLE_RETWEET = "templateArticleRetweet.txt";
	public static String HTML_END_COMMENT = "templateHTMLEndComment.txt";
	public static String ARTICLE_END_NO_LIKE = "templateArticleEndNoLike.txt";
	public static String ARTICLE_FOR_UNDO = "templateArticleForUndo.txt";
	public static String ARTICLE_SHARE_PHOTO = "templateArticleBeginSharePhoto.txt";
	public static String ARTICLE_BODY_SHARED_LINK_VIDEO = "templateArticleBodySharedLinkVideo.txt";
	public static String TWEET_BEGIN_NORMAL = "tempTweetBeginNormal.txt";
	public static String TWEET_BEGIN_RETWEET = "tempTweetBeginRetweet.txt";
	public static String TWEET_BODY_STATUS = "tempTweetBodyStatus.txt";
	public static String TWEET_BODY_STATUS_RETWEET = "tempTweetBodyStatusRetweet.txt";
	public static String TWEET_BODY_STATUS_RETWEET_FAVORITE = "tempTweetBodyStatusRetweetFavorite.txt";
	public static String TEMPLATE_SEND_MAIL_TWEET = "tempSendMailTweet.txt";
	public static String TWEET_BODY_PHOTO = "tempTweetBodyPhoto.txt";
	public static String TEMPLATE_COMMENT = "templateComment.txt";

	public static String readFileFromAssets(String fileName, Context context) {

		String stg = "";

		try {
			InputStream inputStream = context.getAssets().open(fileName);
			int size = inputStream.available();
			byte[] buffer = new byte[size];
			inputStream.read(buffer);
			inputStream.close();
			stg = new String(buffer);
		} catch (Exception e) {
			// TODO: handle exception
		}

		return stg;
	}

}
