package com.example.besafepost.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.text.TextUtils;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.besafepost.activity.R;
import com.example.besafepost.adapter.PlaceAdapter;
import com.example.besafepost.fragment.FragmentPost;
import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.model.ArticleItem;
import com.example.besafepost.model.Comment;
import com.example.besafepost.model.ItemFriendList;
import com.example.besafepost.model.ItemPlace;
import com.example.besafepost.model.PhotoDetails;
import com.example.besafepost.model.gsonobject.Place;
import com.example.besafepost.model.gsonobject.Post;
import com.example.besafepost.model.gsonobject.Post.Attachment;
import com.example.besafepost.model.gsonobject.Post.Attachment.MediaItem;
import com.example.besafepost.model.gsonobject.Profile;
import com.example.besafepost.singleton.LoginObject;
import com.example.besafepost.singleton.ManagerFacebookApp;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Request.Callback;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class FacebookUtils {

	public final static String APP_ID = "328711320597060";
	private static String newFeedsString;
	public static List<ArticleItem> listItemTemp = new ArrayList<ArticleItem>();
	public static List<ArticleItem> listItem = new ArrayList<ArticleItem>();
	public static List<Comment> listComment = new ArrayList<Comment>();
	public static List<Comment> listCommentTemp = new ArrayList<Comment>();
	private static Context context = null;
	private static WebView webView;
	private static ListView listPlace;
	public static ArrayList<ItemPlace> listItemPlace = new ArrayList<ItemPlace>();
	private static ArrayList<PhotoDetails> listPhotoDetails = new ArrayList<PhotoDetails>();
	private static ArrayList<ItemFriendList> listFriendList = new ArrayList<ItemFriendList>();
	public static Bitmap bmp;
	public static int task_get_icon_count;
	public static boolean no_task_get_icon = true;
	private static boolean hasNewStory = false;

	public static void getPlace(Context context, String lat, String lon, ListView listPlace,
			int LIMIT, final IActivityDelegate mDelegate) {
		FacebookUtils.context = context;
		FacebookUtils.listPlace = listPlace;
		String fqlQueryPlace = "SELECT pic, geometry, page_id, name, display_subtext, description, checkin_count FROM place WHERE distance(latitude, longitude, \""
				+ lat
				+ "\",\""
				+ lon
				+ "\") < 1000 ORDER BY distance(latitude, longitude, \""
				+ lat + "\",\"" + lon + "\") ASC LIMIT " + String.valueOf(LIMIT);

		Bundle params = new Bundle();
		params.putString("q", fqlQueryPlace);
		Session session = Session.getActiveSession();

		Request request = new Request(session, "/fql", params, HttpMethod.GET,
				new Request.Callback() {

					@Override
					public void onCompleted(Response response) {
						mDelegate.onGetSuccess("");
						listItemPlace.clear();
//						Log.i(Log.TAG, response.toString());
						GraphObject graphObject = response.getGraphObject();
						if (graphObject != null) {
							JSONObject jsonResponse = graphObject.getInnerJSONObject();
							try {
								JSONArray jsonArray = jsonResponse.getJSONArray("data");

								for (int i = 0; i < jsonArray.length(); i++) {
									JSONObject jsonObj = jsonArray.getJSONObject(i);
									String iconLink = jsonObj.getString("pic");
									String name = jsonObj.getString("name");
									String subText = jsonObj.getString("display_subtext");
									String placeId = jsonObj.getString("page_id");
									ItemPlace itemPlace = new ItemPlace();
									itemPlace.setPlaceId(placeId);
									itemPlace.setIconLink(iconLink);
									itemPlace.setName(name);
									itemPlace.setSubText(subText);
									listItemPlace.add(itemPlace);
								}

							} catch (Exception e) {
								Log.d("Exception", e.toString());
							}
							PlaceAdapter adapter = new PlaceAdapter(FacebookUtils.context,
									R.layout.row_list_place, listItemPlace);
							FacebookUtils.listPlace.setAdapter(adapter);

						}
					}
				});

		Request.executeBatchAsync(request);

	}

	// public static void getListGroups(final IDelegate mIDelegate) {
	// Bundle params = new Bundle();
	// Session session = Session.getActiveSession();
	// Request request = new Request(session, "me/groups", params,
	// HttpMethod.GET, new Callback() {
	//
	// @Override
	// public void onCompleted(Response response) {
	// GraphObject graphObject = response.getGraphObject();
	// }
	// });
	// Request.executeBatchAsync(request);
	//
	// }

	public static void getListFriendList(final IActivityDelegate mIDelegate) {
		Bundle params = new Bundle();
		params.putString("q",
				"SELECT flid, owner, name,owner_cursor,type FROM friendlist WHERE owner=me()");
		Session session = Session.getActiveSession();
		Request request = new Request(session, "/fql", params, HttpMethod.GET, new Callback() {

			@Override
			public void onCompleted(Response response) {
				GraphObject graphObject = response.getGraphObject();
				listFriendList.clear();
				if (graphObject != null) {
					JSONObject jsonResponse = graphObject.getInnerJSONObject();
					try {
						JSONArray data = jsonResponse.getJSONArray("data");
						for (int i = 0; i < data.length(); i++) {
							JSONObject jsonFriendList = data.getJSONObject(i);

							ItemFriendList item = new ItemFriendList();
							item.setIdFriendList(jsonFriendList.getLong("flid"));
							item.setNameFriendList(jsonFriendList.getString("name"));
							listFriendList.add(item);
						}

					} catch (JSONException e) {
						e.printStackTrace();
					}
					mIDelegate.onGetSuccess(Utils.REQUEST_GET_LIST_FRIENDLIST);
				} else {
					mIDelegate.onGetFailure(Utils.REQUEST_GET_LIST_FRIENDLIST);
				}
			}
		});
		Request.executeBatchAsync(request);

	}

	public static ArrayList<ItemFriendList> getListFriendList() {
		return listFriendList;
	}

	/**
	 * get new feed from facebook
	 * 
	 * @param context
	 * @param mWebView
	 * @param LIMIT
	 * @param mDelegate
	 * @param isRefresh
	 */
	public static void getNewFeeds(Context context, WebView mWebView, int LIMIT,
			final IActivityDelegate mDelegate, final boolean isRefresh) {
		// Log.i(TAG, "get new feed");
		task_get_icon_count = 0;
		no_task_get_icon = true;
		FacebookUtils.context = context;
		webView = mWebView;
		JSONObject jsonFQL = new JSONObject();
		String fqlQueryItem = "SELECT permalink,share_info,app_id, post_id, actor_id, target_id, message, source_id, comments, attachment, created_time, updated_time, type, likes, attribution, action_links, place, targeting,comment_info,like_info FROM stream WHERE filter_key in (SELECT filter_key FROM stream_filter WHERE uid = me() AND type = \'newsfeed\') AND is_hidden = 0 ORDER BY created_time DESC LIMIT "
				+ String.valueOf(LIMIT);
		String fqlQueryDetail = "SELECT id, name, pic_square, pic_big FROM profile WHERE id IN (SELECT actor_id FROM #query1)";
		String fqlQueryLatLon = "SELECT page_id, name, description, geometry, pic, pic_crop, pic_large, pic_small, content_age, display_subtext, is_unclaimed FROM place WHERE page_id IN (SELECT place FROM #query1)";
		String fqlQueryTarget = "SELECT id, name FROM profile WHERE id IN (SELECT target_id FROM #query1)";
		try {
			jsonFQL.put("query1", fqlQueryItem);
			jsonFQL.put("query2", fqlQueryDetail);
			jsonFQL.put("query3", fqlQueryLatLon);
			jsonFQL.put("query4", fqlQueryTarget);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		Bundle params = new Bundle();
		params.putString("q", jsonFQL.toString());
		Session session = Session.getActiveSession();

		Request request = new Request(session, "/fql", params, HttpMethod.GET,
				new Request.Callback() {

					@Override
					public void onCompleted(Response response) {
						// Log.i(Log.TAG, response.toString());
						GraphObject graphObject = response.getGraphObject();
						if (graphObject != null) {
							JSONObject jsonObject = graphObject.getInnerJSONObject();
							if (isRefresh) {
								listItem.clear();
								StringUtils.clearUserComment();
								StringUtils.clearUserLike();
							}
							listItemTemp.clear();

							// analyzeNewFeedRespond(jsonObject);
							newAnalyzeNewFeedRespond(jsonObject);
							getIconShared(isRefresh);
						}
						mDelegate.onGetSuccess("");

					}
				});

		Request.executeBatchAsync(request);
	}

	private static void analyzeNewFeedRespond(JSONObject jsonObject) {
		try {
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			JSONArray query1 = jsonArray.getJSONObject(0).getJSONArray("fql_result_set");
			JSONArray query2 = jsonArray.getJSONObject(1).getJSONArray("fql_result_set");
			JSONArray query3 = jsonArray.getJSONObject(2).getJSONArray("fql_result_set");
			JSONArray query4 = jsonArray.getJSONObject(3).getJSONArray("fql_result_set");

			if (listItem.size() > 0
					&& query1.length() > 0
					&& query1.getJSONObject(0).has("post_id")
					&& !query1.getJSONObject(0).getString("post_id")
							.equals(listItem.get(0).getPostId())) {
				hasNewStory = true;
			} else {
				hasNewStory = false;
			}
			int startGetItem = 0;
			if (query1.length() - StringUtils.LIMIT_ADVANCE > 0)
				startGetItem = query1.length() - StringUtils.LIMIT_ADVANCE;
			for (int i = startGetItem; i < query1.length(); i++) {
				JSONObject item1 = query1.getJSONObject(i);
//				 Log.i(Log.TAG, item1.toString());

				// variable set to articleItem
				String appId = "", sourceId = "", actor_id = "", place = "";
				String targetId = "", permalink = "", message = "";
				String profileName = "", profileIcon = "", nameTarget = "";
				String timeStamp = "", placeName = "", lat = "", lon = "", postId = "";
				String typeAttachment = "", shareLink = "", shareContent = "";
				String shareVia = "", shareTitle = "", source_url = "", fbObjectId = "";
				String sharePhotoOf = "", srcIconImage = "";
				int commentCount = 0, likeCount = 0, type = 0;
				boolean userLiked = false, canShare = false;
				ArrayList<String> listFbidPhoto = new ArrayList<String>();

				if (item1.has("app_id"))
					appId = item1.getString("app_id");
				if (item1.has("source_id"))
					sourceId = item1.getString("source_id");
				if (item1.has("actor_id"))
					actor_id = item1.getString("actor_id");
				if (item1.has("place"))
					place = item1.getString("place");
				if (item1.has("target_id"))
					targetId = item1.getString("target_id");
				if (item1.has("permalink"))
					permalink = item1.getString("permalink");
				if (item1.has("message"))
					message = item1.getString("message");
				if (item1.has("share_info") && item1.getJSONObject("share_info").has(""))
					canShare = item1.getJSONObject("share_info").getBoolean("can_share");

				// target
				if (!StringUtils.isEmptyString(targetId)) {
					for (int j = 0; j < query4.length(); j++) {
						JSONObject item4 = query4.getJSONObject(j);
						if (item4 != null && item4.has("id")
								&& item4.getString("id").equalsIgnoreCase(targetId)
								&& item4.has("name")) {
							nameTarget = item4.getString("name");
							break;
						}
					}
				}

				// profile icon and name
				for (int j = 0; j < query2.length(); j++) {
					JSONObject item2 = query2.getJSONObject(j);
					if (item2 != null && item2.has("id") && actor_id.equals(item2.getString("id"))) {
						if (item2.has("pic_square"))
							profileIcon = analyseProfileLink(item2.getString("pic_square"));
						if (item2.has("name"))
							profileName = item2.getString("name");
						break;
					}

				}

				// Log.d(Log.TAG, profileName + " : " + item1.toString());
				// placename, latitude and longitude
				if (!StringUtils.isEmptyString(place)) {
					for (int k = 0; k < query3.length(); k++) {
						JSONObject item3 = query3.getJSONObject(k);
						if (item3.has("page_id") && place.equals(item3.getString("page_id"))) {
							if (item3.has("name"))
								placeName = item3.getString("name");
							if (item3.has("geometry") && item3.getJSONObject("geometry") != null
									&& item3.getJSONObject("geometry").has("coordinates")) {
								JSONArray coordinates = item3.getJSONObject("geometry")
										.getJSONArray("coordinates");
								if (coordinates.length() > 1) {
									lon = coordinates.getString(0);
									lat = coordinates.getString(1);
								}
							}
							break;
						}
					}
				}

				// time
				if (item1.has("created_time")) {
					Date date = new Date();
					date.setTime(Long.parseLong(item1.getString("created_time") + "000"));
					timeStamp = DateUtils.timeAgoInWords(date);
				}

				// comments count
				if (item1.has("comment_info") && item1.getJSONObject("comment_info") != null
						&& item1.getJSONObject("comment_info").has("comment_count"))
					commentCount = item1.getJSONObject("comment_info").getInt("comment_count");

				if (item1.has("like_info") && item1.getJSONObject("like_info") != null) {
					JSONObject likeInfo = item1.getJSONObject("like_info");
					// like count
					if (likeInfo.has("like_count"))
						likeCount = item1.getJSONObject("like_info").getInt("like_count");

					// user like
					if (likeInfo.has("user_likes"))
						userLiked = item1.getJSONObject("like_info").getBoolean("user_likes");
				}

				// postId
				if (item1.has("post_id"))
					postId = item1.getString("post_id");

				// post type
				if (item1.has("type"))
					type = item1.getInt("type");

				// attachment analyze
				if (item1.has("attachment") && item1.getJSONObject("attachment") != null) {
					JSONObject attachment = item1.getJSONObject("attachment");
					if (attachment.has("href")) {
						shareLink = attachment.getString("href");
					}

					if (attachment.has("fb_object_id")) {
						fbObjectId = attachment.getString("fb_object_id");
					}

					if (attachment.has("properties")
							&& attachment.getJSONArray("properties").length() > 0
							&& attachment.getJSONArray("properties").getJSONObject(0).has("text")) {
						sharePhotoOf = attachment.getJSONArray("properties").getJSONObject(0)
								.getString("text");
					}

					if (attachment.has("media") && attachment.getJSONArray("media") != null) {
						JSONArray medialist = attachment.getJSONArray("media");
						if (medialist.length() > 0) {
							for (int j = 0; j < medialist.length(); j++) {
								JSONObject mediaItem = medialist.getJSONObject(j);

								if (medialist.getJSONObject(j).has("photo")
										&& medialist.getJSONObject(j).getJSONObject("photo") != null
										&& medialist.getJSONObject(j).getJSONObject("photo")
												.has("fbid")) {
									String fbidPhoto = medialist.getJSONObject(j)
											.getJSONObject("photo").getString("fbid");
									listFbidPhoto.add(fbidPhoto);
								}

								if (j == 0) {
									if (mediaItem.has("type"))
										typeAttachment = mediaItem.getString("type");
									if (mediaItem.has("src")) {
										srcIconImage = mediaItem.getString("src");
										srcIconImage = srcIconImage.replace("_s.", "_n.");
									}
									if (mediaItem.has("video")
											&& mediaItem.getJSONObject("video") != null
											&& mediaItem.getJSONObject("video").has("source_url"))
										source_url = mediaItem.getJSONObject("video").getString(
												"source_url");
								}
							}
						}
					}
					if (attachment.has("description"))
						shareContent = attachment.getString("description");
					if (attachment.has("caption"))
						shareVia = attachment.getString("caption");
					if (attachment.has("name"))
						shareTitle = attachment.getString("name");

					// set permarlink with the post share photo of any one
					if (type == 80 && !sharePhotoOf.isEmpty() && attachment.has("media")
							&& attachment.getJSONArray("media") != null) {
						// this is share photo from any one or group
						if (attachment.getJSONArray("media").length() > 0
								&& attachment.getJSONArray("media").getJSONObject(0).has("href")) {

							permalink = attachment.getJSONArray("media").getJSONObject(0)
									.getString("href");

//							Log.d(Log.TAG, profileName + " : " + permalink);
						}
					}
				}
				// not use items has isduplicateItem=true
				boolean isduplicateItem = false;
				// not use items hide
				boolean isHide = false;
				// if post_id duplicate,two item is duplicate and isduplicate
				// =true
				for (int j = 0; j < listItem.size(); j++) {

					if (listItem.get(j).getPostId().equals(postId)) {
						isduplicateItem = true;
						break;
					}
				}
				// post_id duplicate,but in some case,not duplicate item
				for (int j = 0; j < listItem.size(); j++) {
					if ((listItem.get(j).getProfileName().equals(profileName) || listItem.get(j)
							.getProfileName().equals(nameTarget))
							&& (listItem.get(j).getType() == 245 || listItem.get(j).getType() == 257)) {
						isduplicateItem = false;
						break;
					}
				}
				for (int j = 0; j < FragmentPost.listPostIdHide.size(); j++) {
					if (FragmentPost.listPostIdHide.get(j).equals(postId)) {
						isHide = true;
					}
				}
				if (!isduplicateItem && !isHide) {
					ArticleItem articleItem = new ArticleItem();
					articleItem.setActorId(actor_id);
					articleItem.setPermalink(permalink);
					articleItem.setCanShare(canShare);
					articleItem.setSharePhotoOf(sharePhotoOf);
					articleItem.setNameTarget(nameTarget);
					articleItem.setAppId(appId);
					articleItem.setSourceId(sourceId);
					articleItem.setListFbidPhoto(listFbidPhoto);
					articleItem.setPlace(placeName);
					articleItem.setLat(lat);
					articleItem.setLon(lon);
					articleItem.setUserLiked(userLiked);
					articleItem.setTypeAttachment(typeAttachment);
					articleItem.setType(type);
					articleItem.setCommentCount(commentCount);
					articleItem.setLikeCount(likeCount);
					articleItem.setMessage(message);
					articleItem.setPostId(postId);
					articleItem.setProfileIcon(profileIcon);
					articleItem.setProfileName(profileName);
					articleItem.setTimeStamp(timeStamp);
					articleItem.setShareContent(shareContent);
					articleItem.setShareLink(shareLink);
					articleItem.setShareTitle(shareTitle);
					articleItem.setShareVia(shareVia);
					articleItem.setSrcIconImage(srcIconImage);
					articleItem.setFbObjectId(fbObjectId);
					articleItem.setSource_url(source_url);
					FacebookUtils.listItem.add(articleItem);
					FacebookUtils.listItemTemp.add(articleItem);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private static void newAnalyzeNewFeedRespond(JSONObject jsonObject) {
		try {
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			JSONArray query1 = jsonArray.getJSONObject(0).getJSONArray("fql_result_set");
			JSONArray query2 = jsonArray.getJSONObject(1).getJSONArray("fql_result_set");
			JSONArray query3 = jsonArray.getJSONObject(2).getJSONArray("fql_result_set");
			JSONArray query4 = jsonArray.getJSONObject(3).getJSONArray("fql_result_set");
			Gson gson = new GsonBuilder().serializeNulls().create();
			Type listType = new TypeToken<List<Post>>() {
			}.getType();
			List<Post> posts = gson.fromJson(query1.toString(), listType);

			listType = new TypeToken<List<Profile>>() {
			}.getType();
			List<Profile> profiles = gson.fromJson(query2.toString(), listType);

			listType = new TypeToken<List<Profile>>() {
			}.getType();
			List<Profile> targets	 = gson.fromJson(query4.toString(), listType);

			listType = new TypeToken<List<Place>>() {
			}.getType();
			List<Place> places = gson.fromJson(query3.toString(), listType);

			if (posts.size() > 0 && listItem.size() > 0) {
				if (!posts.get(0).postId.equals(listItem.get(0).getPostId())) {
					hasNewStory = true;
				} else {
					hasNewStory = false;
				}
			} else {
				hasNewStory = false;
			}
			int startGetItem = 0;
			if (posts.size() - StringUtils.LIMIT_ADVANCE > 0)
				startGetItem = posts.size() - StringUtils.LIMIT_ADVANCE;
			for (int i = startGetItem; i < posts.size(); i++) {
				Post post = posts.get(i);

				// variable set to articleItem
				String timeStamp = "";

				ArrayList<String> listFbidPhoto = new ArrayList<String>();

				if (!StringUtils.isEmptyString(post.targetId)) {
					for (int j = 0; j < targets.size(); j++) {
						Profile target = targets.get(j);
						if (target.id != null && target.id.equalsIgnoreCase(post.targetId)) {
							post.target = target;
							break;
						}
					}
				}

				for (int j = 0; j < profiles.size(); j++) {
					Profile profile = profiles.get(j);
					if (post.actorId.equals(profile.id)) {
						post.profile = profile;
						break;
					}
				}

				if (!TextUtils.isEmpty(post.place) && !post.place.equals("null"))
					for (int j = 0; j < places.size(); j++) {
						Place place = places.get(j);
						if (place.pageId.equals(post.place)) {
							post.placeObject = place;
						}
					}
				else
					post.place = "";

				if (post.createdTime > 0) {
					Date date = new Date();
					date.setTime(Long.parseLong(String.valueOf(post.createdTime) + "000"));
					timeStamp = DateUtils.timeAgoInWords(date);
				}

				// attachment analyze
				if (post.attachment != null) {
					Attachment attachment = post.attachment;

					if (attachment.media != null) {
						List<MediaItem> mediaItems = attachment.media;
						for (int j = 0; j < mediaItems.size(); j++) {
							MediaItem mediaItem = mediaItems.get(j);

							if (mediaItem.photo != null && !TextUtils.isEmpty(mediaItem.photo.fbid)) {
								String fbidPhoto = mediaItem.photo.fbid;
								listFbidPhoto.add(fbidPhoto);
							}
						}
					}

					String sharePhotoOf = "";
					if (attachment.properties != null && attachment.properties.size() > 0) {
						sharePhotoOf = attachment.properties.get(0).text;
					}
					// set permarlink with the post share photo of any one
					if (post.type == 80 && !sharePhotoOf.isEmpty() && attachment.media != null) {
						// this is share photo from any one or group
						if (attachment.media.size() > 0)
							post.permalink = attachment.media.get(0).href;
					}
				}
				// not use items has isduplicateItem=true
				boolean isduplicateItem = false;
				// not use items hide
				boolean isHide = false;
				// if post_id duplicate,two item is duplicate and isduplicate
				// =true
				for (int j = 0; j < listItem.size(); j++) {
					if (listItem.get(j).getPostId().equals(post.postId)) {
						isduplicateItem = true;
						break;
					}
				}
				// post_id duplicate,but in some case,not duplicate item
				for (int j = 0; j < listItem.size(); j++) {
					if ((listItem.get(j).getProfileName().equals(post.profile.name) || listItem
							.get(j).getProfileName().equals(post.target.name))
							&& (listItem.get(j).getType() == 245 || listItem.get(j).getType() == 257)) {
						isduplicateItem = false;
						break;
					}
				}
				for (int j = 0; j < FragmentPost.listPostIdHide.size(); j++) {
					if (FragmentPost.listPostIdHide.get(j).equals(post.postId)) {
						isHide = true;
					}
				}
				if (!isduplicateItem && !isHide) {
					ArticleItem articleItem = new ArticleItem();
					articleItem.setPost(post);
					articleItem.setListFbidPhoto(listFbidPhoto);
					articleItem.setTimeStamp(timeStamp);
					FacebookUtils.listItem.add(articleItem);
					FacebookUtils.listItemTemp.add(articleItem);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private static void getIconShared(final boolean isRefresh) {
		for (int i = 0; i < listItemTemp.size(); i++) {
			final int finalI = i;
			if (listItemTemp.get(i).getType() == 80
					&& !TextUtils.isEmpty(listItemTemp.get(i).getFbObjectId())
					&& !TextUtils.isEmpty(listItemTemp.get(i).getSharePhotoOf())) {
				Bundle params = new Bundle();
				task_get_icon_count++;
				no_task_get_icon = false;
				String userSharedId = "";
				for (int j = 0; j < listItemTemp.get(i).getFbObjectId().length(); j++) {
					if (listItemTemp.get(i).getFbObjectId().substring(j, j + 1).equals("_")) {
						userSharedId = listItemTemp.get(i).getFbObjectId().substring(0, j);
						break;
					}
				}
				// if (StringUtils.isEmptyString(userSharedId)) {
				// userSharedId = listItemTemp.get(i).getFbObjectId();
				// }
				params.putString("q", "SELECT id, name, pic_square FROM profile WHERE id IN "
						+ userSharedId);
				Session session = Session.getActiveSession();
				Request request = new Request(session, "/fql", params, HttpMethod.GET,
						new Callback() {

							@Override
							public void onCompleted(Response response) {
								GraphObject graphObject = response.getGraphObject();
								if (graphObject != null) {
									JSONObject jsonResponse = graphObject.getInnerJSONObject();
									try {
										JSONArray data = jsonResponse.getJSONArray("data");
										String iconUserShared = data.getJSONObject(0).getString(
												"pic_square");
										listItemTemp.get(finalI).setIconUserShared(iconUserShared);
									} catch (JSONException e) {
										e.printStackTrace();
									}
								}
								task_get_icon_count--;
								if (task_get_icon_count == 0) {
									showWebview(isRefresh, false);
								}
							}
						});
				Request.executeBatchAsync(request);
			}
		}
		if (no_task_get_icon) {
			showWebview(isRefresh, false);
		}
	}

	private static void showWebview(boolean isRefresh, boolean detail) {

		newFeedsString = StringUtils.dataNewFeeds(listItemTemp, FacebookUtils.context, detail);
		newFeedsString = newFeedsString.replace("\n", "<br>");
		newFeedsString = newFeedsString.replace("\t", "");
		newFeedsString = newFeedsString.replace("\r", "");
		newFeedsString = newFeedsString.replace("'", "&#39;");
		// Log.i(Log.TAG, "newFeedsString: " + newFeedsString);
		String insertWebview;
		if (isRefresh) {
			insertWebview = "javascript:insertCodeOnBottom('" + newFeedsString + "',true);";
		} else {
			insertWebview = "javascript:insertCodeOnBottom('" + newFeedsString + "',false);";
		}
		// Log.i(Log.TAG, "insert html" + insertWebview);
		// try {
		// File file = new
		// File(Environment.getExternalStorageDirectory().getAbsolutePath(),
		// "besafe.txt");
		// file.createNewFile();
		// FileOutputStream fout = new FileOutputStream(file);
		// OutputStreamWriter out = new OutputStreamWriter(fout);
		// out.write(newFeedsString);
		// out.close();
		// Log.d(Log.TAG, file.getAbsolutePath());
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		webView.loadUrl(insertWebview);
		webView.setHorizontalScrollbarOverlay(false);
		ArrayList<ArticleItem> listItemHasMap = StringUtils.getListItemHasMap();
		for (int i = 0; i < listItemHasMap.size(); i++) {
			String map = "javascript:initialized('" + listItemHasMap.get(i).getLat() + "','"
					+ listItemHasMap.get(i).getLon() + "','" + "idmap:"
					+ listItemHasMap.get(i).getPostId() + "','','')";
			webView.loadUrl(map);
		}
	}

	public static void getUser(final IActivityDelegate mIDelegate) {
		final Session session = Session.getActiveSession();
		if (session != null && session.isOpened()) {
			// If the session is open, make an API call to get user data
			// and define a new callback to handle the response
			Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
				@Override
				public void onCompleted(GraphUser user, Response response) {
					// If the response is successful
					if (session == Session.getActiveSession()) {
						if (user != null) {
							String uid = user.getId();
							String accFacebook = user.getName();
							LoginObject.get_instance().setFacebookId(uid);
							ManagerFacebookApp.getInstance().setUid(uid);
							ManagerFacebookApp.getInstance().setAccFacebook(accFacebook);
							Utils.saveStringIntoFreference(Utils.keyAcccountFb, accFacebook);
							mIDelegate.onGetSuccess(Utils.LOGIN_FACEBOOK);
						} else {
							mIDelegate.onGetFailure(Utils.LOGIN_FACEBOOK);
						}
					}
				}
			});
			Request.executeBatchAsync(request);
		}
	}

	public static void getSrcImgProfile() {
		String fqlQuerySrcImgProfile = "SELECT id, name,pic_big FROM profile WHERE id IN "
				+ ManagerFacebookApp.getInstance().getUid();
		Bundle params = new Bundle();
		params.putString("q", fqlQuerySrcImgProfile);
		Session session = Session.getActiveSession();
		Request requests = new Request(session, "/fql", params, HttpMethod.GET, new Callback() {

			@Override
			public void onCompleted(Response response) {
				GraphObject graphObj = response.getGraphObject();
				if (graphObj != null) {
					JSONObject jsonObj = graphObj.getInnerJSONObject();
					String src = "";
					try {
						StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
								.permitAll().build();
						StrictMode.setThreadPolicy(policy);
						src = jsonObj.getJSONArray("data").getJSONObject(0).getString("pic_big");
						URL url = new URL(src);
						URLConnection urlConnect = url.openConnection();
						InputStream is = urlConnect.getInputStream();
						bmp = BitmapFactory.decodeStream(is);
						is.close();
					} catch (JSONException e) {
						e.printStackTrace();
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});

		Request.executeBatchAsync(requests);
	}

	public static void getDetailsPhoto(Context context, ArrayList<String> listFbidPhoto,
			final IActivityDelegate mDelegate) {
		FacebookUtils.context = context;
		listPhotoDetails.clear();
		JSONObject jsonFQL = new JSONObject();
		for (int i = 0; i < listFbidPhoto.size(); i++) {
			String query = "SELECT pid,src, like_info, comment_info, caption, object_id FROM photo WHERE object_id = "
					+ listFbidPhoto.get(i);

			Log.d(Log.TAG, "photos : " + query);
			try {
				jsonFQL.put("query" + String.valueOf(i), query);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}

		Bundle params = new Bundle();
		params.putString("q", jsonFQL.toString());
		Session session = Session.getActiveSession();

		Request request = new Request(session, "/fql", params, HttpMethod.GET,
				new Request.Callback() {

					@Override
					public void onCompleted(Response response) {

						GraphObject graphObject = response.getGraphObject();
						if (graphObject != null) {
							JSONObject jsonObject = graphObject.getInnerJSONObject();
							try {
								JSONArray jsonArray = jsonObject.getJSONArray("data");
								Log.d(Log.TAG, "photos : " + jsonArray.toString());
								for (int i = 0; i < jsonArray.length(); i++) {
									JSONArray query = jsonArray.getJSONObject(i).getJSONArray(
											"fql_result_set");
									String src = query.getJSONObject(0).getString("src");
									src = src.replace("_s", "_n");
									int likeCount = query.getJSONObject(0)
											.getJSONObject("like_info").getInt("like_count");
									Boolean isLike = query.getJSONObject(0)
											.getJSONObject("like_info").getBoolean("user_likes");
									int commentCount = query.getJSONObject(0)
											.getJSONObject("comment_info").getInt("comment_count");
									String objectId = query.getJSONObject(0).getString("object_id");

									String caption = query.getJSONObject(0).getString("caption");
									PhotoDetails photoDetails = new PhotoDetails();
									photoDetails.setIsLike(isLike);
									photoDetails.setSrc(src);
									photoDetails.setLikeCount(likeCount);
									photoDetails.setCommentCount(commentCount);
									photoDetails.setObjectId(objectId);
									photoDetails.setDescription(caption);
									listPhotoDetails.add(photoDetails);
								}

							} catch (JSONException e) {
								e.printStackTrace();
							}
							mDelegate.onGetSuccess(Utils.GET_DETAILS_PHOTO);
						} else {
							mDelegate.onGetFailure(Utils.GET_DETAILS_PHOTO);
						}

					}
				});

		Request.executeBatchAsync(request);
	}

	public static void like(String postID) {
		String grapPath = String.format("%s/likes", postID);
		Request request = new Request(Session.getActiveSession(), grapPath, null, HttpMethod.POST,
				new Callback() {

					@Override
					public void onCompleted(Response response) {
						Log.i(Log.TAG, "Success!");
					}
				});
		Request.executeBatchAsync(request);
	}

	public static void unlike(String postID) {

		String grapPath = String.format("%s/likes", postID);
		Request request = new Request(Session.getActiveSession(), grapPath, null,
				HttpMethod.DELETE, new Callback() {

					@Override
					public void onCompleted(Response response) {
						Log.i(Log.TAG, "Success!");
					}
				});
		Request.executeBatchAsync(request);
	}

	public static void postComment(String comment, String postID, final IActivityDelegate mIDelegate) {

		String grapPath = postID + "/comments";
		Bundle bundle = new Bundle();
		bundle.putString("message", comment);
		Request request = new Request(Session.getActiveSession(), grapPath, bundle,
				HttpMethod.POST, new Callback() {

					@Override
					public void onCompleted(Response response) {
						GraphObject graphObj = response.getGraphObject();
						if (graphObj != null) {
							mIDelegate.onGetSuccess(Utils.REQUEST_POST_COMMENT);
						} else {
							mIDelegate.onGetFailure(Utils.REQUEST_POST_COMMENT);
						}

					}
				});
		Request.executeBatchAsync(request);
	}

	public static void postPhotoToWall(final IActivityDelegate mIDelegate, Bitmap bitmap,
			String message) {
		Request request = Request.newUploadPhotoRequest(Session.getActiveSession(), bitmap,
				new Request.Callback() {
					@Override
					public void onCompleted(Response response) {
						GraphObject graphObj = response.getGraphObject();
						if (graphObj != null) {
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							mIDelegate.onGetSuccess(Utils.REQUEST_POST_PHOTO);
						} else {
							mIDelegate.onGetFailure(Utils.REQUEST_POST_PHOTO);
						}
					}
				});
		Bundle params = request.getParameters();
		params.putString("message", message);
		request.executeAsync();
	}

	public static void getComment(final String postID, final WebView mWebView,
			final Context context, final int LIMIT, final IActivityDelegate mDelegate) {
		JSONObject jsonFQL = new JSONObject();
		String fqlQueryItem = "SELECT can_like,user_likes,id,text,time,user_likes,likes,fromid FROM comment WHERE post_id = \'"
				+ postID + "\'" + "LIMIT " + String.valueOf(LIMIT);
		String fqlQueryDetail = "SELECT id, name, pic_big FROM profile WHERE id IN (SELECT fromid FROM #query1)";

		try {
			jsonFQL.put("query1", fqlQueryItem);
			jsonFQL.put("query2", fqlQueryDetail);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		Bundle params = new Bundle();
		params.putString("q", jsonFQL.toString());
		Session session = Session.getActiveSession();

		Request request = new Request(session, "/fql", params, HttpMethod.GET,
				new Request.Callback() {

					@Override
					public void onCompleted(Response response) {
						if (LIMIT == 4) {
							listComment.clear();
						}
						listCommentTemp.clear();
						GraphObject graphObject = response.getGraphObject();

						if (graphObject != null) {
							JSONObject jsonObject = graphObject.getInnerJSONObject();

							String nameActor = "";
							String textComment = "";
							String time = "";
							int likeCount;
							String id = "";
							String profileIcon = "";
							boolean user_likes = false;
							boolean canLike = false;

							try {

								JSONArray jsonArray = jsonObject.getJSONArray("data");
								JSONArray query1 = jsonArray.getJSONObject(0).getJSONArray(
										"fql_result_set");
								JSONArray query2 = jsonArray.getJSONObject(1).getJSONArray(
										"fql_result_set");
								for (int i = 0; i < query1.length(); i++) {
//									Log.i("comment query1 " + String.valueOf(i), query1
//											.getJSONObject(i).toString());
									// get name
									String actorID = query1.getJSONObject(i).getString("fromid");
									for (int j = 0; j < query2.length(); j++) {
										if (actorID.equals(query2.getJSONObject(j).getString("id"))) {
											Log.i("comment query2 " + String.valueOf(j), query2
													.getJSONObject(j).toString());
											nameActor = query2.getJSONObject(j).getString("name");
											profileIcon = query2.getJSONObject(j).getString(
													"pic_big");
										}
									}

									// get text of comment
									textComment = query1.getJSONObject(i).getString("text");

									// get time
									Date date = new Date();
									date.setTime(Long.parseLong(query1

									.getJSONObject(i).getString("time") + "000"));
									time = DateUtils.timeAgoInWords(date);

									// get like count
									likeCount = query1.getJSONObject(i).getInt("likes");
									// get post id
									id = query1.getJSONObject(i).getString("id");
									// get user_likes
									user_likes = query1.getJSONObject(i).getBoolean("user_likes");
									// get can_like
									canLike = query1.getJSONObject(i).getBoolean("can_like");
									Comment comment = new Comment();
									comment.setCanLike(canLike);
									comment.setPostId(id);
									comment.setNameActor(nameActor);
									comment.setTextComment(textComment);
									comment.setLikeCount(likeCount);
									comment.setTime(time);
									comment.setUser_likes(user_likes);
									comment.setProfileIcon(profileIcon);
									boolean isduplicate = false;
									for (int j = 0; j < listComment.size(); j++) {
										if (id.equals(listComment.get(j).getPostId())) {
											isduplicate = true;
										}
									}
									if (!isduplicate) {
										listCommentTemp.add(comment);
										listComment.add(comment);
									}

								}
							} catch (Exception e) {
								Log.i("Parse Json", "Error");
							}

							String dataComment = StringUtils.getStringHtmlShowAllComment(
									listCommentTemp, context);
							dataComment = dataComment.replace("\n", "<br>");
							dataComment = dataComment.replace("\t", "");
							dataComment = dataComment.replace("\r", "");
							dataComment = dataComment.replace("'", "&#39;");
							// Log.i(Log.TAG, "dataComment: " + dataComment);
							String insertWebview;
							if (LIMIT == 4) {
								// refresh
								insertWebview = "javascript:changeTextInTag('commentOfThePost','"
										+ dataComment + "');";
							} else {
								insertWebview = "javascript:insertCodeOnBottom('" + dataComment
										+ "');";
							}
							mWebView.loadUrl(insertWebview);
							mDelegate.onGetSuccess(Utils.REQUEST_GET_COMMENT);
						} else {
							mDelegate.onGetFailure(Utils.REQUEST_GET_COMMENT);
							Log.i("GraphObject", "Null");
						}
					}
				});
		Request.executeBatchAsync(request);
	}

	public static void postSttPlace(String message, String place, final IActivityDelegate mIDelegate) {

		Bundle params = new Bundle();
		params.putString("message", message);
		params.putString("place", place);
		Session session = Session.getActiveSession();

		Request request = new Request(session, "me/feed", params, HttpMethod.POST,
				new Request.Callback() {

					@Override
					public void onCompleted(Response response) {
						GraphObject graphObj = response.getGraphObject();
						if (graphObj != null) {
							Toast.makeText(context, "Post status successful!", Toast.LENGTH_LONG)
									.show();
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							mIDelegate.onGetSuccess("");
						} else {
							Toast.makeText(context, "Post status not successful!",
									Toast.LENGTH_LONG).show();
							mIDelegate.onGetFailure("");
						}
					}

				});

		RequestAsyncTask task = new RequestAsyncTask(request);
		task.execute();
	}

	public static void postToWall(String message, final IActivityDelegate mIDelegate, long flId) {
		Bundle params = new Bundle();
		params.putString("message", message);
		// put privacy
		JSONObject json = new JSONObject();

		try {
			if (flId == 1) {
				json.put("value", "EVERYONE");
			} else if (flId == 2) {
				json.put("value", "ALL_FRIENDS");
			} else if (flId == 3) {
				json.put("value", "SELF");
			} else {
				json.put("value", "CUSTOM");
				json.put("allow", flId);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		params.putString("privacy", json.toString());
		Session session = Session.getActiveSession();
		if (session.isOpened()) {

			Request request = new Request(session, "me/feed", params, HttpMethod.POST,
					new Request.Callback() {

						@Override
						public void onCompleted(Response response) {
							GraphObject graphObj = response.getGraphObject();
							if (graphObj != null) {
								Toast.makeText(context, "Post status successful!",
										Toast.LENGTH_LONG).show();
								try {
									Thread.sleep(2000);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								mIDelegate.onGetSuccess(Utils.REQUEST_POST_STATUS);
							} else {
								Toast.makeText(context, "Post status not successful!",
										Toast.LENGTH_LONG).show();
								mIDelegate.onGetFailure(Utils.REQUEST_POST_STATUS);
							}
						}
					});
			Request.executeBatchAsync(request);

		}
	}

	public static void sharePost(String message, String link, long flId,
			final IActivityDelegate mIDelegate) {

		Bundle params = new Bundle();
		params.putString("message", message);
		params.putString("link", link);
		// put privacy
		JSONObject json = new JSONObject();

		try {
			if (flId == 1) {
				json.put("value", "EVERYONE");
			} else if (flId == 2) {
				json.put("value", "ALL_FRIENDS");
			} else if (flId == 3) {
				json.put("value", "SELF");
			} else {
				json.put("value", "CUSTOM");
				json.put("allow", flId);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		params.putString("privacy", json.toString());
		Session session = Session.getActiveSession();

		Request request = new Request(session, "me/feed", params, HttpMethod.POST,
				new Request.Callback() {

					@Override
					public void onCompleted(Response response) {
						GraphObject graphObj = response.getGraphObject();
						if (graphObj != null) {
							mIDelegate.onGetSuccess(Utils.REQUEST_SHARE_POST);
						} else {
							mIDelegate.onGetFailure(Utils.REQUEST_SHARE_POST);
						}
					}

				});

		RequestAsyncTask task = new RequestAsyncTask(request);
		task.execute();
	}

	public static void delete(String postID) {
		String grapPath = postID;
		Request request = new Request(Session.getActiveSession(), grapPath, null,
				HttpMethod.DELETE, new Callback() {

					@Override
					public void onCompleted(Response response) {
					}
				});
		Request.executeBatchAsync(request);
	}

	public static ArrayList<PhotoDetails> getListPhotoDetails() {
		return listPhotoDetails;
	}

	public static void getBitmapFromSrc(final String src) {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					InputStream in = new java.net.URL(src).openStream();
					bmp = BitmapFactory.decodeStream(in);
				} catch (Exception e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}
			}
		});
		thread.start();
	}

	public static boolean isHasNewStory() {
		return hasNewStory;
	}

	public static ArticleItem getCurrentItemFromListArticleItem(String postId) {
		ArticleItem currentItem = new ArticleItem();
		for (int i = 0; i < FacebookUtils.listItem.size(); i++) {
			if (postId.equalsIgnoreCase(FacebookUtils.listItem.get(i).getPostId())) {
				currentItem = FacebookUtils.listItem.get(i);
				// if type isn't like or comment on a post, break
				if (currentItem.getType() != 245 && currentItem.getType() != 257) {
					break;
				}
			}
		}
		return currentItem;
	}

	private static String analyseProfileLink(String link) {
		String s = link;
		String newS = s.replace("_q", "_n");
		return newS;
	}
}
