package com.example.besafepost.utils;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.example.besafepost.interfaces.ISwipeDetector;

public class ActivitySwipeDetector implements OnTouchListener {

	static final String logTag = "ActivitySwipeDetector";
	private ISwipeDetector detector;
	static final int MIN_DISTANCE = 20;
	private float touchActionDownY;
	private float touchActionMoveY;
	private float threshold = 10;
	private boolean down;

	public ActivitySwipeDetector(ISwipeDetector detector) {
		this.detector = detector;
	}

	public void onClick() {
		detector.doOnClick();
	}

	public void onRightToLeftSwipe() {
	}

	public void onLeftToRightSwipe() {
	}

	public void onTopToBottomSwipe() {
		detector.doTopToBottom();
	}

	public void onBottomToTopSwipe() {
		detector.doBottomToTop();
	}
	private float beforeY;
	private boolean changed = false;
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touchActionDownY = event.getY();
			beforeY = touchActionDownY;
			break;
			
		case MotionEvent.ACTION_MOVE:
			touchActionMoveY = event.getY();
			if (touchActionMoveY-beforeY > 0) {
				if (!down){
					touchActionDownY = touchActionMoveY;
					down = true;
					changed = true;
				}
			} else {
				if (down) {
					touchActionDownY = touchActionMoveY;
					down = false;	
					changed = true;
				}
			}
			beforeY = touchActionMoveY;
			float UpDown = Math.abs(touchActionMoveY - touchActionDownY);

			if (UpDown > threshold && changed) {
				if (!down) {
					onBottomToTopSwipe();
				} else {
					onTopToBottomSwipe();
				}
				changed = false;
			}
			break;

		}
		return false;
	}
}
