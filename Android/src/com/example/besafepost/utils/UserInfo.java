package com.example.besafepost.utils;

public class UserInfo {

	public String userName;
	public String userID;
	public String userAvatar;

	public UserInfo(String userName, String userID, String userAvatar) {
		super();
		this.userName = userName;
		this.userID = userID;
		this.userAvatar = userAvatar;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getUserAvatar() {
		return userAvatar;
	}

	public void setUserAvatar(String userAvatar) {
		this.userAvatar = userAvatar;
	}

}
