package com.example.besafepost.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import twitter4j.Status;

import android.content.Context;

import com.example.besafepost.BesafePostApplication;
import com.example.besafepost.activity.R;
import com.example.besafepost.model.ArticleItem;
import com.example.besafepost.model.Comment;

public final class StringUtils {
	public static int LIMIT_TWITTER_DEFAULT = 20;
	public static int LIMIT_TWITTER_ADVANCE = 10;
	public static int LIMIT = 10;
	public static int LIMIT_ADVANCE = 10;
	public static int LIMIT_COMMENT = 4;
	public static int LIMIT_PLACE = 30;
	public static int[] listTypeUsePhoto = { 247, 60, 46, 373, 56, 308, 295 };
	public static ArrayList<ArticleItem> listItemHasMap = new ArrayList<ArticleItem>();
	private static ArticleItem userLiked;
	private static ArticleItem userComment;
	public static final String KEY_COUNT_DOWN = "Count Down";
	public static final String KEY_TWEET_TIP = "Besafe Twitter";
	public static final String KEY_POST_TIP = "Besafe Tip";
	public static final String KEY_DELAY = "Delay";
	private static String article_begin_likes, article_begin_normal, article_body_photo,
			article_body_shared_link, article_body_status, article_body_video, article_end,
			article_end_no_like, article_body_shared_link_video, article_share_photo,
			article_body_share_link_no_icon;
	private static final int LIKE_A_POST = 1;
	private static final int CMT_A_POST = 2;
	public static String IMG_LIKE = "<img id=\"iconLike\" src=\"file:///android_asset/image/fb_like.png\" width=\"20\" height=\"32\"/>";
	public static String IMG_COMMENT = "<img src=\"file:///android_asset/image/fb_comment.png\" width=\"20\" height=\"32\"/>";
	public static String COUNT_1 = "<!-- COUNT_1 -->";
	public static String ID_COUNT_1 = "ID_COUNT_1";
	public static String ID_IMG_2 = "ID_IMG_2";
	public static String ID_COUNT_3 = "ID_COUNT_3";
	public static String COUNT_3 = "<!-- COUNT_3 -->";
	public static String ID_IMG_4 = "ID_IMG_4";
	public static String IMG_4 = "<!-- IMG_4 -->";
	public static String IMG_2 = "<!-- IMG_2 -->";
	public static String idLikeCount = "idLikeCount:";
	public static String idCmtCount = "idCmtCount:";

	public static boolean listIntegerHasVar(int[] listInt, int var) {
		boolean result = false;
		for (int i = 0; i < listInt.length; i++) {
			if (listInt[i] == var) {
				result = true;
				break;
			}
		}
		return result;
	}

	public static String replaceString(String currentString, String oldString, String newString) {
		return currentString.replace(oldString, newString);
	}

	public static void clearUserLike() {
		userLiked = null;
	}

	public static void clearUserComment() {
		userComment = null;
	}

	public static ArrayList<ArticleItem> getListItemHasMap() {
		return listItemHasMap;
	}

	private static void get_article_template(Context context) {
		article_begin_normal = FileUtils
				.readFileFromAssets(FileUtils.ARTICLE_BEGIN_NORMAL, context);
		article_body_status = FileUtils.readFileFromAssets(FileUtils.ARTICLE_BODY_STATUS, context);
		article_end = FileUtils.readFileFromAssets(FileUtils.ARTICLE_END_3, context);
		article_end_no_like = FileUtils.readFileFromAssets(FileUtils.ARTICLE_END_NO_LIKE, context);
		article_body_shared_link = FileUtils.readFileFromAssets(FileUtils.ARTICLE_BODY_SHARE_LINK,
				context);
		article_body_photo = FileUtils.readFileFromAssets(FileUtils.ARTICLE_BODY_PHOTO, context);
		article_begin_likes = FileUtils.readFileFromAssets(FileUtils.ARTICLE_BEGIN_LIKES, context);
		article_body_share_link_no_icon = FileUtils.readFileFromAssets(
				FileUtils.ARTICLE_BODY_SHARE_LINK_NO_ICON, context);
		article_share_photo = FileUtils.readFileFromAssets(FileUtils.ARTICLE_SHARE_PHOTO, context);
		article_body_shared_link_video = FileUtils.readFileFromAssets(
				FileUtils.ARTICLE_BODY_SHARED_LINK_VIDEO, context);
		article_body_video = FileUtils.readFileFromAssets(FileUtils.ARTICLE_BODY_VIDEO, context);
	}

	public static String dataNewFeeds(List<ArticleItem> listItem, Context context, boolean detail) {
		listItemHasMap.clear();
		String content = "";
		String stgComponent = "";
		get_article_template(context);
		for (int i = 0; i < listItem.size(); i++) {
			ArticleItem item = listItem.get(i);
			
//			Log.i(Log.TAG, item.getProfileName() + ": " + String.valueOf(item.getType())+ ", " + item.getPost().permalink);
			if (!(item.getLat().isEmpty() || item.getLon().isEmpty())) {
				listItemHasMap.add(item);
			}

			if (item.getType() == 8 || item.getType() == 424) {
				stgComponent = "";
			} else if (item.getType() == 347) {
				// type like any thing,if can not get title and content of
				// share,will hide
				if (!StringUtils.isEmptyString(item.getShareContent())
						|| !StringUtils.isEmptyString(item.getShareTitle())) {
					userLiked = (item);
					stgComponent = getStrCommentAndLikeAPost(item, LIKE_A_POST, detail);
				} else {
					stgComponent = "";
				}
			} else if (item.getType() == 245) {
				userLiked = (item);
				stgComponent = "";
			} else if (userLiked != null) {
				stgComponent = getStrCommentAndLikeAPost(item, LIKE_A_POST, detail);
			} else if (item.getType() == 257) {
				userComment = (item);
				stgComponent = "";
			} else if (userComment != null) {
				stgComponent = getStrCommentAndLikeAPost(item, CMT_A_POST, detail);
			} else if (item.getType() == 80 && !item.getSharePhotoOf().isEmpty()) {
				stgComponent = getStrShareAPhoto(item, detail);
			} else {
				stgComponent = getStgComponent(item, detail);
			}
			content += stgComponent;
		}
		return content;
	}

	public static String getStrShareAPhoto(ArticleItem articleItem, boolean detail) {

		String begin;
		begin = article_share_photo.replace(ArticleItem.USER_SHARE, articleItem.getProfileName());
		begin = begin.replace(ArticleItem.TIME_USER_SHARE, articleItem.getTimeStamp());
		// link of hide or delete a post
		begin = begin.replace(ArticleItem.LINK_HIDE_DELELE_POST, ArticleItem.LINK_HIDE_DELELE_POST
				+ articleItem.getPostId());

		// add id of item
		begin = begin.replace(ArticleItem.ID_OF_ITEM,
				ArticleItem.ID_OF_ITEM + articleItem.getPostId());

		begin = begin.replace(ArticleItem.ICON_USER_SHARED, articleItem.getIconUserShared());
		begin = begin.replace(ArticleItem.USER_SHARED, articleItem.getSharePhotoOf());
		if (!detail) {
			begin = begin.replace(ArticleItem.MESSAGE_USER_SHARE,
					fixLongMessage(articleItem.getMessage(), articleItem.getPostId()));
			begin = begin.replace(ArticleItem.MESSAGE_USER_SHARED, "");
		} else {
			begin = begin.replace(ArticleItem.MESSAGE_USER_SHARE,
					showLink(showLink(articleItem.getMessage())));
			begin = begin.replace(ArticleItem.MESSAGE_USER_SHARED, "");
		}

		// Article Body
		String body = "";

		body = article_body_photo.replace(ArticleItem.SRC_FULL_IMG, articleItem.getSrcIconImage());
		body = body.replace(ArticleItem.CLICK_ALBUM,
				"http://onclick/album" + articleItem.getPostId());

		String caption = article_body_status.replace(ArticleItem.MESSAGE,
				(!detail) ? fixLongMessage(articleItem.getShareVia(), articleItem.getPostId())
						: articleItem.getShareVia());
		// Article End
		String end = article_end;
		if (articleItem.getListFbidPhoto().size() == 1) {
			end = end.replace(ArticleItem.LIKE_LINK, ArticleItem.LIKE_ON_PHOTO_LINK
					+ articleItem.getListFbidPhoto().get(0) + ArticleItem.LIKE_ON_PHOTO_LINK
					+ articleItem.getPostId());
		} else {
			end = end.replace(ArticleItem.LIKE_LINK,
					ArticleItem.LIKE_LINK + articleItem.getPostId());
		}
		end = end.replace(ArticleItem.COMMENT_LINK,
				ArticleItem.COMMENT_LINK + articleItem.getPostId());
		end = end.replace(ArticleItem.SHARE_POST_LINK,
				ArticleItem.SHARE_POST_LINK + articleItem.getPostId());
		if (articleItem.getCanShare()) {
			end = end.replace(ArticleItem.SHARE_BUTTON, "Share");
			end = end.replace(ArticleItem.SHARE_SPACE, " . ");
		}
		end = end.replace(ArticleItem.ID_LIKE_BUTTON, "postId:" + articleItem.getPostId());
		String likeOrUnLike = "";
		if (articleItem.isUserLiked()) {
			likeOrUnLike = "Unlike";
		} else {
			likeOrUnLike = "Like";
		}
		end = end.replace(ArticleItem.LIKE_OR_UNLIKE, likeOrUnLike);

		int likeCount = articleItem.getLikeCount();
		int cmtCount = articleItem.getCommentCount();

		if (cmtCount > 0) {
			if (likeCount > 0) {
				end = end.replace(COUNT_3, rountOffNumber(likeCount));
				end = end.replace(COUNT_1, rountOffNumber(cmtCount));
				end = end.replace(IMG_2, IMG_COMMENT);
				end = end.replace(IMG_4, IMG_LIKE);
			} else {
				end = end.replace(COUNT_1, rountOffNumber(cmtCount));
				end = end.replace(IMG_2, IMG_COMMENT);
			}
		} else {
			if (likeCount > 0) {
				end = end.replace(COUNT_1, rountOffNumber(likeCount));
				end = end.replace(IMG_2, IMG_LIKE);
			}
		}

		end = end.replace(ID_COUNT_1, ID_COUNT_1 + articleItem.getPostId());
		end = end.replace(ID_COUNT_3, ID_COUNT_3 + articleItem.getPostId());
		end = end.replace(ID_IMG_2, ID_IMG_2 + articleItem.getPostId());
		end = end.replace(ID_IMG_4, ID_IMG_4 + articleItem.getPostId());

		String stgComponent = begin + body + caption + end;
		return stgComponent;

	}

	private static String getStrCommentAndLikeAPost(ArticleItem articleItem, int likeOrCmtAPost,
			boolean detail) {
		String begin;
		if (likeOrCmtAPost == LIKE_A_POST) {

			begin = article_begin_likes.replace(ArticleItem.USER_LIKE, userLiked.getProfileName());
			begin = begin.replace(ArticleItem.TIMESTAMP_USER_LIKE, userLiked.getTimeStamp());
			begin = begin.replace(ArticleItem.TYPE_OF_ACTION, "liked on a");
		} else {
			begin = article_begin_likes
					.replace(ArticleItem.USER_LIKE, userComment.getProfileName());
			begin = begin.replace(ArticleItem.TIMESTAMP_USER_LIKE, userComment.getTimeStamp());
			begin = begin.replace(ArticleItem.TYPE_OF_ACTION, "commented on a");
		}
		if (articleItem.getTypeAttachment().length() > 0) {
			begin = begin.replace(ArticleItem.TYPE_OF_LIKED, articleItem.getTypeAttachment());
		} else if (articleItem.getType() == 46) {
			begin = begin.replace(ArticleItem.TYPE_OF_LIKED, "status");
		} else if (articleItem.getType() == 56 || articleItem.getType() == 308) {
			begin = begin.replace(ArticleItem.TYPE_OF_LIKED, "post");
		} else if (articleItem.getType() == 66) {
			begin = begin.replace(ArticleItem.TYPE_OF_LIKED, "note");
		} else if (articleItem.getType() == 282) {
			begin = begin.replace(ArticleItem.TYPE_OF_LIKED, "life event");
		}
		// link of hide or delete a post
		begin = begin.replace(ArticleItem.LINK_HIDE_DELELE_POST, ArticleItem.LINK_HIDE_DELELE_POST
				+ articleItem.getPostId());

		// add id of item
		begin = begin.replace(ArticleItem.ID_OF_ITEM,
				ArticleItem.ID_OF_ITEM + articleItem.getPostId());

		begin = begin.replace(ArticleItem.ICON_LIKED_USER, articleItem.getProfileIcon());
		begin = begin.replace(ArticleItem.LIKED_USER, articleItem.getProfileName());
		begin = begin.replace(ArticleItem.TIMESTAMP_LIKED_USER, articleItem.getTimeStamp());

		if (!isEmptyString(articleItem.getNameTarget())) {
			begin = begin.replace(ArticleItem.TARGET, "<font size=\"4\">&raquo;</font> "
					+ articleItem.getNameTarget());
		}

		// Article body status
		String body_status = "";
		body_status = showMessage(detail, articleItem);
		// Article Body
		String body = "";

		if (listIntegerHasVar(listTypeUsePhoto, articleItem.getType())) {
			body = article_body_photo.replace(ArticleItem.SRC_FULL_IMG,
					articleItem.getSrcIconImage());
			body = body.replace(ArticleItem.CLICK_ALBUM,
					"http://onclick/album" + articleItem.getPostId());
		} else if (StringUtils.isEmptyString(articleItem.getSrcIconImage())) {
			body = article_body_share_link_no_icon.replace(ArticleItem.SHARE_CONTENT,
					articleItem.getShareContent());

			if (!articleItem.getTypeAttachment().equalsIgnoreCase("video")) {
				body = body.replace(ArticleItem.SHARE_LINK,
						ArticleItem.LINK_SHARE + articleItem.getPostId());
			} else {
				body = body.replace(ArticleItem.SHARE_LINK, ArticleItem.LINK_UPLOAD_VIDEO
						+ articleItem.getSource_url());
			}

			body = body.replace(ArticleItem.SHARE_TITLE, articleItem.getShareTitle());
			body = body.replace(ArticleItem.SHARE_VIA, articleItem.getShareVia());
		} else if (articleItem.getType() == 128) {
			body = article_body_video.replace(ArticleItem.SHARE_LINK, ArticleItem.LINK_UPLOAD_VIDEO
					+ articleItem.getSource_url());
			body = body.replace(ArticleItem.HEIGHT_VIDEO, BesafePostApplication.getContext()
					.getString(R.string.height_video));
			body = body.replace(ArticleItem.SRC_ICON_IMAGE, articleItem.getSrcIconImage());

		} else {
			if (articleItem.getType() == 237
					&& articleItem.getTypeAttachment().equalsIgnoreCase("swf")
					|| articleItem.getType() == 80
					&& articleItem.getTypeAttachment().equalsIgnoreCase("video")) {
				body = article_body_shared_link_video.replace(ArticleItem.SHARE_CONTENT,
						articleItem.getShareContent());
			} else {
				body = article_body_shared_link.replace(ArticleItem.SHARE_CONTENT,
						articleItem.getShareContent());
			}
			if (articleItem.getTypeAttachment().equalsIgnoreCase("video")
					&& articleItem.getType() != 80) {
				body = body.replace(ArticleItem.SHARE_LINK, ArticleItem.LINK_UPLOAD_VIDEO
						+ articleItem.getSource_url());
			} else {
				body = body.replace(ArticleItem.SHARE_LINK,
						ArticleItem.LINK_SHARE + articleItem.getPostId());
			}
			body = body.replace(ArticleItem.SHARE_TITLE, articleItem.getShareTitle());
			body = body.replace(ArticleItem.SHARE_VIA, articleItem.getShareVia());
			body = body.replace(ArticleItem.SRC_ICON_IMAGE, articleItem.getSrcIconImage());
		}
		// Article Map
		String map = "";
		if (!(articleItem.getLat().isEmpty() || articleItem.getLon().isEmpty())) {
			map = "<div id=\"" + "idmap:" + articleItem.getPostId() + "\"></div>";
		}
		// Article End
		String end = article_end;
		if (articleItem.getListFbidPhoto().size() == 1) {
			end = end.replace(ArticleItem.LIKE_LINK, ArticleItem.LIKE_ON_PHOTO_LINK
					+ articleItem.getListFbidPhoto().get(0) + ArticleItem.LIKE_ON_PHOTO_LINK
					+ articleItem.getPostId());
		} else {
			end = end.replace(ArticleItem.LIKE_LINK,
					ArticleItem.LIKE_LINK + articleItem.getPostId());
		}
		end = end.replace(ArticleItem.COMMENT_LINK,
				ArticleItem.COMMENT_LINK + articleItem.getPostId());
		end = end.replace(ArticleItem.SHARE_POST_LINK,
				ArticleItem.SHARE_POST_LINK + articleItem.getPostId());
		if (articleItem.getCanShare()) {
			end = end.replace(ArticleItem.SHARE_BUTTON, "Share");
			end = end.replace(ArticleItem.SHARE_SPACE, " . ");
		}
		end = end.replace(ArticleItem.ID_LIKE_BUTTON, "postId:" + articleItem.getPostId());
		String likeOrUnLike = "";
		if (articleItem.isUserLiked()) {
			likeOrUnLike = "Unlike";
		} else {
			likeOrUnLike = "Like";
		}
		end = end.replace(ArticleItem.LIKE_OR_UNLIKE, likeOrUnLike);

		int likeCount = articleItem.getLikeCount();
		int cmtCount = articleItem.getCommentCount();

		if (cmtCount > 0) {
			if (likeCount > 0) {
				end = end.replace(COUNT_3, rountOffNumber(likeCount));
				end = end.replace(COUNT_1, rountOffNumber(cmtCount));
				end = end.replace(IMG_2, IMG_COMMENT);
				end = end.replace(IMG_4, IMG_LIKE);
			} else {
				end = end.replace(COUNT_1, rountOffNumber(cmtCount));
				end = end.replace(IMG_2, IMG_COMMENT);
			}
		} else {
			if (likeCount > 0) {
				end = end.replace(COUNT_1, rountOffNumber(likeCount));
				end = end.replace(IMG_2, IMG_LIKE);
			}
		}

		end = end.replace(ID_COUNT_1, ID_COUNT_1 + articleItem.getPostId());
		end = end.replace(ID_COUNT_3, ID_COUNT_3 + articleItem.getPostId());
		end = end.replace(ID_IMG_2, ID_IMG_2 + articleItem.getPostId());
		end = end.replace(ID_IMG_4, ID_IMG_4 + articleItem.getPostId());
		String stgComponent = begin + body_status + body + map + end;
		if (likeOrCmtAPost == LIKE_A_POST) {
			userLiked = null;
		} else {
			userComment = null;
		}
		return stgComponent;
	}

	public static String getStgComponent(ArticleItem articleItem, boolean detail) {

		// Article begin normal
		String begin = article_begin_normal.replace(ArticleItem.PROFILE_ICON,
				articleItem.getProfileIcon());
		begin = begin.replace(ArticleItem.PROFILE_NAME, articleItem.getProfileName());
		begin = begin.replace(ArticleItem.TIME_STAMP, articleItem.getTimeStamp());
		if (articleItem.getType() == 60) {
			begin = begin.replace(ArticleItem.ACTION_ENTER_HERE, "changed profile picture");
		} else if (articleItem.getType() == 373) {
			begin = begin.replace(ArticleItem.ACTION_ENTER_HERE, "updated cover photo");
		}
		// link of hide or delete a post
		begin = begin.replace(ArticleItem.LINK_HIDE_DELELE_POST, ArticleItem.LINK_HIDE_DELELE_POST
				+ articleItem.getPostId());
		// add id of item
		begin = begin.replace(ArticleItem.ID_OF_ITEM,
				ArticleItem.ID_OF_ITEM + articleItem.getPostId());
		if (!isEmptyString(articleItem.getNameTarget())) {
			begin = begin.replace(ArticleItem.TARGET, "<font size=\"4\">&raquo;</font> "
					+ articleItem.getNameTarget());
		}
		// Article body status
		String body_status = "";
		body_status = showMessage(detail, articleItem);
		// Article Body
		String body = "";
		if (listIntegerHasVar(listTypeUsePhoto, articleItem.getType())) {
			body = article_body_photo.replace(ArticleItem.SRC_FULL_IMG,
					articleItem.getSrcIconImage());
			body = body.replace(ArticleItem.CLICK_ALBUM,
					"http://onclick/album" + articleItem.getPostId());
		} else if (StringUtils.isEmptyString(articleItem.getSrcIconImage())) {
			body = article_body_share_link_no_icon.replace(ArticleItem.SHARE_CONTENT,
					articleItem.getShareContent());

			if (!articleItem.getTypeAttachment().equalsIgnoreCase("video")) {
				// Log.d("Share", articleItem.getShareLink() + " abc");
				body = body.replace(ArticleItem.SHARE_LINK,
						ArticleItem.LINK_SHARE + articleItem.getPostId());
			} else {
				body = body.replace(ArticleItem.SHARE_LINK, ArticleItem.LINK_UPLOAD_VIDEO
						+ articleItem.getSource_url());
			}

			body = body.replace(ArticleItem.SHARE_TITLE, articleItem.getShareTitle());
			body = body.replace(ArticleItem.SHARE_VIA, articleItem.getShareVia());
		} else if (articleItem.getType() == 128) {
			body = article_body_video.replace(ArticleItem.SHARE_LINK, ArticleItem.LINK_UPLOAD_VIDEO
					+ articleItem.getSource_url());
			body = body.replace(ArticleItem.HEIGHT_VIDEO, BesafePostApplication.getContext()
					.getString(R.string.height_video));
			body = body.replace(ArticleItem.SRC_ICON_IMAGE, articleItem.getSrcIconImage());
//			Log.d(Log.TAG, "128 : " + body);
		} else {
			if (articleItem.getType() == 237
					&& articleItem.getTypeAttachment().equalsIgnoreCase("swf")
					|| articleItem.getType() == 80
					&& articleItem.getTypeAttachment().equalsIgnoreCase("video")) {
				body = article_body_shared_link_video.replace(ArticleItem.SHARE_CONTENT,
						articleItem.getShareContent());
			} else {
				body = article_body_shared_link.replace(ArticleItem.SHARE_CONTENT,
						articleItem.getShareContent());
			}
			if (articleItem.getTypeAttachment().equalsIgnoreCase("video")
					&& articleItem.getType() != 80) {
				body = body.replace(ArticleItem.SHARE_LINK, ArticleItem.LINK_UPLOAD_VIDEO
						+ articleItem.getSource_url());

			} else {
				body = body.replace(ArticleItem.SHARE_LINK,
						ArticleItem.LINK_SHARE + articleItem.getPostId());
			}
			body = body.replace(ArticleItem.SHARE_TITLE, articleItem.getShareTitle());
			body = body.replace(ArticleItem.SHARE_VIA, articleItem.getShareVia());
			body = body.replace(ArticleItem.SRC_ICON_IMAGE, articleItem.getSrcIconImage());
//			Log.d(Log.TAG, "237 : " + body);
		}

		// Article Map
		String map = "";
		if (!(articleItem.getLat().isEmpty() || articleItem.getLon().isEmpty())) {
			map = "<div id=\"" + "idmap:" + articleItem.getPostId() + "\"></div>";
		}

		// Article End
		String end = article_end;
		if (articleItem.getListFbidPhoto().size() == 1) {
			end = end.replace(ArticleItem.LIKE_LINK, ArticleItem.LIKE_ON_PHOTO_LINK
					+ articleItem.getListFbidPhoto().get(0) + ArticleItem.LIKE_ON_PHOTO_LINK
					+ articleItem.getPostId());
		} else {
			end = end.replace(ArticleItem.LIKE_LINK,
					ArticleItem.LIKE_LINK + articleItem.getPostId());
		}
		end = end.replace(ArticleItem.COMMENT_LINK,
				ArticleItem.COMMENT_LINK + articleItem.getPostId());
		end = end.replace(ArticleItem.SHARE_POST_LINK,
				ArticleItem.SHARE_POST_LINK + articleItem.getPostId());
		if (articleItem.getCanShare()) {
			end = end.replace(ArticleItem.SHARE_BUTTON, "Share");
			end = end.replace(ArticleItem.SHARE_SPACE, " . ");
		}
		end = end.replace(ArticleItem.ID_LIKE_BUTTON, "postId:" + articleItem.getPostId());
		String likeOrUnLike = "";
		if (articleItem.isUserLiked()) {
			likeOrUnLike = "Unlike";
		} else {
			likeOrUnLike = "Like";
		}
		end = end.replace(ArticleItem.LIKE_OR_UNLIKE, likeOrUnLike);

		int likeCount = articleItem.getLikeCount();
		int cmtCount = articleItem.getCommentCount();

		if (cmtCount > 0) {
			if (likeCount > 0) {
				end = end.replace(COUNT_3, rountOffNumber(likeCount));
				end = end.replace(COUNT_1, rountOffNumber(cmtCount));
				end = end.replace(IMG_2, IMG_COMMENT);
				end = end.replace(IMG_4, IMG_LIKE);
			} else {
				end = end.replace(COUNT_1, rountOffNumber(cmtCount));
				end = end.replace(IMG_2, IMG_COMMENT);
			}
		} else {
			if (likeCount > 0) {
				end = end.replace(COUNT_1, rountOffNumber(likeCount));
				end = end.replace(IMG_2, IMG_LIKE);
			}
		}

		end = end.replace(ID_COUNT_1, ID_COUNT_1 + articleItem.getPostId());
		end = end.replace(ID_COUNT_3, ID_COUNT_3 + articleItem.getPostId());
		end = end.replace(ID_IMG_2, ID_IMG_2 + articleItem.getPostId());
		end = end.replace(ID_IMG_4, ID_IMG_4 + articleItem.getPostId());

		return begin + body_status + body + map + end;
	}

	public static String getStgComment(ArticleItem articleItem, boolean detail) {

		// Article begin normal
		String begin = article_begin_normal.replace(ArticleItem.PROFILE_ICON,
				articleItem.getProfileIcon());
		begin = begin.replace(ArticleItem.PROFILE_NAME, articleItem.getProfileName());
		begin = begin.replace(ArticleItem.TIME_STAMP, articleItem.getTimeStamp());
		if (articleItem.getType() == 60) {
			begin = begin.replace(ArticleItem.ACTION_ENTER_HERE, "changed profile picture");
		} else if (articleItem.getType() == 373) {
			begin = begin.replace(ArticleItem.ACTION_ENTER_HERE, "updated cover photo");
		}
		// Article body status
		String body_status = "";
		body_status = showMessage(detail, articleItem);
		// Article Body
		String body = "";

		if (articleItem.getType() == 56) {
			body = "";
		} else if (listIntegerHasVar(listTypeUsePhoto, articleItem.getType())) {
			body = article_body_photo.replace(ArticleItem.SRC_FULL_IMG,
					articleItem.getSrcIconImage());
		} else if (StringUtils.isEmptyString(articleItem.getSrcIconImage())) {
			body = article_body_share_link_no_icon.replace(ArticleItem.SHARE_CONTENT,
					articleItem.getShareContent());
			body = body.replace(ArticleItem.SHARE_LINK, articleItem.getShareLink());
			body = body.replace(ArticleItem.SHARE_TITLE, articleItem.getShareTitle());
			body = body.replace(ArticleItem.SHARE_VIA, articleItem.getShareVia());
		} else {
			body = article_body_shared_link.replace(ArticleItem.SHARE_CONTENT,
					articleItem.getShareContent());
			body = body.replace(ArticleItem.SHARE_LINK, articleItem.getShareLink());
			body = body.replace(ArticleItem.SHARE_TITLE, articleItem.getShareTitle());
			body = body.replace(ArticleItem.SHARE_VIA, articleItem.getShareVia());
			body = body.replace(ArticleItem.SRC_ICON_IMAGE, articleItem.getSrcIconImage());
		}

		// Article Map
		String map = "";
		if (!(articleItem.getLat().isEmpty() || articleItem.getLon().isEmpty())) {
			map = "<div id=\"" + "idmap:" + articleItem.getPostId() + "\"></div>";
		}

		// Article Endarticle_end_no_like
		String end = article_end_no_like.replace(ArticleItem.LIKE_COUNT,
				rountOffNumber(articleItem.getLikeCount()));
		end = end.replace(ArticleItem.COMMENT_COUNT, rountOffNumber(articleItem.getCommentCount()));
		end = end.replace(ArticleItem.ID_LIKE_COUNT, "idLikeCount:" + articleItem.getPostId());
		end = end.replace(ArticleItem.ID_CMT_COUNT, "idCmtCount:" + articleItem.getPostId());

		return begin + body_status + body + map + end;
	}

	public static String getStringHtmlShowAllComment(List<Comment> listComment, Context context) {
		String content = "";

		for (int i = 0; i < listComment.size(); i++) {

			String comment = FileUtils.readFileFromAssets(FileUtils.TEMPLATE_COMMENT, context);
			comment = comment.replace(Comment.SRC_PROFILE, listComment.get(i).getProfileIcon());
			comment = comment.replace(Comment.NAME_ACTOR, listComment.get(i).getNameActor());
			comment = comment.replace(Comment.COMMENT, listComment.get(i).getTextComment());
			String like_or_unlike = "";
			if (listComment.get(i).isUser_likes()) {
				like_or_unlike = "Unlike";
			} else {
				like_or_unlike = "Like";
			}
			comment = comment.replace(Comment.LIKE_OR_UNLIKE, like_or_unlike);
			comment = comment.replace(Comment.LIKE_COUNT,
					StringUtils.rountOffNumber(listComment.get(i).getLikeCount()));
			comment = comment.replace(Comment.TIME, listComment.get(i).getTime());
			comment = comment.replace(Comment.HREF_LIKE_COMMEN, Comment.HREF_LIKE_COMMEN
					+ listComment.get(i).getPostId());
			comment = comment.replace(Comment.ID_LIKE_COUNT, Comment.ID_LIKE_COUNT
					+ listComment.get(i).getPostId());
			comment = comment.replace(Comment.ID_LIKE_OR_UNLIKE, Comment.ID_LIKE_OR_UNLIKE
					+ listComment.get(i).getPostId());
			content = content + comment;
		}

		return content;
	}

	// 12345==12.3k and 12999=12.9K
	public static String rountOffNumber(int number) {
		String result = ".";
		if (number < 1000) {
			result = String.valueOf(number);
		} else {
			int x = (int) Math.floor(number / 1000);
			int temp = number % 1000;
			int y = (int) Math.floor(temp / 100);
			result = x + "." + y + "k";
		}

		return result;
	}

	@SuppressWarnings("deprecation")
	public static String getTimeTwitter(Status status) {
		Long now = (new Date()).getTime();
		Long then = status.getCreatedAt().getTime();
		if (now < then) {
			return "Just now";
		} else {
			Long diff = (now - then) / 1000;
			if (diff < 3600) {
				return Long.toString(diff / 60) + " min";
			} else if (diff < 24 * 3600) {
				long hours = diff / 3600;
				return Long.toString(hours) + " h";
			} else {
				int day = status.getCreatedAt().getDay();
				int mon = status.getCreatedAt().getMonth();
				String month = "Jan";
				switch (mon) {
				case 1:
					month = "Jan";
					break;
				case 2:
					month = "Feb";
					break;
				case 3:
					month = "Mar";
					break;
				case 4:
					month = "Apr";
					break;
				case 5:
					month = "May";
					break;
				case 6:
					month = "Jun";
					break;
				case 7:
					month = "Jul";
					break;
				case 8:
					month = "Aug";
					break;
				case 9:
					month = "Sep";
					break;
				case 10:
					month = "Oct";
					break;
				case 11:
					month = "Nov";
					break;
				case 12:
					month = "Dec";
					break;
				}
				return Long.toString(day) + " " + month;
			}
		}
	}

	private static String showLink(String message) {
		message = message + " ";
		String pattern = "([h][t][t][p]|[w][w][w])([^\\s]+)(\\s{1})";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(message);
		while (m.find()) {
			String s = m.group();
			String link = s.substring(0, s.length() - 1);
			String newCharacter = "<a href=\"" + link + "\">" + link + "</a>";
			message = message.replace(link, newCharacter);
		}
		return message;
	}

	public static boolean isEmptyString(String string) {
		if (string == null || string.equals("") || string.equals("null")) {
			return true;
		} else {
			return false;
		}
	}

	private static String fixLongMessage(String message, String id) {
		String[] listline = message.split("\r\n");
		if (listline.length > 5) {
			String newString = new String();
			for (int i = 0; i < listline.length; i++) {
				newString += (listline[i] + "</br>");
				if (i == 4)
					break;
			}
			return showLink(newString) + "</br><a href=\"" + ArticleItem.LOAD_MORE_LINK + id
					+ "\">...Read More</a>";
		}
		if (message.length() > 256) {
			return showLink(message.substring(0, 256)) + "<a href=\""
					+ ArticleItem.LOAD_MORE_LINK + id + "\">...Continue reading</a>";
		}
		return message;
	}

	private static String showMessage(boolean detail, ArticleItem articleItem) {
		String body = "";
		if (!detail) {
			if (!(articleItem.getLat().isEmpty() || articleItem.getLon().isEmpty())) {
				body = article_body_status.replace(ArticleItem.MESSAGE,
						fixLongMessage(articleItem.getMessage(), articleItem.getPostId())
								+ "</br> At <b>" + articleItem.getPlace() + "</b>");
			} else {
				body = article_body_status.replace(ArticleItem.MESSAGE,
						fixLongMessage(articleItem.getMessage(), articleItem.getPostId()));
			}
		} else {
			if (!(articleItem.getLat().isEmpty() || articleItem.getLon().isEmpty())) {
				body = article_body_status.replace(
						ArticleItem.MESSAGE,
						showLink(articleItem.getMessage()) + " At " + "<b>"
								+ articleItem.getPlace() + "</b>");
			} else {
				body = article_body_status.replace(ArticleItem.MESSAGE,
						showLink(articleItem.getMessage()));
			}
		}
		return body;
	}
}
