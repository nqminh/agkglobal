package com.example.besafepost.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import twitter4j.MediaEntity;
import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.URLEntity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.webkit.WebView;

import com.example.besafepost.activity.R;
import com.example.besafepost.fragment.FragmentTwitter;
import com.example.besafepost.interfaces.IActivityDelegate;
import com.example.besafepost.singleton.ManagerTwitterApp;

public class TwitterUtils {
	// str_tweet_item = str_tweet_item.replace(ICON_VIDEO,
	// "file:///android_asset/image/icon_video.png");
	public static String URL_VIDEO = "<!-- URL_VIDEO -->";
	public static String HREF_ICON_RETWEET = "<!-- HREF_ICON_RETWEET -->";
	public static String ICON_RETWEET = "<!-- ICON_RETWEET -->";
	public static String URL_PROFILE_ICON = "<!-- URL_PROFILE_ICON -->";
	public static String TWEET_OF_ME = "<!-- TWEET_OF_ME -->";
	public static String SCREEN_NAME = "<!-- SCREEN_NAME -->";
	public static String NAME = "<!-- NAME -->";
	public static String TIME = "<!-- TIME -->";
	public static String TEXT = "<!-- TEXT -->";
	public static String ICON_FAVORITE = "<!-- ICON_FAVORITE -->";
	public static String THIS_IS_NAME = "<!-- THIS IS NAME -->";
	public static String SRC_FULL_PHOTO = "<!-- SRC_FULL_PHOTO -->";
	public static String TAG_FOR_PHOTO_OR_VIDEO = "<!-- TAG_FOR_PHOTO_OR_VIDEO -->";
	public static String THIS_IS_SCREEN_NAME = "<!-- THIS IS SCREEN_NAME -->";
	public static String THIS_IS_TIME = "<!-- THIS IS TIME -->";
	// don't insert
	public static String THIS_IS_ICON_TIME = "<!-- THIS IS ICON TIME -->";
	public static String THIS_IS_RETWEETED_NAME = "<!-- THIS IS RETWEETED NAME-->";
	public static String THIS_IS_CONTENT = "<!-- THIS IS CONTENT -->";
	public static String THIS_IS_ICON_URL = "<!-- THIS IS ICON URL -->";
	public static String CLICK_TWITTER_ROW = "http://null/clickRow/";
	private static String THIS_IS_SHOW_LINK = "<!-- THIS IS SHOW LINK -->";
	private static String THIS_IS_HREF_SHOW_LINK = "<!-- THIS IS HREF SHOW LINK -->";

	private static String DETAILS_TEXT = "<!-- DETAILS_TEXT -->";
	private static String DETAILS_TIME = "<!-- DETAILS_TIME -->";
	private static String RETWEET_COUNT = "<!-- RETWEET_COUNT -->";
	private static String RETWEET_OR_RETWEETS = "<!-- RETWEET_OR_RETWEETS -->";
	private static String FAVORITE_COUNT = "<!-- FAVORITE_COUNT -->";
	private static String FAVORITE_OR_FAVORITES = "<!-- FAVORITE_OR_FAVORITES -->";
	private static String COUNT_RETWEET_OR_FAVORITE = "<!-- COUNT_RETWEET_OR_FAVORITE -->";
	private static String RETWEET_OR_FAVORITE = "<!-- RETWEET_OR_FAVORITE -->";
	private static String ICON_VIDEO = "<!-- ICON_VIDEO -->";
	private static String SHOW_IMAGE = "<!-- THIS IS IMAGE LINK -->";
	private static String SHOW_VIDEO = "<!-- THIS IS VIDEO LINK -->";
	private static String MEDIA_CONTENT = "<!-- THIS IS MEDIA CONTENT -->";

	public static ResponseList<Status> listStatus;
	private static ResponseList<Status> listStatusOlder;
	private static String tag_show_link;
	public static String CLICK_REPLY = "http://null/reply";
	public static String CLICK_RETWEET = "http://null/retweet";
	public static String CLICK_FAVORITE = "http://null/favorite";
	public static String CLICK_MORE = "http://null/more";
	public static String CLICK_LINK_ON_TWEET = "http://null/clickLink/";
	public static String CLICK_NAME_OR_API_SEARCH = "http://null/clickTaged/";

	// for action favorite.because library isn't right,use hash map for save
	// favorite count,retweet count and isfavorite
	public static HashMap<String, Integer> hMapFavorite = new HashMap<String, Integer>();
	public static String KEY_FAVORITE_COUNT = "key_favorite_count";
	public static String KEY_RETWEET_COUNT = "key_retweet_count";
	public static String KEY_IS_FAVORITE = "key_is_favorite";
	public static String KEY_IS_RETWEET = "key_is_retwet";
	public static final int TRUE = 1;
	public static final int FALSE = 0;
	static String article_tweet;
	static String article_retweet;
	static String article_tweet_img;
	static String article_tweet_vid;

	private static void loadTemplate(Context context) {
		article_tweet = FileUtils.readFileFromAssets(FileUtils.ARTICLE_TWEET, context);
		article_retweet = FileUtils.readFileFromAssets(FileUtils.ARTICLE_RETWEET, context);
		article_tweet_img = FileUtils.readFileFromAssets(FileUtils.ARTICLE_TWEET_IMG, context);
		article_tweet_vid = FileUtils.readFileFromAssets(FileUtils.ARTICLE_TWEET_VIDEO, context);
	}

	public static void getFeedTwitter(Context context, int limit, Twitter twitter,
			final IActivityDelegate mDelegate, WebView mWebView) {
		String newFeedsString = "";
		loadTemplate(context);
		tag_show_link = FileUtils.readFileFromAssets(FileUtils.TAG_SHOW_LINK, context);
		if (listStatusOlder != null) {
			listStatusOlder.clear();
		}
		if (limit == 20) {
			if (listStatus != null) {
				listStatus.clear();
			}
			try {
				listStatusOlder = twitter.getHomeTimeline();
				listStatus = twitter.getHomeTimeline();
			} catch (TwitterException e) {
				e.printStackTrace();
			}
			hMapFavorite.clear();
		} else {
			ResponseList<Status> listStatusTemp;
			try {
				Paging page = new Paging();
				page.setCount(limit);
				listStatusTemp = twitter.getHomeTimeline(page);
				for (int i = listStatusTemp.size() - StringUtils.LIMIT_TWITTER_ADVANCE; i < listStatusTemp
						.size(); i++) {
					boolean isDuplicate = false;
					long id = listStatusTemp.get(i).getId();
					for (int j = 0; j < listStatus.size(); j++) {
						if (id == listStatus.get(j).getId()) {
							isDuplicate = true;
							break;
						}
					}
					if (!isDuplicate) {
						listStatusOlder.add(listStatusTemp.get(i));
						listStatus.add(listStatusTemp.get(i));
					}

				}
			} catch (TwitterException e) {
				e.printStackTrace();
			}
		}
		if (listStatusOlder != null) {
			for (int i = 0; i < listStatusOlder.size(); i++) {
				newFeedsString += generateCode(listStatusOlder.get(i), context);
			}
			newFeedsString = newFeedsString.replace("\n", "");
			newFeedsString = newFeedsString.replace("\t", "");
			newFeedsString = newFeedsString.replace("\r", "");
			newFeedsString = newFeedsString.replace("'", "&#39;");
			String insertWebview;
			if (limit == 20) {
				insertWebview = "javascript:insertCodeOnBottom('" + newFeedsString + "',true);";
			} else {
				insertWebview = "javascript:insertCodeOnBottom('" + newFeedsString + "',false);";
			}
			mWebView.loadUrl(insertWebview);
			mDelegate.onGetSuccess(FragmentTwitter.GET_FEED_TWITTER);
		} else {
			mDelegate.onGetFailure(FragmentTwitter.GET_FEED_TWITTER);
		}
	}

	private static String generateCode(Status status, Context context) {
		String str_tweet_item;
		// Log.i(Log.TAG, status.toString());
		URLEntity[] urlEntity;
		MediaEntity[] mediaEntity;
		if (status.getRetweetedStatus() == null) {

			urlEntity = status.getURLEntities();

			mediaEntity = status.getMediaEntities();
			str_tweet_item = generateSameString(article_tweet, status, urlEntity, mediaEntity);
		} else {
			Status reStatus = status.getRetweetedStatus();
			urlEntity = reStatus.getURLEntities();
			mediaEntity = reStatus.getMediaEntities();
			str_tweet_item = generateSameString(article_retweet, reStatus, urlEntity, mediaEntity);
			str_tweet_item = str_tweet_item.replace(THIS_IS_RETWEETED_NAME, status.getUser()
					.getName());
		}
		// insert iconVideo
		boolean hasVideoYouTube = false;
		boolean hasImage = false;
		String strUrlVideo = "";
		if (urlEntity.length > 0) {
			strUrlVideo = urlEntity[0].getExpandedURL();
			strUrlVideo = strUrlVideo.replace(" ", "");
			if (isYoutubeLink(strUrlVideo)) {
				hasVideoYouTube = true;
				String bodyVideo = FileUtils.readFileFromAssets(
						context.getResources().getString(R.string.tweet_body_video), context);
				bodyVideo = bodyVideo.replace(URL_VIDEO, getYouTubeEmbedLink(strUrlVideo));
				str_tweet_item = str_tweet_item.replace(MEDIA_CONTENT, "<\br>" + bodyVideo);
			}
		}
		if (mediaEntity.length > 0) {
			hasImage = true;
//			Log.d(Log.TAG, mediaEntity[0].getMediaURL());
			String bodyPhoto = FileUtils.readFileFromAssets(FileUtils.TWEET_BODY_PHOTO, context);
			String strUrl = "";
			strUrl = mediaEntity[0].getMediaURL();
			strUrl = strUrl.replace(" ", "");
			bodyPhoto = bodyPhoto.replace(SRC_FULL_PHOTO, strUrl);
			str_tweet_item = str_tweet_item.replace(MEDIA_CONTENT, "<\br>" + bodyPhoto);
		}
		if (hasVideoYouTube) {
			str_tweet_item = str_tweet_item.replace(ICON_VIDEO,
					"file:///android_asset/image/icon_video.png");
		}
		if (hasImage) {
			str_tweet_item = str_tweet_item.replace(ICON_VIDEO,
					"file:///android_asset/image/icon_image.png");
		}
		str_tweet_item = str_tweet_item.replace(THIS_IS_TIME, StringUtils.getTimeTwitter(status));
		str_tweet_item = str_tweet_item.replace(CLICK_TWITTER_ROW,
				CLICK_TWITTER_ROW + status.getId());

		return str_tweet_item;
	}

	private static String generateSameString(String str, Status status, URLEntity[] urlEntity,
			MediaEntity[] mediaEntity) {
		String str_tweet_item = str;
		str_tweet_item = str_tweet_item.replace(THIS_IS_NAME, status.getUser().getName());
		str_tweet_item = str_tweet_item.replace(THIS_IS_SCREEN_NAME, status.getUser()
				.getScreenName());
		str_tweet_item = str_tweet_item.replace(THIS_IS_ICON_URL, status.getUser()
				.getProfileImageURLHttps());
		String content = status.getText();

		str_tweet_item = str_tweet_item.replace(THIS_IS_CONTENT, content);
		// insert tag link into text

		for (int j = 0; j < urlEntity.length; j++) {
			String url = urlEntity[j].getURL();
			String displayUrl = urlEntity[j].getDisplayURL();
			str_tweet_item = str_tweet_item.replace(url, showLinkOnTweet(displayUrl, url));
		}

		for (int j = 0; j < mediaEntity.length; j++) {
			String url = mediaEntity[j].getURL();
			String displayUrl = mediaEntity[j].getDisplayURL();
			str_tweet_item = str_tweet_item.replace(url, showLinkOnTweet(displayUrl, url));
		}
		return str_tweet_item;
	}

	private static String showLinkOnTweet(String displayUrl, String url) {
		String showLink = "";
		showLink = tag_show_link.replace(THIS_IS_SHOW_LINK, displayUrl);
		showLink = showLink.replace(THIS_IS_HREF_SHOW_LINK, url);
		return showLink;
	}

	public static void showTweetDetails(Context context, Status status, Status retweetStatus,
			WebView mWebView) {
		String beginDetails, bodyDetails = "";
		int favoritesCount;
		int retweetsCount;
		boolean isFavorite = false;
		boolean canRetweet = true;
		long userId;
		if (retweetStatus == null) {
			// uses status and template normal
			beginDetails = FileUtils.readFileFromAssets(FileUtils.TWEET_BEGIN_NORMAL, context);
			beginDetails = beginDetails.replace(THIS_IS_NAME, status.getUser().getName());
			beginDetails = beginDetails.replace(THIS_IS_SCREEN_NAME, status.getUser()
					.getScreenName());
			beginDetails = beginDetails.replace(THIS_IS_ICON_URL, status.getUser()
					.getProfileImageURLHttps());

			// get favorite and retweet count
			favoritesCount = getFavoriteCount(status);
			retweetsCount = getRetweetCount(status);
			isFavorite = getIsFavorite(status);
			userId = status.getUser().getId();
			canRetweet = !status.getUser().isProtected();
		} else {
			beginDetails = FileUtils.readFileFromAssets(FileUtils.TWEET_BEGIN_RETWEET, context);
			beginDetails = beginDetails.replace(THIS_IS_NAME, retweetStatus.getUser().getName());
			beginDetails = beginDetails.replace(THIS_IS_SCREEN_NAME, retweetStatus.getUser()
					.getScreenName());
			beginDetails = beginDetails.replace(THIS_IS_ICON_URL, retweetStatus.getUser()
					.getProfileImageURLHttps());
			beginDetails = beginDetails.replace(THIS_IS_RETWEETED_NAME, status.getUser().getName());

			// get favorite and retweet count
			favoritesCount = getFavoriteCount(retweetStatus);
			retweetsCount = getRetweetCount(retweetStatus);
			isFavorite = getIsFavorite(retweetStatus);
			userId = retweetStatus.getUser().getId();
			canRetweet = !retweetStatus.getUser().isProtected();
		}
		// set favorite and retweet count for details activity
		if (favoritesCount == 0 && retweetsCount == 0) {
			bodyDetails = FileUtils.readFileFromAssets(FileUtils.TWEET_BODY_STATUS, context);
		} else if (favoritesCount == 0 && retweetsCount != 0) {
			bodyDetails = FileUtils
					.readFileFromAssets(FileUtils.TWEET_BODY_STATUS_RETWEET, context);
			bodyDetails = bodyDetails.replace(COUNT_RETWEET_OR_FAVORITE,
					String.valueOf(retweetsCount));
			if (retweetsCount == 1) {
				bodyDetails = bodyDetails.replace(RETWEET_OR_FAVORITE, "RETWEET");
			} else {
				bodyDetails = bodyDetails.replace(RETWEET_OR_FAVORITE, "RETWEETS");
			}

		} else if (favoritesCount != 0 && retweetsCount == 0) {
			bodyDetails = FileUtils
					.readFileFromAssets(FileUtils.TWEET_BODY_STATUS_RETWEET, context);
			bodyDetails = bodyDetails.replace(COUNT_RETWEET_OR_FAVORITE,
					String.valueOf(favoritesCount));
			if (favoritesCount == 1) {
				bodyDetails = bodyDetails.replace(RETWEET_OR_FAVORITE, "FAVORITE");
			} else {
				bodyDetails = bodyDetails.replace(RETWEET_OR_FAVORITE, "FAVORITES");
			}

		} else if (favoritesCount != 0 && retweetsCount != 0) {
			bodyDetails = FileUtils.readFileFromAssets(
					FileUtils.TWEET_BODY_STATUS_RETWEET_FAVORITE, context);
			bodyDetails = bodyDetails.replace(RETWEET_COUNT, String.valueOf(retweetsCount));
			bodyDetails = bodyDetails.replace(FAVORITE_COUNT, String.valueOf(favoritesCount));
			if (favoritesCount == 1) {
				bodyDetails = bodyDetails.replace(FAVORITE_OR_FAVORITES, "FAVORITE");
			} else {
				bodyDetails = bodyDetails.replace(FAVORITE_OR_FAVORITES, "FAVORITES");
			}
			if (retweetsCount == 1) {
				bodyDetails = bodyDetails.replace(RETWEET_OR_RETWEETS, "RETWEET");
			} else {
				bodyDetails = bodyDetails.replace(RETWEET_OR_RETWEETS, "RETWEETS");
			}
		}

		URLEntity[] urlEntity;
		MediaEntity[] mediaEntity;
		String bodyPhoto = "";
		String time;
		if (retweetStatus == null) {
			time = status.getCreatedAt().toString();
			time = convertTime(time);
			String content = showNameTaged(status.getText());
			content = showApiSearch(content);
			bodyDetails = bodyDetails.replace(DETAILS_TEXT, content);
			urlEntity = status.getURLEntities();
			mediaEntity = status.getMediaEntities();
			bodyDetails = bodyDetails.replace(DETAILS_TIME, time);

		} else {
			time = retweetStatus.getCreatedAt().toString();
			time = convertTime(time);
			String content = showNameTaged(retweetStatus.getText());
			content = showApiSearch(content);
			bodyDetails = bodyDetails.replace(DETAILS_TEXT, content);
			urlEntity = retweetStatus.getURLEntities();
			mediaEntity = retweetStatus.getMediaEntities();
			bodyDetails = bodyDetails.replace(DETAILS_TIME, time);
		}
		// insert icon favorite
		if (isFavorite) {
			bodyDetails = bodyDetails.replace(ICON_FAVORITE,
					"file:///android_asset/image/btn_star_active.png");
		} else {
			bodyDetails = bodyDetails.replace(ICON_FAVORITE,
					"file:///android_asset/image/btn_star_inactive.png");
		}
		if (userId == ManagerTwitterApp.getInstance().getUserId()) {
			canRetweet = false;
		}
		// if()
		// insert icon retweet and click retweet
		if (!canRetweet) {
			// this tweet can't retweet, replace by clouded icon
			bodyDetails = bodyDetails.replace(ICON_RETWEET,
					"file:///android_asset/image/btn_retweet_protected.png");
		} else {
			// this tweet can retweet, replace by apparent icon
			bodyDetails = bodyDetails.replace(ICON_RETWEET,
					"file:///android_asset/image/btn_retweet.png");
			bodyDetails = bodyDetails.replace(HREF_ICON_RETWEET, CLICK_RETWEET);
		}
		// insert tag link into text
		for (int j = 0; j < urlEntity.length; j++) {
			String url = urlEntity[j].getURL();
			String displayUrl = urlEntity[j].getDisplayURL();
			bodyDetails = bodyDetails.replace(url, showLinkOnTweet(displayUrl, url));
		}
		for (int j = 0; j < mediaEntity.length; j++) {
			String url = mediaEntity[j].getURL();
			String displayUrl = mediaEntity[j].getDisplayURL();
			bodyDetails = bodyDetails.replace(url, showLinkOnTweet(displayUrl, url));
		}

		String detailsTweetString = beginDetails + bodyDetails;

		// insert bodyphoto 
		if (mediaEntity.length > 0) {
			bodyPhoto = FileUtils.readFileFromAssets(FileUtils.TWEET_BODY_PHOTO, context);
			String strUrl = "";
			strUrl = mediaEntity[0].getMediaURL();
			strUrl = strUrl.replace(" ", "");
			bodyPhoto = bodyPhoto.replace(SRC_FULL_PHOTO, strUrl);
			detailsTweetString = detailsTweetString.replace(TAG_FOR_PHOTO_OR_VIDEO, bodyPhoto);
		}
		// insert bodyvideo
		boolean hasVideoYouTube = false;
		String strUrlVideo = "";
		String bodyVideo = "";
		if (urlEntity.length > 0) {
			strUrlVideo = urlEntity[0].getExpandedURL();
			strUrlVideo = strUrlVideo.replace(" ", "");
			hasVideoYouTube = isYoutubeLink(strUrlVideo);
		}
		if (hasVideoYouTube) {
			bodyVideo = FileUtils.readFileFromAssets(
					context.getResources().getString(R.string.tweet_body_video), context);
			bodyVideo = bodyVideo.replace(URL_VIDEO, getYouTubeEmbedLink(strUrlVideo));
			detailsTweetString = detailsTweetString.replace(TAG_FOR_PHOTO_OR_VIDEO, bodyVideo);
		}
		detailsTweetString = detailsTweetString.replace("\n", "");
		detailsTweetString = detailsTweetString.replace("\t", "");
		detailsTweetString = detailsTweetString.replace("\r", "");
		detailsTweetString = detailsTweetString.replace("'", "&#39;");

		String insertWebviewDetailsTweet = "javascript:insertCode('" + detailsTweetString + "');";
		mWebView.loadUrl(insertWebviewDetailsTweet);
	}

	public static String getHtmlForSendMail(Status status, Context context) {
		String htmlSendMail = "";
		htmlSendMail = FileUtils.readFileFromAssets(FileUtils.TEMPLATE_SEND_MAIL_TWEET, context);
		htmlSendMail = htmlSendMail.replace(URL_PROFILE_ICON, status.getUser()
				.getProfileImageURLHttps());
		htmlSendMail = htmlSendMail.replace(NAME, status.getUser().getName());
		htmlSendMail = htmlSendMail.replace(SCREEN_NAME, status.getUser().getScreenName());
		String time = status.getCreatedAt().toString();
		time = convertTime(time);
		htmlSendMail = htmlSendMail.replace(TIME, time);
		htmlSendMail = htmlSendMail.replace(TWEET_OF_ME, "https://twitter.com/"
				+ status.getUser().getScreenName());
		htmlSendMail = htmlSendMail.replace(TEXT, status.getText());
		return htmlSendMail;

	}

	public static int getFavoriteCount(Status status) {
		if (hMapFavorite.get(status.getId() + KEY_FAVORITE_COUNT) == null) {
			return status.getFavoriteCount();
		} else {
			return hMapFavorite.get(status.getId() + KEY_FAVORITE_COUNT);
		}
	}

	public static int getRetweetCount(Status status) {
		if (hMapFavorite.get(status.getId() + KEY_RETWEET_COUNT) == null) {
			return status.getRetweetCount();
		} else {
			return hMapFavorite.get(status.getId() + KEY_RETWEET_COUNT);
		}
	}

	public static boolean getIsFavorite(Status status) {
		if (hMapFavorite.get(status.getId() + TwitterUtils.KEY_IS_FAVORITE) == null) {
			return status.isFavorited();
		} else {
			int temp = TwitterUtils.hMapFavorite.get(status.getId() + TwitterUtils.KEY_IS_FAVORITE);
			if (temp == TRUE) {
				return true;
			} else {
				return false;
			}
		}
	}

	public static boolean getIsReTweet(Status status) {
		if (hMapFavorite.get(status.getId() + TwitterUtils.KEY_IS_RETWEET) == null) {
			return status.isRetweetedByMe();
		} else {
			int temp = TwitterUtils.hMapFavorite.get(status.getId() + TwitterUtils.KEY_IS_RETWEET);
			if (temp == TRUE) {
				return true;
			} else {
				return false;
			}
		}
	}

	public static void setFavoriteCount(long idTweet, int favoriteCount) {
		hMapFavorite.put(idTweet + TwitterUtils.KEY_FAVORITE_COUNT, favoriteCount);
	}

	public static void setRetweetCount(long idTweet, int retweetCOunt) {
		hMapFavorite.put(idTweet + TwitterUtils.KEY_RETWEET_COUNT, retweetCOunt);
	}

	public static void setIsFavorite(long idTweet, boolean isFavorite) {
		if (isFavorite) {
			hMapFavorite.put(idTweet + TwitterUtils.KEY_IS_FAVORITE, TRUE);
		} else {
			hMapFavorite.put(idTweet + TwitterUtils.KEY_IS_FAVORITE, FALSE);
		}
	}

	public static void setIsRetweet(long idTweet, boolean isRetweetedByMe) {
		if (isRetweetedByMe) {
			hMapFavorite.put(idTweet + TwitterUtils.KEY_IS_RETWEET, TRUE);
		} else {
			hMapFavorite.put(idTweet + TwitterUtils.KEY_IS_RETWEET, FALSE);
		}
	}

	public static void replyTweet(String text, long idTweet) {
		try {
			StatusUpdate stat = new StatusUpdate(text);
			stat.setInReplyToStatusId(idTweet);
			FragmentTwitter.twitter.updateStatus(stat);
		} catch (TwitterException e) {
			e.printStackTrace();
		}
	}

	@SuppressLint("SimpleDateFormat")
	private static String convertTime(String s) {
		Date date;
		String str = null;
		try {
			date = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").parse(s);
			str = new SimpleDateFormat("MM/dd/yy, HH:mm a").format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	private static String showNameTaged(String content) {
		String pattern = "(@)(\\w+)(\\s{1})";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(content);
		while (m.find()) {
			String s = m.group();
			String screenName = s.substring(1, s.length() - 1);
			String newCharacter = "<a href=\"" + CLICK_NAME_OR_API_SEARCH + "https://twitter.com/"
					+ screenName + "\">" + s + "</a>";
			content = content.replace(s, newCharacter);
		}
		return content;
	}

	private static String showApiSearch(String content) {
		content = content + " ";
		String pattern = "(#)(\\w+)(\\s{1})";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(content);
		while (m.find()) {
			String s = m.group();
			String keySearch = s.substring(1, s.length() - 1);
			String newCharacter = "<a href=\"" + CLICK_NAME_OR_API_SEARCH
					+ "https://twitter.com/search?q=%23%@&src=hash" + "\">" + s + "</a>";
			newCharacter = newCharacter.replace("%@", keySearch);
			content = content.replace(s, newCharacter);
		}
		return content;
	}

	private static boolean isYoutubeLink(String strUrlVideo) {
		if (strUrlVideo.contains("http://www.youtube.com/watch?v=")
				|| strUrlVideo.contains("http://m.youtube.com/watch?v=")
				|| strUrlVideo.contains("http://www.youtube.com/watch?feature=player_embedded&v=")
				|| strUrlVideo.contains("http://youtu.be/")
				|| strUrlVideo.contains("https://www.youtube.com/watch?v=")
				|| strUrlVideo.contains("http://m.youtube.com/watch?feature=player_embedded&v=")) {
			return true;
		} else
			return false;
	}

	private static String getYouTubeEmbedLink(String link) {
		String strUrlVideo = link;
		strUrlVideo = strUrlVideo.replace("http://www.youtube.com/watch?v=",
				"http://www.youtube.com/embed/");
		strUrlVideo = strUrlVideo.replace("http://m.youtube.com/watch?v=",
				"http://www.youtube.com/embed/");
		strUrlVideo = strUrlVideo.replace(
				"http://www.youtube.com/watch?feature=player_embedded&v=",
				"http://www.youtube.com/embed/");
		strUrlVideo = strUrlVideo.replace("http://youtu.be/", "http://www.youtube.com/embed/");
		strUrlVideo = strUrlVideo.replace("https://www.youtube.com/watch?v=",
				"https://www.youtube.com/embed/");
		strUrlVideo = strUrlVideo.replace("http://m.youtube.com/watch?feature=player_embedded&v=",
				"http://www.youtube.com/embed/");
		return strUrlVideo;
	}
}
