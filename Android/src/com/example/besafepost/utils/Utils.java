package com.example.besafepost.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.besafepost.BesafePostApplication;

public class Utils {
	public static final String REQUEST_GET_COMMENT = "REQUEST_GET_COMMENT";
	public static final String REQUEST_POST_NEW_TWEET = "REQUEST_POST_NEW_TWEET";
	public static final String REQUEST_SEND_DIRECT_SMS = "REQUEST_SEND_DIRECT_SMS";
	public static final int POST_COMMENT = 6;
	public static final int REQUEST_CODE_SHOW_ALBUM = 5;
	public static final int REQUEST_CODE_POST_STATUS = 2;
	public static final int REQUEST_CODE_POST_PHOTO = 3;
	public static final int CALL_COMMENT_ACTIVITY = 4;
	public static String REQUEST_POST_COMMENT = "REQUEST_POST_COMMENT";
	public static final int START_COUNTDOWN_FOR_POST_FB_STATUS = 1;
	public static final String REQUEST_POST_PHOTO = "request_post_photo";
	public static final String REQUEST_GET_MY_PROFILE = "request_get_my_profile";
	public static final int CALL_CMT_ON_PHOTO_ACT = 7;
	public static String GET_DETAILS_PHOTO = "GET_DETAILS_PHOTO";
	public static String REQUEST_POST_STATUS = "REQUEST_POST_STATUS";
	public static String REQUEST_GET_LIST_FRIENDLIST = "request get list friendlist";
	public static String LOGIN_FACEBOOK = "facebook_login";
	public static String REQUEST_SHARE_POST = "request share post";
	public static String keyPrefSMS = "keyPrefSMS";
	public static String keyBannedWord = "keyBannedWord";
	public static String keyBannedAddress = "keyBannedAddress";
	public static String keyAcccountFb = "keyAcccountFb";
	public static String keyAcccountTwitter = "keyAcccountTwitter";
	public static final int TAKE_PICTURE = 0;
	public static final int PICK_EXISTING_PHOTO_RESULT_CODE = 2;
	public static final int PHOTO_RESULT_CODE = 1;
	public static final int START_COUNT_DOWN_FOR_NEWTWEET = 7;
	public static final int START_COUNT_DOWN_FOR_SEND_SMS = 8;
	public static final int START_COUNT_DOWN_FOR_RETWEET = 12;
	public static final int START_NEW_TWEET_ACT = 9;
	public static final int START_COUNT_DOWN_REPLY = 10;
	public static final int START_REPLY_ACT = 11;
	public static final int START_ACT_FOR_GET_TOKEN_TWITTER = 13;
	public static final int CALL_TIP_FOR_LIKE_POST = 14;
	public static final int CALL_TIP_FOR_SHARE_POST = 15;
	public static final int CALL_TIP_FOR_CHECK_IN = 16;
	public static final int CALL_TIP_FOR_FAVORITE = 17;
	public static final int CALL_TIP_FOR_RETWEET = 18;
	public static final int START_COUNTDOWN_FOR_POST_PHOTO_FB = 19;
	public static final int START_COUNTDOWN_FOR_POST_COMMENT = 20;
	public static final int START_COUNTDOWN_FOR_POST_COMMENT_ON_PHOTO = 21;
	public static final int CALL_TIP_FOR_LIKE_POST_IN_COMMENT_ACTIVITY = 22;
	public static final int CALL_TIP_FOR_LIKE_COMMENT = 23;
	public static final int AFTER_SHARE_FACEBOOK = 24;
	public static final int REQUEST_CALL_PHOTO_ACT = 25;

	public static void hideKeyboard(Activity activity) {
		try {
			InputMethodManager inputManager = (InputMethodManager) activity
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (inputManager.isActive()) {
				inputManager
						.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
			}
		} catch (Exception e) {
		}
	}

	public static void showKeyboard(Activity activity, EditText edt) {
		edt.requestFocus();
		InputMethodManager mgr = (InputMethodManager) activity
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		mgr.showSoftInput(edt, InputMethodManager.SHOW_IMPLICIT);
	}

	public static void savefile(String string) {
		String filename = "myfile.html";
		File file = new File(Environment.getExternalStorageDirectory(), filename);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(file);
			fos.write(string.getBytes());
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String getStringFromFreference(String key) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(BesafePostApplication.getContext());
		return pref.getString(key, "");
	}

	public static void saveStringIntoFreference(String key, String stg) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(BesafePostApplication.getContext());
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(key, stg);
		editor.commit();
	}

	public static List<String> getListStringFromPreference(String key) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(BesafePostApplication.getContext());
		List<String> list = new ArrayList<String>();
		String stg = pref.getString(key, "");
		Log.i("JSON", stg);
		if (!TextUtils.isEmpty(stg)) {
			try {
				JSONObject jsonObject = new JSONObject(stg);

				for (int i = 0; i < jsonObject.length(); i++) {
					list.add(jsonObject.getString(i + ""));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return list;
	}

	public static void putStringIntoListStringPreference(String key, String stg) {
		JSONObject jsonObject = new JSONObject();
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(BesafePostApplication.getContext());
		try {
			jsonObject.put("0", stg);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i = 0; i < getListStringFromPreference(key).size(); i++) {
			try {
				jsonObject.put(i + 1 + "", getListStringFromPreference(key).get(i));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(key, jsonObject.toString());
		editor.commit();
	}

	public static void deleteStringInListStringPreference(String key, int position) {
		JSONObject jsonObject = new JSONObject();
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(BesafePostApplication.getContext());
		for (int i = 0; i < getListStringFromPreference(key).size(); i++) {
			if (i < position) {
				try {
					jsonObject.put(i + "", getListStringFromPreference(key).get(i));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (i == position) {
				// do nothing
			} else if (i > position) {
				try {
					jsonObject.put(i - 1 + "", getListStringFromPreference(key).get(i));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		SharedPreferences.Editor editor = pref.edit();
		editor.putString(key, jsonObject.toString());
		editor.commit();
	}

	public static boolean isBlockedWord(String text) {
		boolean isBlocked = false;
		List<String> listWordBlocked = getListStringFromPreference(keyBannedWord);
		for (int i = 0; i < listWordBlocked.size(); i++) {
			if (text.contains(listWordBlocked.get(i))) {
				isBlocked = true;
				break;
			}
		}
		return isBlocked;
	}

	public static boolean isBlockedAddress(String text) {
		boolean isBlocked = false;
		List<String> listWordBlocked = getListStringFromPreference(keyBannedAddress);
		for (int i = 0; i < listWordBlocked.size(); i++) {
			if (text.contains(listWordBlocked.get(i))) {
				isBlocked = true;
				break;
			}
		}
		return isBlocked;
	}

	public static boolean writeObject2File(Context context, Serializable obj, String fileName) {
		try {
			File cacheDir = context.getCacheDir();
			File fileSave = new File(cacheDir, fileName);
			FileOutputStream out = new FileOutputStream(fileSave);
			ObjectOutputStream os = new ObjectOutputStream(out);
			os.writeObject(obj);
			os.flush();
			os.close();
			return true;
		} catch (Exception e) {
			Log.e(Log.TAG, e);
			return false;
		}
	}

	public static Object loadObjectFromFile(Context context, String fileName) {

		String loadFile = context.getCacheDir() + "/" + fileName;
		Log.d("besafe", "Load user setting from: " + loadFile);
		File file = new File(loadFile);
		if (file.exists()) {
			try {
				FileInputStream fin = new FileInputStream(file);
				ObjectInputStream os = new ObjectInputStream(fin);
				Object obj = os.readObject();
				os.close();
				return obj;
			} catch (Exception e) {
				Log.e(Log.TAG, e);
				return null;
			}
		} else {
			return null;
		}

	}

	public static boolean isFirstInstall() {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(BesafePostApplication.getContext());
		boolean b = pref.getBoolean("BESAFE_INSTALLED", true);
		if (b) {
			SharedPreferences.Editor editor = pref.edit();
			editor.putBoolean("BESAFE_INSTALLED", false);
			editor.commit();
			return true;
		} else {
			return false;
		}
	}

	public static boolean IsBuddyFunction() {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(BesafePostApplication.getContext());
		return pref.getBoolean("BUDDY_FUNCTION", false);

	}

	public static void setBuddyFunction(boolean isBuddy) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(BesafePostApplication.getContext());
		SharedPreferences.Editor editor = pref.edit();
		editor.putBoolean("BUDDY_FUNCTION", isBuddy);
		editor.commit();
	}
}
