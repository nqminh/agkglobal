package com.example.besafepost.utils;

public class Const {
    public static final String CONSUMER_KEY = "bSnBQFNzMi1ufoMZBHB2rw";
    public static final String CONSUMER_SECRET = "6TFvXs5LR8hrsdIfYEOneLSifZAJGwsLKJFhTHkBVGE";
    public static final String CALLBACK_URL = "twitterapp://connect";

 // Twitter oauth urls
    public static final String IEXTRA_AUTH_URL = "auth_url";
    public static final String IEXTRA_OAUTH_VERIFIER = "oauth_verifier";
    public static final String IEXTRA_OAUTH_TOKEN = "oauth_token";

    public static final String PREF_NAME = "com.example.android-twitter-oauth-demo";
    public static final String PREF_KEY_ACCESS_TOKEN = "access_token";
    public static final String PREF_KEY_ACCESS_TOKEN_SECRET = "access_token_secret";
    
 // Preference Constants
 	public static final String PREF_KEY_USER_ID = "user_id";
 	public static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
 	public static final String PREF_KEY_TWITTER_LOGIN = "isTwitterLogedIn";
 	
 	public static final String HOST = "http://bio.vmodev.com:7200/besafe/api.php?m=";

// 	public static final String HOST = "http://besafeapps.com/api/api.php?m=";
 	
 	public static final String URL_REGISTER = HOST + "registerUser";
 	public static final String URL_POST_TO_SERVER = HOST + "postData";
 	public static final String URL_CHECK_ACCOUNT = HOST + "checkAccount";
 	public static final String URL_UPDATE_BUDDY = HOST + "updateBuddy";
 	public static final String URL_REMOVE_BUDDY = HOST + "removeBuddy";
 	public static final String URL_GET_POST_PENDING = HOST + "getPostPending";
 	public static final String URL_UPDATE_ACCEPT = HOST + "updateAccept";
 	public static final String URL_GET_POST_ACCEPT = HOST + "getPostAccept";
 	public static final String URL_REMOVE_ACCEPTED = HOST + "removeAccepted";
 	public static final String URL_FORGOT = HOST + "forgotPassword";
}
