package com.example.besafepost.utils;

import java.util.Date;

public class DateUtils {

	public static String timeAgoInWords(Date from) {
		Date now = new Date();
		long difference = now.getTime() - from.getTime();
		long distanceInMin = difference / 60000;

		if (0 <= distanceInMin && distanceInMin <= 1) {
			return "Less than 1 minute ago";
		} else if (1 <= distanceInMin && distanceInMin <= 45) {
			return distanceInMin + " minutes ago";
		} else if (45 <= distanceInMin && distanceInMin <= 89) {
			return "About 1 hour";
		} else if (90 <= distanceInMin && distanceInMin <= 1439) {
			return "About " + (distanceInMin / 60) + " hours ago";
		} else if (1440 <= distanceInMin && distanceInMin <= 2529) {
			return "1 day";
		} else if (2530 <= distanceInMin && distanceInMin <= 43199) {
			return (distanceInMin / 1440) + "days ago";
		} else if (43200 <= distanceInMin && distanceInMin <= 86399) {
			return "About 1 month ago";
		} else if (86400 <= distanceInMin && distanceInMin <= 525599) {
			return "About " + (distanceInMin / 43200) + " months ago";
		} else {
			long distanceInYears = distanceInMin / 525600;
			return "About " + distanceInYears + " years ago";
		}
	}

}