package com.example.besafepost.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.besafepost.activity.R;
import com.example.besafepost.interfaces.IAddLocation;
import com.example.besafepost.interfaces.IDeSkipBanned;
import com.example.besafepost.interfaces.IdeResumeFromHome;

public class DialogUtils {
	static ProgressDialog dialog;

	public static void showProgressDialog(Context context, String title,
			String msg, boolean cancelAble) {
		dismissDialog();
		dialog = ProgressDialog.show(context, title, msg);
		dialog.setCancelable(cancelAble);
	}

	public static void dismissDialog() {
		if (dialog != null && dialog.isShowing())
			dialog.dismiss();
	}

	public static void showAlertDialog(Context context, String title,
			String message, Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting OK Button
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,
							final int which) {
					}
				});

		// Showing Alert Message
		alertDialog.show();
	}

	public static void showDialogBannedWord(Context context,
			boolean isBannedWord, boolean isBannedAddress) {
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.custom_dialog_notify_banned);
		TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
		TextView tvNotify = (TextView) dialog.findViewById(R.id.tvNotice);
		if (isBannedAddress && isBannedWord) {
			tvTitle.setText(R.string.title_banned_word_address);
			tvNotify.setText(R.string.notify_banned_word_address);
		} else if (isBannedAddress) {
			tvTitle.setText(R.string.title_banned_address);
			tvNotify.setText(R.string.notify_banned_address);
		} else if (isBannedWord) {
			tvTitle.setText(R.string.title_banned_word);
			tvNotify.setText(R.string.notify_banned_word);
		}

		Button btnOK = (Button) dialog.findViewById(R.id.btnOK);
		btnOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	public static void showDialogBannedWordEnableSendAnyway(Context context,
			boolean isBannedWord, boolean isBannedAddress,
			final IDeSkipBanned mIDeSkipBanned) {
		final Dialog dialog = new Dialog(context);
		dialog.setContentView(R.layout.custom_dialog_notify_banned_enabled_send_anyway);
		TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
		TextView tvNotify = (TextView) dialog.findViewById(R.id.tvNotice);
		Button btnStop = (Button) dialog.findViewById(R.id.btnClose);
		Button btnSendAnyway = (Button) dialog
				.findViewById(R.id.btnActionAnyway);

		if (isBannedAddress && isBannedWord) {
			tvTitle.setText(R.string.title_banned_word_address);
			tvNotify.setText(R.string.notify_banned_word_address);
		} else if (isBannedAddress) {
			tvTitle.setText(R.string.title_banned_address);
			tvNotify.setText(R.string.notify_banned_address);
		} else if (isBannedWord) {
			tvTitle.setText(R.string.title_banned_word);
			tvNotify.setText(R.string.notify_banned_word);
		}

		btnStop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		btnSendAnyway.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				mIDeSkipBanned.sendAnyway();

			}
		});
		dialog.show();
	}

	public static void showDialogWhenResumeFromHome(Context context,
			final IdeResumeFromHome mIdeResumeFromHome, boolean isTipOfFb,
			String action, String notify) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_when_resume_from_home);
		TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
		if (isTipOfFb) {
			tvTitle.setText("Besafe Post");
		} else {
			tvTitle.setText("Besafe Tweet");
		}
		TextView tvNotify = (TextView) dialog.findViewById(R.id.tvNotice);
		tvNotify.setText(notify);
		Button btnStop = (Button) dialog.findViewById(R.id.btnStop);
		Button btnSendAnyway = (Button) dialog.findViewById(R.id.btnSendNow);
		if (!StringUtils.isEmptyString(action)) {
			btnStop.setText("Close");
			btnSendAnyway.setText(action + " Anyway");
		}

		btnStop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				mIdeResumeFromHome.stop();
			}
		});
		btnSendAnyway.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				mIdeResumeFromHome.sendNow();

			}
		});
		dialog.show();
	}

	public static void showDialogWhenGetLocation(Context context,
			final IAddLocation mIAddLocation) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_when_get_location);
		TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
		tvTitle.setText(context.getResources().getString(
				R.string.tweet_location));
		TextView tvNotify = (TextView) dialog.findViewById(R.id.tvNotice);
		tvNotify.setText(context.getResources().getString(
				R.string.notify_when_get_location));
		Button btnCancel = (Button) dialog.findViewById(R.id.btnStop);
		Button btnOk = (Button) dialog.findViewById(R.id.btnSendNow);
		btnCancel.setText(context.getResources().getString(R.string.cancel));
		btnOk.setText(context.getResources().getString(android.R.string.ok));
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				mIAddLocation.addLocation();

			}
		});
		dialog.show();
	}
}
