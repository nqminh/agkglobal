package com.example.besafepost.utils;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;

public class GpsHelper implements LocationListener {
	private static final String tag = "LocationHelper";
	private String bestProvider;
	private LocationManager locationManager;

	private OnLocationChangeListener listener;

	private Location location;
	private String gpsProvider;

	static long MINTIME = 3000;
	static float MINDISTANCE = 0f;
	static float SPEED_UNIT = 1f;

	static public interface OnLocationChangeListener {
		public void onLocationChanged(Location location);
	}

	public void setOnLocationChangeListener(OnLocationChangeListener listener) {
		this.listener = listener;
	}

	public Location getLocation() {
		return location;
	}

	public GpsHelper(Context context) {
		locationManager = (LocationManager) context
				.getSystemService(Context.LOCATION_SERVICE);
		findBestProvider();
		locationManager.requestLocationUpdates(bestProvider, MINTIME,
				MINDISTANCE, this);

	}

	public void findBestProvider() {
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		bestProvider = locationManager.getBestProvider(criteria, false);
		location = locationManager.getLastKnownLocation(bestProvider);
		if (location == null) {
			bestProvider = LocationManager.GPS_PROVIDER;
			location = locationManager.getLastKnownLocation(bestProvider);
		}
		if (location == null) {
			location = locationManager
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}
		Log.e("Lilo", "find best provider:" + bestProvider);
		printProvider(bestProvider);

		// if(bestProvider == null)
		{
			gpsProvider = LocationManager.GPS_PROVIDER;
			bestProvider = gpsProvider;
		}
	}

	@Override
	public void onLocationChanged(Location location) {

		if (location == null)
			return;

		printLocation(location);

		this.location = location;

		if (listener != null) {
			listener.onLocationChanged(location);
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.e(tag, "onProviderDisabled:  " + provider);
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.e(tag, "onProviderEnabled:  " + provider);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.e(tag, "onStatusChanged:  " + provider + "," + status);
	}

	private void printLocation(Location location) {
		if (location == null) {
			Log.e(tag, "Location[unknown]");
		}
	}

	private void printProvider(String provider) {
		LocationProvider info = locationManager.getProvider(provider);
		StringBuilder builder = new StringBuilder();
		builder.append("LocationProvider[").append("name=")
				.append(info.getName()).append(",enabled=")
				.append(locationManager.isProviderEnabled(provider))
				.append(",getAccuracy=").append(info.getAccuracy())
				.append(",getPowerRequirement=")
				.append(info.getPowerRequirement()).append(",hasMonetaryCost=")
				.append(info.hasMonetaryCost()).append(",requiresCell=")
				.append(info.requiresCell()).append(",requiresNetwork=")
				.append(info.requiresNetwork()).append(",requiresSatellite=")
				.append(info.requiresSatellite()).append(",supportsAltitude=")
				.append(info.supportsAltitude()).append(",supportsBearing=")
				.append(info.supportsBearing()).append(",supportsSpeed=")
				.append(info.supportsSpeed()).append("]");
		Log.e(tag, builder.toString());
	}
}
