package com.example.besafepost.adapter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import twitter4j.Status;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.besafepost.activity.R;

@SuppressLint("DefaultLocale")
public class HomeTwitterAdapter extends ArrayAdapter<Status> {
	private final Context context;
	private List<Status> statuses;
	private int oldPosition;

	static class ViewHolder {
		public TextView tvUserName;
		public WebView wvText;
		public TextView tvTime;
		public ImageView imgProfile;
	}

	public HomeTwitterAdapter(Context context, int resource,
			List<Status> statuses) {
		super(context, resource, statuses);
		this.statuses = statuses;
		this.context = context;
	}

	@Override
	public long getItemId(int position) {

		return statuses.size();
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		ViewHolder viewHolder;
		if (rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(
					R.layout.item_view_home_timeline_twitter, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.wvText = (WebView) rowView.findViewById(R.id.wvText);
			viewHolder.tvUserName = (TextView) rowView
					.findViewById(R.id.tvUserName);
			viewHolder.tvTime = (TextView) rowView.findViewById(R.id.tvTime);
			viewHolder.imgProfile = (ImageView) rowView
					.findViewById(R.id.img_profile);
			rowView.setTag(viewHolder);
		} else {

			viewHolder = (ViewHolder) rowView.getTag();
		}

		String userName = statuses.get(position).getUser().getName();
		viewHolder.tvUserName.setText(userName);
		String urlImg = statuses.get(position).getUser()
				.getProfileImageURLHttps();
		
		String pre = getContext().getString(R.string.TwitterCSSStringPre);
		String post = getContext().getString(R.string.TwitterCSSStringPost);
		
		Status status = statuses.get(position);
		
/////////////////////////////////////////////////////////Set WebView content
		String tweet = status.getText();
		String orig = status.getText();
		int pos = -1;
		pos = tweet.toLowerCase().indexOf("http://");
		if (pos < 0)
			pos = tweet.toLowerCase().indexOf("https://");
		if (pos > 0) {
			int pos2 = tweet.toLowerCase().indexOf(". ", pos + 1);
			if (pos2 < 0)
				pos2 = tweet.toLowerCase().indexOf(" ", pos + 1);
			if (pos2 < 0)
				pos2 = tweet.toLowerCase().indexOf("	", pos + 1);
			if (pos2 < 0)
				pos2 = tweet.length();
			String href = tweet.substring(pos, pos2);
			boolean lastHREF = false;
			if (pos2 == tweet.length())
				lastHREF = true;
			tweet = orig.substring(0, pos) + "<a href=\"" + href + "\">" + href
					+ "</a>";
			if (!lastHREF)
				tweet = tweet.trim() + orig.substring(pos2);
		}

//		viewHolder.wvText.loadData(pre + tweet + post, "text/html", "UTF-8");
		viewHolder.wvText.loadDataWithBaseURL(null, pre + tweet + post, "text/html", "UTF-8", null);
		viewHolder.wvText.getSettings().setJavaScriptEnabled(true);
		viewHolder.wvText.setTag(Integer.toString(position));
		
		viewHolder.wvText.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView wv, String url) {
				Log.i("shouldOverrideUrlLoading", "shouldOverrideUrlLoading:"
						+ url);
				String tg = (String) wv.getTag();
				Log.i("WebViewClient got tag:", "WebViewClient got tag:" + tg);
				int position = -1;
				try {
					position = Integer.parseInt(tg);
					Log.i("WebViewClient got position:",
							"WebViewClient got position:" + position);
				} catch (Exception e) {
					// DO NOTHING //
					Log.i("WebViewClient lost position",
							"WebViewClient lost position");
				}
				if (url.toLowerCase().startsWith("http")
						&& !url.toLowerCase().equals("http://null/")) {
					Log.i("WebViewClient opening externally:",
							"WebViewClient opening externally:" + url);
					oldPosition = position;
					Log.i("WebViewClient o-e-1", "WebViewClient o-e-1");
					Uri uri = Uri.parse(url);
					Log.i("WebViewClient o-e-2", "WebViewClient o-e-2");
					Intent intent = new Intent(Intent.ACTION_VIEW, uri);
					Log.i("WebViewClient o-e-3", "WebViewClient o-e-3");
					getContext().startActivity(intent);
					Log.i("WebViewClient o-e-4", "WebViewClient o-e-4");
				} else {
					Log.i("WebViewClient else...", "WebViewClient else...");
					if (position != oldPosition) {
						Log.i("WebViewClient opening internally:",
								"WebViewClient opening internally:" + url);
						Log.i("WebViewClient tag=", "WebViewClient tag=" + tg);
						Status status = statuses.get(position);
						Log.i("WebViewClient position=",
								"WebViewClient position=" + position);
						Log.i("WebViewClient status=", "WebViewClient status="
								+ status.getText());
						// onLongListItemClick(null,position,status.getId());
					} else {
						Log.i("WebViewClient blocked from opening internally:",
								"WebViewClient blocked from opening internally:"
										+ url);
					}
					oldPosition = 0;
				}
				Log.i("//shouldOverrideUrlLoading",
						"//shouldOverrideUrlLoading");
				return true;
			}
		});
/////////////////////////////////////////////////////////End	
		
/////////////////////////////////////////////////////////Set time
		Long now = (new Date()).getTime();
		Long then = status.getCreatedAt().getTime();

		if (now < then) {
			viewHolder.tvTime.setText("Just now");
		} else {
			viewHolder.tvTime.setText("past");
			Long diff = (now - then)/1000;
			if (diff < 3600) {
				viewHolder.tvTime.setText(Long.toString(diff/60)+" min");
			} else if (diff < 24*3600) {
				long hours = diff/3600;
				viewHolder.tvTime.setText(Long.toString(hours)+" h");
			} else {
				int day = status.getCreatedAt().getDay();
				int mon = status.getCreatedAt().getMonth();
				String month = "Jan";
				switch (mon) {
				case 1: month = "Jan";
				break;
				case 2: month = "Feb";
				break;
				case 3: month = "Mar";
				break;
				case 4: month = "Apr";
				break;
				case 5: month = "May";
				break;
				case 6: month = "Jun";
				break;
				case 7: month = "Jul";
				break;
				case 8: month = "Aug";
				break;
				case 9: month = "Sep";
				break;
				case 10: month = "Oct";
				break;
				case 11: month = "Nov";
				break;
				case 12: month = "Dec";
				break;
				}
				viewHolder.tvTime.setText(Long.toString(day)+" "+month);
			}
		}
/////////////////////////////////////////////////////////End
		URL url;
		try {

			url = new URL(urlImg);
			Bitmap image = BitmapFactory.decodeStream(url.openConnection()
					.getInputStream());
			viewHolder.imgProfile.setImageBitmap(image);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rowView;
	}

}
