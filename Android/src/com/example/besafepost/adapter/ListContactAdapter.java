package com.example.besafepost.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.example.besafepost.activity.R;
import com.example.besafepost.model.Contact;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class ListContactAdapter extends ArrayAdapter<Contact> implements Filterable {
	Context mContext;
	List<Contact> mData;
	List<Contact> result;
	int mResource;

	public ListContactAdapter(Context context, int resource, List<Contact> objects) {
		super(context, resource, objects);
		mContext = context;
		mData = new ArrayList<Contact>(objects);
		result = new ArrayList<Contact>(objects);
		mResource = resource;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(mResource, null);
		}
		Contact contact = getItem(position);
		if (contact != null) {
			String name = contact.getNameContact();
			if (name == null)
				name = "";
			List<String> listPhone = contact.getPhoneNumber();
			String phone = "";
			if (listPhone != null && listPhone.size() > 0) {
				phone = listPhone.get(0);
			}
			((TextView) convertView.findViewById(R.id.numberPhone)).setText(name + "\n" + phone);
		}
		return convertView;
	}

	@Override
	public int getCount() {
		return result.size();
	}

	@Override
	public Contact getItem(int position) {
		if (result.size() > position)
			return result.get(position);
		else
			return null;
	}

	@Override
	public Filter getFilter() {
		Filter myFilter = new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				Log.d("besafe", "filter : " + constraint);
				result = new ArrayList<Contact>();
				FilterResults filterResults = new FilterResults();
				if (!TextUtils.isEmpty(constraint)) {
					String s = constraint.toString().toLowerCase(Locale.getDefault());
					for (int i = 0; i < mData.size(); i++) {
						Contact contact = mData.get(i);
						if (contact.getNameAndAddr().toLowerCase(Locale.getDefault()).contains(s)) {
							result.add(mData.get(i));
						}
					}
					Log.d("besafe", "result : " + result.size());
				}
				filterResults.values = result;
				filterResults.count = result.size();
				return filterResults;
			}

			@Override
			protected void publishResults(CharSequence contraint, FilterResults results) {
				if (results != null && results.count > 0) {
					notifyDataSetChanged();
				} else {
					notifyDataSetInvalidated();
				}
			}
		};
		return myFilter;
	}

}
