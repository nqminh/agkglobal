package com.example.besafepost.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.besafepost.activity.R;

public class ListTemplateSMSAdapter extends ArrayAdapter<String> {

	private List<String> listTemplateSMS = new ArrayList<String>();
	private Context context;

	public ListTemplateSMSAdapter(Context context, int resource,
			List<String> listTemplateSMS) {
		super(context, resource);
		this.listTemplateSMS = listTemplateSMS;
		this.context = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listTemplateSMS.size();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.templatesms_item, null);
		}

		TextView nameContact = (TextView) convertView
				.findViewById(R.id.nameContact);

		nameContact.setText(listTemplateSMS.get(position));

		return convertView;
	}
}
