package com.example.besafepost.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.besafepost.activity.R;
import com.example.besafepost.model.Sms;

public class MessageDetailAdapter extends ArrayAdapter<Sms>{
	Context context;
	public MessageDetailAdapter(Context context, int resource, List<Sms> objects) {
		super(context, resource, objects);
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		MyViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.sms_detail, null);
			
			viewHolder = new MyViewHolder();
			viewHolder.main = convertView.findViewById(R.id.main_sms_detail);
			viewHolder.detail = (TextView) convertView.findViewById(R.id.id);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (MyViewHolder) convertView.getTag();
		}
		Sms sms = getItem(position);
		if ((sms.getType() == 1)) {
			viewHolder.main.setBackgroundColor(context.getResources().getColor(R.color.grey_bg));
		} else {
			viewHolder.main.setBackgroundColor(context.getResources().getColor(R.color.WHITE));
		}
		viewHolder.detail.setText(((sms.getType() == 1)? sms.getAddress() : "me") + " :	 " + sms.getMsg());
		return convertView;
	}
	
	static class MyViewHolder{
		public View main;
		public TextView detail;
	}
}
