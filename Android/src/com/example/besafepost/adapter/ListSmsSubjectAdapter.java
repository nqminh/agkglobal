package com.example.besafepost.adapter;

import java.util.List;

import com.example.besafepost.activity.R;
import com.example.besafepost.model.Sms;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListSmsSubjectAdapter extends ArrayAdapter<Sms>{
	Context context;
	public ListSmsSubjectAdapter(Context context, int resource, List<Sms> objects) {
		super(context, resource, objects);
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.sms_subject, null);
		}
		Sms sms = getItem(position);
		if (position % 2 == 0) convertView.findViewById(R.id.main_sms_subject).setBackgroundColor(context.getResources().getColor(R.color.grey_bg));
		else convertView.findViewById(R.id.main_sms_subject).setBackgroundColor(context.getResources().getColor(R.color.transparent_white));
		((TextView) convertView.findViewById(R.id.id)).setText(sms.getAddress() + " :	 " + sms.getMsg());
		return convertView;
	}
}
