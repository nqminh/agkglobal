package com.example.besafepost.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.besafepost.activity.R;
import com.example.besafepost.model.ItemPlace;
import com.example.besafepost.task.DownloadImageTask;

public class PlaceAdapter extends ArrayAdapter<ItemPlace> {

	private int LayoutResourceId;
	private ArrayList<ItemPlace> rows;
	LayoutInflater inflater;

	public PlaceAdapter(Context context, int LayoutResourceId,
			ArrayList<ItemPlace> rows) {
		super(context, LayoutResourceId, rows);
		// TODO Auto-generated constructor stub
		this.LayoutResourceId = LayoutResourceId;
		this.rows = rows;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row;
		row = inflater.inflate(LayoutResourceId, parent, false);
		/*
		if (convertView != null) {
			row = convertView;
		} else {
			row = inflater.inflate(LayoutResourceId, parent, false);
		}
		*/
		ImageView iconImg = (ImageView) row.findViewById(R.id.icon);
		TextView tvName = (TextView) row.findViewById(R.id.name);
		TextView tvSubText = (TextView) row.findViewById(R.id.subText);

		ItemPlace data = rows.get(position);
		tvName.setText(data.getName());
		tvSubText.setText(data.getSubText());

		// show The Image
		new DownloadImageTask(iconImg).execute(data.getIconLink());

		return row;
	}

}