package com.example.besafepost.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.besafepost.activity.R;
import com.example.besafepost.model.ItemForListAccount;

public class ListAccountAdapter extends ArrayAdapter<ItemForListAccount> {
	private int LayoutResourceId;
	private List<ItemForListAccount> rows;
	LayoutInflater inflater;
	Context context;

	public ListAccountAdapter(Context context, int LayoutResourceId,
			List<ItemForListAccount> rows) {
		super(context, LayoutResourceId, rows);
		this.rows = rows;
		this.LayoutResourceId = LayoutResourceId;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row;
		row = inflater.inflate(LayoutResourceId, parent, false);

		ImageView iconAccount = (ImageView) row.findViewById(R.id.iconAccount);
		TextView tvAccountName = (TextView) row.findViewById(R.id.accountName);
		TextView tvAccountType = (TextView) row.findViewById(R.id.accountType);

		ItemForListAccount data = rows.get(position);
		tvAccountName.setText(data.getAccountName());
		tvAccountName.setTextColor(context.getResources().getColor(
				data.getColorAccountNameFromSrc()));
		tvAccountType.setText(data.getAccountType());
		iconAccount.setBackgroundResource(data.getIconFromSrc());
		return row;
	}
}
