package com.example.besafepost.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.besafepost.activity.R;
import com.example.besafepost.interfaces.IDeItemFriendList;
import com.example.besafepost.model.EntryItemFriendList;
import com.example.besafepost.model.SectionItemFriendList;

public class EntryAdapter extends ArrayAdapter<IDeItemFriendList> {
	private Context context;
	private ArrayList<IDeItemFriendList> itemFriendList;
	private LayoutInflater vi;
	private int indexSelected = -1;

	public EntryAdapter(Context context, int resource,
			ArrayList<IDeItemFriendList> itemFriendList) {
		super(context, resource, itemFriendList);
		this.context = context;
		this.itemFriendList = itemFriendList;
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setIndexSelected(int indexSelected) {
		this.indexSelected = indexSelected;
	}

	public int getIndexSelected() {
		return indexSelected;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;

		final IDeItemFriendList i = itemFriendList.get(position);
		if (i != null) {
			if (i.isSection()) {
				SectionItemFriendList si = (SectionItemFriendList) i;
				v = vi.inflate(R.layout.list_item_section, null);

				v.setOnClickListener(null);
				v.setOnLongClickListener(null);
				v.setLongClickable(false);

				final TextView sectionView = (TextView) v
						.findViewById(R.id.tvSection);
				sectionView.setText(si.getTitle());

			} else {
				EntryItemFriendList ei = (EntryItemFriendList) i;
				v = vi.inflate(R.layout.list_item_entry, null);
				final TextView title = (TextView) v.findViewById(R.id.tvEntry);
				final ImageView imgEntry = (ImageView) v
						.findViewById(R.id.imgEntry);
				final ImageView imgClick = (ImageView) v
						.findViewById(R.id.imgClick);
				imgClick.setImageDrawable(null);
				if (indexSelected == position) {
					imgClick.setImageDrawable(context.getResources()
							.getDrawable(R.drawable.img_click));
				}

				if (title != null)
					title.setText(ei.title);
				if (imgEntry != null)
					imgEntry.setImageResource(ei.imgSrc);
			}
		}
		return v;
	}
}
