//
//  ControlObj.m
//  AGKGlobal
//
//  Created by Thuy on 9/21/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "ControlObj.h"

static ControlObj* _wkControl = nil;
@implementation ControlObj


- (id)init
{
    self = [super init];
    delegates = (MulticastDelegate<ControlObjDelegate>*) [[MulticastDelegate alloc] init];
    return self;
}

+ (void)initWithDelegate:(id<ControlObjDelegate>) delegate
{
    [[ControlObj shared] addDelegate:delegate];
}

- (void)addDelegate:(id)delegate
{
    [delegates addDelegate:delegate];
}

+ (void)addDelegate:(id)delegate
{
    [[ControlObj shared] addDelegate:delegate];
}

+ (void)removeDelegate:(id)delegate
{
    [[ControlObj shared] removeDelegate:delegate];
}

- (void)removeDelegate:(id)delegate
{
    [delegates removeDelegate:delegate];
}

+ (ControlObj*)shared
{
    if (!_wkControl) {
        static dispatch_once_t threeToken;
        dispatch_once(&threeToken, ^{
            _wkControl = [[ControlObj alloc] init];
            _wkControl.listPlace = [[NSMutableArray alloc] init];
            _wkControl.listUser = [[NSMutableArray alloc] init];
            _wkControl.listNewStory = [[NSMutableArray alloc] init];
            _wkControl.listSeeMore = [[NSMutableArray alloc] init];
            _wkControl.count = 0;
        });
    }
    return _wkControl;
}

+ (void)addUser:(NSString*)user
{
    [[ControlObj shared] addUser:user];
}

+ (void)addPlace:(NSString*)place
{
    [[ControlObj shared] addPlace:place];
}

- (void)addPlace:(NSString*)place
{
    if ([self.listPlace containsObject:place]) {
        NSLog(@"listPlace did contain : %@",place);
    }
    else
    {
        [self.listPlace addObject:place];
    }
}

+ (NSString*)getStringListPlace
{
    if ([ ControlObj shared].listPlace.count == 0) {
        return @"";
    }
    
    NSString* list = [[ControlObj shared].listPlace componentsJoinedByString:@","];
    [[ControlObj shared].listPlace removeAllObjects];
    return list;
}

+ (NSString*)getStringListUser
{
    if ([ ControlObj shared].listUser.count == 0) {
        return @"";
    }
    NSString* list = [[ControlObj shared].listUser componentsJoinedByString:@","];
    [[ControlObj shared].listUser removeAllObjects];
    return list;
}

+ (void)analysisData:(NSArray*) result withType:(NSInteger)typepost
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [[ControlObj shared] analysisData:result withType:typepost];
    });
}

+ (void)resetCount
{
    [[ControlObj shared] resetCount];
    [[[ControlObj shared]  getAllPostDict] removeAllObjects];
    [[[ControlObj shared]  getAllPostDictHref]removeAllObjects];
}

- (void)resetCount
{
    self.count = 0;
    
    [self.postDict removeAllObjects];
    [self.postDictHref removeAllObjects];
}

- (void)addUser:(NSString*)user
{
    if ([self.listUser containsObject:user]) {
        NSLog(@"listUser did contain : %@",user);
    }
    else
    {
        [self.listUser addObject:user];
    }
}

-(NSMutableDictionary*)getAllPostDict
{
    if (self.postDict == nil) {
        self.postDict = [[NSMutableDictionary alloc] init];
    }
    return self.postDict;
}

-(NSMutableDictionary*)getAllUserDict
{
    if (self.userDict == nil) {
        self.userDict = [[NSMutableDictionary alloc] init];
    }
    return self.userDict;
}

+ (PostObj*)getPostWithKey:(NSString*)key
{
    return [[ControlObj shared] getPostWithKey:key];
}

- (PostObj*)getPostWithKey:(NSString*)key
{
    return [self.postDict objectForKey:key];
}

+ (User*)getUserWithID:(NSString*)keyID
{
    return [[ControlObj shared]  getUserWithID:keyID];
}

+ (NSString*)getNameWithID:(NSString*)keyID
{
    return [[ControlObj shared]  getNameWithID:keyID];
}

- (NSString*)getNameWithID:(NSString*)keyID
{
    User *user = [[self getAllUserDict] objectForKey:[NSString stringWithFormat:@"%@",keyID]];
    return user.name;
}

+ (NSString*)getPicWithID:(NSString*)keyID
{
    return [[ControlObj shared]  getPicWithID:keyID];
}

- (NSString*)getPicWithID:(NSString*)keyID
{
    User *user = [[self getAllUserDict] objectForKey:[NSString stringWithFormat:@"%@",keyID]];
    return user.pic;
}

- (User*)getUserWithID:(NSString*)keyID
{
    return [[self getAllUserDict] objectForKey:[NSString stringWithFormat:@"%@",keyID]];
}

- (void)analysisData:(NSArray*) result withType:(NSInteger)typepost
{
    _typePost = typepost;
    [self.listNewStory removeAllObjects];
    [self.listSeeMore removeAllObjects];
    if (result.count == 0) {
        return;
    }
    
    [self getPost:result];
    
    if (_typePost == seemore) {
        [self checkNewStories:result];
    }
}

- (void) checkNewStories:(NSArray*)result
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        if (_typePost == newsfeedr) {
            return;
        }
        for (NSInteger i = 0; i < [result count] - 10; i ++) {
            
            NSInteger pt = [[[result objectAtIndex:i] objectForKey:@"type"] integerValue];
            
            if (pt == typePost_event ||
                pt == Likes_a_Link1  ||
                pt == 376 ||
                pt == typePost_Note_created ||
                pt == 424 ||
                pt == 361 ||
                pt == typePost_Likes ||
                pt == 8) {
                continue;
            }
            
            if (pt == typePost_Likes_a_photo_or_Link ||
                pt == typePost_Comment_created ) {
                
                int idxsub = i + 1;
                if (idxsub < [result count]) {
                    i++;
                    //set and check
                    if ([[self getAllPostDict] objectForKey:[[result objectAtIndex:i] objectForKey:@"post_id"]] != nil) {
                        // new stories
                        NSLog(@"new stories");
                        if ([delegates respondsToSelector:@selector(ControlObj:newStories:)]) {
                            [delegates ControlObj:self newStories:@"new stories"];
                        }
                        break;
                    }
                    else {
                        break;
                    }
                    
                }
            }
            else {
                //set and check
                if ([[self getAllPostDict] objectForKey:[[result objectAtIndex:i] objectForKey:@"post_id"]] != nil) {
                    // new stories
                    NSLog(@"new stories");
                    if ([delegates respondsToSelector:@selector(ControlObj:newStories:)]) {
                        [delegates ControlObj:self newStories:@"new stories"];
                    }
                    break;
                }
                else {
                    break;
                }
            }
        }
    });
}

- (void)getPost:(NSArray*) result
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSInteger index;
        if (_typePost == seemore) {
            index = self.count;
        }
        else
        {
            index = 0;
        }
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        for (NSInteger i = index; i < [result count]; i ++) {
            
            NSInteger pt = [[[result objectAtIndex:i] objectForKey:@"type"] integerValue];
            
            if (pt == typePost_event ||
                pt == Likes_a_Link1  ||
                pt == 376 ||
                pt == typePost_Note_created ||
                pt == 424 ||
                pt == 361 ||
                pt == typePost_Likes ||
                pt == 8) {
                continue;
            }
            
            if (pt == typePost_Likes_a_photo_or_Link ||
                pt == typePost_Comment_created ) {
                
                int idxsub = i + 1;
                if (idxsub < [result count]) {
                    i++;
                    //set and check
                    if ([[self getAllPostDict] objectForKey:[[result objectAtIndex:i] objectForKey:@"post_id"]] != nil) {
                        continue;
                    }
                    
                    PostObj *post = [[PostObj alloc] initWithDictinary:[result objectAtIndex:i] subPost:[result objectAtIndex:idxsub]];
                    [[self getAllPostDict] setObject:post forKey:post.postID];
                    if ([post.attach_href length] > 0)
                    {
                        [[self getAllPostDictHref] setObject:post forKey:post.attach_href];
                    }
                    
                    [arr addObject:post];
                    
                    if (_typePost == seemore) {
                        if ([delegates respondsToSelector:@selector(ControlObj:seemore:)]) {
                            [delegates ControlObj:self seemore:post];
                        }
                    }
                    else
                    {
                        if ([delegates respondsToSelector:@selector(ControlObj:newsfeeds:)]) {
                            [delegates ControlObj:self newsfeeds:post];
                        }
                    }
                }
            }
            else {
                //set and check
                if ([[self getAllPostDict] objectForKey:[[result objectAtIndex:i] objectForKey:@"post_id"]] != nil) {
                    continue;
                }
                
                PostObj *post = [[PostObj alloc] initWithDictinary:[result objectAtIndex:i]];
                [[self getAllPostDict] setObject:post forKey:post.postID];
                if ([post.attach_href length] > 0)
                {
                    [[self getAllPostDictHref] setObject:post forKey:post.attach_href];
                }
                
                [arr addObject:post];
                
                if (_typePost == seemore) {
                    if ([delegates respondsToSelector:@selector(ControlObj:seemore:)]) {
                        [delegates ControlObj:self seemore:post];
                    }
                }
                else
                {
                    if ([delegates respondsToSelector:@selector(ControlObj:newsfeeds:)]) {
                        [delegates ControlObj:self newsfeeds:post];
                    }
                }
            }
        }
        if ([delegates respondsToSelector:@selector(ControlObj:finishedSeemore:)]) {
            self.count = [result count];
            [delegates ControlObj:self finishedSeemore:arr];
        }
    });
}

+ (void) processFriendList:(NSArray*) tmpary
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [[ControlObj shared] processFriendList:tmpary];
    });
}

- (void) processFriendList:(NSArray*) tmpary
{
    for (NSDictionary *dict in tmpary) {
        User *user = [[User alloc] initWithDictinary:dict];
        [[self getAllUserDict] setObject:user forKey:[NSString stringWithFormat:@"%@",user.userID]];
        if ([delegates respondsToSelector:@selector(ControlObj:jsforFacebookUser:)]) {
            [delegates ControlObj:self jsforFacebookUser:user.jsname];
            [delegates ControlObj:self jsforFacebookUser:user.jspic];
        }
    }
}

#pragma mark - GET POST WITH KEY LINK URL  

-(NSMutableDictionary*)getAllPostDictHref
{
    if (self.postDictHref == nil) {
        self.postDictHref = [[NSMutableDictionary alloc] init];
    }
    return self.postDictHref;
}

//--get PostObj for key format link url (youtube,mp3.zing,...)
+ (PostObj*)getPostHrefWithKey:(NSString*)key
{
    return [[ControlObj shared] getPostHrefWithKey:key];
}

- (PostObj*)getPostHrefWithKey:(NSString *)key
{
    return [self.postDictHref objectForKey:key];
}


@end
