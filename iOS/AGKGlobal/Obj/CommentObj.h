//
//  CommentObj.h
//  AGKGlobal
//
//  Created by Thuy on 9/23/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Obj.h"
#import "ControlObj.h"

@interface CommentObj : NSObject
/*SELECT text, post_id, user_likes, time, text_tags, likes, id, fromid FROM comment  WHERE id ='%@'",CommentID];*/
@property(nonatomic,retain) NSString*   text;
@property(nonatomic,retain) NSString*   time;
@property(nonatomic,retain) NSString*   comment_id;
@property(nonatomic,retain) NSString*   fromid;
@property(nonatomic,retain) NSString*   countLike;
@property(nonatomic,retain) NSString*   link;
@property(nonatomic,retain) NSString*   likeText;
@property(nonatomic,retain) NSString*   strHTML;
@property(nonatomic,assign) BOOL        isFacebookUser;

- (id)initWithDictinary:(NSDictionary*)commentDict;
- (BOOL)isFailName;
- (void)refreshName:(NSString*)name pic:(NSString*)pic;
- (void)liked;
- (void)unliked;

@end
