//
//  BesafeUser.h
//  AGKGlobal
//
//  Created by Dung on 2/26/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BesafeUser : NSObject

@property (nonatomic,strong) NSString *bs_age;
@property (nonatomic,strong) NSString *bs_birthday;
@property (nonatomic,strong) NSString *bs_email;
@property (nonatomic,strong) NSString *bs_facebook_id;
@property (nonatomic,strong) NSString *bs_full_name;
@property (nonatomic,strong) NSString *bs_userid;
@property (nonatomic,strong) NSString *bs_password;
@property (nonatomic,strong) NSString *bs_phone;
@property (nonatomic,strong) NSString *bs_sex;
@property (nonatomic,strong) NSString *bs_twitter_id;
@property (nonatomic,strong) NSString *bs_zip_code;

-(id) initWithDictionary:(NSDictionary*)dict;
@end
