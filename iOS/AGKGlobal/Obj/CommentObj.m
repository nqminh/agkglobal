//
//  CommentObj.m
//  AGKGlobal
//
//  Created by Thuy on 9/23/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "CommentObj.h"

@implementation CommentObj

- (id)initWithDictinary:(NSDictionary*)commentDict
{
    if (self = [super init]) {
        NSNumber *post_time = [NSNumber numberWithDouble:0.0];
        @try {
            post_time = [commentDict objectForKey:@"time"];
        }
        @catch (NSException *exception) {
            post_time = [NSNumber numberWithDouble:0.0];
        }
        @finally {
        }
        self.time = @"";
        self.time = [Utility convertDateMessageFromUnixTime:[post_time doubleValue]];
        if (self.time ==  nil) {
            self.time = @"Recently Posted";
        }
        self.countLike = @"";
        if ([[commentDict objectForKey:@"likes"] integerValue] != 0) {
            self.countLike = [commentDict objectForKey:@"likes"];
        }
        
        if ([[commentDict objectForKey:@"user_likes"] integerValue] == 0) {
            self.link = @"http://null/like1/";
            self.likeText = @"Like";
        }
        else {
            self.link = @"http://null/unlike1/";
            self.likeText = @"Unlike";
        }
        self.text = [commentDict objectForKey:@"text"];
        self.comment_id = [commentDict objectForKey:@"id"];
        self.fromid = [commentDict objectForKey:@"fromid"];
        [self getHTML];
    }
    return self;
}

- (void)getHTML
{
    self.strHTML = @"";
    User *user = [ControlObj getUserWithID:self.fromid];
    
    if (user == nil) {
        DLOG(@"%@",self.fromid);
        self.isFacebookUser = YES;
         self.strHTML = [self.strHTML stringByAppendingFormat:[Template templateHTMLCommentOther],
                        self.comment_id,
                        self.fromid,
                        @"url",
                        self.fromid,
                        @"Facebook User",
                        self.text,
                        self.link,
                        self.comment_id,
                        self.comment_id,
                         self.likeText,
                         self.countLike,
                         self.time];
        
        [ControlObj addUser:self.fromid];
    }
    else {
         self.isFacebookUser = NO;
        self.strHTML = [self.strHTML stringByAppendingFormat:[Template templateHTMLCommentOther],
                        self.comment_id,
                        self.fromid,
                        user.pic,
                        self.fromid,
                        user.name,
                        self.text,
                        self.link,
                        self.comment_id,
                        self.comment_id,
                        self.likeText,
                        self.countLike,
                        self.time];
    }
    NSLog(@"%@",self.strHTML);
}

- (BOOL)isFailName
{
    if (self.isFacebookUser) {
        [self getHTML];
    }
    return self.isFacebookUser;
}


- (void)refreshName:(NSString*)name pic:(NSString*)pic
{
    //
}

- (void)liked
{
    NSInteger countlike = [self.countLike integerValue];
    countlike += 1;
    self.countLike = [NSString stringWithFormat:@"%d",countlike];
    self.likeText = @"Unlike";
    self.link = @"http://null/unlike1/";
    [self getHTML];
}

- (void)unliked
{
    NSInteger countlike = [self.countLike integerValue];
    countlike -= 1;
    self.countLike = [NSString stringWithFormat:@"%d",countlike];
    if (countlike <= 0) {
        countlike = 0;
        self.countLike = @"";
    }    
    self.likeText = @"Like";
     self.link = @"http://null/like1/";
    [self getHTML];
}


@end
