//
//  BesafeUser.m
//  AGKGlobal
//
//  Created by Dung on 2/26/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import "BesafeUser.h"

@implementation BesafeUser

-(id) initWithDictionary:(NSDictionary*)dict
{
    if (self == [super init])
    {
        self.bs_age         = [dict objectForKey:@"age"];
        self.bs_birthday    = [dict objectForKey:@"birthday"];
        self.bs_email       = [dict objectForKey:@"email"];
        self.bs_facebook_id = [dict objectForKey:@"facebook_id"];
        self.bs_full_name   = [dict objectForKey:@"full_name"];
        self.bs_userid      = [dict objectForKey:@"id"];
        self.bs_password    = [dict objectForKey:@"password"];
        self.bs_phone       = [dict objectForKey:@"phone"];
        self.bs_sex         = [dict objectForKey:@"sex"];
        self.bs_twitter_id  = [dict objectForKey:@"twitter_id"];
        self.bs_zip_code    = [dict objectForKey:@"zip_code"];
    }
    return self;
}

@end
