//
//  User.m
//  AGKGlobal
//
//  Created by Thuy on 9/25/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "User.h"

@implementation User

- (id)initWithDictinary:(NSDictionary*)dict
{
    if (self = [super init]) {
        self.userID = [dict objectForKey:@"id"];
        self.userUID = [dict objectForKey:@"uid"];
        self.name = [dict objectForKey:@"name"];
        
         if (self.userID == nil || dict == nil) {
             self.userID = @"xxx";
             self.name = @"Facebook User";
             self.jsname = @"";
             self.pic = @"https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-frc1/283270_257757160902107_690367_n.jpg";
             self.jspic = @"";
         }
         else {
             if (self.name == nil)
             {
                 self.name = @"Facebook User";
                 self.jsname = @"";
             }
             else {
                 self.jsname =[NSString stringWithFormat:@"$('div.name%@').html('%@');",self.userID,self.name];
             }
             
             self.pic = [dict objectForKey:@"pic"];
             if (self.pic ==  nil || self.pic.length < 5)
             {
                 self.pic = @"https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-frc1/283270_257757160902107_690367_n.jpg";
                 self.jspic = @"";
             }
             else
             {
                 self.jspic =[NSString stringWithFormat:@"changeAvatar('image%@', '%@');",self.userID,self.pic];
             }
             
             [[Utility shareProfileDict] setObject:self forKey:self.userID];
         }
    }
    return self;
}

@end
