//
//  Template.m
//  AGKGlobal
//
//  Created by Thuy on 9/21/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "Template.h"

static Template* _wkTemplate = nil;
@implementation Template

+ (Template*)shared
{
    if (!_wkTemplate) {
        static dispatch_once_t twoToken;
        dispatch_once(&twoToken, ^{
            _wkTemplate = [[Template alloc] init];
        });
    }
    return _wkTemplate;
}

- (void) start
{
    [self templateArticleBeginNormal];
    [self templateArticleBeginSharedLink];
    [self templateArticleBeginTagged];
    [self templateArticleBodyCheckin];
    [self templateArticleBodyPhoto ];
    [self templateArticleBodyStatus];
    [self templateArticleBodyVideo];
    [self templateArticleEnd];
    [self templateArticleEnd1];
    [self templateArticleEnd2];
    [self templateArticleForUndo ];
    [self templateHeaderBegin];
    [self templateHTMLBegin];
    [self templateHTMLCommentOther];
    [self templateHTMLEnd];
    [self templateArticleTweet];
    [self geticonRetweet];
    [self tempTweetHTMLBegin];
}

#pragma mark -

- (NSString*)templateHTMLBegin
{
    if (nil == self._templateHTMLBegin) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateHTMLBegin" ofType:@"txt"];
        self._templateHTMLBegin = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateHTMLBegin;
}

- (NSString*)templateHTMLEnd
{
    if (nil == self._templateHTMLEnd) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateHTMLEnd" ofType:@"txt"];
        self._templateHTMLEnd = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateHTMLEnd;
}

- (NSString*)templateArticleBeginNormal
{
    if (nil == self._templateArticleBeginNormal) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleBeginNormal" ofType:@"txt"];
        self._templateArticleBeginNormal = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleBeginNormal = [self._templateArticleBeginNormal stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleBeginNormal;
}

- (NSString*)templateArticleBodyPhoto
{
    if (nil == self._templateArticleBodyPhoto) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleBodyPhoto" ofType:@"txt"];
        self._templateArticleBodyPhoto = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleBodyPhoto = [self._templateArticleBodyPhoto stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleBodyPhoto;
}

- (NSString*)templateArticleBodyVideo
{
    if (nil == self._templateArticleBodyVideo) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleBodyVideo" ofType:@"txt"];
        self._templateArticleBodyVideo = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleBodyVideo = [self._templateArticleBodyVideo stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleBodyVideo;
}

- (NSString*)templateArticleBeginTagged
{
    if (nil == self._templateArticleBeginTagged) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleBeginTagged" ofType:@"txt"];
        self._templateArticleBeginTagged = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleBeginTagged = [self._templateArticleBeginTagged stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleBeginTagged;
}



- (NSString*)templateArticleBeginSharedLink
{
    if (nil == self._templateArticleBeginSharedLink) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleBodySharedLink" ofType:@"txt"];
        self._templateArticleBeginSharedLink = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleBeginSharedLink = [self._templateArticleBeginSharedLink stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleBeginSharedLink;
}



- (NSString*)templateArticleBodyStatus
{
    if (nil == self._templateArticleBodyStatus) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleBodyStatus" ofType:@"txt"];
        self._templateArticleBodyStatus = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleBodyStatus = [self._templateArticleBodyStatus stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleBodyStatus;
}

- (NSString*)templateArticleBodyCheckin
{
    if (nil == self._templateArticleBodyCheckin) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleBodyCheckin" ofType:@"txt"];
        self._templateArticleBodyCheckin = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleBodyCheckin = [self._templateArticleBodyCheckin stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleBodyCheckin;
}

- (NSString*)templateHeaderBegin
{
    if (nil == self._templateHeaderBegin) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateHeaderBegin" ofType:@"txt"];
        
        self._templateHeaderBegin = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateHeaderBegin;
}

- (NSString*)templateArticleEnd
{
    if (nil == self._templateArticleEnd) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleEnd" ofType:@"txt"];
        self._templateArticleEnd = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleEnd = [self._templateArticleEnd stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleEnd;
}

- (NSString*)templateArticleEnd1
{
    if (nil == self._templateArticleEnd1) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleEnd1" ofType:@"txt"];
        self._templateArticleEnd1 = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleEnd1 = [self._templateArticleEnd1 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleEnd1;
}

- (NSString*)templateArticleEnd2
{
    if (nil == self._templateArticleEnd2) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleEnd2" ofType:@"txt"];
        self._templateArticleEnd2 = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleEnd2 = [self._templateArticleEnd2 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleEnd2;
}

//_templateArticleForUndo

- (NSString*)templateArticleForUndo
{
    if (nil == self._templateArticleForUndo) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleForUndo" ofType:@"txt"];
        self._templateArticleForUndo = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleForUndo = [self._templateArticleForUndo stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleForUndo;
}

- (NSString*)templateHTMLCommentOther
{
    if (self._templateHTMLCommentOther == nil) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateHTMLCommentOther" ofType:@"txt"];
        self._templateHTMLCommentOther = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateHTMLCommentOther = [self._templateHTMLCommentOther stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateHTMLCommentOther;
}

- (NSString*)templateArticleTweet
{
    if (self._templateArticleTweet == nil) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleTweet" ofType:@"txt"];
        self._templateArticleTweet = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleTweet = [self._templateArticleTweet stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleTweet;
}

- (NSString*)templateArticleTweetPhoto
{
    if (self._templateArticleTweetPhoto == nil) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleTweetPhoto" ofType:@"txt"];
        self._templateArticleTweetPhoto = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleTweetPhoto = [self._templateArticleTweetPhoto stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleTweetPhoto;
}

- (NSString*)templateArticleTweetVideo
{
    if (self._templateArticleTweetVideo == nil) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleTweetVideo" ofType:@"txt"];
        self._templateArticleTweetVideo = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleTweetVideo = [self._templateArticleTweetVideo stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleTweetVideo;

}

- (NSString*)tempTweetHTMLBegin
{
    if (self._tempTweetHTMLBegin == nil) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetHTMLBegin" ofType:@"txt"];
        self._tempTweetHTMLBegin = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._tempTweetHTMLBegin = [self._tempTweetHTMLBegin stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetHTMLBegin;
}

- (NSString*)getIconPhotoTweet
{
    if (self.btn_choose_img == nil) {
        NSString *btn_choose_imge = [[NSBundle mainBundle] pathForResource:@"btn_choose_img" ofType:@"png"];
        self.btn_choose_img = [NSURL fileURLWithPath:btn_choose_imge];
    }
    return self.btn_choose_img;
}

- (NSString*)getIconVideoTweet
{
    if (self.btn_youtube == nil) {
        NSString *btn_youtubee = [[NSBundle mainBundle] pathForResource:@"btn_youtube" ofType:@"png"];
        self.btn_youtube = [NSURL fileURLWithPath:btn_youtubee];
    }
    return self.btn_youtube;
}

- (NSString*)getIconDirrectTweet
{
    if (self.btn_direct_message == nil) {
        NSString *btn_direct_message3 = [[NSBundle mainBundle] pathForResource:@"btn_direct_message" ofType:@"png"];
        self.btn_direct_message = [NSURL fileURLWithPath:btn_direct_message3];
    }
    return self.btn_direct_message;
}

- (NSString*)getIconNewTweet
{
    if (self.btn_newtweet == nil) {
        NSString *btn_newtweet3 = [[NSBundle mainBundle] pathForResource:@"btn_newtweet" ofType:@"png"];
        self.btn_newtweet = [NSURL fileURLWithPath:btn_newtweet3];
    }
    return self.btn_newtweet;
}

//-- template for change profile picture, update cover photo
- (NSString*)templateArticleBeginChangeProfile
{
    if (nil == self._templateArticleBeginChangeProfile) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateArticleBeginChangeProfile" ofType:@"txt"];
        self._templateArticleBeginChangeProfile = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        self._templateArticleBeginChangeProfile = [self._templateArticleBeginChangeProfile stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateArticleBeginChangeProfile;
}


#pragma mark -
#pragma mark - get Icon app
- (NSString*)getIconLike
{
    if (self.iconLike == nil) {
        NSString *iconLike = [[NSBundle mainBundle] pathForResource:@"like" ofType:@"png"];
        self.iconLike = [NSURL fileURLWithPath:iconLike];
    }
    return self.iconLike;
}

- (NSString*)getIconComment
{
    if (self.iconComment == nil) {
        NSString *iconComment = [[NSBundle mainBundle] pathForResource:@"comment" ofType:@"png"];
        self.iconComment = [NSURL fileURLWithPath:iconComment];
    }
    NSLog(@"iconComment1 %@",self.iconComment);
    return self.iconComment;
}

- (NSString*)getIConPhoto
{
    if (self.iconPhoto == nil) {
        NSString *iconPhoto = [[NSBundle mainBundle] pathForResource:@"groupsPhoto@2x" ofType:@"png"];
        self.iconPhoto = [NSURL fileURLWithPath:iconPhoto];
    }
    return self.iconPhoto;
}


- (NSString*)getIConExpand
{
    if (self.iconExpand == nil) {
        NSString *iconComment = [[NSBundle mainBundle] pathForResource:@"fbc_bookmarks_arrowdownglyph_white" ofType:@"png"];
        self.iconExpand = [NSURL fileURLWithPath:iconComment];
    }
    return self.iconExpand;
}

- (NSString*)getIConStatus
{
    if (self.iconStatus == nil) {
        NSString *iconStatus = [[NSBundle mainBundle] pathForResource:@"groupsPost@2x" ofType:@"png"];
        self.iconStatus = [NSURL fileURLWithPath:iconStatus];
    }
    return self.iconStatus;
}

- (NSString*)getIConCheckin
{
    if (self.iconCheckin == nil) {
        NSString *iconCheckin = [[NSBundle mainBundle] pathForResource:@"facecheckin@2x" ofType:@"png"];
        self.iconCheckin = [NSURL fileURLWithPath:iconCheckin];
    }
    return self.iconCheckin;
}

- (NSString*)getIconUndo
{
    if (self.iconUndo == nil) {
        NSString *iconUndo = [[NSBundle mainBundle] pathForResource:@"iconUndo" ofType:@"png"];
        self.iconUndo = [NSURL fileURLWithPath:iconUndo];
    }
    return self.iconUndo;
}

- (NSString*)getiIconVideo
{
    if (self.iconVideo == nil) {
        NSString *iconVideo = [[NSBundle mainBundle] pathForResource:@"PlayButton" ofType:@"png"];
        self.iconVideo = [NSURL fileURLWithPath:iconVideo];
    }
    return self.iconVideo;
}

- (NSString*)geticonRetweet
{
    if (self.icon_retweet == nil) {
        NSString *icon_retweet1 = [[NSBundle mainBundle] pathForResource:@"icon_retweet" ofType:@"png"];
        self.icon_retweet = [NSURL fileURLWithPath:icon_retweet1];
    }
    return self.icon_retweet;
}

#pragma mark - Method Public

+ (NSString*)templateHTMLBegin
{
    return [[Template shared] templateHTMLBegin];
}

+ (NSString*)templateHTMLEnd
{
    return [[Template shared] templateHTMLEnd];
}

+ (NSString*)templateArticleBeginNormal;
{
    return [[Template shared] templateArticleBeginNormal];
}

+ (NSString*)templateArticleBodyPhoto
{
    return [[Template shared] templateArticleBodyPhoto];
}

+ (NSString*)templateArticleBodyVideo
{
    return [[Template shared] templateArticleBodyVideo];
}

+ (NSString*)templateArticleBeginTagged
{
    return [[Template shared] templateArticleBeginTagged];
}

+ (NSString*)templateArticleBeginSharedLink
{
    return [[Template shared] templateArticleBeginSharedLink];
}

+ (NSString*)templateArticleBodyStatus
{
    return [[Template shared] templateArticleBodyStatus];
}

+ (NSString*)templateArticleBodyCheckin
{
    return [[Template shared] templateArticleBodyCheckin];
}

+ (NSString*)templateHeaderBegin
{
    return [[Template shared] templateHeaderBegin];
}

+ (NSString*)templateArticleEnd
{
    return [[Template shared] templateArticleEnd];
}
+ (NSString*)templateArticleEnd1
{
    return [[Template shared] templateArticleEnd1];
}
+ (NSString*)templateArticleEnd2
{
    return [[Template shared] templateArticleEnd2];
}
+ (NSString*)templateArticleForUndo
{
    return [[Template shared] templateArticleForUndo];
}

+ (NSString*)templateHTMLCommentOther
{
    return [[Template shared] templateHTMLCommentOther];
}

+ (NSString*)templateArticleBeginChangeProfile;
{
    return [[Template shared] templateArticleBeginChangeProfile];
}


+ (NSString*)getiIconVideo
{
    return [[Template shared] getiIconVideo];
}

+ (NSString*)getIconUndo
{
    return [[Template shared] getIconUndo];
}

+ (NSString*)getIConCheckin
{
    return [[Template shared] getIConCheckin];
}

+ (NSString*)getIConStatus
{
    return [[Template shared] getIConStatus];
}

+ (NSString*)getIConExpand
{
    return [[Template shared] getIConExpand];
}

+ (NSString*)getIConPhoto
{
    return [[Template shared] getIConPhoto];
}

+ (NSString*)getIconComment
{
    return [[Template shared] getIconComment];
}

+ (NSString*)getIconLike
{
    return [[Template shared] getIconLike];
}

+ (NSString*)geticonRetweet
{
     return [[Template shared] geticonRetweet];
}

+ (NSString*)templateArticleTweet
{
    return [[Template shared] templateArticleTweet];
}

+ (NSString*)templateArticleTweetPhoto
{
    return [[Template shared] templateArticleTweetPhoto];
}

+ (NSString*)templateArticleTweetVideo
{
    return [[Template shared] templateArticleTweetVideo];
}

+ (NSString*)tempTweetHTMLBegin
{
    return [[Template shared] tempTweetHTMLBegin];
}

+ (NSString*)getIconPhotoTweet
{
    return [[Template shared] getIconPhotoTweet];
}

+ (NSString*)getIconVideoTweet
{
    return [[Template shared] getIconVideoTweet];
}

+ (NSString*)getIconDirrectTweet
{
    return [[Template shared] getIconDirrectTweet];
}

+ (NSString*)getIconNewTweet
{
    return [[Template shared] getIconNewTweet];
}

@end
