//
//  User.h
//  AGKGlobal
//
//  Created by Thuy on 9/25/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property(nonatomic,retain) NSString *userID;
@property(nonatomic,retain) NSString *name;
@property(nonatomic,retain) NSString *pic;
@property(nonatomic,retain) NSString *jsname;
@property(nonatomic,retain) NSString *jspic;
@property(nonatomic,retain) NSString *userUID;

- (id)initWithDictinary:(NSDictionary*)dict;
@end
