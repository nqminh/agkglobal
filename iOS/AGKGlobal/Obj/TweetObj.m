//
//  TweetObj.m
//  AGKGlobal
//
//  Created by Thuy on 10/4/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "TweetObj.h"

@implementation TweetObj

- (id)initWithDictinary:(NSDictionary*)dict
{
    if (self = [super init]) {
        self.contributors = ([dict objectForKey:@"contributors"] == [NSNull null]) ? @"" : [dict objectForKey:@"contributors"];
        self.coordinates = ([dict objectForKey:@"coordinates"] == [NSNull null]) ? @"" : [dict objectForKey:@"coordinates"];
        self.created_at = [dict objectForKey:@"created_at"];
        self.entities = [dict objectForKey:@"entities"];
        self.favorite_count = [[dict objectForKey:@"favorite_count"] integerValue];
        self.favorited = [[dict objectForKey:@"favorited"] integerValue];
        self.geo = ([dict objectForKey:@"geo"] == [NSNull null]) ? @"" : [dict objectForKey:@"geo"];
        self.idTweet = [NSString stringWithFormat:@"%@",[dict objectForKey:@"id"]];
        self.id_str = [NSString stringWithFormat:@"%@",[dict objectForKey:@"id_str"]];
        
        self.in_reply_to_screen_name = ([dict objectForKey:@"in_reply_to_screen_name"] == [NSNull null]) ? @"" : [dict objectForKey:@"in_reply_to_screen_name"];
        
        self.in_reply_to_status_id = ([dict objectForKey:@"in_reply_to_status_id"] == [NSNull null]) ? @"" : [dict objectForKey:@"in_reply_to_status_id"];
        
        self.in_reply_to_status_id_str = ([dict objectForKey:@"in_reply_to_status_id_str"] == [NSNull null]) ? @"" : [dict objectForKey:@"in_reply_to_status_id_str"];
        
        self.in_reply_to_user_id = ([dict objectForKey:@"in_reply_to_user_id"] == [NSNull null]) ? @"" : [dict objectForKey:@"in_reply_to_user_id"];
        
        self.in_reply_to_user_id_str = ([dict objectForKey:@"in_reply_to_user_id_str"] == [NSNull null]) ? @"" : [dict objectForKey:@"in_reply_to_user_id_str"];
        
        self.lang = [dict objectForKey:@"lang"];
        self.place = ([dict objectForKey:@"place"] == [NSNull null]) ? @"" : [dict objectForKey:@"place"];
        self.possibly_sensitive = [[dict objectForKey:@"possibly_sensitive"]integerValue];
        self.retweet_count = [[dict objectForKey:@"retweet_count"]integerValue];
        self.retweeted = [[dict objectForKey:@"retweeted"]integerValue];
        self.source = [dict objectForKey:@"source"];
        self.text = [dict objectForKey:@"text"];
        self.truncated = [[dict objectForKey:@"truncated"] integerValue];
        self.user = [dict objectForKey:@"user"];
        self.retweeted_status = [dict objectForKey:@"retweeted_status"];
        self.defaultProfileStr = [[self.user objectForKey:@"default_profile"] stringValue];
        self.protectedStr = [[self.user objectForKey:@"protected"] stringValue];
        [self analysis];
        [self analysisLink];
        [self getHTML];
    }
    return self;
}

- (void)analysisLink
{
    NSArray *mediaArray = [self.entities valueForKey:@"media"];
    
    NSArray *displayUrlArray = [self.entities objectForKey:@"urls"];
    
    NSArray *retweetUrlArray = [[self.retweeted_status objectForKey:@"entities"] objectForKey:@"urls"];
    
    NSArray *retweetMediaArray = [[self.retweeted_status objectForKey:@"entities"] objectForKey:@"media"];
    
    if (mediaArray || retweetMediaArray) self.isMedia = YES;
    else self.isMedia = NO;
    
    if (displayUrlArray.count>0 || retweetUrlArray.count>0) self.isUrls = YES;
    else self.isUrls = NO;
    
    self.quocteText = self.webString;
    //get display_url
    if (self.isMedia == YES && self.isUrls == YES) {
        if (self.retweeted_status) {
            if (retweetUrlArray.count>0 && retweetMediaArray.count>0){
                self.urlDisplayMedia = [[retweetMediaArray objectAtIndex:0] objectForKey:@"display_url"];
                self.urlMedia = [[retweetMediaArray objectAtIndex:0] objectForKey:@"url"];
                self.urlDisplayUrls = [[retweetUrlArray objectAtIndex:0] objectForKey:@"display_url"];
                self.urlUrls = [[retweetUrlArray objectAtIndex:0] objectForKey:@"url"];
//                [self getHeight];
                
                self.webString = [self convertReplaceTextToLink:self.webString text:self.urlUrls with:self.urlDisplayUrls];
                self.webString = [self convertReplaceTextToLink:self.webString text:self.urlMedia with:self.urlDisplayMedia];
                self.urlExpandedUrls = [[retweetUrlArray objectAtIndex:0] objectForKey:@"expanded_url"];
                self.urlExpandedMedia = [[retweetMediaArray objectAtIndex:0] objectForKey:@"media_url"];
                [self convertToQuocteText:self.urlUrls with:self.urlDisplayUrls];
                [self convertToQuocteText:self.urlMedia with:self.urlDisplayMedia];
                NSLog(@"urlExpandedUrls: %@ \n urlExpandedMedia:%@",self.urlExpandedUrls,self.urlExpandedMedia);
            }
        }
        else {
            if (mediaArray.count>0 && displayUrlArray.count>0){
                self.urlDisplayMedia = [[mediaArray objectAtIndex:0] objectForKey:@"display_url"];
                self.urlMedia = [[mediaArray objectAtIndex:0] objectForKey:@"url"];
                self.urlDisplayUrls = [[displayUrlArray objectAtIndex:0] objectForKey:@"display_url"];
                self.urlUrls = [[displayUrlArray objectAtIndex:0] objectForKey:@"url"];
                
                self.webString = [self convertReplaceTextToLink:self.webString text:self.urlUrls with:self.urlDisplayUrls];
                self.webString = [self convertReplaceTextToLink:self.webString text:self.urlMedia with:self.urlDisplayMedia];
                self.urlExpandedUrls = [[displayUrlArray objectAtIndex:0] objectForKey:@"expanded_url"];
                self.urlExpandedMedia = [[mediaArray objectAtIndex:0] objectForKey:@"media_url"];
                [self convertToQuocteText:self.urlUrls with:self.urlDisplayUrls];
                [self convertToQuocteText:self.urlMedia with:self.urlDisplayMedia];
                 NSLog(@"urlExpandedUrls: %@ \n urlExpandedMedia:%@",self.urlExpandedUrls,self.urlExpandedMedia);
            }
        }
    }
    else if (self.isMedia == NO && self.isUrls == NO) {
        self.urlDisplayMedia = nil;
        self.urlMedia = nil;
        self.urlDisplayUrls = nil;
        self.urlUrls = nil;
    }
    else {
        if (self.isMedia == YES && self.isUrls == NO) {
            if (self.retweeted_status){
                if (retweetMediaArray.count>0) {
                    self.urlDisplayMedia = [[retweetMediaArray objectAtIndex:0] objectForKey:@"display_url"];
                    self.urlMedia = [[retweetMediaArray objectAtIndex:0] objectForKey:@"url"];                    
                    self.webString = [self convertReplaceTextToLink:self.webString text:self.urlMedia with:self.urlDisplayMedia];
                    self.urlExpandedMedia = [[retweetMediaArray objectAtIndex:0] objectForKey:@"media_url"];
                    
                    [self convertToQuocteText:self.urlMedia with:self.urlDisplayMedia];
                }
            }else{
                if (mediaArray.count>0) {
                    self.urlDisplayMedia = [[mediaArray objectAtIndex:0] objectForKey:@"display_url"];
                    self.urlMedia = [[mediaArray objectAtIndex:0] objectForKey:@"url"];
                    self.webString = [self convertReplaceTextToLink:self.webString text:self.urlMedia with:self.urlDisplayMedia];
                    self.urlExpandedMedia = [[mediaArray objectAtIndex:0] objectForKey:@"media_url"];
                    
                    [self convertToQuocteText:self.urlMedia with:self.urlDisplayMedia];
                }
            }
        }
        else {
            if (self.retweeted_status) {
                if (retweetUrlArray.count>0){
                    self.urlDisplayUrls = [[retweetUrlArray objectAtIndex:0] objectForKey:@"display_url"];
                    self.urlUrls = [[retweetUrlArray objectAtIndex:0] objectForKey:@"url"];
                    self.webString = [self convertReplaceTextToLink:self.webString text:self.urlUrls with:self.urlDisplayUrls];
                    self.urlExpandedUrls = [[retweetUrlArray objectAtIndex:0] objectForKey:@"expanded_url"];
                    
                    [self convertToQuocteText:self.urlUrls with:self.urlDisplayUrls];
                }
            }
            else{
                if (displayUrlArray.count>0) {
                    self.urlDisplayUrls = [[displayUrlArray objectAtIndex:0] objectForKey:@"display_url"];
                    self.urlUrls = [[displayUrlArray objectAtIndex:0] objectForKey:@"url"];
                    self.webString = [self convertReplaceTextToLink:self.webString text:self.urlUrls with:self.urlDisplayUrls];
                    self.urlExpandedUrls = [[displayUrlArray objectAtIndex:0] objectForKey:@"expanded_url"];
                    
                    [self convertToQuocteText:self.urlUrls with:self.urlDisplayUrls];
                }
            }
        }
    }
}

- (void)analysis
{
    if (self.retweeted_status){
        self.webString = [self.retweeted_status objectForKey:@"text"];
        self.messageTweet = [self.retweeted_status objectForKey:@"text"];;
        
        self.created_at =[self.retweeted_status objectForKey:@"created_at"];
        
        self.profileUrl = [self getBiggerTwitterImageURL:[[self.retweeted_status objectForKey:@"user"] objectForKey:@"profile_image_url"]];
        
        self.nameStr = [[self.retweeted_status objectForKey:@"user"] objectForKey:@"name"];
        self.screenNameStr = [NSString stringWithFormat:@"%@",[[self.retweeted_status objectForKey:@"user"] objectForKey:@"screen_name"]];
        
        self.favorite_count = [[self.retweeted_status objectForKey:@"favorite_count"] integerValue];
        self.favorited = [[self.retweeted_status objectForKey:@"favorited"] integerValue];
        
        //--when has a param @"retweet_status" in return data, display viewChild
        self.alpha = 1;
        self.retweeter = [NSString stringWithFormat:@"%@ retweeted",[self.user objectForKey:@"name"]];
        
    }else{
        self.alpha = 0;
        
        self.webString = self.text;
        self.messageTweet = self.text;
        
        self.profileUrl = [self getBiggerTwitterImageURL:[self.user objectForKey:@"profile_image_url"]];
        
        self.nameStr = [self.user objectForKey:@"name"];
        self.screenNameStr = [NSString stringWithFormat:@"%@",[self.user objectForKey:@"screen_name"]];
        
        self.favorite_count = self.favorite_count;
        self.favorited = self.favorited;
    }
    
    if (!self.nameStr) self.nameStr = @"";
    if (!self.screenNameStr) self.screenNameStr = @"";
    self.screenNameDis = [NSString stringWithFormat:@" @%@",self.screenNameStr];

    self.dateConverted = [Utility dateTimeStringForTwitterTimeToPush:self.created_at];
    self.dateConvertedHome = [Utility dateTimeStringForTwitterTime:self.created_at];
    
    //display content tweet
    if (!self.webString) {self.webString = @"";self.messageTweet = @"";}
}

- (NSString*)getBiggerTwitterImageURL:(NSString*)url
{
    NSString *result = [url stringByReplacingOccurrencesOfString:@"_normal." withString:@"."];
    
    return result;
}

- (NSString*)convertReplaceTextToLink:(NSString*)msg text:(NSString*)text with:(NSString*)link
{
    NSLog(@"%@ \n %@",text,link);
    NSString* newlink = [NSString stringWithFormat:@"<a href=\"http://null/clickLink/%@\" style=\"text-decoration:none;\">%@</a>",text,link];
                    
    msg = [msg stringByReplacingOccurrencesOfString:text withString:newlink];
      
    return msg;
}

- (void)convertToQuocteText:(NSString*)text with:(NSString*)newtext
{
    NSLog(@"%@ \n %@",text,newtext);
    NSString* newlink = [NSString stringWithFormat:@"%@",newtext];
    
    self.quocteText = [self.quocteText stringByReplacingOccurrencesOfString:text withString:newlink];
}

- (void) getHeight
{
    NSString *textX = self.webString;
    
    CGFloat heightText = 0 ;
    heightText =[Utility heightFromString:textX maxWidth:228 font:[UIFont systemFontOfSize:13]];
    
    self.height = 0;
    if (self.retweeted_status) {
        self.height = heightText + 22 + 20;
        if (self.height <= 60) {
            self.height = 75;
        }
    }
    else{
        self.height = heightText + 22 + 10;
        
        if (self.height <= 50) {
            self.height = 60;
        }
        else if (self.height <= 65 && self.height > 50)
        {
            self.height = 70;
        }
    }
}

- (void)getHTML
{
    NSString* retweeterText = @"";
    if (self.alpha == 1) {
        retweeterText = [self getRetweetStringWithName:self.retweeter];
    }
    NSString* icon = @"";
    NSString* article = @"";
    if (self.urlDisplayMedia) {
        icon = [self getHTMLForIconPhoto];
        article = [NSString stringWithFormat:[Template templateArticleTweetPhoto],
                   self.idTweet,
                   retweeterText,
                   self.profileUrl,
                   @"240px",
                   @"70%",
                   @"70%",
                   self.nameStr,
                   [NSString stringWithFormat:@"@%@",self.screenNameStr],
                   icon,
                   self.dateConvertedHome,
                   @"100%",
                   self.webString,self.urlExpandedMedia];
    }
    
    else if ([self.urlDisplayUrls length] && ([self.urlDisplayUrls rangeOfString:@"youtube.com"].location != NSNotFound ||
                                         [self.urlDisplayUrls rangeOfString:@"m.youtube.com"].location != NSNotFound ||
                                         [self.urlDisplayUrls rangeOfString:@"youtu.be"].location != NSNotFound))
    {
        icon = [self getHTMLForIconVideo];
        article = [NSString stringWithFormat:[Template templateArticleTweet],
                                                     self.idTweet,
                                                     retweeterText,
                                                     self.profileUrl,
                                                     @"240px",
                                                     @"70%",
                                                     @"70%",
                                                     self.nameStr,
                                                     [NSString stringWithFormat:@"@%@",self.screenNameStr],
                                                     icon,
                                                     self.dateConvertedHome,
                                                     @"100%",
                                                     self.webString];
    }
    
    else
    {
        article = [NSString stringWithFormat:[Template templateArticleTweet],
                   self.idTweet,
                   retweeterText,
                   self.profileUrl,
                   @"240px",
                   @"70%",
                   @"70%",
                   self.nameStr,
                   [NSString stringWithFormat:@"@%@",self.screenNameStr],
                   icon,
                   self.dateConvertedHome,
                   @"100%",
                   self.webString];
    }
    self.HTML = article;
}

- (NSString*)getRetweetStringWithName:(NSString*)name
{
    return [NSString stringWithFormat:@"<div style=\"margin-bottom: 7px; margin-top: -4px;\"><div style=\"float: left; margin-left: 20px;margin-top: 2px;background: url(%@) no-repeat scroll 0 0 transparent;height: 17px;width: 27px;\"></div><div style=\"float: left; margin-left: -8px;padding:0;\"><span class=\"mfss fcg\">%@</span></div><div style=\"clear: both;\"></div></div>",
            [Template geticonRetweet],
            name];
}

- (NSString*)getHTMLForIconPhoto
{
    return [NSString stringWithFormat:@"<img style=\"width: 12px; margin-bottom: -2px\" src=\"%@\"/>  ",[Template getIconPhotoTweet]];
}

- (NSString*)getHTMLForIconVideo
{
    return [NSString stringWithFormat:@"<img style=\"width: 12px; margin-bottom: -2px\" src=\"%@\"/>  ",[Template getIconVideoTweet]];
}

@end
