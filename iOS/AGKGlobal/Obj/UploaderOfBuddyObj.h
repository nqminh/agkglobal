//
//  UploaderOfBuddyObj.h
//  AGKGlobal
//
//  Created by Dung on 3/3/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UploaderOfBuddyObj : NSObject

@property (nonatomic,strong) NSString* uob_user_id;
@property (nonatomic,strong) NSString* uob_avatar;
@property (nonatomic,strong) NSString* uob_age;
@property (nonatomic,strong) NSString* uob_count;
@property (nonatomic,strong) NSString* uob_full_name;
@property (nonatomic,strong) NSString* uob_sex;
@property (nonatomic,strong) NSString* uob_birthday;

- (id)initWithDictinary:(NSDictionary*) uploader;
@end
