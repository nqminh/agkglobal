//
//  Template.h
//  AGKGlobal
//
//  Created by Thuy on 9/21/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Template : NSObject
{
    
}

@property (nonatomic,retain) NSString            *_templateHTMLBegin;
@property (nonatomic,retain) NSString            *_templateHTMLEnd;
@property (nonatomic,retain) NSString            *_templateArticleBeginNormal;
@property (nonatomic,retain) NSString            *_templateArticleBeginTagged;
@property (nonatomic,retain) NSString            *_templateArticleBeginSharedLink;
@property (nonatomic,retain) NSString            *_templateArticleBodyCheckin;
@property (nonatomic,retain) NSString            *_templateArticleBodyPhoto;
@property (nonatomic,retain) NSString            *_templateArticleBodyStatus;
@property (nonatomic,retain) NSString            *_templateArticleVideo;
@property (nonatomic,retain) NSString            *_templateArticleEnd;
@property (nonatomic,retain) NSString            *_templateArticleEnd1;
@property (nonatomic,retain) NSString            *_templateArticleEnd2;
@property (nonatomic,retain) NSString            *_templateArticleBodyVideo;
@property (nonatomic,retain) NSString            *_templateArticleForUndo;
@property (nonatomic,retain) NSString            *_templateHeaderBegin;
@property (nonatomic,retain) NSString            *_templateHTMLCommentOther;
@property (nonatomic,retain) NSString            *_templateArticleTweet;
@property (nonatomic,retain) NSString            *_templateArticleTweetPhoto;
@property (nonatomic,retain) NSString            *_templateArticleTweetVideo;
@property (nonatomic,retain) NSString            *_tempTweetHTMLBegin;
@property (nonatomic,retain) NSString            *_templateArticleBeginChangeProfile;


@property (nonatomic,retain) NSString            *iconLike;
@property (nonatomic,retain) NSString            *iconComment;
@property (nonatomic,retain) NSString            *iconExpand;
@property (nonatomic,retain) NSString            *iconStatus;
@property (nonatomic,retain) NSString            *iconCheckin;
@property (nonatomic,retain) NSString            *iconPhoto;
@property (nonatomic,retain) NSString            *iconUndo;
@property (nonatomic,retain) NSString            *iconVideo;
@property (nonatomic,retain) NSString            *icon_retweet;
@property (nonatomic,retain) NSString            *btn_choose_img;
@property (nonatomic,retain) NSString            *btn_youtube;
@property (nonatomic,retain) NSString            *btn_direct_message;
@property (nonatomic,retain) NSString            *btn_newtweet;

- (NSString*)templateHTMLBegin;
- (NSString*)templateHTMLEnd;
- (NSString*)templateArticleBeginNormal;
- (NSString*)templateArticleBodyPhoto;
- (NSString*)templateArticleBodyVideo;
- (NSString*)templateArticleBeginTagged;
- (NSString*)templateArticleBeginSharedLink;
- (NSString*)templateArticleBodyStatus;
- (NSString*)templateArticleBodyCheckin;
- (NSString*)templateHeaderBegin;
- (NSString*)templateArticleEnd;
- (NSString*)templateArticleEnd1;
- (NSString*)templateArticleEnd2;
- (NSString*)templateArticleForUndo;
- (NSString*)templateHTMLCommentOther;
- (NSString*)templateArticleTweet;
- (NSString*)tempTweetHTMLBegin;
- (NSString*)templateArticleBeginChangeProfile;
- (NSString*)templateArticleTweetPhoto;
- (NSString*)templateArticleTweetVideo;

- (NSString*)getiIconVideo;
- (NSString*)getIconUndo;
- (NSString*)getIConCheckin;
- (NSString*)getIConStatus;
- (NSString*)getIConExpand;
- (NSString*)getIConPhoto;
- (NSString*)getIconComment;
- (NSString*)getIconLike;
- (NSString*)geticonRetweet;
- (NSString*)getIconPhotoTweet;
- (NSString*)getIconVideoTweet;
- (NSString*)getIconDirrectTweet;
- (NSString*)getIconNewTweet;

- (void) start;

+ (Template*)shared;

+ (NSString*)templateHTMLBegin;
+ (NSString*)templateHTMLEnd;
+ (NSString*)templateArticleBeginNormal;
+ (NSString*)templateArticleBodyPhoto;
+ (NSString*)templateArticleBodyVideo;
+ (NSString*)templateArticleBeginTagged;
+ (NSString*)templateArticleBeginSharedLink;
+ (NSString*)templateArticleBodyStatus;
+ (NSString*)templateArticleBodyCheckin;
+ (NSString*)templateHeaderBegin;
+ (NSString*)templateArticleEnd;
+ (NSString*)templateArticleEnd1;
+ (NSString*)templateArticleEnd2;
+ (NSString*)templateArticleForUndo;
+ (NSString*)templateHTMLCommentOther;
+ (NSString*)templateArticleTweet;
+ (NSString*)tempTweetHTMLBegin;
+ (NSString*)templateArticleBeginChangeProfile;

+ (NSString*)templateArticleTweetPhoto;
+ (NSString*)templateArticleTweetVideo;

+ (NSString*)getiIconVideo;
+ (NSString*)getIconUndo;
+ (NSString*)getIConCheckin;
+ (NSString*)getIConStatus;
+ (NSString*)getIConExpand;
+ (NSString*)getIConPhoto;
+ (NSString*)getIconComment;
+ (NSString*)getIconLike;
+ (NSString*)geticonRetweet;
+ (NSString*)getIconPhotoTweet;
+ (NSString*)getIconVideoTweet;
+ (NSString*)getIconDirrectTweet;
+ (NSString*)getIconNewTweet;

@end
