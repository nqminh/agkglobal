//
//  UploaderOfBuddyObj.m
//  AGKGlobal
//
//  Created by Dung on 3/3/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import "UploaderOfBuddyObj.h"

@implementation UploaderOfBuddyObj

- (id)initWithDictinary:(NSDictionary*) uploader
{
    if (self == [super init])
    {
        self.uob_age        = [uploader objectForKey:@"age"];
        self.uob_avatar     = [uploader objectForKey:@"avatar"];
        self.uob_birthday   = [uploader objectForKey:@"birthday"];
        self.uob_count      = [uploader objectForKey:@"count"];
        self.uob_full_name  = [uploader objectForKey:@"full_name"];
        self.uob_user_id    = [uploader objectForKey:@"id"];
        self.uob_sex        = [uploader objectForKey:@"sex"];
    }
    return self;    
}


@end
