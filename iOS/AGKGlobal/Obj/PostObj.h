//
//  PostObj.h
//  AGKGlobal
//
//  Created by Thuy on 9/21/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Obj.h"
#import "User.h"
#import "FacebookUtils.h"

@interface PostObj : NSObject
{
    NSInteger typePostf;  //0 simple, 1 double
}

@property(nonatomic,retain) NSString*                   strHtml;

@property(nonatomic,retain) NSString*                   app_id;
@property(nonatomic,retain) NSString*                   postID;
@property(nonatomic,retain) NSString*                   userID;
@property(nonatomic,retain) NSString*                   pic;
@property(nonatomic,retain) NSString*                   name;
@property(nonatomic,retain) NSString*                   datetime;
@property(nonatomic,retain) NSString*                   message;
@property(nonatomic,retain) NSString*                   reason;
@property(nonatomic,retain) NSString*                   linkURL;
@property(nonatomic,retain) NSString*                   postPhotoURL;
@property(nonatomic,retain) NSString*                   attachType;
@property(nonatomic,retain) NSString*                   attachCaption;
@property(nonatomic,retain) NSString*                   checkinName;
@property(nonatomic,retain) NSString*                   checkinIconURL;
@property(nonatomic,retain) NSString*                   checkinMessage;
@property(nonatomic,retain) NSNumber*                   created_time;
@property(nonatomic,retain) NSString*                   attach_descriptionn;
@property(nonatomic,retain) NSString*                   attach_icon;
@property(nonatomic,retain) NSString*                   attach_name;
@property(nonatomic,retain) NSString*                   attach_properties;
@property(nonatomic,retain) NSString*                   fb_object_id;
@property(nonatomic,retain) NSString*                   permalink;
@property(nonatomic,retain) NSString*                   isVideo;
@property(nonatomic,retain) NSString*                   nameLink;
@property(nonatomic,retain) NSString*                   rangeLikeComment;
@property(nonatomic,retain) NSNumber*                   postType;
@property(nonatomic,retain) NSNumber*                   likeCount;
@property(nonatomic,retain) NSNumber*                   likedByUser;
@property(nonatomic,retain) NSNumber*                   commentCount;
@property(nonatomic,retain) NSNumber*                   canLike;
@property(nonatomic,retain) NSNumber*                   canComment;
@property(nonatomic,retain) NSString*                   place;
@property(nonatomic,retain) NSMutableArray*             comment_list;
@property(nonatomic,retain) NSMutableArray*             media_all_array;
@property(nonatomic,retain) NSMutableDictionary*        share_info;
@property(nonatomic,retain) PostObj*                    subPost;
@property(nonatomic,assign) NSInteger                   isContinueReading;
@property(nonatomic,retain) NSString*                   tmpMessage;

@property(nonatomic,assign) NSInteger                   isShared;
@property(nonatomic,retain) NSString*                   link_share_post;

@property(nonatomic,retain) NSMutableArray*       attach_property_photo;
@property(nonatomic,retain) NSString*                   text_name_property_timeline_photo;

- (id)initWithDictinary:(NSDictionary*)post;
- (id)initWitProfile:(User*)user withPermaling:(NSString*)link;
- (PostObj*)initSubPost;
- (id)initWithDictinary:(NSDictionary*)post subPost:(NSDictionary*)subPost;

- (void)liked;
- (void)unliked;

- (NSString*)resetHTML;
- (void)resetName;

@end
