//
//  ControlObj.h
//  AGKGlobal
//
//  Created by Thuy on 9/21/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MulticastDelegate.h"
#import "Obj.h"
#import "FacebookUtils.h"

@class PostObj;
@class ControlObj;
@class User;

@protocol ControlObjDelegate <NSObject>

@optional

- (void)ControlObj:(ControlObj*)controlObj finishedSeemore:(NSMutableArray*)arr;
- (void)ControlObj:(ControlObj*)controlObj newsfeeds:(PostObj*)PostObj;
- (void)ControlObj:(ControlObj*)controlObj seemore:(PostObj*)PostObj;
- (void)ControlObj:(ControlObj*)controlObj jsforFacebookUser:(NSString*)js;
- (void)ControlObj:(ControlObj*)controlObj newStories:(NSString*)text;

@end

enum {
    seemore = 0,
    newsfeedr = 1
};
typedef NSInteger typeGetPost;

@interface ControlObj : NSObject <FacebookUtilsDelegate>
{
    MulticastDelegate<ControlObjDelegate> *delegates;
}

//@property(nonatomic,assign) id <ControlObjDelegate> delegate;
@property(nonatomic,assign) NSInteger       count;
@property(nonatomic,assign) typeGetPost     typePost;
@property(nonatomic,retain) NSMutableArray* listUser;
@property(nonatomic,retain) NSMutableArray* listPlace;
@property(nonatomic,retain) NSMutableArray* listNewStory;
@property(nonatomic,retain) NSMutableArray* listSeeMore;

@property(nonatomic,retain) NSMutableDictionary* postDict;
@property(nonatomic,retain) NSMutableDictionary* userDict;
@property(nonatomic,retain) NSMutableDictionary* postDictHref;

+ (void)initWithDelegate:(id<ControlObjDelegate>) delegate;
+ (void)addDelegate:(id)delegate;
+ (void)removeDelegate:(id)delegate;
- (void)addDelegate:(id)delegate;
- (void)removeDelegate:(id)delegate;
+ (ControlObj*)shared;

+ (void)addUser:(NSString*)user;
+ (void)addPlace:(NSString*)place;
+ (NSString*)getStringListUser;
+ (NSString*)getStringListPlace;
+ (void)analysisData:(NSArray*) result withType:(NSInteger)typepost;
+ (void)resetCount;
+ (PostObj*)getPostWithKey:(NSString*)key;
+ (User*)getUserWithID:(NSString*)keyID;
+ (PostObj*)getPostHrefWithKey:(NSString*)key;
+ (void) processFriendList:(NSArray*) tmpary;
+ (NSString*)getNameWithID:(NSString*)keyID;
+ (NSString*)getPicWithID:(NSString*)keyID;

- (PostObj*)getPostWithKey:(NSString*)key;
- (PostObj*)getPostHrefWithKey:(NSString*)key;

- (User*)getUserWithID:(NSString*)keyID;
- (void)resetCount;
- (void)analysisData:(NSArray*) result withType:(NSInteger)typepost;
- (void)addPlace:(NSString*)place;
- (void)addUser:(NSString*)user;
- (void) processFriendList:(NSArray*) tmpary;
-(NSMutableDictionary*)getAllUserDict;
@end
