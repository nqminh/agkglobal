//
//  TweetObj.h
//  AGKGlobal
//
//  Created by Thuy on 10/4/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Template.h"

@interface TweetObj : NSObject
{
    
}

@property(nonatomic,retain) NSString*                   HTML;
@property(nonatomic,retain) NSString*                   contributors;
@property(nonatomic,retain) NSString*                   coordinates;
@property(nonatomic,retain) NSString*                   created_at;
@property(nonatomic,retain) NSDictionary*               entities;
@property(nonatomic,assign) NSInteger                   favorite_count;
@property(nonatomic,assign) NSInteger                   favorited;
@property(nonatomic,retain) NSString*                   geo;
@property(nonatomic,retain) NSString*                   idTweet;
@property(nonatomic,retain) NSString*                   id_str;
@property(nonatomic,retain) NSString*                   in_reply_to_screen_name;
@property(nonatomic,retain) NSString*                   in_reply_to_status_id;
@property(nonatomic,retain) NSString*                   in_reply_to_status_id_str;
@property(nonatomic,retain) NSString*                   in_reply_to_user_id;
@property(nonatomic,retain) NSString*                   in_reply_to_user_id_str;
@property(nonatomic,retain) NSString*                   lang;
@property(nonatomic,retain) NSString*                   place;
@property(nonatomic,assign) NSInteger                   possibly_sensitive;
@property(nonatomic,assign) NSInteger                   retweet_count;
@property(nonatomic,assign) NSInteger                   retweeted;
@property(nonatomic,retain) NSString*                   source;
@property(nonatomic,retain) NSString*                   text;
@property(nonatomic,assign) NSInteger                   truncated;
@property(nonatomic,retain) NSDictionary*               user;
@property(nonatomic,retain) NSDictionary*               retweeted_status;

//-- infor property
@property(nonatomic,retain) NSString*                   urlDisplayMedia;
@property(nonatomic,retain) NSString*                   urlMedia;
@property(nonatomic,retain) NSString*                   urlDisplayUrls;
@property(nonatomic,retain) NSString*                   urlUrls;
@property(nonatomic,retain) NSString*                   displayURL;
@property(nonatomic,retain) NSString*                   webString;
@property(nonatomic,retain) NSString*                   messageTweet;
@property(nonatomic,retain) NSString*                   nameStr;
@property(nonatomic,retain) NSString*                   screenNameStr;
@property(nonatomic,assign) NSInteger                   alpha;
@property(nonatomic,retain) NSString*                   retweeter;
@property(nonatomic,retain) NSString*                   screenNameDis;
@property(nonatomic,retain) NSString*                   dateConverted;
@property(nonatomic,retain) NSString*                   dateConvertedHome;
@property(nonatomic,retain) NSString*                   profileUrl;
@property(nonatomic,assign) CGFloat                     height;
@property(nonatomic,retain) NSString*                   urlExpandedMedia;
@property(nonatomic,retain) NSString*                   urlExpandedUrls;
@property(nonatomic,retain) NSString*                   protectedStr;
@property(nonatomic,retain) NSString*                   defaultProfileStr;
@property(nonatomic,assign) BOOL                        isMedia;
@property(nonatomic,assign) BOOL                        isUrls;
@property(nonatomic,retain) NSString*                   quocteText;

- (id)initWithDictinary:(NSDictionary*)dict;
- (void)getHTML;


@end
