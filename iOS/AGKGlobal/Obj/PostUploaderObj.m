//
//  PostUploaderObj.m
//  AGKGlobal
//
//  Created by Dung on 3/1/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import "PostUploaderObj.h"

@implementation PostUploaderObj


- (id)initWithDictinary:(NSDictionary*)post
{
    if (self == [super init])
    {
        self.pul_user_id    =    [post objectForKey:@"user_id"];
        self.pul_post_id    = [post objectForKey:@"id"];
        self.pul_media_file = [post objectForKey:@"media_file"];
        self.pul_social_type = [post objectForKey:@"social_type"];
        self.pul_status      = [post objectForKey:@"status"] ;
        
        self.pul_dict_data    = [[NSMutableDictionary alloc] init];
        self.pul_dict_privacy = [[NSMutableDictionary alloc] init];
                
        self.pul_dict_data    = [post objectForKey:@"data"];
        if ([self.pul_dict_data count] > 0)
        {            
                self.pul_message = [self.pul_dict_data objectForKey:STT_MESSAGE_POST];
                self.pul_dict_privacy = [self.pul_dict_data objectForKey:STT_PRIVACY_POST];
                self.pul_checkin      = [self.pul_dict_data objectForKey:STT_PLACE_CHECKIN];
                self.pul_post_type    = [self.pul_dict_data objectForKey:STT_POST_TYPE];
                self.pul_post_date    = [self.pul_dict_data objectForKey:STT_DATE_POST];
        }
        
    }
    return self;

}

@end
