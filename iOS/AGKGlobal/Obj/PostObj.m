//
//  PostObj.m
//  AGKGlobal
//
//  Created by Thuy on 9/21/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "PostObj.h"

@implementation PostObj

- (id)initWitProfile:(User*)user withPermaling:(NSString*)link
{
    if (self = [super init]) {
        self.name = user.name;
        self.pic = user.pic;
        self.permalink = link;
    }
    return self;
}

- (id)initWithDictinary:(NSDictionary*)post
{
    if (self = [super init]) {
        
        DLOG(@"post: \n %@",post);
        self.linkURL = @"";
        
        self.app_id = [NSString stringWithFormat:@"%@",[post objectForKey:@"app_id"]];
        
        self.share_info = [[NSMutableDictionary alloc] init];
        self.attach_property_photo  = [[NSMutableArray alloc] init] ;
        self.share_info = [post objectForKey:@"share_info"];
        NSLog(@"share_info: \n %@",self.share_info);
        
        BOOL didShareALink = NO;
        
        self.postID = [post objectForKey:@"post_id"];
        
        if ([post objectForKey:@"actor_id"] != nil) {
            self.userID = [NSString stringWithFormat:@"%@",[post objectForKey:@"actor_id"]];
        }
        
        @try {
            self.place = [post objectForKey:@"place"] != [NSNull null] ? [[post objectForKey:@"place"] stringValue] : @"";
        }
        @catch (NSException *exception) {
            self.place = @"";
            DLOG(@"exception");
        }
        @finally {
            self.checkinIconURL = @"";
            self.checkinName = @"";
            self.checkinMessage = @"";
        }
        
        User *user = [ControlObj getUserWithID:self.userID];
        if (user == nil) {
            NSLog(@"profile nil");
            user = [[User alloc] initWithDictinary:nil];
        }
        
        self.pic = user.pic;
        if (self.pic == nil || self.pic.length < 5) {
            self.pic = @"https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-frc1/283270_257757160902107_690367_n.jpg";
        }
        
        self.name = user.name;
        if (self.name == nil || self.name.length < 1) {
            self.name = @"Facebook User";
            DLOG(@"Facebook_User");
        }
        
        if ([self.name isEqualToString:@"Facebook User"]) {
            if (self.userID.length != 0) {
                [ControlObj addUser:self.userID];
            }
        }
        
        NSNumber *post_time = [NSNumber numberWithDouble:0.0];
        @try {
            post_time = [post objectForKey:@"created_time"];
            self.created_time = [post objectForKey:@"created_time"];
        }
        @catch (NSException *exception) {
            NSLog(@"created_time error");
            DLOG(@"exception");
            post_time = [NSNumber numberWithDouble:0.0];
        }
        @finally {
        }
        self.datetime = [Utility convertDateMessageFromUnixTime:[post_time doubleValue]];
        if (self.datetime == nil) {
            self.datetime = @"Recently Posted";
        }
        
        NSInteger pt = 0;
        @try {
            pt = [[post objectForKey:@"type"] integerValue];
        }
        @catch (NSException *exception) {
            pt = -1;
            DLOG(@"exception");
        }
        @finally {
        }
        self.postType = [NSNumber numberWithInteger:pt];
        
        NSDictionary *likes = [post objectForKey:@"likes"];
        if (likes !=  nil) {
            self.canLike = [likes objectForKey:@"can_like"];
            self.likedByUser = [likes objectForKey:@"user_likes"];
            self.likeCount = [likes objectForKey:@"count"];
        }
        
        NSDictionary *comments = [post objectForKey:@"comments"];
        if (nil != comments) {
            self.canComment = [comments objectForKey:@"can_post"];
            self.commentCount = [comments objectForKey:@"count"];
            self.comment_list = [comments objectForKey:@"comment_list"];
        }
        
        self.message = [post objectForKey:@"message"];
        self.message = [Utility showTextWithCode:self.message];
        
        //        self.message = [Utility convertTextToHTMLText:self.message];
        
        NSMutableDictionary *media_dict =  nil;
        
        switch (pt) {
            case typePost_App_or_Games_story:
            {
                NSLog(@"post typePost_App_or_Games_story");
                break;
            }
                
            case Likes_a_Link1:
            {
                NSLog(@"post Likes_a_Link1");
                break;
            }
                
            case typePost_Likes:
            {
                NSLog(@"post typePost_Likes");
                break;
            }
                
            case typePost_Comment_created:
            {
                NSLog(@"post typePost_Comment_created");
                break;
            }
                
                
            case typePost_Likes_a_photo_or_Link:
            {
                NSLog(@"post typePost_Likes_a_photo_or_Link");
                break;
            }
                
            case typePost_Tagged_photo:
            {
                NSLog(@"post typePost_Tagged_photo");
                break;
            }
                
            case typePost_Link_posted_shared_Photo_or_Link:
            {
                NSLog(@"post typePost_Link_posted_shared_Photo_or_Link");
                break;
            }
                
            case typePost_Checkin_to_a_place:
            {
                NSLog(@"typePost_Checkin_to_a_place");
                break;
            }
                
                
            default:
                break;
        }
        
        NSMutableDictionary *attach_dict = [post objectForKey:@"attachment"];
        if (attach_dict != nil) {
            self.attachType = [attach_dict objectForKey:@"fb_object_type"];
            self.attachCaption = [attach_dict objectForKey:@"caption"];
            if (self.message == nil || [self.message isEqualToString:@""]) {
                self.message = self.attachCaption;
            }
            self.attach_descriptionn = [attach_dict objectForKey:@"description"];
            self.attach_icon = [attach_dict objectForKey:@"icon"];
            self.attach_properties = [[attach_dict objectForKey:@"properties"] valueForKey:@"text"];
            self.attach_name = [attach_dict objectForKey:@"name"];
            self.fb_object_id = [attach_dict objectForKey:@"fb_object_id"];
            
            self.link_share_post = [attach_dict objectForKey:@"href"];
            
            // get sub name when name is Timeline Photo
            self.attach_property_photo = [attach_dict valueForKey:@"properties"];           
            if ([self.attach_property_photo count] > 0) {
                NSDictionary *dict_properties = [self.attach_property_photo objectAtIndex:0];
                self.text_name_property_timeline_photo = [dict_properties objectForKey:@"text"];
            }
            
            if (pt == typePost_Link_posted_shared_Photo_or_Link && self.attachType == nil) {
                NSLog(@"looking for non-photo and non-video sharing...");
                didShareALink = YES;
                self.linkURL = [attach_dict objectForKey:@"href"];
                
                if ([attach_dict objectForKey:@"message"] != nil) {
                    self.message = [attach_dict objectForKey:@"message"];
                    self.message = [Utility showTextWithCode:self.message];
                }
                
                NSLog(@"linkURL:%@",self.linkURL);
                NSLog(@"attachCaption:%@",self.attachCaption);
                NSLog(@"message A1:%@",self.message);
            }
            
            else if (pt == typePost_App_or_Games_story ||
                     pt == typePost_Link_posted_shared_Photo_or_Link ||
                     pt == typePost_Likes ||
                     pt == Likes_a_Link1 ) {
                didShareALink = YES;
            }
            
            else if (pt == typePost_Video_posted){
                NSLog(@"%@",post);
                NSArray *mediaArr = [attach_dict objectForKey:@"media"];
                
                if ([mediaArr count] > 0) {
                    NSDictionary *mediaDic = [mediaArr objectAtIndex:0];
                    NSDictionary *videoDic = [mediaDic objectForKey:@"video"];
                    if (videoDic != nil ) {
                        NSString *ptvURL = [videoDic objectForKey:@"src"];
                        self.linkURL = [videoDic objectForKey:@"source_url"];
                        self.postPhotoURL = ptvURL != nil ? ptvURL : [mediaDic objectForKey:@"src"];
                    }
                }
            }
            
            self.media_all_array = [attach_dict objectForKey:@"media"];
            if (self.media_all_array != nil ) {
                NSLog(@"media_all_array.count :%d",self.media_all_array.count);
                @try {
                    // thuy. get ra media cuoi cung.
                    media_dict = [self.media_all_array objectAtIndex:0];
                }
                @catch (NSException *exception) {
                    DLOG(@"exception");
                    self.media_all_array = [[NSMutableArray alloc] init];
                    NSLog(@"media_all_array exception:%@",exception);
                }
                @finally {
                }
            } else {
                NSLog(@"nil media_all_array");
                self.media_all_array = [[NSMutableArray alloc] init];
            }
        }
        
        if (media_dict != nil) {
            self.attachType = [media_dict objectForKey:@"type"];
            DLOG(@"\n\n Dung attach %@\n\n",self.attachType);
            if (didShareALink) {
                NSLog(@"media_dict.didShareALink");
                //self.attachType = [media_dict objectForKey:@"type"];
                self.postPhotoURL = [media_dict objectForKey:@"src"];
                self.nameLink = [attach_dict objectForKey:@"name"];
                if ([attach_dict objectForKey:@"href"] != nil) {
                    self.linkURL = [attach_dict objectForKey:@"href"];
                }
                
                if (self.postPhotoURL == nil || [self.postPhotoURL isEqualToString:@""]) {
                    self.postPhotoURL = [attach_dict objectForKey:@"href"];
                }
                
                NSLog(@"attachType:%@",self.attachType);
                NSLog(@"postPhotoURL:%@",self.postPhotoURL);
                
                if ([self.attachType isEqualToString:@"link"]) {
                    self.attachType = @"link";
                }
                else if ([self.linkURL rangeOfString:@"http://www.youtube.com/"].location != NSNotFound) {
                    //linkURL = [linkURL stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"embed/"];
                    self.isVideo = @"1";
                    self.attachType = @" ";
                }
                else {
                    //  fix auto load other web same as load (mp3.zing)
                    self.attachType = ([self.attachType isEqualToString:@"photo"]) ? self.attachType : @" ";
                }
            }
            
            NSMutableDictionary *photo_dict = [media_dict objectForKey:@"photo"];
            if (photo_dict != nil) {
                NSNumber *media_id = @0;
                @try {
                    media_id = [photo_dict objectForKey:@"fbid"];
                }
                @catch (NSException *exception) {
                    media_id = @0;
                    DLOG(@"exception");
                }
                @finally {
                }
                
                NSArray *images = [photo_dict objectForKey:@"images"];
                NSString *tmp_url = [[images objectAtIndex:0] objectForKey:@"src"];
                
                if (tmp_url == nil) {
                    tmp_url = [media_dict objectForKey:@"src"];
                }
                self.postPhotoURL = [Utility adjustPhotoURL:tmp_url];
                
                if (self.message == nil || [self.message isEqualToString:@""]) {
                }
            }
            
        }
        
        if (pt == typePost_Link_posted_shared_Photo_or_Link) {
            if (self.message == nil) {
                self.message = [NSString stringWithFormat:@"Shared a %@",self.attachType];
            }
            else {
                //              message = [NSString stringWithFormat:@"Shared a %@",attachType];
                NSLog(@"message A2: %@",self.message);
            }
            
        }
        self.permalink = [post objectForKey:@"permalink"];
        
        if ([self.postID isEqualToString:@"121734051198912_593235334048779"]) {
            DLOG(@"THUY MESSSAGE PER");
        }
        
        self.strHtml = [self getArticleHTMLForPost];
        DLOG(@"strHtml:\n %@",self.strHtml);
        typePostf = 0;
        
    }
    return self;
}

- (void)refreshHTML
{
    
}

- (PostObj*)initSubPost
{
    PostObj * subPost = [PostObj new];
    subPost.media_all_array = self.media_all_array;
    subPost.attachType = self.attachType;
    subPost.postPhotoURL = self.postPhotoURL;
    if ([subPost.attachType isEqualToString:@"link"])
    {
        if (subPost.media_all_array.count > 0)
        {
            NSDictionary *dict = [subPost.media_all_array objectAtIndex:0];
            subPost.name = [dict objectForKey:@"alt"];
            subPost.pic = [dict objectForKey:@"src"];
        }
        else
        {
            subPost.name = @"";
            subPost.pic = @"https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-frc1/283270_257757160902107_690367_n.jpg";
        }
        
        subPost.postType = [NSNumber numberWithInteger:46];
        subPost.postPhotoURL = @"";
    }
    
    else
    {
        DLOG(@"");
        subPost.fb_object_id = self.fb_object_id;
        NSArray* id_arr = [subPost.fb_object_id componentsSeparatedByString:@"_"];
        if ([id_arr count] > 1) {
            subPost.userID = [id_arr objectAtIndex:0];
        }
         //User *user = [ControlObj getUserWithID:self.userID];
        User *user = [ControlObj getUserWithID:subPost.userID];
        
        subPost.pic = user.pic;
        if (subPost.pic == nil || subPost.pic.length < 5) {
            subPost.pic = @"https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-frc1/283270_257757160902107_690367_n.jpg";
        }
        
        subPost.name = user.name;
        if (subPost.name == nil || subPost.name.length < 1) {
            subPost.name = @"Facebook User";
        }
        
        if ([subPost.name isEqualToString:@"Facebook User"]) {
            if (subPost.userID.length != 0) {
                [ControlObj addUser:subPost.userID];
            }
            else {
                subPost.name = @"";
            }
        }
        subPost.postType = [NSNumber numberWithInteger:80];
    }
    
    subPost.attach_descriptionn = self.attach_descriptionn;
    subPost.attachCaption = self.attachCaption;
    if (subPost.attachCaption.length == 0) {
        subPost.message = [Utility convertTextToHTMLText:subPost.attach_descriptionn];
        subPost.message = [Utility showTextWithCode:subPost.message];
    }
    else {
        subPost.message = [Utility convertTextToHTMLText:subPost.attachCaption];
        subPost.message = [Utility showTextWithCode:subPost.message];
        if ([subPost.message isEqualToString:[Utility convertTextToHTMLText:self.message]]) {
            self.message = @"";
        }
        DLOG(@"POST_MES %@",self.message);
        DLOG(@"SUB_POST_MES %@",subPost.message);
    }
    
    subPost.datetime = self.datetime;
    subPost.linkURL = self.linkURL;
    subPost.checkinName = self.checkinName;
    subPost.checkinIconURL = self.checkinIconURL;
    subPost.checkinMessage = self.checkinMessage;
    subPost.likeCount = self.likeCount;
    subPost.likedByUser = self.likedByUser;
    subPost.commentCount = self.commentCount;
    subPost.canLike = self.canLike;
    subPost.canComment = self.canComment;
    subPost.place = self.place;
    subPost.nameLink = self.nameLink;
    subPost.comment_list = self.comment_list;
    subPost.attach_icon = self.attach_icon;
    subPost.attach_name = self.attach_name;
    subPost.attach_properties = self.attach_properties;
    subPost.postID = self.postID;
    subPost.isVideo = self.isVideo;
    subPost.app_id = self.app_id;
    subPost.created_time = self.created_time;
    //
    
    //subPost.attach_property_photo = self.attach_property_photo;
    //
    subPost.text_name_property_timeline_photo = self.text_name_property_timeline_photo;    
    //
    subPost.share_info = [[NSMutableDictionary alloc] initWithDictionary:self.share_info];
    NSLog(@"referPostDictWithPost");
    if (subPost.share_info == nil) {
        NSLog(@"share_info empty!");
    }
    return subPost;
}

- (id)initWithDictinary:(NSDictionary*)post subPost:(NSDictionary*)subPost
{
    self = [self initWithDictinary:post];
    self.subPost = [self initWithDictinary:subPost];
    self.strHtml = [self getDoubleArticle];
    typePostf = 1;
    return self;
}

#pragma mark - getHTML

- (NSString*)getArticleHTMLForPost
{
    NSLog(@"getArticleHTMLForPost");
    
    NSString *article = @"";
    
    NSInteger postType = [self.postType integerValue];
    
    
    if (postType == typePost_Tagged_photo ||
        postType == typePost_Link_posted_shared_Photo_or_Link ||
        postType == typePost_Post_in_Group)
    {
        NSLog(@"post name:%@",self.name);
        
        article = [self getArticleSubpostWithSimplePost];
    }
    else {
        NSLog(@"not subpost");
        article = [article stringByAppendingString:[self partialArticleHTMLForPost:self occurTag:NO]];
    }
    
    return [NSString stringWithFormat:@"%@%@%@",
            [Utility getHeadConfigForPost],
            article,
            [Utility getEndConfigForPost]];
}

- (NSString*)getDoubleArticle
{
    NSString *article = @"";
    NSString* reason = [self getReason];
    NSString *subAttach = self.attachType;
    NSString *subUserid= self.subPost.userID;
    NSString *subUsername = self.subPost.name;
    
    NSString *message = self.message;
    message = [Utility convertTextWithSharedLink:message];
    NSInteger postType = [self.postType integerValue];
    if (postType == 60) {
        NSLog(@"\n\nToi o day\n\n ");
    }
    if ([self.userID isEqualToString:self.subPost.userID] &&
        [self.message isEqualToString:self.subPost.message]) {
        NSLog(@"fix #135");
        subAttach = @"comment on this.";
        
        if([reason isEqualToString:@"like this."])
        {
            //subAttach = @"like this.";
            subAttach = reason;
        }
        reason = @"";
        subUserid = @"";
        subUsername = @"";
        
        NSString *referHTML = [self partialArticleHTMLForPost:self.subPost occurTag:YES];
        article = [article stringByAppendingFormat:[Template templateArticleBeginTagged],
                   [NSString stringWithFormat:@"<div id=\"post_%@\">",self.postID],
                   self.postID,
                   [Template getIConExpand],
                   self.userID,
                   self.name,reason,subAttach,@"", subUserid,subUsername,//self.name,
                   self.subPost.datetime,
                   @"",
                   referHTML,
                   nil];
    }
    else {
        if (postType == 60) {
            NSLog(@"\n\nToi o day\n\n ");
        }
        
        NSString *referHTML = [self partialArticleHTMLForPost:self.subPost occurTag:YES];
        article = [article stringByAppendingFormat:[Template templateArticleBeginTagged],
                   [NSString stringWithFormat:@"<div id=\"post_%@\">",self.postID],
                   self.postID,
                   [Template getIConExpand],
                   self.userID,
                   self.name,reason,self.attachType,@"",subUserid,subUsername,//self.name,
                   self.subPost.datetime,
                   message,
                   referHTML,
                   nil];
    }
    
    article = [NSString stringWithFormat:@"%@%@%@%@",
               [Utility getHeadConfigForPost],
               article,
               [self getLikeCommentComponent:self.subPost],
               [Utility getEndConfigForPost]];
    return article;
}

- (NSString*)getArticleSubpostWithSimplePost
{
    NSString *article = @"";
    // NSString* reason = [self getReason];
    // NSString *subAttach = self.attachType;
    
    // check content of post
    NSInteger postType = [self.postType integerValue];
    if (postType == 60) {
        NSLog(@"\n\nToi o day\n\n ");
    }
    
    if ([self.name isEqualToString:self.nameLink])
    {
        NSLog(@"not subpost");
        return [article stringByAppendingString:[self partialArticleHTMLForPost:self occurTag:NO]];
    }
    
    else
    {
        DLOG(@"subpost");
        PostObj *subPost = [self initSubPost];
        self.subPost = subPost;
        NSLog(@"getArticleHTMLForPost VALID referPost");
        
        NSString *name = self.name;
        NSString *myName =@"";
        DLOG(@"POST %@ SUBPOST NAME %@",name,subPost.name);
        if ([subPost.name isEqualToString:@""]) {
            return [article stringByAppendingString:[self partialArticleHTMLForPost:self occurTag:NO]];
        }
        
        NSString* reason = [self getReason];
        DLOG(@"SUBPOST REASON %@",reason);
        
        if ([reason isEqualToString:@"shared"] || [reason isEqualToString:@" was tagged in"]) {
            myName = @"of";
        }
        
        NSString *message = [Utility convertTextToHTMLText:self.message];
        subPost.tmpMessage = message;
        DLOG(@"MESS DAI %d \tGOC %@",[message length],message);
        if (subPost.isContinueReading == 1)
        {
            message = subPost.tmpMessage;
        }
        else
        {
            message = [self FormatContinueReading:message withURL:subPost.postID];
        }
        DLOG(@"MESS DAI %d \t CONVERT %@",[message length],message);
        
        NSString *referHTML = [self partialArticleHTMLForPost:subPost occurTag:YES];
        
        //NSString *referHTML = [self partialArticleHTMLForPost:subPost occurTag:YES datetime:@" "];
        
        if ([self.attach_name length] > 0 && [self.text_name_property_timeline_photo length] > 0 ) {
            article = [article stringByAppendingFormat:[Template templateArticleBeginTagged],
                       [NSString stringWithFormat:@"<div id=\"post_%@\">",self.postID],
                       self.postID,
                       [Template getIConExpand],
                       self.userID,
                       name,reason,subPost.attachType,myName,subPost.userID,subPost.name,
                       // name,
                       self.subPost.datetime,
                       message,
                       referHTML,
                       nil];
            
        }
        else{
            article = [article stringByAppendingFormat:[Template templateArticleBeginTagged],
                       [NSString stringWithFormat:@"<div id=\"post_%@\">",self.postID],
                       self.postID,
                       [Template getIConExpand],
                       self.userID,
                       name,reason,subPost.attachType,myName,subPost.userID,subPost.name,
                       // name,
                       self.subPost.datetime,
                       message,
                       referHTML,
                       nil];
        }
        NSLog(@"\n\n SUBPOST ID %@\n\n",subPost.userID);
        
        return [NSString stringWithFormat:@"%@%@",article, [self getLikeCommentComponent:subPost]];
    }
    return article;
}

- (NSString*)getReason
{
    NSInteger postType = [self.postType integerValue];//[self.subPost.postType integerValue];
    NSString *reason = @"";
    
    switch (postType) {
        case 8:
            reason = @" is now friends with another user";
            break;
        case 56:
            reason = @" posted on the timeline of another user";
            break;
        case 65:
            //reason = @" was tagged in a photo";
            reason = @" was tagged in";
            break;
        case typePost_changed_profile_picture:
        {
            reason = @" changed profile picture";
            break;
        }
        case 80://typePost_Link_posted_shared_Photo_or_Link:
        {
            //            reason = [NSString stringWithFormat:@" Shared <a href=\"unlink/111\" style=\"text-decoration:none;\">%@</a>'s %@",@"Thuy Dao",[post objectForKey:@"attachType"]];
            //reason = [NSString stringWithFormat:@" shared a %@",self.attachType];
            reason = @"shared";
            break;
        }
        case 161:
            reason = @" likes a page";
            break;
        case 245:
        {
            NSString *cmt_type = @"";
            if (self.subPost != nil) {
                cmt_type = self.subPost.attachType;
            }
            if (nil == cmt_type || cmt_type.length < 2) cmt_type = @"post";
            reason = [NSString stringWithFormat:@" likes a %@",cmt_type];
            break;
        }
        case 247:
            //reason = @"Posted a photo";
            reason = @"posted";
            break;
        case 257:
        {
            NSString *cmt_type = @"";
            if (self.subPost != nil) {
                cmt_type = self.subPost.attachType ;
            }
            if (nil == cmt_type || cmt_type.length < 2) cmt_type = @"post";
            reason = [NSString stringWithFormat:@" Commented on a %@",cmt_type];
        }
            break;
        case 272:
            //reason = @" Posted a story";
            reason = @"posted";
            break;
        case 285:
            //reason = @" Checked in";
            reason = @" checked in";
            break;
        case typePost_Post_in_Group:
            //reason = @" Posted in a group";
            reason = @" posted in a group";
            break;
        case 373:
            //reason = @"Updated their cover photo";
            reason = @"updated cover photo";
            break;
        case 164:
            reason = @"like this.";
            break;
        default:
            reason = @" likes something";
            NSLog(@"--- DEFAULT POST ---");
            NSLog(@"default message for type:%d",postType);
            break;
    }
    if (reason == nil) {
        return @"";
    }
    return reason;
}

// set cho thang post with subpost
- (NSString*)partialArticleHTMLForPost:(PostObj*)post occurTag:(BOOL) occurTag datetime:(NSString *)timeCreated
{
    NSLog(@"partialArticleHTMLForPostdatetime");
    
    NSString *partial = @"";
    
    NSInteger postType = 0;
    postType = [post.postType integerValue];
    
    // check link and show link in message
    
    NSString* message = [Utility convertTextWithSharedLink:post.message];
    
    // TODO: ADD AN ADDITIONAL TEXT AREA AFTER PROFILE NAME FOR STUFF LIKE "was tagged in..." OR "likes..."
    NSString *div = @"";
    if (!occurTag) {
        div = [NSString stringWithFormat:@"<div id=\"post_%@\">",post.postID];
    }
    post.datetime = timeCreated;
    partial = [NSString stringWithFormat:[Template templateArticleBeginNormal],
               div,
               post.postID,
               [Template getIConExpand],
               post.postID,
               post.userID,
               post.pic,
               post.userID,
               post.name,
               post.datetime];//post.datetime
    
    // GO THROUGH AND GET DIFFERENT BODY TEMPLATES DEPENDING ON TYPE
    
    // PHOTOS //
    BOOL isArticleDone = NO;
    
    NSLog(@"post: \n:postType: %d\t message: %@ \t postPhotoURL:%@\n",postType,post.message,post.postPhotoURL);
    
    switch (postType) {
            
        case Likes_a_Link1:
        {
            NSLog(@"post Likes_a_Link1");
            isArticleDone = YES;
            partial = [self getPartialWithShareLink:post partial:partial occurTag:occurTag];
            break;
        }
            
        case typePost_Likes:
        {
            NSLog(@"post typePost_Likes");
            isArticleDone = YES;
            partial = [self getPartialWithShareLink:post partial:partial occurTag:occurTag];
            break;
        }
            
        case typePost_Comment_created:
        {
            NSLog(@"post typePost_Comment_created");
            break;
        }
            
        case typePost_Likes_a_photo_or_Link:
        {
            NSLog(@"post typePost_Likes_a_photo_or_Link");
            break;
        }
            
        case typePost_Status_update:
        {
            NSLog(@"post typePost_Status_update");
            if ([post.postPhotoURL length] > 1 ) {
                isArticleDone = YES;
                partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            }
            
            else
            {
                // check in
                if (post.place.length > 0) {
                    isArticleDone = YES;
                    [ControlObj addPlace:post.place];
                    partial = [self getPartialWithCheckin:post partial:partial occurTag:occurTag];
                }
            }
            break;
        }
            
        case typePost_Tagged_photo:
        {
            NSLog(@"post typePost_Tagged_photo");
            if ([post.postPhotoURL length] > 1 ) {
                isArticleDone = YES;
                partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            }
            break;
        }
            
        case typePost_Link_posted_shared_Photo_or_Link:
        {
            NSLog(@"post typePost_Link_posted_shared_Photo_or_Link");
            if ([post.postPhotoURL length] > 1 ) {
                isArticleDone = YES;
                DLOG(@"");
                if ([post.attachType isEqualToString:@"photo"] || [post.attachType isEqualToString:@"album"]) {
                    //share photo
                    DLOG(@"");
                    partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
                }
                
                else if([post.attachType isEqualToString:@"video"])
                {
                    DLOG(@"");
                    // share video
                    partial = [self getPartialWithVideo:post partial:partial occurTag:occurTag];
                }
                
                else {
                    DLOG(@"");
                    // share link
                    partial = [self getPartialWithShareLink:post partial:partial occurTag:occurTag];
                    
                }
            }
            break;
        }
            
        case typePost_Video_posted:
        {
            NSLog(@"post typePost_Video_posted");
            partial = [self getPartialWithVideo:post partial:partial occurTag:occurTag];
            isArticleDone = YES;
            break;
        }
            
        case typePost_Photos_posted:
        {
            NSLog(@"post typePost_Photos_posted");
            isArticleDone = YES;
            partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            break;
        }
            
        case updated_cover_photo:
        {
            NSLog(@"post updated_cover_photo");
            isArticleDone = YES;
            partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            break;
        }
            
        case typePost_Post_in_Group:
        {
            NSLog(@"post typePost_Post_in_Group");
            if ([post.postPhotoURL length] > 1) {
                isArticleDone = YES;
                partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            }
            
            break;
        }
            
        case typePost_changed_profile_picture:
        {
            NSLog(@"post typePost_changed_profile_picture");
            if ([post.postPhotoURL length] > 1) {
                isArticleDone = YES;
                partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            }
            
            break;
        }
            
        case typePost_App_or_Games_story:
        {
            //thuy fix
            NSLog(@"post typePost_App_or_Games_story");
            isArticleDone = YES;
            partial = [self getPartialWithShareLink:post partial:partial occurTag:occurTag];
            
            break;
        }
            
        case typePost_Checkin_to_a_place:
        {
            NSLog(@"post typePost_Checkin_to_a_place");
            isArticleDone = YES;
            partial = [partial stringByAppendingFormat:[Template templateArticleBodyCheckin],
                       message,
                       post.checkinName,
                       post.checkinIconURL,
                       post.checkinName,
                       post.checkinMessage];
            break;
        }
        default:
            break;
    }
    
    if (isArticleDone == NO) {
        NSLog(@"pre-checkin");
        isArticleDone = YES;
        
        if (message == nil && post.checkinName == nil) {
            DLOG(@"fix empty a post");
            return @"";
        }
        
        partial = [partial stringByAppendingFormat:[Template templateArticleBodyCheckin],
                   message,
                   post.checkinName,
                   post.checkinIconURL,
                   post.checkinName,
                   post.checkinMessage];
        NSLog(@"post-checkin");
    }
    
    // NORMAL STATUS //
    if (isArticleDone == NO) {
        NSLog(@"pre-status");
        
        if (message == nil || [message isEqualToString:@""]) {
            DLOG(@"fix empty a post");
            return @"";
        }
        
        partial = [partial stringByAppendingFormat:[Template templateArticleBodyStatus],message];
        NSLog(@"post-status");
    }
    
    if (!occurTag) {
        partial = [NSString stringWithFormat:@"%@%@",partial,[self getLikeCommentComponent:post]];
        //        [self sendImageName];
    }
    
    NSLog(@"%@",partial);
    if (occurTag) {
        partial = [partial stringByReplacingOccurrencesOfString:@"article" withString:@"div"];
        partial = [partial stringByReplacingOccurrencesOfString:@"<div class=\"fullwidth carded _51s story async_like voice acw abb\" id=\"u_mcwu0y_1\">" withString:@"<div class=\"_51s story async_like voice acw abb\" style=\"background-color:white; width:255px;\"> "];
        partial = [NSString stringWithFormat:@"%@</div>",partial];
    }
    return partial;
}


- (NSString*)partialArticleHTMLForPost:(PostObj*)post occurTag:(BOOL) occurTag
{
    NSLog(@"partialArticleHTMLForPost");
    NSString *partial = @"";
    
    NSString *reaseon = [self getReason];
    
    NSInteger postType = 0;
    postType = [post.postType integerValue];
    DLOG(@"REASON %@",reaseon);
    // check link and show link in message
    
    NSString* message = [Utility convertTextWithSharedLink:post.message];
    post.tmpMessage = message;
    DLOG(@"MESS DAI %d \tGOC %@",[message length],message);
    if (post.isContinueReading == 1)
    {
        message = post.tmpMessage;
    }
    else
    {
        message = [self FormatContinueReading:message withURL:post.postID];
    }
    DLOG(@"MESS DAI %d \t CONVERT %@",[message length],message);

    
    // TODO: ADD AN ADDITIONAL TEXT AREA AFTER PROFILE NAME FOR STUFF LIKE "was tagged in..." OR "likes..."
    NSString *div = @"";
    if (!occurTag) {
        div = [NSString stringWithFormat:@"<div id=\"post_%@\">",post.postID];
    }
    //ap dung cho truong hop attach name co gia tri va properties co thuoc tinh text co gia tri.
    // text thuoc properties la ten cua user cua sub post .
    
    if ([post.postType integerValue] == 60 || [post.postType integerValue] == 373) {
        if ([self.attach_name length] > 0 && [self.text_name_property_timeline_photo length] > 0 ) {
            partial = [NSString stringWithFormat:[Template templateArticleBeginChangeProfile],
                       div,
                       post.postID,
                       [Template getIConExpand],
                       post.postID,
                       post.userID,
                       post.pic,
                       post.userID,
                       post.name,reaseon,
                       post.datetime];
        }
        else
        {
            partial = [NSString stringWithFormat:[Template templateArticleBeginChangeProfile],
                       div,
                       post.postID,
                       [Template getIConExpand],
                       post.postID,
                       post.userID,
                       post.pic,
                       post.userID,
                       post.name,reaseon,
                       post.datetime];
            
        }

    }
    else
    {
        if ([self.attach_name length] > 0 && [self.text_name_property_timeline_photo length] > 0 ) {
            partial = [NSString stringWithFormat:[Template templateArticleBeginNormal],
                       div,
                       post.postID,
                       [Template getIConExpand],
                       post.postID,
                       post.userID,
                       post.pic,
                       post.userID,
                       post.name,
                       post.datetime];
        }
        else
        {
            partial = [NSString stringWithFormat:[Template templateArticleBeginNormal],
                       div,
                       post.postID,
                       [Template getIConExpand],
                       post.postID,
                       post.userID,
                       post.pic,
                       post.userID,
                       post.name,
                       post.datetime];
            
        }

    }
    
    // GO THROUGH AND GET DIFFERENT BODY TEMPLATES DEPENDING ON TYPE
    
    // PHOTOS //
    BOOL isArticleDone = NO;
    
    NSLog(@"post: \n:postType: %d\t message: %@ \t postPhotoURL:%@\n",postType,post.message,post.postPhotoURL);
    
    switch (postType) {
            
        case Likes_a_Link1:
        {
            NSLog(@"post Likes_a_Link1");
            isArticleDone = YES;
            partial = [self getPartialWithShareLink:post partial:partial occurTag:occurTag];
            break;
        }
            
        case typePost_Likes:
        {
            NSLog(@"post typePost_Likes");
            isArticleDone = YES;
            partial = [self getPartialWithShareLink:post partial:partial occurTag:occurTag];
            break;
        }
            
        case typePost_Comment_created:
        {
            NSLog(@"post typePost_Comment_created");
            break;
        }
            
        case typePost_Likes_a_photo_or_Link:
        {
            NSLog(@"post typePost_Likes_a_photo_or_Link");
            break;
        }
            
        case typePost_Status_update:
        {
            NSLog(@"post typePost_Status_update");
            if ([post.postPhotoURL length] > 1 ) {
                isArticleDone = YES;
                partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            }
            
            else
            {
                // check in
                if (post.place.length > 0) {
                    isArticleDone = YES;
                    [ControlObj addPlace:post.place];
                    partial = [self getPartialWithCheckin:post partial:partial occurTag:occurTag];
                }
            }
            break;
        }
            
        case typePost_Tagged_photo:
        {
            NSLog(@"post typePost_Tagged_photo");
            if ([post.postPhotoURL length] > 1 ) {
                isArticleDone = YES;
                partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            }
            break;
        }
            
        case typePost_Link_posted_shared_Photo_or_Link:
        {
            NSLog(@"post typePost_Link_posted_shared_Photo_or_Link");
            if ([post.postPhotoURL length] > 1 ) {
                isArticleDone = YES;
                
                if ([post.attachType isEqualToString:@"photo"] || [post.attachType isEqualToString:@"album"]) {
                    //share photo
                    partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
                }
                
                else if([post.attachType isEqualToString:@"video"])
                {
                    // share video
                    partial = [self getPartialWithVideo:post partial:partial occurTag:occurTag];
                }
                
                else {
                    // share link
                    partial = [self getPartialWithShareLink:post partial:partial occurTag:occurTag];
                    
                }
            }
                       
            break;
        }
            
        case typePost_Video_posted:
        {
            NSLog(@"post typePost_Video_posted");
            partial = [self getPartialWithVideo:post partial:partial occurTag:occurTag];
            isArticleDone = YES;
            break;
        }
            
        case typePost_Photos_posted:
        {
            NSLog(@"post typePost_Photos_posted");
            isArticleDone = YES;
            partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            break;
        }
            
        case updated_cover_photo:
        {
            NSLog(@"post updated_cover_photo");
            isArticleDone = YES;
            partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            break;
        }
            
        case typePost_Post_in_Group:
        {
            NSLog(@"post typePost_Post_in_Group");
            if ([post.postPhotoURL length] > 1) {
                isArticleDone = YES;
                partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            }
            
            break;
        }
            
        case typePost_changed_profile_picture:
        {
            NSLog(@"post typePost_changed_profile_picture");
            if ([post.postPhotoURL length] > 1) {
                isArticleDone = YES;
                partial = [self getPartialWithPhoto:post partial:partial occurTag:occurTag];
            }
            
            break;
        }
            
        case typePost_App_or_Games_story:
        {
            //thuy fix
            NSLog(@"post typePost_App_or_Games_story");
            isArticleDone = YES;
            partial = [self getPartialWithShareLink:post partial:partial occurTag:occurTag];
            
            
            break;
        }
            
        case typePost_Checkin_to_a_place:
        {
            NSLog(@"post typePost_Checkin_to_a_place");
            isArticleDone = YES;
            partial = [partial stringByAppendingFormat:[Template templateArticleBodyCheckin],
                       message,
                       post.checkinName,
                       post.checkinIconURL,
                       post.checkinName,
                       post.checkinMessage];
            break;
        }
        default:
            break;
    }
    
    if (isArticleDone == NO) {
        NSLog(@"pre-checkin");
        isArticleDone = YES;
        
        if (message == nil && post.checkinName == nil) {
            DLOG(@"fix empty a post");
            return @"";
        }
        
        partial = [partial stringByAppendingFormat:[Template templateArticleBodyCheckin],
                   message,
                   post.checkinName,
                   post.checkinIconURL,
                   post.checkinName,
                   post.checkinMessage];
        NSLog(@"post-checkin");
    }
    
    // NORMAL STATUS //
    if (isArticleDone == NO) {
        NSLog(@"pre-status");
        
        if (message == nil || [message isEqualToString:@""]) {
            DLOG(@"fix empty a post");
            return @"";
        }
        
        partial = [partial stringByAppendingFormat:[Template templateArticleBodyStatus],message];
        NSLog(@"post-status");
    }
    
    if (!occurTag) {
        partial = [NSString stringWithFormat:@"%@%@",partial,[self getLikeCommentComponent:post]];
        //        [self sendImageName];
    }
    
    NSLog(@"%@",partial);
    if (occurTag) {
        partial = [partial stringByReplacingOccurrencesOfString:@"article" withString:@"div"];
        partial = [partial stringByReplacingOccurrencesOfString:@"<div class=\"fullwidth carded _51s story async_like voice acw abb\" id=\"u_mcwu0y_1\">" withString:@"<div class=\"_51s story async_like voice acw abb\" style=\"background-color:white; width:255px;\"> "];
        partial = [NSString stringWithFormat:@"%@</div>",partial];
    }
    return partial;
}

- (NSString*)getPartialWithCheckin:(PostObj*) post partial:(NSString*)partial occurTag:(BOOL)occurTag
{
    // check link and show link in message
    
    NSString* message = [Utility convertTextWithSharedLink:post.message];
    post.tmpMessage = message;
    DLOG(@"MESS DAI %d \tGOC %@",[message length],message);
    if (post.isContinueReading == 1)
    {
        message = post.tmpMessage;
    }
    else
    {
        message = [self FormatContinueReading:message withURL:post.postID];
    }
    DLOG(@"MESS DAI %d \t CONVERT %@",[message length],message);

    NSString* place = [NSString stringWithFormat:@"%@",post.place];
    
    if (occurTag) {
        return [partial stringByAppendingFormat:[Template templateArticleBodyCheckin],
                message,
                place,
                @"",
                place,
                @"margin-left: 10px;",
                @"width: 260px;"];
    }
    else {
        return [partial stringByAppendingFormat:[Template templateArticleBodyCheckin],
                message,
                place,
                @"",
                place,
                @"margin-left: 10px;",
                @"width: 290px;"];
    }
    
}

- (NSString*) FormatContinueReading:(NSString*)strText withURL:(NSString*)url
{
    NSString *str=strText;
    
    //CGRect rect=CGRectMake(51, 16, 25r7, 0);
    
    CGSize size1=[str sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(257, 3000) lineBreakMode:NSLineBreakByTruncatingTail];
    int lines=(size1.height/14);
    DLOG(@"LINE CUT %d",lines);
        if(lines > 5 && [str length] > size1.width)
        {
            str = [str stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
            str = [str substringToIndex:size1.width - 1];           
            str = [str stringByAppendingString:[NSString stringWithFormat:@"...<a href=\"http://null/readmore/%@\">Continue Reading</a>",url]];
            DLOG(@"LINE 3 %@",str);
        }
    
        //self.lblQuestion.lineBreakMode=NSLineBreakByTruncatingHead;
    return str;
}

- (NSString*)getPartialWithPhoto:(PostObj*) post partial:(NSString*)partial occurTag:(BOOL)occurTag
{
    // check link and show link in message
    NSString* message = [Utility convertTextWithSharedLink:post.message];
    post.tmpMessage = message;
    DLOG(@"MESS DAI %d \tGOC %@",[message length],message);
    if (post.isContinueReading == 1)
    {
        message = post.tmpMessage;
    }
    else
    {
        message = [self FormatContinueReading:message withURL:post.postID];
    }
    DLOG(@"MESS DAI %d \t CONVERT %@",[message length],message);
    
    DLOG(@" getPartialWithPhoto ");
    if ([post.postID isEqualToString:@"XXX"]) {
        NSLog(@"%@",post);
    }
    
    if (occurTag) {
        return [partial stringByAppendingFormat:[Template templateArticleBodyPhoto],
                message,
                post.postID,
                post.postPhotoURL,
                @"margin-left: 10px;",
                @"width: 260px;"]; // 260px
    }
    else {
        
        return [partial stringByAppendingFormat:[Template templateArticleBodyPhoto],
                message,
                post.postID,
                post.postPhotoURL,
                @"margin-left: 10px;",
                @"width: 290px;"]; // 290px
    }
    
}

- (NSString*)getPartialWithVideo:(PostObj*) post partial:(NSString*)partial occurTag:(BOOL)occurTag
{
    // check link and show link in message
    
    NSString* message = [Utility convertTextWithSharedLink:post.message];
    
    if (occurTag) {
        return [partial stringByAppendingFormat:[Template templateArticleBodyVideo],
                message,
                @"margin-left:-5px",
                @"width: 260px",
                post.linkURL,
                post.linkURL,
                post.linkURL,
                post.linkURL,
                @"width: 260px",
                post.linkURL,
                @"width: 260px"];
    }
    else {
        
        return [partial stringByAppendingFormat:[Template templateArticleBodyVideo],
                message,
                @"margin-left:-5px",
                @"width: 290px",
                post.linkURL,
                post.linkURL,
                post.linkURL,
                post.linkURL,
                @"width: 290px",
                post.linkURL,
                @"width: 290px"];
    }
}


- (NSString*)getPartialWithShareLink:(PostObj*) post partial:(NSString*)partial occurTag:(BOOL)occurTag
{
    if ([post.linkURL rangeOfString:@"http://www.youtube.com/"].location != NSNotFound ||
        [post.attachCaption rangeOfString:@"youtube.com"].location != NSNotFound ||
        [post.attach_descriptionn rangeOfString:@"youtube.com"].location != NSNotFound) {
        self.isVideo = @"1";
    }
    
    
    NSString *isVideo = post.isVideo;
    DLOG(@"isVideo:\n %@",isVideo);
    NSString* message = [Utility convertTextWithSharedLink:post.message];
    
    NSString *videoIcon = @"";
    if ([isVideo isEqualToString:@"1"]) {
        videoIcon = [NSString stringWithFormat:@"<img id=\"playButton\" src=\"%@\" width=\"24\" height=\"24\"/>",[Template getiIconVideo]];
    }
    
    if ([self.linkURL isEqualToString:@""]) {
        return partial = [partial stringByAppendingFormat:[Template templateArticleBodyStatus],message];
    }
    
    return  [partial stringByAppendingFormat:[Template templateArticleBeginSharedLink],
             message,
             post.linkURL,
             post.postPhotoURL,
             videoIcon,
             [NSString stringWithFormat:@"<b>%@</b><br>%@",post.nameLink,post.attach_descriptionn]];
}

- (NSString*) getLikeCommentComponent:(PostObj*)post
{
    NSString* partial = @"";
    
    NSInteger hasLikeComment = 0;
    
    NSNumber *likeCount = @0;
    likeCount = post.likeCount;
    NSNumber *commentCount = @0;
    commentCount = post.commentCount;
    
    if ([likeCount integerValue] > 0) {
        hasLikeComment += 1;
    }
    
    if ([commentCount integerValue] > 0) {
        hasLikeComment += 2;
    }
    
    NSInteger isShared;
    NSInteger isLike = ([post.likedByUser integerValue] == 1) ? 0 : 1;
    NSDictionary *share_info = post.share_info;
    isShared = [[share_info objectForKey:@"can_share"] integerValue];
    post.isShared = isShared;
    NSLog(@"can_share: %d",isShared);
    NSString *postID = post.postID;
    
    switch (hasLikeComment) {
        case 0:
            partial = [partial stringByAppendingFormat:[Template templateArticleEnd2],
                       postID,
                       [Utility getArticleLike:postID isLike:isLike],
                       postID,
                       [Utility getArticleShare:postID isShared:isShared]];
            break;
        case 1:
            partial = [partial stringByAppendingFormat:[Template templateArticleEnd1],
                       postID,
                       [Utility getArticleLike:postID isLike:isLike],
                       postID,
                       [Utility getArticleShare:postID isShared:isShared],
                       [Template getIconLike],
                       [FacebookUtils ConverCount:[NSString stringWithFormat:@"%@",likeCount]]];
            
            break;
            
        case 2:
            partial = [partial stringByAppendingFormat:[Template templateArticleEnd1],
                       postID,
                       [Utility getArticleLike:postID isLike:isLike],
                       postID,
                       [Utility getArticleShare:postID isShared:isShared],
                       [Template getIconComment],
                       [FacebookUtils ConverCount:[NSString stringWithFormat:@"%@",commentCount]]];
            
            break;
        default:
            partial = [partial stringByAppendingFormat:[Template templateArticleEnd],
                       postID,
                       [Utility getArticleLike:postID isLike:isLike],
                       postID,
                       [Utility getArticleShare:postID isShared:isShared],
                       [Template getIconLike],
                       [FacebookUtils ConverCount:[NSString stringWithFormat:@"%@",likeCount]],
                       [Template getIconComment],
                       [FacebookUtils ConverCount:[NSString stringWithFormat:@"%@",commentCount]]];
            break;
    }
    self.rangeLikeComment = @"";
    self.rangeLikeComment = [NSString stringWithFormat:@"%@",partial];
    //    self.rangeLikeComment = [self.rangeLikeComment stringByReplacingOccurrencesOfString:@"</section></div></div>" withString:@"</section></div>"];
    return  self.rangeLikeComment;
}

- (void)liked
{
    self.likedByUser = [NSNumber numberWithInteger:1];
    self.likeCount = [NSNumber numberWithInteger:[self.likeCount integerValue] + 1];
    if ([self.likeCount integerValue] == 0) {
        self.likeCount = @1;
    }
    self.strHtml = [self getArticleHTMLForPost];
}
- (void)unliked
{
    self.likedByUser = [NSNumber numberWithInteger:0];
    self.likeCount = [NSNumber numberWithInteger:[self.likeCount integerValue] - 1];
    self.strHtml = [self getArticleHTMLForPost];
}

- (void)resetName
{
    User *user = [ControlObj getUserWithID:self.userID];
    if (user == nil) {
        NSLog(@"profile nil");
        user = [[User alloc] initWithDictinary:nil];
    }
    
    self.pic = user.pic;
    if (self.pic == nil || self.pic.length < 5) {
        self.pic = @"https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-frc1/283270_257757160902107_690367_n.jpg";
    }
    
    self.name = user.name;
    if (self.name == nil || self.name.length < 1) {
        self.name = @"Facebook User";
        DLOG(@"Facebook_User");
    }
    
    if ([self.name isEqualToString:@"Facebook User"]) {
        if (self.userID.length != 0) {
            [ControlObj addUser:self.userID];
        }
    }
}

- (NSString*)resetHTML
{
    
    if (typePostf == 0) {
        self.strHtml = [self getArticleHTMLForPost];
    }
    else {
        self.strHtml = [self getDoubleArticle];
    }
    return self.strHtml;
}

@end
