//
//  PostUploaderObj.h
//  AGKGlobal
//
//  Created by Dung on 3/1/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostUploaderObj : NSObject

@property (nonatomic,strong) NSString *pul_user_id;
@property (nonatomic,strong) NSString *pul_post_id;
@property (nonatomic,strong) NSString *pul_media_file;
@property (nonatomic,strong) NSString *pul_social_type;
@property (nonatomic,strong) NSString *pul_status;
@property (nonatomic,strong) NSString *pul_post_type;
@property (nonatomic,strong) NSString *pul_post_date;
@property (nonatomic,assign) NSInteger pul_is_posted;

@property (nonatomic,strong) NSMutableDictionary *pul_dict_privacy;
@property (nonatomic,strong) NSMutableDictionary *pul_dict_data;

//-- post status
@property (nonatomic,strong) NSString *pul_message;
@property (nonatomic,strong) NSString *pul_privacy;
//--post checkin
@property (nonatomic,strong) NSString *pul_checkin;

- (id)initWithDictinary:(NSDictionary*)post;


@end
