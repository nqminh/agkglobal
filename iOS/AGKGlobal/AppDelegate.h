//
//  AppDelegate.h
//  AGKGlobal
//
//  Created by Jeff Jolley on 5/28/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookUtils.h"
#import "TwitterViewController.h"
#import "QMessageViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "Obj.h"
#import "BaseNavigationController.h"
#import "VMUsers.h"


@class SignInViewController;
@class BuddyViewController;
@class BaseNavigationController;

#define isIphone5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>
{
    BOOL                    twitterAccountFound;
    BOOL                    connectedToTwitter;
    BOOL                    willHideWarning;
    BOOL                    justSetLocalNotification;
    BOOL                    isNewTweet;
    BOOL                    isSuccess;
    BOOL                    isOpenTweetLink;
    BOOL                    isMedia;
    BOOL                    isUrls;
    BOOL                    isConnecting;
    BOOL                    isPresentText;
    BOOL                    isNetworkNotFound;
    BOOL                    isLoginSuccessfully;
    BOOL                    isTerms;
    BOOL                    isLogOut;
    BOOL                    isQuitApp;
    
    NSInteger               actionSheetMode;
    ACAccountStore          *_accountStore;
    ACAccountType           *_twitterType;
    NSArray                 *twitterAccounts;
    ACAccount               *selectedTwitterAccount;
    UIImageView             *splashView;
    UIActivityIndicatorView *splashSpinner;
}

@property (strong, nonatomic) UIWindow                *window;
@property (strong, nonatomic) UITabBarController      *tabBarController;
@property (nonatomic, retain) ACAccountStore          *_accountStore;
@property (nonatomic, retain) ACAccountType           *_twitterType;
@property (nonatomic, retain) NSArray                 *twitterAccounts;
@property (nonatomic, retain) ACAccount               *selectedTwitterAccount;
@property (nonatomic, retain) UIImageView             *splashView;
@property (nonatomic, retain) UIActivityIndicatorView *splashSpinner;
@property (nonatomic, retain) UIActionSheet           *actionSheet;
@property (nonatomic, assign) NSTimeInterval          timer;
@property (nonatomic, assign) UIBackgroundTaskIdentifier *bgTask;


@property (nonatomic, retain) UILocalNotification     *localNotification;

@property (assign)            BOOL                    isNewTweet;
@property (assign)            BOOL                    isNewReply;
@property (assign)            BOOL                    isSuccess;
@property (assign)            BOOL                    isOpenTweetLink;
@property (assign)            BOOL                    isMedia;
@property (assign)            BOOL                    isUrls;
@property (assign)            BOOL                    isConnecting;
@property (assign)            BOOL                    isPresentText;
@property (assign)            BOOL                    isNetworkNotFound;
@property (assign)            BOOL                    isLoginSuccessfully;
@property (assign)            BOOL                    isTerms;
@property (assign)            BOOL                    isLogOut;
@property (assign)            BOOL                    isQuitApp;
@property (assign)            BOOL                    willForceSend;
@property (assign)            BOOL                    isShowLink;
@property (assign)            BOOL                    isShowText;

@property (nonatomic, retain) UIImageView             *imgTabBar;
@property (nonatomic, retain) UIImageView             *imgPost;
@property (nonatomic, retain) UIImageView             *imgTweet;
@property (nonatomic, retain) UIImageView             *imgText;
@property (nonatomic, retain) UIImageView             *imgSetting;
@property (nonatomic, retain) UIImageView             *imgBuddy;
@property (nonatomic, retain) UIImageView             *imgSignin;

@property (nonatomic,strong) NSMutableDictionary *nameDict;
@property (nonatomic,strong) NSMutableDictionary *userDict;

@property (nonatomic,strong) NSString * buddy_id_update;


+ (AppDelegate*)shared;

+ (void)checkACC;

//--logout twitter
+ (void)logoutTwitter;

//--switch twitter accounts
+ (void)switchTwitterAccounts;

//--startup after login/register successfully
+ (void)startUp;

- (void)getTwitterAccounts;
- (BOOL)isTwitterAccountFound;
- (void)doSelectTwitterAccount;
- (BOOL)isConnectedToTwitter;
- (void)setIsConnectedToTwitter:(BOOL)connected;
- (BOOL)checkForPreviousTwitterAccount;
- (void)setTwitterAccountToDefaults:(NSString *)username;
- (void)displayTwitterComposerSheet:(NSString *)recipient body:(NSString *)body abId:(NSNumber *)abId imagePath:(NSString *)imagePath view:(id)view title:(NSString *)title;

-(ACAccountStore*)accountStore;
-(ACAccountType*)twitterType;

+(NSString *) convertStringFromString :(NSString *) stringFormat;
+(NSString *) getFileNameFromURLString:(NSString *)urlStr;

@property (nonatomic, retain) SignInViewController *viewController;
//@property (nonatomic, retain) BaseNavigationController *viewController;


@end
