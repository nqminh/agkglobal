//
//  CountDownViewController.h
//  BeSafe
//
//  Created by The Rand's on 7/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import "SMSComposerSheetViewController.h"
#import "SendTwitterMessageViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "TwitterViewController.h"
#import "SendFacebookMessageViewController.h"
#import "SendFacebookImageViewController.h"
#import "FacebookCommentViewController.h"
#import "DetailsTweetWebViewController.h"
#import "SendFacebookImageViewController.h"
#import "FacebookWebViewController.h"
#import "ImageViewController.h"
#import "CommentViewController.h"
#import "ShareViewController.h"
#import "SendFacebookMessageViewController.h"
#import "SendFacebookCheckinViewController.h" 
#import "BuddyViewController.h"

@class SendFacebookImageViewController;
@class SendFacebookMessageViewController;
@class FacebookWebViewController;
@class ImageViewController;
@class CommentViewController;
@class ShareViewController;
@class SendFacebookCheckinViewController;
@class DetailsTweetWebViewController;
@class BuddyViewController;


@protocol countdowntSending <NSObject>
-(void) showKeyboard;
@end


@interface CountDownViewController : UIViewController <UIAlertViewDelegate>
{
    NSTimer                             *timer;
    NSNumber                            *abId;
    NSInteger                           timeSec;
    NSInteger                           sendType;
    NSInteger                           alertType;
    NSInteger                           banned_flag;
    BOOL                                isPlayingSound;
    BOOL                                isImageLoaded;
    
    AVAudioPlayer *soundEffect;
    BOOL isProfileFBPost;
}

@property (nonatomic,retain) SMSComposerSheetViewController    *delegate;
@property (nonatomic,retain) SendTwitterMessageViewController  *twitterDelegate;
@property (nonatomic,retain) TwitterViewController             *retwitterDelegate;
@property (nonatomic,retain) SendFacebookImageViewController   *facebookImageDelegate;
@property (nonatomic,retain) FacebookCommentViewController     *facebookCommentDelegate;
@property (nonatomic,retain) DetailsTweetWebViewController     *detailsTweetDelegate;
@property (nonatomic,retain) FacebookWebViewController         *facebookwebDelegate;
@property (nonatomic,retain) ImageViewController               *ImageViewDelegate;
@property (nonatomic,retain) CommentViewController             *commentViewDelegate;
@property (nonatomic,retain) ShareViewController               *shareViewDelegate;
@property (nonatomic,retain) SendFacebookMessageViewController *sendMessageDelegate;
@property (nonatomic,retain) SendFacebookCheckinViewController *sendCheckinDelegate;
@property (nonatomic,retain) BuddyViewController               *buddyDelegate;

@property (nonatomic,retain) UIImage                           *fbImage;

@property (nonatomic,retain) IBOutlet UIImageView              *personImageView;
@property (nonatomic,retain) IBOutlet UIImageView              *bgImageView;
@property (nonatomic,retain) IBOutlet UIButton                 *cancelButton;
@property (nonatomic,retain) IBOutlet UILabel                  *recipientLabel;
@property (nonatomic,retain) IBOutlet UILabel                  *messageLabel;
@property (nonatomic,retain) IBOutlet UILabel                  *countDownLabel;
@property (nonatomic,retain) IBOutlet UILabel                  *lblSeconds;
@property (nonatomic,retain) NSString                          *recipientText;
@property (nonatomic,retain) NSString                          *messageText;
@property (nonatomic,retain) NSString                          *imagePath;
@property (nonatomic,retain) NSTimer                           *timer;
@property (nonatomic,retain) NSNumber                          *abId;
@property (nonatomic) int                                      sendType;
//
@property (nonatomic,retain) UIImage                           *fbImage_postPhoto;

@property (assign, nonatomic) id <countdowntSending> delegate_countdown;

@property (nonatomic,strong) NSArray  *_bannedWords;
@property (nonatomic,strong) NSArray  *_bannedAddresses;
@property (nonatomic,strong) NSString *bannedList;

//--index row selected buddy
@property (nonatomic,assign) NSInteger indexRowSelected;

- (IBAction)closeWindow;
- (void)stopTimer;
- (void)loadImages;
- (void)animationFinished:(NSString *)animationID finished:(BOOL)finished context:(void *)context;

- (NSArray*) bannedWords;
- (NSArray*) bannedAddresses;



@end
