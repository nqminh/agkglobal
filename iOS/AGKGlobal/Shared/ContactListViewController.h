//
//  ContactListViewController.h
//  BeSafe
//
//  Created by The Rand's on 7/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "SMSComposerSheetViewController.h"

@interface ContactListViewController : UITableViewController <UIActionSheetDelegate, MFMessageComposeViewControllerDelegate> {

}

@property (nonatomic, retain) NSMutableArray        *contacts;
@property (nonatomic, retain) NSMutableArray        *allContacts;
@property (nonatomic, retain) NSArray               *alphabet;
@property (nonatomic, retain) NSMutableArray        *asContactArray;
@property (nonatomic, retain) NSString              *message;
@property (nonatomic, retain) SMSComposerSheetViewController *smsController;
@property (nonatomic, retain) UIButton              *otherSendButton;

- (void)loadAddressBook;
- (void)openActionSheet:(NSIndexPath *)indexPath;
//- (void)displayTwitterComposerSheet:(NSString *)recipient body:(NSString *)body abId:(NSNumber *)abId;
- (void)displaySMSComposerSheet:(NSString *)recipient body:(NSString *)body abId:(id)abId;
@end
