//
//  CountDownViewController.m
//  BeSafe
//
//  Created by The Rand's on 7/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CountDownViewController.h"
#import "UIImageView+Cached.h"
#import "Constants.h"

#define ALERT_TYPE_BANNED   1
#define ALERT_TYPE_NORMAL   2

#define PIM_ORIGIN_X        95
#define PIM_ORIGIN_Y        5
#define PIM_SIZE_WIDTH      140
#define PIM_SIZE_HEIGHT     140

@implementation CountDownViewController

@synthesize retwitterDelegate,facebookCommentDelegate,facebookImageDelegate,fbImage, detailsTweetDelegate,facebookwebDelegate,twitterDelegate,delegate,commentViewDelegate;
@synthesize personImageView, cancelButton, recipientLabel, messageLabel, recipientText, lblSeconds;
@synthesize countDownLabel, messageText, timer, abId;
@synthesize sendType, bgImageView, imagePath;
@synthesize _bannedWords,_bannedAddresses;
@synthesize fbImage_postPhoto;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //--set label font
    [self setLabelFont];
    
    // set frame cho image co kich thuoc nho
    isImageLoaded = YES;
    alertType = 0;
    [self.cancelButton setTitle:@"Sending" forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
    self.messageLabel.text = self.messageText;
    NSString *cancelButonText = @"Stop Sending";
    switch (sendType) {
        case FACEBOOK_POSTING_TYPE:
        case FACEBOOK_IMAGE_TYPE:
        case FACEBOOK_COMMENT_TYPE:
        case FBcomment:
        case FBPost:
            cancelButonText = @"Stop Posting";
            self.recipientLabel.text = @"Post to wall:";
            break;
        case FBPostBuddy:
            cancelButonText = @"Sending...";
            self.recipientLabel.text = @"Post to wall:";
            break;
            
        default:
            self.recipientLabel.text = [NSString stringWithFormat:@"%@", self.recipientText];
            break;
    }
    
    //--set button text
    [self.cancelButton setTitle:cancelButonText forState:UIControlStateNormal];
    
    //-- Set radius for image avatar
    personImageView.layer.masksToBounds = YES;
    personImageView.layer.cornerRadius = 6;
    
    
    
    //SET BACKGROUND IMAGE
    switch (sendType) {
        case TWITTER_TYPE: [bgImageView setImage:[UIImage imageNamed:@"bg_ct_twitter@2x.png"]]; break;
        case SMS_TYPE: [bgImageView setImage:[UIImage imageNamed:@"bg_ct_sms@2x.png"]]; break;
        case RETWITTER_TYPE: [bgImageView setImage:[UIImage imageNamed:@"bg_ct_twitter@2x.png"]]; break;
        case NEWTWEET_TYPE: [bgImageView setImage:[UIImage imageNamed:@"bg_ct_twitter@2x.png"]]; break;
        case REPLY_TWEET_TYPE: [bgImageView setImage:[UIImage imageNamed:@"bg_ct_twitter@2x.png"]]; break;
        case RETWEET_TYPE:[bgImageView setImage:[UIImage imageNamed:@"bg_ct_twitter@2x.png"]]; break;
        case FACEBOOK_POSTING_TYPE: [bgImageView setImage:[UIImage imageNamed:@"bg_ct_fb@2x.png"]]; break;
        case FACEBOOK_IMAGE_TYPE: [bgImageView setImage:[UIImage imageNamed:@"bg_ct_fb@2x.png"]]; break;
        case FACEBOOK_COMMENT_TYPE: [bgImageView setImage:[UIImage imageNamed:@"bg_ct_fb@2x.png"]]; break;
            
        default: break;
    }
    
    // CHECK FOR BANNED WORDS AND ADDRESSES //
    banned_flag = 0;
    self.bannedList = @"";
    for (NSString *word in [self bannedWords]) {
        if ([self.messageText rangeOfString:word options:NSCaseInsensitiveSearch].length != 0) {
            if (0 == banned_flag) self.bannedList = word;
            else self.bannedList = [NSString stringWithFormat:@"%@, %@",self.bannedList,word];
            banned_flag |= 1;
        }
    }
    for (NSString *address in [self bannedAddresses]) {
        if ([self.recipientText rangeOfString:address options:NSCaseInsensitiveSearch].length != 0) {
            if (0 == banned_flag) self.bannedList = address;
            else self.bannedList = [NSString stringWithFormat:@"%@, %@",self.bannedList,address];
            banned_flag |= 2;
        }
    }
    
    [self.countDownLabel setText:@" "];
    self.countDownLabel.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.countDownLabel.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.countDownLabel.layer.shadowOpacity = 1.0f;
    self.countDownLabel.layer.shadowRadius = 2.0f;
    
    self.personImageView.frame = CGRectMake(PIM_ORIGIN_X,
                                            PIM_ORIGIN_Y,
                                            PIM_SIZE_WIDTH,
                                            PIM_SIZE_HEIGHT);
    
    if (banned_flag == 0) {
        [self performSelector:@selector(startCountdown) withObject:nil afterDelay:COUNTDOWN_TRANSITION];
    } else {
        [self performSelector:@selector(startBannedAlert) withObject:nil afterDelay:COUNTDOWN_TRANSITION];
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    BOOL wasRestarted = [[NSUserDefaults standardUserDefaults] boolForKey:@"APP_RESTARTED"];
    if (wasRestarted) {
        timeSec = 2;
        [self.countDownLabel setText:@" "];
        if (nil != soundEffect) [soundEffect stop];
    }
}


- (void)viewDidDisappear:(BOOL)animated
{
    //    [soundEffect stop];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    BOOL wasRestarted = [[NSUserDefaults standardUserDefaults] boolForKey:@"APP_RESTARTED"];
    if (wasRestarted) {
        timeSec = 2;
        [self.countDownLabel setText:@" "];
        if (nil != soundEffect) [soundEffect stop];
    }
}



#pragma mark - TIME

//Event called every time the NSTimer ticks.
- (void)timerTick:(NSTimer *)theTimer
{    
    
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateActive)
    {
        return;
    }
    
    BOOL wasRestarted = [[NSUserDefaults standardUserDefaults] boolForKey:@"APP_RESTARTED"];
    BOOL forceSendMessage = [[NSUserDefaults standardUserDefaults] boolForKey:@"FORCE_SEND_MESSAGE"];
    if (!wasRestarted || timeSec > 2) {
        timeSec--;
        
        [[NSUserDefaults standardUserDefaults] setInteger:timeSec forKey:@"TIMER_SECONDS"];
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"voiceCountdown"] && !isPlayingSound && timeSec > 0)
        {
            NSInteger fileTime = timeSec;
            if (fileTime>10) fileTime = 10;
            NSLog(@"fileTime:%d",fileTime);
            NSURL *file = [[NSURL alloc] initFileURLWithPath: [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"full%d",fileTime]  ofType:@"mp3"]];
            
            [[JSQSystemSoundPlayer sharedPlayer] playVibrateSound];
            
            soundEffect = [[AVAudioPlayer alloc] initWithContentsOfURL:file error:nil];
            [soundEffect setNumberOfLoops:0];
            NSError *setCategoryErr = nil;
            NSError *activationErr  = nil;
            [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error:&setCategoryErr];
            [[AVAudioSession sharedInstance] setActive:YES error:&activationErr];
            [soundEffect prepareToPlay];
            if (timeSec <= 10) {
                [soundEffect play];
            }
            else
            {
                NSTimeInterval then = (NSTimeInterval)(timeSec-10);
                [soundEffect playAtTime:soundEffect.deviceCurrentTime+then];
            }
            isPlayingSound = YES;
        }        
        else  
        {
            //--app should still countdown through vibration fix #262
            [[JSQSystemSoundPlayer sharedPlayer] playVibrateSound];
        }
        // fix crazy number #199 count down .
        if (timeSec > 0)
        {
            [self.countDownLabel setText:[NSString stringWithFormat:@"%i",timeSec]];
            DLOG(@"COUNT DOWN TIME %d",timeSec);
        }
        
        if (timeSec == 0 || forceSendMessage)
        {
            if (theTimer != nil) [theTimer invalidate];
            if (soundEffect != nil) [soundEffect stop];
            switch (self.sendType) {
                case TWITTER_TYPE: [twitterDelegate countDownComplete]; break;
                case SMS_TYPE: [delegate sendSmsMessage]; break;
                case RETWITTER_TYPE: [self.retwitterDelegate countDownComplete]; break;
                case REPLY_TWEET_TYPE: [self.detailsTweetDelegate replyCountDownComplete:messageText]; break;
                case RETWEET_TYPE: [self.detailsTweetDelegate retweetCountDownComplete]; break;
                case NEWTWEET_TYPE: [self.retwitterDelegate newTweetComplete]; break;
                case FBLike: {
                    break;
                }
                case FBUnlike: {
                    break;
                }
                case FBshare: {
                    break;
                }
                case FBcomment: {
                    [self.sendMessageDelegate countDownComplete];
                    break;
                }
                case FBPost: {
                    [self.sendMessageDelegate countDownComplete];
                    [facebookImageDelegate countDownComplete];
                    [self.sendCheckinDelegate countDownComplete];
                    break;
                }
                case FBPostBuddy:
                {
                    [self.buddyDelegate countDownCompleteWithIndex:self.indexRowSelected];
                }
                    
                default: break;
            }
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"IS_SENDING_MESSGE"];
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_SECONDS"];
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SEND_TYPE"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SEND_ADDRESS"];
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self closeWindow];
        }
    } else {
        if (theTimer != nil) [theTimer invalidate];
        timeSec = 2;
        [self.countDownLabel setText:@"1"];
        if (soundEffect != nil) [soundEffect stop];
        
        NSString *alertTypeText;
        NSString *buttonText = @"Send Now";
        NSString *actionText;
        switch (sendType) {
            case TWITTER_TYPE:{
                alertTypeText = @"Tweet";
                actionText = @"tweet";
                break;
            }
                
            case SMS_TYPE:{
                alertTypeText = @"Text";
                actionText = @"send";
                break;
            }
                
            case RETWITTER_TYPE:
            {
                alertTypeText = @"Tweet";
                actionText = @"retweet";
                break;
            }
                
            case NEWTWEET_TYPE:
            {
                alertTypeText = @"Tweet";
                actionText = @"tweet";
                break;
            }
                
            case REPLY_TWEET_TYPE:
            {
                alertTypeText = @"Reply";
                actionText = @"reply";
                break;
            }
                
            case RETWEET_TYPE:
            {
                alertTypeText = @"Tweet";
                actionText = @"retweet";
                break;
            }
                
            case FACEBOOK_POSTING_TYPE:
            {
                alertTypeText = @"Post";
                actionText = @"post";
                break;
            }
                
            case typeTWRetweet:
            {
                alertTypeText = @"Tweet";
                actionText = @"retweet";
                break;
            }
                
            case FBcomment:
            {
                alertTypeText = @"Post";
                actionText = @"post";
                break;
            }
                
            case FBPost:
            {
                alertTypeText = @"Post";
                actionText = @"post";
                break;
            }
            case typeShare:
            {
                alertTypeText = @"Post";
                actionText = @"share";
                break;
            }
                
            case FBLike:
            {
                alertTypeText = @"Post";
                actionText = @"like";
                break;
            }
                
            case FACEBOOK_IMAGE_TYPE:
            case FACEBOOK_COMMENT_TYPE:
            case typeComment:
                alertTypeText = @"Post";
                actionText = @"post";
                break;
                
            default: break;
        }
        
        alertType = ALERT_TYPE_NORMAL;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"BeSafe %@",alertTypeText]
                                                        message:[NSString stringWithFormat:@"BeSafe Apps was restarted. Are you sure you want to %@ the last message to %@", actionText,self.recipientText]
                                                       delegate:self
                                              cancelButtonTitle:@"Stop"
                                              otherButtonTitles:buttonText, nil];
        [alert show];
    }
}



#pragma mark - Action

- (void)setLabelFont
{
    [lblSeconds setFont:[UIFont fontWithName:@"Bebas" size:25]];
    [countDownLabel setFont:[UIFont fontWithName:@"Bebas" size:100]];
    [cancelButton.titleLabel setFont:[UIFont fontWithName:@"Bebas" size:22]];
}


- (IBAction)closeWindow
{
    //    if (smsType == sendType) {
    //        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"CAN_FORGET_WARNING"];
    //        [[NSUserDefaults standardUserDefaults] synchronize];
    //    }
    
    [self stopTimer];
    
    UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromLeft;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
    [UIView setAnimationDuration:COUNTDOWN_TRANSITION];
    
    NSInteger twitter_modal = [[NSUserDefaults standardUserDefaults] integerForKey:@"TWITTER_MODAL"];
    if (twitter_modal == 1) {
        [[NSUserDefaults standardUserDefaults] setInteger:2 forKey:@"TWITTER_MODAL"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [UIView commitAnimations];
}


- (void)stopTimer
{
    if (timer != nil) [timer invalidate];
    if (soundEffect != nil) [soundEffect stop];
    timeSec = 0;
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"IS_SENDING_MESSGE"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_SECONDS"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SEND_TYPE"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SEND_ADDRESS"];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[NSUserDefaults standardUserDefaults] synchronize];
    // send delegate to show keyboard
    if (self.delegate_countdown && [self.delegate_countdown respondsToSelector:@selector(showKeyboard)]) {
        [self.delegate_countdown showKeyboard];
    }
}


- (void)doMyLayoutStuff
{
    // stuff
    NSLog(@"doMyLayoutStuff:Return From Background");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSTimeInterval timer2 = [[NSDate date] timeIntervalSinceReferenceDate] - appDelegate.timer;
    //    timeSec = 2; // thuy comment
    timeSec = timeSec - timer2;
    [self.countDownLabel setText:@" "];
    //    [soundEffect play];
    //    if (soundEffect != nil) [soundEffect stop];
}


- (void)startCountdown
{
    if (sendType == FBPostBuddy)
    {
        timeSec = 3;
    }
    else
    {
        timeSec = [[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"];
    }
    
    timeSec++;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    
    if ([self.abId intValue] == -1 && !isImageLoaded) {
        UIImage* image = [UIImage imageNamed:@"public.png"];
        //self.personImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.personImageView.clipsToBounds = YES;
        self.personImageView.image = image;
        
        self.personImageView.frame = CGRectMake(PIM_ORIGIN_X,
                                                PIM_ORIGIN_Y,
                                                PIM_SIZE_WIDTH,
                                                PIM_SIZE_HEIGHT);
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"IS_SENDING_MESSGE"];
    [[NSUserDefaults standardUserDefaults] setInteger:timeSec forKey:@"TIMER_SECONDS"];
    [[NSUserDefaults standardUserDefaults] setInteger:sendType forKey:@"SEND_TYPE"];
    [[NSUserDefaults standardUserDefaults] setObject:self.recipientText forKey:@"SEND_ADDRESS"];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"APP_RESTARTED"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FORCE_SEND_MESSAGE"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doMyLayoutStuff)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}


- (void)animationFinished:(NSString *)animationID finished:(BOOL)finished context:(void *)context
{
    [self performSelectorInBackground:@selector(loadImages) withObject:nil];
}


- (void)loadImages
{
    if (APP_DEBUG) NSLog(@"loadImages");
    UIImage* image = nil;
    //CHECK FOR ADDRESSBOOK IMAGE OR SERVER IMAGE
    
    switch (sendType) {
        case SMS_TYPE:
            image = [UIImage imageNamed:@"sms_countdown.png"];
            isImageLoaded = YES;
            break;
        case FACEBOOK_POSTING_TYPE:
            image = [UIImage imageNamed:@"fb_post_countdown.png"];
            isImageLoaded = YES;
            break;
        case FACEBOOK_IMAGE_TYPE:
            image = self.fbImage;
            isImageLoaded = YES;
            break;
        case FBPost:
        {
            NSLog(@"!!imagePath:%@",self.imagePath);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            // for status , comment , photo .
            if ([self.imagePath length] > 0) {
                imagePath = [imagePath stringByReplacingOccurrencesOfString:@"_s." withString:@"_n."];
                image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.imagePath]]];
                NSLog(@"!!imagePath 123:%@",self.imagePath);
                
            }
            else{
                image = fbImage_postPhoto;
            }
            isImageLoaded = NO;
            NSInteger imgFix = MAX(image.size.width, image.size.height);
            if (imgFix < 150) {
                imgFix = 150;
            }
            self.personImageView.frame = CGRectMake(PIM_ORIGIN_X,
                                                    PIM_ORIGIN_Y,
                                                    PIM_SIZE_WIDTH,
                                                    PIM_SIZE_HEIGHT);
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            break;
        }
        case FBcomment:
        {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            NSLog(@"!!imagePath:%@",self.imagePath);
            if ([self.imagePath length] > 0) {
                imagePath = [imagePath stringByReplacingOccurrencesOfString:@"_s." withString:@"_n."];
                image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.imagePath]]];
                NSLog(@"!!imagePath 123:%@",self.imagePath);
            }
            else{
                image = fbImage_postPhoto;
                
            }
            isImageLoaded = NO;
            NSInteger imgFix = MAX(image.size.width, image.size.height);
            if (imgFix < 150) {
                imgFix = 150;
            }
            self.personImageView.frame = CGRectMake(PIM_ORIGIN_X,
                                                    PIM_ORIGIN_Y,
                                                    PIM_SIZE_WIDTH,
                                                    PIM_SIZE_HEIGHT);
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            break;
        }
        default:
            //isImageLoaded = YES;
            if ([self.abId intValue] == -1) {
                if ([self.imagePath isEqualToString:@""]) {
                    //USE PUBLIC TWITTER IMAGE
                    image = [UIImage imageNamed:@"public.png"];
                }else {
                    //USE TWITTER USER IMAGE - REPLACE NORMAL IN ORDER TO RECIEVE LARGER IMAGE
                    NSLog(@"!!imagePath:%@",self.imagePath);
                    if ([self.imagePath length] > 0) {
                        image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.imagePath]]];
                    }
                    else{
                        image = fbImage_postPhoto;
                    }
                }
            }else {
                //LOOK UP IMAGE FROM ADDRESSBOOK
                ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
                
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
                    __block BOOL accessGranted = NO;
                    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
                    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                        accessGranted = granted;
                        dispatch_semaphore_signal(sema);
                        if (YES == granted) {
                            NSLog(@"do nothing");
                        }
                    });
                }
                
                ABRecordRef personRef = ABAddressBookGetPersonWithRecordID(addressBook, [self.abId intValue]);
                CFDataRef dataref = ABPersonCopyImageData(personRef);
                
                if(ABPersonHasImageData(personRef)){
                    image = [UIImage imageWithData:(__bridge NSData *)dataref];
                }else{
                    image = [UIImage imageNamed:@"sms_countdown.png"];
                }
                if(dataref)CFRelease(dataref);
                CFRelease(addressBook);
            }
            break;
    }
    
    //    NSLog(@"piv:2");
    //self.personImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.personImageView.clipsToBounds = YES;
    if (sendType == FACEBOOK_COMMENT_TYPE && nil != self.imagePath) {
        
        [self.personImageView loadFromURL:[NSURL URLWithString:self.imagePath]];
    } else {
        
        if ([self.imagePath length] > 0) {
            image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.imagePath]]];
        }
        else{
            image = fbImage_postPhoto;
        }
        self.personImageView.image = image;
        //self.personImageView.image = fbImage_postPhoto;
    }
    self.personImageView.alpha = 0.0;
    
    // load frame imageView mac dinh dung chung tru comment,status ,photo dung rieng .
    if (isImageLoaded) {
        self.personImageView.frame = CGRectMake(PIM_ORIGIN_X,
                                                PIM_ORIGIN_Y,
                                                PIM_SIZE_WIDTH,
                                                PIM_SIZE_HEIGHT);
        
    }
    
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.5];
    self.personImageView.alpha = 1.0;
    [UIView commitAnimations];
    isImageLoaded = YES;
}



#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.localNotification) {
        [[UIApplication sharedApplication] cancelLocalNotification:appDelegate.localNotification]; //Cancel the notification with the system
    }    
    
    NSLog(@"countdown alert view:%d",buttonIndex);
    switch (alertType) {
        case ALERT_TYPE_BANNED:
            if (buttonIndex == 1) [self startCountdown];
            else [self closeWindow];
            break;
        case ALERT_TYPE_NORMAL: {
            if (buttonIndex == 1) {
                switch (self.sendType) {
                    case TWITTER_TYPE: [twitterDelegate countDownComplete]; break;
                    case SMS_TYPE: [delegate sendSmsMessage]; break;
                    case RETWITTER_TYPE: [retwitterDelegate countDownComplete]; break; // JLJ //
                    case NEWTWEET_TYPE: [retwitterDelegate newTweetComplete]; break;
                    case REPLY_TWEET_TYPE: [detailsTweetDelegate replyCountDownComplete:messageText]; break;
                    case RETWEET_TYPE: [detailsTweetDelegate retweetCountDownComplete]; break;
                        
                    default: break;
                }
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"IS_SENDING_MESSGE"];
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_SECONDS"];
                [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SEND_TYPE"];
                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SEND_ADDRESS"];
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            [self closeWindow];
        }
            break;
    }
    
    alertType = 0;
}



#pragma mark - BANNED

- (void)startBannedAlert
{
    NSString *alertMsg = @"Your message and your address contain words that you have blocked:";
    switch (banned_flag) {
        case 1:
            alertMsg = @"Your message contains words or phrases that you have blocked:";
            break;
            
        case 2:
            alertMsg = @"Your address contains recipients that you have blocked:";
            break;
    }
    alertType = ALERT_TYPE_BANNED;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BANNED"
                                                    message:[NSString stringWithFormat:@"%@ %@",alertMsg,self.bannedList]
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"Stop",@"Send Anyway", nil];
    [alert show];
}


- (NSArray*) bannedWords
{
    if (!self._bannedWords) {
        self._bannedWords = [[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_WORDS"];
        if (!self._bannedWords) self._bannedWords = [[NSArray alloc] init];
    }
    
    return self._bannedWords;
}


- (NSArray*) bannedAddresses
{
    if (!self._bannedAddresses) {
        self._bannedAddresses = [[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_ADDRESSES"];
        if (!self._bannedAddresses) self._bannedAddresses = [[NSArray alloc] init];
    }
    
    return self._bannedAddresses;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}




@end
