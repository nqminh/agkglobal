//
//  ContactListViewController.m
//  BeSafe
//
//  Created by The Rand's on 7/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ContactListViewController.h"
#import "AppDelegate.h"
#import "SendTwitterMessageViewController.h"
#import "Constants.h"
#import "CountDownViewController.h"

#define smsType         2


@implementation ContactListViewController

@synthesize contacts, alphabet, allContacts, asContactArray, message, smsController, otherSendButton;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"Contacts";
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.alphabet = [[NSArray alloc] initWithObjects:@" ", nil];
    [self loadAddressBook];
}


- (void)loadAddressBook
{
    __block BOOL accessGranted = NO;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
            if (YES == granted) {
                self.alphabet = [[NSArray alloc] initWithObjects:@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",@"#", nil];
                [self.tableView reloadData];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contacts"
                                                                message:@"You must grant BeSafe access to your address book in order to use your contacts."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles: nil];
                [alert show];
            }
        });
    } else {
        self.alphabet = [[NSArray alloc] initWithObjects:@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",@"#", nil];
        [self.tableView reloadData];
    }
    
    CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    self.allContacts = [[NSMutableArray alloc] initWithCapacity:27];
    for (int a=0; a < 27; a++) {
        NSMutableArray *tempArray = [[NSMutableArray alloc] initWithCapacity:0];
        [self.allContacts addObject:tempArray];
    }
    for (int i = 0; i < nPeople; i++) {
        NSMutableDictionary *person = [[NSMutableDictionary alloc] initWithCapacity:0];
        ABRecordRef ref = CFArrayGetValueAtIndex(people, i);
        
        //Name
        CFStringRef firstName = ABRecordCopyValue(ref, kABPersonFirstNameProperty);
        CFStringRef lastName = ABRecordCopyValue(ref,kABPersonLastNameProperty);
        NSString *firstLetter = [[ (__bridge NSString *)firstName substringToIndex:1] capitalizedString];
        NSString *contactFirstLast = @"";
        if (!lastName) {
            contactFirstLast = [NSString stringWithFormat: @"%@",(__bridge NSString *)firstName];
        }else{
            contactFirstLast = [NSString stringWithFormat: @"%@ %@",(__bridge NSString *)firstName, (__bridge NSString *)lastName];
        }
        
        //Phone Numbers
        NSMutableArray *phoneNumbers = [[NSMutableArray alloc] initWithCapacity:0];
        ABMultiValueRef multiPhones = ABRecordCopyValue(ref, kABPersonPhoneProperty);
        for(CFIndex j = 0; j < ABMultiValueGetCount(multiPhones); j++) {
            NSMutableDictionary *phoneNumberDict = [[NSMutableDictionary alloc] initWithCapacity:0];
            NSString *phoneType = (__bridge NSString*)ABAddressBookCopyLocalizedLabel(ABMultiValueCopyLabelAtIndex(multiPhones, j));
            NSString *phoneNumber = (__bridge NSString*)ABMultiValueCopyValueAtIndex(multiPhones, j);
            
            [phoneNumberDict setObject:phoneType forKey:@"phoneType"];
            [phoneNumberDict setObject:phoneNumber forKey:@"phoneNumber"];
            [phoneNumbers addObject:phoneNumberDict];
        }
        CFRelease(multiPhones);
        
        //Image
        UIImage* image;
        if(ABPersonHasImageData(ref)){
            image = [UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageData(ref)];
        }else{
            image = [UIImage imageNamed:@"default_sms.png"];
        }
        
        //Twitter
        NSString *twitterName = @"";
        ABMultiValueRef multiSocialMedia = ABRecordCopyValue(ref, kABPersonSocialProfileProperty);
        for(CFIndex s = 0; s < ABMultiValueGetCount(multiSocialMedia); s++) {
            CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(multiSocialMedia, s);
            NSString *socialType = (__bridge NSString*)CFDictionaryGetValue(dict, kABPersonSocialProfileServiceKey);
            if ([socialType isEqualToString:@"twitter"]) {
                twitterName = (__bridge NSString*)CFDictionaryGetValue(dict, kABPersonSocialProfileUsernameKey);
            }
        }
        
        
        NSNumber *myID = [NSNumber numberWithInt:ABRecordGetRecordID(ref)];
        [person setObject:contactFirstLast forKey:@"name"];
        [person setObject:image forKey:@"image"];
        [person setObject:phoneNumbers forKey:@"phoneNumbers"];
        [person setObject:myID forKey:@"abId"];
        [person setObject:twitterName forKey:@"twitterUserName"];
        
        if(firstLetter){
            if([self.alphabet indexOfObject:firstLetter] == NSNotFound) firstLetter = @"#";
            [[self.allContacts objectAtIndex:[self.alphabet indexOfObject:firstLetter]] addObject:person];
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
            [[self.allContacts objectAtIndex:[self.alphabet indexOfObject:firstLetter]] sortUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        }
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    //-- change title color
    CGRect frame = CGRectMake(100, 0, 100, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



#pragma mark - Table view data source

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
   return self.alphabet;
}


- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString*)title atIndex:(NSInteger)index
{
    return [self.alphabet indexOfObject:title];
}


- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section
{
    if ([[self.allContacts objectAtIndex:section] count] == 0) {
        return nil;
    }else{
        return [self.alphabet objectAtIndex:section];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [self.allContacts count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[self.allContacts objectAtIndex:section] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 40, 40)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(50, 5, 200, 40)];
        imageView.tag = 8;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        label.tag = 9;
        [label setBackgroundColor:[UIColor clearColor]];
        [label setHighlightedTextColor:[UIColor whiteColor]];
        [label setFont:[UIFont boldSystemFontOfSize:18]];
        [cell.contentView addSubview:imageView];
        [cell.contentView addSubview:label];
    }
    
    // Configure the cell...
    [(UILabel *)[cell.contentView viewWithTag:9] setText:[[[self.allContacts objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"name"]];
    [(UIImageView *)[cell.contentView viewWithTag:8] setImage:[[[self.allContacts objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"image"]];

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    [self openActionSheet:indexPath];
}


- (void)openActionSheet:(NSIndexPath *)indexPath
{
    //GET CONTACT NAME, PHONE NUMBERS AND TWITTER USERNAME IF APPLICABLE
    NSString *name = [[[self.allContacts objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"name"];
    NSArray *numbers = [NSArray arrayWithArray:[[[self.allContacts objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"phoneNumbers"]];
    NSString *twitterUserName = [[[self.allContacts objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"twitterUserName"];
    
    NSMutableArray *buttonLabels = [[NSMutableArray alloc] initWithCapacity:0];
    asContactArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (int i = 0; i < numbers.count; i++) {
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithCapacity:0];
        [tempDict setObject:[[numbers objectAtIndex:i] objectForKey:@"phoneNumber"] forKey:@"number"];
        [tempDict setObject:@"SMS" forKey:@"type"];
        [tempDict setObject:[[[self.allContacts objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"abId"] forKey:@"abId"];
        [buttonLabels addObject:[NSString stringWithFormat:@"%@ %@",[[numbers objectAtIndex:i] objectForKey:@"phoneType"],[[numbers objectAtIndex:i] objectForKey:@"phoneNumber"]]];
        [asContactArray addObject:tempDict];
    }
    
    //INITIALIZE THE ACTION SHEET
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:name delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    
    //ADD PHONE NUMBERS TO ACTION SHEET
    for (NSString * title in buttonLabels) { [actionSheet addButtonWithTitle:title]; }
    
    //ADD TWITTER TO ACTION SHEET
    if(![twitterUserName isEqualToString:@""]) {
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithCapacity:0];
        [tempDict setObject:twitterUserName forKey:@"userName"];
        [tempDict setObject:@"twitter" forKey:@"type"];
        [tempDict setObject:[[[self.allContacts objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"abId"] forKey:@"abId"];
        [asContactArray addObject:tempDict];
        
        [actionSheet addButtonWithTitle:@"Twitter: DM"];
    }
    
    //ADD CANCEL BUTTON TO ACTION SHEET
    [actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet setCancelButtonIndex:[actionSheet numberOfButtons]-1];

    //SHOW THE ACTION SHEET
    [actionSheet showInView:appDelegate.window];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"]) {
        //user canceled
    }else{
        if([[[asContactArray objectAtIndex:buttonIndex] objectForKey:@"type"] isEqualToString:@"SMS"])
        {
            //OPEN SMS COMPOSER IF AVAILABLE
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"smsUnlocked"]){
                [self displaySMSComposerSheet:[[asContactArray objectAtIndex:buttonIndex] objectForKey:@"number"] body:self.message abId:[[asContactArray objectAtIndex:buttonIndex] objectForKey:@"abId"]];
            }else {
                //ALERT
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SMS Not Available" message:@"You can purchase sms functionality from the settings tab." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        }else {
            //OPEN TWITTER COMPOSER IF CONNECTED
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            if([appDelegate isConnectedToTwitter]) {
                [appDelegate displayTwitterComposerSheet:[[asContactArray objectAtIndex:buttonIndex] objectForKey:@"userName"] 
                                                    body:self.message 
                                                    abId:[[asContactArray objectAtIndex:buttonIndex] objectForKey:@"abId"] 
                                               imagePath:@"" 
                                                    view:self 
                                                   title:@"New Message"];
            }else {
                //ALERT NOT CONNECTED TO TWITTER
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Connected To Twitter" message:@"You must be logged on and have a network connection to send twitter messages." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        }
    }
    
    [self.tableView reloadData];
}


- (void)displaySMSComposerSheet:(NSString *)recipient body:(NSString *)body abId:(NSNumber*)abId
{
    if([MFMessageComposeViewController canSendText])
	{
        smsController = [[SMSComposerSheetViewController alloc] init];
        smsController.messageComposeDelegate = self;
        smsController.recipients = [NSArray arrayWithObjects:recipient, nil];
        smsController.body = body;
        smsController.abId = abId;

        [self presentViewController:smsController animated:YES completion:nil];
    }
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)prepareSmsMessage
{
    NSLog(@"prepare:begin");
    //REMOVES BUTTON BEHAVIOR
    //[self.smsButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    
    if (nil != self.otherSendButton) [self.otherSendButton removeFromSuperview];
    
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"] == 0) {
        NSLog(@"prepare:if");
        [self sendSmsMessage];
    }else {
        NSLog(@"prepare:else");
        CountDownViewController *controller = nil;
        if (isIphone5)
            controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController-568h" bundle:nil];
        else
            controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController" bundle:nil];
        
        controller.messageText = @"Some Message"; //[(UITextView*)self.inputTextView text];
        //controller.recipientText = self.recipientName;
        controller.recipientText = @"SMS Recipient";
        //controller.delegate = self;
        //controller.abId = self.abId;
        controller.sendType = smsType;

        if (smsController != nil) {
            NSLog(@"prepare:animate:begin");
            UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromRight;
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
            [UIView setAnimationDuration:COUNTDOWN_TRANSITION];
            [UIView setAnimationDelegate:controller];
            [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
            NSLog(@"prepare:animate:present");
            [smsController presentViewController:controller animated:YES completion:nil];
            [UIView commitAnimations];
            NSLog(@"prepare:animate:end");
        } else {
            NSLog(@"else#2");
        }
    }
}


- (void)sendSmsMessage
{
    //SEND ORIGINAL SMS MESSAGE
    //[self.smsDelegateView performSelector:@selector(send:) withObject:self.smsButton];
    
    /*
    //SEND INFO TO HISTORY
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    [tempDict setValue:[(UITextView*)self.inputTextView text] forKey:@"message"];
    [tempDict setValue:[self.recipients objectAtIndex:0] forKey:@"number"];
    [tempDict setValue:self.recipientName forKey:@"name"];
    [tempDict setObject:self.abId forKey:@"abId"];
    [tempDict setObject:[NSDate date] forKey:@"timestamp"];
    
    NSMutableArray *historyArray = [self loadHistory];
    [historyArray insertObject:tempDict atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:historyArray forKey:@"smsHistory"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     */
    
    //CLOSE WINDOW
    //closeOnAppear = YES;
}


- (void)listSubviewsOfView:(UIView *)view atLevel:(NSInteger)level
{
    NSLog(@"-----%d-----",level);
    
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    if (!subviews) {
        NSLog(@"nil");
        return;
    }
    NSLog(@"--aa--:subviews=%d",subviews.count);
    // Return if there are no subviews
    if ([subviews count] == 0) return;
    NSLog(@"--bb--");
    
    for (UIView *subview in subviews) {
        
        NSLog(@"%d:%@ [%@]",level,subview.description,subview.class);
        
        // List the subviews of subview
        [self listSubviewsOfView:subview atLevel:level+1];
    }
}



@end
