//
//  SocialNetworkCell.h
//  AGKGlobal
//
//  Created by ThoaPham on 19/08/2013.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialNetworkCell : UITableViewCell
{
    //
}

@property (nonatomic, retain) IBOutlet UIButton     *btnSocial;
@property (nonatomic, retain) IBOutlet UILabel      *lblSocial;
@property (nonatomic, retain) IBOutlet UILabel      *lblName;


@end
