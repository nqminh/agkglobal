//
//  SocialNetworkViewController.m
//  AGKGlobal
//
//  Created by ThoaPham on 16/08/2013.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "SocialNetworkViewController.h"
#import "SocialNetworkCell.h"

@interface SocialNetworkViewController ()

@end

@implementation SocialNetworkViewController

@synthesize _socialList;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - Main

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Social Network";
    
    NSArray *listArr = [[NSArray alloc] initWithObjects:@"Twitter",@"Facebook", nil];
    _socialList = [[NSMutableArray alloc] initWithArray:listArr];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //-- hide activityIndicator
    [NetworkActivity hide];
    
    [self.tableView reloadData];
    
    //-- change title color
    CGRect frame = CGRectMake(100, 0, 100, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    return [self socialList].count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SocialNetworkCell";
    SocialNetworkCell *cell = (SocialNetworkCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SocialNetworkCell" owner:nil options:nil];
        
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (SocialNetworkCell *) currentObject;
                break;
            }
        }
    }
    
    //-- set data for cell
    [self setDataForTableViewCell:cell withIndexPath:indexPath];
    
    return cell;
}


//-- set data for tableview Settings
-(SocialNetworkCell *) setDataForTableViewCell:(SocialNetworkCell *) cell withIndexPath:(NSIndexPath *)indexPath
{
    cell.lblSocial.text = [[self socialList] objectAtIndex:indexPath.row];
    cell.lblSocial.textColor = [UIColor colorWithRed:72/256.0f green:72/256.0f blue:72/256.0f alpha:1];
    cell.lblSocial.font = [UIFont boldSystemFontOfSize:17];
    
    if (indexPath.row == 0) {
        [cell.btnSocial setImage:[UIImage imageNamed:@"icon_twitter_active.png"] forState:UIControlStateNormal];        
        cell.lblName.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"twitterAccountName"];
        cell.lblName.textColor = [UIColor colorWithRed:112/256.0f green:179/256.0f blue:235/256.0f alpha:1];
    }else{
        [cell.btnSocial setImage:[UIImage imageNamed:@"icon_facebook_active.png"] forState:UIControlStateNormal];
        cell.lblName.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"facebookName"];
        cell.lblName.textColor = [UIColor colorWithRed:72/256.0f green:104/256.0f blue:198/256.0f alpha:1];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [[self socialList] removeObjectAtIndex:indexPath.row];
        [[NSUserDefaults standardUserDefaults] setObject:[self socialList] forKey:@"SOCIAL_NETWORK"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        self._socialList = nil;
        [self.tableView reloadData];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (NSMutableArray*) socialList {
    if (!self._socialList) {
        NSArray *nsa = [[NSUserDefaults standardUserDefaults] arrayForKey:@"SOCIAL_NETWORK"];
        if (!nsa) {
            self._socialList = [[NSMutableArray alloc] init];
        } else {
            self._socialList = [[nsa sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] mutableCopy];
        }
        NSLog(@"//init:%d",self._socialList.count);
    }
    return self._socialList;
}


@end
