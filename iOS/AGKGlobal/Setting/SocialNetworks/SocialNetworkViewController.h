//
//  SocialNetworkViewController.h
//  AGKGlobal
//
//  Created by ThoaPham on 16/08/2013.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialNetworkViewController : UITableViewController
{
    //
}

@property (nonatomic,strong) NSMutableArray *_socialList;

- (NSMutableArray*)socialList;

@end
