//
//  SocialNetworkCell.m
//  AGKGlobal
//
//  Created by ThoaPham on 19/08/2013.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "SocialNetworkCell.h"

@implementation SocialNetworkCell

@synthesize btnSocial, lblName, lblSocial;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
