//
//  SettingsViewController.h
//  BeSafe
//
//  Created by The Rand's on 7/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "BesafeAPI.h"
#import "UIView+HidingView.h"
#import "VMUsers.h"
#import "SignInViewController.h"
#import "UIView+HidingView.h"
#import "ReviewerViewController.h"

@interface SettingsViewController : UIViewController <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate>
{
    UISegmentedControl  *_timerControl;
    UILabel             *_accountLabel;
    UISwitch            *_voiceSwitch;
    UITextField         *_fullnamePrefField;
    UITextField         *_agePrefField;
    UITextField         *_sexPrefField;
    UITextField         *_emailPrefField;
    UITextField         *_phonePrefField;
    
    UIButton *doneButton;
	BOOL numberPadShowing;
	UITextField *selectedTextField;
    UIButton *_sendEmailButton;
    
    BOOL isGetSuccess;
}

@property (nonatomic, retain) IBOutlet UITableView  *tableView;
@property (nonatomic, retain) IBOutlet UIScrollView  *scrollview;
@property (nonatomic, retain) UISegmentedControl	*_timerControl;
//@property (nonatomic, retain) UITextField           *delayPrefField;

@property (nonatomic, assign) BOOL numberPadShowing;
@property (nonatomic, retain) UILabel *_accountLabel;
@property (nonatomic, retain) UIButton *doneButton;
@property (nonatomic, retain) UITextField *selectedTextField;
@property (nonatomic, retain) UISwitch            *_voiceSwitch;
@property (nonatomic, retain) UISwitch            *_tipSwitchTW;
@property (nonatomic, retain) UISwitch            *_tipSwitchFB;
@property (nonatomic, retain) UITextField         *_fullnamePrefField;
@property (nonatomic, retain) UITextField         *_agePrefField;
@property (nonatomic, retain) UITextField         *_sexPrefField;
@property (nonatomic, retain) UITextField         *_emailPrefField;
@property (nonatomic, retain) UITextField         *_phonePrefField;
@property (nonatomic, retain) UIButton            *_sendEmailButton;

@property (nonatomic, retain) NSString            *nameStr;
@property (nonatomic, retain) NSString            *ageStr;
@property (nonatomic, retain) NSString            *sexStr;
@property (nonatomic, retain) NSString            *emailStr;
@property (nonatomic, retain) NSString            *phoneStr;
@property (nonatomic, retain) NSString            *facebookIdStr;

- (UISegmentedControl*)timerControl;
- (UILabel*) accountLabel;
- (UISwitch*) voiceSwitch;
- (UITextField*) fullnamePrefField;
- (UITextField*) agePrefField;
- (UITextField*) sexPrefField;
- (UITextField*) emailPrefField;
- (UITextField*) phonePrefField;
- (UIButton*) sendEmailButton;

- (MBProgressHUD*)showProgressInView:(UIView*)view target:(id)aTarget
                               title:(NSString*)titleText selector:(SEL)aSelector;
- (void) showAlert: (NSString*) msg;



@end
