//
//  BannedWordsViewController.h
//  BeSafe
//
//  Created by Jeff Jolley on 4/11/13.
//
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface BannedWordsViewController : UITableViewController {
    
}

@property (nonatomic,strong) NSMutableArray *_bannedList;

- (NSMutableArray*) bannedList;
- (void)addBanned:(id)sender;

@end
