//
//  SettingsViewController.m
//  BeSafe
//
//  Created by The Rand's on 7/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SettingsViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "BannedAddressesViewController.h"
#import "BannedWordsViewController.h"
#import "SocialNetworkViewController.h" 
#import "BuddyFriendViewController.h"

#define SECTION_TIMERS            0
#define SECTION_VOICE_ON_OFF      1
#define SECTION_BESAFE_TIP_TW     2
#define SECTION_BESAFE_TIP_FB     3
#define SECTION_TWITTER_ACCOUNT   4
#define SECTION_BANNED_WORDS      5
#define SECTION_BANNED_ADDRESSES  6
#define SECTION_REVIEWER          7
#define SECTION_CONTACT_INFO      9
#define MAX_SECTIONS             10
#define SECTION_BUDDY             8


#define TAG_accountLabel        100
#define TAG_fullNamePrefField   101
#define TAG_agePrefField        102
#define TAG_sexPrefField        103
#define TAG_emailPrefField      104
#define TAG_phonePrefField      105
#define TAG_sendEmailButton     106
#define TAG_timerControl        107
#define TAG_voiceSwitch         108
#define TAG_tipSwitchTW         109
#define TAG_tipSwitchFB         110

// 1 = countdown time
// 2 = voice countdown on/off
// 3 = twitter info
// 4 = BANNED words
// 5 = BANNED addresses
// 6 = email/phone

@implementation SettingsViewController

@synthesize _timerControl;
@synthesize _accountLabel,_voiceSwitch,_fullnamePrefField,_agePrefField,_sexPrefField, _emailPrefField,_phonePrefField,_sendEmailButton;
@synthesize selectedTextField, doneButton, numberPadShowing, scrollview;
@synthesize nameStr, ageStr,sexStr, emailStr, phoneStr, facebookIdStr;
@synthesize _tipSwitchTW, _tipSwitchFB;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.title = NSLocalizedString(@"Settings", @"Settings");
        self.tabBarItem.image = [UIImage imageNamed:@"icon_setting_selected.png"];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(twitterAccountFound) name:@"accountFound" object:nil];
    numberPadShowing = NO;
    
    return self;
}


- (void)twitterAccountFound
{
    [self.tableView reloadData];
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
//    [self processTimePrefEntry:delayPrefField.text];
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isGetSuccess = NO;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480)
        scrollview.frame = CGRectMake(0, 92, 320, 608);
    else
        scrollview.frame = CGRectMake(0, 5, 320, 698);
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //-- change title color
    CGRect frame = CGRectMake(150, 0, 150, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    
    [self displayButtonLogOut];
    BOOL isTipTW = [[NSUserDefaults standardUserDefaults] boolForKey:warningControllTwitter];
    BOOL isTipFB = [[NSUserDefaults standardUserDefaults] boolForKey:warningControllFacebook];
    [self._tipSwitchTW setOn:!isTipTW];
    [self._tipSwitchFB setOn:!isTipFB];
    
    [self.tabBarController setTabBarHidden:NO animated:NO];     
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}


- (void)viewWillDisappear:(BOOL)animated
{
	if (numberPadShowing) {
		[doneButton removeFromSuperview];
	}
    [self.navigationItem setRightBarButtonItem:nil animated:NO];
	numberPadShowing = NO;

    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)timerChanged:(id)sender
{
//    NSLog(@"timerChanged");
    NSInteger secs = 0;
    switch ([sender selectedSegmentIndex]) {
        case 0: secs = 0; break; //0
        case 1: secs = 10; break;//3
        case 2: secs = 20; break;//6
        case 3: secs = 30; break;//9
        case 4: secs = 60; break;//9
        default:
            secs = 10; break;//3
            break;
    }
    [[NSUserDefaults standardUserDefaults] setInteger:secs forKey:@"timerSecs"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



#pragma mark - Action  Logout

- (void)displayButtonLogOut
{
    //----------button right navigation bar--------------
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStyleBordered
                                                                     target:self action:@selector(clicktoSelectLogout:)];
    self.navigationItem.rightBarButtonItem = anotherButton;
}


- (void) clicktoSelectLogout:(UIBarButtonItem *)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Log out" message:@"Are you sure you want to logout Besafe?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
    [alertView show];    
     
    /*
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Select Account Logout"
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Facebook",@"Twitter", nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
     */
}


- (void)logoutBesafeApp
{
    [VMUsers logOut];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LOG_OUT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isLogOut = YES;
    
    //--allow click update buddy
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:ONLY_CLICK_BUDDY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //--remove user_id login
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:BESAFE_USER_ID_LOGIN];
    
    
    NSLog(@"BUDDY ID LOGIN %@",[[NSUserDefaults standardUserDefaults] objectForKey:BESAFE_USER_ID_LOGIN]);
    
    //-- come back to HomeViewController
    [self popToHomeViewController];
    
    //-- turn off indicator
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


//-- pop to HomeViewController when select button Logout
-(void) popToHomeViewController
{
    //-- SignInViewController
    SignInViewController *signinController = nil;
    if (isIphone5)
        signinController = [[SignInViewController alloc] initWithNibName:@"SignInViewController-568h" bundle:nil];
    else
        signinController = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];    
    
     UINavigationController *signinNavController = [[UINavigationController alloc] initWithRootViewController:signinController];
    
    /* --Dung comment refact tabbar.
     NSMutableArray *tbViewControllers = [[NSMutableArray alloc] initWithArray:[self.tabBarController viewControllers]];        
    [tbViewControllers insertObject:signinNavController atIndex:0];
    
    [self.tabBarController setViewControllers:tbViewControllers];    
    [self.tabBarController setSelectedIndex:0];
    */
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.window setRootViewController:signinNavController];
    
    [self.navigationController popToRootViewControllerAnimated:YES];    
    
    NSLog(@"vo lon 3: %d", self.tabBarController.selectedIndex);
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
		case 0:
			NSLog(@"Facebook Button Clicked");
            [FacebookUtils logOutFacebook];            
			break;
		case 1:
			NSLog(@"Twitter Button Clicked");
            [self logOutTwitter];
			break;
        case 2:
			NSLog(@"Cancel Button Clicked");
			break;
        default:
            break;
    }
}


- (void) logOutFacebook1
{
    [FacebookUtils logOutFacebook];
}


- (void) logOutTwitter
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Logout Twitter" message:@"You have to switch the Setting in iPhone to logout" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
    [alertView show];
}



#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [self logoutBesafeApp];
    }
    
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return MAX_SECTIONS;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"numberOfRowsInSection");
    // Return the number of rows in the section.
    NSInteger myInteger = 0;
    // 1 = countdown time
    // 2 = voice countdown on/off
    // 3 = twitter info
    // 4 = BANNED words
    // 5 = BANNED addresses
    // 6 = email/phone
    switch (section) {
        case SECTION_TIMERS:
            myInteger = 1;
            break;
        case SECTION_VOICE_ON_OFF:
            myInteger = 1;
            break;
        case SECTION_BESAFE_TIP_TW:
            myInteger = 1;
            break;
        case SECTION_BESAFE_TIP_FB:
            myInteger = 1;
            break;
        case SECTION_TWITTER_ACCOUNT:
            myInteger = 1;
            break;
        case SECTION_BANNED_WORDS:
            myInteger = 1;
            break;
        case SECTION_BANNED_ADDRESSES:
            myInteger = 1;
            break;
        case SECTION_BUDDY:
            myInteger = 1;
            break;
            /*
        case SECTION_REVIEWER:
            myInteger = 1;
            break;
             */
        case SECTION_CONTACT_INFO:
            myInteger = 6;
            break;
        default: break;
    }

    return myInteger;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;

    // JLJ UNCOMMENT THIS FOR SEGMENT CONTROL
    NSInteger sIndex = 0;
    switch (indexPath.section) {
        case SECTION_TIMERS: {
            // WE ADD TAG_timerControl HERE //
            if (nil != [cell viewWithTag:TAG_accountLabel])
                [[self accountLabel] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                [[self fullnamePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_agePrefField])
                [[self agePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sexPrefField])
                [[self sexPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_emailPrefField])
                [[self emailPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_phonePrefField])
                [[self phonePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sendEmailButton])
                [[self sendEmailButton] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_voiceSwitch])
                [[self voiceSwitch] removeFromSuperview];

            switch ([[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"]) {
                case 0 : sIndex = 0; break;
                case 10: sIndex = 1; break;
                case 20: sIndex = 2; break;
                case 30: sIndex = 3; break;
                case 60: sIndex = 4; break;
                default:
                    sIndex = 1; break;
                    [[NSUserDefaults standardUserDefaults] setInteger:10 forKey:@"timerSecs"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    break;
            }
            
            [[self timerControl] setSelectedSegmentIndex:sIndex];
            [cell.contentView addSubview:[self timerControl]];
            cell.textLabel.text = @" ";
            cell.backgroundColor = [UIColor clearColor];
        }
            break;
        case SECTION_VOICE_ON_OFF: {
            // WE ADD TAG_voiceSwitch HERE //

            if (nil != [cell viewWithTag:TAG_accountLabel])
                [[self accountLabel] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                [[self fullnamePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_agePrefField])
                [[self agePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sexPrefField])
                [[self sexPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_emailPrefField])
                [[self emailPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_phonePrefField])
                [[self phonePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sendEmailButton])
                [[self sendEmailButton] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_timerControl])
                [[self timerControl] removeFromSuperview];
            //[cell setAccessoryView:[self voiceSwitch]];
            [self voiceSwitch].center = CGPointMake(cell.frame.size.width-(3*[self voiceSwitch].frame.size.width/5), (cell.frame.size.height/2));
            [cell addSubview:[self voiceSwitch]];
            [cell.textLabel setText:@"Voice Countdown 10 to 1"];
            [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
        }
             break;
        case SECTION_BESAFE_TIP_TW : {
            if (nil != [cell viewWithTag:TAG_accountLabel])
                [[self accountLabel] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                [[self fullnamePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_agePrefField])
                [[self agePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sexPrefField])
                [[self sexPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_emailPrefField])
                [[self emailPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_phonePrefField])
                [[self phonePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sendEmailButton])
                [[self sendEmailButton] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_timerControl])
                [[self timerControl] removeFromSuperview];
            //[cell setAccessoryView:[self voiceSwitch]];
            [self tipSwitchTW].center = CGPointMake(cell.frame.size.width-(3*[self tipSwitchTW].frame.size.width/5), (cell.frame.size.height/2));
            [cell addSubview:[self tipSwitchTW]];
            [cell.textLabel setText:@"Tweet Privacy Tip"];
            [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
        }
            break;
        case SECTION_BESAFE_TIP_FB : {
            if (nil != [cell viewWithTag:TAG_accountLabel])
                [[self accountLabel] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                [[self fullnamePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_agePrefField])
                [[self agePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sexPrefField])
                [[self sexPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_emailPrefField])
                [[self emailPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_phonePrefField])
                [[self phonePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sendEmailButton])
                [[self sendEmailButton] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_timerControl])
                [[self timerControl] removeFromSuperview];
            //[cell setAccessoryView:[self voiceSwitch]];
            [self tipSwitchFB].center = CGPointMake(cell.frame.size.width-(3*[self tipSwitchFB].frame.size.width/5), (cell.frame.size.height/2));
            [cell addSubview:[self tipSwitchFB]];
            [cell.textLabel setText:@"Post Privacy Tip"];
            [cell.textLabel setFont:[UIFont boldSystemFontOfSize:18]];
        }
            break;
        case SECTION_TWITTER_ACCOUNT: {
            // WE ADD TAG_accountLabel HERE //
            if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                [[self fullnamePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_agePrefField])
                [[self agePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sexPrefField])
                [[self sexPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_emailPrefField])
                [[self emailPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_phonePrefField])
                [[self phonePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sendEmailButton])
                [[self sendEmailButton] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_timerControl])
                [[self timerControl] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_voiceSwitch])
                [[self voiceSwitch] removeFromSuperview];

            [cell.textLabel setText:@"Social Network"];
            [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }
            break;
        case SECTION_BANNED_WORDS: {
            // WE DO NOT ADD ANYTHING HERE //

            if (nil != [cell viewWithTag:TAG_accountLabel])
                [[self accountLabel] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                [[self fullnamePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_agePrefField])
                [[self agePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sexPrefField])
                [[self sexPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_emailPrefField])
                [[self emailPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_phonePrefField])
                [[self phonePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sendEmailButton])
                [[self sendEmailButton] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_timerControl])
                [[self timerControl] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_voiceSwitch])
                [[self voiceSwitch] removeFromSuperview];

            [cell.textLabel setText:@"Banned Words"];
            [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }
            break;
        case SECTION_BANNED_ADDRESSES: {
            // WE DO NOT ADD ANYTHING HERE //

            if (nil != [cell viewWithTag:TAG_accountLabel])
                [[self accountLabel] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                [[self fullnamePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_agePrefField])
                [[self agePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sexPrefField])
                [[self sexPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_emailPrefField])
                [[self emailPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_phonePrefField])
                [[self phonePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sendEmailButton])
                [[self sendEmailButton] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_timerControl])
                [[self timerControl] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_voiceSwitch])
                [[self voiceSwitch] removeFromSuperview];

            [cell.textLabel setText:@"Banned Addresses"];
            [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }
            break;
       /*
        case SECTION_REVIEWER: {
            // WE DO NOT ADD ANYTHING HERE //
            
            if (nil != [cell viewWithTag:TAG_accountLabel])
                [[self accountLabel] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                [[self fullnamePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_agePrefField])
                [[self agePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sexPrefField])
                [[self sexPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_emailPrefField])
                [[self emailPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_phonePrefField])
                [[self phonePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sendEmailButton])
                [[self sendEmailButton] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_timerControl])
                [[self timerControl] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_voiceSwitch])
                [[self voiceSwitch] removeFromSuperview];
            
            [cell.textLabel setText:@"Reviewer"];
            [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }
            break;
         */
        
        case SECTION_BUDDY:
        {
            // WE DO NOT ADD ANYTHING HERE //
            
            if (nil != [cell viewWithTag:TAG_accountLabel])
                [[self accountLabel] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                [[self fullnamePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_agePrefField])
                [[self agePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sexPrefField])
                [[self sexPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_emailPrefField])
                [[self emailPrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_phonePrefField])
                [[self phonePrefField] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_sendEmailButton])
                [[self sendEmailButton] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_timerControl])
                [[self timerControl] removeFromSuperview];
            if (nil != [cell viewWithTag:TAG_voiceSwitch])
                [[self voiceSwitch] removeFromSuperview];
            
            cell.textLabel.text = @"Buddy";
            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
            
        }
            break;
        case SECTION_CONTACT_INFO: {
            cell.backgroundColor = [UIColor whiteColor];
            switch (indexPath.row) {
                case 0:
                    // WE ADD TAG_fullnamePrefField HERE //
                    if (nil != [cell viewWithTag:TAG_accountLabel])
                        [[self accountLabel] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_emailPrefField])
                        [[self emailPrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_agePrefField])
                        [[self agePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_sexPrefField])
                        [[self sexPrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_phonePrefField])
                        [[self phonePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_sendEmailButton])
                        [[self sendEmailButton] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_timerControl])
                        [[self timerControl] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_voiceSwitch])
                        [[self voiceSwitch] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchTW])
                        [[self tipSwitchTW] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchFB])
                        [[self tipSwitchFB] removeFromSuperview];
                    cell.textLabel.text = @" ";
                    [cell addSubview:[self fullnamePrefField]];
                    break;
                    
                case 1:
                    // WE ADD TAG_agePrefField HERE //
                    if (nil != [cell viewWithTag:TAG_accountLabel])
                        [[self accountLabel] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                        [[self fullnamePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_phonePrefField])
                        [[self phonePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_sexPrefField])
                        [[self sexPrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_emailPrefField])
                        [[self emailPrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_sendEmailButton])
                        [[self sendEmailButton] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_timerControl])
                        [[self timerControl] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_voiceSwitch])
                        [[self voiceSwitch] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchTW])
                        [[self tipSwitchTW] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchFB])
                        [[self tipSwitchFB] removeFromSuperview];
                    cell.textLabel.text = @" ";
                    [cell addSubview:[self agePrefField]];
                    break;
                    
                case 2:
                    // WE ADD TAG_sexPrefField HERE //
                    if (nil != [cell viewWithTag:TAG_accountLabel])
                        [[self accountLabel] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                        [[self fullnamePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_agePrefField])
                        [[self agePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_sendEmailButton])
                        [[self sendEmailButton] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_emailPrefField])
                        [[self emailPrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_phonePrefField])
                        [[self phonePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_timerControl])
                        [[self timerControl] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_voiceSwitch])
                        [[self voiceSwitch] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchTW])
                        [[self tipSwitchTW] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchFB])
                        [[self tipSwitchFB] removeFromSuperview];
                    cell.textLabel.text = @" ";
                    [cell addSubview:[self sexPrefField]];
                    break;
                    
                case 3:
                    // WE ADD TAG_emailPrefField HERE //
                    if (nil != [cell viewWithTag:TAG_accountLabel])
                        [[self accountLabel] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                        [[self fullnamePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_agePrefField])
                        [[self agePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_sexPrefField])
                        [[self sexPrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_phonePrefField])
                        [[self phonePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_sendEmailButton])
                        [[self sendEmailButton] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_timerControl])
                        [[self timerControl] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_voiceSwitch])
                        [[self voiceSwitch] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchTW])
                        [[self tipSwitchTW] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchFB])
                        [[self tipSwitchFB] removeFromSuperview];
                    cell.textLabel.text = @" ";
                    [cell addSubview:[self emailPrefField]];
                    break;
                    
                case 4:
                    // WE ADD TAG_phonePrefField HERE //
                    if (nil != [cell viewWithTag:TAG_accountLabel])
                        [[self accountLabel] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                        [[self fullnamePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_agePrefField])
                        [[self agePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_sexPrefField])
                        [[self sexPrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_emailPrefField])
                        [[self emailPrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_sendEmailButton])
                        [[self sendEmailButton] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_timerControl])
                        [[self timerControl] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_voiceSwitch])
                        [[self voiceSwitch] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchTW])
                        [[self tipSwitchTW] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchFB])
                        [[self tipSwitchFB] removeFromSuperview];
                    cell.textLabel.text = @" ";
                    [cell addSubview:[self phonePrefField]];
                    break;
                    
                case 5:
                    // WE ADD TAG_sendEmailButton HERE //
                    if (nil != [cell viewWithTag:TAG_accountLabel])
                        [[self accountLabel] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_fullNamePrefField])
                        [[self fullnamePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_agePrefField])
                        [[self agePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_sexPrefField])
                        [[self sexPrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_emailPrefField])
                        [[self emailPrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_phonePrefField])
                        [[self phonePrefField] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_timerControl])
                        [[self timerControl] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_voiceSwitch])
                        [[self voiceSwitch] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchTW])
                        [[self tipSwitchTW] removeFromSuperview];
                    if (nil != [cell viewWithTag:TAG_tipSwitchFB])
                        [[self tipSwitchFB] removeFromSuperview];

                    cell.textLabel.text = @" ";
                    [cell addSubview:[self sendEmailButton]];
                    break;
            }
            break;
        }
        default:break;
    }

    return cell;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{	
	NSString *sectionHeader = nil;
	
    switch (section) {
        case SECTION_TIMERS:
            sectionHeader = @"General";
            break;
        case SECTION_TWITTER_ACCOUNT:
            sectionHeader = @"Accounts";
            break;
        case SECTION_CONTACT_INFO:
            sectionHeader = @"Contact Information";
            break;
        case  SECTION_BUDDY:
            sectionHeader = @"Buddy";
        default:break;
    }
    
    return sectionHeader;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    [self didSelectRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self didSelectRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self resignFirstResponder];
}


- (void) didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case SECTION_VOICE_ON_OFF:
            //[delayPrefField resignFirstResponder];
            break;
            
        case SECTION_TWITTER_ACCOUNT: {
            SocialNetworkViewController *bwvc = [[SocialNetworkViewController alloc] init];
            [self.navigationController pushViewController:bwvc animated:YES];
        }
            break;
            
        case SECTION_BANNED_WORDS: {
            BannedWordsViewController *bwvc = [[BannedWordsViewController alloc] init];
            [self.navigationController pushViewController:bwvc animated:YES];
        }
            break;
            
        case SECTION_BANNED_ADDRESSES: {
            BannedAddressesViewController *bavc = [[BannedAddressesViewController alloc] init];
            [self.navigationController pushViewController:bavc animated:YES];
        }
            break;
            /*
        case SECTION_REVIEWER: {
            ReviewerViewController *rvVC = nil;
            if (isIphone5) 
                rvVC = [[ReviewerViewController alloc] initWithNibName:@"ReviewerViewController-568h" bundle:nil];
            else
                rvVC = [[ReviewerViewController alloc] initWithNibName:@"ReviewerViewController" bundle:nil];
            
            [self.navigationController pushViewController:rvVC animated:YES];
        }
            break;
             */
        case SECTION_BUDDY:
        {
            BuddyFriendViewController *bfriend = [[BuddyFriendViewController alloc] initWithNibName:@"BuddyFriendViewController" bundle:nil];
            [self.navigationController pushViewController:bfriend animated:YES];
        }
        default:
            break;
    }    
}



#pragma mark - CountDown

- (void)voiceCountdownChanged:(id)theSwitch
{
    [[NSUserDefaults standardUserDefaults] setBool:[theSwitch isOn] forKey:@"voiceCountdown"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)tipCountdownChangedTwitter:(id)theSwitch
{
    [[NSUserDefaults standardUserDefaults] setBool:![theSwitch isOn] forKey:warningControllTwitter];
        DLOG(@"%d",[[NSUserDefaults standardUserDefaults] boolForKey:warningControllTwitter]);
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)tipCountdownChangedFacebook:(id)theSwitch
{
    [[NSUserDefaults standardUserDefaults] setBool:![theSwitch isOn] forKey:warningControllFacebook];
    DLOG(@"%d",[[NSUserDefaults standardUserDefaults] boolForKey:warningControllFacebook]);
    [[NSUserDefaults standardUserDefaults] synchronize];
}



#pragma mark --UITextFieldDelegate METHODS--

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480) 
        scrollview.frame = CGRectMake(0, -240, 320, 680);
    else
        scrollview.frame = CGRectMake(0, -240, 320, 760);
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[scrollview scrollRectToVisible:textField.frame animated:YES];
	[UIView commitAnimations];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //[self processTimePrefEntry:textField.text];
    if (APP_DEBUG) NSLog (@"textFieldDidEndEditing");
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationBeginsFromCurrentState:YES];
    if (isIphone5) {
        scrollview.frame = CGRectMake(0, 0, 320, 700);
    }
    else{
        scrollview.frame = CGRectMake(0, 0, 320, 608);//700
    }
	[UIView commitAnimations];
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (APP_DEBUG) NSLog (@"textFieldShouldClear");
    return NO;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (textField == self._fullnamePrefField) {
        [textField resignFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        if (result.height == 480)
            scrollview.frame = CGRectMake(0, -280, 320, 640);
        else
            scrollview.frame = CGRectMake(0, -280, 320, 720);
        
        [UIView commitAnimations];
        [self._agePrefField becomeFirstResponder];
        return NO;
    }else if (textField == self._agePrefField) {
        [textField resignFirstResponder];        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        if (result.height == 480)
            scrollview.frame = CGRectMake(0, -320, 320, 600);
        else
            scrollview.frame = CGRectMake(0, -320, 320, 680);
        
        [UIView commitAnimations];
        [self._sexPrefField becomeFirstResponder];        
        return NO;
    }
    if (textField == self._sexPrefField) {
        [textField resignFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        if (result.height == 480)
            scrollview.frame = CGRectMake(0, -360, 320, 560);
        else
            scrollview.frame = CGRectMake(0, -360, 320, 640);
        
        [UIView commitAnimations];
        [self._emailPrefField becomeFirstResponder];
        return NO;
    }else if (textField == self._emailPrefField) {
        [textField resignFirstResponder];
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        if (result.height == 480) 
            scrollview.frame = CGRectMake(0, -400, 320, 520);
        else
            scrollview.frame = CGRectMake(0, -400, 320, 600);
    
        [UIView commitAnimations];
        [self._phonePrefField becomeFirstResponder];
        return NO;
    }else if (textField == self._phonePrefField) {
        [textField resignFirstResponder];        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        
        if (result.height == 480)
            scrollview.frame = CGRectMake(0, 0, 320, 608);
        else
            scrollview.frame = CGRectMake(0, 0, 320, 693);
        
        [UIView commitAnimations];
        
        //-- display button Logout
        [self displayButtonLogOut];
        
        return NO;
    }
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (APP_DEBUG) NSLog (@"shouldChangeCharactersInRange:%@",textField.text);
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSLog(@"textFieldShouldBeginEditing");
    numberPadShowing = (textField.keyboardType == UIKeyboardTypeNumberPad);
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (APP_DEBUG) NSLog (@"textFieldShouldEndEditing");
    return YES;
}


- (void)keyboardWillShow:(NSNotification *)note
{    
    if (3 != [AppDelegate shared].tabBarController.selectedIndex) {
        [self.navigationItem setRightBarButtonItem:nil animated:NO];
        return;
    }
    
    UIBarButtonItem *btnDone = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButton:)];
    [self.navigationItem setRightBarButtonItem:btnDone animated:NO];
}


- (void)doneButton:(id)sender
{
    [[self fullnamePrefField] resignFirstResponder];
    [[self agePrefField] resignFirstResponder];
    [[self sexPrefField] resignFirstResponder];
    [[self phonePrefField] resignFirstResponder];
    [[self emailPrefField] resignFirstResponder];
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480)
        scrollview.frame = CGRectMake(0, 0, 320, 608);
    else
        scrollview.frame = CGRectMake(0, 0, 320, 693);
    
    [self displayButtonLogOut];
}


// FROM:
// http://eureka.ykyuen.info/2010/04/12/iphone-disable-the-cutcopypaste-menu-on-uitextfield/
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    NSLog(@"canPerformAction");
	UIMenuController *menuController = [UIMenuController sharedMenuController];
	if (menuController) {
		[UIMenuController sharedMenuController].menuVisible = NO;
	}
	return NO;
}


- (UISegmentedControl*)timerControl
{
//    NSLog(@"timerControl");
    if (!self._timerControl) {
        NSArray *segmentTextContent = [[NSArray alloc] initWithObjects:@"No delay", @"10 sec", @"20 sec", @"30 sec", @"60 sec", nil];
        self._timerControl = [[UISegmentedControl alloc] initWithItems:segmentTextContent];
        [self._timerControl addTarget:self action:@selector(timerChanged:) forControlEvents:UIControlEventValueChanged];
        self._timerControl.segmentedControlStyle = UISegmentedControlStyleBar;
        self._timerControl.tag = TAG_timerControl;
        self._timerControl.frame = CGRectMake(10, 3, 300, 38);
        
        [_tableView reloadData];
    }
    return self._timerControl;
}


- (UILabel*) accountLabel
{
//    NSLog(@"accountLabel");
    if (!self._accountLabel) {
        self._accountLabel = [[UILabel alloc] initWithFrame:CGRectMake(90, 0, 200, 30)];
        [self._accountLabel setTextAlignment:NSTextAlignmentCenter];
        [self._accountLabel setBackgroundColor:[UIColor clearColor]];
        self._accountLabel.tag = TAG_accountLabel;
    }
    return self._accountLabel;
}


- (UISwitch*) voiceSwitch
{
    if (!self._voiceSwitch) {
        self._voiceSwitch = [[UISwitch alloc] init];
        [self._voiceSwitch addTarget:self action:@selector(voiceCountdownChanged:) forControlEvents:UIControlEventValueChanged];
        [self._voiceSwitch setOn:[[NSUserDefaults standardUserDefaults] boolForKey:@"voiceCountdown"]];
        self._voiceSwitch.tag = TAG_voiceSwitch;
    }
    return self._voiceSwitch;
}


- (UISwitch*) tipSwitchTW
{
    if (!self._tipSwitchTW) {
        self._tipSwitchTW = [[UISwitch alloc] init];
        [self._tipSwitchTW addTarget:self action:@selector(tipCountdownChangedTwitter:) forControlEvents:UIControlEventValueChanged];
        // set on besafe tip.
        [self._tipSwitchTW setOn:![[NSUserDefaults standardUserDefaults] boolForKey:warningControllTwitter]];
        self._tipSwitchTW.tag = TAG_tipSwitchTW;
    }
    return self._tipSwitchTW;
}


- (UISwitch*) tipSwitchFB
{
    if (!self._tipSwitchFB) {
        self._tipSwitchFB = [[UISwitch alloc] init];
        [self._tipSwitchFB addTarget:self action:@selector(tipCountdownChangedFacebook:) forControlEvents:UIControlEventValueChanged];
        // set on besafe tip.
        [self._tipSwitchFB setOn:![[NSUserDefaults standardUserDefaults] boolForKey:warningControllFacebook]];
        self._tipSwitchFB.tag = TAG_tipSwitchFB;
    }
    return self._tipSwitchFB;
}



#pragma mark - Reset textfields 

-(void) resetTextFields
{
    self._fullnamePrefField.text =@"";
    self._agePrefField.text =@"";
    self.sexPrefField.text=@"";
    self.emailPrefField.text=@"";
    self.phonePrefField.text=@"";
}



#pragma mark - UITextField

- (UITextField*) fullnamePrefField
{
    if (!self._fullnamePrefField) {
        self._fullnamePrefField = [[UITextField alloc] initWithFrame:CGRectMake(30, 6, 260, 32)];
        NSString *value = [[NSUserDefaults standardUserDefaults] stringForKey:@"FULLNAME_PREF_STRING"];
        if (!value) value = @"";
        [self._fullnamePrefField setText:[NSString stringWithFormat:@"%@",value]];
        [self._fullnamePrefField setDelegate:self];
        self._fullnamePrefField.tag = TAG_fullNamePrefField;
        [self._fullnamePrefField setBorderStyle:UITextBorderStyleRoundedRect];
        [self._fullnamePrefField setKeyboardType:UIKeyboardTypeEmailAddress];
    }
    
    if (isGetSuccess)
        self._fullnamePrefField.text = nameStr;
    else
        self._fullnamePrefField.placeholder = @"Full name";

    return self._fullnamePrefField;
}


- (UITextField*) agePrefField
{
    if (!self._agePrefField) {
        self._agePrefField = [[UITextField alloc] initWithFrame:CGRectMake(30, 6, 260, 32)];
        NSString *value = [[NSUserDefaults standardUserDefaults] stringForKey:@"AGE_PREF_STRING"];
        if (!value) value = @"";
        [self._agePrefField setText:[NSString stringWithFormat:@"%@",value]];
        [self._agePrefField setDelegate:self];
        self._agePrefField.tag = TAG_agePrefField;
        [self._agePrefField setBorderStyle:UITextBorderStyleRoundedRect];
        [self._agePrefField setKeyboardType:UIKeyboardTypeEmailAddress];
    }
    
    if (isGetSuccess)
        self._agePrefField.text = ageStr;
    else
        self._agePrefField.placeholder = @"Age";
    
    return self._agePrefField;
}


- (UITextField*) sexPrefField
{
    if (!self._sexPrefField) {
        self._sexPrefField = [[UITextField alloc] initWithFrame:CGRectMake(30, 6, 260, 32)];
        NSString *value = [[NSUserDefaults standardUserDefaults] stringForKey:@"SEX_PREF_STRING"];
        if (!value) value = @"";
        [self._sexPrefField setText:[NSString stringWithFormat:@"%@",value]];
        [self._sexPrefField setDelegate:self];
        self._sexPrefField.tag = TAG_sexPrefField;
        [self._sexPrefField setBorderStyle:UITextBorderStyleRoundedRect];
        [self._sexPrefField setKeyboardType:UIKeyboardTypeEmailAddress];
    }
    
    if (isGetSuccess)
        self._sexPrefField.text = sexStr;
    else
        self._sexPrefField.placeholder = @"Sex";
    
    return self._sexPrefField;
}


- (UITextField*) emailPrefField
{
    if (!self._emailPrefField) {
        self._emailPrefField = [[UITextField alloc] initWithFrame:CGRectMake(30, 6, 260, 32)];
        NSString *value = [[NSUserDefaults standardUserDefaults] stringForKey:@"EMAIL_PREF_STRING"];
        if (!value) value = @"";
        [self._emailPrefField setText:[NSString stringWithFormat:@"%@",value]];
        [self._emailPrefField setDelegate:self];
        self._emailPrefField.tag = TAG_emailPrefField;
        [self._emailPrefField setBorderStyle:UITextBorderStyleRoundedRect];
        [self._emailPrefField setKeyboardType:UIKeyboardTypeEmailAddress];        
    }
    
    if (isGetSuccess)
        self._emailPrefField.text = emailStr;
    else
        self._emailPrefField.placeholder = @"Email Address";
    
    return self._emailPrefField;
}


- (UITextField*) phonePrefField
{
    if (!self._phonePrefField) {
        self._phonePrefField = [[UITextField alloc] initWithFrame:CGRectMake(30, 6, 260, 32)];
        NSString *value = [[NSUserDefaults standardUserDefaults] stringForKey:@"PHONE_PREF_STRING"];
        if (!value) value = @"";
        [self._phonePrefField setText:[NSString stringWithFormat:@"%@",value]];
        [self._phonePrefField setDelegate:self];
        self._phonePrefField.tag = TAG_phonePrefField;
        [self._phonePrefField setBorderStyle:UITextBorderStyleRoundedRect];
        [self._phonePrefField setKeyboardType:UIKeyboardTypeNamePhonePad];
    }
    
    if (isGetSuccess)
        self._phonePrefField.text = phoneStr;
    else
        self._phonePrefField.placeholder = @"Phone Number";
    
    return self._phonePrefField;
}


- (UIButton*) sendEmailButton
{
    [self resignFirstResponder];
    
    if (!self._sendEmailButton) {
        self._sendEmailButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [self._sendEmailButton setTitle:@"Send Info For Updates" forState:UIControlStateNormal];
        [self._sendEmailButton addTarget:self action:@selector(clickToBtnSend:) forControlEvents:UIControlEventTouchUpInside];
        self._sendEmailButton.tag = TAG_sendEmailButton;
        self._sendEmailButton.frame = CGRectMake(60,5,200,34);
    }
    return self._sendEmailButton;
}

- (IBAction)clickToBtnSend:(id)sender
{
    //--validate textfields
    if([self validateInfo])
    {        
        //--hide button Done & dislay button Logout
        [self displayButtonLogOut];        
        [self showProgressInView:self.view target:self title:@"Sending..." selector:@selector(updateContact:)];
    }
}


#pragma mark - using API

- (void)updateContact:(MBProgressHUD*)progress
{
    NSError *error = nil;
    NSDictionary *userInfo = nil;
    
    NSString *fullName_Str = _fullnamePrefField.text;
    NSString *age_Str       = _agePrefField.text;
    NSString *sex_Str       = _sexPrefField.text;
    NSString *email_Str     = _emailPrefField.text;
    NSString *phone_Str     = _phonePrefField.text;
    facebookIdStr   = [[NSUserDefaults standardUserDefaults] objectForKey:facebookID];
    
    userInfo = [BesafeAPI updateContact:fullName_Str age:age_Str sex:sex_Str email:email_Str phone:phone_Str facebookId:facebookIdStr error:&error];
    
    if (error) {
        [progress hide:YES];
        [self showAlert:[error localizedDescription]];
        return;
    }
    else {
        NSLog(@"update success %@",[userInfo objectForKey:@"data"]);
        [progress hide:YES];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You have updated your information successfully. Thank you!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        [self resetTextFields];
        return;
    }
}


- (void)getContact
{
    NSError *error = nil;
    NSDictionary *userInfo = nil;
    
    facebookIdStr   = [[NSUserDefaults standardUserDefaults] objectForKey:facebookID];    
    userInfo = [BesafeAPI getContact:facebookIdStr error:&error];
    
    if (error) {
        NSLog(@"Get contact FAILED");
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        return;
    }
    else {
        NSLog(@"get success %@",[userInfo objectForKey:@"data"]);
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            isGetSuccess = YES;
            
            NSDictionary *dataInfo = [userInfo objectForKey:@"data"];
            nameStr     = [dataInfo objectForKey:@"full_name"];
            ageStr      = [dataInfo objectForKey:@"age"] ;
            sexStr      = [dataInfo objectForKey:@"sex"];
            emailStr    = [dataInfo objectForKey:@"email"];
            phoneStr    = [dataInfo objectForKey:@"phone"];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            [self.tableView reloadData];
        });
        
        
        return;
    }
}


- (BOOL) validateInfo
{
    if([self._fullnamePrefField.text length]==0)
	{
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Please enter full name" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[self._fullnamePrefField becomeFirstResponder];
		return NO;
	}
	if([self._agePrefField.text length]==0)
	{
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Please enter age" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[self._agePrefField becomeFirstResponder];
		return NO;
	}
    if([self._sexPrefField.text length]==0)
	{
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Please enter sex" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[self._sexPrefField becomeFirstResponder];
		return NO;
	}
    if([self._emailPrefField.text length]==0)
	{
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Please enter email address" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[self._emailPrefField becomeFirstResponder];
		return NO;
	}
    if([self._phonePrefField.text length]==0)
	{
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Please enter phone number" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[self._phonePrefField becomeFirstResponder];
		return NO;
	}
	if([self._fullnamePrefField isFirstResponder])
	{
		[self._fullnamePrefField resignFirstResponder];
	}
	if([self._agePrefField isFirstResponder])
	{
		[self._agePrefField resignFirstResponder];
	}
    if([self._sexPrefField isFirstResponder])
	{
		[self._sexPrefField resignFirstResponder];
	}
    if([self._emailPrefField isFirstResponder])
	{
		[self._emailPrefField resignFirstResponder];
	}
    if([self._phonePrefField isFirstResponder])
	{
		[self._phonePrefField resignFirstResponder];
	}
    
    return YES;
}


- (UIView*)frontFromView:(UIView*)aView
{
    UIWindow* tempWindow = nil;
    UIView *keyboard = nil;
    
    if ([[[UIApplication sharedApplication] windows] count] > 1) {
        tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    }
    
    if ([tempWindow.subviews count])
        keyboard = [tempWindow.subviews objectAtIndex:0];
    
    return (keyboard != nil ? tempWindow : aView);
}


- (MBProgressHUD*)showProgressInView:(UIView*)view target:(id)aTarget
                               title:(NSString*)titleText selector:(SEL)aSelector
{
    UIView *frontView = [self frontFromView:view];
    
    MBProgressHUD* mbhub = [[MBProgressHUD alloc] initWithView:frontView];
	mbhub.labelText = titleText;
    mbhub.removeFromSuperViewOnHide = YES;
    [mbhub setDelegate:(id)aTarget];
	[mbhub show:YES];
	[frontView addSubview:mbhub];
    [aTarget performSelector:aSelector withObject:mbhub afterDelay:0.3];
    return mbhub;
}


- (void) showAlert: (NSString*) msg
{
	[[[UIAlertView alloc] initWithTitle: nil
								 message: msg
								delegate: nil
					   cancelButtonTitle: @"OK"
					   otherButtonTitles: nil] show];
}


@end
