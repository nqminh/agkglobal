//
//  CustomCellPosters.h
//  AGKGlobal
//
//  Created by MAC_OSX on 11/27/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellPosters : UITableViewCell
{
    
}

@property (nonatomic,retain) IBOutlet UIImageView *imgAvatar;
@property (nonatomic,retain) IBOutlet UILabel *lblUsername;
@property (nonatomic,retain) IBOutlet UILabel *lblDetails;


@end
