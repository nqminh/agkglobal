//
//  CustomCellPosters.m
//  AGKGlobal
//
//  Created by MAC_OSX on 11/27/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "CustomCellPosters.h"

@implementation CustomCellPosters


@synthesize lblDetails, lblUsername, imgAvatar;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
