//
//  CustomCellSearchFriends.h
//  ShouldIBuyIt
//
//  Created by Duc Trong on 4/22/13.
//  Copyright (c) 2013 Duc Trong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellSearchFriends : UITableViewCell
{
    
}

@property (nonatomic,retain) IBOutlet UIImageView *imgAvatar;
@property (nonatomic,retain) IBOutlet UILabel *lblUserName;
@property (nonatomic,retain) IBOutlet UILabel *lblFullName;
@property (nonatomic,retain) IBOutlet UIButton *btnAddInvite;

@end
