//
//  ReviewerViewController.m
//  AGKGlobal
//
//  Created by MAC on 10/25/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "ReviewerViewController.h"

@interface ReviewerViewController ()

@end

@implementation ReviewerViewController

@synthesize isFriendRequest, isInbox, isActivity, isRequested, isSearch, isFriends, isPosters;
@synthesize btnFriendRequest, btnInbox;
@synthesize listSearch, listActivity, listRequested, listFriends, listPosters;
@synthesize refreshControl;
@synthesize tableviewSearch, tableviewFriends, tableviewInbox, tableviewPosters;
@synthesize txtSearch, lblSearch;
@synthesize viewFriends, viewSearchFriend, viewPosters, viewInbox, viewSearch;
@synthesize imgLineHeader;
@synthesize btnFriends, btnPosters, btnSearch;
@synthesize lblNumberInbox;


//--*************************************************************************--//
#pragma mark - Main

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSArray *searchArr = [[NSArray alloc] initWithObjects:@"Diego Lopez", @"Jesus Fernandez Collado", @"Fernando Pacheco",@"Alvaro Arbeloa", @"Garcia Sergio Ramos", @"Fabio Coentrao",@"David Mateos Ramajo", @"Gareth Bale", @"Angel Di Maria",@"Carlos Casemiro", @"Cristiano Ronaldo", @"Miguel Fernandez", nil];
    NSArray *postersArr = [[NSArray alloc] initWithObjects:@"David Mateos Ramajo", @"Gareth Bale", @"Angel Di Maria",@"Carlos Casemiro", @"Cristiano Ronaldo", @"Miguel Fernandez", nil];
    NSArray *friendsArr = [[NSArray alloc] initWithObjects:@"Diego Lopez", @"Jesus Fernandez Collado", @"Fernando Pacheco",@"Alvaro Arbeloa", @"Garcia Sergio Ramos", @"Fabio Coentrao",@"David Mateos Ramajo", @"Gareth Bale", @"Angel Di Maria",@"Carlos Casemiro", @"Cristiano Ronaldo", @"Miguel Fernandez", nil];
    NSArray *activityArr = [[NSArray alloc] initWithObjects:@"David Mateos Ramajo", @"Gareth Bale", @"Angel Di Maria",@"Carlos Casemiro", @"Cristiano Ronaldo", @"Miguel Fernandez", nil];
    NSArray *requestedArr = [[NSArray alloc] initWithObjects:@"Diego Lopez", @"Jesus Fernandez Collado", @"Fernando Pacheco",@"Alvaro Arbeloa", @"Garcia Sergio Ramos", @"Fabio Coentrao", nil];
    
    isActivity = YES;
    isRequested = YES;
    isFriendRequest = YES;
    isSearch = YES;
    isFriends = YES;
    isPosters = YES;
    
    lblSearch.alpha = 0;
    lblNumberInbox.alpha = 0;
    
    //--alloc some lists
    listSearch = [[NSMutableArray alloc] initWithArray:searchArr];
    listPosters = [[NSMutableArray alloc] initWithArray:postersArr];
    listFriends = [[NSMutableArray alloc] initWithArray:friendsArr];
    listActivity = [[NSMutableArray alloc] initWithArray:activityArr];
    listRequested = [[NSMutableArray alloc] initWithArray:requestedArr];
    
    //--display viewSearch when begin
    [UIView animateWithDuration:0.3 animations:^{
        viewSearch.alpha = 1;
        viewFriends.alpha = 0;
        viewPosters.alpha = 0;
    }];
    
    //-- set frame for image line top
    [imgLineHeader setFrame:CGRectMake(20, 4, 80, 3)];
    
    //-- check version ios
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    __weak ReviewerViewController *myself = self;
    
    if (sysVer < 6.0) {
        // setup pull-to-refresh
        [self.tableviewInbox addPullToRefreshWithActionHandler:^{
            [myself handleRefresh:nil];
        }];
    }
    else {
        //-- add refresh control to tableview
        refreshControl = [[UIRefreshControl alloc] init];
        [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
        refreshControl.tintColor = [UIColor colorWithRed:162/256.0f green:20/256.0f blue:18/256.0f alpha:1];
        
        [self.tableviewInbox addSubview:refreshControl];
    }
    
    // setup infinite scrolling
    [self.tableviewInbox addInfiniteScrollingWithActionHandler:^{
        [myself insertRowAtBottom];
    }];
    
    if (isFriendRequest == YES) {
        //-- selected Friend Request button
        [self clickToBtnFriends:btnFriendRequest];
    }
    if (isInbox == YES) {
        //-- selected Inbox button
        [self clickToBtnInbox:btnInbox];
    }
    
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    //set hide tabbar and navigationbar
    self.navigationController.navigationBarHidden = YES;
    [self.tabBarController setTabBarHidden:YES animated:NO];
    self.navigationItem.hidesBackButton = YES;
    
    //-- custom tableview Search
    tableviewSearch.layer.cornerRadius = 6;
    tableviewSearch.layer.borderColor = [UIColor colorWithRed:222/256.0f green:222/256.0f blue:222/256.0f alpha:1].CGColor;
    tableviewSearch.layer.borderWidth = 1;
    
    CGRect frame = [tableviewSearch frame];
    [tableviewSearch setFrame:frame];
    
    //-- custom tableview Posters
    tableviewPosters.layer.cornerRadius = 6;
    tableviewPosters.layer.borderColor = [UIColor colorWithRed:222/256.0f green:222/256.0f blue:222/256.0f alpha:1].CGColor;
    tableviewPosters.layer.borderWidth = 1;
    
    CGRect frameP = [tableviewPosters frame];
    [tableviewPosters setFrame:frameP];
    
    //-- custom tableview Inbox
    tableviewInbox.layer.cornerRadius = 6;
    tableviewInbox.layer.borderColor = [UIColor colorWithRed:222/256.0f green:222/256.0f blue:222/256.0f alpha:1].CGColor;
    tableviewInbox.layer.borderWidth = 1;
    
    CGRect frame2 = [tableviewInbox frame];
    [tableviewInbox setFrame:frame2];
    
    //-- custom tableview Friend
    tableviewFriends.layer.cornerRadius = 6;
    tableviewFriends.layer.borderColor = [UIColor colorWithRed:222/256.0f green:222/256.0f blue:222/256.0f alpha:1].CGColor;
    tableviewFriends.layer.borderWidth = 1;
    
    CGRect frame3 = [tableviewFriends frame];
    [tableviewFriends setFrame:frame3];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//--*************************************************************************--//
#pragma mark - Action

//--show tableview Friends
- (IBAction)clickToBtnFriends:(id)sender
{
    isFriendRequest = YES;
    isInbox = NO;
    
    lblNumberInbox.alpha = 0;
    
    [btnFriendRequest setSelected:YES];
    [btnInbox setSelected:NO];
    
    self.viewSearchFriend.alpha = 1;
    self.viewInbox.alpha = 0;
    
    btnFriendRequest.userInteractionEnabled = NO;
    btnInbox.userInteractionEnabled = YES;
    
}


- (IBAction)clickToBtnInbox:(id)sender
{
    isFriendRequest = NO;
    isInbox = YES;
    
    lblNumberInbox.alpha = 1;
    
    [btnFriendRequest setSelected:NO];
    [btnInbox setSelected:YES];
    
    self.viewSearchFriend.alpha = 0;
    self.viewInbox.alpha = 1;
    
    btnFriendRequest.userInteractionEnabled = YES;
    btnInbox.userInteractionEnabled = NO;
    
    if (listActivity.count>0) {
        isActivity = YES;
    }
    
    if (listRequested.count>0) {
        isRequested = YES;
    }
    
    //-- reload data
    [tableviewInbox reloadData];
}


- (IBAction)clickToBtnBack:(id)sender
{
    self.navigationController.navigationBarHidden = NO;
    [self.tabBarController setTabBarHidden:NO animated:NO];
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)clickToBtnSearch:(id)sender
{
    [btnSearch setSelected:YES];
    [btnPosters setSelected:NO];
    [btnFriends setSelected:NO];
    
    btnSearch.userInteractionEnabled = NO;
    btnPosters.userInteractionEnabled = YES;
    btnFriends.userInteractionEnabled = YES;
    
    [UIView animateWithDuration:0.3 animations:^{
        viewSearch.alpha = 1;
        viewPosters.alpha = 0;
        viewFriends.alpha = 0;
    }];
    
    //-- reload data
    [tableviewSearch reloadData];
    
    //-- set frame for image line top with animation
    CGRect frameLine = [imgLineHeader frame];
    frameLine.origin.x = 20;
    
    [UIView animateWithDuration:0.3 animations:^{
        //-- set frame for image line top
        [imgLineHeader setFrame:frameLine];
    }];
}


- (IBAction)clickToBtnShowPosters:(id)sender
{
    [btnSearch setSelected:NO];
    [btnPosters setSelected:YES];
    [btnFriends setSelected:NO];
    
    btnSearch.userInteractionEnabled = YES;
    btnPosters.userInteractionEnabled = NO;
    btnFriends.userInteractionEnabled = YES;
    
    [UIView animateWithDuration:0.3 animations:^{
        viewSearch.alpha = 0;
        viewPosters.alpha = 1;
        viewFriends.alpha = 0;
    }];
    
    //-- reload data
    [tableviewSearch reloadData];
    
    //-- set frame for image line top with animation
    CGRect frameLine = [imgLineHeader frame];
    frameLine.origin.x = 130;
    
    [UIView animateWithDuration:0.3 animations:^{
        //-- set frame for image line top
        [imgLineHeader setFrame:frameLine];
    }];
}


//-- Show tableview Friends
- (IBAction)clickToBtnShowFriends:(id)sender
{
    [btnSearch setSelected:NO];
    [btnPosters setSelected:NO];
    [btnFriends setSelected:YES];
    
    btnSearch.userInteractionEnabled = YES;
    btnPosters.userInteractionEnabled = YES;
    btnFriends.userInteractionEnabled = NO;
    
    [UIView animateWithDuration:0.3 animations:^{
        viewFriends.alpha = 1;
        viewPosters.alpha = 0;
        viewSearch.alpha = 0;
    }];
    
    //-- reload data
    [tableviewFriends reloadData];
    
    //-- set frame for image line top with animation
    CGRect frameLine = [imgLineHeader frame];
    frameLine.origin.x = 230;
    
    [UIView animateWithDuration:0.3 animations:^{
        //-- set frame for image line top
        [imgLineHeader setFrame:frameLine];
    }];
}



//--*************************************************************************--//
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int numberSection;
    
    if (tableView == tableviewInbox) {
        if (isActivity == YES && isRequested == YES) {
            numberSection = 2;
        }else if (isActivity == NO && isRequested == NO){
            numberSection = 0;
        }else{
            numberSection = 1;
        }
    }else{
        numberSection = 1;
    }
    
    return numberSection;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowCount = 0;
    
    if (tableView == tableviewSearch){
        rowCount = listSearch.count;
    }else if (tableView == tableviewFriends){
        rowCount = listFriends.count;
    }else if (tableView == tableviewPosters){
        rowCount = listPosters.count;
    }else{
        switch (section) {
            case SECTION_ACTIVITY:
                rowCount = listActivity.count;
                break;
                
            case SECTION_REQUESTED:
                rowCount = listRequested.count;
                break;
                
            default:
                break;
        }
    }
    
    return rowCount;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *titleHeader = nil;
    
    if (tableView == tableviewInbox) {
        switch (section) {
            case SECTION_ACTIVITY:
                titleHeader = @"Activity";
                break;
                
            case SECTION_REQUESTED:
                titleHeader = @"Requested";
                break;
                
            default:
                break;
        }
    }
    
    return titleHeader;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tableviewSearch)
    {
        CustomCellSearchFriends *cell = [self setTableViewCellWithIndexPath:indexPath];
        
        return cell;
    }
    else if (tableView == tableviewFriends)
    {
        CustomCellFriends *cell = [self setTableViewCellFriendsWithIndexPath:indexPath];
        
        return cell;
    }
    else if (tableView == tableviewPosters)
    {
        CustomCellPosters *cell = [self setTableViewCellPostersWithIndexPath:indexPath];
        
        return cell;
    }
    else
    {
        CustomCellMessage *cell = [self setTableViewCellMessageWithIndexPath:indexPath];
        
        return cell;
    }
}



//*************************************************************************//
#pragma mark - TableView SearchFriend

//-- set up table view cell for iPhone with tableview FindFriends
-(CustomCellSearchFriends *) setTableViewCellWithIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCellSearchFriends";
    CustomCellSearchFriends *cell = (CustomCellSearchFriends *) [tableviewSearch dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomCellSearchFriends" owner:nil options:nil];
        
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (CustomCellSearchFriends *) currentObject;
                break;
            }
        }
    }
    
    //-- set data for cell
    [self setDataForTableViewCell:cell withIndexPath:indexPath];
    
    return cell;
}


//-- set data for tableview FrindFriends
-(CustomCellSearchFriends *) setDataForTableViewCell:(CustomCellSearchFriends *)cell withIndexPath:(NSIndexPath *)indexPath
{
    cell.lblUserName.text = [listSearch objectAtIndex:indexPath.row];
    
    [self setIsSearch:YES];
    [self setIsPosters:NO];
    [self setIsFriends:NO];
    
    return cell;
}



//*************************************************************************//
#pragma mark - TableView Friends

//-- set up table view cell for iPhone with tableview FindFriends
-(CustomCellFriends *) setTableViewCellFriendsWithIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCellFriends";
    CustomCellFriends *cell = (CustomCellFriends *) [tableviewFriends dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomCellFriends" owner:nil options:nil];
        
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (CustomCellFriends *) currentObject;
                break;
            }
        }
    }
    
    //-- set data for cell
    [self setDataForTableViewCellFriends:cell withIndexPath:indexPath];
    
    return cell;
}


//-- set up table view cell for iPhone with tableview Posters
-(CustomCellPosters *) setTableViewCellPostersWithIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCellPosters";
    CustomCellPosters *cell = (CustomCellPosters *) [tableviewPosters dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomCellPosters" owner:nil options:nil];
        
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (CustomCellPosters *) currentObject;
                break;
            }
        }
    }
    
    //-- set data for cell
    [self setDataForTableViewCellPosters:cell withIndexPath:indexPath];
    
    return cell;
}


//-- set data for tableview FindFriends
-(CustomCellFriends *) setDataForTableViewCellFriends:(CustomCellFriends *)cell withIndexPath:(NSIndexPath *)indexPath
{
    cell.lblUsername.text = [listFriends objectAtIndex:indexPath.row];
    
    [self setIsSearch:NO];
    [self setIsPosters:NO];
    [self setIsFriends:YES];
    
    return cell;
}


//-- set data for tableview Posters
-(CustomCellPosters *) setDataForTableViewCellPosters:(CustomCellPosters *)cell withIndexPath:(NSIndexPath *)indexPath
{
    cell.lblUsername.text = [listPosters objectAtIndex:indexPath.row];
    
    [self setIsSearch:NO];
    [self setIsPosters:YES];
    [self setIsFriends:NO];
    
    return cell;
}



//*************************************************************************//
#pragma mark - TableView Message

//-- set up table view cell for iPhone with tableview FindFriends
-(CustomCellMessage *) setTableViewCellMessageWithIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCellMessage";
    CustomCellMessage *cell = (CustomCellMessage *) [tableviewSearch dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        //-- get nibName
        NSString *nibName = nil;
        
        switch (indexPath.section) {
            case SECTION_ACTIVITY:
                nibName = @"CustomCellMessageActivity";
                break;
                
            case SECTION_REQUESTED:
                nibName = @"CustomCellMessageFriendsRequest";
                break;
                
            default:
                break;
        }
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
        
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (CustomCellMessage *) currentObject;
                break;
            }
        }
    }
    
    //-- set data for cell
    [self setDataForTableViewCellMessage:cell withIndexPath:indexPath];
    
    return cell;
}


//-- set data for tableview FrindFriends
-(CustomCellMessage *) setDataForTableViewCellMessage:(CustomCellMessage *)cell withIndexPath:(NSIndexPath *)indexPath
{
    //--set number for inbox message
    lblNumberInbox.text = @"(12)";
    
    switch (indexPath.section) {
        case SECTION_ACTIVITY:
            cell.lblUsername.text = [listActivity objectAtIndex:indexPath.row];
            
            if (indexPath.row % 2) {
                cell.lblActivity.text = @"has posted a new post";
            }else{
                cell.lblActivity.text = @"has tweeted a new tweet";
            }
            
            break;
            
        case SECTION_REQUESTED:
            cell.lblUsername.text = [listRequested objectAtIndex:indexPath.row];
            break;
            
        default:
            break;
    }
    
    return cell;
}



//--*************************************************************************--//
#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int height = 0;
    
    if (tableView == tableviewInbox) {
        height = 70;
    }else{
        height = 50;
    }
        
    return height;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    int height = 0;
    if (tableView == tableviewInbox) {
        height = 30;
    }else{
        height = 0;
    }
    
    return height;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



//************************************************************************//
#pragma mark - Refresh UITableView

//-- update data when pull to refresh UITableView
- (IBAction)handleRefresh:(id)sender
{
    //
}


- (void)insertRowAtBottom
{
    //
}



#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


@end
