//
//  ReviewerViewController.h
//  AGKGlobal
//
//  Created by MAC on 10/25/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "CustomCellMessage.h"
#import "CustomCellFriends.h"
#import "CustomCellPosters.h"
#import "CustomCellSearchFriends.h"
#import "UIView+HidingView.h"
#import "AppDelegate.h"

#define SECTION_ACTIVITY          0
#define SECTION_REQUESTED         1
#define SECTION_BOTH_ACT_REQ      2

@interface ReviewerViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    NSMutableArray         *listSearch;
    NSMutableArray         *listPosters;
    NSMutableArray         *listFriends;
    NSMutableArray         *listActivity;
    NSMutableArray         *listRequested;
    
    IBOutlet UIButton *btnBack;
    
    int indexCellRequestReject;
    int indexCellRequestAccept;
    
    BOOL isFriendRequest;
    BOOL isSearch;
    BOOL isFriends;
    BOOL isPosters;
    BOOL isInbox;
    BOOL isActivity;
    BOOL isRequested;
}

@property (nonatomic,retain) IBOutlet UIView        *viewSearchFriend;
@property (nonatomic,retain) IBOutlet UIView        *viewSearch;
@property (nonatomic,retain) IBOutlet UIView        *viewPosters;
@property (nonatomic,retain) IBOutlet UIView        *viewFriends;
@property (nonatomic,retain) IBOutlet UIView        *viewInbox;

@property (nonatomic,retain) IBOutlet UITableView   *tableviewSearch;
@property (nonatomic,retain) IBOutlet UITableView   *tableviewPosters;
@property (nonatomic,retain) IBOutlet UITableView   *tableviewFriends;
@property (nonatomic,retain) IBOutlet UITableView   *tableviewInbox;

@property (nonatomic,retain) IBOutlet UITextField   *txtSearch;
@property (nonatomic,retain) IBOutlet UILabel       *lblSearch;
@property (nonatomic,retain) IBOutlet UILabel       *lblNumberInbox;
@property(nonatomic,retain) UIRefreshControl        *refreshControl;

@property (nonatomic,retain) IBOutlet UIButton      *btnSearch;
@property (nonatomic,retain) IBOutlet UIButton      *btnPosters;
@property (nonatomic,retain) IBOutlet UIButton      *btnFriends;
@property (retain,nonatomic) IBOutlet UIImageView   *imgLineHeader;

@property (assign) BOOL isFriendRequest;
@property (assign) BOOL isInbox;
@property (assign) BOOL isActivity;
@property (assign) BOOL isRequested;
@property (assign) BOOL isSearch;
@property (assign) BOOL isPosters;
@property (assign) BOOL isFriends;

@property (nonatomic,retain) IBOutlet UIButton      *btnInbox;
@property (nonatomic,retain) IBOutlet UIButton      *btnFriendRequest;

@property (nonatomic,retain) NSMutableArray         *listSearch;
@property (nonatomic,retain) NSMutableArray         *listPosters;
@property (nonatomic,retain) NSMutableArray         *listFriends;
@property (nonatomic,retain) NSMutableArray         *listActivity;
@property (nonatomic,retain) NSMutableArray         *listRequested;

//--Action
- (IBAction)clickToBtnFriends:(id)sender;
- (IBAction)clickToBtnInbox:(id)sender;
- (IBAction)clickToBtnBack:(id)sender;
- (IBAction)clickToBtnSearch:(id)sender;
- (IBAction)clickToBtnShowPosters:(id)sender;
- (IBAction)clickToBtnShowFriends:(id)sender;


@end
