//
//  CustomCellMessage.m
//  ShouldIBuyIt
//
//  Created by Duc Trong on 5/4/13.
//  Copyright (c) 2013 Vmodev. All rights reserved.
//

#import "CustomCellMessage.h"

@implementation CustomCellMessage

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
