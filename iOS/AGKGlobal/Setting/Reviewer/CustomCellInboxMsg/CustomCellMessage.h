//
//  CustomCellMessage.h
//  ShouldIBuyIt
//
//  Created by Duc Trong on 5/4/13.
//  Copyright (c) 2013 Vmodev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellMessage : UITableViewCell
{
    
}

@property (nonatomic,retain) IBOutlet UIImageView *imgAvatar;
@property (nonatomic,retain) IBOutlet UIButton *btnAvatar;

@property (nonatomic,retain) IBOutlet UIButton *btnAccept;
@property (nonatomic,retain) IBOutlet UIButton *btnIgnore;
@property (nonatomic,retain) IBOutlet UIButton *btnNumberInbox;

@property (nonatomic,retain) IBOutlet UIButton *btnApprove;
@property (nonatomic,retain) IBOutlet UIButton *btnDeny;

@property (nonatomic,retain) IBOutlet UILabel *lblNumberInbox;
@property (nonatomic,retain) IBOutlet UILabel *lblUsername;
@property (nonatomic,retain) IBOutlet UILabel *lblActivity;

@end
