//
//  CustomCellFriends.h
//  AGKGlobal
//
//  Created by MAC on 10/28/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellFriends : UITableViewCell
{
    
}

@property (nonatomic,retain) IBOutlet UIImageView *imgAvatar;
@property (nonatomic,retain) IBOutlet UIButton *btnAvatar;

@property (nonatomic,retain) IBOutlet UIButton *btnAccept;
@property (nonatomic,retain) IBOutlet UIButton *btnIgnore;

@property (nonatomic,retain) IBOutlet UILabel *lblUsername;
@property (nonatomic,retain) IBOutlet UILabel *lblDetails;

@end
