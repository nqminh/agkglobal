//
//  BannedAddressesViewController.m
//  BeSafe
//
//  Created by Jeff Jolley on 4/11/13.
//
//

#import "BannedAddressesViewController.h"
#import "AddBannedViewController.h"

@interface BannedAddressesViewController ()

@end

@implementation BannedAddressesViewController

@synthesize _bannedList;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Blocked Addresses";
    //ADD BUTTON
    UIBarButtonItem *btnNew = [[UIBarButtonItem alloc]
                               initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                               target:self
                               action:@selector(addBanned:)];
    
    [self.navigationItem setRightBarButtonItem:btnNew animated:NO];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self._bannedList = nil;
    [self.tableView reloadData];
    
    //-- change title color
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self bannedList].count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }
    
    // Configure the cell...
    cell.textLabel.text = [[self bannedList] objectAtIndex:indexPath.row];
    
    return cell;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [[self bannedList] removeObjectAtIndex:indexPath.row];
        [[NSUserDefaults standardUserDefaults] setObject:[self bannedList] forKey:@"BANNED_ADDRESSES"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        self._bannedList = nil;
        [self.tableView reloadData];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSMutableArray*) bannedList {
    if (!self._bannedList) {
        self._bannedList = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_ADDRESSES"] mutableCopy];
    }
    return self._bannedList;
}


- (void)addBanned:(id)sender
{
    AddBannedViewController *abvc = [[AddBannedViewController alloc] init];
    abvc.mode = BANNED_MODE_ADDRESS;
    [self.navigationController pushViewController:abvc animated:YES];
}


@end
