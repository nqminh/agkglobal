//
//  BuddyFriendCell.h
//  AGKGlobal
//
//  Created by Dung on 2/26/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h> 
#import "CustomXibCell.h"

@interface BuddyFriendCell : CustomXibCell
{
    
}
@property (weak, nonatomic) IBOutlet UILabel *lblBuddyFriend;
@property (weak, nonatomic) IBOutlet UIButton *btnBuddy;

- (IBAction)clickUpdateBuddy:(id)sender;

@end
