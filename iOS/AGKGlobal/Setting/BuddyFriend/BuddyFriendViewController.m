//
//  BuddyFriendViewController.m
//  AGKGlobal
//
//  Created by Dung on 2/26/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import "BuddyFriendViewController.h" 
#import "ControlObj.h" 
#import "User.h"   
#import "BesafeAPI.h"
#import "UIView+HidingView.h"
#import "BesafeUser.h" 
#import "BuddyFriendCell.h" 
#import "CustomXibCell.h"
#import "AppDelegate.h"
#import <Social/Social.h>  
#import "VMUsers.h"



@interface BuddyFriendViewController ()
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSMutableArray *continentList;
@end

@implementation NSJSONSerialization (NSJSONSerialization_MutableBugFix)

+ (id)JSONObjectWithDataFixed:(NSData *)data options:(NSJSONReadingOptions)opt error:(NSError **)error
{
    id object = [NSJSONSerialization JSONObjectWithData:data options:opt error:error];
    
    if (opt & NSJSONReadingMutableContainers) {
        return [self JSONMutableFixObject:object];
    }
    
    return object;
}

+ (id)JSONMutableFixObject:(id)object
{
    if ([object isKindOfClass:[NSDictionary class]]) {
        // NSJSONSerialization creates an immutable container if it's empty (boo!!)
        if ([object count] == 0) {
            object = [object mutableCopy];
        }
        
        for (NSString *key in [object allKeys]) {
            [object setObject:[self JSONMutableFixObject:[object objectForKey:key]] forKey:key];
        }
    } else if ([object isKindOfClass:[NSArray class]]) {
        // NSJSONSerialization creates an immutable container if it's empty (boo!!)
        if (![object count] == 0) {
            object = [object mutableCopy];
        }
        
        for (NSUInteger i = 0; i < [object count]; ++i) {
            [object replaceObjectAtIndex:i withObject:[self JSONMutableFixObject:[object objectAtIndex:i]]];
        }
    }
    
    return object;
}

@end


@implementation BuddyFriendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization        
    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.arrFriend  = [ControlObj getAllUser];
    self.arrBesafeUser = [[NSMutableArray alloc] init];
    //[self getFriends];
    [self initBesafeBuddyDict];
    for (User *user in self.arrFriend)
    {
        self.listID = [NSString stringWithFormat:@"%@,%@",user.userUID,self.listID];
    }
    NSLog(@"chuoi lay dc %@",self.listID);
    [self showProgressInView:self.view target:self title:@"Getting list..." selector:@selector(getListFriendAccountBesafe:)];
    self.isUpdateBuddy = [[NSUserDefaults standardUserDefaults] integerForKey:ONLY_CLICK_BUDDY];
      
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewDidDisappear:(BOOL)animated
{
    [self saveBesafeBuddy];
}


#pragma mark - Progress delegate 

- (MBProgressHUD*)showProgressInView:(UIView*)view target:(id)aTarget
                               title:(NSString*)titleText selector:(SEL)aSelector
{
    UIView *frontView = [self frontFromView:view];
    
    MBProgressHUD* mbhub = [[MBProgressHUD alloc] initWithView:frontView];
	mbhub.labelText = titleText;
    mbhub.removeFromSuperViewOnHide = YES;
    [mbhub setDelegate:(id)aTarget];
	[mbhub show:YES];
	[frontView addSubview:mbhub];
    [aTarget performSelector:aSelector withObject:mbhub afterDelay:0.3];
    return mbhub;
}


- (UIView*)frontFromView:(UIView*)aView
{
    UIWindow* tempWindow = nil;
    UIView *keyboard = nil;
    
    if ([[[UIApplication sharedApplication] windows] count] > 1) {
        tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    }
    
    if ([tempWindow.subviews count])
        keyboard = [tempWindow.subviews objectAtIndex:0];
    
    return (keyboard != nil ? tempWindow : aView);
}

- (void) showAlert: (NSString*) msg
{
	[[[UIAlertView alloc] initWithTitle: nil
                                message: msg
                               delegate: nil
                      cancelButtonTitle: @"OK"
                      otherButtonTitles: nil] show];
}

#pragma mark - Get friend  

- (void)networkNotFound
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationItem setRightBarButtonItem:nil animated:NO];
        [self.navigationItem setLeftBarButtonItem:nil animated:NO];
        appDelegate.isNetworkNotFound = YES;        
    });
}


- (void)getFriends
{
    //https://api.twitter.com/1/friends/ids.json?cursor=-1&screen_name=twitterapi
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/friends/list.json?cursor=-1"];
    NSDictionary *params = nil;
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:myUrl parameters:params];
    
    request.account = [appDelegate selectedTwitterAccount];
    NSLog(@"twitter perform request 2");
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if(error){
             [self networkNotFound];
         }else {
             NSError *jsonError = nil;
             NSDictionary *result_dict = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
             
             NSArray *friends_users = [result_dict objectForKey:@"users"];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (APP_DEBUG) DLOG(@"FRIEND USER TW%@",friends_users);
                 
                 for (int i=0; i<friends_users.count; i++) {
                     if (APP_DEBUG) DLOG(@"FRIEND USER TW %@",result_dict);                                        
                     NSString *idStr = [[friends_users objectAtIndex:i] objectForKey:@"id_str"];
                     self.listIDTW = [NSString stringWithFormat:@"%@,%@",idStr,self.listIDTW];
                     NSLog(@"chuoi lay dc %@",self.listIDTW);
                     [self getListFriendAccountBesafeTW:self.listIDTW];
                 }
             });
         }
     }];
}

#pragma mark - TableView Delegate 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [self.arrBesafeUser count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BuddyFriendCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Buddy"];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"BuddyFriendCell" owner:self options:nil]objectAtIndex:0];
        [BuddyFriendCell route:tableView target:self forCell:cell];
    }
               
    if ([self.arrBesafeUser count] > 0)
    {
        self.btnUpdateBuddy = cell.btnBuddy;
        BesafeUser *user = [[BesafeUser alloc]initWithDictionary:[self.arrBesafeUser objectAtIndex:indexPath.row]];
        cell.lblBuddyFriend.text = user.bs_full_name;
        self.buddyIDStr = user.bs_userid;
        NSLog(@"BUDDY ID UPDATE %@",self.buddyIDStr);
        
        if ([[self.dictBesafeBuddy objectForKey:self.buddyIDStr] integerValue] == 1 )
        {         
            
            self.btnUpdateBuddy.backgroundColor = [UIColor redColor];
            [self.btnUpdateBuddy setTitle:@"Remove Buddy" forState:UIControlStateNormal];                       
            
        }
        else
        {
            self.btnUpdateBuddy.backgroundColor = [UIColor colorWithRed:90.0/255 green:155.0/255.0 blue:0 alpha:1.0];
            [self.btnUpdateBuddy setTitle:@"Update Buddy" forState:UIControlStateNormal];
            
        }
            
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryNone;  
           
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BuddyFriendCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"BuddyFriendCell" owner:self options:nil] objectAtIndex:0];
    return cell.frame.size.height;
}


- (void) clickUpdateBuddy:(id)sender atIndexPath:(NSIndexPath *)indexPath 
{
    BesafeUser *bsUser = [[BesafeUser alloc]initWithDictionary:[self.arrBesafeUser objectAtIndex:indexPath.row]];
    if (self.isUpdateBuddy)
    {       
        
        if ([[self.dictBesafeBuddy objectForKey:bsUser.bs_userid] integerValue] == 1)
        {
            //--Remove buddy
            BesafeUser *bsUser = [[BesafeUser alloc]initWithDictionary:[self.arrBesafeUser objectAtIndex:indexPath.row]];
            VMUsers *loginUser = [VMUsers readData];
            NSLog(@"PARAM REMOVE %@ - %@",loginUser.userId_login,bsUser.bs_userid);
            [self removeAccountBuddy:@"facebook" userid:loginUser.userId_login buddy:bsUser.bs_userid];
        }
        else
        {
            [self showAlert:@"You have only update Buddy one times!"];
        }        
    }
    else
    {
        
        VMUsers *loginUser = [VMUsers readData];
        NSString *strId = @"";
        //--check user_id nil get user_id store in nsuserdefault.
        if ([loginUser.userId_login length] == 0)
        {
            strId = [[NSUserDefaults standardUserDefaults] objectForKey:BESAFE_USER_ID_LOGIN];
        }
        else
        {
            strId = loginUser.userId_login;
        }
        NSLog(@"PARAM %@ - %@",loginUser.userId_login,bsUser.bs_userid);
        [self updateAccountToBuddy:@"facebook" userid:loginUser.userId_login buddy:bsUser.bs_userid];
    }
}

#pragma mark - Manage Besafe Buddy

- (void) initBesafeBuddyDict
{
    self.dictBesafeBuddy = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:SAVE_BESAFE_BUDDY]];
    if (self.dictBesafeBuddy == nil)
    {
        self.dictBesafeBuddy = [[NSMutableDictionary alloc] init];
    }     
}

- (void) saveBesafeBuddy
{
    
        [[NSUserDefaults standardUserDefaults] setObject:self.dictBesafeBuddy forKey:SAVE_BESAFE_BUDDY];
        [[NSUserDefaults standardUserDefaults] setInteger:self.isUpdateBuddy forKey:ONLY_CLICK_BUDDY];
        if ([self.buddyIDUpdatedByUSer length] > 0)
        {
            [self saveBuddyIdUpdated:self.buddyIDUpdatedByUSer];
        }    
        [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Request API 

- (void) getListFriendAccountBesafe:(MBProgressHUD*)progess
{
    NSError *error = nil;
    NSDictionary *dict = nil;    
    
    dict = [BesafeAPI getListUserRegistedBesafe:@"facebook" withFriend:self.listID error:&error];
    
    if (error)
    {
        [progess hide:YES];
        [self showAlert:[error localizedDescription]];
    }
    else
    {        
        [progess hide:YES];
        self.arrBesafeUser = [dict objectForKey:@"data"];
        
        if ([self.dictBesafeBuddy count] == 0)
        {
            for (NSInteger i = 0;i < [self.arrBesafeUser count];i++)
            {
                BesafeUser *user = [[BesafeUser alloc] initWithDictionary:[self.arrBesafeUser objectAtIndex:i]];
                [self.dictBesafeBuddy setValue:@"0" forKey:user.bs_userid];
            }

        }
        
        [self.tblFriendFB reloadData];
        
    }
           
}


- (void) getListFriendAccountBesafeTW:(NSString*)list_id
{
    NSError *error = nil;
    NSDictionary *dict = nil;
    
    dict = [BesafeAPI getListUserRegistedBesafe:@"twitter" withFriend:list_id error:&error];
    
    if (error)
    {        
        [self showAlert:[error localizedDescription]];
    }
    else
    {
        self.isUpdateBuddy = 1;
        self.arrBesafeUser = [dict objectForKey:@"data"];
        [self initBesafeBuddyDict];
        [self.tblFriendFB reloadData];
        
    }
    
}

#pragma mark - Update Buddy API 

- (void) saveBuddyIdUpdated:(NSString*)buddy_id
{
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    delegate.buddy_id_update = buddy_id;        
}


- (void) updateAccountToBuddy:(NSString*)typeAcc userid:(NSString*)userid buddy:(NSString*)buddy_id
{
    NSError *error = nil;
    NSDictionary *dict = nil;
    
    dict = [BesafeAPI updateAccountToBuddy:typeAcc userID:userid buddyID:buddy_id error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
    }
    else
    {
        self.isUpdateBuddy = 1;
        //--remark besafe user id friend become to buddy_id 
        [self.dictBesafeBuddy setValue:@"1" forKey:buddy_id];
        [self showAlert:[dict objectForKey:@"msg"]];
        [self.tblFriendFB reloadData];
        //--save buddy id has just been updated
        self.buddyIDUpdatedByUSer = [[dict objectForKey:@"data"] objectForKey:@"buddy_id"];
    }
}


- (void) removeAccountBuddy:(NSString*)typeAcc userid:(NSString*)userid buddy:(NSString*)buddy_id
{
    NSError *error = nil;
    NSDictionary *dict = nil;
    
    dict = [BesafeAPI removeAccountBuddy:typeAcc userID:userid buddyID:buddy_id error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
    }
    else
    {
        self.isUpdateBuddy = 0;        
        [self.dictBesafeBuddy setValue:@"0" forKey:buddy_id];
        [self showAlert:[dict objectForKey:@"msg"]];
        [self.tblFriendFB reloadData];
    }

}

@end
