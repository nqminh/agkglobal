//
//  BuddyFriendViewController.h
//  AGKGlobal
//
//  Created by Dung on 2/26/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h> 
#import "FacebookUtils.h"  
#import "MBProgressHUD.h"

@interface BuddyFriendViewController : UIViewController<FacebookUtilsDelegate,UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate,NSURLConnectionDelegate>
{
    
}

@property (weak, nonatomic)  IBOutlet UITableView *tblFriendFB;
@property (strong,nonatomic) NSMutableArray      *arrFriend;
@property (strong,nonatomic) NSMutableArray      *arrBesafeUser;
@property (strong,nonatomic) NSMutableDictionary *dictBesafeBuddy;
@property (strong,nonatomic) NSString *listID;
@property (strong,nonatomic) NSString *listIDTW;
@property (strong,nonatomic) NSString *buddyIDStr;
@property (assign,nonatomic) NSInteger isUpdateBuddy;
@property (strong,nonatomic) NSString *buddyIDUpdatedByUSer;
@property (weak,nonatomic)   IBOutlet UIButton *btnUpdateBuddy;

@end
