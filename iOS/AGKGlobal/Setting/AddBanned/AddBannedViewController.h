//
//  AddBannedViewController.h
//  BeSafe
//
//  Created by Jeff Jolley on 4/11/13.
//
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface AddBannedViewController : UIViewController {
    
}

@property BOOL                                          mode;
@property (nonatomic, retain) UITextField               *bannedTextField;

- (void)saveBanned:(id)sender;

@end
