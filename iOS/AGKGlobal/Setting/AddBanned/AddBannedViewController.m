//
//  AddBannedViewController.m
//  BeSafe
//
//  Created by Jeff Jolley on 4/11/13.
//
//

#import "AddBannedViewController.h"

@interface AddBannedViewController ()

@end

@implementation AddBannedViewController

@synthesize bannedTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.bannedTextField = [[UITextField alloc] initWithFrame:CGRectMake(8, 8, 304, 34)];
    self.bannedTextField.borderStyle = UITextBorderStyleBezel;
    [self.view addSubview:self.bannedTextField];
    [self.bannedTextField becomeFirstResponder];

    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    [saveButton addTarget:self action:@selector(saveBanned:) forControlEvents:UIControlEventTouchUpInside];
    saveButton.frame = CGRectMake(160, 50, 120, 44);
    [self.view addSubview:saveButton];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)saveBanned:(id)sender
{
    NSString *defaultString = @"";
    if (self.mode == BANNED_MODE_WORDS) {
        defaultString = @"BANNED_WORDS";
    } else {
        defaultString = @"BANNED_ADDRESSES";
    }
    NSArray *nsa = [[NSUserDefaults standardUserDefaults] arrayForKey:defaultString];
    NSMutableArray *nsma = [[NSMutableArray alloc] init];
    if (nil != nsa) nsma = [nsa mutableCopy];
    [nsma addObject:[self.bannedTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
    [[NSUserDefaults standardUserDefaults] setObject:nsma forKey:defaultString];
    BOOL result = [[NSUserDefaults standardUserDefaults] synchronize];
    if (result) {
        NSLog(@"synchronize:success:%d",nsma.count);
    } else {
        NSLog(@"synchronize:fail:%d",nsma.count);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
