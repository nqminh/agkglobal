//
//  BuddyDBService.m
//  DatabaseSample
//
//  Created by Dung on 3/22/14.
//  Copyright (c) 2014 Dung. All rights reserved.
//

#import "BSDBService.h" 
#import "BuddyDBConfig.h" 
#import "BesafeUser.h" 

@interface BSDBService()

@property (nonatomic,strong) FMDatabaseQueue *databaseQueue;
@property (nonatomic,strong) NSString *databaseName;

@end

BSDBService *sharedManager;
@implementation BSDBService

#pragma mark - Shared

+ (void) createSharedManager
{
    NSLog(@"");
    
    @synchronized(self)
    {
        if (sharedManager == nil)
        {
            sharedManager = [[BSDBService alloc] init];
        }
    }
}

+ (void) createSharedManagerWithDatabase:(NSString*)databaseName
{
     NSLog(@"");
    
    @synchronized(self)
    {
        if (sharedManager == nil)
        {
            sharedManager = [[BSDBService alloc] initWithDatabaseName:databaseName];
        }
    }
}

+ (BSDBService *)sharedManager
{
     NSLog(@"");
    
    if (sharedManager == nil)
    {
        [BSDBService createSharedManager];
    }
    return sharedManager;
}

+ (void) destroySharedManager
{
     NSLog(@"");
    
    @synchronized(self)
    {
        sharedManager = nil;
    }
}


#pragma mark - Database

+ (BOOL) databaseExists:(NSString*)databaseName
{
    NSLog(@"");
    
    return [[NSFileManager defaultManager] fileExistsAtPath:[BuddyDBConfig databaseAppPathFor:databaseName]];
}

+ (BOOL) createDatabase:(NSString*)databaseName
{
     NSLog(@"");
    
    return [[NSFileManager defaultManager] copyItemAtPath:[BuddyDBConfig databaseBundlePathFor:databaseName]
                                                   toPath:[BuddyDBConfig databaseAppPathFor:databaseName] error:nil];
}

+ (BOOL) deleteDatabase:(NSString*)databaseName
{
     NSLog(@"");
    
    return [[NSFileManager defaultManager] removeItemAtPath:[BuddyDBConfig databaseAppPathFor:databaseName] error:nil];
}


#pragma mark - Init

- (id)init
{
     NSLog(@"");
    
    return [self initWithDatabaseName:[BuddyDBConfig databaseName]];
}

- (id)initWithDatabaseName:(NSString *)databaseName
{
     NSLog(@"");
    
    self = [super init];
    if (self != nil)
    {
        self.databaseName = databaseName;
        self.databaseQueue = [[FMDatabaseQueue alloc] initWithPath:[BuddyDBConfig databasePath]];
    }
    return self;
}


#pragma mark - City

- (NSArray *)getCityWithKey:(NSString *)key
             optionalColumn:(NSString *)column
{
     NSLog(@"");
    
    NSMutableArray *cities = [NSMutableArray array];
    /*
    [self.databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        NSString *query;
        if (column.length > 0 && key.length > 0)
        {
            query = [NSString stringWithFormat:@"SELECT * FROM tblCities WHERE %@ = '%@'", column, key];
        }
        else
        {
            query = @"SELECT * FROM tblCities";
        }
        
        FMResultSet *results = [db executeQuery:query];
        while([results next])
        {
            TEHCityEntities *city = [[TEHCityEntities alloc] init];
            
            city.cityID           = [results intForColumn:@"cityID"];
            city.cityName         = [results stringForColumn:@"cityName"];
            city.cityURL          = [results stringForColumn:@"cityURL"];
            
            [cities addObject:city];
        }
        [results close];
    }];
    */
    return cities;
} 

- (BOOL) insertBuddy:(BesafeUser *)buddies
{
     NSLog(@"");
    
    __block BOOL success = NO;
    
    [self.databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        
        success = [db executeUpdate:@"INSERT INTO besafe_user (id, full_name, age, sex, email, phone, facebook_id, twitter_id, birthday, zip_code) VALUES (?, ?, ?, ?, ?, ? ,? , ? ,?, ?);",
                   buddies.bs_userid,
                   buddies.bs_full_name,
                   buddies.bs_age,buddies.bs_sex,
                   buddies.bs_email,
                   buddies.bs_phone,
                   buddies.bs_facebook_id,
                   buddies.bs_twitter_id,
                   buddies.bs_birthday,
                   buddies.bs_zip_code,                 
                   nil];
    }];
    
    return success;
}


- (BOOL) deleteBuddyLogin
{
    NSLog(@"");
    __block BOOL success = NO;
    [self.databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        success = [db executeUpdate:@"DELETE FROM besafe_user"];
    }];
    return success;
}

- (NSMutableArray *) getCheckAccounUser
{
    NSLog(@"");
    
    NSMutableArray *userArr = [[NSMutableArray alloc] init];
    [self.databaseQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSString *query = @"SELECT * FROM besafe_user";
        FMResultSet *result = [db executeQuery:query];
        while ([result next])
        {
            BesafeUser *user        = [[BesafeUser alloc] init];
            user.bs_userid          = [result stringForColumn:@"id"];
            user.bs_full_name       = [result stringForColumn:@"full_name"];
            user.bs_age             = [result stringForColumn:@"age"];
            user.bs_sex             = [result stringForColumn:@"sex"];
            user.bs_email           = [result stringForColumn:@"email"];
            user.bs_phone           = [result stringForColumn:@"phone"];
            user.bs_facebook_id     = [result stringForColumn:@"facebook_id"];
            user.bs_twitter_id      = [result stringForColumn:@"twitter_id"];
            user.bs_birthday        = [result stringForColumn:@"birthday"];
            user.bs_zip_code        = [result stringForColumn:@"zip_code"];          
            
            [userArr addObject:user];
            
        }
        [result close];
    }];
    return userArr;
}

@end
