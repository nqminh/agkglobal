//
//  TEHDBConfig.m
//  TehuanPrj
//
//  Created by Hong on 1/3/14.
//  Copyright (c) 2014 vmodev. All rights reserved.
//

#import "BuddyDBConfig.h"

NSArray *versions;
BuddyDBConfig *sharedConfig;

@implementation BuddyDBConfig

+ (BuddyDBConfig *) sharedConfigObject
{
    @synchronized (self)
    {
        if (sharedConfig == nil)
        {
            sharedConfig = [[BuddyDBConfig alloc] init];
        }
    }
    return sharedConfig;
}

/*
 * Version DB names - sorted form oldes to newest.
 */
+ (NSArray *) databaseVersionNames
{
    if (versions == nil)
    {
        versions = [NSArray arrayWithObjects:
                    @"BesafeDB.sqlite", nil];
    }
    
    return versions;
}

+ (NSString *) databaseBundlePathFor:(NSString*)databaseName
{
    return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
}

+ (NSString *) databaseAppPathFor:(NSString*)databaseName
{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    return [documentDir stringByAppendingPathComponent:databaseName];
}

+ (NSString *) databaseName
{
    return [[BuddyDBConfig databaseVersionNames] lastObject];
}

+ (NSString *) databasePath
{
    return [BuddyDBConfig databaseAppPathFor:[BuddyDBConfig databaseName]];
}

+ (NSString*) currentDatabaseVersion
{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    
    
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentDir error:nil];
    int maxVer = -1;
    for (NSString *file in files)
    {
        if ([file hasSuffix:@".sqlite"])
        {
            int fileVer = [[BuddyDBConfig databaseVersionNames] indexOfObject:file];
            if (fileVer != NSNotFound)
            {
                if (fileVer > maxVer)
                {
                    maxVer = fileVer;
                }
            }
        }
    }
    
    if (maxVer >= 0)
    {
        return [[BuddyDBConfig databaseVersionNames] objectAtIndex:maxVer];
    }
    
    return nil;
}
@end
