//
//  BuddyDBConfig.h
//  DatabaseSample
//
//  Created by Dung on 3/22/14.
//  Copyright (c) 2014 Dung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BuddyDBConfig : NSObject

+ (NSArray*)  databaseVersionNames;
+ (NSString*) databaseBundlePathFor:(NSString*)databaseName;
+ (NSString*) databaseAppPathFor:(NSString*)databaseName;
+ (NSString*) databaseName;
+ (NSString*) databasePath;
+ (NSString*) currentDatabaseVersion;


@end
