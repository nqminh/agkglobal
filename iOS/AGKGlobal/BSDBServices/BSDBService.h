//
//  BuddyDBService.h
//  DatabaseSample
//
//  Created by Dung on 3/22/14.
//  Copyright (c) 2014 Dung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "FMResultSet.h"
#import "FMDatabaseAdditions.h" 


@class BSDBService;
@class BesafeUser;

typedef void (^BSDBServiceSelectBlock) (NSArray *results);
typedef void (^BSDBServiceInsertBlock) (BOOL success);
typedef void (^BSDBServiceUpdateBlock) (BOOL success);
typedef void (^BSDBServiceBlock) (BOOL success);

@interface BSDBService : NSObject


// Shared manager
+ (void) createSharedManager;
+ (void) createSharedManagerWithDatabase:(NSString*)databaseName;
+ (BSDBService *) sharedManager;
+ (void) destroySharedManager;

//Check / Create / Delete DB
+ (BOOL) databaseExists:(NSString*)databaseName;
+ (BOOL) createDatabase:(NSString*)databaseName;
+ (BOOL) deleteDatabase:(NSString*)databaseName;

//Initialization
- (id) initWithDatabaseName:(NSString*)databaseName;

//City
- (NSArray *) getCityWithKey:(NSString *) key
              optionalColumn:(NSString *) column;
- (BOOL) insertBuddy:(BesafeUser *) buddies;
- (BOOL) deleteBuddyLogin;

- (NSMutableArray *) getCheckAccounUser;

@end
