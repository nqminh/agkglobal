//
//  SignInViewController.m
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 10/10/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "SignInViewController.h"
#import "RegisterViewController.h"

@interface SignInViewController ()

@end

@implementation SignInViewController

@synthesize txtEmail, txtPassword, scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.title = NSLocalizedString(@"Sign in", @"Sign in");
        self.tabBarItem.image = [UIImage imageNamed:@"tab_6_setting"];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(twitterAccountFound) name:@"accountFound" object:nil];
    
    return self;
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isLogOut || [[NSUserDefaults standardUserDefaults] boolForKey:@"LOG_OUT"]) {
        
        if (isIphone5)
            scrollView.frame = CGRectMake(0, 0, 320, 700);
        else
            scrollView.frame = CGRectMake(0, 0, 320, 600);
    }
        
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [tap setCancelsTouchesInView:NO];
    
    if (isIphone5) 
        scrollView.frame = CGRectMake(0, 0, 320, 548);
    else
        scrollView.frame = CGRectMake(0, 0, 320, 460);
    
#ifdef DEBUG
    txtEmail.text = @"thoadaika@gmail.com";
    txtPassword.text = @"xxxxxx";
#endif
    
    //-- hide tab bar and navigation bar
    [self.tabBarController setTabBarHidden:YES animated:NO];
    self.navigationController.navigationBarHidden = YES;
    
    //-- hide view pass
    viewForgot.alpha = 0;
}


- (void)viewWillAppear:(BOOL)animated
{
    //
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//-- go to Home Screen
- (void) hiddenSplashScreenAndGotoHomeViewController
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.isLogOut || [[NSUserDefaults standardUserDefaults] boolForKey:@"LOG_OUT"]) {
        
        appDelegate.isLogOut = NO;
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LOG_OUT"];
    }
    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.window setRootViewController:appDelegate.tabBarController];
    
    NSLog(@"vo lon 0: %d", self.tabBarController.selectedIndex);
    
    //get twitter account
    [AppDelegate checkACC];

    /* --Dung comment refact tabbar. 
    NSMutableArray *tbViewControllers = [NSMutableArray arrayWithArray:[self.tabBarController viewControllers]];
    [tbViewControllers removeObjectAtIndex:0];
    [self.tabBarController setViewControllers:tbViewControllers];
    
    [self.tabBarController setSelectedIndex:0]; 
    [self.tabBarController setTabBarHidden:NO animated:NO];
     */    
    
}



#pragma mark - ACTION

- (IBAction)clickToBtnSignin:(id)sender
{
    //--validate textfields
    if([self validateInfo])
    {
        //--hide button Done & dislay button Logout
        [self showProgressInView:self.view target:self title:@"Logging in..." selector:@selector(loginAPI:)];
    }
}


- (IBAction)clickToBtnSignUp:(id)sender
{
    RegisterViewController *rgVC = nil;
    if(isIphone5) {
        rgVC = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController-568h" bundle:nil];
    }else{
        rgVC = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    }
    
    NSLog(@"clickToBtnSignUp");
    
    [self.navigationController pushViewController:rgVC animated:YES];
}


- (IBAction)clickToBtnForgotPass:(id)sender
{
    viewForgot.alpha = 0.8;
}


- (IBAction)clickToBtnHelp:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    TermsPolicyViewController *tpVC = [[TermsPolicyViewController alloc] initWithNibName:@"TermsPolicyViewController" bundle:nil];
    
    appDelegate.isShowLink = YES;
    
    [self.navigationController pushViewController:tpVC animated:YES];
}


- (IBAction)clickToBtnHideView:(id)sender
{
    viewForgot.alpha = 0;
}


- (IBAction) clickToBtnOK:(id)sender
{
    if ([textPass.text length] > 0) {
        //-- hide view
        viewForgot.alpha = 0;
        [self showProgressInView:self.view target:self title:@"Sending..." selector:@selector(forgotPasswordAPI:)];
        //-- hide keyboard        
        [textPass resignFirstResponder];
    }
}

- (IBAction)clickPatentPending:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    TermsPolicyViewController *tpVC = [[TermsPolicyViewController alloc] initWithNibName:@"TermsPolicyViewController" bundle:nil];
    
    appDelegate.isShowLink = YES;
    
    [self.navigationController pushViewController:tpVC animated:YES];
}



#pragma mark - using API

- (void)loginAPI:(MBProgressHUD*)progress
{
    NSError *error = nil;
    NSDictionary *userInfo = nil;
    
    NSString *email_Str     = txtEmail.text;
    NSString *pass_Str      = txtPassword.text;
    
    userInfo = [BesafeAPI signinAPI:email_Str password:pass_Str error:&error];
    
    if (error) {
        [progress hide:YES];
        [self showAlert:[error localizedDescription]];        
        return;
    }else {
        NSLog(@"update success %@",[userInfo objectForKey:@"data"]);
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.isLoginSuccessfully = YES;
        
        [[NSUserDefaults standardUserDefaults] setObject:@"isLoginSuccessfully" forKey:@"QUIT_APP"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSDictionary *data = [userInfo objectForKey:@"data"];
        [progress hide:YES];
        
        VMUsers *user       = [[VMUsers alloc] init];
        user.email          = [data objectForKey:@"email"];
        user.birthday       = [data objectForKey:@"birthday"];
        user.zipcode        = [data objectForKey:@"zip_code"];
        user.gender         = [data objectForKey:@"sex"];
        user.userId_login   = [data objectForKey:@"id"];
        user.arrbuddys      = [data objectForKey:@"buddys"];

        [VMUsers saveUser:user];
        NSLog(@"data user: %@",user);
        [[NSUserDefaults standardUserDefaults] setObject:user.userId_login forKey:BESAFE_USER_ID_LOGIN];
        [self performSelector:@selector(hiddenSplashScreenAndGotoHomeViewController) withObject:self afterDelay:1];
        
        return;
    }
}

- (void) forgotPasswordAPI:(MBProgressHUD*)progress
{
    NSError *error = nil;
    NSDictionary *dict = nil;
    
    NSString *emailStr = textPass.text;
    
    dict = [BesafeAPI forgotPassAPI:emailStr error:&error];
    textPass.text = @"";
    if (error) {
        [progress hide:YES];
        [self showAlert:[error localizedDescription]];
        return;
    }
    else
    {        
        [progress hide:YES];
        [self showAlert:[dict objectForKey:@"msg"]];
    }    
    
}

#pragma mark - Private methods

- (BOOL) validateInfo
{
    int check = 0;
    if ([txtEmail.text length] == 0) {
        [txtEmail setText:@""];
        [btnEmail setImage:[UIImage imageNamed:@"btn_warning.png"] forState:UIControlStateNormal];
        check = 1;
    }else{
        btnEmail.imageView.image = nil;
    }
    
    if ([txtPassword.text length] == 0) {
        [txtPassword setText:@""];
        [btnPassword setImage:[UIImage imageNamed:@"btn_warning.png"] forState:UIControlStateNormal];
        check = 1;
    }else{
        btnPassword.imageView.image = nil;
    }
    
    if (check == 0) {
        return YES;
    }
    else {
        return NO;
    }
}


- (UIView*)frontFromView:(UIView*)aView
{
    UIWindow* tempWindow = nil;
    UIView *keyboard = nil;
    
    if ([[[UIApplication sharedApplication] windows] count] > 1) {
        tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    }
    
    if ([tempWindow.subviews count])
        keyboard = [tempWindow.subviews objectAtIndex:0];
    
    return (keyboard != nil ? tempWindow : aView);
}


- (MBProgressHUD*)showProgressInView:(UIView*)view target:(id)aTarget
                               title:(NSString*)titleText selector:(SEL)aSelector
{
    UIView *frontView = [self frontFromView:view];
    
    MBProgressHUD* mbhub = [[MBProgressHUD alloc] initWithView:frontView];
	mbhub.labelText = titleText;
    mbhub.removeFromSuperViewOnHide = YES;
    [mbhub setDelegate:(id)aTarget];
	[mbhub show:YES];
	[frontView addSubview:mbhub];
    [aTarget performSelector:aSelector withObject:mbhub afterDelay:0.3];
    return mbhub;
}


- (void) showAlert: (NSString*) msg
{
	[[[UIAlertView alloc] initWithTitle: nil
                                message: msg
                               delegate: nil
                      cancelButtonTitle: @"OK"
                      otherButtonTitles: nil] show];
}


- (void)resetTextFields
{
    self.txtEmail.text      = @"";
    self.txtPassword.text   = @"";
}


-(void)dismissKeyboard
{
    [txtEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
}



#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtEmail) {
        [textField resignFirstResponder];
        [self.txtPassword becomeFirstResponder];
        
        return NO;
    }else if (textField == self.txtPassword) {
        [textField resignFirstResponder];
        
        return NO;
    }else if (textField == textPass){
        [textField resignFirstResponder];
        
        return NO;
    }
    
    return YES;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (isIphone5)
        scrollView.frame = CGRectMake(0, -100, 320, 668);
    else
        scrollView.frame = CGRectMake(0, -90, 320, 550);
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[scrollView scrollRectToVisible:textField.frame animated:YES];
	[UIView commitAnimations];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //[self processTimePrefEntry:textField.text];
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationBeginsFromCurrentState:YES];
    if (isIphone5) {
        scrollView.frame = CGRectMake(0, 0, 320, 548);
    }
    else{
        scrollView.frame = CGRectMake(0, 0, 320, 460);
    }
	[UIView commitAnimations];
}



@end
