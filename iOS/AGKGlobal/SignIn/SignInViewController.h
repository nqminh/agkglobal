//
//  SignInViewController.h
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 10/10/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "BesafeAPI.h"
#import "AppDelegate.h"
#import "VMUsers.h"
#import "UIView+HidingView.h"

@interface SignInViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UIButton         *btnEmail;
    IBOutlet UIButton         *btnPassword;
    IBOutlet UIButton         *btnSignIn;
    IBOutlet UIButton         *btnSignUp;
    IBOutlet UIButton         *btnForgotPass;
    IBOutlet UIButton         *btnHelp;
    IBOutlet UIButton         *btnPatentPending;
    
    //-- view forgot pass
    IBOutlet UIView           *viewForgot;
    IBOutlet UIButton         *btnHideViewForgot;
    IBOutlet UITextField      *textPass;
    IBOutlet UIButton         *btnOK;
}

@property (nonatomic, retain) IBOutlet UIScrollView     *scrollView;
@property (nonatomic, retain) IBOutlet UITextField      *txtEmail;
@property (nonatomic, retain) IBOutlet UITextField      *txtPassword;

- (IBAction)clickToBtnSignin:(id)sender;
- (IBAction)clickToBtnSignUp:(id)sender;
- (IBAction)clickToBtnForgotPass:(id)sender;
- (IBAction)clickToBtnHelp:(id)sender;
- (IBAction)clickToBtnHideView:(id)sender;
- (IBAction)clickToBtnOK:(id)sender;
- (IBAction)clickPatentPending:(id)sender;

@end
