//
//  Constants.h
//  BeSafe
//
//  Created by Jeff Jolley on 11/7/12.
//
//

#define APP_DEBUG 1

#define TWITTER_TYPE                21
#define SMS_TYPE                    2
#define RETWITTER_TYPE              3   // JLJ //
#define FACEBOOK_POSTING_TYPE 		4
#define FACEBOOK_IMAGE_TYPE 		5
#define FACEBOOK_COMMENT_TYPE 		26
#define REPLY_TWEET_TYPE            7  
#define RETWEET_TYPE                8
#define NEWTWEET_TYPE               19
#define FAVORITE_TYPE               10
#define FBLike                      11
#define FBUnlike                    12
#define FBcomment                   13
#define FBshare                     14
#define FBPost                      15

#define FBPostBuddy                 16

#define BANNED_MODE_WORDS           1
#define BANNED_MODE_ADDRESS         2

#define DEFAULT_TIMER_SECS          10
#define COUNTDOWN_TRANSITION        0.5



/*
 *  System Versioning Preprocessor Macros
 
 http://stackoverflow.com/questions/3339722/check-iphone-ios-version
 
 */

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

/*                                  Twitter keys                                  */
#define     kOAuthConsumerKey       @"UxsB7aprBkJouFmLoCBeAg"
#define     kOAuthConsumerSecret    @"z0N8WptzIL1Ii6PGHfSuP9F1c4Q3ykYL9TLJh6Y4E"

// Dictionary key
#define USER_AVATAR_DICT_KEY            @"avatar"
#define USER_ID_DICT_KEY                @"userId"
#define FULL_NAME_DICT_KEY              @"name"
#define USER_NAME_DICT_KEY              @"username"
#define PHONE_DICT_KEY                  @"phone"
#define AUTH_DATA_DICT_KEY              @"authData"
#define CREATED_AT_DICT_KEY             @"createdAt"
#define UPDATED_AT_DICT_KEY             @"updatedAt"
#define SESSION_TOKEN_DICT_KEY          @"sessionToken"
#define DEVICE_TOKEN_DICT_KEY           @"deviceToken"
#define OS_NAME_DICT_KEY                @"os_name"
#define CODE_DICT_KEY                   @"code"
#define DETAIL_DICT_KEY                 @"detail"

#define EMAIL_DICT_KEY                  @"email"
#define PASSWORD_DICT_KEY               @"password"
#define ZIPCODE_DICT_KEY                @"zipcode"
#define GENDER_DICT_KEY                 @"gender"
#define BIRTHDAY_DICT_KEY               @"birthday" 
#define ARR_BUDDYS_ID                   @"buddys"
#define USER_ID_LOGIN_DICT_KEY          @"userId_login" 
#define UPDATE_BUDDY_ID_KEY             @"buddy_id" 
#define FACEBOOK_ID_LOGIN               @"facebook_id" 
#define SEX_LOGIN_KEY                   @"sex"
#define AGE_USER_KEY                    @"age"

///////////////////////////////////////////////////////////
/************************** CID keys **************************/
#define     CID_ServiceName             @"com.vmodev.besafe.CID_ServiceName"


