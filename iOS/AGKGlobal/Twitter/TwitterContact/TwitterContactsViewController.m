//
//  TwitterContactsViewController.m
//  BeSafe
//
//  Created by Jeff Jolley on 4/19/13.
//
//

#import "TwitterContactsViewController.h"
#import "AppDelegate.h"
#import "UIImageView+Cached.h"

@interface TwitterContactsViewController ()

@end

@implementation TwitterContactsViewController

@synthesize _names,nameDict,userDict;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear");
    
    //-- change title color
    CGRect frame = CGRectMake(0, 150, 100, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;

}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"viewDidAppear");
    NSInteger twitter_modal = [[NSUserDefaults standardUserDefaults] integerForKey:@"TWITTER_MODAL"];
    NSLog(@"twitter_modal:%d",twitter_modal);
    if (2 == twitter_modal) {
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TWITTER_MODAL"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Followed you";
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.nameDict allKeys].count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.imageView.image = [UIImage imageNamed:@"public.png"];
    }
    
    cell.textLabel.text = [[self names] objectAtIndex:indexPath.row];
    [cell.imageView loadFromURL:[NSURL URLWithString:[[self getDictForRow:indexPath.row] objectForKey:@"profile_image_url"]]];
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //OPEN TWITTER COMPOSER IF CONNECTED
    NSString *screen_name = [[self getDictForRow:indexPath.row] objectForKey:@"screen_name"];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"sendDirectMsg"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate displayTwitterComposerSheet:screen_name
                                        body:@""
                                        abId:@-1
                                   imagePath:[self getOriginalTwitterImageURL:[[self getDictForRow:indexPath.row] objectForKey:@"profile_image_url"]]
                                        view:self
                                       title:@"New Message"];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"sendDirectMsg"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [appDelegate setIsNewTweet:NO];
}


- (NSArray*)names {
    if (self._names == nil && self.nameDict != nil)
    {
        self._names = [[self.nameDict allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    }
    return self._names;
}


- (NSString*)getBiggerTwitterImageURL:(NSString*)url
{
    NSString *result = [url stringByReplacingOccurrencesOfString:@"_normal." withString:@"."];

    return result;
}


- (NSString*)getOriginalTwitterImageURL:(NSString*)url
{
    NSString *result = [url stringByReplacingOccurrencesOfString:@"_normal." withString:@"."];

    return result;
}


- (NSDictionary*)getDictForRow:(NSInteger)row
{
    NSString *a_id = [self.nameDict objectForKey:[self.names objectAtIndex:row]];
    return [self.userDict objectForKey:a_id];
}


@end
