//
//  TwitterContactsViewController.h
//  BeSafe
//
//  Created by Jeff Jolley on 4/19/13.
//
//

#import <UIKit/UIKit.h>

@interface TwitterContactsViewController : UITableViewController {
    
}

@property (nonatomic,strong) NSArray *_names;
@property (nonatomic,strong) NSMutableDictionary *nameDict;
@property (nonatomic,strong) NSMutableDictionary *userDict;

- (NSArray*)names;

@end
