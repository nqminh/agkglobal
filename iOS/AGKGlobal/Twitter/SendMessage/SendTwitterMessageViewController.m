//
//  SendTwitterMessageViewController.m
//  BeSafe
//
//  Created by The Rand's on 8/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SendTwitterMessageViewController.h"
#import "CountDownViewController.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "JSON.h"

#define address_not_found @"Address not found"

@implementation SendTwitterMessageViewController

@synthesize recipient, body, abId, recipientLabel, bodyTextView, charCountLabel, imagePath, navBar, titleText, attachButton, attachImage, attachImageView, attachPaperClip, attachToolbar, lat, lon, address, scrollView, tableViewFilter, _names, _namesFilter, nameDict, userDict, valueStr, isFilter, trendsAvailableDict, _trends, _trendsFilter, isTrendsAvail, profileUrl, twitterDelegate;
@synthesize btnCancel, btnTweet;
@synthesize lblTitle;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        closeOnAppear = NO;
    }
    
    [self registerForKeyboardNotifications];
    return self;
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
}


- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    UIWindow* tempWindow;
	UIView* keyboard;
	
	//Check each window in our application
	for(int c = 0; c < [[[UIApplication sharedApplication] windows] count]; c ++)
	{
		//Get a reference of the current window
		tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:c];
		
		//Get a reference of the current view 
		for(int i = 0; i < [tempWindow.subviews count]; i++)
		{
			keyboard = [tempWindow.subviews objectAtIndex:i];
			
			if([[keyboard description] hasPrefix:@"<UIKeyboard"] == YES)
			{
                
            }
		}
	}
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    bodyTextView.contentInset = contentInsets;
    bodyTextView.scrollIndicatorInsets = contentInsets;
    
    
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - private method

- (NSString*)getAddressFromLat:(NSString*)latString lon:(NSString*)lonString
{
#if notok
    NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps/geo?q=%@,%@&output=json", latString, lonString];
    NSString *needAddress = nil;
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *locationString = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSDictionary *dic = [locationString JSONValue];
    
    NSArray *placemarkArr = [dic objectForKey:@"Placemark"];
    
    if ([placemarkArr count]) {
        NSDictionary *addressDic = [placemarkArr objectAtIndex:0];
        needAddress = [addressDic objectForKey:@"address"];
    }
    return needAddress;
#else
    //http://maps.googleapis.com/maps/api/geocode/json?latlng=21.035059,105.791510&sensor=true
    //@"http://maps.google.com/maps/geo?q=%@,%@&output=json
    NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%@,%@&sensor=true", latString, lonString];
    NSLog(@"urlStrign: %@",urlString);
    if ([self.lat floatValue] == 0.0 && [self.lon floatValue] == 0.0) {
        return address_not_found;
    }
    else {
        NSString *needAddress = nil;
        NSURL *url = [NSURL URLWithString:urlString];
        
        NSString *locationString = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
        NSDictionary *dic = [locationString JSONValue];
        // NSLog(@"dic:%@",dic);
        NSArray *arr = [dic objectForKey:@"results"];
        NSDictionary *dic1 = [arr objectAtIndex:0];
        // NSLog(@"%@",[dic1 objectForKey:@"formatted_address"]);
        needAddress = [dic1 objectForKey:@"formatted_address"];
        //NSLog(@"address: %@",needAddress);
        if (!needAddress) {
            needAddress = address_not_found;
        }
        return needAddress;
    }
#endif
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.recipientLabel setText:[NSString stringWithFormat:@"@%@", self.recipient]];
    [self.bodyTextView setText:self.body];
    [self.bodyTextView becomeFirstResponder];
    [self.bodyTextView setDelegate:self];
    [self textViewDidChange:self.bodyTextView];
    [self.navBar setTitle:self.titleText];
    [self.bodyTextView setKeyboardType:UIKeyboardTypeTwitter];
    [self.attachPaperClip setHidden:true];
    
    BOOL wasDirect = [[NSUserDefaults standardUserDefaults] boolForKey:@"sendDirectMsg"];
    if (wasDirect)
    {
        self.navigationItem.title = @"Direct Message";
    }
    else
    {
        self.navigationItem.title = @"New Message";
    }    
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480)
        scrollView.frame = CGRectMake(0, 44, 320, 500);
    else
        scrollView.frame = CGRectMake(0, 44, 320, 600);

    self.tableViewFilter.hidden = YES;
    
    [self setIsFilter:NO];
    [self setIsTrendsAvail:NO];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    userDict = [defaults valueForKey:@"userDict"];
    nameDict = [defaults valueForKey:@"nameDict"];
    
    trendsAvailableDict = [[NSArray alloc] init];
    trendsAvailableDict = [[defaults valueForKey:@"trendsDict"] valueForKey:@"TWITTER_TRENDS_AVAILABLE_DICT"];
    [self trends];
    NSLog(@"userDict : %@",userDict);
    NSLog(@"nameDict : %@",nameDict);
    NSLog(@"trendsDict: %@",trendsAvailableDict);
}


- (void)viewWillAppear:(BOOL)animated
{
    //-- change title color
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    //self.navigationItem.titleView = label;
    self.navBar.titleView = label;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if(closeOnAppear){
        [self dismissViewControllerAnimated:NO completion:nil];
    }

    NSInteger twitter_modal = [[NSUserDefaults standardUserDefaults] integerForKey:@"TWITTER_MODAL"];
    NSLog(@"twitter_modal:%d",twitter_modal);
    if (2 == twitter_modal) {
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TWITTER_MODAL"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
      
        CLLocation *location1 = [locationManager location];
        float longitude = location1.coordinate.longitude;
        float latitude = location1.coordinate.latitude;
        NSLog(@"dLongitude : %f", longitude);
        NSLog(@"dLatitude : %f", latitude);
        self.lat = [NSString stringWithFormat:@"%f",latitude];
        self.lon = [NSString stringWithFormat:@"%f",longitude];
        
        self.address = [self getAddressFromLat:lat lon:lon];
        NSString *atLocation = self.address;
        
        bodyTextView.text = [NSString stringWithFormat:@"%@ - at %@",bodyTextView.text, atLocation];
    }
}



#pragma mark - update current location

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    NSLog(@"currentLocation : %@",currentLocation);
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
    [locationManager stopUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"update current location failse");
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        switch (buttonIndex) {
            case 0:[self openCamera];break;
            case 1: [self openGallery];break;
            default: break;
        };
    }else {
        switch (buttonIndex) {
            case 0: [self openGallery];break;
            default: break;
        };
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    self.attachImage = image;
    self.attachImageView.image = image;
    self.attachPaperClip.hidden = false;
    [self dismissModalViewControllerAnimated:YES];
}


- (IBAction)sendClicked:(id)sender
{
    // get data from banner words , banned addresses.
    // check banned word, banned address. 
    textSrc_setwitter = self.bodyTextView.text;
    bannedWordArr_setwitter = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_WORDS"] mutableCopy];
    bannedAddressArr_setwitter = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_ADDRESSES"] mutableCopy];
    
    showBannedWordStr_setwitter = [Utility showBanned:textSrc_setwitter withArr:bannedWordArr_setwitter];
    showBannedAddressStr_setwitter = [Utility showBanned:textSrc_setwitter withArr:bannedAddressArr_setwitter];
    
    if(showBannedWordStr_setwitter.length > 0 )
    {
        if (showBannedAddressStr_setwitter.length > 0)
        {
            NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_setwitter);
            alv_setwitter = [[UIAlertView alloc]initWithTitle:@"Banned Word and Address!" message:[NSString stringWithFormat:@"You have typed Banned Word and Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_setwitter show];
        }
        else{
            NSLog(@"\n\n banned word %@\n\n",showBannedWordStr_setwitter);
            alv_setwitter = [[UIAlertView alloc]initWithTitle:@"Banned Word!" message:[NSString stringWithFormat:@"You have typed Banned Word \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_setwitter show];
        }
        
    }
    else if (showBannedAddressStr_setwitter.length > 0)
    {
        NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_setwitter);
        alv_setwitter = [[UIAlertView alloc]initWithTitle:@"Banned Address!" message:[NSString stringWithFormat:@"You have typed Banned Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alv_setwitter show];
    }
    else
    {
        // send quote tweet .
        profileUrl = [[NSString alloc] initWithString:imagePath];
        profileUrl = [profileUrl stringByReplacingOccurrencesOfString:@"_normal." withString:@"."];
        
        CountDownViewController *controller = nil;
        if (isIphone5)
            controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController-568h" bundle:nil];
        else
            controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController" bundle:nil];
        
        controller.messageText = self.bodyTextView.text;    
        controller.recipientText = [NSString stringWithFormat:@"@%@", self.recipient];
        controller.twitterDelegate = self;
        controller.abId = self.abId;
        controller.imagePath = self.profileUrl;
        controller.sendType = TWITTER_TYPE;
        NSLog(@"???imagePath:%@",profileUrl);
        
        UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromRight;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
        [UIView setAnimationDuration:1];
        [UIView setAnimationDelegate:controller];
        [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
        [self presentViewController:controller animated:YES completion:nil];
        [UIView commitAnimations];
    }
}


- (void)sendDirectMessage
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSDictionary *paramDict = [NSDictionary dictionaryWithObjectsAndKeys: 
                                 self.recipient, @"screen_name",
                               self.bodyTextView.text, @"text", nil];
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/direct_messages/new.json"];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:myUrl parameters:paramDict];
    request.account = [appDelegate selectedTwitterAccount];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
    {
        NSError *jsonError = nil;
        id timeLineData = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonError];
             
        dispatch_async(dispatch_get_main_queue(), ^{
            if (APP_DEBUG) NSLog(@"%@",timeLineData);
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
    }];
}


- (void)sendPublicMessage
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *paramDict = [NSDictionary dictionaryWithObjectsAndKeys: 
                               self.bodyTextView.text, @"status", nil];
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:myUrl parameters:paramDict];
    request.account = [appDelegate selectedTwitterAccount];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         NSError *jsonError = nil;
         id timeLineData = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonError];
         
         dispatch_async(dispatch_get_main_queue(), ^{
             NSLog(@"%@",timeLineData);
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             appDelegate.isSuccess = YES;
             
             [self.twitterDelegate newTweetComplete];
         });
     }];
}


- (void)sendPublicMessageWithImage:(UIImage*)twitterImage
{
    #define kTWMultiPartFormData @"multipart/form-data"
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update_with_media.json"];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:url parameters:nil];
    request.account = [appDelegate selectedTwitterAccount];
    
    // image is just an UIImage, we need to convert to NSData
    NSData *imageData = UIImagePNGRepresentation(twitterImage);
    [request addMultipartData:imageData withName:@"media[]" type:kTWMultiPartFormData filename:nil];
    
    NSString *status = self.bodyTextView.text;    
    [request addMultipartData:[status dataUsingEncoding:NSUTF8StringEncoding] withName:@"status" type:kTWMultiPartFormData filename:nil];
    
    // Perform the request
    // TODO this should include error handling
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        NSDictionary *dict = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
        
        // Log the result
        NSLog(@"%@", dict);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"Done.");
            [twitterDelegate newTweetComplete];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            appDelegate.isSuccess = YES;
        });
    }];
}


- (void)countDownComplete
{
    if ([self.recipient isEqualToString:@"Public Tweet"]) {
        
        if (self.attachImage)
            [self sendPublicMessageWithImage:self.attachImage];
        else 
            [self sendPublicMessage];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.isSuccess = YES;
        
    }else {
        
        [self sendDirectMessage];
        
    }
    
    //CLOSE WINDOW
    closeOnAppear = YES;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



#pragma mark - ACTION

- (IBAction)cancelClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)attachImageButtonClicked:(id)sender
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [self openCamera];
    }
}


- (IBAction)clickToBtnChooseImage:(id)sender
{
    [self openGallery];
}


//-- Get my current location
- (IBAction)getMyLocation:(id)sender
{
    currentLocation = [[CLLocation alloc] init];
    
    if (!locationManager) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = (id)self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
    }
    [locationManager startUpdatingLocation];
    CLLocation *location1 = [locationManager location];
    float longitude = location1.coordinate.longitude;
    float latitude = location1.coordinate.latitude;
    NSLog(@"dLongitude : %f", longitude);
    NSLog(@"dLatitude : %f", latitude);
    self.lat = [NSString stringWithFormat:@"%f",latitude];
    self.lon = [NSString stringWithFormat:@"%f",longitude];
    
    self.address = [self getAddressFromLat:lat lon:lon];
    
    NSString *addressString = [NSString stringWithFormat:@"Enable to add a location to your Tweets."];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Tweet location" message:addressString delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Enable",nil];
    
    [alertView show];
}


- (void)openCamera
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
}


- (void)openGallery
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isFilter){
        if (self.isTrendsAvail) 
            return _trendsFilter.count;
        else
            return _namesFilter.count;
    }else{
        if (self.isTrendsAvail) 
            return self.trendsAvailableDict.count;
        else
            return [self.nameDict allKeys].count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableViewFilter dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.imageView.image = [UIImage imageNamed:@"public.png"];
    }
    
    
    if (self.isTrendsAvail) {
        if (self.isFilter) {
            cell.textLabel.text = [[self _trendsFilter] objectAtIndex:indexPath.row];
            NSLog(@"cell.textLabel.isFilter: %@",cell.textLabel.text);
        }
        else{
            cell.textLabel.text = [[self trends] objectAtIndex:indexPath.row];
            NSLog(@"cell.textLabel: %@",cell.textLabel.text);
        }
    }else{
        if (self.isFilter)
            cell.textLabel.text = [[self _namesFilter] objectAtIndex:indexPath.row];
        else
            cell.textLabel.text = [[self names] objectAtIndex:indexPath.row];
        
        [cell.imageView loadFromURL:[NSURL URLWithString:[[self getDictForRow:indexPath.row] objectForKey:@"profile_image_url"]]];
    }
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //OPEN TWITTER COMPOSER IF CONNECTED
    NSString *screen_name = [[self getDictForRow:indexPath.row] objectForKey:@"screen_name"];
    NSString *trend_name = [self.trendsAvailableDict objectAtIndex:indexPath.row];
    
    self.tableViewFilter.hidden = YES;
    recordingHashTag = NO;
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480)
        scrollView.frame = CGRectMake(0, 44, 320, 500);
    else
        scrollView.frame = CGRectMake(0, 44, 320, 600);
    
    NSLog(@"bodyTextView.text: %@",bodyTextView.text);
    
    valueStr = [AppDelegate getFileNameFromURLString:bodyTextView.text];
    
    NSLog(@"valueStr: %@",valueStr);
    
    if (self.isTrendsAvail) 
        bodyTextView.text = trend_name;
    else
        bodyTextView.text = [bodyTextView.text stringByReplacingOccurrencesOfString:valueStr withString:screen_name];
}


- (NSArray*)names {
    if (nil == self._names && nil != self.nameDict) {
        self._names = [[self.nameDict allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    }
    return self._names;
}


- (NSArray *)trends
{
    NSLog(@"trendsAvailableDict222 : %@",trendsAvailableDict);
    
    if (nil == self._trends && nil != self.trendsAvailableDict) {
        self._trends = [trendsAvailableDict sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    }
    
    NSLog(@"_trends222 : %@",_trends);
    return _trends;
}


- (NSString*)getBiggerTwitterImageURL:(NSString*)url {
    NSString *result = [url stringByReplacingOccurrencesOfString:@"_normal." withString:@"_bigger."];
    return result;
}


- (NSString*)getOriginalTwitterImageURL:(NSString*)url {
    NSString *result = [url stringByReplacingOccurrencesOfString:@"_normal." withString:@"."];
    return result;
}


- (NSDictionary*)getDictForRow:(NSInteger)row {
    NSString *a_id = nil;
    if (self.isFilter)
        a_id = [self.nameDict objectForKey:[self._namesFilter objectAtIndex:row]];        
    else
        a_id = [self.nameDict objectForKey:[self.names objectAtIndex:row]];
    
    return [self.userDict objectForKey:a_id];
}


#pragma mark - Util methods

-(void)filterHashTagTableWithHash:(NSString *)hash
{
    [filterHashTagArray removeAllObjects];
    
    for (NSString *hashTag in hashTagArray ) {
        
        NSRange result = [hashTag rangeOfString:hash options:NSCaseInsensitiveSearch];
        if (result.location != NSNotFound) {
            [filterHashTagArray addObject:hashTag];
        }
    }
    
    if (filterHashTagArray.count) {
        self.tableViewFilter.hidden = NO;
    }else{
        self.tableViewFilter.hidden = YES;
    }
    
    [self.tableViewFilter reloadData];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    int maxChars = 140;
    int charCount = maxChars - [textView.text length];
    
    self.charCountLabel.text = [NSString stringWithFormat:@"%i", charCount];
    [self.charCountLabel setTextColor:[UIColor darkGrayColor]];
    
    if (charCount == maxChars) {
        [self.btnTweet setEnabled:NO];
    } else if (charCount < 0) {
        [self.charCountLabel setTextColor:[UIColor redColor]];
        [self.btnTweet setEnabled:NO];
    }else {
        [self.btnTweet setEnabled:YES];
    }
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isNewTweet) {
        if ([text isEqualToString:@"@"]) {
            recordingHashTag = YES;
            startParse = range.location;
//            if ([text isEqualToString:@"#"]) [self setIsTrendsAvail:YES];
//            if ([text isEqualToString:@"@"]) [self setIsTrendsAvail:NO];
        }
        else if ([text isEqualToString:@" "] || [text isEqualToString:@""]) {
            currentHashTag = nil;
            recordingHashTag = NO;
            self.tableViewFilter.hidden = YES;
            self.valueStr = @"";
            
            //--set scrollview frame
            CGSize result = [[UIScreen mainScreen] bounds].size;
            if (result.height == 480)
                scrollView.frame = CGRectMake(0, 44, 320, 500);
            else
                scrollView.frame = CGRectMake(0, 44, 320, 600);
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [scrollView scrollRectToVisible:textView.frame animated:YES];
            [UIView commitAnimations];
        }
        
        if (recordingHashTag == YES) {
            NSInteger offset = [textView.text length] - startParse;
            
            if (offset > 0) {
                //--set scrollview frame
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if (result.height == 480)
                    scrollView.frame = CGRectMake(0, 0, 320, 500);
                else
                    scrollView.frame = CGRectMake(0, 0, 320, 600);
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDelegate:self];
                [UIView setAnimationDuration:0.5];
                [UIView setAnimationBeginsFromCurrentState:YES];
                [scrollView scrollRectToVisible:textView.frame animated:YES];
                [UIView commitAnimations];
                
                //--set Filter
                [self setIsFilter:YES];
                
                //--set data for valueStr
                valueStr = [textView.text substringWithRange:NSMakeRange(startParse, offset)];
                
                NSLog(@"valueStr: %@",valueStr);
                
                //--receive notification when
                [self searchContact];
                
                self.tableViewFilter.hidden = NO;
            }
            else {
                self.tableViewFilter.hidden = YES;
            }
        }
    }
    
    return YES;
}


#pragma mark - Method searchContact

-(void) searchContact
{
    if (self.isFilter) {
        NSString *string = valueStr;
        if([string isEqualToString:@" "])
        {
            self.isFilter = NO;
        }
        else
        {
            self.isFilter = YES;
            if (self.isTrendsAvail) {
                if (!_trendsFilter) {
                    _trendsFilter = [[NSMutableArray alloc] init];
                }
                else {
                    [_trendsFilter removeAllObjects];
                }

                //----//
                //[self trends];
                for (int i=0; i<_trends.count ; i++)
                {
                    NSString *userName = [_trends objectAtIndex:i];
                    NSLog(@"userName; %@",userName);
                    
                    if ([userName rangeOfString:valueStr options:NSCaseInsensitiveSearch].location != NSNotFound){
                        [_trendsFilter addObject:userName];
                        break;
                    }
                }
                
                NSLog(@"_trendsFilter count; %d",_trendsFilter.count);
                
                if (_trendsFilter.count>0)
                    [self.tableViewFilter reloadData];
            }else{
                if (!_namesFilter) {
                    _namesFilter = [[NSMutableArray alloc] init];
                }
                else {
                    [_namesFilter removeAllObjects];
                }
                
                //----//
                for (int i=0; i<_names.count ; i++)
                {
                    NSString *userName = [[self names] objectAtIndex:i];
                    if ([userName rangeOfString:valueStr options:NSCaseInsensitiveSearch].location != NSNotFound)
                        [_namesFilter addObject:userName];
                }
            }
            if (_namesFilter.count>0)
                [self.tableViewFilter reloadData];
        }
    }
}


@end
