//
//  SendTwitterMessageViewController.h
//  BeSafe
//
//  Created by The Rand's on 8/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>
#import <Twitter/TWTweetComposeViewController.h>
#import <CoreLocation/CoreLocation.h>
#import "TwitterContactsViewController.h"
#import "UIImageView+Cached.h"
#import "AppDelegate.h"
#import "TwitterViewController.h"

@interface SendTwitterMessageViewController : UIViewController <UITextViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate, UIAlertViewDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSString                    *recipient;
    NSString                    *body;
    NSNumber                    *abId;
    IBOutlet UIBarButtonItem    *sendButton;
    IBOutlet UILabel            *recipientLabel;
    IBOutlet UITextView         *bodyTextView;
    IBOutlet UILabel            *charCountLabel;
    NSString                    *imagePath;
    NSString                    *profileUrl;
    IBOutlet UINavigationItem   *navBar;
    NSString                    *titleText;
    IBOutlet UIBarButtonItem    *attachButton;
    IBOutlet UIImageView        *attachImageView;
    UIImage                     *attachImage;
    IBOutlet UIImageView        *attachPaperClip;
    IBOutlet UIToolbar          *attachToolbar;
    
    //-- boolean
    BOOL                        closeOnAppear;
    BOOL                        isFilter;
    BOOL                        isTrendsAvail;
    
    //--location
    CLLocationManager           *locationManager;    
    CLLocation                  *currentLocation;
    
    //-- auto complete
    BOOL                        recordingHashTag;
    NSUInteger                  startParse;
    NSTextCheckingResult        *currentHashTag;
    
    NSMutableArray              *filterHashTagArray;
    NSMutableArray              *hashTagArray;
    
    // check banned word , address
    NSString *textSrc_setwitter;
    NSString *showBannedWordStr_setwitter;
    NSString *showBannedAddressStr_setwitter;
    
    NSArray *bannedWordArr_setwitter;
    NSArray *bannedAddressArr_setwitter;
    UIAlertView *alv_setwitter;
}

@property (nonatomic, retain) IBOutlet UIScrollView         *scrollView;
@property (nonatomic, retain) IBOutlet UITableView          *tableViewFilter;
@property (nonatomic, retain) IBOutlet UILabel              *lblTitle;
@property (nonatomic, retain) NSString                      *recipient;
@property (nonatomic, retain) NSString                      *body;
@property (nonatomic, retain) NSNumber                      *abId;
@property (nonatomic, retain) IBOutlet UILabel              *recipientLabel;
@property (nonatomic, retain) IBOutlet UITextView           *bodyTextView;
@property (nonatomic, retain) IBOutlet UILabel              *charCountLabel;
@property (nonatomic, retain) NSString                      *imagePath;
@property (nonatomic, retain) NSString                      *profileUrl;
@property (nonatomic, retain) IBOutlet UINavigationItem     *navBar;
@property (nonatomic, retain) NSString                      *titleText;
@property (nonatomic, retain) IBOutlet UIBarButtonItem      *attachButton;
@property (nonatomic, retain) IBOutlet UIBarButtonItem      *buttonLocation;
@property (nonatomic, retain) IBOutlet UIBarButtonItem      *buttonChooseImage;
@property (nonatomic, retain) IBOutlet UIImageView          *attachImageView;
@property (nonatomic, retain) UIImage                       *attachImage;
@property (nonatomic, retain) IBOutlet UIImageView          *attachPaperClip;
@property (nonatomic, retain) IBOutlet UIToolbar            *attachToolbar;
@property (assign)            BOOL                          isFilter;
@property (assign)            BOOL                          isTrendsAvail;
@property (nonatomic, strong) NSString                      *lat;
@property (nonatomic, strong) NSString                      *lon;
@property (nonatomic, assign) NSString                      *address;
@property (nonatomic, retain) NSString                      *valueStr;
@property (nonatomic, retain) IBOutlet UIButton             *btnCancel;
@property (nonatomic, retain) IBOutlet UIButton             *btnTweet;
@property (nonatomic,retain) TwitterViewController          *twitterDelegate;

//--Contact list
@property (nonatomic, strong) NSArray                       *_names;
@property (nonatomic, strong) NSArray                       *_trends;
@property (nonatomic, strong) NSMutableArray                *_namesFilter;
@property (nonatomic, strong) NSMutableArray                *_trendsFilter;
@property (nonatomic, strong) NSMutableDictionary           *nameDict;
@property (nonatomic, strong) NSMutableDictionary           *userDict;
@property (nonatomic, strong) NSArray                       *trendsAvailableDict;

- (NSArray*)names;
- (NSArray*)trends;

- (void)registerForKeyboardNotifications;
- (IBAction)sendClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;
- (void)sendDirectMessage;
- (void)sendPublicMessage;
- (void)countDownComplete;
- (IBAction)attachImageButtonClicked:(id)sender;
- (IBAction)getMyLocation:(id)sender;
- (IBAction)clickToBtnChooseImage:(id)sender;
- (void)openCamera;
- (void)openGallery;

@end
