//
//  DetailsTweetWebViewController.m
//  AGKGlobal
//
//  Created by HpProbook on 9/4/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "DetailsTweetWebViewController.h"
#import "UIImageView+Cached.h"
#import "CountDownViewController.h"

#define kStatusBarHeight 20
#define kDefaultToolbarHeight 40
#define kKeyboardHeightPortrait 216
#define kKeyboardHeightLandscape 140

@interface DetailsTweetWebViewController ()

@end

@implementation NSJSONSerialization (NSJSONSerialization_MutableBugFix)

+ (id)JSONObjectWithDataFixed:(NSData *)data options:(NSJSONReadingOptions)opt error:(NSError **)error
{
    id object = [NSJSONSerialization JSONObjectWithData:data options:opt error:error];
    
    if (opt & NSJSONReadingMutableContainers) {
        return [self JSONMutableFixObject:object];
    }
    
    return object;
}

+ (id)JSONMutableFixObject:(id)object
{
    if ([object isKindOfClass:[NSDictionary class]]) {
        // NSJSONSerialization creates an immutable container if it's empty (boo!!)
        if ([object count] == 0) {
            object = [object mutableCopy];
        }
        
        for (NSString *key in [object allKeys]) {
            [object setObject:[self JSONMutableFixObject:[object objectForKey:key]] forKey:key];
        }
    } else if ([object isKindOfClass:[NSArray class]]) {
        // NSJSONSerialization creates an immutable container if it's empty (boo!!)
        if (![object count] == 0) {
            object = [object mutableCopy];
        }
        
        for (NSUInteger i = 0; i < [object count]; ++i) {
            [object replaceObjectAtIndex:i withObject:[self JSONMutableFixObject:[object objectAtIndex:i]]];
        }
    }
    
    return object;
}

@end


@implementation DetailsTweetWebViewController

@synthesize _tempTweetHTMLBegin, _tempTweetHTMLEnd, _tempTweetBeginNormal, _tempTweetBeginRetweet, _tempTweetBodyPhoto, _tempTweetBodyStatus, _tempTweetBodyVideo, _tempTweetBodyStatusRetweet, _tempTweetBodyPhotoRetweet, _tempTweetBodyVideoRetweet, _tempTweetBodyStatusRetweetFavorite, _tempTweetBodyPhotoRetweetFavorite, _tempTweetBodyVideoRetweetFavorite, _tempSendMailTweet, _tempTweetEmptyBegin;
@synthesize nameTweet, screenNameTweet, screenNameDisPlay, dateTweet,createAtTweet,messageTweet,imageTweet,idTweet,mediaURLTweet,arrayData,_cssBaseString, inputToolbar, followNameDict, followUserDict, friendsNameDict, friendsUserDict, htmlString, idTweetStr;
@synthesize displayURLTweet, expandedURLTweet, retweetStatusDict,retweeter,retweetCount,profileUrlBig, favoriteCount,favorited,defaultProfile,messageConvert;
@synthesize viewShowImage, tweetWebView;
@synthesize nameLabel,dateLabel,wvContent,imgAvatar,imgTweet, protectedCount;
@synthesize urlDisplayMedia, urlDisplayUrls, urlUrls, urlMedia, retweetDict, urlExpandedMedia, urlExpandedUrls, iconReply, iconRetweet, iconRetweetBegin, iconFavorite, iconMore;
@synthesize dictionaryTweet;
@synthesize isMedia,isUrls;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Tweet";
    }
    return self;
}


#pragma mark - View cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [NetworkActivity show];
    
    keyboardIsVisible = NO;
    
    isLoadSuccess = YES;
    
    /* Calculate screen size */
    CGRect screenFrame = [[UIScreen mainScreen] applicationFrame];
    
    /* Create toolbar */
    self.inputToolbar = [[UIInputToolbar alloc] initWithFrame:CGRectMake(0, screenFrame.size.height-92-kDefaultToolbarHeight, screenFrame.size.width, kDefaultToolbarHeight)];
    [self.view addSubview:self.inputToolbar];
    inputToolbar.inputDelegate = self;
    inputToolbar.textView.placeholder = [NSString stringWithFormat:@"Reply to %@",nameTweet];
    
    profileUrlBig = [[NSString alloc] initWithString:imageTweet];

    objDict = [[NSMutableDictionary alloc] init];
    
    //--get list favorites    
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self getListReplies];
    });    
    
    //-- get all data for html    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self reloadData];
    });
    
    //--set image button favorite
    [self setButtonFavorite];
    
    //--get data for viewShowImage
    [self.viewShowImage removeFromSuperview];
    
    //--convert date tweet
    dateTweet = [Utility dateTimeStringForTwitterTimeToPush:createAtTweet];
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    //-- change title color
    CGRect frame = CGRectMake(0, 150, 100, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isOpenTweetLink = NO;
    
    isShowImage = NO;
    
     [NetworkActivity show];
    if (animated) {
        [NetworkActivity hide];
    }
    
	/* Listen for keyboard */
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 568) {
        [self.tweetWebView setFrame:CGRectMake(self.tweetWebView.frame.origin.x, self.tweetWebView.frame.origin.y, self.tweetWebView.frame.size.width, 417)];
    }
    else{
        [self.tweetWebView setFrame:CGRectMake(self.tweetWebView.frame.origin.x, self.tweetWebView.frame.origin.y, self.tweetWebView.frame.size.width, 329)];
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	/* No longer listen for keyboard */
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    // show tabbar    
    //[self showTabBar:self.tabBarController];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark Notifications

- (void)keyboardWillShow:(NSNotification *)notification
{
    /* Move the toolbar to above the keyboard */
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	CGRect frame = self.inputToolbar.frame;
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        //frame.origin.y = self.view.frame.size.height - frame.size.height - kKeyboardHeightPortrait + 50;
        if (isIphone5) 
            frame.origin.y = 249;
        else
            frame.origin.y = 161;
    }
    else {
        frame.origin.y = self.view.frame.size.width - frame.size.height - kKeyboardHeightLandscape - kStatusBarHeight;
    }
	self.inputToolbar.frame = frame;
	[UIView commitAnimations];
    keyboardIsVisible = YES;
}


- (void)keyboardWillHide:(NSNotification *)notification
{
    /* Move the toolbar back to bottom of the screen */
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	CGRect frame = self.inputToolbar.frame;
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        //frame.origin.y = self.view.frame.size.height - frame.size.width;
        if (isIphone5) 
            frame.origin.y = 417;
        else
            frame.origin.y = 329;
    }
    else {
        frame.origin.y = self.view.frame.size.width - frame.size.height;
    }
	self.inputToolbar.frame = frame;
	[UIView commitAnimations];
    keyboardIsVisible = NO;
    
    // fix content view hien thi k0 dung vi tri,bi troi xuong duoi.
    if (isIphone5) {
        [self.tweetWebView setFrame:CGRectMake(self.tweetWebView.frame.origin.x, self.tweetWebView.frame.origin.y, self.tweetWebView.frame.size.width, 417)];
    }
    else{
        [self.tweetWebView setFrame:CGRectMake(self.tweetWebView.frame.origin.x, self.tweetWebView.frame.origin.y, self.tweetWebView.frame.size.width, 329)];
    }
}



#pragma mark - Get data

- (void)reloadData
{
    NSLog(@"-- reloadData HTML string --");
    
    isLoadSuccess = YES;
    
    self.htmlString = [[self tempTweetHTMLBegin] copy];
    
    screenNameDisPlay = [NSString stringWithFormat:@"@%@",screenNameTweet];
    
    NSInteger tweetType = 0;
    
    if (retweetStatusDict) {        
        self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBeginRetweet],[self getIconRetweetBegin],retweeter,idTweetStr,@" ",imageTweet,@" ",nameTweet,screenNameDisPlay];
        
        NSLog(@"beginretweet: %@",self.htmlString);
    }else{
        self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBeginNormal],idTweetStr,@" ",imageTweet,@" ",nameTweet,screenNameDisPlay];
    }

    //get display_url
    if (isMedia == YES && isUrls == YES) {
        [objDict setObject:urlDisplayMedia forKey:urlMedia];
        [objDict setObject:urlDisplayUrls forKey:urlUrls];
    }else if (isMedia == NO && isUrls == NO){
        urlDisplayMedia = nil;
        urlDisplayUrls  = nil;
        urlMedia        = nil;
        urlUrls         = nil;
    }else{
        if (isMedia == YES && isUrls == NO) {
            [objDict setObject:urlDisplayMedia forKey:urlMedia];
        }else{
            [objDict setObject:urlDisplayUrls forKey:urlUrls];
        }
    }
    
    //--messsage
    messageConvert = [[NSString alloc] initWithString:messageTweet];
    messageConvert = [self convertTextWithSharedLink:messageConvert];
    
    NSString *aaa = urlDisplayMedia;
    
    if ([retweetCount isEqualToString:@"0"] && [favoriteCount isEqualToString:@"0"]) {
        if (aaa.length>0) {
            tweetType = 2;
        }else{            
            if ([urlExpandedUrls length] && ([urlExpandedUrls rangeOfString:@"http://www.youtube.com/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"http://m.youtube.com/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"http://youtu.be/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"https://www.youtube.com/"].location != NSNotFound)) {
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://www.youtube.com/watch?v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://m.youtube.com/watch?v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://www.youtube.com/watch?feature=player_embedded&v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://youtu.be/" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@"https://www.youtube.com/embed/"];
                
                if ([urlExpandedUrls rangeOfString:@"http://m.youtube.com/watch?feature=player_embedded&v="].location != NSNotFound) {
                    NSString *replaceStr = [self replaceYoutubeLinkByString:urlExpandedUrls];
                    NSString *convertStr = [NSString stringWithFormat:@"&desktop_uri%@",replaceStr];
                    urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:convertStr withString:@""];
                    
                    urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://m.youtube.com/watch?feature=player_embedded&v=" withString:@"http://www.youtube.com/embed/"];
                }
                
                tweetType = 1;
            }
            else {
                tweetType = 3;
            }
        }
    }else if (![retweetCount isEqualToString:@"0"] && [favoriteCount isEqualToString:@"0"]){
        if (aaa.length>0) {
            tweetType = 5;
        }else{
            if ([urlExpandedUrls length] && ([urlExpandedUrls rangeOfString:@"http://www.youtube.com/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"http://m.youtube.com/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"http://youtu.be/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"https://www.youtube.com/"].location != NSNotFound)) {
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://www.youtube.com/watch?v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://m.youtube.com/watch?v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://www.youtube.com/watch?feature=player_embedded&v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://youtu.be/" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@"https://www.youtube.com/embed/"];
                
                if ([urlExpandedUrls rangeOfString:@"http://m.youtube.com/watch?feature=player_embedded&v="].location != NSNotFound) {
                    NSString *replaceStr = [self replaceYoutubeLinkByString:urlExpandedUrls];
                    NSString *convertStr = [NSString stringWithFormat:@"&desktop_uri%@",replaceStr];
                    urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:convertStr withString:@""];
                    
                    urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://m.youtube.com/watch?feature=player_embedded&v=" withString:@"http://www.youtube.com/embed/"];
                }

                tweetType = 4;
            }
            else {
                tweetType = 6;
            }
        }
    }else if ([retweetCount isEqualToString:@"0"] && ![favoriteCount isEqualToString:@"0"]){
        if (aaa.length>0) {
            tweetType = 8;
        }else{
            if ([urlExpandedUrls length] && ([urlExpandedUrls rangeOfString:@"http://www.youtube.com/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"http://m.youtube.com/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"http://youtu.be/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"https://www.youtube.com/"].location != NSNotFound)) {
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://www.youtube.com/watch?v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://m.youtube.com/watch?v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://www.youtube.com/watch?feature=player_embedded&v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://youtu.be/" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@"https://www.youtube.com/embed/"];
                
                if ([urlExpandedUrls rangeOfString:@"http://m.youtube.com/watch?feature=player_embedded&v="].location != NSNotFound) {
                    NSString *replaceStr = [self replaceYoutubeLinkByString:urlExpandedUrls];
                    NSString *convertStr = [NSString stringWithFormat:@"&desktop_uri%@",replaceStr];
                    urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:convertStr withString:@""];
                    
                    urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://m.youtube.com/watch?feature=player_embedded&v=" withString:@"http://www.youtube.com/embed/"];
                }
                
                tweetType = 7;
            }
            else {
                tweetType = 9;
            }
        }
    }else{
        if (aaa.length>0) {
            tweetType = 11;
        }else{
            if ([urlExpandedUrls length] && ([urlExpandedUrls rangeOfString:@"http://www.youtube.com/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"http://m.youtube.com/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"http://youtu.be/"].location != NSNotFound || [urlExpandedUrls rangeOfString:@"https://www.youtube.com/"].location != NSNotFound)) {
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://www.youtube.com/watch?v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://m.youtube.com/watch?v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://www.youtube.com/watch?feature=player_embedded&v=" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://youtu.be/" withString:@"http://www.youtube.com/embed/"];
                
                urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"https://www.youtube.com/watch?v=" withString:@"https://www.youtube.com/embed/"];
                
                if ([urlExpandedUrls rangeOfString:@"http://m.youtube.com/watch?feature=player_embedded&v="].location != NSNotFound) {
                    NSString *replaceStr = [self replaceYoutubeLinkByString:urlExpandedUrls];
                    NSString *convertStr = [NSString stringWithFormat:@"&desktop_uri%@",replaceStr];
                    urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:convertStr withString:@""];
                    
                    urlExpandedUrls = [urlExpandedUrls stringByReplacingOccurrencesOfString:@"http://m.youtube.com/watch?feature=player_embedded&v=" withString:@"http://www.youtube.com/embed/"];
                }
                
                tweetType = 10;
            }
            else {
                tweetType = 12;
            }
        }
    }
    
    //-- switch tweet type
    switch (tweetType) {
        case 1:
            //video
            self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyVideo],messageConvert,dateTweet,urlExpandedUrls,@"margin-left: 10px;",@"width: 290px;",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            break;
        case 2:
            //--photo
            self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyPhoto],messageConvert,dateTweet,idTweetStr,urlExpandedMedia,@"margin-left: 10px;",@"width: 290px;",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            break;
        case 3:
            //--status
            self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyStatus],messageConvert,dateTweet,[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            break;
        case 4:
            //video
            if ([retweetCount isEqualToString:@"1"]) {
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyVideoRetweet],messageConvert,dateTweet,urlExpandedUrls,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEET",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else{
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyVideoRetweet],messageConvert,dateTweet,urlExpandedUrls,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEETS",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }
            break;
        case 5:
            //--photo
            if ([retweetCount isEqualToString:@"1"]) {
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyPhotoRetweet],messageConvert,dateTweet,idTweetStr,urlExpandedMedia,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEET",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else{
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyPhotoRetweet],messageConvert,dateTweet,idTweetStr,urlExpandedMedia,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEETS",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }
            break;
        case 6:
            //--status
            if ([retweetCount isEqualToString:@"1"]) {
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyStatusRetweet],messageConvert,dateTweet,retweetCount,@"RETWEET",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else{
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyStatusRetweet],messageConvert,dateTweet,retweetCount,@"RETWEETS",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }
            break;
        case 7:
            //video
            if ([favoriteCount isEqualToString:@"1"]) {
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyVideoRetweet],messageConvert,dateTweet,urlExpandedUrls,@"margin-left: 10px;",@"width: 290px;",favoriteCount,@"FAVORITE",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else{
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyVideoRetweet],messageConvert,dateTweet,urlExpandedUrls,@"margin-left: 10px;",@"width: 290px;",favoriteCount,@"FAVORITES",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }
            break;
        case 8:
            //--photo
            if ([favoriteCount isEqualToString:@"1"]) {
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyPhotoRetweet],messageConvert,dateTweet,idTweetStr,urlExpandedMedia,@"margin-left: 10px;",@"width: 290px;",favoriteCount,@"FAVORITE",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else{
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyPhotoRetweet],messageConvert,dateTweet,idTweetStr,urlExpandedMedia,@"margin-left: 10px;",@"width: 290px;",favoriteCount,@"FAVORITES",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }
            break;
        case 9:
            //--status
            if ([favoriteCount isEqualToString:@"1"]) {
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyStatusRetweet],messageConvert,dateTweet,favoriteCount,@"FAVORITE",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else{
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyStatusRetweet],messageConvert,dateTweet,favoriteCount,@"FAVORITES",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }
            break;
        case 10:
            //video
            if ([retweetCount isEqualToString:@"1"] && [favoriteCount isEqualToString:@"1"]) {
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyVideoRetweetFavorite],messageConvert,dateTweet,urlExpandedUrls,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEET",favoriteCount,@"FAVORITE",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else if (![retweetCount isEqualToString:@"1"] && [favoriteCount isEqualToString:@"1"]){
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyVideoRetweetFavorite],messageConvert,dateTweet,urlExpandedUrls,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEETS",favoriteCount,@"FAVORITE",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else if ([retweetCount isEqualToString:@"1"] && ![favoriteCount isEqualToString:@"1"]){
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyVideoRetweetFavorite],messageConvert,dateTweet,urlExpandedUrls,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEET",favoriteCount,@"FAVORITES",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else{
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyVideoRetweetFavorite],messageConvert,dateTweet,urlExpandedUrls,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEETS",favoriteCount,@"FAVORITES",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }
            break;
        case 11:
            //--photo
            if ([retweetCount isEqualToString:@"1"] && [favoriteCount isEqualToString:@"1"]) {
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyPhotoRetweetFavorite],messageConvert,dateTweet,idTweetStr,urlExpandedMedia,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEET",favoriteCount,@"FAVORITE",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else if (![retweetCount isEqualToString:@"1"] && [favoriteCount isEqualToString:@"1"]){
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyPhotoRetweetFavorite],messageConvert,dateTweet,idTweetStr,urlExpandedMedia,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEETS",favoriteCount,@"FAVORITE",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else if ([retweetCount isEqualToString:@"1"] && ![favoriteCount isEqualToString:@"1"]){
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyPhotoRetweetFavorite],messageConvert,dateTweet,idTweetStr,urlExpandedMedia,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEET",favoriteCount,@"FAVORITES",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else{
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyPhotoRetweetFavorite],messageConvert,dateTweet,idTweetStr,urlExpandedMedia,@"margin-left: 10px;",@"width: 290px;",retweetCount,@"RETWEETS",favoriteCount,@"FAVORITES",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }
            break;
        case 12:
            //--status
            if ([retweetCount isEqualToString:@"1"] && [favoriteCount isEqualToString:@"1"]) {
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyStatusRetweetFavorite],messageConvert,dateTweet,retweetCount,@"RETWEET",favoriteCount,@"FAVORITE",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else if (![retweetCount isEqualToString:@"1"] && [favoriteCount isEqualToString:@"1"]){
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyStatusRetweetFavorite],messageConvert,dateTweet,retweetCount,@"RETWEETS",favoriteCount,@"FAVORITE",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }else if ([retweetCount isEqualToString:@"1"] && ![favoriteCount isEqualToString:@"1"]){
                // thuy fix crash: add @""
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyStatusRetweetFavorite],
                                   messageConvert,
                                   dateTweet,
                                   retweetCount,
                                   @"RETWEET",
                                   favoriteCount,
                                   @"FAVORITES",
                                   [self getIconReply],
                                   [self getIconRetweet],
                                   [self getIconMore],
                                   @""];
            }else{
                self.htmlString = [self.htmlString stringByAppendingFormat:[self tempTweetBodyStatusRetweetFavorite],messageConvert,dateTweet,retweetCount,@"RETWEETS",favoriteCount,@"FAVORITES",[self getIconReply],[self getIconRetweet],[self getIconFavorite],[self getIconMore]];
            }
            break;
            
        default:
            break;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{ 
        self.htmlString = [self.htmlString stringByAppendingString:[self tempTweetHTMLEnd]];
        NSLog(@"HTML DetailsTweet//: %@",self.htmlString);
        [self.tweetWebView loadHTMLString:self.htmlString baseURL:nil];
        [NetworkActivity hide];
    });
    
    NSLog(@"-- reloadData HTML DONE --");
}


- (void)getAllData
{
    //display image avatar
    if (nil != imageTweet && imageTweet.length > 5) {
        [self.imgAvatar loadFromURL:[NSURL URLWithString:imageTweet]];
    }
    //-- Set border & radius for image
    self.imgAvatar.layer.masksToBounds = YES;
    self.imgAvatar.layer.cornerRadius = 4;
    [self.imgAvatar setBackgroundColor:[UIColor clearColor]];
    
    //-- get data for cell
    self.nameLabel  = [self customLableWithNSAttributedstring:self.nameLabel withfristString:nameTweet withSecondString:screenNameDisPlay withFontStr1:[UIFont boldSystemFontOfSize:13] withFontStr2:[UIFont systemFontOfSize:12]];
    
    //display date label
    NSLog(@"test crash");
    self.dateLabel.text = [Utility dateTimeStringForTwitterTime:createAtTweet];
    NSLog(@"no crash");
    
    //--messsage
    messageConvert = [[NSString alloc] initWithString:messageTweet];
    messageConvert = [self convertTextWithSharedLink:messageConvert];
    
    [self.wvContent loadHTMLString:[self cssStringForTweet:messageConvert] baseURL:nil];
    self.wvContent.dataDetectorTypes = UIDataDetectorTypeLink;
    self.wvContent.delegate = self;
    self.wvContent.scrollView.scrollEnabled = NO;
    self.wvContent.opaque = NO;
    self.wvContent.backgroundColor = [UIColor clearColor];
    
    [self.imgTweet loadFromURL:[NSURL URLWithString:urlExpandedMedia]];
}



#pragma mark - Prepare for convert

// find link in msg and show link by hyperlink
- (NSString*)convertTextWithSharedLink:(NSString*)msg
{
    NSMutableCharacterSet *cset = [[NSMutableCharacterSet alloc] init];
    [cset addCharactersInString:@" "];
    [cset addCharactersInString:@"\n"];
    
    NSArray *arrString = [msg componentsSeparatedByCharactersInSet:cset];
    
    for(int i=0; i<arrString.count;i++){
        
        //-- check "#" string        
        NSString* firtString = @"";
        
        if ([[arrString objectAtIndex:i] length] > 0) {
            firtString = [[arrString objectAtIndex:i] substringToIndex:1];
        }
        
        if([firtString isEqualToString:@"#"] && ![[arrString objectAtIndex:i] isEqualToString:@"#"])
        {
            NSString *link = [arrString objectAtIndex:i];
            NSString* newlink = [NSString stringWithFormat:@"<a href=\"http://null/trendLink/%@\" style=\"text-decoration:none;\">%@</a>",link,link];
            
            msg = [msg stringByReplacingOccurrencesOfString:link withString:newlink];
        }
        
        //-- check "@" string 
        if([firtString isEqualToString:@"@"] && ![[arrString objectAtIndex:i] isEqualToString:@"@"])
        {
            NSString *link = [arrString objectAtIndex:i];
            NSString* newlink = [NSString stringWithFormat:@"<a href=\"http://null/profileLink/%@\" style=\"text-decoration:none;\">%@</a>",link,link];
            
            msg = [msg stringByReplacingOccurrencesOfString:link withString:newlink];
        }
        
        msg = [msg stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    }
    
    return msg;
}


- (NSString*)convertTextToHTMLText:(NSString*)text
{
    text = [text stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    NSArray *arr = [text componentsSeparatedByString:@"<br>"];
    NSMutableArray *arrNew = [[NSMutableArray alloc] init];
    for (NSString *aStr in arr) {
        [arrNew addObject:[aStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    }
    
    NSString* textConverted = [arrNew componentsJoinedByString:@" <br> "];
    
    return textConverted;
}


- (NSString*)tempTweetHTMLBegin
{
    if (!self._tempTweetHTMLBegin) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetHTMLBegin" ofType:@"txt"];
        self._tempTweetHTMLBegin = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetHTMLBegin;
}


- (NSString*)tempTweetHTMLEnd
{
    if (!self._tempTweetHTMLEnd) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetHTMLEnd" ofType:@"txt"];
        self._tempTweetHTMLEnd = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetHTMLEnd;
}
 
 
- (NSString*)tempTweetBeginNormal
{
    if (!self._tempTweetBeginNormal) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetBeginNormal" ofType:@"txt"];
        self._tempTweetBeginNormal = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetBeginNormal;
}


- (NSString*)tempTweetBeginRetweet
{
    if (!self._tempTweetBeginRetweet) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetBeginRetweet" ofType:@"txt"];
        self._tempTweetBeginRetweet = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetBeginRetweet;
}


- (NSString*)tempTweetBodyStatus
{
    if (!self._tempTweetBodyStatus) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetBodyStatus" ofType:@"txt"];
        self._tempTweetBodyStatus = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetBodyStatus;
}


- (NSString*)tempTweetBodyPhoto
{
    if (!self._tempTweetBodyPhoto) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetBodyPhoto" ofType:@"txt"];
        self._tempTweetBodyPhoto = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetBodyPhoto;
}


- (NSString*)tempTweetBodyVideo
{
    if (!self._tempTweetBodyVideo) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetBodyVideo" ofType:@"txt"];
        self._tempTweetBodyVideo = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetBodyVideo;
}


- (NSString*)tempTweetBodyStatusRetweet
{
    if (!self._tempTweetBodyStatusRetweet) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetBodyStatusRetweet" ofType:@"txt"];
        self._tempTweetBodyStatusRetweet = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetBodyStatusRetweet;
}


- (NSString*)tempTweetBodyPhotoRetweet
{
    if (!self._tempTweetBodyPhotoRetweet) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetBodyPhotoRetweet" ofType:@"txt"];
        self._tempTweetBodyPhotoRetweet = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetBodyPhotoRetweet;
}


- (NSString*)tempTweetBodyVideoRetweet
{
    if (!self._tempTweetBodyVideoRetweet) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetBodyVideoRetweet" ofType:@"txt"];
        self._tempTweetBodyVideoRetweet = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetBodyVideoRetweet;
}


- (NSString*)tempTweetBodyStatusRetweetFavorite
{
    if (!self._tempTweetBodyStatusRetweetFavorite) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetBodyStatusRetweetFavorite" ofType:@"txt"];
        self._tempTweetBodyStatusRetweetFavorite = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetBodyStatusRetweetFavorite;
}


- (NSString*)tempTweetBodyPhotoRetweetFavorite
{
    if (!self._tempTweetBodyPhotoRetweetFavorite) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetBodyPhotoRetweetFavorite" ofType:@"txt"];
        self._tempTweetBodyPhotoRetweetFavorite = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetBodyPhotoRetweetFavorite;
}


- (NSString*)tempTweetBodyVideoRetweetFavorite
{
    if (!self._tempTweetBodyVideoRetweetFavorite) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetBodyVideoRetweetFavorite" ofType:@"txt"];
        self._tempTweetBodyVideoRetweetFavorite = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetBodyVideoRetweetFavorite;
}


- (NSString*)tempSendMailTweet
{
    if (!self._tempSendMailTweet) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempSendMailTweet" ofType:@"txt"];
        self._tempSendMailTweet = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempSendMailTweet;
}


- (NSString*)tempTweetEmptyBegin
{
    if (!self._tempTweetEmptyBegin) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tempTweetEmptyBegin" ofType:@"txt"];
        self._tempTweetEmptyBegin = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._tempTweetEmptyBegin;
}


#pragma mark - webview Delegate

- (NSString *)decodeHTMLEntities:(NSString *)string {
    // Reserved Characters in HTML
    string = [string stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&apos;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    
    // ISO 8859-1 Symbols
    string = [string stringByReplacingOccurrencesOfString:@"&iexcl;" withString:@"¡"];
    string = [string stringByReplacingOccurrencesOfString:@"&cent;" withString:@"¢"];
    string = [string stringByReplacingOccurrencesOfString:@"&pound;" withString:@"£"];
    string = [string stringByReplacingOccurrencesOfString:@"&curren;" withString:@"¤"];
    string = [string stringByReplacingOccurrencesOfString:@"&yen;" withString:@"¥"];
    string = [string stringByReplacingOccurrencesOfString:@"&brvbar;" withString:@"¦"];
    string = [string stringByReplacingOccurrencesOfString:@"&sect;" withString:@"§"];
    string = [string stringByReplacingOccurrencesOfString:@"&uml;" withString:@"¨"];
    string = [string stringByReplacingOccurrencesOfString:@"&copy;" withString:@"©"];
    string = [string stringByReplacingOccurrencesOfString:@"&ordf;" withString:@"ª"];
    string = [string stringByReplacingOccurrencesOfString:@"&laquo;" withString:@"«"];
    string = [string stringByReplacingOccurrencesOfString:@"&not;" withString:@"¬"];
    string = [string stringByReplacingOccurrencesOfString:@"&shy;" withString:@"    "];
    string = [string stringByReplacingOccurrencesOfString:@"&reg;" withString:@"®"];
    string = [string stringByReplacingOccurrencesOfString:@"&macr;" withString:@"¯"];
    string = [string stringByReplacingOccurrencesOfString:@"&deg;" withString:@"°"];
    string = [string stringByReplacingOccurrencesOfString:@"&plusmn;" withString:@"±       "];
    string = [string stringByReplacingOccurrencesOfString:@"&sup2;" withString:@"²"];
    string = [string stringByReplacingOccurrencesOfString:@"&sup3;" withString:@"³"];
    string = [string stringByReplacingOccurrencesOfString:@"&acute;" withString:@"´"];
    string = [string stringByReplacingOccurrencesOfString:@"&micro;" withString:@"µ"];
    string = [string stringByReplacingOccurrencesOfString:@"&para;" withString:@"¶"];
    string = [string stringByReplacingOccurrencesOfString:@"&middot;" withString:@"·"];
    string = [string stringByReplacingOccurrencesOfString:@"&cedil;" withString:@"¸"];
    string = [string stringByReplacingOccurrencesOfString:@"&sup1;" withString:@"¹"];
    string = [string stringByReplacingOccurrencesOfString:@"&ordm;" withString:@"º"];
    string = [string stringByReplacingOccurrencesOfString:@"&raquo;" withString:@"»"];
    string = [string stringByReplacingOccurrencesOfString:@"&frac14;" withString:@"¼"];
    string = [string stringByReplacingOccurrencesOfString:@"&frac12;" withString:@"½"];
    string = [string stringByReplacingOccurrencesOfString:@"&frac34;" withString:@"¾"];
    string = [string stringByReplacingOccurrencesOfString:@"&iquest;" withString:@"¿"];
    string = [string stringByReplacingOccurrencesOfString:@"&times;" withString:@"×"];
    string = [string stringByReplacingOccurrencesOfString:@"&divide;" withString:@"÷"];
    
    // ISO 8859-1 Characters
    string = [string stringByReplacingOccurrencesOfString:@"&Agrave;" withString:@"À"];
    string = [string stringByReplacingOccurrencesOfString:@"&Aacute;" withString:@"Á"];
    string = [string stringByReplacingOccurrencesOfString:@"&Acirc;" withString:@"Â"];
    string = [string stringByReplacingOccurrencesOfString:@"&Atilde;" withString:@"Ã"];
    string = [string stringByReplacingOccurrencesOfString:@"&Auml;" withString:@"Ä"];
    string = [string stringByReplacingOccurrencesOfString:@"&Aring;" withString:@"Å"];
    string = [string stringByReplacingOccurrencesOfString:@"&AElig;" withString:@"Æ"];
    string = [string stringByReplacingOccurrencesOfString:@"&Ccedil;" withString:@"Ç"];
    string = [string stringByReplacingOccurrencesOfString:@"&Egrave;" withString:@"È"];
    string = [string stringByReplacingOccurrencesOfString:@"&Eacute;" withString:@"É"];
    string = [string stringByReplacingOccurrencesOfString:@"&Ecirc;" withString:@"Ê"];
    string = [string stringByReplacingOccurrencesOfString:@"&Euml;" withString:@"Ë"];
    string = [string stringByReplacingOccurrencesOfString:@"&Igrave;" withString:@"Ì"];
    string = [string stringByReplacingOccurrencesOfString:@"&Iacute;" withString:@"Í"];
    string = [string stringByReplacingOccurrencesOfString:@"&Icirc;" withString:@"Î"];
    string = [string stringByReplacingOccurrencesOfString:@"&Iuml;" withString:@"Ï"];
    string = [string stringByReplacingOccurrencesOfString:@"&ETH;" withString:@"Ð"];
    string = [string stringByReplacingOccurrencesOfString:@"&Ntilde;" withString:@"Ñ"];
    string = [string stringByReplacingOccurrencesOfString:@"&Ograve;" withString:@"Ò"];
    string = [string stringByReplacingOccurrencesOfString:@"&Oacute;" withString:@"Ó"];
    string = [string stringByReplacingOccurrencesOfString:@"&Ocirc;" withString:@"Ô"];
    string = [string stringByReplacingOccurrencesOfString:@"&Otilde;" withString:@"Õ"];
    string = [string stringByReplacingOccurrencesOfString:@"&Ouml;" withString:@"Ö"];
    string = [string stringByReplacingOccurrencesOfString:@"&Oslash;" withString:@"Ø"];
    string = [string stringByReplacingOccurrencesOfString:@"&Ugrave;" withString:@"Ù"];
    string = [string stringByReplacingOccurrencesOfString:@"&Uacute;" withString:@"Ú"];
    string = [string stringByReplacingOccurrencesOfString:@"&Ucirc;" withString:@"Û"];
    string = [string stringByReplacingOccurrencesOfString:@"&Uuml;" withString:@"Ü"];
    string = [string stringByReplacingOccurrencesOfString:@"&Yacute;" withString:@"Ý"];
    string = [string stringByReplacingOccurrencesOfString:@"&THORN;" withString:@"Þ"];
    string = [string stringByReplacingOccurrencesOfString:@"&szlig;" withString:@"ß"];
    string = [string stringByReplacingOccurrencesOfString:@"&agrave;" withString:@"à"];
    string = [string stringByReplacingOccurrencesOfString:@"&aacute;" withString:@"á"];
    string = [string stringByReplacingOccurrencesOfString:@"&acirc;" withString:@"â"];
    string = [string stringByReplacingOccurrencesOfString:@"&atilde;" withString:@"ã"];
    string = [string stringByReplacingOccurrencesOfString:@"&auml;" withString:@"ä"];
    string = [string stringByReplacingOccurrencesOfString:@"&aring;" withString:@"å"];
    string = [string stringByReplacingOccurrencesOfString:@"&aelig;" withString:@"æ"];
    string = [string stringByReplacingOccurrencesOfString:@"&ccedil;" withString:@"ç"];
    string = [string stringByReplacingOccurrencesOfString:@"&egrave;" withString:@"è"];
    string = [string stringByReplacingOccurrencesOfString:@"&eacute;" withString:@"é"];
    string = [string stringByReplacingOccurrencesOfString:@"&ecirc;" withString:@"ê"];
    string = [string stringByReplacingOccurrencesOfString:@"&euml;" withString:@"ë"];
    string = [string stringByReplacingOccurrencesOfString:@"&igrave;" withString:@"ì"];
    string = [string stringByReplacingOccurrencesOfString:@"&iacute;" withString:@"í"];
    string = [string stringByReplacingOccurrencesOfString:@"&icirc;" withString:@"î"];
    string = [string stringByReplacingOccurrencesOfString:@"&iuml;" withString:@"ï"];
    string = [string stringByReplacingOccurrencesOfString:@"&eth;" withString:@"ð"];
    string = [string stringByReplacingOccurrencesOfString:@"&ntilde;" withString:@"ñ"];
    string = [string stringByReplacingOccurrencesOfString:@"&ograve;" withString:@"ò"];
    string = [string stringByReplacingOccurrencesOfString:@"&oacute;" withString:@"ó"];
    string = [string stringByReplacingOccurrencesOfString:@"&ocirc;" withString:@"ô"];
    string = [string stringByReplacingOccurrencesOfString:@"&otilde;" withString:@"õ"];
    string = [string stringByReplacingOccurrencesOfString:@"&ouml;" withString:@"ö"];
    string = [string stringByReplacingOccurrencesOfString:@"&oslash;" withString:@"ø"];
    string = [string stringByReplacingOccurrencesOfString:@"&ugrave;" withString:@"ù"];
    string = [string stringByReplacingOccurrencesOfString:@"&uacute;" withString:@"ú"];
    string = [string stringByReplacingOccurrencesOfString:@"&ucirc;" withString:@"û"];
    string = [string stringByReplacingOccurrencesOfString:@"&uuml;" withString:@"ü"];
    string = [string stringByReplacingOccurrencesOfString:@"&yacute;" withString:@"ý"];
    string = [string stringByReplacingOccurrencesOfString:@"&thorn;" withString:@"þ"];
    string = [string stringByReplacingOccurrencesOfString:@"&yuml;" withString:@"ÿ"];
    string = [string stringByReplacingOccurrencesOfString:@"%20" withString:@" "];
    
    return string;
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *urlString = request.URL.absoluteString;
    
    NSRange replyRange          = [urlString rangeOfString:@"reply"];
    NSRange retweetRange        = [urlString rangeOfString:@"retweet"];
    NSRange quotetweetRange     = [urlString rangeOfString:@"quotetweet"];
    NSRange favoritetweetRange  = [urlString rangeOfString:@"favorite"];
    NSRange moretweetRange      = [urlString rangeOfString:@"more"];
    NSRange clickLinkRange      = [urlString rangeOfString:@"http://null/clickLink/"];
    NSRange trendLinkRange      = [urlString rangeOfString:@"http://null/trendLink/"];
    NSRange profileLinkRange    = [urlString rangeOfString:@"http://null/profileLink/"];
    NSRange photoclickRange     = [urlString rangeOfString:@"http://null/clickPhoto/"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (replyRange.location != NSNotFound)
    {
        [self clickButtonReply];
        return NO;
    }
    
    if (retweetRange.location != NSNotFound)
    {
        if ([protectedCount isEqualToString:@"0"] && ![self.screenNameDisPlay isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"twitterAccountName"]])  {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
            
            [actionSheet addButtonWithTitle:@"Retweet"];
            [actionSheet addButtonWithTitle:@"Quote Tweet"];
            
            //ADD CANCEL BUTTON TO ACTION SHEET
            [actionSheet addButtonWithTitle:@"Cancel"];
            [actionSheet setCancelButtonIndex:[actionSheet numberOfButtons]-1];
            
            [actionSheet showInView:appDelegate.window];
        }
        
        return NO;
    }
    
    if (quotetweetRange.location != NSNotFound)
    {
        [self clickButtonQuoteTweet];
        return NO;
    }
    
    if (clickLinkRange.location != NSNotFound) {
        appDelegate.isOpenTweetLink = YES;
        
        NSString* urlString1 = [urlString substringFromIndex:22];
        
        ShowLinkViewController *wv = [[ShowLinkViewController alloc] initWithNibName:@"ShowLinkViewController" bundle:nil];
        wv.strLinkUrl = urlString1;
        [self.navigationController pushViewController:wv animated:YES];
        
        return NO;
    }
    
    if (profileLinkRange.location != NSNotFound){
        appDelegate.isOpenTweetLink = YES;
        
        NSString* urlString1 = [NSString stringWithFormat:@"https://twitter.com/%@",[urlString substringFromIndex:25]];
        
        ShowLinkViewController *wv = [[ShowLinkViewController alloc] initWithNibName:@"ShowLinkViewController" bundle:nil];
        wv.strLinkUrl = urlString1;
        [self.navigationController pushViewController:wv animated:YES];
        
        return NO;
    }
    
    if (trendLinkRange.location != NSNotFound){
        appDelegate.isOpenTweetLink = YES;
        
        NSString *aaa = @"https://twitter.com/search?q=%23";
        NSString* urlString1 = [NSString stringWithFormat:@"%@%@&src=hash",aaa,[urlString substringFromIndex:23]];
        
        ShowLinkViewController *wv = [[ShowLinkViewController alloc] initWithNibName:@"ShowLinkViewController" bundle:nil];
        wv.strLinkUrl = urlString1;
        [self.navigationController pushViewController:wv animated:YES];
        
        return NO;
    }
    
    if (photoclickRange.location != NSNotFound)
    {
        isShowing = YES;
        
        //--when lick on photo, viewShowImage will be displayed, tabbarcontroller and navigationController will be hidden
        [self.view addSubview:viewShowImage];
        isShowImage = YES;
        [self hideTabBar:self.tabBarController];
        [self.navigationController setNavigationBarHidden:YES];
        
        [self getAllData];
        
        //--push status favorited to detail screen
        [self setButtonFavorite];

        return NO;
    }
    
    if (favoritetweetRange.location != NSNotFound)
    {
        if (isLoadSuccess) {
            isLoadSuccess = NO;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self clickButtonFavorite];
            });
        }        
        
        return NO;
    }
    
    if (moretweetRange.location != NSNotFound)
    {
        [self clickButtonMore];
        
        return NO;
    }
    
    return YES;
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    //
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [NetworkActivity hide];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [NetworkActivity hide];
}


- (NSString*)cssStringForTweet:(NSString*)tweet {
    return [NSString stringWithFormat:[self cssBaseString],tweet];
}


- (NSString*)cssBaseString {
    if (!self._cssBaseString) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"CssBaseStringTextWhite" ofType:@"txt"];
        NSError *error;
        self._cssBaseString = [NSString stringWithContentsOfFile:path encoding:NSASCIIStringEncoding error:&error];
        if (error) NSLog(@"CssBaseString error");
        if (APP_DEBUG) NSLog(@"%@",self._cssBaseString);
    }
    return self._cssBaseString;
}



#pragma mark - FAVORITE

- (void)createFavorite
{    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *paramDict = [NSDictionary dictionaryWithObjectsAndKeys:
                               idTweetStr, @"id", nil];
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/favorites/create.json"];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:myUrl parameters:paramDict];
    request.account = [appDelegate selectedTwitterAccount];
    [NetworkActivity show];
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         NSError *jsonError = nil;
         id timeLineData = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
         
         dispatch_async(dispatch_get_main_queue(), ^{
             NSLog(@"%@",timeLineData);
             [NetworkActivity hide];
             favorited = @"1";

             NSLog(@"createFavorite Successfully!");
             
             //--save value @"favorited" to dictionaryTweet
             [self.dictionaryTweet setObject:[NSNumber numberWithInteger:1] forKey:@"favorited"];
             
             NSNumber *addFavoriteCount = [NSNumber numberWithInteger:[favoriteCount intValue]+1];
             favoriteCount = [addFavoriteCount stringValue];
             
             //--//--save value @"favorite_count" to dictionaryTweet
             [self.dictionaryTweet setObject:addFavoriteCount forKey:@"favorite_count"];
             
             self.tweetObj.favorited= 1;
             self.tweetObj.favorite_count = [addFavoriteCount integerValue];
             
             [self.btnFavorite setImage:[UIImage imageNamed:@"btn_star_active@2x"] forState:UIControlStateNormal];
             
             isLoadSuccess = NO;
             
             [self reloadData];
             
             [self.view makeToast:@"Favorite Success!"
                         duration:3.0
                         position:@"top"
                            title:nil];
         });
     }];
}


- (void)destroyFavorite
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *paramDict = [NSDictionary dictionaryWithObjectsAndKeys:
                               idTweetStr, @"id", nil];
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/favorites/destroy.json"];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:myUrl parameters:paramDict];
    request.account = [appDelegate selectedTwitterAccount];
    [NetworkActivity show];
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         NSError *jsonError = nil;
         id timeLineData = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
         
         dispatch_async(dispatch_get_main_queue(), ^{
             self.isClickFavourite = 0;
             NSLog(@"%@",timeLineData);
             [NetworkActivity hide];
             favorited = @"0";
             
             NSLog(@"destroyFavorite Successfully!");
             //--save value @"favorited" to dictionaryTweet
             [self.dictionaryTweet setObject:[NSNumber numberWithInteger:0] forKey:@"favorited"];
             
             NSNumber *destroyFavoriteCount = [NSNumber numberWithInteger:[favoriteCount intValue]-1];
             if (destroyFavoriteCount<0) {
                 destroyFavoriteCount = 0;
             }
             
             favoriteCount = [destroyFavoriteCount stringValue];
             
             //--//--save value @"favorite_count" to dictionaryTweet
             [self.dictionaryTweet setObject:favoriteCount forKey:@"favorite_count"];
             
             self.tweetObj.favorited= 0;
             self.tweetObj.favorite_count = [favoriteCount integerValue];
             
             [self.btnFavorite setImage:[UIImage imageNamed:@"btn_star_inactive@2x"] forState:UIControlStateNormal];
             
             isLoadSuccess = NO;
             
             [self reloadData];
             
             [self.view makeToast:@"Unfavorite Success!"
                         duration:3.0
                         position:@"top"
                            title:nil];
         });
     }];
}


- (void)getListFavorites
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/mentions_timeline.json"];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            screenNameTweet,@"screen_name", nil];;
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:myUrl parameters:params];
    
    request.account = [appDelegate selectedTwitterAccount];
    NSLog(@"twitter perform request 1");
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if(error){
             NSLog(@"Error:%@",error);
         }else {
             NSError *jsonError = nil;
             NSDictionary *result_dict = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
             NSLog(@"result_dict /:/: %@",result_dict);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 //
                 
             });
         }
     }];
}


/*Currently I'm doing a best-effort hack to mirror the functionality:
for user @bob's tweet ID=1234

I query /1.1/search/tweets?q=@bob&since_id=1234
page through results and check if the in_reply_to_status_id_str matches the original tweet ID.
*/

- (void)getListReplies
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/search/tweets"];
    NSString *inRep = @"true";
    
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            screenNameTweet, @"q", idTweetStr, @"id",inRep,@"in_reply_to_status_id_str",nil];
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:myUrl parameters:params];
    
    request.account = [appDelegate selectedTwitterAccount];
    NSLog(@"twitter perform request 1");
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if(error){
             NSLog(@"Error:%@",error);
         }else {
             NSError *jsonError = nil;
             NSDictionary *result_dict = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
             NSLog(@"result_dict 222 /:/: %@",result_dict);
         }
     }];
}



#pragma mark - RETWEET

- (void) retweetStartCountDown
{
    CountDownViewController *controller = nil;
    if (isIphone5)
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController-568h" bundle:nil];
    else
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController" bundle:nil];
    
    controller.messageText = self.tweetObj.quocteText;
    controller.recipientText = [NSString stringWithFormat:@"Retweet of %@", screenNameDisPlay];
    controller.detailsTweetDelegate = self;
    controller.abId = [NSNumber numberWithInt:-1];   // HARD CODE AS PUBLIC //
    controller.imagePath = profileUrlBig;
    controller.sendType = RETWEET_TYPE;
    
    UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromRight;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
    [UIView setAnimationDuration:COUNTDOWN_TRANSITION];
    [UIView setAnimationDelegate:controller];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    [self presentViewController:controller animated:YES completion:nil];
    [UIView commitAnimations];
}



#pragma mark - ACTION

- (void)clickButtonReply
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *bodyStr = [NSString stringWithFormat:@"%@ ",screenNameDisPlay];
    [appDelegate displayTwitterComposerSheet:@"Public Tweet"
                                        body:bodyStr
                                        abId:[NSNumber numberWithInt:-1]
                                   imagePath:profileUrlBig
                                        view:self
                                       title:@"New Tweet"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:followNameDict forKey:@"nameDict"];
    [defaults setObject:followUserDict forKey:@"userDict"];
    [defaults setObject:screenNameTweet forKey:@"screenNameTweet"];    
    [defaults setBool:NO forKey:@"sendDirectMsg"];
    
    [defaults synchronize];
    
    [appDelegate setIsNewReply:YES];
    [appDelegate setIsNewTweet:NO];
}


- (void)clickButtonRetweet
{    
    [self retweetStartCountDown];
}


- (void)clickButtonQuoteTweet
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *quotedMessage = self.tweetObj.quocteText;
    NSString *quotedUserName = screenNameDisPlay;
    NSString *quoteImagePath = profileUrlBig;
    NSString *finalMessage = quotedMessage;
    if (![quotedMessage hasPrefix:@"RT @"]) {
        finalMessage = [NSString stringWithFormat:@"RT %@: %@",quotedUserName,quotedMessage];
        finalMessage = [Utility decodeHTMLEntities:finalMessage];
    }
    
    [appDelegate displayTwitterComposerSheet:@"Public Tweet"
                                        body:finalMessage
                                        abId:[NSNumber numberWithInt:-1]
                                   imagePath:quoteImagePath
                                        view:self
                                       title:@"Quote Tweet"];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"sendDirectMsg"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)clickButtonFavorite
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:warningControllTwitter] || [[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"] == 0)
    {
        if (isYellowStar){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self destroyFavorite];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self createFavorite];
            });
        }
        
        return;
    }
    if (self.isClickFavourite == 0)
    {
        self.isClickFavourite = 1;
        TutorViewController *tuto = nil;
        if (isIphone5)
            tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController-568h" bundle:nil];
        else
            tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
        
        tuto.delegate = (id)self;
        
        if (!isYellowStar){
            tuto.type = typeTWFavorite;
            [self presentPopupViewController:tuto animationType:MJPopupViewAnimationSlideBottomTop bckClickable:NO];
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self destroyFavorite];
            });
        }

    }
    
}


- (void)clickButtonMore
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    
    [actionSheet addButtonWithTitle:@"Mail Tweet"];
    [actionSheet addButtonWithTitle:@"Copy link to Tweet"];
    
    //ADD CANCEL BUTTON TO ACTION SHEET
    [actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet setCancelButtonIndex:[actionSheet numberOfButtons]-1];
    
    [actionSheet showInView:appDelegate.window];
}


- (void)sendMailTweet
{
    NSString *linkProfile = [NSString stringWithFormat:@"https://twitter.com/%@",screenNameTweet];
    NSString *urlApp = @"http://www.besafeapps.com";
    NSString *urlStatus = [NSString stringWithFormat:@"https://twitter.com/%@/status/%@",screenNameTweet,idTweetStr];
    NSString *bodyMail = nil;
    
    bodyMail = [[self tempTweetEmptyBegin] copy];
    
    bodyMail = [bodyMail stringByAppendingFormat:[self tempSendMailTweet],imageTweet,nameTweet,linkProfile,screenNameDisPlay,urlStatus,dateTweet,messageConvert,urlApp];
    
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:[NSString stringWithFormat:@"Tweet from %@ (%@)",nameTweet,screenNameDisPlay]];
        [mailViewController setMessageBody:bodyMail isHTML:YES];
        
        [self presentViewController:mailViewController animated:YES completion:nil];        
    }else {
        NSLog(@"Device is unable to send email in its current state.");
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cannot send mail" message:@"Configure a mail account in your Settings to send mail." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
    }
}


- (void)inputButtonPressed:(NSString *)inputText
{
    CountDownViewController *controller = nil;
    if (isIphone5)
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController-568h" bundle:nil];
    else
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController" bundle:nil];
    
    controller.messageText = inputText;;
    controller.recipientText = [NSString stringWithFormat:@"%@", screenNameDisPlay];
    NSLog(@"controller.recipientText: %@",controller.recipientText);
    controller.detailsTweetDelegate = self;
    controller.abId = [NSNumber numberWithInt:-1];   // HARD CODE AS PUBLIC //
    controller.imagePath = profileUrlBig;
    controller.sendType = REPLY_TWEET_TYPE;
    
    UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromRight;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
    [UIView setAnimationDuration:COUNTDOWN_TRANSITION];
    [UIView setAnimationDelegate:controller];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    [self presentViewController:controller animated:YES completion:nil];
    [UIView commitAnimations];
}


- (void)retweetCountDownComplete
{
    
    NSLog(@"retwitter countDownComplete");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *tweetId = idTweetStr;
    NSString *urlString = [NSString stringWithFormat:@"https://api.twitter.com/1.1/statuses/retweet/%@.json",tweetId];
    NSURL *myUrl = [NSURL URLWithString:urlString];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:myUrl parameters:nil];
    request.account = [appDelegate selectedTwitterAccount];
    NSLog(@"twitter perform request 6");
    [NetworkActivity show];
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         NSError *jsonError = nil;
         id timeLineData = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
         
         dispatch_async(dispatch_get_main_queue(), ^{
             if (APP_DEBUG) NSLog(@" timeLineData: %@",timeLineData);
             [NetworkActivity hide];
             
             [self.view makeToast:@"Retweet Success!"
                         duration:3.0
                         position:@"top"
                            title:nil];
             
             //[appDelegate setIsSuccess:YES];
         });
     }];
}


- (void)replyCountDownComplete:(NSString *)messageText
{
    NSString* text = [NSString stringWithFormat:@"%@ %@",screenNameDisPlay,messageText];
    
    //send reply status
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *urlString = [NSString stringWithFormat:@"https://api.twitter.com/1.1/statuses/update.json"];
    NSURL *myUrl = [NSURL URLWithString:urlString];
    
    NSString *postID = idTweetStr;
    
    //NSMutableDictionary *md = [NSMutableDictionary dictionaryWithObject:text forKey:@"status"];
    NSDictionary *paraStr = [NSDictionary dictionaryWithObjectsAndKeys:postID,@"in_reply_to_status_id",
                             text,@"status",nil];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:myUrl parameters:paraStr];
    request.account = [appDelegate selectedTwitterAccount];
    NSLog(@"twitter perform request 10");
    
    [NetworkActivity show];
    
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         NSError *jsonError = nil;
         id timeLineData = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
         
         dispatch_async(dispatch_get_main_queue(), ^{
             if (APP_DEBUG) NSLog(@" timeLineData: %@",timeLineData);
             [NetworkActivity hide];
             
             [self.view makeToast:@"Reply Success!"
                         duration:3.0
                         position:@"top"
                            title:nil];
             
             [appDelegate setIsSuccess:YES];
         });
     }];
}


- (IBAction)clickToBtnReply:(id)sender
{
    [self clickButtonReply];
}


- (IBAction)clickToBtnRetweet:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    
    [actionSheet addButtonWithTitle:@"Retweet"];
    [actionSheet addButtonWithTitle:@"Quote Tweet"];
    
    //ADD CANCEL BUTTON TO ACTION SHEET
    [actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet setCancelButtonIndex:[actionSheet numberOfButtons]-1];
    
    [actionSheet showInView:appDelegate.window];
}


- (IBAction)clickToBtnFavorite:(id)sender
{
    if (isLoadSuccess) {
        isLoadSuccess = NO;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self clickButtonFavorite];
        });
    }
}


- (IBAction)clickToBtnMore:(id)sender
{
    [self clickButtonMore];
}


/**
 *button btnHideView was placed at the top, when click on it, viewShowImage will be removed and tabBarController, navigationController will be showed
 **/
- (IBAction)clickToBtnHideView:(id)sender
{
    isShowing = NO;
    
    [self.viewShowImage removeFromSuperview];
    [self showTabBar:self.tabBarController];
    [self.navigationController setNavigationBarHidden:NO];

    if (isIphone5) {
        [self.tweetWebView setFrame:CGRectMake(self.tweetWebView.frame.origin.x, self.tweetWebView.frame.origin.y, self.tweetWebView.frame.size.width, 417)];
    }
    else{
        [self.tweetWebView setFrame:CGRectMake(self.tweetWebView.frame.origin.x, self.tweetWebView.frame.origin.y, self.tweetWebView.frame.size.width, 329)];
    }
    
    //fix blank bottom webview
    if (isIphone5){
        if (isShowImage){
            if (isShowing)
                [Utility setFrameOfView:self.viewShowImage withHeightView:568.0 andYCordinate:0.0];
            else
                [Utility setFrameOfView:self.view withHeightView:503.0 andYCordinate:0.0];
        }
        else
            [Utility setFrameOfView:self.view withHeightView:456.0 andYCordinate:0.0];
    }else{
        if (isShowImage){
            if (isShowing)
                [Utility setFrameOfView:self.view withHeightView:460.0 andYCordinate:0.0];
            else
                [Utility setFrameOfView:self.view withHeightView:415.0 andYCordinate:0.0];
        }
        
        else
            [Utility setFrameOfView:self.view withHeightView:416.0 andYCordinate:0.0];// 368 fix bi mat tabbar khi vao slide photo tweets.
    }
}


#pragma mark - Change text

//-- custom lable with NSAttributedstring_Class
- (UILabel *) customLableWithNSAttributedstring:(UILabel *)lbl withfristString:(NSString *) str1 withSecondString:(NSString *) str2 withFontStr1:(UIFont *)fontForStr1 withFontStr2:(UIFont *)fontForStr2
{
    NSDictionary *arialdict = [NSDictionary dictionaryWithObject: fontForStr1 forKey:NSFontAttributeName];
    NSMutableAttributedString *AattrString = [[NSMutableAttributedString alloc] initWithString:str1 attributes: arialdict];
    
    NSDictionary *veradnadict = [NSDictionary dictionaryWithObject:fontForStr2 forKey:NSFontAttributeName];
    NSMutableAttributedString *VattrString = [[NSMutableAttributedString alloc]initWithString:str2 attributes:veradnadict];
    
    [AattrString appendAttributedString:VattrString];
    
    lbl.attributedText = AattrString;
    
    return lbl;
}



#pragma mark - show / hide tabbar

- (void)hideTabBar:(UITabBarController *) tabbarcontroller
{    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    self.tabBarController.tabBar.hidden = YES;
    [self.tabBarController.tabBar setAlpha:0.0];
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480) {
        for(UIView *view in tabbarcontroller.view.subviews)
        {
            if([view isKindOfClass:[UITabBar class]])
            {
                [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
            }else{
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,480)];
            }            
        }        
    }
    if (result.height == 568) {
        for(UIView *view in tabbarcontroller.view.subviews)
        {
            if([view isKindOfClass:[UITabBar class]])
            {
                [view setFrame:CGRectMake(view.frame.origin.x, 568, view.frame.size.width, view.frame.size.height)];
            }else{
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,568)];
            }            
        }        
    }    
    self.tabBarController.tabBar.hidden = YES;
    [UIView commitAnimations];
}


- (void)showTabBar:(UITabBarController *) tabbarcontroller
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.05];
    self.tabBarController.tabBar.hidden = NO;
    [tabbarcontroller.tabBar setAlpha:1.0];
    
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            CGSize result = [[UIScreen mainScreen] bounds].size;
            if (result.height == 480) {
                [view setFrame:CGRectMake(view.frame.origin.x, 431, view.frame.size.width, view.frame.size.height)];
            }
            if (result.height == 568) {
                [view setFrame:CGRectMake(view.frame.origin.x, 519, view.frame.size.width, view.frame.size.height)];
            }
        }
    }
    [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    self.tabBarController.tabBar.hidden = NO;
    
    [UIView commitAnimations];
}



#pragma mark - get Icon app

- (NSString*)getIconReply
{
    if (!self.iconReply) {
        NSString *iconRep = [[NSBundle mainBundle] pathForResource:@"btn_reply@2x" ofType:@"png"];
        self.iconReply = [NSURL fileURLWithPath:iconRep];
    }
    return self.iconReply;
}


- (NSString*)getIconRetweet
{
    NSString *iconRet = nil;
    
    NSLog(@"defaultProfile: %@",defaultProfile);
    
    if ([protectedCount isEqualToString:@"0"] && ![self.screenNameDisPlay isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"twitterAccountName"]]) {
        iconRet = [[NSBundle mainBundle] pathForResource:@"btn_retweet@2x" ofType:@"png"];
    }
    
    else
    {
        iconRet = [[NSBundle mainBundle] pathForResource:@"btn_retweet_protected@2x" ofType:@"png"];
    }
    
    self.iconRetweet = [NSURL fileURLWithPath:iconRet];

    return self.iconRetweet;
}


- (NSString*)getIconRetweetBegin
{
    if (!self.iconRetweetBegin) {
        NSString *iconRet = [[NSBundle mainBundle] pathForResource:@"btn_retweet_protected" ofType:@"png"];
        self.iconRetweetBegin = [NSURL fileURLWithPath:iconRet];
    }
    
    return self.iconRetweetBegin;
}


- (NSString*)getIconFavorite
{    
    NSString *iconRet = nil;
    if ([favorited isEqualToString:@"0"]){
        iconRet = [[NSBundle mainBundle] pathForResource:@"btn_star_inactive@2x" ofType:@"png"];
        isYellowStar = NO;
    }else{
        iconRet = [[NSBundle mainBundle] pathForResource:@"btn_star_active@2x" ofType:@"png"];
        isYellowStar = YES;
    }
    
    self.iconFavorite = [NSURL fileURLWithPath:iconRet];
    
    return self.iconFavorite;
}


- (NSString*)getIconMore
{
    if (!self.iconMore) {
        NSString *iconRet = [[NSBundle mainBundle] pathForResource:@"btn_more@2x" ofType:@"png"];
        self.iconMore = [NSURL fileURLWithPath:iconRet];
    }
    
    return self.iconMore;
}


- (void)setButtonFavorite
{
    if ([favorited isEqualToString:@"0"]){
        [self.btnFavorite setImage:[UIImage imageNamed:@"btn_star_inactive@2x"] forState:UIControlStateNormal];
        
        isYellowStar = NO;
    }else{
        [self.btnFavorite setImage:[UIImage imageNamed:@"btn_star_active@2x"] forState:UIControlStateNormal];
        
        isYellowStar = YES;
    }
}



#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"]) {
        //user canceled
    }else{
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Retweet"]){
            
            [self clickButtonRetweet];
            
        }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Quote Tweet"]){
            
            [self clickButtonQuoteTweet];
            
        }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Mail Tweet"]){
            
            [self sendMailTweet];
            
        }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Copy link to Tweet"]){
            
            //action Copy link to Tweet
            NSMutableDictionary * pasteboardDict = [NSMutableDictionary dictionary];
            NSString *urlStatus = [NSString stringWithFormat:@"https://twitter.com/%@/status/%@",screenNameTweet,idTweetStr];
            if (urlStatus)
                [pasteboardDict setValue:urlStatus forKey:@"public.utf8-plain-text"];
            
            [UIPasteboard generalPasteboard].items = [NSArray arrayWithObject:pasteboardDict];
            
        }else{
            
            // action Report Tweet
            
        }
    }
}



#pragma mark - MFMailComposeViewControllerDelegate

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [NetworkActivity hide];
    
}


- (NSString *)replaceYoutubeLinkByString:(NSString *)link
{
    //request
    NSArray *listStr = [link componentsSeparatedByString:@"&desktop_uri"];
    return  [listStr objectAtIndex:[listStr count]-1];
}



#pragma mark - tutorDelegate

- (void)acceptLike:(warningType)type
{
    self.isClickFavourite = 0;
    switch (type) {
        case typeTWFavorite: {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self createFavorite];
            });
            break;
        }
        case typeTWUnFavorite: {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self destroyFavorite];
            });
            break;
        }
        case typeTWRetweet: {
            [self retweetStartCountDown];
            break;
        }
            
        default:
            break;
    }
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    
    //fix blank bottom webview
    if (isIphone5){
        if (isShowImage){
            if (isShowing) 
                [Utility setFrameOfView:self.viewShowImage withHeightView:568.0 andYCordinate:0.0];
            else
                [Utility setFrameOfView:self.view withHeightView:503.0 andYCordinate:0.0];
        }
        else
            [Utility setFrameOfView:self.view withHeightView:456.0 andYCordinate:0.0];
    }else{
        if (isShowImage){
            if (isShowing) 
                [Utility setFrameOfView:self.view withHeightView:460.0 andYCordinate:0.0];
            else
                [Utility setFrameOfView:self.view withHeightView:415.0 andYCordinate:0.0];
        }
            
        else
            [Utility setFrameOfView:self.view withHeightView:368.0 andYCordinate:0.0];
    }
}


- (void)cancelLike
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    self.isClickFavourite = 0;
    isLoadSuccess = YES;
    
    //fix blank bottom webview
    if (isIphone5){
        if (isShowImage){
            if (isShowing)
                [Utility setFrameOfView:self.viewShowImage withHeightView:568.0 andYCordinate:0.0];
            else
                [Utility setFrameOfView:self.view withHeightView:503.0 andYCordinate:0.0];
        }
        else
            [Utility setFrameOfView:self.view withHeightView:456.0 andYCordinate:0.0];
    }else{
        if (isShowImage){
            if (isShowing)
                [Utility setFrameOfView:self.view withHeightView:460.0 andYCordinate:0.0];
            else
                [Utility setFrameOfView:self.view withHeightView:415.0 andYCordinate:0.0];
        }else
            [Utility setFrameOfView:self.view withHeightView:368.0 andYCordinate:0.0];
    }
}


- (void) showPrivacyTip
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    appDelegate.isOpenTweetLink = YES;
    NSString* url = @"https://twitter.com/privacy";
    
    isLoadSuccess = YES;
    
    ShowLinkViewController *wv = [[ShowLinkViewController alloc] initWithNibName:@"ShowLinkViewController" bundle:nil];
    wv.strLinkUrl = url;
    if (isShowing) {
        self.navigationController.navigationBarHidden = NO;
    }
    
    [self.navigationController pushViewController:wv animated:YES];    
}


@end
