//
//  DetailsTweetWebViewController.h
//  AGKGlobal
//
//  Created by HpProbook on 9/4/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CPTextViewPlaceholder.h"
#import "UIInputToolbar.h"
#import "Constants.h"
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "TutorViewController.h"
#import "Toast+UIView.h"

@interface DetailsTweetWebViewController : UIViewController<UIWebViewDelegate,UIInputToolbarDelegate, UIActionSheetDelegate,MFMailComposeViewControllerDelegate, tutorDelegate>
{
    UIInputToolbar *inputToolbar;
    
@private
    BOOL keyboardIsVisible;
    
    NSString            *nameTweet;
    NSString            *screenNameTweet;
    NSString            *screenNameDisPlay;
    NSString            *dateTweet;
    NSString            *createAtTweet;
    NSString            *messageTweet;
    NSString            *imageTweet;
    NSString            *idTweet;
    NSString            *mediaURLTweet;
    NSString            *displayURLTweet;
    NSString            *expandedURLTweet;
    NSDictionary        *retweetStatusDict;
    NSString            *retweeter;
    NSString            *retweetCount;
    NSString            *profileUrlBig;
    NSString            *urlDisplayMedia;
    NSString            *urlMedia;
    NSString            *urlDisplayUrls;
    NSString            *urlUrls;
    NSString            *protectedCount;

    NSMutableArray      *arrayData;
    
    NSMutableDictionary *objDict;
    
    NSString            * postHTML;
    
    IBOutlet UIButton   *btnHideView;
    
    BOOL                isLoadSuccess;
    BOOL                isYellowStar;
    BOOL                isShowImage;
    BOOL                isShowing;
}

@property (nonatomic,strong) IBOutlet UIWebView   *tweetWebView;
@property (nonatomic,strong) IBOutlet UIView      *viewShowImage;
@property (nonatomic,strong) NSMutableDictionary  *dictionaryTweet;

@property (nonatomic,strong) TweetObj             *tweetObj;

// VARIABLES FOR HTML TEMPLATES //
@property (nonatomic,retain) NSString            *htmlString;
@property (nonatomic,retain) NSString            *_tempTweetHTMLBegin;
@property (nonatomic,retain) NSString            *_tempTweetHTMLEnd;
@property (nonatomic,retain) NSString            *_tempTweetBeginNormal;
@property (nonatomic,retain) NSString            *_tempTweetBeginRetweet;
@property (nonatomic,retain) NSString            *_tempTweetBodyPhoto;
@property (nonatomic,retain) NSString            *_tempTweetBodyStatus;
@property (nonatomic,retain) NSString            *_tempTweetBodyVideo;
@property (nonatomic,retain) NSString            *_tempTweetBodyPhotoRetweet;
@property (nonatomic,retain) NSString            *_tempTweetBodyStatusRetweet;
@property (nonatomic,retain) NSString            *_tempTweetBodyVideoRetweet;
@property (nonatomic,retain) NSString            *_tempTweetBodyPhotoRetweetFavorite;
@property (nonatomic,retain) NSString            *_tempTweetBodyStatusRetweetFavorite;
@property (nonatomic,retain) NSString            *_tempTweetBodyVideoRetweetFavorite;
@property (nonatomic,retain) NSString            *_tempSendMailTweet;
@property (nonatomic,retain) NSString            *_tempTweetEmptyBegin;

@property (nonatomic,retain) NSString            *nameTweet;
@property (nonatomic,retain) NSString            *screenNameTweet;
@property (nonatomic,retain) NSString            *screenNameDisPlay;
@property (nonatomic,retain) NSString            *dateTweet;
@property (nonatomic,retain) NSString            *createAtTweet;
@property (nonatomic,retain) NSString            *messageTweet;
@property (nonatomic,retain) NSString            *messageConvert;
@property (nonatomic,retain) NSString            *imageTweet;
@property (nonatomic,retain) NSString            *idTweet;
@property (nonatomic,retain) NSString            *idTweetStr;
@property (nonatomic,retain) NSString            *mediaURLTweet;
@property (nonatomic,retain) NSString            *displayURLTweet;
@property (nonatomic,retain) NSString            *expandedURLTweet;
@property (nonatomic,retain) NSDictionary        *retweetStatusDict;
@property (nonatomic,retain) NSString            *retweeter;
@property (nonatomic,retain) NSString            *retweetCount;
@property (nonatomic,retain) NSString            *favoriteCount;
@property (nonatomic,retain) NSString            *favorited;
@property (nonatomic,retain) NSString            *profileUrlBig;
@property (nonatomic,retain) NSString            *urlDisplayMedia;
@property (nonatomic,retain) NSString            *urlExpandedMedia;
@property (nonatomic,retain) NSString            *urlMedia;
@property (nonatomic,retain) NSString            *urlDisplayUrls;
@property (nonatomic,retain) NSString            *urlExpandedUrls;
@property (nonatomic,retain) NSString            *urlUrls;
@property (nonatomic,retain) NSString            *protectedCount;
@property (nonatomic,retain) NSString            *defaultProfile;

@property (nonatomic,retain) NSString            *iconReply;
@property (nonatomic,retain) NSString            *iconRetweet;
@property (nonatomic,retain) NSString            *iconRetweetBegin;
@property (nonatomic,retain) NSString            *iconFavorite;
@property (nonatomic,retain) NSString            *iconMore;
@property(nonatomic,assign) BOOL                 isMedia;
@property(nonatomic,assign) BOOL                 isUrls;

@property (nonatomic,assign) NSInteger           isClickFavourite;

@property (nonatomic,retain) NSMutableArray      *arrayData;
@property (nonatomic,strong) NSString            *_cssBaseString;

@property (nonatomic,strong) UIInputToolbar      *inputToolbar;

@property (nonatomic,strong) NSMutableDictionary *followNameDict;
@property (nonatomic,strong) NSMutableDictionary *followUserDict;

@property (nonatomic,strong) NSMutableDictionary *friendsNameDict;
@property (nonatomic,strong) NSMutableDictionary *friendsUserDict;

@property (nonatomic,strong) NSDictionary         *retweetDict;

@property (nonatomic,retain) IBOutlet UIButton   *btnReply;
@property (nonatomic,retain) IBOutlet UIButton   *btnRetweet;
@property (nonatomic,retain) IBOutlet UIButton   *btnFavorite;
@property (nonatomic,retain) IBOutlet UIButton   *btnMore;

//--show image
@property (nonatomic,retain) IBOutlet UILabel     *nameLabel;
@property (nonatomic,retain) IBOutlet UILabel     *dateLabel;
@property (nonatomic,retain) IBOutlet UIWebView   *wvContent;
@property (nonatomic,retain) IBOutlet UIImageView *imgAvatar;
@property (nonatomic,retain) IBOutlet UIImageView *imgTweet;

//--ACTION
- (IBAction)clickToBtnReply:(id)sender;
- (IBAction)clickToBtnRetweet:(id)sender;
- (IBAction)clickToBtnFavorite:(id)sender;
- (IBAction)clickToBtnMore:(id)sender;
- (IBAction)clickToBtnHideView:(id)sender;

//--Delegate complete
- (void)retweetCountDownComplete;
- (void)replyCountDownComplete:(NSString *)messageText;

@end
