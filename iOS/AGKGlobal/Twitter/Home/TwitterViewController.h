//
//  TwitterViewController.h
//  BeSafe
//
//  Created by The Rand's on 8/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>
#import <Twitter/TWTweetComposeViewController.h>
#import "TwitterCell.h"
#import "Constants.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "Obj.h"
#import "Toast+UIView.h"

@protocol TwitterViewControllerDelegate <NSObject>
- (void)handleURL:(NSURL*)url;
@end


@interface TwitterViewController : UIViewController <UIActionSheetDelegate,UIWebViewDelegate,UIScrollViewDelegate,UIAlertViewDelegate,UIWebViewDelegate>
{
    BOOL                    isLoadedWebHeader;
    BOOL                    isGettingAdditionalTimeLine;
    NSInteger               refreshFriendsCount;
    NSTimeInterval          lastTimelineInterval;
    NSString                *displayURL;
    NSString                *expandedURL;
    NSString                *profileUrl;
    NSString                *mediaURL;
    int                     indexCell;
    
    int                     heightCell;
    CGFloat                 heightText ;
    
    NSMutableDictionary     *objDict;
    
    UIFont                  *_font;
    UIWebView               *_aWebView;
    NSString                *htmlBody;
    NSString                *strJsForPost;
    BOOL                    loop;
    NSMutableDictionary     *tweetDict;
    BOOL                    isCallReqest;
    BOOL                    checkLoaded;
    NSMutableArray          *timeLineDataCache;
}

@property (nonatomic,assign) BOOL                       isSigned;
@property (nonatomic,assign) id<TwitterViewControllerDelegate> delegate;
@property (strong,nonatomic) IBOutlet UIWebView         *wvHeader;
@property (nonatomic,retain) NSMutableDictionary        *tweetDict;

@property (nonatomic,strong) IBOutlet UIWebView         *wvTweet;
@property (nonatomic,strong) IBOutlet UIView            *loggedOutView;
@property (nonatomic,strong) IBOutlet UIView            *noNetworkView;
@property (nonatomic,strong) IBOutlet UIView            *loadingView;
@property (nonatomic,strong) IBOutlet UIScrollView      *scrollLoadingView;
@property (nonatomic,strong) IBOutlet UIButton          *loginButton;
@property (nonatomic,strong) NSMutableDictionary        *_followNameDict;
@property (nonatomic,strong) NSMutableDictionary        *_followUserDict;
@property (nonatomic,strong) NSMutableDictionary        *_friendsNameDict;
@property (nonatomic,strong) NSMutableDictionary        *_friendsUserDict;
@property (nonatomic,strong) NSMutableArray             *_timeLineArray;
@property (nonatomic,strong) NSMutableDictionary        *_trendsAvailableDict;
@property (nonatomic,strong) NSMutableDictionary        *_idDictionary;
@property (nonatomic,strong) NSArray                    *messageArray;
@property (nonatomic,strong) NSIndexPath                *lastIndexPath;
@property (nonatomic,strong) NSString                   *_cssBaseString;
@property (nonatomic,retain) UIRefreshControl           *refreshControl;
@property (nonatomic,retain) NSString                   *displayURL;
@property (nonatomic,retain) NSString                   *expandedURL;
@property (nonatomic,retain) NSString                   *profileUrl;
@property (nonatomic,retain) NSString                   *mediaURL;
@property (nonatomic,retain) NSString                   *charConvert;

@property (nonatomic, retain) UIFont                    *font;
@property (nonatomic, retain) UIWebView                 *aWebView;

//-- Header View
@property (nonatomic, retain) IBOutlet UIView           *headerView;
@property (nonatomic, retain) IBOutlet UIButton         *btnNewTweet;
@property (nonatomic, retain) IBOutlet UIButton         *btnDirectMsg;
@property (nonatomic, retain) IBOutlet UILabel          *lblUsername;

- (IBAction)clickToBtnNewTweet:(id)sender;
- (IBAction)clickToBtnDirectMessage:(id)sender;

- (IBAction)sendUserToTwitterSettings;
- (void)newTwitterMessage;
- (void)openContactsList;
- (void)getPublicTimeLine;
- (void)twitterAccountFound;
- (void)updatePublicTimeLine;
- (void)cellClickActionSheet:(NSIndexPath *)indexPath;
- (void)quoteTweetSheet;
- (void)directMessageSheet;
- (void)reTweetMessage;
- (void)networkNotFound;
- (void)showTimeLineLoading;

- (void)getTrendsAvailable;
- (void)getTrendsCloset;

// JLJ // 
- (void)countDownComplete;
-(void)newTweetComplete;
- (NSMutableArray*) timeLineArray;
- (NSMutableDictionary*) followNameDict;
- (NSMutableDictionary*) followUserDict;
- (NSMutableDictionary*) friendsNameDict;
- (NSMutableDictionary*) friendsUserDict;
- (NSMutableDictionary*) idDictionary;
- (NSMutableDictionary*) trendsAvailableDict;
- (NSString*)cssStringForTweet:(NSString*)tweet;
- (NSString*)cssBaseString;

-(CGFloat)heightFromString:(NSString*)aString maxWidth:(CGFloat)maxWidth font:(UIFont*)aFont;

@end
