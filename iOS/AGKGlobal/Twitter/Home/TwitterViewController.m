//
//  TwitterViewController.m
//  BeSafe
//
//  Created by The Rand's on 8/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TwitterViewController.h"
#import "AppDelegate.h"
#import "TwitterContactsViewController.h"
#import "CountDownViewController.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import "DetailsTweetWebViewController.h"
#import <Social/Social.h>

#define publicTimeline                  0
#define directMessage                   1
#define MAX_REFRESH_FRIEND_COUNT        10
#define TIMELINE_REFRESH_INTERVAL       15
#define kMSTextViewFont [UIFont fontWithName:@"Helvetica" size:20]

//static char *KVOMSTextViewTextDidChange = "KVOMSTViewTextDidChange";
//static char *KVOMSTextViewFrameDidChange = "KVOMSTextViewFrameDidChange";

@interface TwitterViewController (PrivateMethods)
- (void)       parseText;
- (NSString *) linkRegex;
- (CGFloat)    fontSize;
- (NSString *) fontName;
- (NSString *) embedHTMLWithFontName:(NSString *)fontName
                                size:(CGFloat)size
                                text:(NSString *)theText;
@end

@implementation NSJSONSerialization (NSJSONSerialization_MutableBugFix)

+ (id)JSONObjectWithDataFixed:(NSData *)data options:(NSJSONReadingOptions)opt error:(NSError **)error
{
    id object = [NSJSONSerialization JSONObjectWithData:data options:opt error:error];
    
    if (opt & NSJSONReadingMutableContainers) {
        return [self JSONMutableFixObject:object];
    }
    
    return object;
}

+ (id)JSONMutableFixObject:(id)object
{
    if ([object isKindOfClass:[NSDictionary class]]) {
        // NSJSONSerialization creates an immutable container if it's empty (boo!!)
        if ([object count] == 0) {
            object = [object mutableCopy];
        }
        
        for (NSString *key in [object allKeys]) {
            [object setObject:[self JSONMutableFixObject:[object objectForKey:key]] forKey:key];
        }
    } else if ([object isKindOfClass:[NSArray class]]) {
        // NSJSONSerialization creates an immutable container if it's empty (boo!!)
        if (![object count] == 0) {
            object = [object mutableCopy];
        }
        
        for (NSUInteger i = 0; i < [object count]; ++i) {
            [object replaceObjectAtIndex:i withObject:[self JSONMutableFixObject:[object objectAtIndex:i]]];
        }
    }
    
    return object;
}

@end

@implementation TwitterViewController

@synthesize loggedOutView, loginButton, _timeLineArray, _trendsAvailableDict, _followNameDict, _followUserDict, _idDictionary, messageArray, lastIndexPath, noNetworkView, loadingView, _cssBaseString;
@synthesize refreshControl, btnNewTweet, btnDirectMsg, lblUsername, headerView,scrollLoadingView;
@synthesize displayURL, expandedURL, profileUrl, mediaURL, charConvert;
@synthesize font = _font;
@synthesize aWebView = _aWebView;
@synthesize delegate;
@synthesize _friendsNameDict, _friendsUserDict;
@synthesize tweetDict;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])
    {
        self.title = NSLocalizedString(@"Tweet", @"Tweet");
        self.navigationItem.title = @"BeSafe Tweet";
        self.tabBarItem.image = [UIImage imageNamed:@"icon_tweet_selected.png"];
        checkLoaded = NO;
        isCallReqest = NO;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(twitterAccountFound) name:@"twitterAccountFound" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(becomeActive) name:@"applicationDidBecomeActiveNotify" object:nil];
    }
    return self;
}

- (void)becomeActive
{
    isCallReqest = YES;
}

- (void)twitterAccountFound
{
    //--delay 3 seconds for loading facebook data
    //    sleep(3);
    
    NSLog(@"twitterAccountFound");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    BOOL accountFound = [appDelegate isTwitterAccountFound];
    self.isSigned = FALSE;
    if (accountFound) {
        if (APP_DEBUG) NSLog(@"TVC:twitterAccountFound:YES");
        // SHOW LOADING SCREEN //
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isCallReqest) {
                [self delayPublicTimeLine];
            }
            else {
                [self.view addSubview:scrollLoadingView];
            }
        });
        self.isSigned = YES;
        
    }
    else {
        NSLog(@"TVC:twitterAccountFound:NO");
        [self.navigationItem setRightBarButtonItem:nil animated:NO];
        [self.navigationItem setLeftBarButtonItem:nil animated:NO];
        
        self._timeLineArray = nil;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view addSubview:self.loggedOutView];
        });
        
        // REMOVE THE MESSAGES WE DO NOT NEED //
        [self.scrollLoadingView removeFromSuperview];
        [self.noNetworkView removeFromSuperview];
    }
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    tweetDict = [[NSMutableDictionary alloc] init];
    
    isLoadedWebHeader = NO;
    
    objDict = [[NSMutableDictionary alloc] init];
    
    indexCell = 0;
    
    heightText = 0;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480)
        scrollLoadingView.frame = CGRectMake(0, 0, 320, 600);
    else
        scrollLoadingView.frame = CGRectMake(0, 0, 320, 700);
    
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    __weak TwitterViewController *myself = self;
    
    if (sysVer < 6.0) {
        // setup pull-to-refresh
        [self.wvTweet.scrollView addPullToRefreshWithActionHandler:^{
            [myself handleRefresh:nil];
        }];
        
        [self.wvTweet.scrollView addPullToRefreshWithActionHandler:^{
            [myself handleRefresh:nil];
        }];
    }
    else {
        //-- add refresh control to tableview
        refreshControl = [[UIRefreshControl alloc] init];
        [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
        refreshControl.tintColor = [UIColor colorWithRed:162/256.0f green:20/256.0f blue:18/256.0f alpha:1];
        
        [self.wvTweet.scrollView addSubview:refreshControl];
    }
    
    // setup infinite scrolling
    [self.wvTweet.scrollView addInfiniteScrollingWithActionHandler:^{
        [myself insertRowAtBottom];
    }];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //-- change title color
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isSuccess){
        
        //--display notice success
        [self.view makeToast:@"Message Sent!"
                    duration:3.0
                    position:@"top"
                       title:nil];
        
        //-- reload data
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            [[self timeLineArray] removeAllObjects];
            [self delayPublicTimeLine];
        });
        
        appDelegate.isSuccess = NO;
    }
        
    appDelegate.isOpenTweetLink = NO;
    
    if (!animated && self.isSigned) {
        //REMOVE LOGIN SCREEN IF VISIBLE
        [self.loggedOutView removeFromSuperview];
        [self.noNetworkView removeFromSuperview];
        
        // CHECK IF WE HAVE CACHED INFO //
        NSInteger c1 = [self followNameDict].count;
        c1 += [self followUserDict].count;
        NSInteger c2 = [self friendsNameDict].count;
        c2 += [self friendsUserDict].count;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            if (!checkLoaded) {
                [self delayPublicTimeLine];
            }
        });
        
        if (refreshFriendsCount > MAX_REFRESH_FRIEND_COUNT)
        {
            [self performSelector:@selector(getFriendAndFollower) withObject:nil afterDelay:5];
            refreshFriendsCount = 0;
        } else {
            NSLog(@"cannot refresh friends and followers:%d",refreshFriendsCount);
        }
        refreshFriendsCount++;
    }
    
    lblUsername.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"twitterAccountName"];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



#pragma mark - Refresh Control

- (IBAction)handleRefresh:(id)sender
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [[self timeLineArray] removeAllObjects];
        [self delayPublicTimeLine];
    });
}


//-- insert row at bottom when UITableView infinite scrolling
- (void)insertRowAtBottom
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self delayAdditionalPublicTimeLine];
    });
}


- (void)showTimeLineLoading
{
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    //    [self.tableView reloadData];
}


#pragma mark - get height text in UITextView

- (CGFloat)heightFromString:(NSString*)aString maxWidth:(CGFloat)maxWidth font:(UIFont*)aFont
{
	CGSize maximumLabelSize = CGSizeMake(maxWidth,999);
	CGSize sz = [aString sizeWithFont:aFont constrainedToSize:maximumLabelSize
                        lineBreakMode:NSLineBreakByWordWrapping];
    
    return sz.height;
}


- (void)openActionSheet
{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Send a twitter message."
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"New Tweet", @"Direct Message",nil];
    appDelegate.userDict = [self followUserDict];
    appDelegate.nameDict = [self followNameDict];
    
    [actionSheet setTag:1];
    [actionSheet showInView:appDelegate.window];
}


- (void)cellClickActionSheet:(NSIndexPath *)indexPath
{
    self.lastIndexPath = indexPath;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Retweet", @"Quote Tweet", @"Direct Message", nil];
    [actionSheet setTag:2];
    [actionSheet showInView:appDelegate.window];
}


//--- asdfadsf
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch ([actionSheet tag]) {
        case 1:
            switch (buttonIndex) {
                case 0:[self newTwitterMessage];break; //PUBLIC TWEET
                case 1: [self openContactsList];break; //DIRECT MESSAGE
                default: break;
            };
            break;
        case 2:
            switch (buttonIndex) {
                case 0: [self reTweetMessage]; break; //RETWEET
                case 1: [self quoteTweetSheet]; break; //QUOTE TWEET
                case 2: [self directMessageSheet]; break; //DIRECT MESSAGE
                default: break;
            };
            break;
            
        default:
            break;
    }
    
    //    [self.tableView deselectRowAtIndexPath:self.lastIndexPath animated:YES];
}




#pragma mark - GET DATA

- (void)getFriendAndFollower
{
    NSLog(@"--getFollowers--");
    [self getFollowers];
    NSLog(@"--getFriends--");
    [self getFriends];
    NSLog(@"--getTrendsAvailable--");
    //[self getTrendsAvailable];
}


//-- get list follwers
- (void)getFollowers
{
    //https://api.twitter.com/1/followers/ids.json?cursor=-1&screen_name=twitterapi
    if (APP_DEBUG) NSLog(@"getFollowers");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/followers/list.json?cursor=-1"];
    NSDictionary *params = nil;
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:myUrl parameters:params];
    
    request.account = [appDelegate selectedTwitterAccount];
    NSLog(@"twitter perform request 1");
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if(error){
             [self networkNotFound];
             // NSLog(@"Error:%@",error);
         }else {
             NSError *jsonError = nil;
             NSDictionary *result_dict = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
             NSArray *follower_users = [result_dict objectForKey:@"users"];
             
             // thuy comment: if main thread will app very slowly
             dispatch_async(dispatch_get_main_queue(), ^{
                 if ([follower_users isKindOfClass:[NSArray class]]) {
                     for (int i=0; i<follower_users.count; i++) {
                         if (APP_DEBUG) NSLog(@"%@",result_dict);
                         NSString *screenName = [[follower_users objectAtIndex:i] objectForKey:@"screen_name"];
                         NSString *name = [[follower_users objectAtIndex:i] objectForKey:@"name"];
                         NSString *idStr = [[follower_users objectAtIndex:i] objectForKey:@"id_str"];
                         if (!screenName) screenName = @"Twitter User";
                         NSString *purl = [[follower_users objectAtIndex:i] objectForKey:@"profile_image_url"];
                         if (!purl) purl = @"https://si0.twimg.com/profile_images/1873315052/new-default-twitter-avatar.jpg";
                         
                         [[self followNameDict] setObject:idStr forKey:[NSString stringWithFormat:@"%@ (@%@)",name,screenName]];
                         NSMutableDictionary *tmp_user = [[NSMutableDictionary alloc] init];
                         [tmp_user setObject:screenName forKey:@"screen_name"];
                         [tmp_user setObject:purl forKey:@"profile_image_url"];
                         [[self followUserDict] setObject:tmp_user forKey:idStr];
                         
                         // SAVE THIS INFORMATION FOR THE FUTURE //
                         [[NSUserDefaults standardUserDefaults] setObject:[self followNameDict] forKey:@"TWITTER_FOLLOW_NAME_DICT"];
                         [[NSUserDefaults standardUserDefaults] setObject:[self followUserDict] forKey:@"TWITTER_FOLLOW_USER_DICT"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                     }
                 }
             });
         }
     }];
}


//get list following (friends)
- (void)getFriends
{
    //https://api.twitter.com/1/friends/ids.json?cursor=-1&screen_name=twitterapi
    if (APP_DEBUG) NSLog(@"getFriends");
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/friends/list.json?cursor=-1"];
    NSDictionary *params = nil;
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:myUrl parameters:params];
    
    request.account = [appDelegate selectedTwitterAccount];
    NSLog(@"twitter perform request 2");
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if(error){
             [self networkNotFound];
         }else {
             NSError *jsonError = nil;
             NSDictionary *result_dict = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
             
             NSArray *friends_users = [result_dict objectForKey:@"users"];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (APP_DEBUG) NSLog(@"%@",friends_users);
                 
                 for (int i=0; i<friends_users.count; i++) {
                     if (APP_DEBUG) NSLog(@"%@",result_dict);
                     NSString *screenName = [[friends_users objectAtIndex:i] objectForKey:@"screen_name"];
                     NSString *name = [[friends_users objectAtIndex:i] objectForKey:@"name"];
                     NSString *idStr = [[friends_users objectAtIndex:i] objectForKey:@"id_str"];
                     if (!screenName) screenName = @"Twitter User";
                     NSString *purl = [[friends_users objectAtIndex:i] objectForKey:@"profile_image_url"];
                     if (!purl) purl = @"https://si0.twimg.com/profile_images/1873315052/new-default-twitter-avatar.jpg";
                     
                     [[self friendsNameDict] setObject:idStr forKey:[NSString stringWithFormat:@"%@ (@%@)",name,screenName]];
                     NSMutableDictionary *tmp_user = [[NSMutableDictionary alloc] init];
                     [tmp_user setObject:screenName forKey:@"screen_name"];
                     [tmp_user setObject:purl forKey:@"profile_image_url"];
                     [[self friendsUserDict] setObject:tmp_user forKey:idStr];
                     
                     // SAVE THIS INFORMATION FOR THE FUTURE //
                     [[NSUserDefaults standardUserDefaults] setObject:[self friendsNameDict] forKey:@"TWITTER_FRIENDS_NAME_DICT"];
                     [[NSUserDefaults standardUserDefaults] setObject:[self friendsUserDict] forKey:@"TWITTER_FRIENDS_USER_DICT"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                 }
             });
         }
     }];
}


- (void)delayPublicTimeLine
{
    if (isGettingAdditionalTimeLine)
    {
        //-- end refresh control
        [self.refreshControl endRefreshing];
        return;
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    isGettingAdditionalTimeLine = YES;
    
    NSTimeInterval ii = TIMELINE_REFRESH_INTERVAL - ([[NSDate date] timeIntervalSinceReferenceDate]-lastTimelineInterval);
    NSLog(@"%f",ii);
    if (ii <= 0) {
        [self getUserInfo];
        [self getPublicTimeLine];
    }
    else {
        NSLog(@"watting");
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSelector:@selector(getPublicTimeLine) withObject:nil afterDelay:ii];
        });
    }
}

- (void) getUserInfo
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ACAccount *tweetAcc = [appDelegate selectedTwitterAccount];
    NSLog(@"KKKK %@",tweetAcc.username);
    NSString *urlStr =  [NSString stringWithFormat:@"https://api.twitter.com/1.1/users/show.json?screen_name=%@",tweetAcc.username];
    NSURL *myUrl = [NSURL URLWithString:urlStr];//
    NSDictionary *params = nil;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:myUrl parameters:params];
    request.account = [appDelegate selectedTwitterAccount];
    
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         
         if(error)
         {
             [self networkNotFound];
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         }
         else
         {
             NSError *jsonError = nil;
             NSDictionary *timeLineData = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                 [[NSUserDefaults standardUserDefaults] setObject:[timeLineData objectForKey:@"profile_image_url"] forKey:PROFILE_TWITTER_URL];
             });
         }
     }];
}
     
                


- (void)getPublicTimeLine
{
    isCallReqest = NO;
    
    NSLog(@"getPublicTimeLine");
    
    lastTimelineInterval = [[NSDate date] timeIntervalSinceReferenceDate];
    refreshFriendsCount++;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/home_timeline.json"];//users/lookup.json
    NSDictionary *params = nil;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    if ([self timeLineArray].count > 0)
    {
        NSLog(@"appending new tweets");
        NSDictionary *firstDict = (NSDictionary*)[[self timeLineArray] objectAtIndex:0];
        NSString *firstID = [[firstDict objectForKey:@"id"] stringValue];
        
        params = [[NSDictionary alloc] initWithObjectsAndKeys:firstID,@"since_id", nil];
    } else {
        NSLog(@"show connecting spinner");
        [self showTimeLineLoading];
    }
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:myUrl parameters:params];
    request.account = [appDelegate selectedTwitterAccount];
    
    NSLog(@"twitter perform request 4");
    
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         isGettingAdditionalTimeLine = NO;
         if(error)
         {
             [self networkNotFound];
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         }
         else
         {
             NSLog(@"received respone");
             NSError *jsonError = nil;
             NSArray *timeLineData = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                 DLOG(@"timeLineData: %@",timeLineData);
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                 NSDictionary* dict = [timeLineData isKindOfClass:[NSDictionary class]] ? (NSDictionary*)timeLineData : nil;
                 @try
                 {
                     if ([dict objectForKey:@"errors"] != nil)
                     {
                         [self.view makeToast:@"request error!"
                                     duration:3.0
                                     position:@"top"
                                        title:nil];
                         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                         
                         appDelegate.isConnecting = YES;
                         
                         NSString *codeStr = [[[[dict objectForKey:@"errors"] objectAtIndex:0] objectForKey:@"code"] stringValue];
                         if ([codeStr isEqualToString:@"215"]) {
                             appDelegate.isNetworkNotFound = YES;
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No Twitter accounts" message:@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                 [alertView show];
                             });
                         }
                         
                         return;
                     }
                 }
                 @catch (NSException *exception)
                 {
                     NSLog(@"crash");
                 }
                 @finally {
                 }
                 
                 if (!params) {
                     NSLog(@"RESPONSE:initializing timeline");
                     // NEW ARRAY OF TWITTER TIMELINE
                     if (timeLineData.count > 0) {
                         self._timeLineArray = [timeLineData mutableCopy];
                         timeLineDataCache = [[NSMutableArray alloc] initWithArray:timeLineData];
                     }
                     else {
                         self._timeLineArray = [timeLineDataCache mutableCopy];
                     }
                     
                     [self loadWebTweet];
                     if (!isLoadedWebHeader) [self loadWebHeader];
                     [self refresh:[timeLineData mutableCopy]];
                 }
                 else
                 {
                     NSLog(@"RESPONSE:appending new tweets:%d",timeLineData.count);
                     [self loadWebTweet];
                     if (!isLoadedWebHeader) [self loadWebHeader];
                     [self refresh:[timeLineDataCache mutableCopy]];
                     //                     NSInteger ii = 0;
                     //                     for (tweetDict in [timeLineData reverseObjectEnumerator]) {
                     //                         NSString *tweetID = [tweetDict objectForKey:@"id"];
                     //                         NSLog(@"%d:%@",ii,tweetID);
                     //                         if (![[self idDictionary] objectForKey:tweetID]) {
                     //                             NSLog(@"...did not exist");
                     //                             [[self timeLineArray] insertObject:tweetDict atIndex:0];
                     //                         }
                     //                     }
                 }
             });
             
             //-- end refresh control
             [self.refreshControl endRefreshing];
         }
     }];
}


- (void)delayAdditionalPublicTimeLine
{
    if (isGettingAdditionalTimeLine)
    {
        //-- end refresh control
        [self.wvTweet.scrollView.infiniteScrollingView stopAnimating];
        [self.wvTweet.scrollView.pullToRefreshView stopAnimating];
        
        return;
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    isGettingAdditionalTimeLine = YES;
    
    NSTimeInterval ii = TIMELINE_REFRESH_INTERVAL - ([[NSDate date] timeIntervalSinceReferenceDate]-lastTimelineInterval);
    NSLog(@"%f",ii);
    if (ii <= 0) {
        [self getAdditionalPublicTimeLine];
    }
    else {
        NSLog(@"watting");
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSelector:@selector(getAdditionalPublicTimeLine) withObject:nil afterDelay:ii];
        });
    }
}


- (void)getAdditionalPublicTimeLine
{
    lastTimelineInterval = [[NSDate date] timeIntervalSinceReferenceDate];
    NSLog(@"getAdditionalPublicTimeLine");
    
    refreshFriendsCount++;
    // ?? DO WE WANT TO SHOW THE SPINNING AGAIN ?? //
    //[self showTimeLineLoading];
    if (self._timeLineArray == nil || [self timeLineArray].count == 0)
    {
        [self delayPublicTimeLine];
        [self.wvTweet.scrollView.infiniteScrollingView stopAnimating];
        [self.wvTweet.scrollView.pullToRefreshView stopAnimating];
        return;
    }
    
    NSDictionary *lastDict = (NSDictionary*)[[self timeLineArray] lastObject];
    NSString *lastID = [[lastDict objectForKey:@"id"] stringValue];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/home_timeline.json"];
    NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:lastID,@"max_id", nil];
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:myUrl parameters:params];
    
    request.account = [appDelegate selectedTwitterAccount];
    NSLog(@"twitter perform request 5");
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         isGettingAdditionalTimeLine = NO;
         NSLog(@":: additional results ::");
         if(error)
         {
             [self networkNotFound];
             NSLog(@"Error:%@",error);
             [self.wvTweet.scrollView.infiniteScrollingView stopAnimating];
             [self.wvTweet.scrollView.pullToRefreshView stopAnimating];
         }
         else
         {
             NSError *jsonError = nil;
             NSArray* timeLineData = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
             
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                 NSLog(@"timeLineData; %@",timeLineData);
                 NSLog(@"[[ additional results ]]");
                 NSLog(@"orig:%d",[self timeLineArray].count);
                 NSLog(@"addl:%d",timeLineData.count);
                 [[self timeLineArray] removeLastObject];
                 // thuy fix crash ios 6
                 if (![timeLineData isKindOfClass:[NSArray class]]) {
                     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                     dispatch_async(dispatch_get_main_queue(), ^{
                         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:rateLimit delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         
                         [self.wvTweet.scrollView.infiniteScrollingView stopAnimating];
                         [self.wvTweet.scrollView.pullToRefreshView stopAnimating];
                         
                         [alertView show];
                     });
                     //                    return ;
                 }
                 else {
                     [[self timeLineArray] addObjectsFromArray:timeLineData];
#if refactorCodeTweet
                     [self addTweet:timeLineData];
#endif
                 }
                 
                 //                 for (tweetDict in [self timeLineArray]) {
                 //                     [[self idDictionary] setObject:tweetDict forKey:[[tweetDict objectForKey:@"id"] stringValue]];
                 //                 }
#if !refactorCodeTweet
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self updatePublicTimeLine];
                 });
#endif
             });
             
             //-- end refresh control
             [self.wvTweet.scrollView.infiniteScrollingView stopAnimating];
             [self.wvTweet.scrollView.pullToRefreshView stopAnimating];
         }
     }];
}


- (void)getTrendsAvailable
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSURL *myUrl = [NSURL URLWithString:@"https://api.twitter.com/1.1/trends/available.json"];
    NSDictionary *params = nil;
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:myUrl parameters:params];
    request.account = [appDelegate selectedTwitterAccount];
    NSLog(@"twitter perform request 10");
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if(error){
             [self networkNotFound];
         }else {
             NSError *jsonError = nil;
             NSDictionary *result_dict = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
             
             double delayInSeconds = 2.0;
             dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
             dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                 [[self trendsAvailableDict] setObject:[result_dict valueForKey:@"name"] forKey:@"TWITTER_TRENDS_AVAILABLE_DICT"];
                 
                 // SAVE THIS INFORMATION FOR THE FUTURE //
                 [[NSUserDefaults standardUserDefaults] setObject:[self trendsAvailableDict] forKey:@"TWITTER_TRENDS_AVAILABLE_DICT"];
                 
                 [[NSUserDefaults standardUserDefaults] synchronize];
             });
         }
     }];
}


- (void)getTrendsCloset
{
    //
}


- (void)updatePublicTimeLine
{
    //REMOVE LOADING SCREEN
    [self.scrollLoadingView removeFromSuperview];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    //SET GLOBAL TWITTER FLAG
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setIsConnectedToTwitter:YES];
    
    //Thuy: viec reload o day se khien app chay rat nang, can phai co 1 soluton khac cho viec nay
    //    [self.tableView reloadData];
    [self.navigationItem.leftBarButtonItem setEnabled:YES];
}


- (void)countDownComplete
{
    NSLog(@"retwitter countDownComplete");
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *tweetId = [[[self timeLineArray] objectAtIndex:indexCell] objectForKey:@"id_str"];
    NSString *urlString = [NSString stringWithFormat:@"https://api.twitter.com/1.1/statuses/retweet/%@.json",tweetId];
    NSURL *myUrl = [NSURL URLWithString:urlString];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:myUrl parameters:nil];
    
    request.account = [appDelegate selectedTwitterAccount];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSLog(@"twitter perform request 6");
    [request performRequestWithHandler:
     ^(NSData *responseData,NSHTTPURLResponse *urlResponse, NSError *error)
     {
         NSError *jsonError = nil;
         id timeLineData = [NSJSONSerialization JSONObjectWithDataFixed:responseData options:NSJSONReadingMutableContainers error:&jsonError];
         
         dispatch_async(dispatch_get_main_queue(), ^{
             DLOG(@" timeLineData: %@",timeLineData);
             [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             
             __weak TwitterViewController *myself = self;
             [myself handleRefresh:nil];
         });
     }];
}


-(void)newTweetComplete
{
    [self delayPublicTimeLine];
    
    NSLog(@"newTweetComplete - DONE");
}


- (void)reTweetMessage
{
    // JLJ //
    //int retwitterType = RETWITTER_TYPE; // HARD CODE FOR RETWEETS //
    NSString *retweetId = [[[self timeLineArray] objectAtIndex:self.lastIndexPath.row] objectForKey:@"id_str"];
    NSString *retweetText = [[[self timeLineArray] objectAtIndex:self.lastIndexPath.row] objectForKey:@"text"];
    NSString *retweetScreenName = [[[[self timeLineArray] objectAtIndex:self.lastIndexPath.row] objectForKey:@"user"] objectForKey:@"screen_name"];
    NSString *retweetImagePath = [self getOriginalTwitterImageURL:[[[[self timeLineArray] objectAtIndex:self.lastIndexPath.row] objectForKey:@"user"] objectForKey:@"profile_image_url"]];
    
    if (APP_DEBUG) NSLog(@"Retweet id:%@",retweetId);
    if (APP_DEBUG) NSLog(@"Retweet msg:%@",retweetText);
    
    CountDownViewController *controller = nil;
    if (isIphone5)
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController-568h" bundle:nil];
    else
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController" bundle:nil];
    
    controller.messageText = retweetText;;
    controller.recipientText = [NSString stringWithFormat:@"Retweet of %@", retweetScreenName];
    controller.retwitterDelegate = self;
    controller.abId = [NSNumber numberWithInt:-1];   // HARD CODE AS PUBLIC //
    controller.imagePath = retweetImagePath;
    controller.sendType = RETWITTER_TYPE;
    
    UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromRight;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
    [UIView setAnimationDuration:COUNTDOWN_TRANSITION];
    [UIView setAnimationDelegate:controller];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    [self presentViewController:controller animated:YES completion:nil];
    [UIView commitAnimations];
}


- (void)quoteTweetSheet
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *quotedMessage = [[[self timeLineArray] objectAtIndex:self.lastIndexPath.row] objectForKey:@"text"];
    NSString *quotedUserName = [[[[self timeLineArray] objectAtIndex:self.lastIndexPath.row] objectForKey:@"user"] objectForKey:@"screen_name"];
    NSString *quoteImagePath = [self getOriginalTwitterImageURL:[[[[self timeLineArray] objectAtIndex:self.lastIndexPath.row] objectForKey:@"user"] objectForKey:@"profile_image_url"]];
    NSString *finalMessage = quotedMessage;
    if (![quotedMessage hasPrefix:@"RT @"]) {
        finalMessage = [NSString stringWithFormat:@"RT @%@: %@",quotedUserName,quotedMessage];
    }
    
    [appDelegate displayTwitterComposerSheet:@"Public Tweet"
                                        body:finalMessage
                                        abId:[NSNumber numberWithInt:-1]
                                   imagePath:quoteImagePath
                                        view:self
                                       title:@"Quote Tweet"];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"sendDirectMsg"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)directMessageSheet
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *imagePath = [self getOriginalTwitterImageURL:[[[[self timeLineArray] objectAtIndex:self.lastIndexPath.row] objectForKey:@"user"] objectForKey:@"profile_image_url"]];
    NSString *userName = [[[[self timeLineArray] objectAtIndex:self.lastIndexPath.row] objectForKey:@"user"] objectForKey:@"screen_name"];
    [appDelegate displayTwitterComposerSheet:userName
                                        body:@""
                                        abId:[NSNumber numberWithInt:-1]
                                   imagePath:imagePath
                                        view:self
                                       title:@"Direct Message"];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"sendDirectMsg"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (void)networkNotFound
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationItem setRightBarButtonItem:nil animated:NO];
        [self.navigationItem setLeftBarButtonItem:nil animated:NO];
        [self.scrollLoadingView removeFromSuperview];
        //    [self.loggedOutView removeFromSuperview];
        [self.view addSubview:self.noNetworkView];
        appDelegate.isNetworkNotFound = YES;
        [self.wvTweet.scrollView setScrollEnabled:NO];
    });
}


- (NSMutableArray*) timeLineArray
{
    if (!self._timeLineArray) {
        self._timeLineArray = [[NSMutableArray alloc] init];
    }
    return self._timeLineArray;
}

- (NSMutableDictionary*) idDictionary
{
    if (!self._idDictionary) {
        self._idDictionary = [[NSMutableDictionary alloc] init];
    }
    return self._idDictionary;
}


- (NSString*)getBiggerTwitterImageURL:(NSString*)url
{
    NSString *result = [url stringByReplacingOccurrencesOfString:@"_normal." withString:@"."];
    
    return result;
}


- (NSString*)getOriginalTwitterImageURL:(NSString*)url
{
    NSString *result = [url stringByReplacingOccurrencesOfString:@"_normal." withString:@"."];
    
    return result;
}

- (NSString*)cssStringForTweet:(NSString*)tweet
{
    return [NSString stringWithFormat:[self cssBaseString],tweet];
}


- (NSString*)cssBaseString
{
    if (!self._cssBaseString) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"CssBaseString" ofType:@"txt"];
        NSError *error;
        self._cssBaseString = [NSString stringWithContentsOfFile:path encoding:NSASCIIStringEncoding error:&error];
        if (error) NSLog(@"CssBaseString error");
        if (APP_DEBUG) NSLog(@"%@",self._cssBaseString);
    }
    return self._cssBaseString;
}

- (NSString*)dateTimeStringForTwitterTimeToPush:(NSString*)created_at {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:usLocale];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    
    // see http://unicode.org/reports/tr35/tr35-6.html#Date_Format_Patterns
    [dateFormatter setDateFormat: @"EEE MMM dd HH:mm:ss Z yyyy"];
    
    NSDate *tweet_date = [dateFormatter dateFromString:created_at];
    NSDate *date = [NSDate date];
    
    if (APP_DEBUG) {
        NSLog(@"dateTimeStringForTwitterTime:%@",created_at);
        NSLog(@"tweet_date=%@",tweet_date);
        NSLog(@"date=%@",date);
    }
    
    NSString *result = @"date-time-stamp";
    NSTimeInterval now = [date timeIntervalSinceReferenceDate];
    NSTimeInterval then = [tweet_date timeIntervalSinceReferenceDate];
    NSTimeInterval diff = now - then;
    if (diff < 0) {
        if (APP_DEBUG) NSLog(@"posted recently");
        result = @"Posted recently";
    } else {
        if (APP_DEBUG) NSLog(@"diff 2");
        //NSDate *curentDate = [NSDate date];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSMinuteCalendarUnit|NSHourCalendarUnit fromDate:tweet_date]; // Get necessary date components
        
        NSInteger mm = [components month];
        NSInteger day = [components day];
        NSInteger year = [components year];
        NSString *yearStr = [NSString stringWithFormat:@"%d", year];
        
        NSString *yearFormat = [[NSString alloc] initWithString:yearStr];
        yearFormat = [yearFormat stringByReplacingOccurrencesOfString:@"20" withString:@""];
        
        NSInteger hh = [components hour];
        
        NSInteger minute = [components minute];
        NSString *minuteStr = nil;
        if (minute<10) {
            minuteStr = [NSString stringWithFormat:@"0%d",minute];
        }else{
            minuteStr = [NSString stringWithFormat:@"%d",minute];
        }
        
        NSString *ampm = @"AM";
        NSInteger hour = hh;
        if (hh > 12) {
            hour = hh-12;
            ampm = @"PM";
        }
        
        NSString *month = @"1";
        switch (mm) {
            case 2:
                month = @"2";
                break;
            case 3:
                month = @"3";
                break;
            case 4:
                month = @"4";
                break;
            case 5:
                month = @"5";
                break;
            case 6:
                month = @"6";
                break;
            case 7:
                month = @"7";
                break;
            case 8:
                month = @"8";
                break;
            case 9:
                month = @"9";
                break;
            case 10:
                month = @"10";
                break;
            case 11:
                month = @"11";
                break;
            case 12:
                month = @"12";
                break;
            default:
                month = [NSString stringWithFormat:@"Other:%d",mm];
                break;
        }
        result = [NSString stringWithFormat:@"%@/%d/%@, %d:%@ %@",month,day,yearFormat,hour,minuteStr,ampm];
    }
    
    return result;
}


- (NSMutableDictionary*) followNameDict
{
    if (!self._followNameDict) {
        self._followNameDict = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"TWITTER_FOLLOW_NAME_DICT"] mutableCopy];
        if (!self._followNameDict) {
            self._followNameDict = [[NSMutableDictionary alloc] init];
            refreshFriendsCount = MAX_REFRESH_FRIEND_COUNT+999;
            NSLog(@"followNameDict init");
        } else {
            NSLog(@"followNameDict loaded");
        }
    }
    
    return self._followNameDict;
}


- (NSMutableDictionary*) followUserDict
{
    if (!self._followUserDict) {
        self._followUserDict = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"TWITTER_FOLLOW_USER_DICT"] mutableCopy];
        if (!self._followUserDict) {
            self._followUserDict = [[NSMutableDictionary alloc] init];
            refreshFriendsCount = MAX_REFRESH_FRIEND_COUNT+999;
            if (APP_DEBUG) NSLog(@"followUserDict init");
        } else {
            if (APP_DEBUG) NSLog(@"followUserDict loaded");
        }
    }
    
    return self._followUserDict;
}


- (NSMutableDictionary*) friendsNameDict
{
    if (!self._friendsNameDict) {
        self._friendsNameDict = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"TWITTER_FRIENDS_NAME_DICT"] mutableCopy];
        if (!self._friendsNameDict) {
            self._friendsNameDict = [[NSMutableDictionary alloc] init];
            refreshFriendsCount = MAX_REFRESH_FRIEND_COUNT+999;
            NSLog(@"friendsNameDict init");
        } else {
            NSLog(@"friendsNameDict loaded");
        }
    }
    
    return self._friendsNameDict;
}


- (NSMutableDictionary*) friendsUserDict
{
    if (!self._friendsUserDict) {
        self._friendsUserDict = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"TWITTER_FRIENDS_USER_DICT"] mutableCopy];
        if (!self._friendsUserDict) {
            self._friendsUserDict = [[NSMutableDictionary alloc] init];
            refreshFriendsCount = MAX_REFRESH_FRIEND_COUNT+999;
            if (APP_DEBUG) NSLog(@"friendsUserDict init");
        } else {
            if (APP_DEBUG) NSLog(@"friendsUserDict loaded");
        }
    }
    
    return self._friendsUserDict;
}



- (NSMutableDictionary *)trendsAvailableDict
{
    if (!self._trendsAvailableDict) {
        self._trendsAvailableDict = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"TWITTER_TRENDS_AVAILABLE_DICT"] mutableCopy];
        if (!self._trendsAvailableDict) {
            self._trendsAvailableDict = [[NSMutableDictionary alloc] init];
            refreshFriendsCount = MAX_REFRESH_FRIEND_COUNT+999;
            if (APP_DEBUG) NSLog(@"trendsAvailableDict init");
        } else {
            if (APP_DEBUG) NSLog(@"trendsAvailableDict loaded");
        }
    }
    
    return self._trendsAvailableDict;
}



#pragma mark - ACTION

- (IBAction)clickToBtnNewTweet:(id)sender
{
    [self newTwitterMessage]; //PUBLIC TWEET
}


- (IBAction)clickToBtnDirectMessage:(id)sender
{
    [self openContactsList]; //DIRECT MESSAGE
}


- (IBAction)sendUserToTwitterSettings
{
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)newTwitterMessage
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *imageURL = [[NSUserDefaults standardUserDefaults] objectForKey:PROFILE_TWITTER_URL];
    [appDelegate displayTwitterComposerSheet:@"Public Tweet"
                                        body:@""
                                        abId:[NSNumber numberWithInt:-1]
                                   imagePath:imageURL
                                        view:self
                                       title:@"New Tweet"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_friendsNameDict forKey:@"nameDict"];
    [defaults setObject:_friendsUserDict forKey:@"userDict"];
    
    NSLog(@"afdadsfasdfa : %@",_trendsAvailableDict);
    [defaults setObject:_trendsAvailableDict forKey:@"trendsDict"];    
    [defaults setBool:NO forKey:@"sendDirectMsg"];
    [defaults synchronize];
    
    [appDelegate setIsNewTweet:YES];
    [appDelegate setIsNewReply:NO];
}


- (void)openContactsList
{
    if (APP_DEBUG) NSLog(@"openContactsList");
    
    TwitterContactsViewController *contacts = [[TwitterContactsViewController alloc] initWithNibName:@"TwitterContactsViewController" bundle:nil];
    
    contacts.userDict = [self followUserDict];
    contacts.nameDict = [self followNameDict];
    [[self navigationController] pushViewController:contacts animated:YES];
}



#pragma mark - Change text

//-- custom lable with NSAttributedstring_Class
- (UILabel *) customLableWithNSAttributedstring:(UILabel *)lbl withfristString:(NSString *) str1 withSecondString:(NSString *) str2 withFontStr1:(UIFont *)fontForStr1 withFontStr2:(UIFont *)fontForStr2
{
    NSDictionary *arialdict = [NSDictionary dictionaryWithObject: fontForStr1 forKey:NSFontAttributeName];
    NSMutableAttributedString *AattrString = [[NSMutableAttributedString alloc] initWithString:str1 attributes: arialdict];
    
    NSDictionary *veradnadict = [NSDictionary dictionaryWithObject:fontForStr2 forKey:NSFontAttributeName];
    NSMutableAttributedString *VattrString = [[NSMutableAttributedString alloc]initWithString:str2 attributes:veradnadict];
    
    [AattrString appendAttributedString:VattrString];
    
    lbl.attributedText = AattrString;
    
    return lbl;
}


// find link in msg and show link by hyperlink
- (NSString*)convertTextWithSharedLink:(NSString*)msg
{
    NSMutableCharacterSet *cset = [[NSMutableCharacterSet alloc] init];
    [cset addCharactersInString:@" "];
    [cset addCharactersInString:@"\n"];
    
    NSArray *arrString = [msg componentsSeparatedByCharactersInSet:cset];
    
    for(int i=0; i<arrString.count;i++){
        NSString *axString = [arrString objectAtIndex:i];
        // check link
        if([axString rangeOfString:@"http://"].location != NSNotFound
           || [axString rangeOfString:@"https://"].location != NSNotFound)
        {
            NSArray *arrNewString = [[arrString objectAtIndex:i]  componentsSeparatedByString:@"\""];
            
            NSString *link = [arrString objectAtIndex:i];
            BOOL checkSubLink = FALSE;
            
            // check truong hop abcdfdfdf("http://abc")
            
            for(int ii=0; ii<arrNewString.count;ii++){
                
                if([[arrNewString objectAtIndex:ii] rangeOfString:@"http://"].location != NSNotFound || [[arrString objectAtIndex:i] rangeOfString:@"https://"].location != NSNotFound)
                {
                    checkSubLink = TRUE;
                    link = [arrNewString objectAtIndex:ii];
                    NSString* newlink = [NSString stringWithFormat:@"<a href=\"http://null/clickLink/%@\" style=\"text-decoration:none;\">%@</a>",link,[objDict objectForKey:link]];
                    
                    msg = [msg stringByReplacingOccurrencesOfString:link withString:newlink];
                }
            }
            
            // end check
            if (!checkSubLink) {
                NSString* newlink = [NSString stringWithFormat:@"<a href=\"http://null/clickLink/%@\" style=\"text-decoration:none;\">%@</a>",link,[objDict objectForKey:link]];
                
                msg = [msg stringByReplacingOccurrencesOfString:link withString:newlink];
            }
        }
        
        msg = [msg stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    }
    
    return msg;
}



#pragma mark - load data HTML

- (void)reloadJs
{
    NSString *js =[NSString stringWithFormat:@"document.getElementById('data').innerHTML = '%@';",
                   strJsForPost];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *result = [self.wvTweet stringByEvaluatingJavaScriptFromString:js];
        if (([result isEqualToString:@""] || result == nil) && loop) {
            CLEAR( @"eee");
            NSLog(@"js tweet fails");
            [self performSelector:@selector(reloadJs) withObject:nil afterDelay:4];
        }
        else {
            [self.wvTweet.scrollView.infiniteScrollingView stopAnimating];
            [self.wvTweet.scrollView.pullToRefreshView stopAnimating];
            [NetworkActivity hide];
        }
    });
}

- (void)addTweet:(NSArray*)arr
{
    strJsForPost = @"";
    for (NSInteger ii = 1;ii < arr.count; ii++) {
        NSDictionary *dic = [arr objectAtIndex:ii];
        TweetObj *obj = [[TweetObj alloc] initWithDictinary:dic];
        NSLog(@"%@",obj.HTML);
        [tweetDict setObject:obj forKey:obj.idTweet];
        strJsForPost = [NSString stringWithFormat:@"%@<div id=\"datatweet\" class=\"_7k7 storyStream _2v9s\" style=\"width: 310px; height: 93; overflow-x: hidden; overflow-y: auto;\">%@</div>",strJsForPost,obj.HTML];
    }
    strJsForPost = [Utility convertTextToHTMLText:strJsForPost];
    NSString*  js =[NSString stringWithFormat:@"document.getElementById('datanew').innerHTML += '%@';",
                    strJsForPost];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *result = [self.wvTweet stringByEvaluatingJavaScriptFromString:js];
        NSLog(@"result:%@",result);
        [self.wvTweet.scrollView.infiniteScrollingView stopAnimating];
        [self.wvTweet.scrollView.pullToRefreshView stopAnimating];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [NetworkActivity hide];
        
    });
    
}

- (void)refresh:(NSArray*) arr
{
    //    strJsForPost = @"";
    //    for (NSDictionary* dic in arr) {
    //        TweetObj *obj = [[TweetObj alloc] initWithDictinary:dic];
    //        NSLog(@"%@",obj.HTML);
    //        [tweetDict setObject:obj forKey:obj.idTweet];
    //        strJsForPost = [NSString stringWithFormat:@"%@<div id=\"datatweet\" class=\"_7k7 storyStream _2v9s\" style=\"width: 310px; height: 93; overflow-x: hidden; overflow-y: auto;\">%@</div>",strJsForPost,obj.HTML];
    //    }
    //    strJsForPost = [Utility convertTextToHTMLText:strJsForPost];
    //    NSString*  js =[NSString stringWithFormat:@"document.getElementById('data').innerHTML = '%@';",
    //                    strJsForPost];
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //    NSString *result = [self.wvTweet stringByEvaluatingJavaScriptFromString:js];
    //    if ([result isEqualToString:@""] || result == nil) {
    //        CLEAR(@"eee");
    //        NSLog(@"js tweet fails");
    //        loop = YES;
    //        [self performSelector:@selector(reloadJs) withObject:nil afterDelay:2];
    //    }
    //    else {
    //        [NetworkActivity hide];
    //    }
    //    });
    
    // thuy refix
    
    NSString *html = @"";
    html = [NSString stringWithFormat:@"%@%@",html,[Template templateHTMLBegin]];
    html = [html stringByAppendingString:@"<div id=\"datanew\">"];
    
    for (NSDictionary* dic in arr) {
        TweetObj *obj = [[TweetObj alloc] initWithDictinary:dic];
        NSLog(@"%@",obj.HTML);
        [tweetDict setObject:obj forKey:obj.idTweet];
        html = [NSString stringWithFormat:@"%@<div id=\"datatweet\" class=\"_7k7 storyStream _2v9s\" style=\"width: 310px; height: 93; overflow-x: hidden; overflow-y: auto;\">%@</div>",html,obj.HTML];
    }
    html = [html stringByAppendingString:@"</div>"];
    html = [html stringByAppendingString:[Template templateHTMLEnd]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.wvTweet loadHTMLString:html baseURL:nil];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    });
    
}

- (void)loadWebTweet
{
    NSString *html = @"";
    html = [NSString stringWithFormat:@"%@%@",html,[Template templateHTMLBegin]];
    html = [html stringByAppendingString:@"<div id=\"datatweet\" class=\"_7k7 storyStream _2v9s\" style=\"width: 310px; height: 93%; overflow-x: hidden; overflow-y: auto;\">"];
    html = [html stringByAppendingString:@"</div>"];
    html = [html stringByAppendingString:[Template templateHTMLEnd]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.wvTweet loadHTMLString:html baseURL:nil];
    });
}

- (void)loadWebHeader
{
    NSString *html = @"";
    html = [NSString stringWithFormat:@"%@%@",html,[Template templateHTMLBegin]];
    html = [html stringByAppendingString:[self getHeaderBar]];
    html = [html stringByAppendingString:[Template templateHTMLEnd]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.wvHeader loadHTMLString:html baseURL:nil];
        self.wvHeader.scrollView.scrollEnabled = NO;
    });
}

- (NSString*)getHeaderBar
{
    NSString *abc = [NSString stringWithFormat:@"<div style=\"height: 35px; margin-top: 7px;\"><div style=\"-webkit-border-radius: 7px 7px 0 0; -moz-border-radius: 7px 7px 0 0; border-radius: 7px 7px 0 0; padding: 9px 7px; background-color: #757575; margin: 0 9px 11px; padding-top: 7px; color: #FFF;\"><strong>%@</strong><div style=\"float: right; margin-right: 5px; margin-top: -2px;\"><a href=\"http://null/Dirrect/\"><img style=\"width: 30px;\" src=\"%@\"/></a>&nbsp;<a href=\"http://null/newTweet/\"><img style=\"width: 30px;\" src=\"%@\"/></a></div></div></div>",
                     [[NSUserDefaults standardUserDefaults] objectForKey:@"twitterAccountName"],
                     [Template getIconDirrectTweet],
                     [Template getIconNewTweet]];
    return abc;
}

- (void)gotoDetailWithID:(NSString*)tweetID
{
    DetailsTweetWebViewController *details = nil;
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480) {
        details = [[DetailsTweetWebViewController alloc] initWithNibName:@"DetailsTweetWebViewController" bundle:nil];
    }else{
        details = [[DetailsTweetWebViewController alloc] initWithNibName:@"DetailsTweetWebViewController-568h" bundle:nil];
    }
    if (tweetID == nil || [tweetID isEqualToString:@""]) {
        NSLog(@"tweetID nil");
        return;
    }
    
    TweetObj *twObj = [tweetDict objectForKey:tweetID];;
    details.retweetDict = twObj.retweeted_status;
    details.retweetStatusDict = twObj.retweeted_status;
    details.retweeter = twObj.retweeter;
    details.tweetObj = twObj;
    
    details.nameTweet       = twObj.nameStr;
    
    //--screennameTweet
    details.screenNameTweet = twObj.screenNameStr;
    
    //--dateTweet
    //    NSString *dateToPush    = [self dateTimeStringForTwitterTimeToPush:created_at];
    details.createAtTweet   = twObj.created_at;
    details.dateTweet       = twObj.dateConverted;
    
    //--messsageTweet
    details.messageTweet    = twObj.webString;
    
    //imageTweet
    details.imageTweet      = twObj.profileUrl;
    
    //--idTweet
    details.idTweetStr      = twObj.id_str;
    details.idTweet         = twObj.idTweet;
    
    //mediaURl
    details.mediaURLTweet   = twObj.urlMedia;
    
    //--displayUrl
    details.displayURLTweet = twObj.displayURL;
    
    //--expandedUrl
    details.expandedURLTweet= twObj.urlExpandedUrls;
    
    //--push url link to DetailsTweetVC
    details.urlDisplayMedia = twObj.urlDisplayMedia;
    details.urlDisplayUrls  = twObj.urlDisplayUrls;
    details.urlMedia        = twObj.urlMedia;
    details.urlUrls         = twObj.urlUrls;
    details.urlExpandedMedia= twObj.urlExpandedMedia;
    details.urlExpandedUrls = twObj.urlExpandedUrls;
    details.isUrls          = twObj.isUrls;
    details.isMedia         = twObj.isMedia;
    NSLog(@"%@\n%@\n%@\n%@\n%@\n%@\n",twObj.urlDisplayMedia,twObj.urlDisplayUrls,twObj.urlMedia,twObj.urlUrls,twObj.urlExpandedMedia,twObj.urlExpandedUrls);
    
    //--retweet_count
    details.retweetCount    = [NSString stringWithFormat:@"%d",twObj.retweet_count];
    
    //--favorite_count
    details.favoriteCount   = [NSString stringWithFormat:@"%d",twObj.favorite_count];
    
    //--favorited
    details.favorited       =  [NSString stringWithFormat:@"%d",twObj.favorited];
    
    //protected_count
    details.protectedCount  = twObj.defaultProfileStr;
    
    //--default profile
    details.defaultProfile  = twObj.defaultProfileStr;
    
    //--push data
    details.followNameDict  = _followNameDict;
    details.followUserDict  = _followUserDict;
    details.friendsNameDict = _friendsNameDict;
    details.friendsUserDict = _friendsUserDict;
    [self.navigationController pushViewController:details animated:YES];
    
}

- (void)gotoShowLink:(NSString*)url
{
    ShowLinkViewController *slVC = [[ShowLinkViewController alloc] initWithNibName:@"ShowLinkViewController" bundle:nil];
    slVC.strLinkUrl = url;
    [self.navigationController pushViewController:slVC animated:YES];
}

#pragma mark - UIWebViewDelegate Methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *urlString = request.URL.absoluteString;
    DLOG(@"%@",urlString);
    
    NSRange clickLinkRange = [urlString rangeOfString:@"http://null/clickLink/"];
    NSRange clickRowRange  = [urlString rangeOfString:@"http://null/clickRow/"];
    NSRange clickdirrectRange  = [urlString rangeOfString:@"http://null/Dirrect/"];
    NSRange clicknewTweetRange  = [urlString rangeOfString:@"http://null/newTweet/"];
    
    BOOL isContinue = YES;
    
    if (clickLinkRange.location != NSNotFound && isContinue)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.isOpenTweetLink = YES;
        NSLog(@"clickLinkRange");
        isContinue = NO;
         NSString* urlString1 = [urlString substringFromIndex:22];
        [self gotoShowLink:urlString1];
        return NO;
    }

    
    if (clickRowRange.location != NSNotFound && isContinue)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (appDelegate.isOpenTweetLink) {
            return NO;
        }
        NSLog(@"clickRowRange");
        isContinue = NO;
        NSString* tweetID = [urlString substringFromIndex:21];
        NSLog(@"tweetID;%@",tweetID);
        [self gotoDetailWithID:tweetID];
        return NO;
    }
    
    if (clickdirrectRange.location != NSNotFound && isContinue)
    {
        isContinue = NO;
        NSLog(@"clickdirrectRange");
        [self openContactsList];
        return NO;
    }
    if (clicknewTweetRange.location != NSNotFound && isContinue) {
        isContinue = NO;
        NSLog(@"clicknewTweetRange");
        [self newTwitterMessage];
        return NO;
    }
    
    return isContinue;
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidStartLoad");
    [self.scrollLoadingView removeFromSuperview];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if ([webView isEqual:self.wvHeader]) {
        isLoadedWebHeader = YES;
    }
    else
    {
        checkLoaded = YES;
    }
    NSLog(@"webViewDidFinishLoad");
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    // optimize speed scroll uiwebview content bug #87.
    webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"didFailLoadWithError");
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}



@end
