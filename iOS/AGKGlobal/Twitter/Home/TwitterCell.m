//
//  TwitterCell.m
//  BeSafe
//
//  Created by Jeff Jolley on 2/26/13.
//
//

#import "TwitterCell.h"

@implementation TwitterCell

@synthesize nameLabel,dateLabel,messageLabel,twitterImage,txvContent,tasksImage,viewParent, viewChild, lblRetweeted;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
