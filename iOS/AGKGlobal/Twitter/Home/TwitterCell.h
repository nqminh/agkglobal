//
//  TwitterCell.h
//  BeSafe
//
//  Created by Jeff Jolley on 2/26/13.
//
//

#import <UIKit/UIKit.h>

@interface TwitterCell : UITableViewCell<UITextViewDelegate,UIWebViewDelegate>
{
    
}

@property (nonatomic,retain) IBOutlet UIView                *viewParent;
@property (nonatomic,retain) IBOutlet UIView                *viewChild;
@property (nonatomic,retain) IBOutlet UILabel               *nameLabel;
@property (nonatomic,retain) IBOutlet UILabel               *dateLabel;
@property (nonatomic,retain) IBOutlet UIWebView             *messageLabel;
@property (nonatomic,retain) IBOutlet UITextView            *txvContent;
@property (nonatomic,retain) IBOutlet UIImageView           *twitterImage;
@property (nonatomic,retain) IBOutlet UIImageView           *tasksImage;
@property (nonatomic,retain) IBOutlet UILabel               *lblRetweeted;

@end
