//
//  VMUsers.h
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 10/11/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "VMObject.h"

@interface VMUsers : VMObject

@property (nonatomic, retain) NSString *userId;
@property (nonatomic, retain) NSString *userAvatar;
@property (nonatomic, retain) NSString *username;
@property (nonatomic, retain) NSString *fullname;
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, retain) NSString *authData;
@property (nonatomic, retain) NSString *sessionToken;

@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *password;
@property (nonatomic, retain) NSString *zipcode;
@property (nonatomic, retain) NSString *birthday;
@property (nonatomic, retain) NSString *gender;
@property (nonatomic, retain) NSString *userId_login;
@property (nonatomic, retain) NSMutableArray *arrbuddys;

/**
 * Singleton Pattern VMUsers
 * @result The current VMUsers
 */
+ (VMUsers*)currentUser;

/**
 * ReadData of VMUsers
 * @result The current VMUsers
 */
+ (VMUsers *)readData;

/**
 * Save a VMUsers
 * @param object The VMUsers need to save
 */
+ (void)saveUser:(VMUsers *)object;

/**
 * Delete current VMUsers
 */
+ (void)cleanUser;

/**
 * Check login status
 * @result YES / NO
 */
+ (BOOL)isLogin;

/**
 * Logout Besafe account
 */
+ (void)logOut;

+ (void)currentUserCopy:(VMUsers*)user;

@end
