//
//  BesafeAPI.m
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 9/20/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "BesafeAPI.h"

#define BESAFE_API_HANDLE_ERROR(code,msg)\
if (error != nil) {\
if (msg != nil) *error = [BesafeAPI errorWithCode:code description:msg];\
else *error = [BesafeAPI errorWithCode:code description:@"An exception error"];\
}

// Error Description

#define BESAFE_ERROR_MSG_NETWORK         (@"Unable to connect to the network please check your network connection and try again")
#define BESAFE_ERROR_MSG_DATA            (@"Data from the server returns incorrectly!")
#define BESAFE_ERROR_MSG_UNKNOW          (@"An error occurred unspecified!")

@implementation BesafeAPI

+ (NSError*)errorWithCode:(NSInteger)code description:(NSString*)description
{
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:description
                                                         forKey:NSLocalizedDescriptionKey];
    return [NSError errorWithDomain:BESAFE_ERROR_DOMAIN
                               code:code userInfo:userInfo];
}


+ (NSDictionary *)updateContact:(NSString *)fullname age:(NSString *)age sex:(NSString *)sex email:(NSString *)email phone:(NSString *)phone facebookId:(NSString *)facebookId error:(NSError **)error
{
    NSString *fullName        =  [fullname stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *ageStr          =  [age stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];    
    NSString *sexStr          =  [sex stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *emailStr        =  [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneStr        =  [phone stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *facebookIdStr   =  [facebookId stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=updateContact",BESAFE_HOST];
    NSURL *url = [NSURL URLWithString:urlString];
    //
    
    NSString *bodyString = [NSString stringWithFormat:@"full_name=%@&age=%@&sex=%@&email=%@&phone=%@&facebook_id=%@",fullName,ageStr,sexStr,emailStr,phoneStr,facebookIdStr];
    NSLog(@"%@", bodyString);
    NSData *postBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setHTTPMethod:@"POST"];
    [theRequest addValue:[NSString stringWithFormat:@"%d",[postBody length]] forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPBody:postBody];
    [theRequest addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    
    responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return NO;
    }
    else {
        [responseData writeToFile:@"/BESAFE/updateContact.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc] initWithBytes:[responseData bytes]
                                                             length:[responseData length]
                                                           encoding:NSUTF8StringEncoding] autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            
            if (dictResponse == NO) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA, BESAFE_ERROR_MSG_DATA);
                NSLog(@"test_error data");
                return nil;
            }
            else {
                if ([[dictResponse objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
//                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
//                                               [[dictResponse objectForKey:@"msg"] objectAtIndex:0] );
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"] );
                    return nil;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW, BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;
}

+ (NSDictionary *)signinAPI:(NSString *)email password:(NSString *)password error:(NSError **)error
{
    NSString *email_Str       =  [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *password_Str    =  [password stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=login",BESAFE_HOST];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *a = [[NSUserDefaults standardUserDefaults] valueForKey:TOKEN_DEVICE];
    NSString *bodyString = [NSString stringWithFormat:@"email=%@&password=%@&os_type=%@&device_token=%@",email_Str,password_Str,@"ios",a];
    NSLog(@"%@", bodyString);
    NSData *postBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setHTTPMethod:@"POST"];
    [theRequest addValue:[NSString stringWithFormat:@"%d",[postBody length]] forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPBody:postBody];
    [theRequest addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    
    responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return NO;
    }
    else {
        [responseData writeToFile:@"/BESAFE/signinAPI.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc] initWithBytes:[responseData bytes]
                                                             length:[responseData length]
                                                           encoding:NSUTF8StringEncoding] autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            
            if (dictResponse == NO) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA, BESAFE_ERROR_MSG_DATA);
                NSLog(@"test_error data");
                return nil;
            }
            else {
                if ([[dictResponse objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"] );
                    return nil;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW, BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;
}


+ (NSDictionary *)registerAPI:(NSString *)email full_name:(NSString *)full_name password:(NSString *)password zipcode:(NSString *)zipcode birthyear:(NSString *)birthyear gender:(NSString *)gender error:(NSError **)error
{
    NSString *email_Str       =  [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *fullnameStr       =  [full_name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSString *password_Str    =  [password stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *zipcode_Str     =  [zipcode stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *birthyear_Str   =  [birthyear stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *gender_Str      =  [gender stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=registerUser",BESAFE_HOST];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *bodyString = [NSString stringWithFormat:@"full_name=%@&email=%@&password=%@&zip_code=%@&birthday=%@&sex=%@",fullnameStr,email_Str,password_Str,zipcode_Str,birthyear_Str,gender_Str];
    NSLog(@"%@", bodyString);
    NSData *postBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setHTTPMethod:@"POST"];
    [theRequest addValue:[NSString stringWithFormat:@"%d",[postBody length]] forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPBody:postBody];
    [theRequest addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    
    responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return NO;
    }
    else {
        [responseData writeToFile:@"/BESAFE/registerAPI.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc] initWithBytes:[responseData bytes]
                                                             length:[responseData length]
                                                           encoding:NSUTF8StringEncoding] autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            
            if (dictResponse == NO) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA, BESAFE_ERROR_MSG_DATA);
                NSLog(@"test_error data");
                return nil;
            }
            else {
                if ([[dictResponse objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"]);
                    return nil;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW, BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;
}


+ (NSDictionary *)getContact:facebookId error:(NSError **)error
{
    NSString *facebookIdStr   =  [facebookId stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];    
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=getContact",BESAFE_HOST];
    NSString *bodyString = [NSString stringWithFormat:@"facebook_id=%@",facebookIdStr];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&%@",urlString,bodyString]];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [theRequest setHTTPMethod:@"GET"];

    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    responseData = [NSURLConnection sendSynchronousRequest:theRequest  returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return nil;
    }
    else {
        [responseData writeToFile:@"BESAFE/getContacts.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc ] initWithBytes:[responseData bytes]
                                                              length:[responseData length]
                                                            encoding:NSUTF8StringEncoding ]autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            if (!dictResponse) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA,
                                           BESAFE_ERROR_MSG_DATA);
                
                return nil;
            }
            else {
                if ([[dictResponse  objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                               [dictResponse objectForKey:@"msg"]);
                    NSLog(@"test_error server");
                    return nil;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW,
                               BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;
}


+ (NSDictionary *)forgotPassAPI:(NSString *)email  error:(NSError **)error
{
    NSString *email_Str       =  [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];   
    
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=forgotPassword",BESAFE_HOST];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *bodyString = [NSString stringWithFormat:@"email=%@",email_Str];
    DLOG(@"%@", bodyString);
    NSData *postBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setHTTPMethod:@"POST"];
    [theRequest addValue:[NSString stringWithFormat:@"%d",[postBody length]] forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPBody:postBody];
    [theRequest addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    
    responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return NO;
    }
    else {
        [responseData writeToFile:@"/BESAFE/forgotAPI.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc] initWithBytes:[responseData bytes]
                                                             length:[responseData length]
                                                           encoding:NSUTF8StringEncoding] autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            
            if (dictResponse == NO) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA, BESAFE_ERROR_MSG_DATA);
                NSLog(@"test_error data");
                return nil;
            }
            else {
                if ([[dictResponse objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"] );
                    return dictResponse;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW, BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;

}

+ (NSDictionary *)getListUserRegistedBesafe:(NSString*)typeAcc withFriend:(NSString*)friend_list error:(NSError**)error
{
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=checkAccount",BESAFE_HOST];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *bodyString = [NSString stringWithFormat:@"social_type=%@&friend_list=%@",typeAcc,friend_list];
    DLOG(@"%@", bodyString);
    NSData *postBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setHTTPMethod:@"POST"];
    [theRequest addValue:[NSString stringWithFormat:@"%d",[postBody length]] forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPBody:postBody];
    [theRequest addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    
    responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return NO;
    }
    else {
        [responseData writeToFile:@"/BESAFE/checkAccountAPI.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc] initWithBytes:[responseData bytes]
                                                             length:[responseData length]
                                                           encoding:NSUTF8StringEncoding] autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            
            if (dictResponse == NO) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA, BESAFE_ERROR_MSG_DATA);
                NSLog(@"test_error data");
                return nil;
            }
            else {
                if ([[dictResponse objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {                    
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"] );
                    return dictResponse;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW, BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;

}
#pragma mark - Update,Remove account to buddy

+ (NSDictionary *) updateAccountToBuddy:(NSString*)typeAcc userID:(NSString*)user_id buddyID:(NSString*)buddy_id error:(NSError**)error 
{
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=updateBuddy",BESAFE_HOST];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *bodyString = [NSString stringWithFormat:@"social_type=%@&user_id=%@&buddy_id=%@",typeAcc,user_id,buddy_id];
    DLOG(@"%@", bodyString);
    NSData *postBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setHTTPMethod:@"POST"];
    [theRequest addValue:[NSString stringWithFormat:@"%d",[postBody length]] forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPBody:postBody];
    [theRequest addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    
    responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return NO;
    }
    else {
        [responseData writeToFile:@"/BESAFE/updateBuddyAPI.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc] initWithBytes:[responseData bytes]
                                                             length:[responseData length]
                                                           encoding:NSUTF8StringEncoding] autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            
            if (dictResponse == NO) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA, BESAFE_ERROR_MSG_DATA);
                NSLog(@"test_error data");
                return nil;
            }
            else {
                if ([[dictResponse objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"] );
                    return dictResponse;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW, BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;

}


+ (NSDictionary *) removeAccountBuddy:(NSString*)typeAcc userID:(NSString*)user_id buddyID:(NSString*)buddy_id error:(NSError**)error
{
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=removeBuddy",BESAFE_HOST];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *bodyString = [NSString stringWithFormat:@"social_type=%@&user_id=%@&buddy_id=%@",typeAcc,user_id,buddy_id];
    DLOG(@"%@", bodyString);
    NSData *postBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setHTTPMethod:@"POST"];
    [theRequest addValue:[NSString stringWithFormat:@"%d",[postBody length]] forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPBody:postBody];
    [theRequest addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    
    responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return NO;
    }
    else {
        [responseData writeToFile:@"/BESAFE/updateBuddyAPI.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc] initWithBytes:[responseData bytes]
                                                             length:[responseData length]
                                                           encoding:NSUTF8StringEncoding] autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            
            if (dictResponse == NO) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA, BESAFE_ERROR_MSG_DATA);
                NSLog(@"test_error data");
                return nil;
            }
            else {
                if ([[dictResponse objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"] );
                    return dictResponse;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW, BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;
}

#pragma mark - Post data buddy

+ (NSDictionary *)postDataToPendingBuddy:(NSString*)typeAcc userID:(NSString*)user_id post_id:(NSString*)post_id data:(NSString*)dict error:(NSError**)error
{
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=postData",BESAFE_HOST];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *bodyString = [NSString stringWithFormat:@"social_type=%@&user_id=%@&post_id=%@&data=%@&media_file=%@",typeAcc,user_id,@"0",dict,@""];
    DLOG(@"%@", bodyString);
    NSData *postBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setHTTPMethod:@"POST"];
    [theRequest addValue:[NSString stringWithFormat:@"%d",[postBody length]] forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPBody:postBody];
    [theRequest addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    
    responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return NO;
    }
    else {
        [responseData writeToFile:@"/BESAFE/postDataAPI.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc] initWithBytes:[responseData bytes]
                                                             length:[responseData length]
                                                           encoding:NSUTF8StringEncoding] autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            
            if (dictResponse == NO) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA, BESAFE_ERROR_MSG_DATA);
                NSLog(@"test_error data");
                return nil;
            }
            else {
                if ([[dictResponse objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"] );
                    return dictResponse;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW, BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;

}

+ (NSDictionary*)getPostPendingUploaderId:(NSString*)user_id page:(NSString*)page_index error:(NSError**)error
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=getPostOfUploader",BESAFE_HOST];
    NSString *bodyString = [NSString stringWithFormat:@"user_id=%@&page_index=%@",user_id,page_index];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&%@",urlString,bodyString]];
    NSLog(@"bodyString %@",bodyString);
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [theRequest setHTTPMethod:@"GET"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    responseData = [NSURLConnection sendSynchronousRequest:theRequest  returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return nil;
    }
    else {
        [responseData writeToFile:@"BESAFE/getPostUploader.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc ] initWithBytes:[responseData bytes]
                                                              length:[responseData length]
                                                            encoding:NSUTF8StringEncoding ]autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            if (!dictResponse) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA,
                                        BESAFE_ERROR_MSG_DATA);
                
                return nil;
            }
            else {
                if ([[dictResponse  objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"]);
                    NSLog(@"test_error server");
                    return nil;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW,
                            BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;

}


+ (NSDictionary*)getPostPendingUploaderId:(NSString*)user_id error:(NSError**)error
{
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=getPostOfUploader",BESAFE_HOST];
    NSString *bodyString = [NSString stringWithFormat:@"user_id=%@",user_id];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&%@",urlString,bodyString]];
    NSLog(@"bodyString %@",bodyString);
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [theRequest setHTTPMethod:@"GET"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    responseData = [NSURLConnection sendSynchronousRequest:theRequest  returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return nil;
    }
    else {
        [responseData writeToFile:@"BESAFE/getPostUploader.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc ] initWithBytes:[responseData bytes]
                                                              length:[responseData length]
                                                            encoding:NSUTF8StringEncoding ]autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            if (!dictResponse) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA,
                                        BESAFE_ERROR_MSG_DATA);
                
                return nil;
            }
            else {
                if ([[dictResponse  objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"]);
                    NSLog(@"test_error server");
                    return nil;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW,
                            BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;
}

#pragma mark - Get uploader

+ (NSDictionary*)getListUploaderOfBuddy:(NSString*)buddy_id error:(NSError**)error
{
    if ([buddy_id length]==0)
    {
        buddy_id = @"";
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=getListUploaderOfBuddy",BESAFE_HOST];
    NSString *bodyString = [NSString stringWithFormat:@"buddy_id=%@",buddy_id];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&%@",urlString,bodyString]];
    DLOG(@"URL REQUEST %@",url);
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [theRequest setHTTPMethod:@"GET"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    responseData = [NSURLConnection sendSynchronousRequest:theRequest  returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return nil;
    }
    else {
        [responseData writeToFile:@"BESAFE/getListUploaderOfBuddy.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc ] initWithBytes:[responseData bytes]
                                                              length:[responseData length]
                                                            encoding:NSUTF8StringEncoding ]autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            if (!dictResponse) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA,
                                        BESAFE_ERROR_MSG_DATA);
                
                return nil;
            }
            else {
                if ([[dictResponse  objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"]);
                    NSLog(@"test_error server");
                    return nil;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW,
                            BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;

}


+ (NSDictionary*) getListPostForBuddyConfirmID:(NSString*) buddy_id userID:(NSString*) user_id page_index:(NSString*)page_index error:(NSError**)error
{
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=getPostOfBuddy",BESAFE_HOST];
    NSString *bodyString = [NSString stringWithFormat:@"buddy_id=%@&user_id=%@&page_index=%@",buddy_id,user_id,page_index];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&%@",urlString,bodyString]];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [theRequest setHTTPMethod:@"GET"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    responseData = [NSURLConnection sendSynchronousRequest:theRequest  returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return nil;
    }
    else {
        [responseData writeToFile:@"BESAFE/getListPostOfBuddy.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc ] initWithBytes:[responseData bytes]
                                                              length:[responseData length]
                                                            encoding:NSUTF8StringEncoding ]autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            if (!dictResponse) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA,
                                        BESAFE_ERROR_MSG_DATA);
                
                return nil;
            }
            else {
                if ([[dictResponse  objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"]);
                    NSLog(@"test_error server");
                    return nil;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW,
                            BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;

}


+ (NSDictionary*) updatePostbyBuddy:(NSString*) buddy_id idPost:(NSString*) id_post status:(NSString*)status error:(NSError**)error
{
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=updateAccept",BESAFE_HOST];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *bodyString = [NSString stringWithFormat:@"buddy_id=%@&id=%@&status=%@",buddy_id,id_post,status];
    DLOG(@"bodyString %@", bodyString);
    NSData *postBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setHTTPMethod:@"POST"];
    [theRequest addValue:[NSString stringWithFormat:@"%d",[postBody length]] forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPBody:postBody];
    [theRequest addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    
    responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return NO;
    }
    else {
        [responseData writeToFile:@"/BESAFE/updatePost.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc] initWithBytes:[responseData bytes]
                                                             length:[responseData length]
                                                           encoding:NSUTF8StringEncoding] autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            
            if (dictResponse == NO) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA, BESAFE_ERROR_MSG_DATA);
                NSLog(@"test_error data");
                return nil;
            }
            else {
                if ([[dictResponse objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"] );
                    return dictResponse;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW, BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;

}

#pragma mark - Get detail post uploader 

+ (NSDictionary*) getDetailPostByID:(NSString*)post_id userID:(NSString*)user_id error:(NSError**)error
{
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=getPostById",BESAFE_HOST];
    NSString *bodyString = [NSString stringWithFormat:@"post_id=%@&user_id=%@",post_id,user_id];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@&%@",urlString,bodyString]];
    DLOG(@"BODY STRING %@",url);
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [theRequest setHTTPMethod:@"GET"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    responseData = [NSURLConnection sendSynchronousRequest:theRequest  returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return nil;
    }
    else {
        [responseData writeToFile:@"BESAFE/getDetailPostByID.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc ] initWithBytes:[responseData bytes]
                                                              length:[responseData length]
                                                            encoding:NSUTF8StringEncoding ]autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            if (!dictResponse) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA,
                                        BESAFE_ERROR_MSG_DATA);
                
                return nil;
            }
            else {
                if ([[dictResponse  objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"]);
                    NSLog(@"test_error server");
                    return nil;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW,
                            BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;

}

#pragma mark - Post Photo album

+ (NSDictionary*) postPhotoAlbum:(UIImage*)image account_type:(NSString*)typeAcc userID:(NSString*)user_id  data:(NSString*)json_string error:(NSError**)error
{        
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=postData",BESAFE_HOST];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *BOUNDRY = [[NSProcessInfo processInfo] globallyUniqueString];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", BOUNDRY]forHTTPHeaderField:@"Content-Type"];
	
    NSData *data = UIImageJPEGRepresentation(image, 0.5);
    
    NSMutableData *body = [NSMutableData dataWithCapacity:[data length] + 512];
    
    // user_id
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BOUNDRY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"user_id\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[user_id dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // social_type
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BOUNDRY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"social_type\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[typeAcc dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // name
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BOUNDRY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"data\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[json_string dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // image
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BOUNDRY] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"media_file\"; filename=\"steven.png\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:data]];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [theRequest setHTTPBody:body];
    
    // close form
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BOUNDRY] dataUsingEncoding:NSUTF8StringEncoding]];    
    
    // set request body
    //DLOG(@"upload file anh %@",theRequest);
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    
    responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&aError];
    
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return NO;
    }
    else {
        [responseData writeToFile:@"/BESAFE/postDataAPI.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc] initWithBytes:[responseData bytes]
                                                             length:[responseData length]
                                                           encoding:NSUTF8StringEncoding] autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            
            if (dictResponse == NO) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA, BESAFE_ERROR_MSG_DATA);
                NSLog(@"test_error data");
                return nil;
            }
            else {
                if ([[dictResponse objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"] );
                    return dictResponse;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW, BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;
    
}


#pragma mark - changeStatusPostByUploader
+ (NSDictionary*) changeStatusPostByUploader:(NSString*)post_id userID:(NSString*)user_id status:(NSString*)status  error:(NSError**)error
{
    NSString *urlString = [NSString stringWithFormat:@"%@api.php?m=changeStatusPostByUploader",BESAFE_HOST];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSString *bodyString = [NSString stringWithFormat:@"user_id=%@&id=%@&status=%@",user_id,post_id,status];
    NSLog(@"bodyString %@", bodyString);
    NSData *postBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setHTTPMethod:@"POST"];
    [theRequest addValue:[NSString stringWithFormat:@"%d",[postBody length]] forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPBody:postBody];
    [theRequest addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response = nil;
    NSError *aError = nil;
    NSData *responseData = nil;
    
    responseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&aError];
    if (aError != nil || response == nil) {
        BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_NETWORK, BESAFE_ERROR_MSG_NETWORK);
        NSLog(@"test_error network");
        return NO;
    }
    else {
        [responseData writeToFile:@"/BESAFE/updatePost.txt" atomically:YES];
        NSString *responseString = [[[NSString alloc] initWithBytes:[responseData bytes]
                                                             length:[responseData length]
                                                           encoding:NSUTF8StringEncoding] autorelease];
        if ([responseString isKindOfClass:[NSString class]]) {
            NSDictionary *dictResponse = [responseString JSONValue];
            
            if (dictResponse == NO) {
                BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_DATA, BESAFE_ERROR_MSG_DATA);
                NSLog(@"test_error data");
                return nil;
            }
            else {
                if ([[dictResponse objectForKey:@"returnCode"] isEqualToString:@"SUCCESS"]) {
                    return dictResponse;
                }
                else {
                    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_FROM_SERVER ,
                                            [dictResponse objectForKey:@"msg"] );
                    return dictResponse;
                }
            }
        }
    }
    BESAFE_API_HANDLE_ERROR(BESAFE_ERROR_CODE_UNKNOW, BESAFE_ERROR_MSG_UNKNOW);
    NSLog(@"test_error unknow");
    return nil;

}


@end
