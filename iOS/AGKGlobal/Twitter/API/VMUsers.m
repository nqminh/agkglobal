//
//  VMUsers.m
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 10/11/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "VMUsers.h"
#import "SFHFKeychainUtils.h"
#import "Constants.h"

static VMUsers *currentUser = nil;

@implementation VMUsers

@synthesize userId = objectId;
@synthesize userAvatar;
@synthesize username;
@synthesize fullname;
@synthesize password;
@synthesize phone;
@synthesize email;
@synthesize authData;
@synthesize sessionToken;
@synthesize birthday;
@synthesize zipcode;
@synthesize gender;
@synthesize userId_login;

#pragma mark Singleton Pattern
/**
 * Singleton Pattern WFUser
 */
+ (VMUsers *)currentUser
{
    if (!currentUser) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            currentUser = [[VMUsers alloc] init];
        });
    }
    
    return currentUser;
}

/**
 * Save VMUsers information
 */
- (void)saveUserInfo
{
    NSError *error = nil;
    // userId
    [SFHFKeychainUtils storeUsername:USER_ID_DICT_KEY andPassword:self.userId
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    // userAvatar
    [SFHFKeychainUtils storeUsername:USER_AVATAR_DICT_KEY andPassword:self.userAvatar
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    // username
    [SFHFKeychainUtils storeUsername:USER_NAME_DICT_KEY andPassword:self.username
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    // fullname
    [SFHFKeychainUtils storeUsername:FULL_NAME_DICT_KEY andPassword:self.fullname
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    // phone
    [SFHFKeychainUtils storeUsername:PHONE_DICT_KEY andPassword:self.phone
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    
    // email
    [SFHFKeychainUtils storeUsername:EMAIL_DICT_KEY andPassword:self.email
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    
    // authData
    [SFHFKeychainUtils storeUsername:AUTH_DATA_DICT_KEY andPassword:self.authData
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    
    // createdAt
    [SFHFKeychainUtils storeUsername:CREATED_AT_DICT_KEY andPassword:self.createdAt
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    // updatedAt
    [SFHFKeychainUtils storeUsername:UPDATED_AT_DICT_KEY andPassword:self.updatedAt
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    // sessionToken
    [SFHFKeychainUtils storeUsername:SESSION_TOKEN_DICT_KEY andPassword:self.sessionToken
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    
    // zipcode
    [SFHFKeychainUtils storeUsername:ZIPCODE_DICT_KEY andPassword:self.zipcode
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    
    // birthday
    [SFHFKeychainUtils storeUsername:BIRTHDAY_DICT_KEY andPassword:self.birthday
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    
    // gender
    [SFHFKeychainUtils storeUsername:GENDER_DICT_KEY andPassword:self.gender
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    
    
    // userID login
    [SFHFKeychainUtils storeUsername:USER_ID_LOGIN_DICT_KEY andPassword:self.userId_login
                      forServiceName:CID_ServiceName updateExisting:YES error:&error];
    
    // buddy id arr
    
    if ([self.arrbuddys count] > 0)
    {
        [[NSUserDefaults standardUserDefaults] setObject:self.arrbuddys forKey:ARR_BUDDY_LOGIN];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }    
} 

/**
 * Get WFUser information
 */
- (void)getUserInfo
{
    NSError *error = nil;
    self.userId       = [SFHFKeychainUtils getPasswordForUsername:USER_ID_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.userAvatar   = [SFHFKeychainUtils getPasswordForUsername:USER_AVATAR_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.username     = [SFHFKeychainUtils getPasswordForUsername:USER_NAME_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.fullname     = [SFHFKeychainUtils getPasswordForUsername:FULL_NAME_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.phone        = [SFHFKeychainUtils getPasswordForUsername:PHONE_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.email        = [SFHFKeychainUtils getPasswordForUsername:EMAIL_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.authData     = [SFHFKeychainUtils getPasswordForUsername:AUTH_DATA_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.createdAt    = [SFHFKeychainUtils getPasswordForUsername:CREATED_AT_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.updatedAt    = [SFHFKeychainUtils getPasswordForUsername:UPDATED_AT_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.sessionToken = [SFHFKeychainUtils getPasswordForUsername:SESSION_TOKEN_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.zipcode = [SFHFKeychainUtils getPasswordForUsername:ZIPCODE_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.birthday = [SFHFKeychainUtils getPasswordForUsername:BIRTHDAY_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.gender = [SFHFKeychainUtils getPasswordForUsername:GENDER_DICT_KEY andServiceName:CID_ServiceName error:&error];
    //--get userID
    self.userId_login = [SFHFKeychainUtils getPasswordForUsername:USER_ID_LOGIN_DICT_KEY andServiceName:CID_ServiceName error:&error];
    self.arrbuddys = [[NSMutableArray alloc] init];
    self.arrbuddys = [[NSUserDefaults standardUserDefaults] objectForKey:ARR_BUDDY_LOGIN];
    DLOG(@"buddys 123 %@",self.arrbuddys);
}

- (void)copyx:(VMUsers*)other
{
    self.userId       = other.userId;
    self.userAvatar   = other.userAvatar;
    self.username     = other.username;
    self.fullname     = other.fullname;
    self.phone        = other.phone;
    self.email        = other.email;
    self.authData     = other.authData;
    self.createdAt    = other.createdAt;
    self.updatedAt    = other.updatedAt;
    self.sessionToken = other.sessionToken;
    self.zipcode      = other.zipcode;
    self.birthday     = other.birthday;
    self.gender       = other.gender;
}

+ (void)currentUserCopy:(VMUsers*)user
{
    [[VMUsers currentUser] copyx:user];
}

/**
 * Delete WFUser information
 */
+ (void)deleteUserInfo
{
    NSError *error = nil;
    [SFHFKeychainUtils deleteItemForUsername:USER_ID_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:USER_AVATAR_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:USER_NAME_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:FULL_NAME_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:PHONE_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:EMAIL_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:AUTH_DATA_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:CREATED_AT_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:UPDATED_AT_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:SESSION_TOKEN_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:ZIPCODE_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:BIRTHDAY_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:GENDER_DICT_KEY andServiceName:CID_ServiceName error:&error];
    [SFHFKeychainUtils deleteItemForUsername:USER_ID_LOGIN_DICT_KEY andServiceName:CID_ServiceName error:&error];    
    
}

/**
 * Save a VMUsers
 * @param object The VMUsers need to save
 */
+ (void)saveUser:(VMUsers *)object
{
    [object saveUserInfo];
    
}

/**
 * ReadData of VMUsers
 * @result The current VMUsers
 */
+ (VMUsers *)readData
{
    if (!currentUser) {
        VMUsers *newUser = [[VMUsers alloc] init];
        [newUser getUserInfo];
        currentUser = [newUser retain];
        
    }
    return currentUser;
}

/**
 * Delete current VMUsers
 */
+ (void)cleanUser
{
    [VMUsers deleteUserInfo];
}

/**
 * Check login status
 * @result YES / NO
 */
+ (BOOL)isLogin
{
    BOOL isLogin = NO;
    VMUsers *user = [ VMUsers readData ];
    if ( user.sessionToken != nil && user.sessionToken.length > 0 ) {
        isLogin = YES;
    }
    
    return isLogin;
    
}

/**
 * Logout Besafe account
 */
+ (void)logOut
{
    [VMUsers cleanUser];
    currentUser = nil;
}


@end

