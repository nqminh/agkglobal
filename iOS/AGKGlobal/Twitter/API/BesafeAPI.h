//
//  BesafeAPI.h
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 9/20/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSON.h"

#define NDEBUG 1

#ifdef NDEBUG
#define DDBG DBG(@"")
#define DBG(xx) NSLog(@"%p %s (%d): %@", self, __PRETTY_FUNCTION__, __LINE__, xx)
#define DBGRECT(xx) DBG(NSStringFromCGRect(xx))
#define DBGPOINT(xx) DBG(NSStringFromCGPoint(xx))
#define LOG(xx, ...) NSLog(@"%p %s (%d): %@", self, __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:xx, ##__VA_ARGS__])
#else
#define DDBG
#define DBG(xx)
#define DBGRECT(xx)
#define DBGPOINT(xx)
#define LOG(xx, ...)
#endif

#define BESAFE_HOST (@"http://bio.vmodev.com:7200/besafe/") // http://besafeapps.com/api/
#define BESAFE_ERROR_DOMAIN (@"besafeapps.error.com") 


#define BESAFE_ERROR_CODE_NETWORK          0x100
#define BESAFE_ERROR_CODE_DATA             0x101
#define BESAFE_ERROR_CODE_UNKNOW           0x102
#define BESAFE_ERROR_CODE_FROM_SERVER      0x103

@interface BesafeAPI : NSObject

+ (NSDictionary *)updateContact:(NSString *)fullname age:(NSString *)age sex:(NSString *)sex email:(NSString *)email phone:(NSString *)phone facebookId:(NSString *)facebookId error:(NSError **)error;
 
+ (NSDictionary *)signinAPI:(NSString *)email password:(NSString *)password error:(NSError **)error;


+ (NSDictionary *)registerAPI:(NSString *)email full_name:(NSString *)full_name password:(NSString *)password zipcode:(NSString *)zipcode birthyear:(NSString *)birthyear gender:(NSString *)gender error:(NSError **)error;

+ (NSDictionary *)getContact:facebookId error:(NSError **)error;

+ (NSDictionary *)forgotPassAPI:(NSString *)email  error:(NSError **)error;

+ (NSDictionary *)getListUserRegistedBesafe:(NSString*)typeAcc withFriend:(NSString*)friend_list error:(NSError**)error;


#pragma mark - Update,Remove account to buddy

+ (NSDictionary *) updateAccountToBuddy:(NSString*)typeAcc userID:(NSString*)user_id buddyID:(NSString*)buddy_id error:(NSError**)error;

+ (NSDictionary *) removeAccountBuddy:(NSString*)typeAcc userID:(NSString*)user_id buddyID:(NSString*)buddy_id error:(NSError**)error;

#pragma mark - Post data buddy 

+ (NSDictionary *)postDataToPendingBuddy:(NSString*)typeAcc userID:(NSString*)user_id post_id:(NSString*)post_id data:(NSString*)dict error:(NSError**)error;

+ (NSDictionary*)getPostPendingUploaderId:(NSString*)user_id page:(NSString*)page_index error:(NSError**)error;
+ (NSDictionary*)getPostPendingUploaderId:(NSString*)user_id error:(NSError**)error;

#pragma mark - Post Photo album 
+ (NSDictionary*) postPhotoAlbum:(UIImage*)image account_type:(NSString*)typeAcc userID:(NSString*)user_id  data:(NSString*)dict error:(NSError**)error; 


#pragma mark - Get uploader 
+ (NSDictionary*) getListUploaderOfBuddy:(NSString*) buddy_id error:(NSError**)error;
+ (NSDictionary*) getListPostForBuddyConfirmID:(NSString*) buddy_id userID:(NSString*) user_id page_index:(NSString*)page_index error:(NSError**)error;


#pragma mark - Accept,Reject post uploader 
+ (NSDictionary*) updatePostbyBuddy:(NSString*) buddy_id idPost:(NSString*) id_post status:(NSString*)status error:(NSError**)error;
#pragma mark - Get detail post uploader 
+ (NSDictionary*) getDetailPostByID:(NSString*)post_id userID:(NSString*)user_id error:(NSError**)error;

#pragma mark - changeStatusPostByUploader
+ (NSDictionary*) changeStatusPostByUploader:(NSString*)post_id userID:(NSString*)user_id status:(NSString*)status  error:(NSError**)error;

@end
