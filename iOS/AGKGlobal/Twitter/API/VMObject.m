//
//  VMObject.m
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 10/11/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "VMObject.h"

@implementation VMObject

@synthesize objectId = _objectId;
@synthesize createdAt = _createdAt;
@synthesize updatedAt = _updatedAt;

/**
 * Saves the WFObject.
 * @result Returns whether the save succeeded.
 */
- (BOOL)save
{
    return YES;
}

/**
 * Saves the WFObject and sets an error if it occurs.
 * @param error Pointer to an NSError that will be set if necessary.
 * @result Returns whether the save succeeded.
 */
- (BOOL)save:(NSError **)error
{
    return YES;
}

/**
 * Deletes the WFObject.
 * @result Returns whether the delete succeeded.
 */
- (BOOL)delete
{
    return YES;
}

/**
 * Deletes the WFObject and sets an error if it occurs.
 * @param error Pointer to an NSError that will be set if necessary.
 * @result Returns whether the delete succeeded.
 */
- (BOOL)delete:(NSError **)error
{
    return YES;
}

/**
 * Update the WFObject.
 * @result Returns whether the update succeeded.
 */
- (BOOL)update
{
    return YES;
}

/**
 * Update the WFObject and sets an error if it occurs.
 * @param error Pointer to an NSError that will be set if necessary.
 * @result Returns whether the save succeeded.
 */
- (BOOL)update:(NSError **)error
{
    return YES;
}


@end
