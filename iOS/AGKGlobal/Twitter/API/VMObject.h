//
//  VMObject.h
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 10/11/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMObject : NSObject

@property (nonatomic, retain) NSString *objectId;
@property (nonatomic, retain) NSString *createdAt;
@property (nonatomic, retain) NSString *updatedAt;

/**
 * Saves the WFObject.
 * @result Returns whether the save succeeded.
 */
- (BOOL)save;

/**
 * Saves the WFObject and sets an error if it occurs.
 * @param error Pointer to an NSError that will be set if necessary.
 * @result Returns whether the save succeeded.
 */
- (BOOL)save:(NSError **)error;

/**
 * Deletes the WFObject.
 * @result Returns whether the delete succeeded.
 */
- (BOOL)delete;

/**
 * Deletes the WFObject and sets an error if it occurs.
 * @param error Pointer to an NSError that will be set if necessary.
 * @result Returns whether the delete succeeded.
 */
- (BOOL)delete:(NSError **)error;

/**
 * Update the WFObject.
 * @result Returns whether the update succeeded.
 */
- (BOOL)update;

/**
 * Update the WFObject and sets an error if it occurs.
 * @param error Pointer to an NSError that will be set if necessary.
 * @result Returns whether the save succeeded.
 */
- (BOOL)update:(NSError **)error;

@end

