//
//  ListPostUploaderVC.h
//  AGKGlobal
//
//  Created by Dung on 3/3/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailPostUploaderOfBuddyVC.h"


@interface ListPostUploaderVC : UIViewController<DetailPostUploaderOfBuddyDelegate>
{
    
}

@property (weak, nonatomic) IBOutlet UILabel *lblUploaderName;
@property (weak, nonatomic) IBOutlet UITableView *tblListPost;
@property (strong,nonatomic) NSMutableArray *listPostArr;
@property (strong,nonatomic) NSMutableArray *listPostObjArr;
@property (strong,nonatomic) NSString *uploader_name;
@property (strong,nonatomic) NSString *post_message;
@property (strong,nonatomic) NSString *post_url;
@property (strong,nonatomic) NSString *status_post;
@property (strong,nonatomic) NSString *post_id;
@property (strong,nonatomic) NSString *count_post;
@property (assign,nonatomic) NSInteger page_index;
@property (assign,nonatomic) CGFloat  page_number;
// for call api
@property (strong,nonatomic) NSString *user_id;
@property (strong,nonatomic) NSString *buddy_id;

@end
