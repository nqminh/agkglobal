//
//  ListPostUploaderVC.m
//  AGKGlobal
//
//  Created by Dung on 3/3/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import "ListPostUploaderVC.h" 
#import "UploaderCell.h" 
#import "MBProgressHUD.h" 
#import "BesafeAPI.h"  
#import "PostUploaderObj.h"  
#import "DetailPostUploaderOfBuddyVC.h" 
#import "ODRefreshControl.h"  
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UIScrollView+SVPullToRefresh.h"


typedef enum
{
    TableSectionListPostUploader,
    TableSectionLoadMore
} TableSection;

@interface ListPostUploaderVC ()

@end

@implementation ListPostUploaderVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.listPostArr    = [[NSMutableArray alloc] init];
    self.listPostObjArr = [[NSMutableArray alloc] init];
    [self showProgressInView:self.view target:self title:@"Getting List..." selector:@selector(apiGetListPostUploaderOfBuddy:)];
	[self setupRefreshControlTableView];
    self.page_index = 1;
    self.page_number = [self.count_post integerValue] / 20;
    if ([self.count_post integerValue] - self.page_number * 20 > 0)
    {
        self.page_number += 1;
    }
    
    
    [self.tblListPost addInfiniteScrollingWithActionHandler:^{
        self.page_index += 1;
        if (self.page_index <= self.page_number)
        {                        
            [self apiGetListPostUploaderOfBuddypage:[NSString stringWithFormat:@"%d",self.page_index]];
            NSLog(@"page index %d",self.page_index);
            
        }
//        else
//        {
//            [self performSelector:@selector(endRefreshViewTimeOut) withObject:nil afterDelay:5.0];
//        }
        
        [self.tblListPost.infiniteScrollingView stopAnimating];
    }];
     
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIRefresh Control

- (void) endRefreshViewTimeOut
{
//    [self.tblListPost.infiniteScrollingView stopAnimating];
//    [self.tblListPost.pullToRefreshView stopAnimating];
    
}

- (void) setupRefreshControlTableView
{
   
    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tblListPost];
    [refreshControl addTarget:self action:@selector(pulldownRefreshingPost:) forControlEvents:UIControlEventValueChanged];
    refreshControl.tintColor = [UIColor colorWithRed:162/256.0f green:20/256.0f blue:18/256.0f alpha:1.0];
    
}


- (void)pulldownRefreshingPost:(ODRefreshControl *)refreshControl
{
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.listPostObjArr removeAllObjects];
        NSError *error = nil;
        NSDictionary *dict = nil;
        
        //dict  = [BesafeAPI getListPostForBuddyConfirmID:self.buddy_id userID:self.user_id error:&error];
       dict  = [BesafeAPI getListPostForBuddyConfirmID:self.buddy_id userID:self.user_id page_index:@"1" error:&error];
        if (error)
        {
            [self showAlert:[error localizedDescription]];            
        }
        else
        {            
            self.listPostArr = [dict objectForKey:@"data"];
            NSLog(@"pulldownRefreshingPost response %@",self.listPostArr);
            if ([self.listPostArr count] > 0)
            {
                for (NSInteger i = 0; i < [self.listPostArr count]; i++)
                {
                    PostUploaderObj *post = [[PostUploaderObj alloc] initWithDictinary:[self.listPostArr objectAtIndex:i]];
                    [self.listPostObjArr addObject:post];
                    
                }
                [self.tblListPost reloadData];
            }
        }
        
        [refreshControl endRefreshing];
    });
}



#pragma mark - TableView Delegate 


- (void) showImageAvailable:(PostUploaderObj*)post withCell:(UploaderCell*)cell
{
    //--show
    if ([post.pul_media_file length] > 0)
    {
        cell.imgAvailable.image = [UIImage imageNamed:@"btn_choose_img.png"];
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.listPostObjArr count];    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UploaderCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"UploaderCell" owner:self options:nil] objectAtIndex:0];
    return cell.frame.size.height;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PostUploaderObj *post_obj = [self.listPostObjArr objectAtIndex:indexPath.row];
    UploaderCell *cell = nil;            
        if (cell == nil)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"UploaderCell" owner:self options:nil] objectAtIndex:0];
        }              
        
        CGRect frame = cell.avatarUploader.frame;
        frame.size.width = 10;
        cell.avatarUploader.frame = frame;
        cell.avatarUploader.image = [UIImage imageNamed:@"icn-fblist.png"];
        self.lblUploaderName.text = self.uploader_name;
        self.post_message = post_obj.pul_message;
        cell.lblContent.text = self.post_message;
         NSString *date_post = [Utility stringFromDatewithTime:[NSDate dateWithTimeIntervalSince1970:[post_obj.pul_post_date longLongValue]]];
        cell.lblTimePost.text = [Utility getFormatDateStringWithDate:date_post];
        [self showImageAvailable:post_obj withCell:cell];
        [self getStatusForPost:post_obj.pul_status withLabel:cell.lblStatus];        
          
    return cell;  
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DetailPostUploaderOfBuddyVC *detail = [[DetailPostUploaderOfBuddyVC alloc] initWithNibName:@"DetailPostUploaderOfBuddyVC" bundle:nil];
    PostUploaderObj *post_obj = [self.listPostObjArr objectAtIndex:indexPath.row];
    detail.avatar_name      = self.uploader_name;
    detail.post_date        = [Utility getFormatDateStringWithDate:post_obj.pul_post_date];
    detail.content = post_obj.pul_message;
    detail.url_image = [NSString stringWithFormat:@"%@%@",BESAFE_HOST, post_obj.pul_media_file];
    detail.buddy_id = self.buddy_id;
    detail.post_id = post_obj.pul_post_id;
    detail.status_post = post_obj.pul_status;
    detail.delegate = self;
    [self.navigationController pushViewController:detail animated:YES];   
    
}


#pragma mark - Call API

- (void) apiGetListPostUploaderOfBuddy:(MBProgressHUD*)progress
{
    NSError *error = nil;
    NSDictionary *dict = nil;
    
    dict  = [BesafeAPI getListPostForBuddyConfirmID:self.buddy_id userID:self.user_id page_index:@"1" error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
        [progress hide:YES];
    }
    else
    {
        [progress hide:YES];
        self.listPostArr = [dict objectForKey:@"data"];
        if ([self.listPostArr count] > 0)
        {
            for (NSInteger i = 0; i < [self.listPostArr count]; i++)
            {
                PostUploaderObj *post = [[PostUploaderObj alloc] initWithDictinary:[self.listPostArr objectAtIndex:i]];
                [self.listPostObjArr addObject:post];
                
            }
            [self.tblListPost reloadData];
        }
    }
    [self.tblListPost.infiniteScrollingView stopAnimating];
    // [[self.tblListPost infiniteScrollingViewForPosition:SVInfiniteScrollingPositionBottom]stopAnimating];
    [self.tblListPost.pullToRefreshView stopAnimating];
}

- (void) apiGetListPostUploaderOfBuddypage:(NSString*)page
{
    NSError *error = nil;
    NSDictionary *dict = nil;
    
    dict  = [BesafeAPI getListPostForBuddyConfirmID:self.buddy_id userID:self.user_id page_index:page error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
        
    }
    else
    {
       
        self.listPostArr = [dict objectForKey:@"data"];
        if ([self.listPostArr count] > 0)
        {
            for (NSInteger i = 0; i < [self.listPostArr count]; i++)
            {
                PostUploaderObj *post = [[PostUploaderObj alloc] initWithDictinary:[self.listPostArr objectAtIndex:i]];
                [self.listPostObjArr addObject:post];
                
            }           
            [self.tblListPost reloadData];
            [self.tblListPost.infiniteScrollingView stopAnimating];
             //[[self.tblListPost infiniteScrollingViewForPosition:SVInfiniteScrollingPositionBottom]stopAnimating];
            [self.tblListPost.pullToRefreshView stopAnimating];
        }
    }
}


#pragma mark - Ultility method

- (UIView*)frontFromView:(UIView*)aView
{
    UIWindow* tempWindow = nil;
    UIView *keyboard = nil;
    
    if ([[[UIApplication sharedApplication] windows] count] > 1) {
        tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    }
    
    if ([tempWindow.subviews count])
        keyboard = [tempWindow.subviews objectAtIndex:0];
    
    return (keyboard != nil ? tempWindow : aView);
}


- (MBProgressHUD*)showProgressInView:(UIView*)view target:(id)aTarget
                               title:(NSString*)titleText selector:(SEL)aSelector
{
    UIView *frontView = [self frontFromView:view];
    
    MBProgressHUD* mbhub = [[MBProgressHUD alloc] initWithView:frontView];
	mbhub.labelText = titleText;
    mbhub.removeFromSuperViewOnHide = YES;
    [mbhub setDelegate:(id)aTarget];
	[mbhub show:YES];
	[frontView addSubview:mbhub];
    [aTarget performSelector:aSelector withObject:mbhub afterDelay:0.3];
    return mbhub;
}


- (void) showAlert: (NSString*) msg
{
	[[[UIAlertView alloc] initWithTitle: nil
                                message: msg
                               delegate: nil
                      cancelButtonTitle: @"OK"
                      otherButtonTitles: nil] show];
}

- (void) getStatusForPost:(NSString*)status_code withLabel:(UILabel*)label
{
    
    switch ([status_code integerValue])
    {
        case 0:
            label.text = @"Waiting";
            label.textColor = [UIColor colorWithRed:157.0/255.0 green:157.0/255.0 blue:157.0/255.0 alpha:1.0];            
            break;
        case 1:
            label.text = @"Approved";
            label.textColor = [UIColor colorWithRed:138.0/255.0 green:183.0/255.0 blue:72.0/255.0 alpha:1.0];
            break;
        case 2:
            label.text = @"Rejected";
            label.textColor = [UIColor colorWithRed:195.0/255.0 green:71.0/255.0 blue:71.0/255.0 alpha:1.0];
            break;
        case 4:
            label.text = @"Posted";
            label.textColor = [UIColor colorWithRed:108.0/255.0 green:173.0/255.0 blue:238.0/255.0 alpha:1.0];
            break;
            
        default:
            label.text = @"Watting";
            label.textColor = [UIColor colorWithRed:157.0/255.0 green:157.0/255.0 blue:157.0/255.0 alpha:1.0]; 
            break;
    }
}

#pragma mark - Change status post uploader of buddy 

- (void) changeStatusPost:(NSString*)status postid:(NSString *)post_id
{
    self.status_post = status;
    self.post_id = post_id;
    [self.listPostObjArr removeAllObjects];
    [self showProgressInView:self.view target:self title:@"Getting List..." selector:@selector(apiGetListPostUploaderOfBuddy:)];
    [self.tblListPost reloadData];
}

@end
