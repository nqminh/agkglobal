//
//  DetailPostUploaderOfBuddyVC.m
//  AGKGlobal
//
//  Created by Dung on 3/3/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import "DetailPostUploaderOfBuddyVC.h" 
#import "Template.h" 
#import "BesafeAPI.h"  
#import "MBProgressHUD.h" 
#import "Utility.h"

@interface DetailPostUploaderOfBuddyVC ()

@end

@implementation DetailPostUploaderOfBuddyVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self showInfo];
    [self setupInfoLabel];
    [self setupButtonView];
    
} 


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) showInfo
{
    self.lblContent.text = self.content;
    self.lblusername.text = self.avatar_name;
    self.lblPostDate.text = self.post_date;   
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
       NSURL *url = [NSURL URLWithString:self.url_image];
       NSData *imageData = [NSData dataWithContentsOfURL:url];
       UIImage *image = [UIImage imageWithData:imageData];
       
       dispatch_async(dispatch_get_main_queue(), ^{
           [self setupImageView:image];
       });
   });
    
}


#pragma mark - setup label 

- (void) setupInfoLabel
{
    self.lblContent.font = [UIFont systemFontOfSize:14.0];
    [self.lblContent sizeToFit];
    CGSize ActualSizeOfLabel = [self.lblContent.text sizeWithFont:self.lblContent.font
                                    constrainedToSize:self.lblContent.frame.size
                                        lineBreakMode:NSLineBreakByWordWrapping];   
    
    int noOfline = ActualSizeOfLabel.height/22;
    self.lblContent.numberOfLines = noOfline;
    CGRect frame = self.lblContent.frame;
    frame.size.height = ActualSizeOfLabel.height;
    height_label = ActualSizeOfLabel.height;
    DLOG(@"HEIGHT X %f",ActualSizeOfLabel.height);
    self.lblContent.frame = frame;
}


- (void) setupImageView:(UIImage*)imagePost
{
    CGRect frame = self.imgPost.frame;
    frame.origin.y  = self.lblContent.frame.origin.y + height_label + 20;
    self.imgPost.frame = frame;    
    NSInteger heigh_view = (280 * imagePost.size.height) / imagePost.size.width;
    //NSLog(@"heigh_view %d",heigh_view);
    heigh_view = (heigh_view < 0) ? 0 : heigh_view;
    height_label = (height_label < 0) ? 0 : height_label;
    frame.size.height = heigh_view;
    self.imgPost.frame = frame;
    self.imgPost.image = imagePost;
    NSInteger heigh_scrl = heigh_view + height_label + 200 ;
    CGRect frame_scroll = self.scrlContent.frame;
    frame_scroll.size.height = heigh_view + height_label + 180;
    self.scrlContent.frame = frame_scroll;
    self.scrlContent.contentSize = CGSizeMake(300, heigh_scrl);
    self.imgAvatar.image = [UIImage imageNamed:@"uploader_avatar.png"];
}


- (void) setupButtonView
{
    UIBarButtonItem *AcceptButton = [[UIBarButtonItem alloc] initWithTitle:@"Approve" style:UIBarButtonItemStyleDone target:self action:@selector(clickAccept)];
    
    
    UIBarButtonItem *RejectButton = [[UIBarButtonItem alloc] initWithTitle:@"Reject" style:UIBarButtonItemStyleDone target:self action:@selector(clickReject)];
    
    switch ([self.status_post integerValue])
    {
        case 1:
            AcceptButton.enabled = FALSE;
            RejectButton.enabled = FALSE;
            break;
        case 2:
            AcceptButton.enabled = FALSE;
            RejectButton.enabled = FALSE;
            break;
        case 4:
            AcceptButton.enabled = FALSE;
            RejectButton.enabled = FALSE;
            break;

        default:
            break;
    }
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:AcceptButton, RejectButton, nil];
    self.navigationItem.rightBarButtonItems = myButtonArray;


}

#pragma mark - Utility methods 

- (UIView*)frontFromView:(UIView*)aView
{
    UIWindow* tempWindow = nil;
    UIView *keyboard = nil;
    
    if ([[[UIApplication sharedApplication] windows] count] > 1) {
        tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    }
    
    if ([tempWindow.subviews count])
        keyboard = [tempWindow.subviews objectAtIndex:0];
    
    return (keyboard != nil ? tempWindow : aView);
}


- (MBProgressHUD*)showProgressInView:(UIView*)view target:(id)aTarget
                               title:(NSString*)titleText selector:(SEL)aSelector
{
    UIView *frontView = [self frontFromView:view];
    
    MBProgressHUD* mbhub = [[MBProgressHUD alloc] initWithView:frontView];
	mbhub.labelText = titleText;
    mbhub.removeFromSuperViewOnHide = YES;
    [mbhub setDelegate:(id)aTarget];
	[mbhub show:YES];
	[frontView addSubview:mbhub];
    [aTarget performSelector:aSelector withObject:mbhub afterDelay:0.3];
    return mbhub;
}


- (void) showAlert: (NSString*) msg
{
	[[[UIAlertView alloc] initWithTitle: nil
                                message: msg
                               delegate: nil
                      cancelButtonTitle: @"OK"
                      otherButtonTitles: nil] show];
}

#pragma mark - Accept & Reject Action 

- (void) clickAccept
{
    [self updatePostbyBuddyID:self.buddy_id idPost:self.post_id status:@"1"];
}


- (void) clickReject
{
    [self updatePostbyBuddyID:self.buddy_id idPost:self.post_id status:@"2"];
}


- (void) updatePostbyBuddyID:(NSString*)buddy_id idPost:(NSString*) post_id status:(NSString*)status
{
    NSError *error = nil;
    NSDictionary *dict = nil;
    
    
    dict = [BesafeAPI updatePostbyBuddy:buddy_id idPost:post_id status:status error:&error];
    if(error)
    {
        [self showAlert:[error localizedDescription]];
    }
    else
    {
        [self showAlert:[dict objectForKey:@"msg"]];
        if (self.delegate && [self.delegate respondsToSelector:@selector(changeStatusPost:postid:)])
        {
            [self.delegate changeStatusPost:status postid:post_id];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }

}


@end
