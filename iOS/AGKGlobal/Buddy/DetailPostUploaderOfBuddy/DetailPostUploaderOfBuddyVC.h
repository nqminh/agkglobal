//
//  DetailPostUploaderOfBuddyVC.h
//  AGKGlobal
//
//  Created by Dung on 3/3/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailPostUploaderOfBuddyDelegate <NSObject>
@required
- (void) changeStatusPost:(NSString*)status postid:(NSString*)post_id;
@end

@interface DetailPostUploaderOfBuddyVC : UIViewController
{
    int height_label;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblusername;
@property (weak, nonatomic) IBOutlet UILabel *lblPostDate;


@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIImageView *imgPost;
@property (assign,nonatomic) NSInteger is_enable;

//--
@property (strong,nonatomic) NSString  *avatar_imagae;
@property (strong,nonatomic) NSString  *avatar_name;
@property (strong,nonatomic) NSString  *post_date;
@property (strong,nonatomic) NSString  *content;
@property (strong,nonatomic) NSString  *url_image; 

//-- call api
@property (strong,nonatomic) NSString  *post_id;
@property (strong,nonatomic) NSString  *buddy_id;
@property (strong,nonatomic) NSString  *status_post;
@property (weak, nonatomic) IBOutlet UIScrollView *scrlContent;

//--delegate
@property (nonatomic,assign) id<DetailPostUploaderOfBuddyDelegate> delegate;


@end
