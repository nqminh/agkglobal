//
//  UploaderCell.h
//  AGKGlobal
//
//  Created by Dung on 3/1/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploaderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarUploader;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblTimePost;


@property (weak, nonatomic) IBOutlet UIImageView *imgAvailable;
@end
