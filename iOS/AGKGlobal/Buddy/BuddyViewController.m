//
//  BuddyViewController.m
//  AGKGlobal
//
//  Created by Dung on 2/25/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import "BuddyViewController.h" 
#import "BesafeAPI.h"  
#import "VMUsers.h" 
#import "BuddyCell.h" 
#import "PostUploaderObj.h" 
#import "UploaderCell.h"  
#import "ODRefreshControl.h" 
#import "UploaderOfBuddyObj.h" 
#import "ListPostUploaderVC.h"  
#import "Utility.h" 
#import "JSBadgeView.h"


@interface BuddyViewController ()

@end

@implementation BuddyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiverPostBuddyConfirm:) name:RECEIVE_NOTIFICATION object:nil];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationItem.title = @"BeSafe Buddy";
    self.arrUploaderTable = [[NSMutableArray alloc] init];
    self.arrBuddyTable = [[NSMutableArray alloc] init];
    self.segmentBuddy.selectedSegmentIndex = 0;
    self.isBuddyTab = 1;
    [self setupRefreshControlTableView];
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //-- change title color
    CGRect frame = CGRectMake(100, 0, 200, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    
    [self getBuddyIDUser];
    [self apiGetListUploaderOfBuddy];    
    [self getPostOfUploader];  

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewDidAppear:(BOOL)animated
{
    [self.tblBuddyList triggerInfiniteScrolling];
}

- (void) getBuddyIDUser
{
    VMUsers *user = [VMUsers readData];
    self.buddy_id = user.userId_login;
    if ([self.buddy_id length] == 0)
    {
        self.buddy_id = BESAFE_USER_DEFAULT;
    }
    NSLog(@"BUDDY ID %@",self.buddy_id);
}

#pragma mark - UIRefresh Control 

- (void) setupRefreshControlTableView
{
    
        NSLog(@"buddy tab %d",self.isBuddyTab);
        ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tblBuddyList];
        [refreshControl addTarget:self action:@selector(pulldownRefreshingBuddyTab:) forControlEvents:UIControlEventValueChanged];
        refreshControl.tintColor = [UIColor colorWithRed:162/256.0f green:20/256.0f blue:18/256.0f alpha:1.0];   
}


#pragma mark - Get post pending

- (void) showAlert: (NSString*) msg
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle: nil
                                message: msg
                               delegate: nil
                      cancelButtonTitle: @"OK"
                      otherButtonTitles: nil] ;
    [alert show];      
}

#pragma mark - Pull refresh Uploader tab 

- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refreshControl
{
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.arrUploaderTable removeAllObjects];
        NSError *error = nil;
        NSDictionary *dict_response = nil;
        
        VMUsers *user = [VMUsers readData];
        dict_response = [BesafeAPI getPostPendingUploaderId:user.userId_login error:&error];
        if(error)
        {            
            [self showAlert:[error localizedDescription]];
        }
        else
        {
            NSLog(@"DICT RESPONSE %@",dict_response);
            self.arrUploaderTable = [dict_response objectForKey:@"data"];
            [self.tblBuddyList reloadData];
        }

        [refreshControl endRefreshing];
    });
}



- (void) handlePullgetPostUploader:(ODRefreshControl*)refreshControl
{
    [self.arrUploaderTable removeAllObjects];
    [self getPostOfUploader];
    [refreshControl endRefreshing];
}


- (void) getPostOfUploader
{
    NSError *error = nil;
    NSDictionary *dict_response = nil;
    
    VMUsers *user = [VMUsers readData];
    NSString *user_id_bs = @"";
    if ([user.userId_login length] == 0)
    {
        user_id_bs = BESAFE_USER_DEFAULT;
    }
    else
    {
        user_id_bs = user.userId_login;
    }
    NSLog(@"BESAFE USER ID %@",user_id_bs);
    dict_response = [BesafeAPI getPostPendingUploaderId:user_id_bs error:&error];
    if(error)
    {
        [self showAlert:[error localizedDescription]];       
    }
    else
    {
        NSLog(@"DICT RESPONSE %@",dict_response);
        self.arrUploaderTable = [dict_response objectForKey:@"data"];
    }
}


#pragma mark - Pull refresh Buddy tab 

- (void)pulldownRefreshingBuddyTab:(ODRefreshControl *)refreshControl
{
    if (self.isBuddyTab)
    {
        double delayInSeconds = 3.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self.arrUploaderTable removeAllObjects];
            NSError *error = nil;
            NSDictionary *dict_response = nil;
            
            dict_response = [BesafeAPI getListUploaderOfBuddy:self.buddy_id error:&error];
            if (error)
            {
                [self showAlert:[error localizedDescription]];
            }
            else
            {
                self.arrBuddyTable = [dict_response objectForKey:@"data"];
                NSLog(@"BUDDY USER %@",self.arrBuddyTable);
                [self.tblBuddyList reloadData];
            }
            
            [refreshControl endRefreshing];
        });

    }
    else
    {
        [self dropViewDidBeginRefreshing:refreshControl];
    }
}


#pragma mark - Get List Uploader 

- (void) apiGetListUploaderOfBuddy
{
    NSError *error = nil;
    NSDictionary *dict_response = nil; 
    
    dict_response = [BesafeAPI getListUploaderOfBuddy:self.buddy_id error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];        
        
    }
    else
    {
        self.arrBuddyTable = [dict_response objectForKey:@"data"];
        NSLog(@"BUDDY USER %@",self.arrBuddyTable);
        [self.tblBuddyList reloadData];
    }
}

#pragma mark - Handle action 
- (IBAction)switchChangeSegment:(id)sender
{
    switch (self.segmentBuddy.selectedSegmentIndex)
    {
        case 0:
        {           
            [self.segmentBuddy setImage:[UIImage imageNamed:@"myposts_blank"] forSegmentAtIndex:1];
            [self.segmentBuddy setImage:[UIImage imageNamed:@"myreviews"] forSegmentAtIndex:0];           
            
            
            //--reload 
            self.isBuddyTab = 1; 
            [self.tblBuddyList reloadData];
        }
        break;
        case 1:
        {            
            [self.segmentBuddy setImage:[UIImage imageNamed:@"myposts"] forSegmentAtIndex:1];
            [self.segmentBuddy setImage:[UIImage imageNamed:@"myreviews_blank"] forSegmentAtIndex:0];            
          
            //--reload
            self.isBuddyTab = 0;
            [self.tblBuddyList reloadData];
        }
            break;
        default:
        {
            self.view.backgroundColor = [UIColor redColor];
        }

            break;
    }
}

#pragma mark - TableView Delegate  


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isBuddyTab)
    {
        return [self.arrBuddyTable count];
    }
    else
    {
        return [self.arrUploaderTable count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BuddyCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"BuddyCell" owner:self options:nil] objectAtIndex:0];
    return cell.frame.size.height;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"CellIdentifier";
    
    
    if (self.isBuddyTab)
    {
        BuddyCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"BuddyCell" owner:self options:nil]objectAtIndex:0];
        }

        UploaderOfBuddyObj *uploader = [[UploaderOfBuddyObj alloc] initWithDictinary:[self.arrBuddyTable objectAtIndex:indexPath.row] ];
        cell.imgAvatar.image = [UIImage imageNamed:@"uploader_avatar"];
        self.uploader_name = uploader.uob_full_name;
        cell.lblUploadName.text = self.uploader_name;     
        self.user_id = uploader.uob_user_id;
        if (uploader.uob_count > 0)
        {
            //-- add badge view number
            JSBadgeView *badgeView = [[JSBadgeView alloc] initWithParentView:cell.imgAvatar alignment:JSBadgeViewAlignmentCenterRight];
            badgeView.badgeText = uploader.uob_count;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType  = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    else
    {
        //--Uploader tab .
        UploaderCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"UploaderCell" owner:self options:nil]objectAtIndex:0];
        }
       
        if ([self.arrUploaderTable count] > 0)
        {
             PostUploaderObj *post = [[PostUploaderObj alloc] initWithDictinary:[self.arrUploaderTable objectAtIndex:indexPath.row]];
            if ([post.pul_social_type isEqualToString:@"facebook"])
            {
                CGRect frame = cell.avatarUploader.frame;
                frame.size.width = 10;
                cell.avatarUploader.frame = frame;
                cell.avatarUploader.image = [UIImage imageNamed:@"icn-fblist.png"];
                cell.lblContent.text = post.pul_message;
                cell.lblTimePost.text = [Utility getFormatDateStringWithDate:post.pul_post_date];
                NSLog(@"ID %@ - MESS %@",post.pul_post_id,post.pul_message);
                switch ([post.pul_status integerValue])
                {
                    case 0:
                        cell.lblStatus.text = @"Waiting";
                        cell.lblStatus.textColor = [UIColor colorWithRed:157.0/255.0 green:157.0/255.0 blue:157.0/255.0 alpha:1.0];
                        break;
                    case 1:
                        cell.lblStatus.text = @"Approved";
                        cell.lblStatus.textColor = [UIColor colorWithRed:138.0/255.0 green:183.0/255.0 blue:72.0/255.0 alpha:1.0];
                        break;
                    case 2:
                       cell.lblStatus.text = @"Rejected";
                       cell.lblStatus.textColor = [UIColor colorWithRed:195.0/255.0 green:71.0/255.0 blue:71.0/255.0 alpha:1.0];
                        break;
                        
                    default:
                        cell.lblStatus.text = @"Watting";
                        cell.lblStatus.textColor = [UIColor colorWithRed:157.0/255.0 green:157.0/255.0 blue:157.0/255.0 alpha:1.0];
                        break;
                }            
                
            }
            
        }        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
               
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isBuddyTab)
    {
        //--Buddy tab
        ListPostUploaderVC *list_post = [[ListPostUploaderVC alloc]  initWithNibName:@"ListPostUploaderVC" bundle:nil];
        list_post.buddy_id      = self.buddy_id;
        UploaderOfBuddyObj *uploader = [[UploaderOfBuddyObj alloc] initWithDictinary:[self.arrBuddyTable objectAtIndex:indexPath.row] ];
        list_post.user_id       = uploader.uob_user_id;        
        list_post.uploader_name = uploader.uob_full_name;
        list_post.count_post    = uploader.uob_count;
        [self.navigationController pushViewController:list_post animated:YES];
    }
    else
    {
        //--Uploader tab
    }
    
}


@end
