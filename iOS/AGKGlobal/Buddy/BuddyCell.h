//
//  BuddyCell.h
//  AGKGlobal
//
//  Created by Dung on 3/1/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h> 
#import "CustomXibCell.h"

@interface BuddyCell : UITableViewCell
{
    
}

@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblUploadName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberRequest;

@end
