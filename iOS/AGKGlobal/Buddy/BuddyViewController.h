//
//  BuddyViewController.h
//  AGKGlobal
//
//  Created by Dung on 2/25/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UIScrollView+SVPullToRefresh.h" 
#import "ODRefreshControl.h"
#import "DetailPostUploaderOfBuddyVC.h"

@interface BuddyViewController : UIViewController<DetailPostUploaderOfBuddyDelegate>
{
    //ODRefreshControl *refreshControl;
}

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentBuddy;
@property (weak, nonatomic) IBOutlet UITableView *tblBuddyList;

@property (assign,nonatomic) NSInteger isBuddyTab;
@property (strong,nonatomic) NSMutableArray *arrBuddyTable;
@property (strong,nonatomic) NSMutableArray *arrUploaderTable;
@property (strong,nonatomic) NSString *buddy_id;
@property (strong,nonatomic) NSString *user_id;
@property (strong,nonatomic) NSString *uploader_name;

@property(nonatomic,retain) UIRefreshControl    *refreshControl;


 
- (IBAction)switchChangeSegment:(id)sender;

@end
