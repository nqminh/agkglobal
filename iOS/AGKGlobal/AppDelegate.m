//
//  AppDelegate.m
//  AGKGlobal
//
//  Created by Jeff Jolley on 5/28/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "AppDelegate.h"

#import "FacebookWebViewController.h"
#import "SettingsViewController.h"
#import "TwitterViewController.h"
#import "SendTwitterMessageViewController.h"
#import "MessageViewController.h"
#import "MessageHistoryController.h"
#import "FacebookUnavailableViewController.h"
#import "QMessageViewController.h"
#import "RegisterViewController.h"
#import "SignInViewController.h"
#import "BuddyViewController.h"
#import "PlaySound.h"  
#import "TestFlight.h"

#define TFAppToken @"8798580e-bde0-4343-810f-7e4a70b8bc4d"

@implementation AppDelegate

#define actionSheetTwitterMode  1
#define actionSheetFacebookMode 2

@synthesize window = _window;
@synthesize tabBarController = _tabBarController;
@synthesize _accountStore,twitterAccounts;
@synthesize _twitterType,selectedTwitterAccount;
@synthesize splashView,splashSpinner;
@synthesize isNewTweet, isNewReply, userDict, nameDict, isSuccess, isOpenTweetLink, isMedia, isUrls,isConnecting, isPresentText,isNetworkNotFound,isLoginSuccessfully,isTerms,isLogOut,isQuitApp;
@synthesize imgTabBar, imgPost, imgTweet, imgText, imgSetting,imgBuddy;
@synthesize localNotification, willForceSend;
@synthesize isShowLink, isShowText;


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [FacebookUtils handleOpenURL:url sourceApplication:sourceApplication];
    
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    NSLog(@"app window size:%1.0fx%1.0f",self.window.frame.size.width,self.window.frame.size.height);
    
    UIImage *img;
    if (self.window.frame.size.height == 568 || self.window.frame.size.height == 1136) {
        NSLog(@"pre-1");
        img = [UIImage imageNamed:@"Default-568h@2x.png"];
    } else {
        NSLog(@"pre-2");
        img = [UIImage imageNamed:@"Default.png"];
    }
    self.splashView = [[UIImageView alloc] initWithFrame:self.window.bounds];
    self.splashView.image = img;
    
    [self.window addSubview:self.splashView];
    [self.window makeKeyAndVisible];
    
    self.splashSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    if (self.window.frame.size.height == 568 || self.window.frame.size.height == 1136) {
        self.splashSpinner.center = CGPointMake(160,394);
    } else {
        self.splashSpinner.center = CGPointMake(160,350);
    }
    [self.splashSpinner startAnimating];
    [self.window addSubview:self.splashSpinner];
    
     //--add remote logging . 
     [TestFlight takeOff:TFAppToken];
    
    [self performSelector:@selector(startup) withObject:self.splashView afterDelay:3.0];
    [[UIApplication sharedApplication]
     registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeAlert |
      UIRemoteNotificationTypeBadge |
      UIRemoteNotificationTypeSound)];
    
#if TARGET_IPHONE_SIMULATOR
    [[NSUserDefaults standardUserDefaults] setObject:IPAD3_DEVICE_TOKEN forKey:TOKEN_DEVICE];
    [[NSUserDefaults standardUserDefaults] synchronize];
#endif
    return YES;
}


- (void) startup
{
    [[Template shared] start];
    [self.splashView removeFromSuperview];
    [self.splashSpinner removeFromSuperview];
    
    //SET USER DEFAULTS
    NSString *path = [[NSBundle mainBundle] pathForResource:@"QuickList" ofType:@"plist"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSDictionary *appDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithInteger:DEFAULT_TIMER_SECS], @"timerSecs",
                                 [NSArray arrayWithContentsOfFile:path], @"QUICK_LIST",
                                 [NSArray arrayWithObjects:nil ], @"smsHistory", nil];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"voiceCountdown"];
	[defaults registerDefaults:appDefaults];
    
    //-- SignInViewController
    UIViewController *signinController = nil;
    if (isIphone5)
        signinController = [[SignInViewController alloc] initWithNibName:@"SignInViewController-568h" bundle:nil];
    else
        signinController = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
    UINavigationController *signinNavController = [[UINavigationController alloc] initWithRootViewController:signinController];
    
    
    //--FacebookWebViewController
    UIViewController *facebookController = nil;
    UINavigationController *facebookNavController;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
        if (isIphone5) {
            facebookController = [[FacebookWebViewController alloc] initWithNibName:@"FacebookWebViewController-568h" bundle:nil];
        }
        else{
            facebookController = [[FacebookWebViewController alloc] initWithNibName:@"FacebookWebViewController" bundle:nil];
        }
        facebookNavController = [[UINavigationController alloc] initWithRootViewController:facebookController];
    } else {
        facebookController = [[FacebookUnavailableViewController alloc] initWithNibName:@"FacebookUnavailableViewController" bundle:nil];
        facebookNavController = [[UINavigationController alloc] initWithRootViewController:facebookController];
    }
    
    
    //-- TwitterViewController
    UIViewController *twitterController = nil;
    if (isIphone5)
        twitterController = [[TwitterViewController alloc] initWithNibName:@"TwitterViewController-568h" bundle:nil];
    else
        twitterController = [[TwitterViewController alloc] initWithNibName:@"TwitterViewController" bundle:nil];
    UINavigationController *twitterNavController = [[UINavigationController alloc] initWithRootViewController:twitterController];
    
    
    //-- QMessageViewController
    UIViewController *qMsgController = [[QMessageViewController alloc] initWithStyle:UITableViewStylePlain];
    UINavigationController *qMsgNavController = [[UINavigationController alloc] initWithRootViewController:qMsgController];
   
    
    //-- SettingsViewController
    UIViewController *settingsController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
    UINavigationController *settingsNavController = [[UINavigationController alloc] initWithRootViewController:settingsController];
   
    
    UIViewController *buddyController = [[BuddyViewController alloc] initWithNibName:@"BuddyViewController" bundle:nil];
    //}
    UINavigationController *buddyNavController = [[UINavigationController alloc] initWithRootViewController:buddyController];
    
    //ADD ORANGE THEME IF DEVICE HAS IOS 5
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.0")) {
        [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:(53.0/255.0) green:(162.0/255.0) blue:(221.0/255.0) alpha:1]];
        [[UISegmentedControl appearance] setTintColor:[UIColor colorWithRed:(53.0/255.0) green:(162.0/255.0) blue:(221.0/255.0) alpha:1]];
        [[UISwitch appearance] setOnTintColor:[UIColor colorWithRed:(53.0/255.0) green:(162.0/255.0) blue:(221.0/255.0) alpha:1]];
        [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:(53.0/255.0) green:(162.0/255.0) blue:(221.0/255.0) alpha:1]];
    }
    
     
    
    self.tabBarController = [[UITabBarController alloc] init];
    
    /* --Dung comment refact uitabbar 
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"QUIT_APP"] || [[NSUserDefaults standardUserDefaults] boolForKey:@"LOG_OUT"]) {
        
        self.tabBarController.viewControllers = @[
                                                 signinNavController,
                                                 facebookNavController,
                                                 twitterNavController,
                                                 qMsgNavController,
                                                 buddyNavController,
                                                 settingsNavController
                                            ];
        
        self.isLoginSuccessfully = NO;
    }else{
        self.tabBarController.viewControllers = [NSArray arrayWithObjects:
                                                 facebookNavController,
                                                 twitterNavController,
                                                 qMsgNavController,
                                                 buddyNavController,
                                                 settingsNavController,
                                                 nil];
        [self getTwitterAccounts];
    }
    */
    
    self.tabBarController.viewControllers = @[
                                              facebookNavController,
                                              twitterNavController,
                                              qMsgNavController,
                                              buddyNavController,
                                              settingsNavController
                                              ];

    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"QUIT_APP"] || [[NSUserDefaults standardUserDefaults] boolForKey:@"LOG_OUT"])
    {
         self.window.rootViewController = signinNavController;
         self.isLoginSuccessfully = NO;
    }
    else
    {
         [self getTwitterAccounts];
         self.window.rootViewController = self.tabBarController;

    }
    
    self.tabBarController.delegate = self;   
    
    //--set image for tabbar
    [self setupImage5Tab];
    //[self setupImagefor4Tabbar];
    
    
    
    //--willHideWarning
    willHideWarning = [[NSUserDefaults standardUserDefaults] boolForKey:@"WILL_HIDE_WARNING"];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"smsUnlocked"];
    
    // RESET THE TEMPORARY MESSAGE THAT MIGHT BE SENT OUT //
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"IS_SENDING_MESSGE"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_SECONDS"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_RESUME"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SEND_TYPE"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SEND_ADDRESS"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FORCE_SEND"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    justSetLocalNotification = NO;
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:warningControllTwitter];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:warningControllFacebook];
}


- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    //willForceSend = NO;
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FORCE_SEND"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSInteger indexOfTab = [tabBarController.viewControllers indexOfObject:viewController];
    if (indexOfTab == 2 && !willHideWarning) {
        
        //-- "Dung" comment bug #211 BeSafe Text
        //[self presentBeSafeTextMessage];
        
        
    }else {
        
        if ([viewController isKindOfClass:[UINavigationController class]]) {
            [(UINavigationController*)viewController popToRootViewControllerAnimated:NO];
        }
    }
    
    if (indexOfTab == 0)
    {
        [imgPost setImage:[UIImage imageNamed:@"post_selection.png"]];
        [imgTweet setImage:[UIImage imageNamed:@"tweet.png"]];
        [imgText setImage:[UIImage imageNamed:@"text.png"]];
        [imgBuddy setImage:[UIImage imageNamed:@"buddy.png"]];
        [imgSetting setImage:[UIImage imageNamed:@"settings.jpg"]];
        
    }
    else if (indexOfTab == 1)
    {
        [imgPost setImage:[UIImage imageNamed:@"post.png"]];
        [imgTweet setImage:[UIImage imageNamed:@"tweet_selection.png"]];
        [imgText setImage:[UIImage imageNamed:@"text.png"]];
        [imgBuddy setImage:[UIImage imageNamed:@"buddy.png"]];
        [imgSetting setImage:[UIImage imageNamed:@"settings.jpg"]];
    }
    else if (indexOfTab == 2)
    {
        [imgPost setImage:[UIImage imageNamed:@"post.png"]];
        [imgTweet setImage:[UIImage imageNamed:@"tweet.png"]];
        [imgText setImage:[UIImage imageNamed:@"text_selection.png"]];
        [imgBuddy setImage:[UIImage imageNamed:@"buddy.png"]];
        [imgSetting setImage:[UIImage imageNamed:@"settings.jpg"]];
    }
    else if (indexOfTab == 3)
    {
        [imgPost setImage:[UIImage imageNamed:@"post.png"]];
        [imgTweet setImage:[UIImage imageNamed:@"tweet.png"]];
        [imgText setImage:[UIImage imageNamed:@"text.png"]];
        [imgBuddy setImage:[UIImage imageNamed:@"buddy_selection.png"]];
        [imgSetting setImage:[UIImage imageNamed:@"settings.jpg"]];
    }
    
    else if (indexOfTab == 4)
    {
        [imgPost setImage:[UIImage imageNamed:@"post.png"]];
        [imgTweet setImage:[UIImage imageNamed:@"tweet.png"]];
        [imgText setImage:[UIImage imageNamed:@"text.png"]];
        [imgBuddy setImage:[UIImage imageNamed:@"buddy.png"]];
        [imgSetting setImage:[UIImage imageNamed:@"settings_selection.png"]];
    }

}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refresh data" object:self];
    NSLog(@"applicationWillEnterForeground");
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    justSetLocalNotification = NO;
    BOOL willForceSendxxx = [[NSUserDefaults standardUserDefaults] boolForKey:@"FORCE_SEND"];
    
    if (!willForceSendxxx) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"APP_RESTARTED"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"APP_RESUME"];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"APP_RESTARTED"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"APP_RESUME"];
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FORCE_SEND_MESSAGE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self setLocalNotification];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AutoRefreshNewsFeed"];
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [FBAppCall handleDidBecomeActiveWithSession:[FacebookUtils facebookSession]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"applicationDidBecomeActiveNotify" object:self];
    
    if ([self isAutoRefreshNewsFeed])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:AUTO_REFRESH_NEWFEED_DATA object:self];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AutoRefreshNewsFeed"];
        DLOG(@"AUTO REFESH NEW FEED");
    }
    
    if (self.isNetworkNotFound) {
        [self getTwitterAccounts];
        self.isNetworkNotFound = NO;
    }
}


- (BOOL) isAutoRefreshNewsFeed
{
    BOOL refresh = [[NSUserDefaults standardUserDefaults] boolForKey:@"AutoRefreshNewsFeed"];
    return refresh;
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[FacebookUtils facebookSession] close];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"QUIT_APP"]){
        isQuitApp = YES;
        [self setUpTabbarViewController];
    }else
        isQuitApp = NO;
}


//- (void)application:(UIApplication *)app didReceiveLocalNotification:(UILocalNotification *)notif
//{
//    // Handle the notificaton when the app is running
//    NSLog(@"Recieved Notification %@",notif);
//}

#pragma mark - Push Notification

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *token = [NSString stringWithFormat:@"%@",deviceToken];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    DLOG(@"[DEVICE_TOKEN]: %@", token);
    //
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:TOKEN_DEVICE];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	DLOG(@"\nDEVICE_TOKEN GET FAILED: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].applicationIconBadgeNumber += 1;
        [PlaySound playSoundWhenReceivePush];
        
        if ([application applicationState] == UIApplicationStateInactive || [application applicationState] == UIApplicationStateActive)
        {
            DLOG(@"\nAPPLICATION IS RUNNING.... %@",userInfo);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:RECEIVE_NOTIFICATION object:nil userInfo:userInfo];           
        }
        else
        {
            DLOG(@"\nAPPLICATION IS BACKGROUND...%@",userInfo);
            
        }
    });
}


#pragma mark - Set up tabbar

- (void)setUpTabbarViewController
{
    FacebookWebViewController *facebookVC = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        if(isIphone5) {
            facebookVC = [[FacebookWebViewController alloc] initWithNibName:@"FacebookWebViewController-568h" bundle:nil];
        }else{
            facebookVC = [[FacebookWebViewController alloc] initWithNibName:@"FacebookWebViewController" bundle:nil];
        }
    }
    
    SignInViewController *signInVC = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        if(isIphone5) {
            signInVC = [[SignInViewController alloc] initWithNibName:@"SignInViewController-568h" bundle:nil];
        }else{
            signInVC = [[SignInViewController alloc] initWithNibName:@"SignInViewController" bundle:nil];
        }
    }
    
    UINavigationController *facebookNaVC = [[UINavigationController alloc] initWithRootViewController:facebookVC];
    UINavigationController *signInNaVC = [[UINavigationController alloc] initWithRootViewController:signInVC];
    
    //-- changes list tabbarViewController
    NSMutableArray *tbViewControllers = [NSMutableArray arrayWithArray:[self.tabBarController viewControllers]];
    [tbViewControllers removeObjectAtIndex:0];
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"QUIT_APP"] || [[NSUserDefaults standardUserDefaults] boolForKey:@"LOG_OUT"])
        [tbViewControllers insertObject:signInNaVC atIndex:0];
    else
        [tbViewControllers insertObject:facebookNaVC atIndex:0];
    
    [self.tabBarController setViewControllers:tbViewControllers];
}

- (void) setupImagefor4Tabbar
{
    //--set image for tabbar
    
    imgTabBar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 384, 49)];
    self.tabBarController.tabBar.tag = 10;
    [self.tabBarController.tabBar addSubview:imgTabBar];
    self.tabBarController.delegate = self;
    
    imgPost = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 49)];
    imgPost.image = [UIImage imageNamed:@"icon_post_selected.png"];
    [imgTabBar addSubview:imgPost];
    
    imgTweet = [[UIImageView alloc]initWithFrame:CGRectMake(80, 0, 80, 49)];
    imgTweet.image = [UIImage imageNamed:@"icon_tweet.png"];
    [imgTabBar addSubview:imgTweet];
    
    imgText = [[UIImageView alloc]initWithFrame:CGRectMake(160, 0, 80, 49)];
    imgText.image = [UIImage imageNamed:@"icon_text.png"];
    [imgTabBar addSubview:imgText];
    
    imgSetting = [[UIImageView alloc]initWithFrame:CGRectMake(240, 0, 80, 49)];
    imgSetting.image = [UIImage imageNamed:@"icon_setting.png"];
    [imgTabBar addSubview:imgSetting];
}

- (void) setupImage5Tab
{
    imgTabBar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 384, 49)];
    imgTabBar.image = [UIImage imageNamed:@"tabbar.png"];
    self.tabBarController.tabBar.tag = 10;    
    self.tabBarController.delegate = self;
    
    imgPost = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 64, 49)];
    imgPost.image = [UIImage imageNamed:@"post_selection.png"];
    [imgTabBar addSubview:imgPost];   
        
    imgTweet = [[UIImageView alloc]initWithFrame:CGRectMake(64, 0, 64, 49)];
    imgTweet.image = [UIImage imageNamed:@"tweet.png"];
    [imgTabBar addSubview:imgTweet];
    
    imgText = [[UIImageView alloc]initWithFrame:CGRectMake(128, 0, 64, 49)];
    imgText.image = [UIImage imageNamed:@"text.png"];
    [imgTabBar addSubview:imgText];    
    
    imgBuddy = [[UIImageView alloc] initWithFrame:CGRectMake(192, 0, 64, 49)];
    imgBuddy.image = [UIImage imageNamed:@"buddy.png"];
    [imgTabBar addSubview:imgBuddy];
    
    imgSetting = [[UIImageView alloc]initWithFrame:CGRectMake(256, 0, 64, 49)];
    imgSetting.image = [UIImage imageNamed:@"settings.jpg"];
    [imgTabBar addSubview:imgSetting];
    
    
    self.imgSignin = [[UIImageView alloc]initWithFrame:CGRectMake(320, 0, 64, 49)];
    self.imgSignin.image = [UIImage imageNamed:@"settings.jpg"];
    [imgTabBar addSubview:self.imgSignin];
    [self.tabBarController.tabBar addSubview:imgTabBar];    
}

#pragma mark - Public

+ (AppDelegate*)shared
{
    return (id)[[UIApplication sharedApplication] delegate];
}


+ (void)checkACC
{
    [[AppDelegate shared] getTwitterAccounts];
}


+ (void)logoutTwitter
{
    [[AppDelegate shared] logOutTwitterAccount];
}


+ (void)switchTwitterAccounts
{
    [[AppDelegate shared] doSelectTwitterAccount];
}


+ (void)startUp
{
    [[AppDelegate shared] startup];;
}



#pragma mark - Twitter

// TWITTER //
- (void)getTwitterAccounts
{
    self.isConnectedToTwitter = NO;
    ACAccountType *twitterAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [self.accountStore requestAccessToAccountsWithType:twitterAccountType options:nil
                                            completion:^(BOOL granted, NSError *error)
     
     {
         if (granted) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 self.twitterAccounts = [[self accountStore] accountsWithAccountType:[self twitterType]];
                 if([self.twitterAccounts count]){
                     BOOL previousAccount = [self checkForPreviousTwitterAccount];
                     if(!previousAccount) {
                         if([self.twitterAccounts count] > 1) {
                             [self doSelectTwitterAccount];
                         }else {
                             self.selectedTwitterAccount = [self.twitterAccounts objectAtIndex:0];
                             NSLog(@"account found");
                             twitterAccountFound = YES;
                             [self setTwitterAccountToDefaults:[self.selectedTwitterAccount accountDescription]];
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"twitterAccountFound" object:self];
                         }
                     }
                 }else {
                     NSLog(@"no account found");
                     twitterAccountFound = NO;
                     [self setTwitterAccountToDefaults:@""];
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"twitterAccountFound" object:self];
                 }
             });
         }else {
             NSLog(@"Access to account store declined");
             twitterAccountFound = NO;
             [self setTwitterAccountToDefaults:@""];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"twitterAccountFound" object:self];
         }
     }];
}


// TWITTER //
- (void)doSelectTwitterAccount
{
    if (self.actionSheet != nil) {
        return;
    }
    actionSheetMode = actionSheetTwitterMode;
    self.actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Twitter Account."
                                                   delegate:(id)self
                                          cancelButtonTitle:nil
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:nil];
    
    for (int i = 0; i < [self.twitterAccounts count]; i++) {
        [self.actionSheet addButtonWithTitle:[[self.twitterAccounts objectAtIndex:i] accountDescription]];
    }
    [self.actionSheet addButtonWithTitle:@"Cancel"];
    [self.actionSheet setCancelButtonIndex:[self.actionSheet numberOfButtons]-1];
    
    [self.actionSheet showInView:self.window];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (actionSheetMode) {
        case actionSheetTwitterMode:
            if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Cancel"]) {
                //user canceled
                twitterAccountFound = NO;
                [self setTwitterAccountToDefaults:@""];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"twitterAccountFound" object:self];
            }else{
                self.selectedTwitterAccount = [self.twitterAccounts objectAtIndex:buttonIndex];
                twitterAccountFound = YES;
                [self setTwitterAccountToDefaults:[self.selectedTwitterAccount accountDescription]];
                //        NSLog(@"%@",[self.account accountDescription]);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"twitterAccountFound" object:self];
            }
            break;
            
        default:
            break;
    }
    actionSheetMode = 0;
}


- (void)logOutTwitterAccount
{
    self.isConnectedToTwitter = NO;
    ACAccountType *accountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [self.accountStore requestAccessToAccountsWithType:accountType options:nil
                                            completion:^(BOOL granted, NSError *error) {
                                                if(granted) {
                                                    ACAccount * twitterAccount = [twitterAccounts objectAtIndex:0];
                                                    if (twitterAccount) {
                                                        [self.accountStore removeAccount:twitterAccount withCompletionHandler:^(BOOL success, NSError *error) {
                                                            if(success){
                                                                NSLog(@"Account Logged Out");
                                                                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey: @"authData"];
                                                                [[NSUserDefaults standardUserDefaults] setObject:@"" forKey: @"twitterAccountName"];
                                                            }else{
                                                                NSLog(@"Err detail: %@",error);
                                                            }
                                                        }];
                                                    }
                                                }
                                            }];
}


// TWITTER //
- (BOOL)isTwitterAccountFound
{
    return twitterAccountFound;
}


// TWITTER //
- (BOOL)isConnectedToTwitter
{
    return connectedToTwitter;
}


// TWITTER //
- (void)setIsConnectedToTwitter:(BOOL)connected
{
    connectedToTwitter = connected;
}


// TWITTER //
- (void)displayTwitterComposerSheet:(NSString *)recipient body:(NSString *)body abId:(NSNumber *)abId imagePath:(NSString *)imagePath view:(id)view title:(NSString *)title
{
    SendTwitterMessageViewController *controller = nil;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480) {
        controller = [[SendTwitterMessageViewController alloc] initWithNibName:@"SendTwitterMessageViewController" bundle:nil];
    }else{
        controller = [[SendTwitterMessageViewController alloc] initWithNibName:@"SendTwitterMessageViewController-568h" bundle:nil];
    }
    controller.recipient = recipient;
    controller.body = body;
    controller.abId = abId;
    controller.imagePath = imagePath;
    controller.titleText = title;
    controller.userDict = userDict;
    controller.nameDict = nameDict;
    
    NSLog(@"xxx1 : %@",userDict);
    NSLog(@"xxx2 : %@",nameDict);
    
    [view presentModalViewController:controller animated:YES];
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"TWITTER_MODAL"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


// TWITTER //
- (BOOL)checkForPreviousTwitterAccount
{
    NSString *previousUserName = [[NSUserDefaults standardUserDefaults] objectForKey:@"twitterAccountName"];
    //    NSLog(@"PREVIOUS ACCOUNT FOUND:%@",previousUserName);
    BOOL returnValue = NO;
    for(int i = 0; i < [self.twitterAccounts count]; i++) {
        if ([[[self.twitterAccounts objectAtIndex:i] accountDescription] isEqualToString:previousUserName]) {
            self.selectedTwitterAccount = [self.twitterAccounts objectAtIndex:i];
            twitterAccountFound = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"twitterAccountFound" object:self];
            returnValue = YES;
        }
    }
    
    return returnValue;
}


// TWITTER //
- (void)setTwitterAccountToDefaults:(NSString *)username
{
    if([username isEqualToString:@""]){
        username = @"Not Logged In";
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"twitterAccountName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (ACAccountStore*)accountStore
{
    if (!self._accountStore) {
        self._accountStore = [[ACAccountStore alloc] init];
    }
    return self._accountStore;
}


-(ACAccountType*)twitterType
{
    if (!self._twitterType) {
        self._twitterType = [[self accountStore] accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    }
    return self._twitterType;
}


- (void)presentBeSafeTextMessage
{
    BOOL canForgetWarning = [[NSUserDefaults standardUserDefaults] boolForKey:@"CAN_FORGET_WARNING"];
    if (!canForgetWarning) {
        //self.isPresentText = YES;
        
        NSString *msg = @"The iPhone version of BeSafe Text does not support sending SMS messages. There is a feature called 'Quick Messages' that saves and sends pre-typed messages.";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"BeSafe Text"
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:@"View Details",@"Hide Warning",nil];
        [[NSUserDefaults standardUserDefaults] setValue:@"Hide Warning" forKey:@"SECOND_BUTTON_TEXT"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [alert show];
        
    }
}


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"BesafeViewLink" object:self];
    }
    if (buttonIndex == 2) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_FORGET_WARNING"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}


// FROM THIS URL:
// http://mobile.tutsplus.com/tutorials/iphone/ios-multitasking-local-notifications/
//

- (void)setLocalNotification
{
    self.timer = [[NSDate date] timeIntervalSinceReferenceDate];
    //willForceSend = NO;
    BOOL aaa = [[NSUserDefaults standardUserDefaults] boolForKey:@"PLAY_PAUSE"];
    if (aaa)
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FORCE_SEND"];
    else
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FORCE_SEND"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (justSetLocalNotification) return;
    DLOG(@"IS SENDING MESSAGE %d",[self isSendingMessage]);
    if ([self isSendingMessage]) {
        justSetLocalNotification = YES;
        
        NSInteger timerSeconds = [[NSUserDefaults standardUserDefaults] integerForKey:@"TIMER_SECONDS"];
        NSInteger timerResume = [[NSUserDefaults standardUserDefaults] integerForKey:@"TIMER_RESUME"];
        NSInteger sendType = [[NSUserDefaults standardUserDefaults] integerForKey:@"SEND_TYPE"];
        NSString *sendAddress = [[NSUserDefaults standardUserDefaults] stringForKey:@"SEND_ADDRESS"];
        
        if (!localNotification) {
            localNotification = [[UILocalNotification alloc] init]; //Create the localNotification object
        }
        
        if (timerSeconds) {
            [localNotification setFireDate:[NSDate dateWithTimeIntervalSinceNow:timerSeconds]]; //Set the date when the alert will be launched using the date adding the time the user selected on the timer
        }
        
        if (timerResume) {
            [localNotification setFireDate:[NSDate dateWithTimeIntervalSinceNow:timerResume]]; //Set the date when the alert will be launched using the date adding the time the user selected on the timer
        }
        
        NSString *alertType = nil;
        NSString *buttonText = nil;
        NSString *sendText = nil;
        // DOUBLE-CHECK THE ALERTTYPE VALUE
        switch (sendType) {
            case typeLike:{
                alertType = @"Post";
                buttonText = @"Like Anyway";
                sendText = @"Like";
                break;
            }
                
            case typeComment:
            {
                alertType = @"Post";
                buttonText = @"Comment Anyway";
                sendText = @"Comment";
                break;
            }
                
            case typePostCheckin:{
                alertType = @"Post";
                buttonText = @"Check in Anyway";
                sendText = @"Check in";
                break;
            }
                
            case typeShare:
            {
                alertType = @"Post";
                buttonText = @"Share Anyway";
                sendText = @"Share";
                break;
            }
                
            case FACEBOOK_POSTING_TYPE:
            {
                alertType = @"Post";
                buttonText = @"Post Anyway";
                sendText = @"Post";
                break;
            }
                
            case FACEBOOK_COMMENT_TYPE:
            {
                alertType = @"Post";
                buttonText = @"Comment Anyway";
                sendText = @"Comment";
                break;
            }
                
            case FACEBOOK_IMAGE_TYPE:
            {
                alertType = @"Post";
                buttonText = @"Post Anyway";
                sendText = @"Post";
                break;
            }
                
            case typeTWRetweet:
            {
                alertType = @"Tweet";
                buttonText = @"Retweet Anyway";
                sendText = @"Retweet";
                break;
            }
                
            case typeTWFavorite:
            {
                alertType = @"Tweet";
                buttonText = @"Favorite Anyway";
                sendText = @"Favorite";
                break;
            }
                
            case RETWEET_TYPE:
            {
                alertType = @"Tweet";
                buttonText = @"Retweet Anyway";
                sendText = @"Retweet";
                break;
            }
                
            case NEWTWEET_TYPE:
            {
                alertType = @"Tweet";
                buttonText = @"Tweet Anyway";
                sendText = @"Tweet";
                break;
            }
                
            case TWITTER_TYPE:
            {
                alertType = @"Tweet";
                buttonText = @"Tweet Anyway";
                sendText = @"Tweet";
                break;
            }
                
            case RETWITTER_TYPE:
            {
                alertType = @"Tweet";
                buttonText = @"Tweet Anyway";
                sendText = @"Tweet";
                break;
            }
                
            case FBcomment:
            {
                alertType = @"Post";
                buttonText = @"Comment Anyway";
                sendText = @"Comment";
                break;
            }
                
            case FBPost:
            {
                alertType = @"Post";
                buttonText = @"Post Anyway";
                sendText = @"Post";
                break;
            }
                
            default:
                break;
        }
        
        [localNotification setAlertAction:buttonText]; //The button's text that launches the application and is shown in the alert
        
        NSString *alertBody;
        if (sendAddress) {
            alertBody = [NSString stringWithFormat:@"BeSafe %@ is ready to %@ this message. Are you sure you want to %@ this message?", alertType, sendText, sendText];
        }else{
            [NSString stringWithFormat:@"BeSafe %@ is ready to %@ this message. Are you sure you want to %@ this message?",alertType, sendText , sendText];
        }
        
        [localNotification setAlertBody:alertBody]; //Set the message in the notification from the textField's text
        [localNotification setHasAction:YES]; //Set that pushing the button will launch the application
        [localNotification setApplicationIconBadgeNumber:1]; //Set the Application Icon Badge Number of the application's icon to the current Application Icon Badge Number plus 1
        [localNotification setRepeatInterval:0]; // 0 means don't repeat
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification]; //Schedule the notification with the system
    }
}


- (NSInteger)isSendingMessage
{
    NSInteger isSend = [[NSUserDefaults standardUserDefaults] integerForKey:@"IS_SENDING_MESSGE"];
    return isSend;
}


/**
 * Convert character string
 *
 * @param res input nsstring
 *
 * @result The NSString after convert
 */
+(NSString *) convertStringFromString :(NSString *) stringFormat
{
    NSString *convertStr1 = [[NSString alloc] initWithString:stringFormat];
    convertStr1 = [convertStr1 stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    
    NSString *convertStr2 = [[NSString alloc] initWithString:convertStr1];
    convertStr2 = [convertStr2 stringByReplacingOccurrencesOfString:@":\"{" withString:@":{"];
    
    NSString *convertStr3 = [[NSString alloc] initWithString:convertStr2];
    convertStr3 = [convertStr3 stringByReplacingOccurrencesOfString:@"}\"}" withString:@"}}"];
    
    return convertStr3;
}


+(NSString *) getFileNameFromURLString:(NSString *)urlStr
{
    int numberOfOccurences = [[urlStr componentsSeparatedByString:@"@"] count];
    
    if (numberOfOccurences == 0)
        return urlStr;
    else
    {
        //request
        NSArray *listURL = [urlStr componentsSeparatedByString:@"@"];
        
        return  [listURL objectAtIndex:[listURL count]-1];
    }
}


@end
