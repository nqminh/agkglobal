//
//  PlaySound.m
//  TaxiDriver
//
//  Created by Thoa Pham on 2/27/13.
//  Copyright (c) 2013 VMODEV. All rights reserved.
//

#import "PlaySound.h"


@implementation PlaySound

+ (void)playSoundWhenReceivePush
{
    SystemSoundID soundID;
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/incomingmms.wav", [[NSBundle mainBundle] resourcePath]]];
    
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &soundID);
    AudioServicesPlaySystemSound (soundID);
}

@end
