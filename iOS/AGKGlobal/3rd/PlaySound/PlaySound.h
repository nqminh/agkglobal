//
//  PlaySound.h
//  TaxiDriver
//
//  Created by Thoa Pham on 2/27/13.
//  Copyright (c) 2013 VMODEV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface PlaySound : NSObject

+ (void)playSoundWhenReceivePush;

@end
