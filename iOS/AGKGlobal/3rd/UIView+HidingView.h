//
//  UIView+HidingView.h
//  UIHidingView
//
//  Created by Roman Barzyczak on 20.07.2013.
//  Copyright (c) 2013 yoman07. All rights reserved.
//

@interface UITabBarController (hidable)

- (void)setTabBarHidden:(BOOL)hidden animated:(BOOL)animated;

@end
