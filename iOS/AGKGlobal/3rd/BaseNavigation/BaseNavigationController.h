//
//  BaseNavigationController.h
//  FanZombie
//
//  Created by Sinbad Flyce on 9/21/12.
//  Copyright (c) 2012 VMODEV. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BaseNavigationBar;

@interface BaseNavigationController : UINavigationController

@property (readonly) BaseNavigationBar *baseNavigationBar;

+ (BaseNavigationController*)createFromNib;
- (void)setRootViewController:(UIViewController*)rootViewController;

@end
