//
//  Extensions.h
//  Betonamugo
//
//  Created by vmodev on 8/14/12.
//  Copyright (c) 2012 VMODEV. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface UINavigationController (Extensions)

- (void)bringUpView:(UIView*)view inTarget:(id)target;
- (void)bringUpViewController:(UIViewController*)viewController;
- (void)pushViewControllerFade:(UIViewController *)viewController animated:(BOOL)animated;
- (void)popViewControllerFade:(BOOL)animated;

@end


@interface UITableViewCell (Extension)

- (UITextField*)fieldTextWithTag:(NSInteger)tag;
- (UILabel*)labelWithTag:(NSInteger)tag;
- (UIButton*)buttonWithTag:(NSInteger)tag;
- (UIImageView*)imageViewWithTag:(NSInteger)tag;
- (UITextView*)textViewWithTag:(NSInteger) tag;
-(UITextField*)textFieldWithTag:(NSInteger) tag;

@end


@interface UIView (Extension)

- (UIButton*)buttonWithTag:(NSInteger)tag;
- (UIImageView*)imageViewWithTag:(NSInteger)tag;
- (UILabel*)labelWithTag:(NSInteger)tag;
- (void)makeBorderRound;
- (void)makeBorderWithColor;
- (void) makeBorderWithColorBlack;

@end

@interface UITextField (Extension)

- (void)standardInset;
- (CGRect)textRectForBounds:(CGRect)bounds;
- (CGRect)editingRectForBounds:(CGRect)bounds;
@end

@interface UIImage (Extension)

-(UIImage*)scaleToSize:(CGSize)size;
-(UIImage*)rotate:(UIImageOrientation)orient;

@end
