//
//  BaseNavigationBar.m
//  FanZombie
//
//  Created by vmodev on 2/12/13.
//  Copyright (c) 2013 vmodev. All rights reserved.
//

#import "BaseNavigationBar.h"

@implementation BaseNavigationBar

@synthesize useBackgroundColor = _useBackgroundColor;
@synthesize useBackgroundImage = _useBackgroundImage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self) {
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    if (_useBackgroundImage) {
        [_useBackgroundImage drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    else if (_useBackgroundColor) {
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColor(context, CGColorGetComponents([_useBackgroundColor CGColor]));
        CGContextFillRect(context, rect);
    }
    else {
        [super drawRect:rect];
    }
}

- (void)setUseBackgroundImage:(UIImage *)useBackgroundImage
{
    [self setBackgroundColor:[UIColor redColor]];
    [_useBackgroundImage release];
    _useBackgroundImage = [useBackgroundImage retain];
    [self setNeedsDisplay];
}

- (UIImage*)useBackgroundImage
{
    return _useBackgroundImage;
}

- (UIColor*)useBackgroundColor
{
    return _useBackgroundColor;
}

- (void)setUseBackgroundColor:(UIColor *)useBackgroundColor
{
    [_useBackgroundColor release];
    _useBackgroundColor = [useBackgroundColor retain];
    [self setNeedsDisplay];
}

- (void)applyDropShadow
{
//    self.layer.shadowColor = [[UIColor whiteColor] CGColor];
//    self.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
//    self.layer.shadowRadius = 1.0f;
//    self.layer.shadowOpacity = 1;
//    self.layer.shadowRadius = 2;
//    
//    self.clipsToBounds = NO;
}

@end


