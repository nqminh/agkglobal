//
//  Extensions.m
//  Betonamugo
//
//  Created by vmodev on 8/14/12.
//  Copyright (c) 2012 VMODEV. All rights reserved.
//

#import "Extensions.h"
#import <QuartzCore/CAAnimation.h>

static CGRect swapWidthAndHeight(CGRect rect)
{
    CGFloat swap = rect.size.width;
    
    rect.size.width = rect.size.height;
    rect.size.height = swap;
    
    return rect;
}

@implementation UINavigationController (Extension)

- (void)bringUpView:(UIView*)aView inTarget:(id)target
{
    CGRect newRect = aView.frame;
    newRect.origin.y = 20;
    
    [aView setFrame:newRect];
    [self.view addSubview:aView];
    [aView setBackgroundColor:[UIColor clearColor]];
    
    
    if ([target respondsToSelector:@selector(willBringUpView)]) {
        [target performSelector:@selector(willBringUpView)];
    }
    
    
    if ([target respondsToSelector:@selector(shouldTapForClosing:)]) {
        
        if ([target respondsToSelector:@selector(view)]) {
            
            UIView *targetView = [target performSelector:@selector(view)];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                            initWithTarget:target
                                            action:@selector(shouldTapForClosing:)];
            
            [tap setCancelsTouchesInView:NO];
            [targetView addGestureRecognizer:tap];
        }
    }
}

- (void)bringUpViewController:(UIViewController*)viewController
{
    CGRect newRect = viewController.view.frame;
    newRect.origin.y = 20;
    
    [viewController.view setFrame:newRect];
    [self.view addSubview:viewController.view];
    [viewController.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.75]];
    [viewController viewWillAppear:NO];
    
    if ([viewController respondsToSelector:@selector(willBringUpView)]) {
        [viewController performSelector:@selector(willBringUpView)];
    }
    
    if ([viewController respondsToSelector:@selector(shouldTapForClosing:)]) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                        initWithTarget:viewController
                                        action:@selector(shouldTapForClosing:)];
        [tap setCancelsTouchesInView:NO];
        [viewController.view addGestureRecognizer:tap];
    }
}

- (void)pushViewControllerFade:(UIViewController *)viewController animated:(BOOL)animated
{
    if (animated) {
        CATransition* transition = [CATransition animation];
        transition.duration = 0.3;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFade;
        [self.view.layer addAnimation:transition
                               forKey:kCATransition];
        [self pushViewController:viewController animated:NO];
    }
    else {
        [self pushViewController:viewController animated:NO];
    }
}

- (void)popViewControllerFade:(BOOL)animated
{
    if (animated) {
        CATransition* transition = [CATransition animation];
        transition.duration = 0.3;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFade;
        [self.view.layer addAnimation:transition
                               forKey:kCATransition];
        [self popViewControllerAnimated:NO];
    }
    else {
        [self popViewControllerAnimated:NO];
    }
}

@end

@implementation UITableViewCell (Extension)

- (UITextField*)fieldTextWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

- (UILabel*)labelWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

- (UIButton*)buttonWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

- (UIImageView*)imageViewWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

-(UITextView*)textViewWithTag:(NSInteger) tag
{
    return (id)[self viewWithTag:tag];
}

-(UITextField*)textFieldWithTag:(NSInteger) tag
{
    return (id)[self viewWithTag:tag];
}

@end


@implementation UIView (Extension)

- (void)makeBorderRound
{
    self.layer.cornerRadius = 14;
}

- (void)makeBorderWithColor
{
    self.layer.borderWidth = 1.0;
    self.layer.cornerRadius = 8;
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

- (void) makeBorderWithColorBlack
{
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [[UIColor blackColor] CGColor];
}

- (UIButton*)buttonWithTag:(NSInteger)tag
{
    return (id)[self viewWithTag:tag];
}

- (UIImageView*)imageViewWithTag:(NSInteger)tag
{
	return (id)[self viewWithTag:tag];
}

- (UILabel*)labelWithTag:(NSInteger)tag
{
	return (id)[self viewWithTag:tag];
}

@end

@implementation UITextField (Extension)

#pragma clang diagnostic push

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectMake(bounds.origin.x + 5, bounds.origin.y + 0, bounds.size.width - 5 * 2, bounds.size.height - 0 * 2);
}
- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self textRectForBounds:bounds];
}

#pragma clang diagnostic pop

- (void)standardInset
{
    CGRect newRect = self.frame;
    newRect.origin.x = 10;
    newRect.origin.y = 0;
    [self textRectForBounds:newRect];
}


@end

#pragma mark - uiimage extension

@implementation UIImage (Extension)

-(UIImage*)scaleToSize:(CGSize)size
{
    // Create a bitmap graphics context
    // This will also set it as the current context
    UIGraphicsBeginImageContext(size);
    
    // Draw the scaled image in the current context
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    // Create a new image from current context
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Pop the current context from the stack
    UIGraphicsEndImageContext();
    
    // Return our new scaled image
    return scaledImage;
}

- (UIImage*)rotate:(UIImageOrientation)orient
{
    CGRect          bnds = CGRectZero;
    UIImage*         copy = nil;
    CGContextRef      ctxt = nil;
    CGImageRef       imag = self.CGImage;
    CGRect          rect = CGRectZero;
    CGAffineTransform tran = CGAffineTransformIdentity;
    
    rect.size.width = CGImageGetWidth(imag);
    rect.size.height = CGImageGetHeight(imag);
    
    bnds = rect;
    
    switch (orient)
    {
        case UIImageOrientationUp:
            // would get you an exact copy of the original
            /*
             assert(false);
             return nil;
             */
            return self;
            
        case UIImageOrientationUpMirrored:
            tran = CGAffineTransformMakeTranslation(rect.size.width, 0.0);
            tran = CGAffineTransformScale(tran, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown:
            tran = CGAffineTransformMakeTranslation(rect.size.width,
                                                    rect.size.height);
            tran = CGAffineTransformRotate(tran, M_PI);
            break;
            
        case UIImageOrientationDownMirrored:
            tran = CGAffineTransformMakeTranslation(0.0, rect.size.height);
            tran = CGAffineTransformScale(tran, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeft:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeTranslation(0.0, rect.size.width);
            tran = CGAffineTransformRotate(tran, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeftMirrored:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeTranslation(rect.size.height,
                                                    rect.size.width);
            tran = CGAffineTransformScale(tran, -1.0, 1.0);
            tran = CGAffineTransformRotate(tran, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRight:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeTranslation(rect.size.height, 0.0);
            tran = CGAffineTransformRotate(tran, M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored:
            bnds = swapWidthAndHeight(bnds);
            tran = CGAffineTransformMakeScale(-1.0, 1.0);
            tran = CGAffineTransformRotate(tran, M_PI / 2.0);
            break;
            
        default:
            // orientation value supplied is invalid
            assert(false);
            return nil;
    }
    
    UIGraphicsBeginImageContext(bnds.size);
    ctxt = UIGraphicsGetCurrentContext();
    
    switch (orient)
    {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextScaleCTM(ctxt, -1.0, 1.0);
            CGContextTranslateCTM(ctxt, -rect.size.height, 0.0);
            break;
            
        default:
            CGContextScaleCTM(ctxt, 1.0, -1.0);
            CGContextTranslateCTM(ctxt, 0.0, -rect.size.height);
            break;
    }
    
    CGContextConcatCTM(ctxt, tran);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), rect, imag);
    
    copy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return copy;
}

@end





