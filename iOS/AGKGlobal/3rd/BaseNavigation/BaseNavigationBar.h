//
//  BaseNavigationBar.h
//  FanZombie
//
//  Created by vmodev on 2/12/13.
//  Copyright (c) 2013 vmodev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationBar : UINavigationBar

@property (retain) UIColor *useBackgroundColor;
@property (retain) UIImage *useBackgroundImage;

- (void)applyDropShadow;

@end
