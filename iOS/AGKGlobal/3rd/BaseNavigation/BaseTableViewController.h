//
//  BaseTableViewController.h
//  FanZombie
//
//  Created by Sinbad Flyce on 5/30/12.
//  Copyright (c) 2012 VMODEV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@class BaseViewController;

@interface BaseTableViewController : BaseViewController 
{
    BOOL enableRefresh;
}

@property (strong) IBOutlet UITableView *tableView;


@property (readonly) BOOL reloading;
@property (assign)   BOOL enableRefresh;

- (BOOL)shouldAllowFresh;
- (void)syncingDoing;


@end
