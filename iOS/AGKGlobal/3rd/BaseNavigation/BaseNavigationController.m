//
//  BaseNavigationController.m
//  FanZombie
//
//  Created by Sinbad Flyce on 9/21/12.
//  Copyright (c) 2012 VMODEV. All rights reserved.
//

#import "BaseNavigationController.h"
#import "BaseNavigationBar.h"

@implementation BaseNavigationController 

@synthesize baseNavigationBar = _baseNavigationBar;

- (NSUInteger) supportedInterfaceOrientations{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        return UIInterfaceOrientationMaskPortrait;
    }else{
        return UIInterfaceOrientationMaskLandscape;
    }
}

+ (BaseNavigationController*)createFromNib
{
    UINib *nib = [UINib nibWithNibName:@"BaseNavigationController" bundle:nil];
    UINavigationController *navController = 
    [[nib instantiateWithOwner:nil options:nil] objectAtIndex:0]; 
    return (id)navController;
}

- (void)setRootViewController:(UIViewController*)rootViewController
{
    [self setViewControllers:[NSArray arrayWithObject:rootViewController]];
}

- (BaseNavigationBar*)baseNavigationBar
{
    return (id)self.navigationBar;
}

@end
