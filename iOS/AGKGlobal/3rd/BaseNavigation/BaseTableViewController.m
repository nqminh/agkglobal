//
//  BaseTableViewController.m
//  FanZombie
//
//  Created by Sinbad Flyce on 5/30/12.
//  Copyright (c) 2012 VMODEV. All rights reserved.
//

#import "BaseTableViewController.h"
#import "BaseViewController.h"

@implementation BaseTableViewController

@synthesize tableView = _tableView;
@synthesize reloading = _reloading;
@synthesize enableRefresh;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAllowFresh
{
    return FALSE;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    enableRefresh = [self shouldAllowFresh];     
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)syncingDoing
{
}

- (BOOL)enableRefresh
{
    return enableRefresh;
}

- (void)setEnableRefresh:(BOOL)aEnableRefresh
{
    enableRefresh = aEnableRefresh;
}

- (void)dealloc
{
    [self setTableView:nil];
    [super dealloc];
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource
{	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
	
}



@end
