//
//  SlideView.h
// 
//
//  Created by Thuy Dao on 8/3/13.
//  Copyright (c) 2013 Thuy Dao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FacebookUtils.h"

@protocol DelegateClickSlideViewButton<NSObject>

- (void)clickSlideViewButtonIndex: (NSInteger) index;
- (void)like:(NSString*)object_id;
- (void)comment:(NSString*)object_id;
- (void)unLike:(NSString*)object_id;
- (void)showView;

@end

@interface SlideView : UIView

@property(strong) IBOutlet UIScrollView             *sv;
@property(nonatomic,strong) NSMutableDictionary     *dataSource;
@property(assign) NSInteger                         height;
@property(nonatomic,strong) NSMutableDictionary     *layoutDict;
@property(nonatomic,assign) BOOL                    isShow;
@property(nonatomic,assign) NSInteger               indexShow;

+ (SlideView*)SlideViewFromNib;
- (void)confiugreWithArr:(NSArray*)arr;
- (void)reload:(NSDictionary*)dict index:(NSInteger)index;
- (void)reloadInfor:(NSDictionary*)dict;
// hide image
-(void)confiugreHideView:(NSArray*)arr;
- (void)hideFooterView;
- (void)showFooterView;


@property (assign) id<DelegateClickSlideViewButton> delegate;

@end
