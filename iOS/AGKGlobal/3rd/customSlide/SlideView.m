//
//  SlideView.m
// 
//
//  Created by Thuy Dao on 8/3/13.
//  Copyright (c) 2013 Thuy Dao. All rights reserved.
//

@interface UIImage (Resize)
- (UIImage*)scaleToSize:(CGSize)size;
@end

// Put this in UIImageResizing.m
@implementation UIImage (Resize)

- (UIImage*)scaleToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, size.width, size.height), self.CGImage);
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

@end

#import "SlideView.h"

@implementation SlideView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return self;
}

+ (SlideView*)SlideViewFromNib
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"SlideView"
                                                   owner:nil options:nil];
    CGSize result = [[UIScreen mainScreen] bounds].size;
    SlideView *SlideView;
    if (result.height == 480) {
        SlideView = (id)[views objectAtIndex:0];
        SlideView.height = 460;//460
    }
    else {
        SlideView = (id)[views objectAtIndex:1];
        SlideView.height = 568;
    }
    
    
  
    [SlideView.sv setFrame:CGRectMake(0, 0, 320, SlideView.height)];
    SlideView.sv.pagingEnabled = YES;
    SlideView.sv.userInteractionEnabled = YES;
    SlideView.sv.backgroundColor = [UIColor blackColor];
    SlideView.layoutDict = [[NSMutableDictionary alloc] init];
    SlideView.indexShow = 0;
        
    return (id)SlideView;
}
#pragma mark - HIDE IMAGE VIEW 
-(void)confiugreHideView:(NSArray*)arr
{
    NSLog(@"slide count remove %d",[self.sv.subviews count]);
    if ([self.sv.subviews count] > 0) {
        for (UIView *v in self.sv.subviews) {
            [v removeFromSuperview];
        }
    }
    
    NSLog(@"slide count add : %d,",arr.count);
    if (self.height == 568) {
        //self.sv.contentSize = CGSizeMake([arr count]* 320, self.height - 20);
        self.sv.contentSize = CGSizeMake([arr count]* 320, self.height);
    }
    else { self.sv.contentSize = CGSizeMake([arr count]* 320, self.height); }
    UIView *view ;
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [arr count]* 320,  self.height)];
    
    for (int i = 0; i < [arr count]; i++)
    {
        UIImageView *imgView = [[UIImageView alloc] init];
        imgView.frame = CGRectMake(320*i, 0, 320,  self.height);
        imgView.tag = i;
        NSDictionary* dict = [arr objectAtIndex:i];
        UIImage *img = [dict objectForKey:@"image"];
        if (img != nil) {
            CGFloat height = img.size.height;
            CGFloat width = img.size.width;
            
            float widthScale = 0;
            float heightScale = 0;
            
            widthScale = 320/width;
            heightScale =  self.height/height;
            
            float scale = MIN(widthScale, heightScale);
            CGSize newSize;
            if (scale == widthScale) {
                newSize = CGSizeMake(320, height*320/width);
            }
            else
            {
                newSize = CGSizeMake(width* self.height/height,  self.height);
            }
            img = [img scaleToSize:newSize];
            [imgView setImage:img];
            NSLog(@"img exit");
        }
        else {
            NSLog(@"img empty");
        }
//        [btn addTarget:self action:@selector(selectBtn:) forControlEvents:UIControlEventTouchUpInside];
//        [btn setAdjustsImageWhenHighlighted:NO];
        [view addSubview:imgView];
    }
    [self.sv addSubview:view];
    
}

#pragma mark - notification

- (UIView*)footerView
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"SlideView"
                                                   owner:nil options:nil];
   return (id)[views objectAtIndex:2];
}

-(void)confiugreWithArr:(NSArray*)arr
{
    self.isShow = YES;
    [self.sv setDelegate:(id)self];
    
    if (self.dataSource == nil) {
        self.dataSource = [[NSMutableDictionary alloc] init];
    }
    [self.dataSource removeAllObjects];
    
    NSLog(@"slide count remove %d",[self.sv.subviews count]);
    if ([self.sv.subviews count] > 0) {
        for (UIView *v in self.sv.subviews) {
            [v removeFromSuperview];
        }
    }
    
    NSLog(@"slide count add : %d,",arr.count);
    if (self.height == 568) {
        //self.sv.contentSize = CGSizeMake([arr count]* 320, self.height - 20);
        self.sv.contentSize = CGSizeMake([arr count]* 320, self.height);
    }
    else { self.sv.contentSize = CGSizeMake([arr count]* 320, self.height); }
    UIView *view ;
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [arr count]* 320,  self.height)];

    for (int i = 0; i < [arr count]; i++)
    {
        UIImageView *imgView = [[UIImageView alloc] init];
        imgView.frame = CGRectMake(320*i, 0, 320,  self.height);
        imgView.tag = i;
        NSDictionary* dict = [arr objectAtIndex:i];
        UIImage *img = [dict objectForKey:@"image"];
        if (img != nil) {
            CGFloat height = img.size.height;
            CGFloat width = img.size.width;
            
            float widthScale = 0;
            float heightScale = 0;
            
            widthScale = 320/width;
            heightScale =  self.height/height;
            
            float scale = MIN(widthScale, heightScale);
//            if (scale < 1) {
                CGSize newSize;
                if (scale == widthScale) {
                    newSize = CGSizeMake(320, height*320/width);
                }
                else
                {
                    newSize = CGSizeMake(width* self.height/height,  self.height);
                }
                img = [img scaleToSize:newSize];
//            }
            [imgView setImage:img];
            NSLog(@"img exit");
        }
        else {
            NSLog(@"img empty");
        }
//        [btn addTarget:self action:@selector(selectBtn:) forControlEvents:UIControlEventTouchUpInside];
//        [imgView setAdjustsImageWhenHighlighted:NO];
        [view addSubview:imgView];
        
        UIView *footer = [self footerView];
        // fix bug #90 "Besafe Post" once posted pic is highlighted .
        // set lai frame cho phu hop khi co cuoc goi den trong khi dang xem anh.
        if (self.height == 568) {
            [footer setFrame:CGRectMake(0, self.height-102, 320, 70)];//(0, self.height-94, 320, 74)
        }
        else
        {
            [footer setFrame:CGRectMake(0, self.height-82, 320, 67)];//(0, self.height-74, 320, 74)
        }
        [footer setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.5]];
        
        NSDictionary *photo = [dict objectForKey:@"photo"];
        
        [self.layoutDict setObject:(id)[footer viewWithTag:1] forKey:[NSString stringWithFormat:@"likeNumber%@",[photo objectForKey:@"fbid"]]];
        [self.layoutDict setObject:(id)[footer viewWithTag:2] forKey:[NSString stringWithFormat:@"likeText%@",[photo objectForKey:@"fbid"]]];
        [self.layoutDict setObject:(id)[footer viewWithTag:3] forKey:[NSString stringWithFormat:@"commentNumber%@",[photo objectForKey:@"fbid"]]];
        [self.layoutDict setObject:(id)[footer viewWithTag:4] forKey:[NSString stringWithFormat:@"commentText%@",[photo objectForKey:@"fbid"]]];
        [self.layoutDict setObject:(id)[footer viewWithTag:5] forKey:[NSString stringWithFormat:@"caption%@",[photo objectForKey:@"fbid"]]];
        
        
        UIButton* btnLike = (UIButton*)[footer viewWithTag:100];
        UIButton* btnComment = (UIButton*)[footer viewWithTag:101];
        
        btnLike.tag =  [arr count]*10 + i;
        [self.dataSource setObject:[photo objectForKey:@"fbid"] forKey:[NSString stringWithFormat:@"%d",[arr count]*10 + i]];
        
        [self.layoutDict setObject:(id)btnLike forKey:[NSString stringWithFormat:@"buttonLike%@",[photo objectForKey:@"fbid"]]];
        
        btnComment.tag = [arr count]*10 +10 + i;
        [self.dataSource setObject:[photo objectForKey:@"fbid"] forKey:[NSString stringWithFormat:@"%d",[arr count]*10 + 10 + i]];
        [btnComment addTarget:self action:@selector(commentPhoto:) forControlEvents:UIControlEventTouchUpInside];
        
        
        footer.tag = 100000;
        [imgView addSubview:footer];
    }
    [self.sv addSubview:view];
}


- (void)selectBtn:(UIButton*)sender
{
    NSLog(@"selected %d",sender.tag);
    if ([_delegate respondsToSelector:@selector(clickSlideViewButtonIndex:)]) {
        [_delegate clickSlideViewButtonIndex:sender.tag];
    }
}

- (void)reload:(NSDictionary*)dict index:(NSInteger)index
{
    if ([self.sv.subviews count] > 0) {
        for (UIView *v in self.sv.subviews) {
            for (UIImageView *imgView in v.subviews ) {
                if (imgView.tag == index) {
                    UIImage *img = [dict objectForKey:@"image"];
                    CGFloat height = img.size.height;
                    CGFloat width = img.size.width;
                    
                    float widthScale = 0;
                    float heightScale = 0;
                    
                    widthScale = 320/width;
                    heightScale =  self.height/height;
                    
                    float scale = MIN(widthScale, heightScale);
//                    if (scale < 1) {
                        CGSize newSize;
                        if (scale == widthScale) {
                            newSize = CGSizeMake(320, height*320/width);
                        }
                        else
                        {
                            newSize = CGSizeMake(width* self.height/height,  self.height);
                        }
                        img = [img scaleToSize:newSize];
//                    }
                    
                    [imgView setImage:img];
                    return;
                }
                else {
                    continue;
                }
            }
        }
    }
}

- (void)reloadInfor:(NSDictionary*)dict
{
    NSString *object_id = [dict objectForKey:@"object_id"];
    NSDictionary* comment_info = [dict objectForKey:@"comment_info"];
    NSDictionary* like_info = [dict objectForKey:@"like_info"];
    NSString *caption = [dict objectForKey:@"caption"];
    NSInteger can_like = [[like_info objectForKey:@"user_likes"] integerValue];
    UIButton* btnLike = (UIButton*)[self.layoutDict objectForKey:[NSString stringWithFormat:@"buttonLike%@",object_id]];
    if (can_like == 0) {
        [btnLike removeTarget:self action:@selector(unlikePhoto:) forControlEvents:UIControlEventTouchUpInside];
         [btnLike addTarget:self action:@selector(likePhoto:) forControlEvents:UIControlEventTouchUpInside];
        [btnLike setBackgroundImage:[UIImage imageNamed:@"spyml_LikeIcon_Normal@2x.png"] forState:UIControlStateNormal];
    }
    else
    {
        [btnLike removeTarget:self action:@selector(likePhoto:) forControlEvents:UIControlEventTouchUpInside];
         [btnLike addTarget:self action:@selector(unlikePhoto:) forControlEvents:UIControlEventTouchUpInside];
        [btnLike setBackgroundImage:[UIImage imageNamed:@"spyml_unLikeIcon_Normal@2x.png"] forState:UIControlStateNormal];
    }
    
    NSInteger likeNumber = [[like_info objectForKey:@"like_count"] integerValue];
    if (likeNumber > 1) {
        ((UILabel*)[self.layoutDict objectForKey:[NSString stringWithFormat:@"likeText%@",object_id]]).text = @"likes";
    }
    else{
        ((UILabel*)[self.layoutDict objectForKey:[NSString stringWithFormat:@"likeText%@",object_id]]).text = @"like";
    }
    
    ((UILabel*)[self.layoutDict objectForKey:[NSString stringWithFormat:@"likeNumber%@",object_id]]).text = [FacebookUtils ConverCount:[NSString stringWithFormat:@"%d",likeNumber]];
    
    
    NSInteger commentNumber = [[comment_info objectForKey:@"comment_count"] integerValue];
    if (commentNumber > 1) {
        ((UILabel*)[self.layoutDict objectForKey:[NSString stringWithFormat:@"commentText%@",object_id]]).text = @"comments";
    }
    else{
        ((UILabel*)[self.layoutDict objectForKey:[NSString stringWithFormat:@"commentText%@",object_id]]).text = @"comment";
    }
    
    ((UILabel*)[self.layoutDict objectForKey:[NSString stringWithFormat:@"commentNumber%@",object_id]]).text = [FacebookUtils ConverCount:[NSString stringWithFormat:@"%d",commentNumber]];
    
    ((UILabel*)[self.layoutDict objectForKey:[NSString stringWithFormat:@"caption%@",object_id]]).text = caption;
    NSLog(@"UIButton Tag: \t %@",object_id);
}

- (void)likePhoto:(UIButton*)sender
{
    NSString* object_id = [self.dataSource objectForKey:[NSString stringWithFormat:@"%d",sender.tag]];
    if (_delegate && [_delegate respondsToSelector:@selector(like:)]) {
        [_delegate like:object_id];
    }
}

- (void)commentPhoto:(UIButton*)sender
{
    NSString* object_id = [self.dataSource objectForKey:[NSString stringWithFormat:@"%d",sender.tag]];
    if (_delegate && [_delegate respondsToSelector:@selector(comment:)]) {
        [_delegate comment:object_id];
    }
}

- (void)unlikePhoto:(UIButton*)sender
{
    NSString* object_id = [self.dataSource objectForKey:[NSString stringWithFormat:@"%d",sender.tag]];
    if (_delegate && [_delegate respondsToSelector:@selector(unLike:)]) {
        [_delegate unLike:object_id];
    }
}

#pragma mark - uiscrollview delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
   DLOG(@"%f",scrollView.contentOffset.x);
    self.indexShow = scrollView.contentOffset.x / 320;
    if (_delegate && [_delegate respondsToSelector:@selector(showView)]) {
        [_delegate showView];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    DLOG(@"%f",scrollView.contentInset.left);
}

#pragma mark - hide/show infor view (footerview)

- (void)showFooterView
{
    if ([self.sv.subviews count] > 0) {
        for (UIView *v in self.sv.subviews) {
            for (UIImageView *imgView in v.subviews ) {
                if (imgView.tag == self.indexShow) {
                    UIView *footerView = [imgView viewWithTag:100000];
                    [footerView setHidden:NO];
                    return;
                }
                else {
                    continue;
                }
            }
        }
    }

}

- (void)hideFooterView
{
    if ([self.sv.subviews count] > 0) {
        for (UIView *v in self.sv.subviews) {
            for (UIImageView *imgView in v.subviews ) {
                if (imgView.tag == self.indexShow) {
                    UIView *footerView = [imgView viewWithTag:100000];
                    [footerView setHidden:YES];
                    return;
                }
                else {
                    continue;
                }
            }
        }
    }

}


@end
