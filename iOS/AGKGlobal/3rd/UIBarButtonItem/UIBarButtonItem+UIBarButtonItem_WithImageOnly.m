//
//  UIBarButtonItem+UIBarButtonItem_WithImageOnly.m
//  AGKGlobal
//
//  Created by Dung on 4/3/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import "UIBarButtonItem+UIBarButtonItem_WithImageOnly.h"

@implementation UIBarButtonItem (UIBarButtonItem_WithImageOnly)

- (id)initWithImageOnly:(UIImage*)image target:(id)target action:(SEL)action;
{
    CGRect frame = CGRectMake(0, 0, image.size.width, image.size.height);
    frame = CGRectInset(frame, -5, 0);
    
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [self initWithCustomView:button];
}

@end
