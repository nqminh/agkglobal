//
//  UIBarButtonItem+UIBarButtonItem_WithImageOnly.h
//  AGKGlobal
//
//  Created by Dung on 4/3/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (UIBarButtonItem_WithImageOnly)

- (id)initWithImageOnly:(UIImage*)image target:(id)target action:(SEL)action;

@end
