//
//  UIViewController+UIViewController_ImageBackButton.h
//  AGKGlobal
//
//  Created by Dung on 4/4/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (UIViewController_ImageBackButton)

- (void)setUpImageBackButton;

@end
