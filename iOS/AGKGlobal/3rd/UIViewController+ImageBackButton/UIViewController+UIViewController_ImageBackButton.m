//
//  UIViewController+UIViewController_ImageBackButton.m
//  AGKGlobal
//
//  Created by Dung on 4/4/14.
//  Copyright (c) 2014 fountaindew. All rights reserved.
//

#import "UIViewController+UIViewController_ImageBackButton.h"

@implementation UIViewController (UIViewController_ImageBackButton)

- (void)setUpImageBackButton
{
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 33, 33)];
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_Arrow.png"] forState:UIControlStateNormal];
    UIBarButtonItem *barBackButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [backButton addTarget:self action:@selector(popCurrentViewController) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = barBackButtonItem;
    self.navigationItem.hidesBackButton = YES;
}

- (void)popCurrentViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
