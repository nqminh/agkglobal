//
//  AudienceViewController.m
//  AGKGlobal
//
//  Created by Thuy on 10/22/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "AudienceViewController.h"
#import "SendFacebookCheckinViewController.h"
@interface AudienceViewController ()

@end

@implementation AudienceViewController
@synthesize audienceActiveDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        DLOG(@"");
        [FacebookUtils addDelegate:self];
        [FacebookUtils getALLFriendsList];
        NSDictionary* Public = [[NSDictionary alloc] initWithObjectsAndKeys:@"Public",@"name",
                                @"AudienceCellPublic@2x.png",@"icon",
                                @"0",@"flid",
                                nil];
        
        audienceActiveDict = [[NSMutableDictionary alloc] initWithDictionary:Public];
        [self reloadAudience];
        
        NSDictionary* Friends = [[NSDictionary alloc] initWithObjectsAndKeys:@"Friends",@"name",
                                 @"AudienceCellFriends@2x.png",@"icon",
                                 @"1",@"flid",
                                 nil];
        
        NSDictionary* Only_Me = [[NSDictionary alloc] initWithObjectsAndKeys:@"Only Me",@"name",
                                 @"AudienceCellOnlyMe@2x.png",@"icon",
                                 @"2",@"flid",
                                 nil];
        
        NSArray *arr = [[NSArray alloc] initWithObjects:Public,Friends,Only_Me, nil];
        dataSourceDict = [[NSMutableDictionary alloc] init];
        [dataSourceDict setObject:arr forKey:@"audience"];
//        [self configureView];
    }
    return self;
}

- (void)configureView
{
    float y = 0;
    CGSize result = [[UIScreen mainScreen] bounds].size;
    
    if (result.height == 480) {
        y = 164; //305
        //
        [self.viewAudience setFrame:CGRectMake(0, y, 320, 35)];
        [self.view addSubview:self.viewAudience];
        //
        //        [self.viewShare setFrame:CGRectMake(0, self.viewAudience.frame.origin.y - 80 , 320, 80)];//-80
        //        [self.view addSubview:self.viewShare];
        
    }
    else {
        y = self.view.frame.size.height -205 ; // -44 250
        //y = 164;
        [self.viewAudience setFrame:CGRectMake(0, y, 320, 36)];
        [self.view addSubview:self.viewAudience];
        //
        //        [self.viewShare setFrame:CGRectMake(0, self.viewAudience.frame.origin.y - 80 , 320, 80)];//-80  160
        //        [self.view addSubview:self.viewShare];
        
    }
    //
    //    [self.postingTextView setFrame:CGRectMake(0, 0, 320,self.viewShare.frame.origin.y  )];
    //    [self.view addSubview:self.postingTextView];
    
    
    [self.myTableView setFrame:CGRectMake(0,200, 320, self.view.frame.size.height - self.viewAudience.frame.origin.y)];
    [self.view addSubview:self.myTableView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickLocation:(id)sender
{
    DLOG(@"");
    SendFacebookCheckinViewController *SVC = [[SendFacebookCheckinViewController alloc] initWithNibName:@"SendFacebookCheckinViewController" bundle:nil];
    [SVC setDelegate:(id)self];
    [SVC setType:typeOther];
    [SVC setTextmsg:self.postingTextView.text];
    [self.navigationController pushViewController:SVC animated:YES];
}

- (IBAction)clickExpand:(id)sender
{
    DLOG(@"");
    [self.myTableView reloadData];
    [self showKeyBroad];
}

#pragma mark - tableview delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    [self.myTableView setBackgroundColor:CELL_COLOR];
    //    return [[dataSourceDict allKeys] count];
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0: {
            NSArray *arr = [dataSourceDict objectForKey:@"audience"];
            return arr.count;
            break;
        }
        case 1: {
            NSArray *arr = [dataSourceDict objectForKey:@"FriendsList"];
            return arr.count;
            break;
        }
        default:
            return 0;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (cell == nil) {
        cell = [self getCellFromTable];
    }
    
    switch (indexPath.section) {
        case 0: {
            NSArray *arr = [dataSourceDict objectForKey:@"audience"];
            NSDictionary* dic = [arr objectAtIndex:indexPath.row];
            [((UIImageView*)[cell viewWithTag:1]) setImage:[UIImage imageNamed:[dic objectForKey:@"icon"]]];
            ((UILabel*)[cell viewWithTag:2]).text = [dic objectForKey:@"name"];
            [cell setBackgroundColor:CELL_COLOR];
            if ([[dic objectForKey:@"name"] isEqualToString:[audienceActiveDict objectForKey:@"name"]]) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            break;
        }
        case 1: {
            NSArray *arr = [dataSourceDict objectForKey:@"FriendsList"];
            NSDictionary* dic = [arr objectAtIndex:indexPath.row];
            [((UIImageView*)[cell viewWithTag:1]) setImage:[UIImage imageNamed:[dic objectForKey:@"icon"]]];
            ((UILabel*)[cell viewWithTag:2]).text = [dic objectForKey:@"name"];
            [cell setBackgroundColor:CELL_COLOR];
            
            if ([[dic objectForKey:@"flid"] isEqualToString:[audienceActiveDict objectForKey:@"flid"]]) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            break;
        }
            
        default:
            break;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = nil;
    if (view == nil) {
        view = [self getHeaderFromTable];
    }
    switch (section) {
        case 0:
            ((UILabel*)[view viewWithTag:1]).text = @"Audience";
            break;
            
        case 1:
            ((UILabel*)[view viewWithTag:1]).text = @"Friends List";
            break;
            
        default:
            break;
    }
    [view setBackgroundColor:HEADER_COLOR];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0: {
            NSMutableArray *arr = [dataSourceDict objectForKey:@"audience"];
            audienceActiveDict = [arr objectAtIndex:indexPath.row];
            [self reloadAudience];
            [self hideKeyBroad];
            break;
        }
            
        case 1:  {
            NSMutableArray *arr = [dataSourceDict objectForKey:@"FriendsList"];
            audienceActiveDict = [arr objectAtIndex:indexPath.row];
            [self reloadAudience];
            [self hideKeyBroad];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - funtion
- (void)reloadAudience
{
    if (audienceActiveDict == nil) {
        return;
    }
    NSString *imageName = [audienceActiveDict objectForKey:@"icon"];
    [self.btnAudience setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}

- (void)hideKeyBroad
{
    [self.postingTextView becomeFirstResponder];
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self.myTableView.hidden = YES;
        self.viewAudience.hidden = NO;
    });
     
    
}

- (void)showKeyBroad
{
    [self.postingTextView resignFirstResponder];
    self.myTableView.hidden = NO;
    self.viewAudience.hidden = YES;
}

#pragma mark - getViewFromNib

- (UIView*)getHeaderFromTable
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"CustomShareView" owner:nil options:nil];
    return [(id) views objectAtIndex:0];
}

- (UITableViewCell*)getCellFromTable
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"CustomShareView" owner:nil options:nil];
    return [(id) views objectAtIndex:1];
}

#pragma mark - facebookUtils delegate

- (void)facebook:(FacebookUtils *)fbU didGetAllFriendListFinish:(NSDictionary *)result
{
    DLOG(@"%@",result);
    NSMutableArray* friendlist = [[NSMutableArray alloc] init];
    NSArray *arr = [result objectForKey:@"data"];
    for (NSMutableDictionary *dic in arr) {
        [dic setObject:@"AudienceCellFriendList@2x.png" forKey:@"icon"];
        [friendlist addObject:dic];
    }
    [dataSourceDict setObject:friendlist forKey:@"FriendsList"];
    [self.myTableView reloadData];
}

- (void)facebook:(FacebookUtils *)fbU didGetAllFriendListFail:(NSError *)error
{
    DLOG(@"%@",[error description]);
}

- (void)selectLocation:(SendFacebookCheckinViewController*)checkin with:(NSDictionary*)dict
{
//    [self.navigationController dismissModalViewControllerAnimated:YES];
    DLOG(@"%@",dict);
}

@end
