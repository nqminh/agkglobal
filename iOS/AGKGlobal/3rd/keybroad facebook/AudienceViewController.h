//
//  AudienceViewController.h
//  AGKGlobal
//
//  Created by Thuy on 10/22/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookUtils.h"
#import "SSTextView.h"


@interface AudienceViewController : UIViewController
{
    NSMutableDictionary* dataSourceDict;
}

@property (nonatomic,retain) IBOutlet UITableView *myTableView;
@property (nonatomic,retain) IBOutlet UIButton    *btnAudience;
@property (nonatomic,retain) IBOutlet UIView      *viewAudience;
@property (nonatomic,retain) IBOutlet SSTextView *postingTextView;
@property (nonatomic,retain) NSMutableDictionary* audienceActiveDict;

- (IBAction)clickExpand:(id)sender;
- (IBAction)clickLocation:(id)sender;
- (void)hideKeyBroad;
- (void)showKeyBroad;

@end
