//
//  Utility.h
//  AGKGlobal
//
//  Created by Thuy Dao on 7/4/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h> 
#import "MBProgressHUD.h"

@interface Utility : NSObject

+ (NSMutableDictionary*) shareProfileDict;

+ (void)saveProfileDict;

+ (void)createCachedFile: (NSMutableArray*)arrID;

+ (NSMutableArray*)getCachedFile;

+ (void) saveCachedFile:(NSMutableArray*) arrID;

+ (NSString*)adjustPhotoURL:(NSString*)photo_url;

+ (NSString*)getStringQueryNewsFeed;

+ (NSString*)getStringCreated_time;

+ (NSString*)getHeadConfigForPost;

+ (NSString*)getEndConfigForPost;

+ (void)setStringCreated_time:(NSString*) created_time;

+ (NSString*)getLastestTime;

+ (void)setLastestTime:(NSString*)time;

+ (NSString*)convertDateMessageFromUnixTime:(NSTimeInterval) unix_time;

+ (NSString*)dateTimeStringForTwitterTime:(NSString*)created_at;

+ (NSString*)dateTimeStringForTwitterTimeToPush:(NSString*)created_at;

+ (NSString*)getArticleLike:(NSString*) postID isLike:(NSInteger) isLike;

+ (NSString*)getArticleShare:(NSString*) postID isShared:(NSInteger)isShared;

#pragma mark - Set color label 

+ (UILabel*) setColorLabel:(UILabel*)label WithText:(NSString*)textStr size:(NSInteger)size;

#pragma mark - Check substring in string 

+ (BOOL)Contains:(NSString *)StrSearchTerm on:(NSString *)StrText;

#pragma mark - Tabbar

+(CGFloat)getYTabbar;

+(CGFloat)getWithTabbar;

#pragma mark - Get time interval 

+ (long) getTimeIntervalSince1970s;

#pragma mark - Get view controller

+(NSString*)getNibNameWithString:(NSString*) nibNameBase;


#pragma mark - Show progress view,alert view

+ (UIView*)frontFromView:(UIView*)aView;
+ (MBProgressHUD*)showProgressInView:(UIView*)view target:(id)aTarget
                               title:(NSString*)titleText selector:(SEL)aSelector;
+ (void) showAlert: (NSString*) msg;


#pragma mar - validate

+ (BOOL) validateEmail:(NSString*)txfText;
+ (BOOL) validatePhoneNumber:(NSString*)txfText;
+ (BOOL) validateFloatNumber:(NSString*)txfText;
+ (BOOL) validateUserName:(NSString*)txfText;
+ (BOOL) validateIntegerNumber:(NSString*)txfText;
+ (BOOL) validateCardNumber:(NSString*)txfText;
+ (BOOL) validatePass:(NSString*)txfText;
+ (BOOL) validatePassContainingNumberAndCharacter:(NSString *)txfText;
+ (BOOL) string:(NSString *)text matches:(NSString *)pattern;
+ (BOOL) validateZipcode:(NSString*)txfText;
+ (BOOL) validateDigitCode:(NSString*)txfText;


#pragma mark - convert text for html

+ (NSString*)convertTextWithSharedLink:(NSString*)msg;

+ (NSString*)convertTextToHTMLText:(NSString*)text;

+ (NSString*)showTextWithCode:(NSString*)text;

+ (NSString*)convertTextInit:(NSString*)text;

+ (NSString *)decodeHTMLEntities:(NSString *)string;

#pragma mark - check bandWord

+ (NSArray *)checkBandWordWithString:(NSString *) str;
+ (BOOL) checkBanned:(NSString*)string withSubString:(NSString*)subString;
+ (NSString*) showBanned:(NSString*)string withArr:(NSArray*)arrBann;

#pragma mark - Utility date time

+ (NSDate*) dateFormStringWithTime: (NSString*) aString;
+ (NSString*) stringFromDatewithTime: (NSDate*) aDate;

+ (NSDate*) dateFormString: (NSString*) aString;
+ (NSString*)stringFromDate:(NSDate*)aDate;
+ (NSString*)stringFullFromDate:(NSDate*)aDate;


#pragma mark - set date formate just now , yesterday ...
+ (NSString*) getFormatDateStringWithDate:(NSString*)date;


# pragma mark - Set frame of Views
+ (void) setFrameOfView:(UIView *)aView withXCordinate:(CGFloat)xCord andYCordinate:(CGFloat)yCord;
+ (void) setFrameOfView:(UIView *)aView withHeightView:(CGFloat)heightView andYCordinate:(CGFloat)yCord;

+ (CGFloat)heightFromString:(NSString*)aString maxWidth:(CGFloat)maxWidth font:(UIFont*)aFont;

+ (CGFloat)widthFromString:(NSString*)aString font:(UIFont*)aFont;

@end

@interface UITableViewCell (Extension)

- (UITextField*)fieldTextWithTag:(NSInteger)tag;
- (UILabel*)labelWithTag:(NSInteger)tag;
- (UIButton*)buttonWithTag:(NSInteger)tag;
- (UIImageView*)imageViewWithTag:(NSInteger)tag;
- (UITextView*)textViewWithTag:(NSInteger) tag;
-(UITextField*)textFieldWithTag:(NSInteger) tag;

@end

@interface NSString (extention)

- (NSString*)deleteAllSpace;

@end