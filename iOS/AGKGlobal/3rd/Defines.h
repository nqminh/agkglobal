

#import <Foundation/Foundation.h>

#define IS_DEBUG 1

#ifdef IS_DEBUG
#define CLEAR(...)          NSLog(@"\n\n\n\n\n\n") 
#define DDBG DBG(@"")
#define DBG(xx) NSLog(@"%p %s (%d): %@", self, __PRETTY_FUNCTION__, __LINE__, xx)
#define DBGRECT(xx) DBG(NSStringFromCGRect(xx))
#define DBGPOINT(xx) DBG(NSStringFromCGPoint(xx))
#define DLOG(xx, ...) NSLog(@"%p %s:Line (%d): %@", self, __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:xx, ##__VA_ARGS__])
#else
#define DDBG
#define DBG(xx)
#define DBGRECT(xx)
#define DBGPOINT(xx)
#define LOG(xx, ...)
#endif

#define testOnePost 0
#define refactorCodeTweet 1
#define BesafeVersion @"1.0"

//color
#define COLORE(r,g,b)       ([UIColor colorWithRed:r/255. green:g/255. blue:b/255. alpha:1.0])

#define HEADER_COLOR        (COLORE(224,228,234))
#define CELL_COLOR          (COLORE(238,240,244))

// post type
#define typePost_Group_created 11
#define typePost_Event_created 12
#define typePost_Status_update 46
#define typePost_Post_on_Timeline_from_another_user 56
#define typePost_changed_profile_picture 60
#define typePost_Tagged_photo 65
#define typePost_Note_created 66
#define typePost_Link_posted_shared_Photo_or_Link 80
#define typePost_Video_posted 128
#define typePost_Likes 161
#define typePost_Likes_a_photo_or_Link 245
#define typePost_Photos_posted 247
#define typePost_App_or_Games_story 237
#define typePost_Comment_created 257
#define typePost_App_story 272
#define typePost_Likes_a_Link 283
#define typePost_Checkin_to_a_place 285
#define typePost_Post_to_Timeline_of_a_friend_from_other_friend 295
#define typePost_Post_in_Group 308 // can hide
#define Likes_a_Link1 347
#define updated_cover_photo 373
#define typePost_event 279

// text
#define textUndo @"This post will no longer appear in your"
#define newsfeed @" News Feed."

//#define textPriavcyContent @"Did you know that when you \"%@\" certain pages, such as business and corporate facebook pages, you may give that organization access to your list of friend, and they can use your personal information for advertizing purposes."

//-- text privacty twitter
//#define textTwitterPrivacy @"Did you know that when you \"%@\" certain pages, such as business and corporate twitter pages, you may give that organization access to your list of friend, and they can use your personal information for advertizing purposes."

#define textRetweetPrivacy @"When you \"%@\" a post it’s immediately delivered via SMS and APIs to a wide range of users and services such as the U.S. Library of Congress, which archives Tweets for historical purposes. Are you sure you want to \"%@\" this?"

#define textFavoritePrivacy @"When you \"%@\" a tweet it’s immediately delivered via SMS and APIs to a wide range of users and services such as the U.S. Library of Congress, which archives Tweets for historical purposes. Are you sure you want to \"%@\" this tweet?"

//-- text privacy post
#define textPostPrivacy @"A new digital initiative will soon collect and distribute everything you \"%@\" on Facebook, which will be archived with the U.S. Library of Congress for historical purposes. Are you sure you want to \"%@\" this?"

#define rateLimit @"You have made more requests than system allowed. Please try again in a few minutes."
#define TimeLinetext @" Timeline"

#define isUseUpdateTime

// key
#define facebookID                      @"facebookID"
#define facebookName                    @"facebookName"
#define facebookUrl                     @"facebookUrl"

//#define warningLike @"warningLike"
//#define warningUnlike @"warningUnlike"
//#define warningComment @"warningComment"
//#define warningShare @"warningShare"
#define warningControllTwitter          @"warningControllTwitter"
#define warningControllFacebook         @"warningControllFacebook"
#define keyAllComment @"keyAllComment"
#define appid @"539623596100993"




#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#define PROFILE_TWITTER_URL @"PROFILE_TWITTER_URL"

#pragma mark - Notification 

#define AUTO_REFRESH_NEWFEED_DATA    @"AutoRefreshNewsFeedData"
#define SAVE_BESAFE_BUDDY            @"SAVE_BESAFE_BUDDY"
#define TOKEN_DEVICE                 @"TOKEN_DEVICE"
#define IPAD3_DEVICE_TOKEN           @"6d66f3b54a50b21ba48d9d9cb1af87f915fb6111c2b5416f21b632d7ebf5eda6"
#define ONLY_CLICK_BUDDY             @"ONLY_CLICK_BUDDY" 
#define RECEIVE_NOTIFICATION         @"RECEIVE_NOTIFICATION" 
#define ARR_BUDDY_LOGIN              @"ARR_BUDDY_LOGIN"  

#define BESAFE_USER_DEFAULT         @"101" 
#define BESAFE_USER_ID_LOGIN        @"BESAFE_USER_ID_LOGIN" 
#define BESAFE_USER_FB_LOGIN        @"BESAFE_USER_FB_LOGIN" 

#pragma mark - Message  

#define MESSAGE_SEND_BUDDY          @"The post has been to send to buddy for review"

#pragma mark - Param post feed 
#define STT_DATE_POST                   @"date_post"
#define STT_PRIVACY_POST                @"privacy"
#define STT_POST_TYPE                   @"post_type" 
#define STT_MESSAGE_POST                @"status"
#define STT_CAPTION_PHOTO               @"caption" 
#define STT_SOURCE_PHOTO                @"source" 
#define STT_PLACE_CHECKIN               @"address" 

#pragma mark - Notify Login Facebook 

#define NOTI_SIGNIN_FACEBOOK_WEBVIEW    @"NOTI_SIGNIN_FACEBOOK_WEBVIEW"
#define TWITTER_ID_STR                  @"TWITTER_ID_STR"


 

















