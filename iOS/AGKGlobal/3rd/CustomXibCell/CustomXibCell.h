
#import <UIKit/UIKit.h>

#define ROUTE(x) [self routeAction:_cmd fromObject:x]

@interface CustomXibCell : UITableViewCell

+ (id)cellForTableView:(UITableView *)tv target:(id)target forRow:(NSInteger)row;
+ (id)cellForTableView:(UITableView *)tv target:(id)target;
+ (void)route:(UITableView *)tv target:(id)target forCell:(id)cell;

- (void)routeAction:(SEL)act fromObject:(id)obj;
- (void)dispatchMessage:(SEL)msg toObject:(id)obj fromObject:(UIControl *)ctl;

@end
