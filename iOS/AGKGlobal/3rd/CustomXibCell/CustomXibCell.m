
#import "CustomXibCell.h"
//#import "MarqueeLabel.h"

@interface CustomXibCell ()
@property (nonatomic, weak) UITableView *currentTableView;
@property (nonatomic, weak) id actionTarget;
@end

@implementation CustomXibCell

+ (id)cellForTableView:(UITableView *)tv target:(id)target forRow:(NSInteger)row
{
    NSString *className = NSStringFromClass([self class]);
    CustomXibCell *cell = [tv dequeueReusableCellWithIdentifier:className];
    if(!cell) {
        /*
        UINib *nib = [UINib nibWithNibName:className bundle:[NSBundle mainBundle]];
        if(nib)
            [tv registerNib:nib forCellReuseIdentifier:className];
        else
            [tv registerClass:self forCellReuseIdentifier:className];
        
        cell = [tv dequeueReusableCellWithIdentifier:className];
         */
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
        cell = [topLevelObjects objectAtIndex:row];
    }
    
    [cell setActionTarget:target];
    [cell setCurrentTableView:tv];
    
    return cell;
}

+ (void)route:(UITableView *)tv target:(id)target forCell:(id)cell
{
    [cell setActionTarget:target];
    [cell setCurrentTableView:tv];
}

+ (id)cellForTableView:(UITableView *)tv target:(id)target
{
    NSString *className = NSStringFromClass([self class]);
    CustomXibCell *cell = [tv dequeueReusableCellWithIdentifier:className];
    if(!cell) {

         UINib *nib = [UINib nibWithNibName:className bundle:[NSBundle mainBundle]];
         if(nib)
         [tv registerNib:nib forCellReuseIdentifier:className];
         else
         [tv registerClass:self forCellReuseIdentifier:className];
         
         cell = [tv dequeueReusableCellWithIdentifier:className];
    }
    
    [cell setActionTarget:target];
    [cell setCurrentTableView:tv];
    
    return cell;
}

- (void)routeAction:(SEL)act fromObject:(id)obj
{
	[self dispatchMessage:act toObject:[self actionTarget] fromObject:obj];
}

- (void)dispatchMessage:(SEL)msg toObject:(id)obj fromObject:(UIControl *)ctl
{
	SEL newSel = NSSelectorFromString([NSStringFromSelector(msg) stringByAppendingFormat:@"atIndexPath:"]);
	NSIndexPath *ip = [[self currentTableView] indexPathForCell:self];
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
	[obj performSelector:newSel withObject:ctl withObject:ip];
    #pragma clang diagnostic pop
}
@end
