 //
//  ImageViewObj.m
//  AGKGlobal
//
//  Created by Dung on 10/1/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "ImageViewObj.h"
#define ZOOM_STEP 1.5
#import "AppDelegate.h"

@implementation ImageViewObj
@synthesize lblLikeText,lblCommentText,lblTitle,lblLikeNumber,lblCommentNumber;
@synthesize dataSource,imageViewObj;
@synthesize scrollImageObj,btnDone,btnLike,btnShare;
@synthesize isDoubletap,isLike,isTapOnce;
@synthesize fouterView;

+ (ImageViewObj*) imgViewCreateFromNib
{
    
    ImageViewObj *obj;
    if (isIphone5) {
        NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"ImageViewObj-568h"
                                                       owner:nil options:nil];

        obj = (id)[views objectAtIndex:0];
    }
    else{
        NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"ImageViewObj"
                                                       owner:nil options:nil];

        obj = (id)[views objectAtIndex:0];
    }
    obj.backgroundColor = [UIColor blackColor];
    [FacebookUtils addDelegate:obj];
    [obj addGestureRecognizertoImage:obj];
    obj.isDoubletap = FALSE;
    //[obj addButtonDone];
    return obj;
    
}


- (void) addGestureRecognizertoImage:(ImageViewObj*)imgObj
{   
    
    // add gesture recognizers to the image view   
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];
        
    [doubleTap setNumberOfTapsRequired:2];
    [twoFingerTap setNumberOfTouchesRequired:2];    
    
    [imgObj addGestureRecognizer:doubleTap];
    [imgObj addGestureRecognizer:twoFingerTap];
    
    // calculate minimum scale to perfectly fit image width, and begin at that scale
    [scrollImageObj setMinimumZoomScale:1.0];
    [scrollImageObj setZoomScale:1.0];
    [scrollImageObj setMaximumZoomScale:10.0];
    //
    imgObj.isTapOnce = FALSE;
    
}


-(void) addButtonDone
{
    //btnDone = [[UIButton alloc] initWithFrame:CGRectMake(250, 20, 60, 25)];
    
    [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    btnDone.titleLabel.font = [UIFont boldSystemFontOfSize: 14];
    [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(clickDone) forControlEvents:UIControlEventTouchUpInside];
    //[btnDone setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.5]];
    btnDone.tag = 1001;
    [self addSubview:btnDone];
}



#pragma mark - funtion

- (void)setFrameFooterView:(CGRect)frame
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.fouterView setFrame:frame];
    });
}


- (void)showImage:(UIImage*)img
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.imageViewObj setImage:img];
    });
}


- (void)setAvatar:(NSString*)url
{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        UIImage* myImage = [UIImage imageWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString: url]]];
        
            dispatch_sync(dispatch_get_main_queue(), ^{
                               
               // [self.imageViewObj setImage:myImage];
                CGSize result = [[UIScreen mainScreen] bounds].size;
                if (myImage != nil) {
                    CGFloat height = myImage.size.height;
                    CGFloat width = myImage.size.width;
                    
                    float widthScale = 0;
                    float heightScale = 0;
                    
                    widthScale = 320/width;
                    heightScale =  result.height/height;
                    
                    float scale = MIN(widthScale, heightScale);
                    CGSize newSize;
                    CGPoint newPoint ;
                    float y1 = (result.height - (height*320/width))/2 - 30;
                    if (scale == widthScale) {
                        newSize = CGSizeMake(320, height*320/width);
                        newPoint = CGPointMake(0, y1);
                    }
                    else
                    {
                        newSize = CGSizeMake(width* result.height/height,  result.height);
                        newPoint = CGPointMake((320 - (width* result.height/height))/2,0 );
                    }
                    //img = [img scaleToSize:newSize];
                    CGRect frame = imageViewObj.frame;
                    frame.size= newSize;
                    frame.origin = newPoint;                    
                    imageViewObj.frame = frame;        
                    [self.imageViewObj setImage:myImage];
                }

                self.myImg = myImage;
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
    });
}


- (void)requestInfo
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [FacebookUtils getDetailPhoto:self.object_id];
        //[FacebookUtils getDetailPhoto:pid];
    });
}


- (void)loadPhoto
{
    UIImage *img = [self.dataSource objectForKey:@"image"];
    if (img != nil) {
        [self showImage:img];
    }
    else {
        self.pid = [[self.dataSource objectForKey:@"photo"] objectForKey:@"pid"];//fbid
        self.object_id = [[self.dataSource objectForKey:@"photo"] objectForKey:@"fbid"];//pid
        //facebook request infor
        [self performSelector:@selector(requestInfo) withObject:nil afterDelay:0.5];        
        
        // download photo
        NSMutableArray *arr = [[self.dataSource objectForKey:@"photo"] objectForKey:@"images"];
        NSString *url = @"";
        if (arr.count == 0) {
            url = [self.dataSource objectForKey:@"src"];
        }
        else {
            if ([arr count] > 1) url = [[arr objectAtIndex:1] objectForKey:@"src"];
            else url = [[arr objectAtIndex:0] objectForKey:@"src"];
        }
        
        url = [url stringByReplacingOccurrencesOfString:@"_s." withString:@"_n."];
        [self setAvatar:url];
    }
}


- (UIView*) footerViewFromNib
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"ImageViewObj"
                                                   owner:nil options:nil];
    return (id)[views objectAtIndex:1];
}


- (void)reload:(NSDictionary*)dict index:(NSInteger)index
{
    CGSize result = [[UIScreen mainScreen] bounds].size;    
    if ([self.subviews count] > 0) {
        for (UIView *v in self.subviews) {
            for (UIImageView *imgView in v.subviews ) {
                if (imgView.tag == index) {
                    UIImage *img = [dict objectForKey:@"image"];
                    CGFloat height = img.size.height;
                    CGFloat width = img.size.width;
                    
                    float widthScale = 0;
                    float heightScale = 0;
                    
                    widthScale = 320/width;
                    heightScale =  result.height/height;
                    
                    float scale = MIN(widthScale, heightScale);
                    //                    if (scale < 1) {
                    CGSize newSize;
                    if (scale == widthScale) {
                        newSize = CGSizeMake(320, height*320/width);
                    }
                    else
                    {
                        newSize = CGSizeMake(width* result.height/height,  result.height);
                    }
                                   
                    
                    [imgView setImage:img];
                    return;
                }
                else {
                    continue;
                }
            }
        }
    }    
}


- (void)reloadInfor:(NSDictionary*)dict
{
//    [self.lblLikeText setText:@"hehe"];
    NSString *object_id = [dict objectForKey:@"object_id"];
    NSDictionary* comment_info = [dict objectForKey:@"comment_info"];
    NSDictionary* like_info = [dict objectForKey:@"like_info"];
    NSString *caption = [dict objectForKey:@"caption"];
    NSInteger can_like = [[like_info objectForKey:@"user_likes"] integerValue];
    
    if (can_like == 0) {
        [self.btnLike setBackgroundImage:[UIImage imageNamed:@"spyml_LikeIcon_Normal@2x.png"] forState:UIControlStateNormal];
        self.isLike = TRUE;
    }
    else
    {
        [self.btnLike setBackgroundImage:[UIImage imageNamed:@"spyml_unLikeIcon_Normal@2x.png"] forState:UIControlStateNormal];
        self.isLike = FALSE;
    }
    
    NSInteger likeNumber = [[like_info objectForKey:@"like_count"] integerValue];
    if (likeNumber > 1) {
        
        self.lblLikeText.text = @"likes";
    }
    else{
        
        self.lblLikeText.text = @"like";
    }
    
        self.lblLikeNumber.text = [FacebookUtils ConverCount:[NSString stringWithFormat:@"%d",likeNumber]];
    
 
    NSInteger commentNumber = [[comment_info objectForKey:@"comment_count"] integerValue];
    if (commentNumber > 1) {
        self.lblCommentText.text = @"comments";
    }
    else{
       self.lblCommentText.text = @"comment";
    }
    
    self.lblCommentNumber.text = [FacebookUtils ConverCount:[NSString stringWithFormat:@"%d",commentNumber]];
    
    self.lblTitle.text = caption;
    NSLog(@"UIButton Tag: \t %@",object_id);
}



#pragma mark - IBAction

- (IBAction)clickLike:(id)sender
{
    DLOG(@"like click : %@",self.object_id);
    if (self.isLike) {
        if (_delegate && [_delegate respondsToSelector:@selector(likePhoto:)]) {
            [_delegate likePhoto:self.object_id];
        }
    }
    else
    {
        if (_delegate && [_delegate respondsToSelector:@selector(unlikePhoto:)]) {
            [_delegate unlikePhoto:self.object_id];
        }
    }
   
}


- (IBAction)clickComment:(id)sender
{
    DLOG(@"comment click : %@",self.object_id);
    if (_delegate && [_delegate respondsToSelector:@selector(commentPhoto:image:)]) {
        [_delegate commentPhoto:self.object_id image:self.myImg];
    }
}


- (IBAction)clickShare:(id)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(sharePostFromSlidePhoto)]) {
        [_delegate sharePostFromSlidePhoto];
    }    
}



#pragma mark - facebookutil delegate

- (void)facebook:(FacebookUtils *)fbU didGetDetailPhotoFinish:(NSDictionary *)result
{
    NSLog(@"didGetDetailPhotoFinish: \n\t result: \t %@",result);
    NSMutableArray *arr = [result objectForKey:@"data"];
    if (arr.count == 0) return;
    NSDictionary*dict = [arr objectAtIndex:0];
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([[dict objectForKey:@"pid"] isEqualToString:self.pid] ) {
            [self reloadInfor:dict];
        }
    });
}


- (void)facebook:(FacebookUtils *)fbU didGetDetailPhotoFail:(NSError *)error
{
    NSLog(@"didGetDetailPhotoFail error : %@",[error description]);
}


- (void)facebook:(FacebookUtils *)fbU didLikeFinish:(NSDictionary *)result
{
    NSLog(@"ImageViewController didLikeFinish");
    DLOG(@"");
   [FacebookUtils getDetailPhoto:self.object_id];
}


- (void)facebook:(FacebookUtils *)fbU didLikeFail:(NSError *)error
{
    NSLog(@"didLikeFail error : %@",[error description]);
}


- (void)facebook:(FacebookUtils *)fbU didCommentFinish:(NSDictionary *)result
{
    NSLog(@"ImageViewController didCommentFinish");
    [FacebookUtils getDetailPhoto:self.object_id];
}


- (void)facebook:(FacebookUtils *)fbU didCommentFail:(NSError *)error
{
    NSLog(@"didCommentFail error : %@",[error description]);    
}



#pragma mark UIScrollViewDelegate methods

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
          
    return imageViewObj;
    
}


- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    //image.frame = [self centeredFrameForScrollView:scrollView andUIView:image];;
    DLOG(@"bat dau zoom");
    if (_delegate && [_delegate respondsToSelector:@selector(hideButtonDone)]) {
        [_delegate hideButtonDone];
    }
    self.fouterView.hidden = YES;    
    
}


- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
    
    [scrollView setZoomScale:scale+0.01 animated:NO];
    [scrollView setZoomScale:scale animated:NO];    
}



#pragma mark TapDetectingImageViewDelegate methods
/*
- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
    // single tap does nothing for now
    //ImageViewObj *imgObj = (ImageViewObj*)[arrObj objectAtIndex:self.indexShow];
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        DLOG(@"UIGestureRecognizerStateEnded");
        if (self.isTapOnce ) {
            self.fouterView.hidden = NO;
            btnDone.hidden = NO;
            viewDone.hidden = NO;
            isTapOnce = FALSE;
         }
        else {
            self.fouterView.hidden = YES;
            btnDone.hidden = YES;
            viewDone.hidden = YES;
            isTapOnce = YES;
            //[self addButtonDone];
        
        }
    }
    
 
}
*/


 - (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
 // double tap zooms in
     if (isDoubletap) {
         float newScale = 1.0;
         CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
         [scrollImageObj zoomToRect:zoomRect animated:YES];
         isDoubletap = FALSE;
         self.fouterView.hidden = TRUE;
         if (_delegate && [_delegate respondsToSelector:@selector(hideButtonDone)]) {
             [_delegate hideButtonDone];
         }
        }else{
         float newScale = 3.0;
         CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
         [scrollImageObj zoomToRect:zoomRect animated:YES];
         isDoubletap = TRUE;
         self.fouterView.hidden = TRUE;
         if (_delegate && [_delegate respondsToSelector:@selector(hideButtonDone)]) {
             [_delegate hideButtonDone];
         }

     }
 
 }
 

- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    // two-finger tap zooms out
    float newScale = [scrollImageObj zoomScale] / ZOOM_STEP;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [scrollImageObj zoomToRect:zoomRect animated:YES];
}



#pragma mark Utility methods

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center
{    
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates.
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = [scrollImageObj frame].size.height / scale;
    zoomRect.size.width  = [scrollImageObj frame].size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    NSLog(@"\n\nzoomRect.origin.x %f \n\n",zoomRect.origin.x);
    
    return zoomRect;    
}



@end
