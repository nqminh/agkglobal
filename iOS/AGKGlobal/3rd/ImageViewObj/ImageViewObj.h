//
//  ImageViewObj.h
//  AGKGlobal
//
//  Created by Dung on 10/1/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookUtils.h"

@protocol DelegateImageViewObject<NSObject>

- (void)commentPhoto:(NSString*)object_id image:(UIImage*)image;
- (void)hideButtonDone;
- (void)sharePostFromSlidePhoto;
- (void)likePhoto:(NSString*)object_id;
- (void)unlikePhoto:(NSString*)object_id;

@end

@interface ImageViewObj : UIView <UIScrollViewDelegate>


// property .
@property (weak, nonatomic) IBOutlet UIImageView *imageViewObj;

@property (weak, nonatomic) IBOutlet UILabel *lblLikeNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeText;

@property (nonatomic) IBOutlet UIView *fouterView;

@property (weak, nonatomic) IBOutlet UILabel *lblCommentNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblCommentText;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollImageObj;
//@property (strong,nonatomic) UIButton               *btnDone;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;

@property (weak, nonatomic) IBOutlet UIButton *btnShare;


@property(nonatomic,assign) BOOL                    isTapOnce;
@property(nonatomic,assign) BOOL                    isDoubletap;
@property (weak, nonatomic) UIImage *myImg;
@property (weak, nonatomic) NSString *pid;
@property (weak, nonatomic) NSString *object_id;
@property (nonatomic) BOOL isLike;

@property (assign) id<DelegateImageViewObject> delegate;

@property(nonatomic,strong) NSMutableDictionary     *dataSource;
//@property(assign) NSInteger                         height;

// action
+ (ImageViewObj*) imgViewCreateFromNib;
- (UIView*) footerViewFromNib;
- (void)reload:(NSDictionary*)dict index:(NSInteger)index;
- (void)reloadInfor:(NSDictionary*)dict;
- (void)showImage:(UIImage*)img;
- (void)loadPhoto;
- (void)setFrameFooterView:(CGRect)frame;

- (IBAction)clickLike:(id)sender;
- (IBAction)clickComment:(id)sender;
- (IBAction)clickShare:(id)sender;


@end
