//
//  RegisterViewController.m
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 10/10/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "RegisterViewController.h"
#import "AppDelegate.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

@synthesize txtEmail,txtPassword,txtBirthYear,txtZipcode, txtGender;
@synthesize scrollView,viewGender,tableView;
@synthesize arrayGender;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        //self.title = NSLocalizedString(@"Register", @"Register");
        self.tabBarItem.image = [UIImage imageNamed:@"tab_6_setting"];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(twitterAccountFound) name:@"accountFound" object:nil];
    
    return self;
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    isChecked = NO;
    
    arrayGender = [[NSArray alloc] initWithObjects:@"Male",@"Female", nil];
    
    viewGender.alpha = 0;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480)
    {
        scrollView.frame = CGRectMake(0, 0, 320, 518);
        scrollView.contentSize = CGSizeMake(320, 580);
    }
    else
        scrollView.frame = CGRectMake(0, 0, 320, 600);
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [tap setCancelsTouchesInView:NO];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated
{
    [self.tabBarController setTabBarHidden:YES animated:NO];
    self.navigationController.navigationBarHidden = YES;
    
    txtEmail.text = @"";
    txtPassword.text = @"";
    txtZipcode.text = @"";
    txtBirthYear.text = @"";
    txtGender.text = @"";
    
    [super viewWillAppear:animated];
}



#pragma mark - ACTION

- (IBAction)clickToBtnCheckBox:(id)sender
{
    if (btnCheckBox.imageView.image == [UIImage imageNamed:@"btn_untick.png"]) {
        [btnCheckBox setImage:[UIImage imageNamed:@"btn_tick.png"] forState:UIControlStateNormal];
        
        isChecked = YES;
    }else{
        [btnCheckBox setImage:[UIImage imageNamed:@"btn_untick.png"] forState:UIControlStateNormal];
    }
}


- (IBAction)clickToBtnTerms:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    TermsPolicyViewController *tpVC = [[TermsPolicyViewController alloc] initWithNibName:@"TermsPolicyViewController" bundle:nil];
    
    appDelegate.isShowText = YES;
    
    [self.navigationController presentViewController:tpVC animated:YES completion:nil];
}


- (IBAction)clickToBtnGoBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)clickToBtnRegister:(id)sender
{
    //--validate textfields
    if([self validateTextField])
    {
                
        if ([self validateTextField] == YES)
        {
            if ([self.txtFullName.text length] == 0)
            {
                [txtEmail becomeFirstResponder];
                
                //-- show message when login failed
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Fullname is required" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
                
                return;
            }
            
            //-- validate email
            BOOL isValidEmail = [Utility validateEmail:txtEmail.text];            
            if (!isValidEmail){
                
                //--beginning editting textfield email
                [txtEmail becomeFirstResponder];
                
                //-- show message when login failed
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"The email not valid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
                
                return;
            }
            
            //-- validate pass
            if ([txtPassword.text length]<6)
            {
                //--beginning editting textfield password
                [txtPassword becomeFirstResponder];
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Password should be atleast 6 characters." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
                
                return;
            }
            
            //-- validate zipcode
            BOOL isValidZipcode = [Utility validateZipcode:txtZipcode.text];
            
            if (!isValidZipcode) {
                
                //--beginning editting textfield email
                [txtZipcode becomeFirstResponder];
                
                //-- show message when login failed
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"The zipcode not valid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
                
                return;
            }
            
            //-- validate zipcode
            BOOL isValidBirthYear = [Utility validateIntegerNumber:txtBirthYear.text];
            
            if (!isValidBirthYear) {
                
                //--beginning editting textfield email
                [txtBirthYear becomeFirstResponder];
                
                //-- show message when login failed
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"The birthyear not valid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
                
                return;
            }            
        }
        
        if (isChecked) {
            [self showProgressInView:self.view target:self title:@"Registering..." selector:@selector(registerAPI:)];
        }else{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Please click to checkbox to register" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [alertView show];
        } 
    }
}

- (IBAction)clickPatentPending:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    TermsPolicyViewController *tpVC = [[TermsPolicyViewController alloc] initWithNibName:@"TermsPolicyViewController" bundle:nil];
    
    appDelegate.isShowLink = YES;
    
    [self.navigationController pushViewController:tpVC animated:YES];
}


-(void)dismissKeyboard
{
    [txtEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
    [txtZipcode resignFirstResponder];
    [txtBirthYear resignFirstResponder];
    [txtGender resignFirstResponder];
    
    viewGender.alpha = 0;
}



#pragma mark - Private methods

- (BOOL) validateTextField
{
    int check = 0;
    if ([self.txtFullName.text length] == 0) {
        [self.txtFullName setText:@""];
        [btnFullname setImage:[UIImage imageNamed:@"btn_warning.png"] forState:UIControlStateNormal];
        check = 1;
    }
    else
    {
        btnFullname.imageView.image = nil;
    }

    
    if ([txtEmail.text length] == 0) {
        [txtEmail setText:@""];
        [btnEmail setImage:[UIImage imageNamed:@"btn_warning.png"] forState:UIControlStateNormal];
        check = 1;
    }else{
        btnEmail.imageView.image = nil;
    }
//    else {
//        [[FanZombieUtility sharedCreateAccount] setValue:txtEmail.text forKey:Account_email];
//    }
    
    if ([txtPassword.text length] == 0) {
        [txtPassword setText:@""];
        [btnPassword setImage:[UIImage imageNamed:@"btn_warning.png"] forState:UIControlStateNormal];
        check = 1;
    }else{
        btnPassword.imageView.image = nil;
    }

    if ([txtZipcode.text length] == 0) {
        [txtZipcode setText:@""];
        [btnZipcode setImage:[UIImage imageNamed:@"btn_warning.png"] forState:UIControlStateNormal];
        check = 1;
    }else{
        btnZipcode.imageView.image = nil;
    }
    
    if ([txtBirthYear.text length] == 0) {
        [txtBirthYear setText:@""];
        [btnBirthYear setImage:[UIImage imageNamed:@"btn_warning.png"] forState:UIControlStateNormal];
        check = 1;
    }else{
        btnBirthYear.imageView.image = nil;
    }
    
    if ([txtGender.text length] == 0) {
        [txtGender setText:@""];
        [btnGender setImage:[UIImage imageNamed:@"btn_warning.png"] forState:UIControlStateNormal];
        check = 1;
    }else{
        btnGender.imageView.image = nil;
    }
    
    if (check == 0) {
        return YES;
    }
    else {
        return NO;
    }
}


- (UIView*)frontFromView:(UIView*)aView
{
    UIWindow* tempWindow = nil;
    UIView *keyboard = nil;
    
    if ([[[UIApplication sharedApplication] windows] count] > 1) {
        tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    }
    
    if ([tempWindow.subviews count])
        keyboard = [tempWindow.subviews objectAtIndex:0];
    
    return (keyboard != nil ? tempWindow : aView);
}


- (MBProgressHUD*)showProgressInView:(UIView*)view target:(id)aTarget
                               title:(NSString*)titleText selector:(SEL)aSelector
{
    UIView *frontView = [self frontFromView:view];
    
    MBProgressHUD* mbhub = [[MBProgressHUD alloc] initWithView:frontView];
	mbhub.labelText = titleText;
    mbhub.removeFromSuperViewOnHide = YES;
    [mbhub setDelegate:(id)aTarget];
	[mbhub show:YES];
	[frontView addSubview:mbhub];
    [aTarget performSelector:aSelector withObject:mbhub afterDelay:0.3];
    return mbhub;
}


- (void) showAlert: (NSString*) msg
{
	[[[UIAlertView alloc] initWithTitle: nil
                                message: msg
                               delegate: nil
                      cancelButtonTitle: @"OK"
                      otherButtonTitles: nil] show];
}


- (void)resetTextFields
{
    self.txtEmail.text      =@"";
    self.txtPassword.text   =@"";
    self.txtZipcode.text    =@"";
    self.txtBirthYear.text  =@"";
    self.txtGender.text     =@"";
}



#pragma mark - using API

- (void)registerAPI:(MBProgressHUD*)progress
{
    NSError *error = nil;
    NSDictionary *userInfo = nil;
    
    NSString *email_Str     = txtEmail.text;
    NSString *pass_Str      = txtPassword.text;
    NSString *zip_Str       = txtZipcode.text;
    NSString *birth_Str     = txtBirthYear.text;
    NSString *gender_Str    = txtGender.text;
    NSString *full_name     = self.txtFullName.text;
    
    userInfo = [BesafeAPI registerAPI:email_Str full_name:full_name password:pass_Str zipcode:zip_Str birthyear:birth_Str gender:gender_Str error:&error];
    
    if (error) {
        [progress hide:YES];
        [self showAlert:[error localizedDescription]];
        
        txtEmail.text = @"";
        txtPassword.text = @"";
        txtZipcode.text = @"";
        txtBirthYear.text = @"";
        txtGender.text = @"";
        
        return;
    }
    else {
        NSLog(@"update success %@",[userInfo objectForKey:@"data"]);
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.isLoginSuccessfully = YES;
        
        [[NSUserDefaults standardUserDefaults] setObject:@"isLoginSuccessfully" forKey:@"QUIT_APP"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSDictionary *data = [userInfo objectForKey:@"data"];
        [progress hide:YES];
        
        VMUsers *user = [[VMUsers alloc] init];
        user.email          = [data objectForKey:@"email"];
        user.birthday       = [data objectForKey:@"birthday"];
        user.zipcode        = [data objectForKey:@"zip_code"];
        user.gender         = [data objectForKey:@"sex"];
        user.fullname       = [data objectForKey:@"full_name"];
        user.userId_login   = [data objectForKey:@"id"];
        user.userId         = [data objectForKey:@"id"];
        
        [VMUsers saveUser:user];
        NSLog(@"data user: %@",user);
        
        //[AppDelegate startUp];
        [self performSelector:@selector(hiddenSplashScreenAndGotoHomeViewController) withObject:self afterDelay:1];
        
        return;
    }
}


//-- go to Home Screen
- (void) hiddenSplashScreenAndGotoHomeViewController
{
    
    //-- changes list tabbarViewController
    NSMutableArray *tbViewControllers = [NSMutableArray arrayWithArray:[self.tabBarController viewControllers]];
    [tbViewControllers removeObjectAtIndex:0];
    [self.tabBarController setViewControllers:tbViewControllers];
    
    self.tabBarController.selectedIndex = 0;
    
    //-- show tabbar, navigationbar
    [self.tabBarController setTabBarHidden:NO animated:NO];
    self.navigationController.navigationBarHidden = NO;
    
    //get twitter account
    [AppDelegate checkACC];
}



#pragma mark - UITabeView

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayGender.count;
}


- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"namecountry-cell"];
    
    cell.textLabel.text = [arrayGender objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor grayColor];
    [cell.textLabel setFont:[UIFont fontWithName:@"System" size:11]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 32;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *name = [arrayGender objectAtIndex:indexPath.row];
    txtGender.text = name;
}



#pragma mark --UITextFieldDelegate METHODS--

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (isIphone5)
        scrollView.frame = CGRectMake(0, -110, 320, 700);
    else
        scrollView.frame = CGRectMake(0, -170, 320, 518); // 780
        scrollView.contentSize = CGSizeMake(320, 580);
    
    if (textField == self.txtGender) {
        if (isIphone5)
            viewGender.frame = CGRectMake(103, 173, viewGender.frame.size.width, viewGender.frame.size.height);
        else
            viewGender.frame = CGRectMake(103, 126, viewGender.frame.size.width, viewGender.frame.size.height);
        
        self.viewGender.alpha = 1;
    }else{
        self.viewGender.alpha = 0;
    }
    
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[scrollView scrollRectToVisible:textField.frame animated:YES];
	[UIView commitAnimations];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationBeginsFromCurrentState:YES];
    if (isIphone5) {
        scrollView.frame = CGRectMake(0, 0, 320, 600); 
    }
    else{
        scrollView.frame = CGRectMake(0, 0, 320, 518); // 600
    }
	[UIView commitAnimations];
    
    [self.tabBarController setTabBarHidden:YES animated:NO];
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return NO;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.txtFullName) {
        [textField resignFirstResponder];
        [self.txtEmail becomeFirstResponder];
        self.viewGender.alpha = 0;
        
        return NO;
    }else if (textField == self.txtEmail) {
        [textField resignFirstResponder];
        [self.txtPassword becomeFirstResponder];
        self.viewGender.alpha = 0;
        
        return NO;
    }else if (textField == self.txtPassword) {
        [textField resignFirstResponder];
        [self.txtZipcode becomeFirstResponder];
        self.viewGender.alpha = 0;
        
        return NO;
    }
    if (textField == self.txtZipcode) {
        [textField resignFirstResponder];
        [self.txtBirthYear becomeFirstResponder];
        self.viewGender.alpha = 0;
        
        return NO;
    }else if (textField == self.txtBirthYear) {
        [textField resignFirstResponder];
        [self.txtGender becomeFirstResponder];
        
        if (isIphone5)
            viewGender.frame = CGRectMake(103, 173, viewGender.frame.size.width, viewGender.frame.size.height);
        else
            viewGender.frame = CGRectMake(103, 126, viewGender.frame.size.width, viewGender.frame.size.height);
        
        self.viewGender.alpha = 1;
        
        return NO;
    }else if (textField == self.txtGender) {
        [textField resignFirstResponder];
        
        self.viewGender.alpha = 0;
        
        [self.navigationItem setRightBarButtonItem:nil animated:NO];
        [self.tabBarController setTabBarHidden:YES animated:NO];
        
        return NO;
    }
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSLog (@"shouldChangeCharactersInRange:%@",textField.text);
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}



@end
