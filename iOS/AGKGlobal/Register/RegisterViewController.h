//
//  RegisterViewController.h
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 10/10/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "BesafeAPI.h"
#import "AppDelegate.h"
#import "TermsPolicyViewController.h"
#import "Utility.h"
#import "UIView+HidingView.h"

@interface RegisterViewController : UIViewController<UITextFieldDelegate,UIScrollViewAccessibilityDelegate,UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIButton         *btnEmail;
    IBOutlet UIButton         *btnPassword;
    IBOutlet UIButton         *btnZipcode;
    IBOutlet UIButton         *btnBirthYear;
    IBOutlet UIButton         *btnGender;
    IBOutlet UIButton         *btnCheckBox;
    IBOutlet UIButton         *btnTerms;
    IBOutlet UIButton         *btnPolicy;
    IBOutlet UIButton         *btnGoBack;
    IBOutlet UIButton         *btnRegister;    
    __weak IBOutlet UIButton *btnFullname;
    
    BOOL isChecked;
}

@property (nonatomic, retain) IBOutlet UIScrollView     *scrollView;
@property (nonatomic, retain) IBOutlet UIView           *viewGender;
@property (nonatomic, retain) IBOutlet UITableView      *tableView;
@property (nonatomic, retain) IBOutlet UITextField      *txtEmail;
@property (nonatomic, retain) IBOutlet UITextField      *txtPassword;
@property (nonatomic, retain) IBOutlet UITextField      *txtZipcode;
@property (nonatomic, retain) IBOutlet UITextField      *txtBirthYear;
@property (nonatomic, retain) IBOutlet UITextField      *txtGender;

@property (weak, nonatomic) IBOutlet UITextField *txtFullName;


@property (nonatomic, retain) NSArray *arrayGender;

- (IBAction)clickToBtnCheckBox:(id)sender;
- (IBAction)clickToBtnTerms:(id)sender;
- (IBAction)clickToBtnGoBack:(id)sender;
- (IBAction)clickToBtnRegister:(id)sender;
- (IBAction)clickPatentPending:(id)sender;



@end
