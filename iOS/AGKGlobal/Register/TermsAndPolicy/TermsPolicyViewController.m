//
//  TermsPolicyViewController.m
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 10/11/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "TermsPolicyViewController.h"

@interface TermsPolicyViewController ()

@end

@implementation TermsPolicyViewController

@synthesize lblTitle, txvContent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.isShowText) {
        
        _viewText.alpha = 1;
        _viewWeb.alpha = 0;
        
        //-- hide navigationbar
        self.navigationController.navigationBarHidden = YES;
        
        [self getContentForViewText];
        
    }else{
        
        _viewText.alpha = 0;
        _viewWeb.alpha = 1;
        
        //-- show navigationbar
        self.navigationController.navigationBarHidden = NO;
        
        // custom navigation bar button
        // left
        UIBarButtonItem *leftbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel:)];
        self.navigationItem.leftBarButtonItem = leftbarBtnItem;
        
        [self customWebView:_wvTerms];
        
        [self getContentForViewWeb];
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    //-- change title color
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Get Data

- (void)getContentForViewText
{
    lblTitle.text = @"Terms of Use";
    txvContent.text = @"Please read through Besafe’s Terms and Conditions page (http://besafe.com/terms.html) before proceeding. By proceeding, you are agreeing that you have read and understood our Terms and Conditions and that you are in complete agreement with them in their entirety. If there is any part of these Terms that you do not agree with, STOP now and do not continue.\nPlease note that Besafe has ZERO tolerance for harassment of any nature and this includes the use of any objectionable material (including sexually explicit content) on the Besafe platform. Users who violate Besafe’s Terms and Conditions will be removed immediately, permanently. You may flag inappropriate content within Besafe’s platform (every image has a flag option on the top right corner). In addition, you can also report any wrongful behavior by emailing us at report@besafe.com . In your email, please include a snapshot of the profile of the user you are reporting, the user’s name and the user’s displayed age.\nIf you understand and agree to the above-mentioned points, please click Proceed. If not, please click Cancel.";
}


- (void)getContentForViewWeb
{
    //-- set webview data
    
    [_indicator startAnimating];
    
    NSURL* nsUrl = [NSURL URLWithString:@"http://www.besafeapps.com"];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:60];
    
    [_wvTerms loadRequest:request];
    
    // optimize speed scroll uiwebview content bug #87.
    _wvTerms.scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
}



#pragma mark - ACTION

- (IBAction)clickToBtnHideView:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.isShowText)
        appDelegate.isShowText = NO;
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark - navigationbar button

- (void)cancel:(UIButton*)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.isShowLink)
        appDelegate.isShowLink = NO;
    
    //-- hide navigationbar
    self.navigationController.navigationBarHidden = YES;
    
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - UIWebviewDelegate

-(void)customWebView:(UIWebView*)aWebView
{
    aWebView.scrollView.scrollEnabled = YES;
    //-- remove background of webView and disable scroll view of webview
    [aWebView setOpaque:NO];
    [aWebView setBackgroundColor:[UIColor whiteColor]];
    for (UIView* subView in [aWebView subviews])
    {
        if ([subView isKindOfClass:[UIScrollView class]])
        {
            //-- disable scroll view of webview
            // ((UIScrollView *)(subView)).scrollEnabled = NO;
            for (UIView* shadowView in [subView subviews])
            {
                if ([shadowView isKindOfClass:[UIImageView class]])
                    [shadowView setHidden:YES];
            }
        }
    }
    //-- set attribute for webview
    aWebView.delegate = self;
    aWebView.scalesPageToFit = NO;
    aWebView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_indicator stopAnimating];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_indicator stopAnimating];
}




@end
