//
//  TermsPolicyViewController.h
//  AGKGlobal
//
//  Created by Thỏa Đại Ka on 10/11/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface TermsPolicyViewController : UIViewController<UITextViewDelegate, UIWebViewDelegate>
{
    IBOutlet UIButton                   *btnHideView;
    IBOutlet UIView                     *_viewText;
    IBOutlet UIView                     *_viewWeb;
    IBOutlet UIWebView                  *_wvTerms;
    IBOutlet UIActivityIndicatorView    *_indicator;
}

@property (nonatomic, retain) IBOutlet UILabel          *lblTitle;
@property (nonatomic, retain) IBOutlet UITextView       *txvContent;

- (IBAction)clickToBtnHideView:(id)sender;

@end
