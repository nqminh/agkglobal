//
//  MessageHistoryController.m
//  BeSafe
//
//  Created by The Rand's on 10/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MessageHistoryController.h"


@implementation MessageHistoryController

@synthesize headerTitle, historyArray;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = headerTitle;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    UIBarButtonItem *btnReply = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose 
                                                                            target:self 
                                                                            action:@selector(replySMS)]; 
    
    self.navigationItem.rightBarButtonItem = btnReply;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.historyArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
       // UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 40, 40)];
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 200, 40)];
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(240, 5, 70, 20)];
        UILabel *numberLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        
        //[imageView setTag:8];
        //[imageView setContentMode:UIViewContentModeScaleAspectFill];
        //[imageView setClipsToBounds:YES];
        
        [messageLabel setTag:9];
        [messageLabel setBackgroundColor:[UIColor clearColor]];
        [messageLabel setTextColor:[UIColor grayColor]];
        [messageLabel setHighlightedTextColor:[UIColor whiteColor]];
        [messageLabel setFont:[UIFont boldSystemFontOfSize:14]];
        
        [timeLabel setTag:12];
        [timeLabel setFont:[UIFont boldSystemFontOfSize:12]];
        [timeLabel setTextColor:[UIColor colorWithRed:1 green:.6 blue:0 alpha:1]];
        [timeLabel setTextAlignment:NSTextAlignmentCenter];

        [numberLabel setTag:11];
        [numberLabel setHidden:YES];
        [numberLabel setBackgroundColor:[UIColor clearColor]];
        
        //[cell.contentView addSubview:imageView];
        [cell.contentView addSubview:messageLabel];
        [cell.contentView addSubview:timeLabel];
        [cell.contentView addSubview:numberLabel];
    }
    
    [(UILabel *)[cell.contentView viewWithTag:9] setText:[[NSMutableDictionary dictionaryWithDictionary:[self.historyArray objectAtIndex:indexPath.row]] objectForKey:@"message"]];
    [(UILabel *)[cell.contentView viewWithTag:11] setText:[[NSMutableDictionary dictionaryWithDictionary:[self.historyArray objectAtIndex:indexPath.row]] objectForKey:@"number"]];
    [(UILabel *)[cell.contentView viewWithTag:12] setText:[self getFormattedTimestamp:[[NSMutableDictionary dictionaryWithDictionary:[self.historyArray objectAtIndex:indexPath.row]] objectForKey:@"timestamp"]]];

    // WE NO LONGER TRACK WHO WAS SENT SMS MESSAGES, SO WE DON'T DO THIS ANYMORE //
    /*
    //LOOK UP IMAGE FROM ADDRESSBOOK
    ABAddressBookRef addressBook = ABAddressBookCreate();
    ABRecordRef personRef = ABAddressBookGetPersonWithRecordID(addressBook, [[[self.historyArray objectAtIndex:indexPath.row] objectForKey:@"abId"] intValue]);
    if(ABPersonHasImageData(personRef)){
        [(UIImageView *)[cell.contentView viewWithTag:8] setImage:[UIImage imageWithData:(__bridge NSData *)ABPersonCopyImageData(personRef)]];
    }else{
        [(UIImageView *)[cell.contentView viewWithTag:8] setImage:[UIImage imageNamed:@"noImage.png"]];
    }*/
    
    return cell;
}

- (NSString *)getFormattedTimestamp:(NSDate *)date 
{
    NSString *formattedDate;
    NSDateFormatter *df = [NSDateFormatter new];
    NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents * components =[gregorian components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate * today = [gregorian dateFromComponents:components];
    
    NSDateComponents * components2 =[gregorian components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:date];
    NSDate * oldDate = [gregorian dateFromComponents:components2];
    
    if([today isEqualToDate:oldDate]) {
        [df setDateFormat:@"h:mm a"];
    }else {
        [df setDateFormat:@"M/d/YYYY"];
    }
    
    formattedDate = [df stringFromDate:date];
    
    return formattedDate;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self displaySMSComposerSheet:[(UILabel *)[cell.contentView viewWithTag:11] text] body:@"" abId:[[self.historyArray objectAtIndex:indexPath.row] objectForKey:@"abId"]];
    
}

- (void)replySMS
{
    [self displaySMSComposerSheet:[[self.historyArray objectAtIndex:0] objectForKey:@"number"] 
                             body:@"" 
                             abId:[[self.historyArray objectAtIndex:0] objectForKey:@"abId"]];
    
    
    
//    [(UILabel *)[cell.contentView viewWithTag:9] setText:[[NSMutableDictionary dictionaryWithDictionary:[self.historyArray objectAtIndex:indexPath.row]] objectForKey:@"message"]];
//    [(UILabel *)[cell.contentView viewWithTag:11] setText:[[NSMutableDictionary dictionaryWithDictionary:[self.historyArray objectAtIndex:indexPath.row]] objectForKey:@"number"]];
//    [(UILabel *)[cell.contentView viewWithTag:12] setText:[self getFormattedTimestamp:[[NSMutableDictionary dictionaryWithDictionary:[self.historyArray objectAtIndex:indexPath.row]] objectForKey:@"timestamp"]]];

}

- (void)displaySMSComposerSheet:(NSString *)recipient body:(NSString *)body abId:(NSNumber *)abId
{
    if([MFMessageComposeViewController canSendText])
	{
        SMSComposerSheetViewController *controller = [[SMSComposerSheetViewController alloc] init];
        controller.messageComposeDelegate = self;
        if (recipient) { controller.recipients = [NSArray arrayWithObjects:recipient, nil];}
        if (body) { controller.body = body; }
        if (abId) { controller.abId = abId; }
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:
			break;
		case MessageComposeResultSent:
            
			break;
		default:
			break;
	}
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
