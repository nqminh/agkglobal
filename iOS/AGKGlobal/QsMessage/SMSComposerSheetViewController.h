//
//  SMSComposerSheetViewController.h
//  BeSafe
//
//  Created by The Rand's on 7/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "MessageViewController.h"

@interface SMSComposerSheetViewController : MFMessageComposeViewController <UIAlertViewDelegate> {
    UIButton                *smsButton;
    UIView                  *smsDelegateView;
    UIView                  *inputTextView;
    UIView                  *recipientView;
    BOOL                    closeOnAppear;
    NSNumber                *abId;
    IBOutlet UIView         *countDownView;
    NSString                *recipientName;
    NSArray                 *originalRecipients;
}

@property (nonatomic, retain) UIButton              *smsButton;
@property (nonatomic, retain) UIView                *smsDelegateView;
@property (nonatomic, retain) UIView                *inputTextView;
@property (nonatomic, retain) UIView                *recipientView;
@property (nonatomic, retain) IBOutlet UIView       *countDownView;
@property (nonatomic, retain) NSNumber              *abId;
@property (nonatomic, retain) NSString              *recipientName;
@property (nonatomic, retain) NSArray               *originalRecipients;

- (void)prepareSmsMessage;
- (void)sendSmsMessage;
- (void)lookUpName;
- (NSMutableArray *)loadHistory;

@end
