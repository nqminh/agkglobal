//
//  SMSComposerSheetViewController.m
//  BeSafe
//
//  Created by The Rand's on 7/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SMSComposerSheetViewController.h"
#import "CountDownViewController.h"
#import "Constants.h"

@implementation SMSComposerSheetViewController

@synthesize smsButton, smsDelegateView, inputTextView, recipientView, countDownView, abId, recipientName,originalRecipients;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        closeOnAppear = NO;
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    BOOL hideGroupWarning = [[NSUserDefaults standardUserDefaults] boolForKey:@"HIDE_GROUP_WARNING"];
    NSString *button2 = [[NSUserDefaults standardUserDefaults] stringForKey:@"SECOND_BUTTON_TEXT"];
    if (!button2) hideGroupWarning = NO;

    // 2012-07-30 JJOLLEY //
    // DO NOT SHOW THE POPUP WARNING SINCE WE NO LONGER TRACK SMS HISTORY //
//    if (!hideGroupWarning) {
//        BOOL canForgetWarning = [[NSUserDefaults standardUserDefaults] boolForKey:@"CAN_FORGET_WARNING"];
//        NSString *msg = @"BeSafe can not know if you make any changes to the selected recipient.";
//        if (canForgetWarning) {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Group SMS" 
//                                                            message:msg
//                                                           delegate:self 
//                                                  cancelButtonTitle:@"OK" 
//                                                  otherButtonTitles:@"Hide Warning",nil];
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_FORGET_WARNING"];
//            [[NSUserDefaults standardUserDefaults] setValue:@"Hide Warning" forKey:@"SECOND_BUTTON_TEXT"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            [alert show];
//        } else {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Group SMS" 
//                                                            message:msg
//                                                           delegate:self 
//                                                  cancelButtonTitle:@"OK" 
//                                                  otherButtonTitles:nil];
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"CAN_FORGET_WARNING"];
//            [[NSUserDefaults standardUserDefaults] setValue:@"Hide Warning" forKey:@"SECOND_BUTTON_TEXT"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            [alert show];
//        }
//    }
}

- (void)viewDidAppear:(BOOL)animated
{
    if(closeOnAppear){
        [self dismissViewControllerAnimated:NO completion:nil];
    }else{
        [self lookUpName];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}


- (void)lookUpName
{
    NSLog(@"abId:%i",[self.abId intValue]);
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
        __block BOOL accessGranted = NO;
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
            if (YES == granted) {
                NSLog(@"do nothing");
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contacts"
                                                                message:@"You must grant BeSafe access to your address book in order to use your contacts."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles: nil];
                [alert show];
            }
        });
    }
    
    
    ABRecordRef personRef = ABAddressBookGetPersonWithRecordID(addressBook, [self.abId intValue]);
    CFStringRef firstName = ABRecordCopyValue(personRef, kABPersonFirstNameProperty);
    CFStringRef lastName = ABRecordCopyValue(personRef, kABPersonLastNameProperty);
    self.recipientName = @"";
    if (!lastName) {
        self.recipientName = [NSString stringWithFormat: @"%@",(__bridge NSString *)firstName];
    }else{
        self.recipientName = [NSString stringWithFormat: @"%@ %@",(__bridge NSString *)firstName, (__bridge NSString *)lastName];
    }
    if (nil != addressBook) CFRelease(addressBook);
}














- (void)listSubviewsOfView:(UIView *)view atLevel:(NSInteger)level {
    NSLog(@"--%d--",level);
    
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    if (subviews == nil) {

        return;
    }

    // Return if there are no subviews
    if ([subviews count] == 0) return;
    NSLog(@"--bb--");
    
    for (UIView *subview in subviews) {
        
        NSLog(@"%d:%@",level, subview);
        
        // List the subviews of subview
        [self listSubviewsOfView:subview atLevel:level+1];
    }
}


- (void)prepareSmsMessage
{
    //REMOVES BUTTON BEHAVIOR
    [self.smsButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];

    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"] == 0) {
        [self sendSmsMessage];
    }else {
        CountDownViewController *controller = nil;
        if (isIphone5)
            controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController-568h" bundle:nil];
        else
            controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController" bundle:nil];
        
        controller.messageText = [(UITextView*)self.inputTextView text];
        //controller.recipientText = self.recipientName;
        controller.recipientText = @"SMS Recipient";
        controller.delegate = self;
        controller.abId = self.abId;
        controller.sendType = SMS_TYPE;

        UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromRight;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
        [UIView setAnimationDuration:COUNTDOWN_TRANSITION];
        [UIView setAnimationDelegate:controller];
        [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
        [self presentViewController:controller animated:YES completion:nil];
        [UIView commitAnimations];
    }
}

- (void)sendSmsMessage
{
    //SEND ORIGINAL SMS MESSAGE
    [self.smsDelegateView performSelector:@selector(send:) withObject:self.smsButton];
    
    //SEND INFO TO HISTORY
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    [tempDict setValue:[(UITextView*)self.inputTextView text] forKey:@"message"];
    [tempDict setValue:[self.recipients objectAtIndex:0] forKey:@"number"];
    [tempDict setValue:self.recipientName forKey:@"name"];
    [tempDict setObject:self.abId forKey:@"abId"];
    [tempDict setObject:[NSDate date] forKey:@"timestamp"];

    NSMutableArray *historyArray = [self loadHistory];
    [historyArray insertObject:tempDict atIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:historyArray forKey:@"smsHistory"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    //CLOSE WINDOW
    closeOnAppear = YES;
}


- (NSMutableArray *)loadHistory
{
    NSArray *fullArray = [NSArray arrayWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"smsHistory"]];
    NSMutableArray *newArray = [[NSMutableArray alloc] initWithCapacity:[fullArray count]];
    
    NSMutableDictionary *myDict;
    for (int i = 0; i < fullArray.count; i++) {
        myDict = [[NSMutableDictionary alloc] initWithCapacity:0];
        [myDict setValue:[[fullArray objectAtIndex:i] valueForKey:@"message"] forKey:@"message"];
        [myDict setValue:[[fullArray objectAtIndex:i] valueForKey:@"number"] forKey:@"number"];
        [myDict setValue:[[fullArray objectAtIndex:i] valueForKey:@"name"] forKey:@"name"];
        [myDict setValue:[[fullArray objectAtIndex:i] valueForKey:@"abId"] forKey:@"abId"];
        [myDict setValue:[[fullArray objectAtIndex:i] valueForKey:@"timestamp"] forKey:@"timestamp"];
        [newArray addObject:myDict];
    }
    
    return newArray;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (1 == buttonIndex) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HIDE_GROUP_WARNING"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
        
}


@end
