//
//  AddQMessageViewController.m
//  BeSafe
//
//  Created by The Rand's on 7/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AddQMessageViewController.h"
#import "QMessageViewController.h"
#import <QuartzCore/QuartzCore.h>

@implementation AddQMessageViewController

@synthesize row, quickTextField, myTextView, myQuickList;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        UIBarButtonItem *btnAdd = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(saveMessage:)];
        self.navigationItem.rightBarButtonItem = btnAdd;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"aqmvc:viewDidLoad:text=%@",self.myTextView.text);
    NSLog(@"aqmvc:viewDidLoad:row=%d",self.row);
    NSLog(@"aqmvc:viewDidLoad:list=%@",self.myQuickList);

    if (self.row >= 0 && nil != self.myQuickList)
        self.myTextView.text = [self.myQuickList objectAtIndex:self.row];
    [myTextView becomeFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)saveMessage:(id)sender
{
    //stuff-goes-here
    NSLog(@"saveMessage");
    NSLog(@"%@",self.myQuickList);
    NSString *newMessage = [self.myTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // get data from banner words , banned addresses.
    textSrc_qmessage = newMessage;
    bannedWordArr_qmessage = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_WORDS"] mutableCopy];
    bannedAddressArr_qmessage = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_ADDRESSES"] mutableCopy];
    
    showBannedWordStr_qmessage = [Utility showBanned:textSrc_qmessage withArr:bannedWordArr_qmessage];
    showBannedAddressStr_qmessage = [Utility showBanned:textSrc_qmessage withArr:bannedAddressArr_qmessage];
    
    if(showBannedWordStr_qmessage.length > 0 )
    {
        if (showBannedAddressStr_qmessage.length > 0)
        {
            NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_qmessage);
            alv_qmessage = [[UIAlertView alloc]initWithTitle:@"Banned Word and Address!" message:[NSString stringWithFormat:@"You have typed Banned Word and Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_qmessage show];
        }
        else{
            NSLog(@"\n\n banned word %@\n\n",showBannedWordStr_qmessage);
            alv_qmessage = [[UIAlertView alloc]initWithTitle:@"Banned Word!" message:[NSString stringWithFormat:@"You have typed Banned Word \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_qmessage show];
        }
        
    }
    else if (showBannedAddressStr_qmessage.length > 0)
    {
        NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_qmessage);
        alv_qmessage = [[UIAlertView alloc]initWithTitle:@"Banned Address!" message:[NSString stringWithFormat:@"You have typed Banned Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alv_qmessage show];
    }
    else
    {
        if (newMessage.length > 0 && nil != self.myQuickList) {
            // WE HAVE SOME EDITED TEXT TO UPDATE //
            if (self.row >= 0) {
                // EDIT MODE //
                NSLog(@"Edit Mode");
                [self.myQuickList replaceObjectAtIndex:self.row withObject:newMessage];
            } else {
                // ADD MODE //
                NSLog(@"Add Mode");
                [self.myQuickList addObject:newMessage];
            }
            NSLog(@"!!! 4 !!!");
            NSLog(@"%@",self.myQuickList);
            [[NSUserDefaults standardUserDefaults] setObject:self.myQuickList forKey:@"QUICK_LIST"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        NSLog(@"//saveMessage//");
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
