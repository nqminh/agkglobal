//
//  MessageHistoryController.h
//  BeSafe
//
//  Created by The Rand's on 10/13/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <AddressBook/AddressBook.h>
#import "SMSComposerSheetViewController.h"

@interface MessageHistoryController : UITableViewController  <MFMessageComposeViewControllerDelegate>
{
    NSString        *headerTitle;
    NSMutableArray  *historyArray;
}

@property (nonatomic, retain) NSString          *headerTitle;
@property (nonatomic, retain) NSMutableArray    *historyArray;

- (void)displaySMSComposerSheet:(NSString *)recipient body:(NSString *)body abId:(NSNumber *)abId;
- (NSString *)getFormattedTimestamp:(NSDate *)date;
- (void)replySMS;

@end
