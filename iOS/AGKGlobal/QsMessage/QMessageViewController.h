//
//  QMessageViewController.h
//  BeSafe
//
//  Created by The Rand's on 7/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMessageViewController : UIViewController<UITableViewDataSource,UITableViewDelegate> {
    // check banned word , address
    NSString *textSrc_contactList;
    NSString *showBannedWordStr_contactList;
    NSString *showBannedAddressStr_contactList;
    
    NSArray *bannedWordArr_contactList;
    NSArray *bannedAddressArr_contactList;
    
    UIAlertView *alv_contactList;

}



@property (weak, nonatomic) IBOutlet UITableView *tblMessage;
@property (nonatomic, retain) NSMutableArray	*_quickList;
@property (nonatomic, retain) NSArray           *alphabet;

- (NSMutableArray*)quickList;

@end
