//
//  AddQMessageViewController.h
//  BeSafe
//
//  Created by The Rand's on 7/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QMessageViewController.h"

@interface AddQMessageViewController : UIViewController {
    
    // check banned word , address
    NSString *textSrc_qmessage;
    
    NSString *showBannedWordStr_qmessage;
    NSString *showBannedAddressStr_qmessage;
    
    NSArray *bannedWordArr_qmessage;
    NSArray *bannedAddressArr_qmessage;
    UIAlertView *alv_qmessage;

}

@property NSInteger                                    row;
@property (nonatomic,strong) UITextField               *quickTextField;
@property (nonatomic,strong) IBOutlet UITextView       *myTextView;
@property (nonatomic,strong) NSMutableArray            *myQuickList;

- (void)saveMessage:(id)sender;

@end
