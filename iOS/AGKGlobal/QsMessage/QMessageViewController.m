//
//  QMessageViewController.m
//  BeSafe
//
//  Created by The Rand's on 7/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "QMessageViewController.h"
#import "AddQMessageViewController.h"
#import "ContactListViewController.h"

#import "Constants.h" 
#import "UIView+HidingView.h"
#import "ShowLinkViewController.h" 
#import "MessageCell.h"

@implementation QMessageViewController

@synthesize _quickList, alphabet;

/*
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"Text";
        self.navigationItem.title = @"BeSafe Text";
        self.tabBarItem.image = [UIImage imageNamed:@"icon_text_selected.png"];
        //self.tabBarItem.image = [UIImage imageNamed:@"tab_5_quick"];
        
        UIButton *btnCustome = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 33, 33)];
        [btnCustome setBackgroundImage:[UIImage imageNamed:@"new_btn.png"] forState:UIControlStateNormal];
        [btnCustome addTarget:self action:@selector(addMessage:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *btnAdd = [[UIBarButtonItem alloc] initWithCustomView:btnCustome];        
//        UIBarButtonItem *btnAdd = [[UIBarButtonItem alloc]
//                                   initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
//                                   target:self
//                                   action:@selector(addMessage:)];
        self.navigationItem.rightBarButtonItem = btnAdd;
        
        
        
        // GET PERMISSIONS TO READ CONTACTS //
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
            __block BOOL accessGranted = NO;
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                accessGranted = granted;
                dispatch_semaphore_signal(sema);
                if (YES == granted) {
                    [self.tableView reloadData];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contacts"
                                                                    message:@"You must grant BeSafe access to your address book in order to use your contacts."
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles: nil];
                    [alert show];
                }
            });
        }


    }
    return self;
}

*/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Text";
        self.navigationItem.title = @"BESAFE TEXT";
        self.tabBarItem.image = [UIImage imageNamed:@"icon_text_selected.png"];
        //self.tabBarItem.image = [UIImage imageNamed:@"tab_5_quick"];
        
        UIButton *btnCustome = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 33, 33)];
        [btnCustome setBackgroundImage:[UIImage imageNamed:@"new_btn.png"] forState:UIControlStateNormal];
        [btnCustome addTarget:self action:@selector(addMessage:) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *btnAdd = [[UIBarButtonItem alloc] initWithCustomView:btnCustome];
        //        UIBarButtonItem *btnAdd = [[UIBarButtonItem alloc]
        //                                   initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
        //                                   target:self
        //                                   action:@selector(addMessage:)];
        self.navigationItem.rightBarButtonItem = btnAdd;
        
        
        
        // GET PERMISSIONS TO READ CONTACTS //
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
            __block BOOL accessGranted = NO;
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
                accessGranted = granted;
                dispatch_semaphore_signal(sema);
                if (YES == granted) {
                    [self.tblMessage reloadData];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contacts"
                                                                    message:@"You must grant BeSafe access to your address book in order to use your contacts."
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles: nil];
                    [alert show];
                }
            });
        }
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clickViewDetailLink) name:@"BesafeViewLink" object:nil];
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    self._quickList = nil;
    [self.tblMessage reloadData];
    
    //-- change title color
    CGRect frame = CGRectMake(100, 0, 200, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor colorWithRed:224.0/255.0 green:243.0/255.0 blue:129.0/255.0 alpha:1.0];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BesafeViewLink" object:nil];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



#pragma mark - Handle action Notification

-(void)clickViewDetailLink
{
    
    NSString* urlString1 = @"http://www.besafeapps.com/smsiphone.html";
    User *user = [[Utility shareProfileDict] objectForKey:[[NSUserDefaults standardUserDefaults] objectForKey:facebookID]];
    if (user == nil) {
        user = [[User alloc] initWithDictinary:nil];
    }
    DLOG(@"url %@",urlString1);
    ShowLinkViewController *wv = [[ShowLinkViewController alloc] initWithNibName:@"ShowLinkViewController" bundle:nil];
    wv.strLinkUrl = urlString1;
    wv.post = user;
    [self.navigationController pushViewController:wv animated:YES];
    
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self quickList].count;
}


- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = @"";
    if (0 == section) sectionTitle = @"Quick Messages";
    return sectionTitle;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //if (!cell) {
      //  cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //}
    //static NSString *CellIdentifier = @"Cell";
    MessageCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MessageCell" owner:self options:nil]objectAtIndex:0];
    }  
    
    
    NSString *tem = [[self quickList] objectAtIndex:indexPath.row];
    if (tem != nil && ![tem isEqualToString:@""]) {
        NSMutableAttributedString *temString=[[NSMutableAttributedString alloc]initWithString:tem];
        [temString addAttribute:NSUnderlineStyleAttributeName
                          value:[NSNumber numberWithInt:1]
                          range:(NSRange){0,[temString length]}];
        //cell.textLabel.attributedText = temString;
        cell.lblContent.text = tem;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageCell *cell = [[[NSBundle mainBundle] loadNibNamed:@"MessageCell" owner:self options:nil] objectAtIndex:0];
    return cell.frame.size.height;
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    //JLJ
    NSLog(@"AccessoryButtonTapped:%d:%@",indexPath.row,[[self quickList] objectAtIndex:indexPath.row]);
    AddQMessageViewController *aqmvc = [[AddQMessageViewController alloc] initWithNibName:@"AddQMessageViewController" bundle:nil];
    aqmvc.row = indexPath.row;
    
    if (!self._quickList) NSLog(@"!!! 3 !!!");
    aqmvc.myQuickList = [self quickList];
    aqmvc.myTextView.text = [[self quickList] objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:aqmvc animated:YES];
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSLog(@"Delete Quick");
        [[self quickList] removeObjectAtIndex:indexPath.row];
        
        if (!self._quickList) NSLog(@"!!! 2 !!!");
        [[NSUserDefaults standardUserDefaults] setObject:self._quickList forKey:@"QUICK_LIST"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        self._quickList = nil;
        [tableView reloadData];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // get data from banner words , banned addresses.
    // check banned word , banned address .
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    textSrc_contactList = [[self quickList] objectAtIndex:indexPath.row];
    bannedWordArr_contactList = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_WORDS"] mutableCopy];
    bannedAddressArr_contactList = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_ADDRESSES"] mutableCopy];
    
    
    showBannedWordStr_contactList = [Utility showBanned:textSrc_contactList withArr:bannedWordArr_contactList];
    showBannedAddressStr_contactList = [Utility showBanned:textSrc_contactList withArr:bannedAddressArr_contactList];
    
    if(showBannedWordStr_contactList.length > 0 )
    {
        if (showBannedAddressStr_contactList.length >= 1)
        {
            NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_contactList);
            alv_contactList = [[UIAlertView alloc]initWithTitle:@"Banned Word and Address!" message:[NSString stringWithFormat:@"You have selected Banned Word and Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_contactList show];
        }
        else{
            NSLog(@"\n\n banned word %@\n\n",showBannedWordStr_contactList);
            alv_contactList = [[UIAlertView alloc]initWithTitle:@"Banned Word!" message:[NSString stringWithFormat:@"You have selected Banned Word \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_contactList show];
        }
        
    }
    else if (showBannedAddressStr_contactList.length > 0)
    {
        NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_contactList);
        alv_contactList = [[UIAlertView alloc]initWithTitle:@"Banned Address!" message:[NSString stringWithFormat:@"You have selected Banned Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alv_contactList show];
    }
    else
    {
        ContactListViewController *contactsViewController = [[ContactListViewController alloc] initWithStyle:UITableViewStylePlain];
        contactsViewController.message = [[self quickList] objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:contactsViewController animated:YES];
    }
}


- (NSMutableArray*)quickList {
    if (!self._quickList) {
        NSLog(@"init quickList");
        NSArray *nsa = [[NSUserDefaults standardUserDefaults] arrayForKey:@"QUICK_LIST"];
        if (!nsa) {
            NSLog(@"building quicklist of dictionary");
            NSMutableArray *tmpList = [[NSMutableArray alloc] init];
            NSArray *oldList = [[NSUserDefaults standardUserDefaults] arrayForKey:@"QMsgArray"];
            if (nil != oldList) {
                for (NSDictionary* dict in oldList) {
                    for (NSString *msg in [dict objectForKey:@"list"]) {
                        [tmpList addObject:msg];
                    }
                }
            }
            self._quickList = [[tmpList sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] mutableCopy];
            if (!self._quickList) NSLog(@"!!! 1 !!!");
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"QMsgArray"];
            [[NSUserDefaults standardUserDefaults] setObject:self._quickList forKey:@"QUICK_LIST"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            NSLog(@"recovering quicklist");
            self._quickList = [[nsa sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)] mutableCopy];
        }
        NSLog(@"//init:%d",self._quickList.count);
        
        // fix tabbar hide disappear bug #101 .
        [self.tabBarController  setTabBarHidden:NO animated:NO];
    }
    
    return self._quickList;
}


- (void)addMessage:(id)sender
{
    NSLog(@"addMessage");
    AddQMessageViewController *aqmvc = [[AddQMessageViewController alloc] initWithNibName:@"AddQMessageViewController" bundle:nil];
    aqmvc.row = -1;
    aqmvc.myQuickList = [self quickList];
    [self.navigationController pushViewController:aqmvc animated:YES];
}



@end
