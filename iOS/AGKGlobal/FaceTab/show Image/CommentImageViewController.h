//
//  SendFacebookImageViewController.h
//  AGKGlobal
//
//  Created by Thuy Dao on 9/29/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookWebViewController.h"
#import "MBProgressHUD.h"
#import "SSTextView.h"
#import "UIViewController+MJPopupViewController.h"
#import "TutorViewController.h"
#import "Obj.h"

@class CommentImageViewController;

@protocol commentImageDelegate <NSObject>

- (void)dismissViewCommentImage;
- (void) showPrivacyTipImageView;

@end


@interface CommentImageViewController : UIViewController <UITableViewDataSource,UITableViewDelegate, UITextViewDelegate>
{
    IBOutlet UITableView*                   myTable;
    IBOutlet UITableViewCell*               cell0;
    IBOutlet UITableViewCell*               cell1;
    IBOutlet UITableViewCell*               cell2;
    
    // check banned word , address
    NSString *textSrc_commentImg;
    
    NSString *showBannedWordStr_commentImg;
    NSString *showBannedAddressStr_commentImg;
    
    NSArray *bannedWordArr_commentImg;
    NSArray *bannedAddressArr_commentImg;
    
    UIAlertView *alv_commentImg;
    
     NSString                    *commentText;

}
@property (nonatomic,retain) IBOutlet UITableView           *myTable;
@property (nonatomic,retain) IBOutlet UITableViewCell       *cell0;
@property (nonatomic,retain) IBOutlet UITableViewCell       *cell1;
@property (nonatomic,retain) IBOutlet UITableViewCell       *cell2;
@property (nonatomic,retain) IBOutlet UIImage               *image;
@property (nonatomic,retain) IBOutlet UIImageView           *imagePost;
@property (nonatomic,retain) IBOutlet UIButton              *postingSubmitButton;
@property (nonatomic,retain) IBOutlet SSTextView            *tvPost;
@property (nonatomic,assign) NSString                       *postId;
@property (nonatomic,assign) id <commentImageDelegate>      delegate;
@property (nonatomic,retain) PostObj                        *post;

-(IBAction)posttowall:(id)sender;
-(IBAction)clickCancel:(id)sender;
-(IBAction)clickComment:(id)sender;

@end
