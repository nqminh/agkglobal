//
//  SendFacebookImageViewController.m
//  AGKGlobal
//
//  Created by Thuy Dao on 9/29/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "CommentImageViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface CommentImageViewController ()

@end

@implementation CommentImageViewController
@synthesize myTable;
@synthesize cell0;
@synthesize cell1;
@synthesize cell2;
@synthesize imagePost;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}



#pragma mark - Main

- (void)viewDidLoad
{
    [super viewDidLoad];
    [myTable setScrollEnabled:NO];   
    
    // custom navigation bar button
    // left
    UIBarButtonItem *leftbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel:)];
    self.navigationItem.leftBarButtonItem = leftbarBtnItem;
    
    // right    
    UIBarButtonItem *rightbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Post" style:UIBarButtonItemStyleBordered target:self action:@selector(postText:)];
    self.navigationItem.rightBarButtonItem = rightbarBtnItem;
    
    //set textview
    
    [self.tvPost setPlaceholder:@"What's on your mind?"];
     [self.tvPost becomeFirstResponder];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.imagePost setImage:self.image];
    //[self.tvPost becomeFirstResponder];
    
    //-- change title color
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
}



#pragma mark - Delegate countdown stop sending 

-(void) showKeyboard
{
    [self.tvPost becomeFirstResponder];
}



#pragma mark - navigationbar button

- (void)cancel:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)postText:(UIButton*)sender
{
    // check banned word , banned address .
    textSrc_commentImg = self.tvPost.text;
    bannedWordArr_commentImg = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_WORDS"] mutableCopy];
    bannedAddressArr_commentImg = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_ADDRESSES"] mutableCopy];
    
    
    showBannedWordStr_commentImg = [Utility showBanned:textSrc_commentImg withArr:bannedWordArr_commentImg];
    showBannedAddressStr_commentImg = [Utility showBanned:textSrc_commentImg withArr:bannedAddressArr_commentImg];
    
    if(showBannedWordStr_commentImg.length > 0 )
    {
        if (showBannedAddressStr_commentImg.length >= 1)
        {
            NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_commentImg);
            alv_commentImg = [[UIAlertView alloc]initWithTitle:@"Banned Word and Address!" message:[NSString stringWithFormat:@"You have typed Banned Word and Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_commentImg show];
        }
        else{
            NSLog(@"\n\n banned word %@\n\n",showBannedWordStr_commentImg);
            alv_commentImg = [[UIAlertView alloc]initWithTitle:@"Banned Word!" message:[NSString stringWithFormat:@"You have typed Banned Word \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_commentImg show];
        }
        
    }
    else if (showBannedAddressStr_commentImg.length > 0)
    {
        NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_commentImg);
        alv_commentImg = [[UIAlertView alloc]initWithTitle:@"Banned Address!" message:[NSString stringWithFormat:@"You have typed Banned Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alv_commentImg show];
    }
    else
    {

        [myTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:YES];
        
        MBProgressHUD* progress = [[MBProgressHUD alloc] initWithView:self.view];
        progress.labelText = @"";
        [progress setDelegate:(id)self.view];
        progress.removeFromSuperViewOnHide = YES;
        [self.view addSubview:progress];
        [progress show:YES];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       self.image, @"source",
                                       self.tvPost.text, @"message",
                                       self.tvPost.text, @"caption",
                                       nil];
        
        FBRequest *request = [FBRequest requestWithGraphPath:@"me/photos" parameters:params HTTPMethod:@"POST"];
        
        
        FBRequestConnection *connection = [[FBRequestConnection alloc] init];
        
        [connection addRequest:request completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            
            NSLog(@"Facebook Post Result: %@", result);
            if (error != nil)
            {
                NSLog(@"Facebook Posting Error: %@", [error localizedDescription]);
            }
            dispatch_async(dispatch_get_main_queue(), ^(void){
                NSLog(@"hehe");
                [progress hide:YES];
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
        }];
        [connection start];
    }
}



#pragma mark - uitableviewdelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            return 300;
        }
            
        case 1:
        {
            return 76;
        }
            
        case 2:
        {
            return 44;
            
        }
                        
        default:
            return 44;
    }
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SendFacebookImageViewController_cell"];
    }
    
    switch (indexPath.row) {
        case 0:
        {
            cell = cell0;
            break;
        }
            
        case 1:
        {
            cell = cell1;
            break;
        }
            
        case 2:
        {
            cell = cell2;
            break;
        }

            
        default:
            break;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}



#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    UITableViewCell *cell = (id)textView.superview.superview;
    NSIndexPath *indexPath = [myTable indexPathForCell:cell];
    
    //NSLog(@"%@-%@",[[cell class] description], indexPath);
    
    [myTable setContentInset:UIEdgeInsetsMake(0, 0, 215, 0)];
    [myTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
    
    return YES;
}


-(IBAction)posttowall:(id)sender
{
     [self.tvPost resignFirstResponder];
    [myTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:YES];
    
    MBProgressHUD* progress = [[MBProgressHUD alloc] initWithView:self.view];
    progress.labelText = @"";
    [progress setDelegate:(id)self.view];
    progress.removeFromSuperViewOnHide = YES;
    [self.view addSubview:progress];
    [progress show:YES];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   self.image, @"source",
                                   self.tvPost.text, @"message",
                                   self.tvPost.text, @"caption",
                                   nil];
    
    FBRequest *request = [FBRequest requestWithGraphPath:@"me/photos" parameters:params HTTPMethod:@"POST"];
    
    
    FBRequestConnection *connection = [[FBRequestConnection alloc] init];
    
    [connection addRequest:request completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        NSLog(@"Facebook Post Result: %@", result);
        if (error != nil)
        {
            NSLog(@"Facebook Posting Error: %@", [error localizedDescription]);
        }
            dispatch_async(dispatch_get_main_queue(), ^(void){
                NSLog(@"hehe");
                [progress hide:YES];
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
    }];
    
    [connection start];
    
}


- (IBAction)clickCancel:(id)sender
{
    NSLog(@"clickCancel");
    [self.tvPost resignFirstResponder];
    if (self.delegate && [self.delegate respondsToSelector:@selector(dismissViewCommentImage)]) {
        [self.delegate dismissViewCommentImage];
    }
}


- (IBAction)clickComment:(id)sender
{
    NSLog(@"clickComment");
    NSLog(@"%@",self.tvPost.text);
    if ([self.tvPost.text isEqualToString:@""]) {
        return;
    }
    [self.tvPost resignFirstResponder];
    if (self.delegate && [self.delegate respondsToSelector:@selector(dismissViewCommentImage)]) {
        
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:warningControllFacebook] integerValue] == 1) {
            
            [FacebookUtils comment:self.postId withText:self.tvPost.text];
            [self.delegate dismissViewCommentImage];
          
        }
        else {
            /*
            TutorViewController *tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
            tuto.delegate = (id)self;
            tuto.type = typeComment;
            [self presentPopupViewController:tuto animationType:MJPopupViewAnimationSlideBottomTop bckClickable:YES];
             */
            commentText = self.tvPost.text;
            [self countDownForPost];
            
        }
    }
}



#pragma mark - Coundownt view 

- (void)countDownForPost
{
    NSString *userID = @"";
    if (self.post.subPost != nil) {
        userID = self.post.subPost.userID;
    }
    else userID = self.post.userID;
    
    User *user = [ControlObj getUserWithID:userID];
    
    CountDownViewController *controller = nil;
    if (isIphone5) {
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController-568h" bundle:nil];
    }else{
        controller =  [[CountDownViewController alloc] initWithNibName:@"CountDownViewController" bundle:nil];
    }
    
    controller.messageText = commentText;
    controller.recipientText = [NSString stringWithFormat:@" %@",user.name];
//    NSLog(@"ID U %d",self.post.userID);
    controller.sendMessageDelegate = (id)self;
    controller.imagePath = user.pic;
    controller.sendType = FBcomment;
    controller.delegate_countdown = (id)self;
    [self addCountDownView:controller];
}


- (void)addCountDownView:(CountDownViewController*)controller
{
    UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromRight;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
    [UIView setAnimationDuration:1];
    [UIView setAnimationDelegate:controller];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    [self presentViewController:controller animated:YES completion:nil];
    [UIView commitAnimations];
}


- (void)countDownComplete
{
    //[FacebookUtils comment:self.post.postID withText:[Utility decodeHTMLEntities:commentText]];
    [FacebookUtils comment:self.postId withText:self.tvPost.text];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    [self.delegate dismissViewCommentImage];

}



#pragma mark - tutordelegate

- (void)acceptLike:(warningType)type
{
    switch (type) {
        case typeComment: {
            [FacebookUtils comment:self.postId withText:self.tvPost.text];
            break;
        }
        default:
            break;
    }
    NSLog(@"acceptLike");
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    [self.delegate dismissViewCommentImage];
}


- (void)cancelLike
{
    NSLog(@"cancelLike");    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}


@end
