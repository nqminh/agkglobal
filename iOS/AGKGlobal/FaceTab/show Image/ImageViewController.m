//
//  ImageViewController.m
//  AGKGlobal
//
//  Created by Thuy Dao on 8/21/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "ImageViewController.h"


#define ZOOM_VIEW_TAG 100
#define ZOOM_STEP 1.5

@interface UIImage (Resize)
- (UIImage*)scaleToSize:(CGSize)size;

@end

// Put this in UIImageResizing.m
@implementation UIImage (Resize)


- (UIImage*)scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, size.width, size.height), self.CGImage);
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

@end

@interface ImageViewController ()

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center;

@end

@implementation ImageViewController
@synthesize lblCommentNumber,lblCommentText,lblLikeNumber,lbllikeText,lblTitle;
@synthesize activePostid,isShowButtonShare,isShowButtonShareSubPost;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //        [FacebookUtils addDelegate:(id)self];
        self.imageDic = [[NSMutableDictionary alloc] init];
    }
    return self;
}


- (void) addGestureRecognizertoImage:(BOOL)animated
{
    
    if (animated) {
        singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [self.view addGestureRecognizer:singleTap];
    }
    else{
        [self.view removeGestureRecognizer:singleTap];
    }
}



#pragma mark - Main

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self setupView];
    isTapOnce = 0;
    
    DLOG(@"\nshare dc k0 %d\n",isShowButtonShare);
    scrollImageView.delegate = self;
    scrollImageView.pagingEnabled = YES;
    scrollImageView.userInteractionEnabled = YES;
    
    arrObj = [[NSMutableArray alloc] init];
    //[self addGestureRecognizertoImage];
    //[self confiugreWithArr:self.arrData];
    
    if (self.dataSource == nil) {
        self.dataSource = [[NSMutableDictionary alloc] init];
    }
    [self.dataSource removeAllObjects];
    
    NSLog(@"slide count remove %d",[self.sv.subviews count]);
    
    
    if ([scrollImageView.subviews count] > 0) {
        for (UIView *v in scrollImageView.subviews) {
            [v removeFromSuperview];
        }
    }
    
    NSLog(@"slide count add : %d,",self.arrData.count);
    CGSize result = [[UIScreen mainScreen] bounds].size;
    scrollImageView.frame = CGRectMake(0, 0, 320, result.height);
    
    for (int i = 0; i < [self.arrData count]; i++)
    {
        ImageViewObj* imgView = [ImageViewObj imgViewCreateFromNib];
        imgView.tag = ZOOM_VIEW_TAG;
        if (isShowButtonShare || isShowButtonShareSubPost) {
            imgView.btnShare.hidden = NO;
        }
        else{
            imgView.btnShare.hidden = YES;
        }
        imgView.delegate = self;
        imgView.frame = CGRectMake(320*i, 0, 320,  result.height);
        imgView.tag = i;
        NSDictionary* dict = [self.arrData objectAtIndex:i];
        imgView.dataSource = [[NSMutableDictionary alloc] initWithDictionary:dict];
        [imgView loadPhoto];
        if (result.height == 568) {
            [imgView setFrameFooterView:CGRectMake(0, result.height-102, 320, 70)];//(0, self.height-94, 320, 74)
        }
        else
        {
            [imgView setFrameFooterView:CGRectMake(0, result.height-104, 320, 67)];//(0, self.height-74, 320, 74)
        }
        [imgView.fouterView setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.5]];
        [scrollImageView addSubview:imgView];
        [arrObj addObject:imgView];
    }
    
    self.btnDone = [[UIButton alloc] initWithFrame:CGRectMake(250, 20, 60, 25)];
    [self.btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    self.btnDone.titleLabel.font = [UIFont boldSystemFontOfSize: 14];
    [self.btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnDone addTarget:self action:@selector(clickDone) forControlEvents:UIControlEventTouchUpInside];
    [self.btnDone setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.5]];
    [self.view addSubview:self.btnDone];
    
    scrollImageView.pagingEnabled = YES;
    scrollImageView.userInteractionEnabled = YES;
    self.layoutDict = [[NSMutableDictionary alloc] init];
    self.indexShow = 0;
    
    [self addGestureRecognizertoImage:YES];
    if (result.height == 568) {
        scrollImageView.contentSize = CGSizeMake([self.arrData count]* 320, result.height);
    }
    else {
        scrollImageView.contentSize = CGSizeMake([self.arrData count]* 320, result.height);
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    //-- change title color
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
}



#pragma mark - Add sub view
- (UIView*)footerView
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"SlideView"
                                                   owner:nil options:nil];
    return (id)[views objectAtIndex:2];
}

- (void)reload:(NSDictionary*)dict index:(NSInteger)index
{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 568) {
        //self.sv.contentSize = CGSizeMake([arr count]* 320, self.height - 20);
        scrollImageView.contentSize = CGSizeMake([self.arrData count]* 320, result.height);
    }
    else {
        scrollImageView.contentSize = CGSizeMake([self.arrData count]* 320, result.height);
    }
    
    if ([scrollImageView.subviews count] > 0) {
        for (UIView *v in scrollImageView.subviews) {
            for (UIImageView *imgView in v.subviews ) {
                if (imgView.tag == index) {
                    NSLog(@"\n\nindex image %d\n\n",index);
                    UIImage *img = [dict objectForKey:@"image"];
                    CGFloat height = img.size.height;
                    CGFloat width = img.size.width;
                    
                    float widthScale = 0;
                    float heightScale = 0;
                    
                    widthScale = 320/width;
                    heightScale =  480/height;//self.height/height;
                    
                    float scale = MIN(widthScale, heightScale);
                    //                    if (scale < 1) {
                    CGSize newSize;
                    CGSize result = [[UIScreen mainScreen] bounds].size;
                    if (scale == widthScale) {
                        newSize = CGSizeMake(320, height*320/width);
                    }
                    else
                    {
                        newSize = CGSizeMake(width* result.height/height,  result.height);
                        
                    }
                    NSLog(@"newSize w %f - h %f",newSize.width,newSize.height);
                    
                    // img = [img scaleToSize:newSize];
                    //                    }
                    [imgView setImage:img];
                    // [imageZoom  setImage:img];
                    return;
                }
                else {
                    continue;
                }
            }
        }
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setupView
{
    if(self.arrData == nil) {
        self.arrData = [[NSMutableArray alloc] init];
    }
    self.sv = [SlideView SlideViewFromNib];
    self.sv.delegate = (id)self;
    [self.sv confiugreWithArr:self.arrData];
    
    [self.sv setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    [self.view addSubview:self.sv];
    
    UIPinchGestureRecognizer *pinchGesture =
    [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    [self.sv addGestureRecognizer:pinchGesture];
    
    
    UITapGestureRecognizer *tapGR;
    tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGR.numberOfTapsRequired = 1;
    [self.sv addGestureRecognizer:tapGR];
    
    //    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    //        [self downloadImageForSlide];
    //    });
}
-(void) setGesturestoZoomImage
{
    
}
- (void)clickDone
{
    NSLog(@"clickDone");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)downloadImageForSlide
{
    for (NSInteger index = 0; index < [self.arrData count]; index ++) {
        NSMutableDictionary *dict = [self.arrData objectAtIndex:index];
        //        NSString *fbid = [[dict objectForKey:@"photo"] objectForKey:@"fbid"];
        //        dispatch_async(dispatch_get_main_queue(), ^{
        //            //            NSString *fbid = [[dict objectForKey:@"photo"] objectForKey:@"fbid"];
        //            [FacebookUtils getDetailPhoto:fbid];
        //        });
        
        NSMutableArray *arr = [[dict objectForKey:@"photo"] objectForKey:@"images"];
        NSString *url = @"";
        if (arr.count == 0) {
            url = [dict objectForKey:@"src"];
        }
        else {
            if ([arr count] > 1) url = [[arr objectAtIndex:1] objectForKey:@"src"];
            else url = [[arr objectAtIndex:0] objectForKey:@"src"];
        }
        
        url = [url stringByReplacingOccurrencesOfString:@"_s." withString:@"_n."];
        
        NSLog(@"started url :%@",url);
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        UIImage* myImage = [UIImage imageWithData:
                            [NSData dataWithContentsOfURL:
                             [NSURL URLWithString: url]]];
        
        NSString* key = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"photo"] objectForKey:@"fbid"]];
        [self.imageDic setObject:myImage forKey:key];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"loaded url :%@",url);
        
        if (myImage != nil) {
            [dict setObject:myImage forKey:@"image"];
        }
        
        
        //        [mstory replaceObjectAtIndex:index withObject:dict];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //[self.sv reload:dict index:index];
            //            [imgObj reload:dict index:index ];
            ImageViewObj *imageViewObj = (ImageViewObj*)[arrObj objectAtIndex:index];
            [imageViewObj showImage:myImage];
        });
    }
    
}



#pragma mark - FacebookUtilsDelegate

- (void)facebook:(FacebookUtils *)fbU didGetDetailPhotoFinish:(NSDictionary *)result
{
    NSLog(@"didGetDetailPhotoFinish: \n\t result: \t %@",result);
    
    NSMutableArray *arr = [result objectForKey:@"data"];
    if (arr.count == 0) return;
    //NSDictionary*dict = [arr objectAtIndex:0];
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self.sv reloadInfor:dict];
        //        [imgObj reloadInfor:dict];
    });
}


- (void)facebook:(FacebookUtils *)fbU didGetDetailPhotoFail:(NSError *)error
{
    NSLog(@"didGetDetailPhotoFail error : %@",[error description]);
}


- (void)facebook:(FacebookUtils *)fbU didLikeFinish:(NSDictionary *)result
{
    NSLog(@"ImageViewController didLikeFinish");
    self.isLikeClick = 0;
    DLOG(@"");    
    [FacebookUtils getDetailPhoto:self.activeObject_id];
    self.activeObject_id = @"";
}


- (void)facebook:(FacebookUtils *)fbU didLikeFail:(NSError *)error
{
    self.isLikeClick = 0;
    NSLog(@"didLikeFail error : %@",[error description]);
}


- (void)facebook:(FacebookUtils *)fbU didCommentFinish:(NSDictionary *)result
{
    NSLog(@"ImageViewController didCommentFinish");
    [FacebookUtils getDetailPhoto:self.activeObject_id];
    self.activeObject_id = @"";
}


- (void)facebook:(FacebookUtils *)fbU didCommentFail:(NSError *)error
{
    NSLog(@"didCommentFail error : %@",[error description]);
    
}



#pragma mark - GET INFOR IMAGE DETAIL

- (void)getInforImage
{
    for (NSInteger index = 0; index < [self.arrData count]; index ++) {
        NSMutableDictionary *dict = [self.arrData objectAtIndex:index];
        NSString *fbid = [[dict objectForKey:@"photo"] objectForKey:@"fbid"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [FacebookUtils getDetailPhoto:fbid];
        });
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.sv reload:dict index:index];
        });
    }
    
}



#pragma mark - Action sharePost 

-(void)sharePostFromSlidePhoto
{
    DLOG(@"");
    [self shareRangeFromSlidePhoto];    
    [self addGestureRecognizertoImage:NO];
}


- (void)shareRangeFromSlidePhoto
{
    NSLog(@"doing share post");
    PostObj *postObj = [ControlObj getPostWithKey:activePostid];
    ShareViewController *svc;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480) {
        svc = [[ShareViewController alloc]initWithNibName:@"ShareViewController" bundle:nil];
    }
    else
    {
        svc = [[ShareViewController alloc]initWithNibName:@"ShareViewController-568" bundle:nil];
    }
    [svc setPostObj:postObj];
    svc.delegate = (id)self;
    svc.isView = @"SlidePhoto";
    //self.view.userInteractionEnabled = FALSE;
    [self presentPopupViewController:svc animationType:MJPopupViewAnimationSlideBottomTop bckClickable:YES];
}



#pragma mark - UIWebViewDelegate Methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    NSLog(@"webView shouldStartLoadWithRequest:");
    NSLog(@"...navigationType=%d",navigationType);
    NSLog(@"...request=%@",request);
    
    NSString *urlString = request.URL.absoluteString;
    
    NSLog(@"come on: %@",urlString);
    return YES;
}



#pragma mark - DelegateClickSlideViewButton

- (void)clickSlideViewButtonIndex:(NSInteger)index
{
    NSLog(@"clickSlideViewButtonIndex");
    if (isTapOnce) {
        isTapOnce = 0;
    }
    else {
        isTapOnce = 1;        
    }
}


- (void)like:(NSString *)object_id
{
    NSLog(@"%@",object_id);
    self.activeObject_id = object_id;    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:warningControllFacebook] || [[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"] == 0) {
        
        [FacebookUtils like:self.activeObject_id HTTPMethod:@"POST"];
        return;
    }    
    
    TutorViewController *tuto = nil;
    // check only click Like
    if (self.isLikeClick == 0)
    {
        self.isLikeClick = 1;
        if (isIphone5)
            tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController-568h" bundle:nil];
        else
            tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
        
        tuto.delegate = (id)self;
        tuto.type = typeLike;
        [self presentPopupViewController:tuto animationType:MJPopupViewAnimationSlideBottomTop bckClickable:NO];
    }
}


- (void)unLike:(NSString *)object_id
{
    self.activeObject_id = object_id;    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:warningControllFacebook] integerValue] == 1 ) {
        
       [FacebookUtils like:self.activeObject_id HTTPMethod:@"DELETE"];
        return;
    }    
    
    /*
    TutorViewController *tuto = nil;
    if (isIphone5)
        tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController-568h" bundle:nil];
    else
        tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
    
    tuto.delegate = (id)self;
    tuto.type = typeUnlike;
    [self presentPopupViewController:tuto animationType:MJPopupViewAnimationSlideBottomTop bckClickable:YES];
     */
     
}


- (void)comment:(NSString *)object_id
{
    NSLog(@"%@",object_id);
    self.activeObject_id = object_id;
    CommentImageViewController *sfivc ;
    CGSize result = [[UIScreen mainScreen] bounds].size;
    
    if (result.height == 480) {
        sfivc = [[CommentImageViewController alloc] initWithNibName:@"CommentImageViewController" bundle:nil];
    }
    else {
        sfivc = [[CommentImageViewController alloc] initWithNibName:@"CommentImageViewController-568" bundle:nil];
    }
    
    NSString* key = [NSString stringWithFormat:@"%@",object_id];
    UIImage* img = (UIImage*)[self.imageDic objectForKey:key];
    
    [sfivc setImage:img];
    sfivc.postId = object_id;
    [sfivc setPost:self.post];
    sfivc.delegate = (id)self;
    [self presentPopupViewController:sfivc animationType:MJPopupViewAnimationSlideBottomTop bckClickable:YES];
}


- (void)commentPhoto:(NSString *)object_id image:(UIImage *)image
{
    NSLog(@"%@",object_id);
    self.activeObject_id = object_id;
    CommentImageViewController *sfivc ;
    CGSize result = [[UIScreen mainScreen] bounds].size;
    
    if (result.height == 480) {
        sfivc = [[CommentImageViewController alloc] initWithNibName:@"CommentImageViewController" bundle:nil];
    }
    else {
        sfivc = [[CommentImageViewController alloc] initWithNibName:@"CommentImageViewController-568" bundle:nil];
    }
    
    [sfivc setImage:image];
    sfivc.postId = object_id;
    sfivc.delegate = (id)self;
     [sfivc setPost:self.post];
    [self presentPopupViewController:sfivc animationType:MJPopupViewAnimationSlideBottomTop bckClickable:YES];
}


- (void)likePhoto:(NSString *)object_id
{
     self.activeObject_id = object_id;
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:warningControllFacebook] integerValue] == 1 || [[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"] == 0) {
        
        [FacebookUtils like:self.activeObject_id HTTPMethod:@"POST"];
        
    }
    else
    {
        TutorViewController *tuto = nil;
        // check only click Like
        if (self.isLikeClick == 0)
        {
            self.isLikeClick = 1;
            if (isIphone5)
                tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController-568h" bundle:nil];
            else
                tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
            
            tuto.delegate = (id)self;
            tuto.type = typeLike;
            [self presentPopupViewController:tuto animationType:MJPopupViewAnimationSlideBottomTop bckClickable:NO];
        }
    }
    
}


- (void)unlikePhoto:(NSString *)object_id
{
    self.activeObject_id = object_id;
    self.isLikeClick = 0;
   [FacebookUtils like:self.activeObject_id HTTPMethod:@"DELETE"];
}


- (void)acceptLike:(warningType)type
{
    NSLog(@"acceptLike");
    self.isLikeClick = 0;
    switch (type) {
        case typeLike: {
           [FacebookUtils like:self.activeObject_id HTTPMethod:@"POST"];
            break;
        }
        case typeUnlike: {
           [FacebookUtils like:self.activeObject_id HTTPMethod:@"DELETE"];
            break;
        }
        default:
            break;
    }
    
    self.activeObject_id = @"";
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}


- (void)cancelLike
{
    NSLog(@"cancelLike");
    self.isLikeClick = 0;
    self.activeObject_id = @"";
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}


- (void)showView
{
    [self showFooterView];
    self.btnDone.hidden = NO;
    isTapOnce = 0;
}


- (void)showPrivacyTip
{
    //
    //[self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.delegate && [self.delegate respondsToSelector:@selector(showPrivacyTip)]) {
        [self.delegate showPrivacyTip];
    }
}


- (void)showFooterView
{    
    if ([scrollImageView.subviews count] > 0) {
        for (UIView *v in scrollImageView.subviews) {
            for (UIImageView *imgView in v.subviews ) {
                if (imgView.tag == self.indexShow) {
                    NSLog(@"Tag image %d",imgView.tag);
                    UIView *footerView1 = [imgView viewWithTag:100000];
                    //footerView = [imgView viewWithTag:100000];
                    [footerView1 setHidden:NO];
                    return;
                }
                else {
                    continue;
                }
            }
        }
    }
}


- (void)hideFooterView
{
    if ([scrollImageView.subviews count] > 0) {
        for (UIView *v in scrollImageView.subviews) {
            for (UIImageView *imgView in v.subviews ) {
                if (imgView.tag == self.indexShow) {
                    UIView *footerView = [imgView viewWithTag:100000];
                    [footerView setHidden:YES];
                    return;
                }
                else {
                    continue;
                }
            }
        }
    }    
}



#pragma mark - Hide button done 
- (void)hideButtonDone{
    self.btnDone.hidden = YES;
    isTapOnce = 0;
}



#pragma mark - commentImageDelegate

- (void)dismissViewCommentImage
{
    NSLog(@"dismissViewCommentImage");
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}


- (void)dismissViewsharePost
{
    NSLog(@"dismissViewsharePost");
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}


- (void)addCountDownView:(CountDownViewController*)controller
{
    UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromRight;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
    [UIView setAnimationDuration:1];
    [UIView setAnimationDelegate:controller];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    [self presentViewController:controller animated:YES completion:nil];
    [UIView commitAnimations];
}



#pragma mark UIScrollViewDelegate methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    DLOG(@"%f",scrollView.contentOffset.x);
    self.indexShow = scrollImageView.contentOffset.x / 320;
    DLOG(@"%d",self.indexShow);
    [self showView];
}



#pragma mark TapDetectingImageViewDelegate methods

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer {
    // single tap does nothing for now
    ImageViewObj *imgObj = (ImageViewObj*)[arrObj objectAtIndex:self.indexShow];
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        if (isTapOnce) {
                DLOG(@"UIGestureRecognizerStateEnded");
                imgObj.fouterView.hidden = NO;
                self.btnDone.hidden = NO;
                isTapOnce = 0;
            }
            else {
                imgObj.fouterView.hidden = YES;
                self.btnDone.hidden = YES;
                isTapOnce = 1;
            }
        }
}


- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    // two-finger tap zooms out
    float newScale = [scrollImageView zoomScale] / ZOOM_STEP;
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    [scrollImageView zoomToRect:zoomRect animated:YES];
}



#pragma mark Utility methods

- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center
{
    
    CGRect zoomRect;
    
    // the zoom rect is in the content view's coordinates.
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    zoomRect.size.height = [scrollImageView frame].size.height / scale;
    zoomRect.size.width  = [scrollImageView frame].size.width  / scale;
    
    // choose an origin so as to get the right center.
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    NSLog(@"\n\nzoomRect.origin.x %f \n\n",zoomRect.origin.x);
    
    return zoomRect;    
}



#pragma mark - UITapGestureRecognizer funtion

-(void)handleTap:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        DLOG(@"UIGestureRecognizerStateEnded");
        if (isTapOnce) {
            [self.sv showFooterView];
            self.btnDone.hidden = NO;
            isTapOnce = 0;
        }
        else {
            [self.sv hideFooterView];
            self.btnDone.hidden = YES;
            isTapOnce = 1;
        }
    }
}



#pragma mark - handlePinchGesture

-(IBAction) handlePinchGesture:(UIGestureRecognizer *) sender
{
    NSLog(@"Pinch");
    CGFloat factor = [(UIPinchGestureRecognizer *) sender scale];
    DLOG(@"zoom : %f",factor);
    if (factor > 1) {
        //---zooming in---
        //        sender.view.transform = CGAffineTransformMakeScale(
        //                                                           lastScaleFactor + (factor-1),
        //                                                           lastScaleFactor + (factor-1));
    }
    else {
        //        //---zooming out---
        //        sender.view.transform = CGAffineTransformMakeScale(lastScaleFactor * factor, lastScaleFactor * factor);
        //    }
        //    if (sender.state == UIGestureRecognizerStateEnded) {
        //        if (factor > 1) {
        //            lastScaleFactor += (factor-1);
        //        } else {
        //            lastScaleFactor *= factor;
        //        }
    }
}


- (IBAction)clickLike:(id)sender
{
    //[self like];
}


- (IBAction)clickComment:(id)sender
{
    
}



@end
