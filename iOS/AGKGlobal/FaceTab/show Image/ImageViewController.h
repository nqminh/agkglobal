//
//  ImageViewController.h
//  AGKGlobal
//
//  Created by Thuy Dao on 8/21/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideView.h"
#import "FacebookUtils.h"
#import "UIViewController+MJPopupViewController.h"
#import "TutorViewController.h"
#import "CommentImageViewController.h"
#import "ImageViewObj.h"
#import <QuartzCore/QuartzCore.h>
#import "ShareViewController.h"

@protocol showTipDelegate <NSObject>
- (void) showPrivacyTip ;
@end

@interface ImageViewController : UIViewController  <FacebookUtilsDelegate,
                                                    DelegateClickSlideViewButton,
                                                    tutorDelegate,
                                                    UIScrollViewDelegate,
                                                    DelegateImageViewObject,UIGestureRecognizerDelegate,UIWebViewDelegate>
{
    int isTapOnce;    
    CGFloat lastScale;
    
    IBOutlet UIScrollView *scrollImageView;
    //UIScrollView *scrollImageView;
    UIImageView *imageZoom;    
    
    //IBOutlet UIView *footerView;
//    NSString *object_id;
   // footerView *footerView1;
    //footerView *slv;
    //ImageViewObj *imgObj;
    
    NSMutableArray* arrObj;
    
    UITapGestureRecognizer *singleTap;
    NSInteger typeCoutDown;
}

@property (strong,nonatomic) NSMutableArray         *arrData;
@property (strong,nonatomic) SlideView              *sv;
@property (strong,nonatomic) NSString               *activeObject_id;
@property (strong,nonatomic) NSMutableDictionary    *imageDic;
@property (strong,nonatomic) UIButton               *btnDone;

// for image view
@property(nonatomic,strong) NSMutableDictionary     *dataSource;
@property(assign) NSInteger                         height;
@property(assign) NSInteger                         width;
@property(nonatomic,strong) NSMutableDictionary     *layoutDict;
@property(nonatomic,assign) BOOL                    isShow;
@property(nonatomic,assign) NSInteger               indexShow;
@property (nonatomic,assign) NSInteger           isLikeClick;

// foooter view
@property (weak, nonatomic) IBOutlet UILabel *lblLikeNumber;

@property (weak, nonatomic) IBOutlet UILabel *lbllikeText;

@property (weak, nonatomic) IBOutlet UILabel *lblCommentNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblCommentText;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
// active post id
@property (nonatomic,retain) NSString                       *activePostid;
@property (nonatomic,assign) NSInteger                      isShowButtonShare;
@property (nonatomic,assign) NSInteger                      isShowButtonShareSubPost;
// post Object
@property (nonatomic,retain) PostObj                        *post;
//
@property(nonatomic,assign) id<showTipDelegate> delegate ;
// action

- (IBAction)clickLike:(id)sender;

- (IBAction)clickComment:(id)sender;

@end
