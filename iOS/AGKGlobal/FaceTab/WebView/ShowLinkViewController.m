//
//  ShowLinkViewController.m
//  AGKGlobal
//
//  Created by Thuy on 9/4/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "ShowLinkViewController.h"

@interface ShowLinkViewController ()

@end

@implementation ShowLinkViewController
@synthesize isViewShare;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        //custom
    }
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
   
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSURL* nsUrl = [NSURL URLWithString:self.strLinkUrl];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:60];
    
    [self.wvShowLink loadRequest:request];
    
    // optimize speed scroll uiwebview content bug #87.
    self.wvShowLink.scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
    
    AppDelegate *dataCenter = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // show right bar button  
    if ([self.strLinkUrl isEqualToString:@"https://m.facebook.com/legal/terms"] || [self.strLinkUrl isEqualToString:@"http://www.besafeapps.com/smsiphone.html"] || [self.strLinkUrl isEqualToString:@"https://twitter.com/privacy"] || (dataCenter.isOpenTweetLink == YES))
    {
        self.navigationItem.rightBarButtonItem = nil;
        
    }else{
        UIBarButtonItem *rightbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Share" style:UIBarButtonItemStyleBordered target:self action:@selector(shareLink:)];
        self.navigationItem.rightBarButtonItem = rightbarBtnItem;
    }
    
}



#pragma mark - navigation bar button
-(void) clicktoBack
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)shareLink:(id)sender
{
    NSLog(@"shareLink");
    
    ShareViewController *svc;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480) {
        svc = [[ShareViewController alloc]initWithNibName:@"ShareViewController" bundle:nil];
    }
    else
    {
        svc = [[ShareViewController alloc]initWithNibName:@"ShareViewController-568" bundle:nil];
    }
    //PostObj *post = [[PostObj alloc] initWitProfile:self.post withPermaling:self.strLinkUrl];
    [svc setPostObj:self.postObj];
    svc.isView = @"ShowLink";
    [self.navigationController pushViewController:svc animated:YES];    
}



#pragma mark - UIWebViewDelegate Methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSLog(@"shouldStartLoadWithRequest:%d",webView.tag);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    return YES;
}


- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"webViewDidStartLoad:%d",webView.tag);
}


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"webViewDidFinishLoad:%d",webView.tag);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    // optimize speed scroll uiwebview content bug #87.
    self.wvShowLink.scrollView.decelerationRate = UIScrollViewDecelerationRateFast;

}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"webView didFailLoadWithError:%d",webView.tag);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


@end
