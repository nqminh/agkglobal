//
//  ShowLinkViewController.h
//  AGKGlobal
//
//  Created by Thuy on 9/4/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareViewController.h"
#import "AppDelegate.h"

@interface ShowLinkViewController : UIViewController <UIWebViewDelegate>
{
    
}

@property (nonatomic,retain) IBOutlet UIWebView             *wvShowLink;
@property (nonatomic,retain) NSString                       *strLinkUrl;
@property (nonatomic,strong) User                           *post;
@property (nonatomic,retain) NSString                       *isViewShare;
@property (nonatomic,strong) PostObj                        *postObj;
@end
