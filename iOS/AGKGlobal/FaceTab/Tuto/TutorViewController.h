
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "JSQSystemSoundPlayer.h"
#import <AudioToolbox/AudioToolbox.h>

enum {
    typeLike            = 0,
    typeShare           = 1,
    typeComment         = 32,
    typeUnlike          = 3,
    typePostStatus      = 4,
    typePostPhoto       = 5,
    typePostCheckin     = 6,
    typeTWFavorite      = 7,
    typeTWUnFavorite    = 8,
    typeTWRetweet       = 9
};
typedef NSInteger warningType;

@protocol TutorViewController;

@protocol tutorDelegate<NSObject>
- (void)acceptLike:(warningType) type;
- (void)cancelLike;
- (void) showPrivacyTip;

@optional

@end

@interface TutorViewController : UIViewController
{
    BOOL                    isTouch;
    BOOL                    isSilent;
    AVAudioPlayer           *soundEffect;
    NSInteger               timeSec;
    BOOL                    isPlayingSound;
    NSInteger               alertType;
    NSInteger               timePause;
}

@property (nonatomic,retain) IBOutlet UILabel           *lbcontent;
@property (nonatomic,retain) IBOutlet UIButton          *btnAction;
@property (nonatomic,retain) IBOutlet UIButton          *btnPlayPause;
@property (nonatomic,retain) IBOutlet UIAlertView       *alertView;
@property (assign, nonatomic) id <tutorDelegate>        delegate;
@property (assign,nonatomic) warningType                type;
@property (assign,nonatomic) int                        secondsLeft;
@property (assign, nonatomic) BOOL                      isTouch;

@property (nonatomic, retain) NSTimer                   *timerBackground;
@property (nonatomic, retain) NSTimer                   *timerResume;
@property (weak, nonatomic) IBOutlet UILabel            *lblCoundown;
@property (assign,nonatomic) int                        isOn;
@property (nonatomic) int                               sendType;
@property (nonatomic,retain) NSTimer                    *timer;

//-- ACTION
- (IBAction)clickCancel:(id)sender;
- (IBAction)clickLike:(id)sender;
- (IBAction)clickMoreInfo:(id)sender;
- (IBAction)clickHide:(id)sender;
- (IBAction)clickToBtnPlayPause:(id)sender;


@end

