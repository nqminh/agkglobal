//
//  ItsAMatchViewController.m
//  Besafe
//
//  Created by Hong Vu Xuan on 7/1/13.
//  Copyright (c) 2013 VMODEV. All rights reserved.
//

#import "TutorViewController.h"
#import "ShowLinkViewController.h"
#import "User.h"
#import "JSQSystemSoundPlayer.h"

#define ALERT_TYPE_BANNED   1
#define ALERT_TYPE_NORMAL   2


@interface TutorViewController ()

- (IBAction)clickCancel:(id)sender;

@end

@implementation TutorViewController
@synthesize delegate = _delegate;
@synthesize sendType, timer, timerBackground, timerResume, type;
@synthesize btnPlayPause, btnAction;
@synthesize isTouch, alertView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        [self config];
    }
    return self;
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //-- facebook config
    [self config];
    
    //-- twitter config
    [self configTwitter];
    
    //-- set default time
    self.secondsLeft = [[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"];
    timeSec = 11;
    self.lblCoundown.text = [NSString stringWithFormat:@"%d",self.secondsLeft];
    
    //-- check vibrate/sound mode
    [self ifSilentMode];
    
    //--start countdown when Application Did Become Active Notification
    BOOL isPlayPause = [[NSUserDefaults standardUserDefaults] boolForKey:@"PLAY_PAUSE"];
    if (isPlayPause)
        [self startResume];
    else
        [self startCountdown];        
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //isTouch = YES;
}


- (void)viewDidDisappear:(BOOL)animated
{
    //[soundEffect stop];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}



#pragma mark - AudiotoolBox

- (void)ifSilentMode
{
    CFStringRef state;
    UInt32 propertySize = sizeof(CFStringRef);
    AudioSessionInitialize(NULL, NULL, NULL, NULL);
    AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &propertySize, &state);
    
    NSString *aNSString = (__bridge NSString *)state;
    
    if ([aNSString isEqualToString:@"Speaker"]) {
        //SILENT
        DLOG(@"Silent switch is on");
        isSilent = NO;
    }else{
        //NOT SILENT
        DLOG(@"Silent switch is off");
        isSilent = YES;
    }
}


#pragma mark - Start countdown

- (void)startCountdown
{
    self.timerBackground = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timerBackground forMode:NSDefaultRunLoopMode];
    
    timeSec = [[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"];
    timeSec++;
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"IS_SENDING_MESSGE"];
    //[[NSUserDefaults standardUserDefaults] setInteger:timeSec forKey:@"TIMER_SECONDS"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.type forKey:@"SEND_TYPE"];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"APP_RESUME"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FORCE_SEND_MESSAGE"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doMyLayoutStuff)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}


- (void)startResume
{
    self.timerBackground = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerRepeat:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timerBackground forMode:NSDefaultRunLoopMode];
    
    if (!timeSec) {
        timeSec = [[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"];
        timeSec++;
    }else{
        [[NSUserDefaults standardUserDefaults] setInteger:timeSec forKey:@"TIMER_RESUME"];
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"IS_SENDING_MESSGE"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.type forKey:@"SEND_TYPE"];
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"APP_RESTARTED"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FORCE_SEND_MESSAGE"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doResume)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}


- (void)doMyLayoutStuff
{
    // stuff
    NSLog(@"doMyLayoutStuff:Return From Background");
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PLAY_PAUSE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    BOOL wasRestarted = [[NSUserDefaults standardUserDefaults] boolForKey:@"APP_RESTARTED"];
    
    if (wasRestarted) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSTimeInterval timer2 = [[NSDate date] timeIntervalSinceReferenceDate] - appDelegate.timer;
        NSInteger time = timer2;
        NSInteger timeSecs = [[NSUserDefaults standardUserDefaults] integerForKey:@"TIMER_SECONDS"];
        
        timeSec = timeSecs - time - 1;
        if (timeSec > 1) 
            [self.lblCoundown setText:[NSString stringWithFormat:@"%i",timeSec]];
        else
            [self.lblCoundown setText:@"1"];    
    }
}


- (void)doResume
{
    // stuff
    NSLog(@"doResume:Return From Background");
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PLAY_PAUSE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    BOOL wasResume = [[NSUserDefaults standardUserDefaults] boolForKey:@"APP_RESUME"];
    
    if (wasResume) {
        if (timeSec > 0) {
            [[NSUserDefaults standardUserDefaults] setInteger:timeSec forKey:@"TIMER_RESUME"];
            [self.lblCoundown setText:[NSString stringWithFormat:@"%i",timeSec]];
        }
    }
}



#pragma mark - Auto cancel

- (void) autoCancelTip
{
    DLOG(@"autoCancelTip123");
    int hours, minutes, seconds;
    
    self.secondsLeft--;
    hours = self.secondsLeft / 3600;
    minutes = (self.secondsLeft % 3600) / 60;
    seconds = (self.secondsLeft %3600) % 60;
    self.lblCoundown.text = [NSString stringWithFormat:@"%d",self.secondsLeft];
    if (self.secondsLeft == 1) {
        DLOG(@"autoCancelTip OFF");
        if (self.timerBackground != nil) [self.timerBackground invalidate];
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    }
}



#pragma mark - Config

- (void)config
{
    switch (self.type) {
        case typePostCheckin:
        {
            [self.lbcontent setText:[NSString stringWithFormat:textPostPrivacy,@"check in",@"check in"]];
            [self.btnAction setTitle:@"Check in Anyway" forState:UIControlStateNormal];
            break;
        }
        case typePostPhoto:
        {
            break;
        }
        case typePostStatus:
        {
            break;
        }
            
        case typeComment: {
            [self.lbcontent setText:[NSString stringWithFormat:textPostPrivacy,@"comment",@"comment"]];
            [self.btnAction setTitle:@"Comment Anyway" forState:UIControlStateNormal];
            break;
        }
        case typeLike: {
            [self.lbcontent setText:[NSString stringWithFormat:textPostPrivacy,@"like",@"like"]];
            [self.btnAction setTitle:@"Like Anyway" forState:UIControlStateNormal];
            break;
        }
        case typeUnlike: {
            [self.lbcontent setText:[NSString stringWithFormat:textPostPrivacy,@"unlike",@"unlike"]];
            [self.btnAction setTitle:@"UnLike Anyway" forState:UIControlStateNormal];
            break;
        }
        case typeShare: {
            [self.lbcontent setText:[NSString stringWithFormat:textPostPrivacy,@"share",@"share"]];
            [self.btnAction setTitle:@"Share Anyway" forState:UIControlStateNormal];
            break;
        }
            
        default:
            break;
    }
}


- (void)configTwitter
{
    switch (self.type) {
        case typeTWFavorite:
        {
            [self.lbcontent setText:[NSString stringWithFormat:textFavoritePrivacy,@"favorite",@"favorite"]];
            [self.btnAction setTitle:@"Favorite Anyway" forState:UIControlStateNormal];
            
            break;
        }
        case typeTWUnFavorite:
        {
            [self.lbcontent setText:[NSString stringWithFormat:textFavoritePrivacy,@"unfavorite",@"unfavorite"]];
            [self.btnAction setTitle:@"UnFavorite Anyway" forState:UIControlStateNormal];
            
            break;
        }
        case typeTWRetweet:
        {
            [self.lbcontent setText:[NSString stringWithFormat:textRetweetPrivacy,@"retweet",@"retweet"]];
            [self.btnAction setTitle:@"Retweet Anyway" forState:UIControlStateNormal];
            
            break;
        }
                    
        default:
            break;
    }
}



#pragma mark - ACTION



- (IBAction)clickCancel:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PLAY_PAUSE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //tat auto cancel tip .
    if (self.timerBackground != nil) [self.timerBackground invalidate];
    [self cancelView];
}


- (IBAction)clickLike:(id)sender
{
    [self autoLikeAnyway];
}


- (IBAction)clickMoreInfo:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PLAY_PAUSE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //tat auto cancel tip .
    if (self.timerBackground != nil) [self.timerBackground invalidate];
    if (soundEffect != nil) [soundEffect stop];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"IS_SENDING_MESSGE"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_SECONDS"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_RESUME"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SEND_TYPE"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SEND_ADDRESS"];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(showPrivacyTip)]) {
        [self.delegate showPrivacyTip];
    }
}


- (void) cancelView
{
    //tat auto cancel tip .
    if (self.timerBackground != nil) [self.timerBackground invalidate];
    if (soundEffect != nil) [soundEffect stop];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"IS_SENDING_MESSGE"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_SECONDS"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_RESUME"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SEND_TYPE"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SEND_ADDRESS"];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(cancelLike)]) {
        [self.delegate cancelLike];
    }
}

- (void) autoLikeAnyway
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PLAY_PAUSE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //tat auto cancel tip .
    if (self.timerBackground != nil) [self.timerBackground invalidate];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(acceptLike:)]) {
        [self.delegate acceptLike:self.type];
    }

}

- (IBAction)clickHide:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PLAY_PAUSE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //tat auto cancel tip .
    if (self.timerBackground != nil) [self.timerBackground invalidate];
    if (soundEffect != nil) [soundEffect stop];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"IS_SENDING_MESSGE"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_SECONDS"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_RESUME"];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SEND_TYPE"];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SEND_ADDRESS"];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[NSUserDefaults standardUserDefaults] synchronize];

    switch (self.type) {
        case typeComment: {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:warningControllFacebook];
            //[self cancelView];
            [self autoLikeAnyway];
            break;
        }
        case typeLike: {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:warningControllFacebook];
//            if (self.delegate && [self.delegate respondsToSelector:@selector(acceptLike:)]) {
//                [self.delegate acceptLike:self.type];
//            }
            //[self cancelView];
            [self autoLikeAnyway];
            break;
        }
        case typeUnlike: {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:warningControllFacebook];
            //[self cancelView];
            [self autoLikeAnyway];
            break;
        }
        case typeShare: {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:warningControllFacebook];
            //[self cancelView];
            [self autoLikeAnyway];
            break;
        }
        case typePostCheckin: {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:warningControllFacebook];
            //[self cancelView];
            [self autoLikeAnyway];
            break;
        }
            
        case typeTWFavorite: {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:warningControllTwitter];
//            if (self.delegate && [self.delegate respondsToSelector:@selector(acceptLike:)]) {
//                [self.delegate acceptLike:self.type];
//            }
            //[self cancelView];
            [self autoLikeAnyway];
            break;
        }
            
        case typeTWUnFavorite: {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:warningControllTwitter];
            //[self cancelView];
            [self autoLikeAnyway];
            break;
        }
            
        case typeTWRetweet: {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:warningControllTwitter];
            //[self cancelView];
            [self autoLikeAnyway];
            break;
        }
            
        default:
            break;
    }
}


- (IBAction)clickToBtnPlayPause:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PLAY_PAUSE"];
    
    if (btnPlayPause.currentImage == [UIImage imageNamed:@"button_pause.png"]) {
        [btnPlayPause setImage:[UIImage imageNamed:@"button_play.png"] forState:UIControlStateNormal];
        if (soundEffect != nil) [soundEffect stop];
        isPlayingSound = NO;
        if (self.timerBackground != nil) [self.timerBackground invalidate];
        
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"IS_SENDING_MESSGE"];
        [[NSUserDefaults standardUserDefaults] setInteger:timeSec forKey:@"TIMER_RESUME"];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SEND_TYPE"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SEND_ADDRESS"];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

    }else{
        [btnPlayPause setImage:[UIImage imageNamed:@"button_pause.png"] forState:UIControlStateNormal];
        [self startResume];
    }
        
    [[NSUserDefaults standardUserDefaults] synchronize];
}



#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.localNotification) {
        [[UIApplication sharedApplication] cancelLocalNotification:appDelegate.localNotification]; //Cancel the notification with the system
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PLAY_PAUSE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"countdown alert view:%d",buttonIndex);
    if (buttonIndex == 0) 
        [self cancelView];
    else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(acceptLike:)]) {
            [self.delegate acceptLike:self.type];
        }
    }
}


//Event called every time the NSTimer ticks.
- (void)timerTick:(NSTimer *)theTimer
{
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateActive)
        return;
    
    BOOL wasRestarted = [[NSUserDefaults standardUserDefaults] boolForKey:@"APP_RESTARTED"];
    BOOL forceSendMessage = [[ NSUserDefaults standardUserDefaults] boolForKey:@"FORCE_SEND_MESSAGE"];
    
    if (!wasRestarted || timeSec > 1) {
        
        [[NSUserDefaults standardUserDefaults] setInteger:timeSec forKey:@"TIMER_SECONDS"];
        timeSec--;
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"voiceCountdown"] && !isPlayingSound && timeSec > 0) {
            //--Countdown set On .
            NSInteger fileTime = timeSec;
            if (fileTime>10) fileTime = 10;
            NSLog(@"fileTime:%d",fileTime);
            
            if (isSilent == YES)
            {
                DLOG(@"SILENT AND RINGTONE");
                
               
                NSURL *file = [[NSURL alloc] initFileURLWithPath: [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"full%d",fileTime]  ofType:@"mp3"]];
                
                soundEffect = [[AVAudioPlayer alloc] initWithContentsOfURL:file error:nil];
                [soundEffect setNumberOfLoops:0];
                NSError *setCategoryErr = nil;
                NSError *activationErr  = nil;
                [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error:&setCategoryErr];
                [[AVAudioSession sharedInstance] setActive:YES error:&activationErr];
                [soundEffect prepareToPlay];
                if (timeSec <= 10) {
                    [soundEffect play];
                    
                } else {
                    NSTimeInterval then = (NSTimeInterval)(timeSec-10);
                    [soundEffect playAtTime:soundEffect.deviceCurrentTime+then];
                }
                isPlayingSound = YES;

            }
            else
            {
                DLOG(@"RINGTONE");
                 [[JSQSystemSoundPlayer sharedPlayer] playVibrateSound];
                NSURL *file = [[NSURL alloc] initFileURLWithPath: [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"full%d",fileTime]  ofType:@"mp3"]];
                
                soundEffect = [[AVAudioPlayer alloc] initWithContentsOfURL:file error:nil];
                [soundEffect setNumberOfLoops:0];
                NSError *setCategoryErr = nil;
                NSError *activationErr  = nil;
                [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error:&setCategoryErr];
                [[AVAudioSession sharedInstance] setActive:YES error:&activationErr];
                [soundEffect prepareToPlay];
                if (timeSec <= 10) {
                    [soundEffect play];
                    
                } else {
                    NSTimeInterval then = (NSTimeInterval)(timeSec-10);
                    [soundEffect playAtTime:soundEffect.deviceCurrentTime+then];
                }
                //isPlayingSound = YES;
            }
        }
        else
        {
            //--Voice countdown set off.
            //--Check if device set mode silent. 
            
                DLOG(@"SILIEN MODE");
               [[JSQSystemSoundPlayer sharedPlayer] playVibrateSound];
            
        }

        if (timeSec > 0) {
            [self.lblCoundown setText:[NSString stringWithFormat:@"%i",timeSec]];
        }
        
        if (timeSec == 0 || forceSendMessage)
        {
            if (theTimer != nil) [theTimer invalidate];
            if (soundEffect != nil) [soundEffect stop];
            
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"IS_SENDING_MESSGE"];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PLAY_PAUSE"];            
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_SECONDS"];
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_RESUME"];
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SEND_TYPE"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SEND_ADDRESS"];
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //[self cancelView];
            [self autoLikeAnyway];
        }
    }else{
        
        NSString *alertTypeText;
        NSString *buttonText;
        NSString *actionText;
        switch (self.type) {
            case typeCheckin:
            {
                alertTypeText = @"Post";
                buttonText = @"Check in Anyway";
                actionText = @"post";
                break;
            }
            case typeComment:
            {
                alertTypeText = @"Post";
                buttonText = @"Comment Anyway";
                actionText = @"comment";
                break;
            }
            case typeLike:
            {
                alertTypeText = @"Post";
                buttonText = @"Like Anyway";
                actionText = @"like";
                break;
            }
            case typeShare:
            {
                alertTypeText = @"Post";
                buttonText = @"Share Anyway";
                actionText = @"share";
                break;
            }
            case typePostCheckin:
            {
                alertTypeText = @"Post";
                buttonText = @"Check in Anyway";
                actionText = @"check in";
                break;
            }
            case typeTWFavorite:
            {
                alertTypeText = @"Tweet";
                buttonText = @"Favorite Anyway";
                actionText = @"favorite";
                break;
            }
            case typeTWRetweet:
            {
                alertTypeText = @"Tweet";
                buttonText = @"Retweet Anyway";
                actionText = @"retweet";
                break;
            }
                
            default:
                break;
        }
        
        alertType = ALERT_TYPE_NORMAL;
        
        if (timeSec > 0) {
            //[self cancelView];
            [self autoLikeAnyway];
        }else{
            if (theTimer != nil) [theTimer invalidate];
            timeSec = 1;
            [self.lblCoundown setText:@"1"];
            if (soundEffect != nil) [soundEffect stop];
            if (!alertView) {
                alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"BeSafe %@",alertTypeText]
                                                       message:[NSString stringWithFormat:@"BeSafe Apps was restarted. Are you sure you want to %@ this message?",actionText]
                                                      delegate:self
                                             cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:buttonText, nil];
                
                [alertView show];
            }        
        }
    }
}


//Event called every time the NSTimer repeat.
- (void)timerRepeat:(NSTimer *)theTimer
{
    BOOL forceSendMessage = [[ NSUserDefaults standardUserDefaults] boolForKey:@"FORCE_SEND_MESSAGE"];
    BOOL wasResume = [[NSUserDefaults standardUserDefaults] boolForKey:@"APP_RESUME"];

    if (!wasResume || timeSec > 1){
        [[NSUserDefaults standardUserDefaults] setInteger:timeSec forKey:@"TIMER_RESUME"];
        timeSec--;
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"voiceCountdown"] && !isPlayingSound && timeSec > 0)
        {
            NSInteger fileTime = timeSec;
            if (fileTime>10) fileTime = 10;
            NSLog(@"fileTime:%d",fileTime);
            
            if (isSilent == YES)
            {
                DLOG(@"silent");
                [[JSQSystemSoundPlayer sharedPlayer] playVibrateSound];
            }
            else
            {
                DLOG(@"ringtone");
                [[JSQSystemSoundPlayer sharedPlayer] playVibrateSound];
                NSURL *file = [[NSURL alloc] initFileURLWithPath: [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"full%d",fileTime]  ofType:@"mp3"]];
                
                soundEffect = [[AVAudioPlayer alloc] initWithContentsOfURL:file error:nil];
                [soundEffect setNumberOfLoops:0];
                NSError *setCategoryErr = nil;
                NSError *activationErr  = nil;
                [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error:&setCategoryErr];
                [[AVAudioSession sharedInstance] setActive:YES error:&activationErr];
                [soundEffect prepareToPlay];
                if (timeSec <= 10) {
                    [soundEffect play];
                    
                } else {
                    NSTimeInterval then = (NSTimeInterval)(timeSec-10);
                    [soundEffect playAtTime:soundEffect.deviceCurrentTime+then];
                }
                //isPlayingSound = YES;
            }
        }
        else
        {
            [[JSQSystemSoundPlayer sharedPlayer] playVibrateSound];
        }
        
        
        if (timeSec > 0) {
            [self.lblCoundown setText:[NSString stringWithFormat:@"%i",timeSec]];
        }
        
        if (timeSec == 0 || forceSendMessage)
        {
            if (theTimer != nil) [theTimer invalidate];
            if (soundEffect != nil) [soundEffect stop];
            
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"IS_SENDING_MESSGE"];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PLAY_PAUSE"];
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_SECONDS"];
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"TIMER_RESUME"];
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SEND_TYPE"];
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"SEND_ADDRESS"];
            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //[self cancelView];
            [self autoLikeAnyway];
        }
    }else{
        NSString *alertTypeText;
        NSString *buttonText;
        NSString *actionText;
        switch (self.type) {
            case typeCheckin:
            {
                alertTypeText = @"Post";
                buttonText = @"Check in Anyway";
                actionText = @"post";
                break;
            }
            case typeComment:
            {
                alertTypeText = @"Post";
                buttonText = @"Comment Anyway";
                actionText = @"comment";
                break;
            }
            case typeLike:
            {
                alertTypeText = @"Post";
                buttonText = @"Like Anyway";
                actionText = @"like";
                break;
            }
            case typeShare:
            {
                alertTypeText = @"Post";
                buttonText = @"Share Anyway";
                actionText = @"share";
                break;
            }
            case typePostCheckin:
            {
                alertTypeText = @"Post";
                buttonText = @"Check in Anyway";
                actionText = @"check in";
                break;
            }
            case typeTWFavorite:
            {
                alertTypeText = @"Tweet";
                buttonText = @"Favorite Anyway";
                actionText = @"favorite";
                break;
            }
            case typeTWRetweet:
            {
                alertTypeText = @"Tweet";
                buttonText = @"Retweet Anyway";
                actionText = @"retweet";
                break;
            }
                
            default:
                break;
        }        
        
        alertType = ALERT_TYPE_NORMAL;
        
        if (timeSec > 1) {
            [self cancelView];
        }else{
            if (theTimer != nil) [theTimer invalidate];
            timeSec = 1;
            [self.lblCoundown setText:@"1"];
            if (soundEffect != nil) [soundEffect stop];
            if (!alertView) {
                alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"BeSafe %@",alertTypeText]
                                                       message:[NSString stringWithFormat:@"BeSafe Apps was restarted. Are you sure you want to %@ this message?",actionText]
                                                      delegate:self
                                             cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:buttonText, nil];
                [alertView show];
            }
        }
    }
}



@end
