//
//  SendFacebookImageViewController.h
//  AGKGlobal
//
//  Created by Jeff Jolley on 5/29/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FacebookWebViewController.h"
#import "MBProgressHUD.h"
#import "SSTextView.h"

@class SendFacebookImageViewController;
@protocol sendFacebookImageDelegate <NSObject>

@optional

- (void)sendFacebookImageFinished:(SendFacebookImageViewController*)sendFacebookImage;
- (void)sendFacebookImageStart:(SendFacebookImageViewController*)sendFacebookImage;

@end

@interface SendFacebookImageViewController : UIViewController <UITableViewDataSource,UITableViewDelegate, UITextViewDelegate>
{
    IBOutlet UITableView*                   myTable;
    IBOutlet UITableViewCell*               cell0;
    IBOutlet UITableViewCell*               cell1;
    IBOutlet UITableViewCell*               cell2;
    // check banned word , address
    NSString *textSrc_sendImg;
    
    NSString *showBannedWordStr_sendImg;
    NSString *showBannedAddressStr_sendImg;
    
    NSArray *bannedWordArr_sendImg;
    NSArray *bannedAddressArr_sendImg;
    UIAlertView *alv_sendImg;

}
@property (nonatomic,retain) IBOutlet UITableView           *myTable;
@property (nonatomic,retain) IBOutlet UITableViewCell       *cell0;
@property (nonatomic,retain) IBOutlet UITableViewCell       *cell1;
@property (nonatomic,retain) IBOutlet UITableViewCell       *cell2;
@property (nonatomic,retain) IBOutlet UIImage               *image;
@property (nonatomic,retain) IBOutlet UIImageView           *imagePost;
@property (nonatomic,retain) IBOutlet UIButton              *postingSubmitButton;
@property (nonatomic,retain) IBOutlet SSTextView            *tvPost;
@property (nonatomic, assign)id<sendFacebookImageDelegate>  delegate;
@property (nonatomic, assign) NSInteger typeSouceImage;
@property (nonatomic,retain) NSMutableArray *arrBuddys;

@property (nonatomic,retain) NSString *textStt;

-(IBAction)posttowall:(id)sender;
- (void)countDownComplete;

@end
