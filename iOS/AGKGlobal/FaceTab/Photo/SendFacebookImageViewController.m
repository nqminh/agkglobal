//
//  SendFacebookImageViewController.m
//  AGKGlobal
//
//  Created by Jeff Jolley on 5/29/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "SendFacebookImageViewController.h"
#import <FacebookSDK/FacebookSDK.h> 
#import "BesafeAPI.h"  
#import "VMUsers.h"

@interface SendFacebookImageViewController ()

@end

@implementation SendFacebookImageViewController
@synthesize myTable;
@synthesize cell0;
@synthesize cell1;
@synthesize cell2;
@synthesize imagePost,typeSouceImage;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [myTable setScrollEnabled:NO];
    self.tvPost.delegate = self;
    
    // custom navigation bar button
    // left
    UIBarButtonItem *leftbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel:)];
    self.navigationItem.leftBarButtonItem = leftbarBtnItem;
    
    // right
    
    UIBarButtonItem *rightbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Post" style:UIBarButtonItemStyleBordered target:self action:@selector(postText:)];
    self.navigationItem.rightBarButtonItem = rightbarBtnItem;
    
    //set textview
    if ([self.textStt length] > 0)
    {
        self.tvPost.text = self.textStt;
    }
    else
    {
        [self.tvPost setPlaceholder:@"What's on your mind?"];
    }
     
     //[self.tvPost becomeFirstResponder];
    // 0 : type library ; 1 : type camera.
    //typeSouceImage = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{    
    //-- change title color
    
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    
    [self.imagePost setImage:self.image];
    [self.tvPost becomeFirstResponder];
}

#pragma mark - navigationbar button

- (void)cancel:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)postText:(UIButton*)sender
{
    [self.tvPost resignFirstResponder];
    
    // get data from banner words , banned addresses.
    // check banned word , banned address . 
    textSrc_sendImg = self.tvPost.text;
    bannedWordArr_sendImg = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_WORDS"] mutableCopy];
    bannedAddressArr_sendImg = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_ADDRESSES"] mutableCopy];      
   
    
    showBannedWordStr_sendImg = [Utility showBanned:textSrc_sendImg withArr:bannedWordArr_sendImg];
    showBannedAddressStr_sendImg = [Utility showBanned:textSrc_sendImg withArr:bannedAddressArr_sendImg];
    
    if(showBannedWordStr_sendImg.length > 0 )
    {
        if (showBannedAddressStr_sendImg.length >= 1)
        {
            NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_sendImg);
            alv_sendImg = [[UIAlertView alloc]initWithTitle:@"Banned Word and Address!" message:[NSString stringWithFormat:@"You have typed Banned Word and Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_sendImg show];
        }
        else{
            NSLog(@"\n\n banned word %@\n\n",showBannedWordStr_sendImg);
            alv_sendImg = [[UIAlertView alloc]initWithTitle:@"Banned Word!" message:[NSString stringWithFormat:@"You have typed Banned Word \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_sendImg show];
        }
        
    }
    else if (showBannedAddressStr_sendImg.length > 0)
    {
        NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_sendImg);
        alv_sendImg = [[UIAlertView alloc]initWithTitle:@"Banned Address!" message:[NSString stringWithFormat:@"You have typed Banned Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alv_sendImg show];
    }
    else
    {

        //[myTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:YES];

        [self countDownForPost];
    }
}

#pragma mark - uitableviewdelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            if (isIphone5)
            {
                return 400;
            }
            else
                return 206;
        }
            
        case 1:
        {
            return 76;
        }
            
        case 2:
        {
            return 44;
            
        }
                        
        default:
            return 44;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SendFacebookImageViewController_cell"];
    }
    
    switch (indexPath.row) {
        case 0:
        {
            cell = cell0;
            break;
        }
            
        case 1:
        {
            cell = cell1;
            break;
        }
            
        case 2:
        {
            cell = cell2;
            break;
        }

            
        default:
            break;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    UITableViewCell *cell = (id)textView.superview.superview;
    NSIndexPath *indexPath = [myTable indexPathForCell:cell];
    
    //NSLog(@"%@-%@",[[cell class] description], indexPath);
    
     [myTable setContentInset:UIEdgeInsetsMake(0, 0, 215, 0)];
     //[myTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
    
    return YES;
}

-(IBAction)posttowall:(id)sender
{
    [self.tvPost resignFirstResponder];
    //[myTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:YES];
    
    MBProgressHUD* progress = [[MBProgressHUD alloc] initWithView:self.view];
    progress.labelText = @"";
    [progress setDelegate:(id)self.view];
    progress.removeFromSuperViewOnHide = YES;
    [self.view addSubview:progress];
    [progress show:YES];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   self.image, @"source",
                                   self.tvPost.text, @"message",
                                   self.tvPost.text, @"caption",
                                   nil];
    
    FBRequest *request = [FBRequest requestWithGraphPath:@"me/photos" parameters:params HTTPMethod:@"POST"];
    
    
    FBRequestConnection *connection = [[FBRequestConnection alloc] init];
    
    [connection addRequest:request completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        NSLog(@"Facebook Post Result: %@", result);
        if (error != nil)
        {
            NSLog(@"Facebook Posting Error: %@", [error localizedDescription]);
        }
            dispatch_async(dispatch_get_main_queue(), ^(void){
                NSLog(@"hehe");
                if (self.delegate && [self.delegate respondsToSelector:@selector(sendFacebookImageFinished:)]) {
                    [self.delegate sendFacebookImageFinished:self];
                }
                
                [progress hide:YES];
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
    }];
    [connection start];
}

- (void)countDownForPost
{
    CountDownViewController *controller = nil;
    if (isIphone5)
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController-568h" bundle:nil];
    else
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController" bundle:nil];
    
    controller.messageText = self.tvPost.text;
    controller.recipientText = TimeLinetext;
    controller.facebookImageDelegate = (id)self;
    //controller.imagePath = [[NSUserDefaults standardUserDefaults] objectForKey:facebookUrl];    
    // 
    if (typeSouceImage) {
        controller.fbImage_postPhoto = self.image;
        controller.imagePath = @"";
    }
    else
    {
        controller.imagePath = [[NSUserDefaults standardUserDefaults] objectForKey:facebookUrl];
    }
    controller.sendType = FBPost;
    
    [self addCountDownView:controller];
}

- (void)addCountDownView:(CountDownViewController*)controller
{
    UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromRight;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
    [UIView setAnimationDuration:1];
    [UIView setAnimationDelegate:controller];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    [self presentViewController:controller animated:YES completion:nil];
    [UIView commitAnimations];
}

- (void)countDownComplete
{
    
    self.arrBuddys = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:ARR_BUDDY_LOGIN] copyItems:YES];
    DLOG(@"ARR_BUDDYS_ID %@",self.arrBuddys);
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if ([self.arrBuddys count] > 0 || [delegate.buddy_id_update length] > 0)
    {
         [self savePostPhotoToServerTemp];
    }
    else
    {
        MBProgressHUD* progress = [[MBProgressHUD alloc] initWithView:self.view];
        progress.labelText = @"";
        [progress setDelegate:(id)self.view];
        progress.removeFromSuperViewOnHide = YES;
        [self.view addSubview:progress];
        [progress show:YES];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       self.image, STT_SOURCE_PHOTO, // source
                                       self.tvPost.text, STT_MESSAGE_POST,
                                       self.tvPost.text, STT_CAPTION_PHOTO,
                                       nil];
        
        FBRequest *request = [FBRequest requestWithGraphPath:@"me/photos" parameters:params HTTPMethod:@"POST"];
        
        
        FBRequestConnection *connection = [[FBRequestConnection alloc] init];
        
        [connection addRequest:request completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            
            NSLog(@"Facebook Post Result: %@", result);
            
            if (error != nil)
            {
                NSLog(@"Facebook Posting Error: %@", [error localizedDescription]);
            }
            dispatch_async(dispatch_get_main_queue(), ^(void){
                NSLog(@"hehe");
                if (self.delegate && [self.delegate respondsToSelector:@selector(sendFacebookImageFinished:)]) {
                    [self.delegate sendFacebookImageFinished:self];
                }
                
                [progress hide:YES];
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
        }];
        [connection start];    

    }
       
}

#pragma mark - Save to server to buddy confirm 

- (void) savePostPhotoToServerTemp
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        NSError *error = nil;
        NSDictionary *dict_response = nil;
        //NSString *date_post = [Utility stringFromDatewithTime:[NSDate date]];
         NSString *date_post = [NSString stringWithFormat:@"%li",[Utility getTimeIntervalSince1970s]];
        VMUsers *user = [VMUsers readData];
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"photo",STT_POST_TYPE, // source
                                       self.tvPost.text, STT_MESSAGE_POST,
                                       self.tvPost.text, STT_CAPTION_PHOTO,
                                       date_post,STT_DATE_POST,
                                       nil];
        
        
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonString = [jsonWriter stringWithObject:params ];
        dict_response = [BesafeAPI postPhotoAlbum:self.image account_type:@"facebook" userID:user.userId_login data:jsonString error:&error];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error)
            {
                [Utility showAlert:[error localizedDescription]];
            }
            else
            {
                DLOG(@"BUDDY USER %@",dict_response);
                [Utility showAlert:MESSAGE_SEND_BUDDY];
            }

        });
    });
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
