
//
//  CommentViewController.h
//  AGKGlobal
//
//  Created by ThuyDao on 6/30/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "FacebookWebViewController.h"
#import "CPTextViewPlaceholder.h"
#import "UIInputToolbar.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "FacebookCommentViewController.h"
#import "Obj.h"
#import "Toast+UIView.h"
#import "UIView+HidingView.h"

@class FacebookWebViewController;
@class AppDelegate;

@interface CommentViewController : UIViewController <UIWebViewDelegate,FacebookUtilsDelegate,UITextViewDelegate,UIInputToolbarDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    BOOL                        isLoadedPost;
    IBOutlet UIWebView          *wvComment;
    IBOutlet UIButton           *btnPost;
    
    NSString                    *activeCommentID;
    NSString                    *activePostID;
    NSString                    *commentText;
    NSMutableArray              *comment_list;
    NSMutableArray              *arrJS;
    NSMutableArray              *listObj;
    NSMutableDictionary         *filterComment;
    NSMutableArray              *listID;
    BOOL                        isSrolltoTop;

    
    NSInteger                   numberCommentList;
    
    UIInputToolbar              *inputToolbar;
    NSInteger                   typeCoutDown;
    
    // check banned word , address
    NSString *textSrc_comment;
    NSString *showBannedWordStr_comment;
    NSString *showBannedAddressStr_comment;
    
    NSArray *bannedWordArr_comment;
    NSArray *bannedAddressArr_comment;
    UIAlertView *alv_comment;
    
@private
    BOOL                        keyboardIsVisible;
    BOOL iskeyboardShow;
}

@property(nonatomic,retain) UIRefreshControl                *refreshControl;
@property (nonatomic, strong) UIInputToolbar                *inputToolbar;
@property (nonatomic,strong) IBOutlet UIWebView             *wvTopHeader;
@property (nonatomic,retain) NSString                       *iconExpand;

@property (nonatomic,retain) PostObj                        *post;
@property (nonatomic,retain) UIWebView                      *wvComment;
@property (nonatomic,retain) FacebookWebViewController      * FacebookWebViewController;

@property (nonatomic,retain) NSString                       *_templateHeaderBegin;

@property (nonatomic,retain) NSString                       *activePostID;
// Dung add icon Status and Checkin
@property (nonatomic,retain) NSString                       *iconStatus;
@property (nonatomic,retain) NSString                       *iconCheckin;
@property (nonatomic,retain) NSString                       *iconPhoto;
//fql
@property (nonatomic,retain) NSMutableDictionary            *_postDict;
//action sheet
@property (nonatomic,retain) UIActionSheet                  *popupQuery;
@property (nonatomic,retain) AppDelegate                    *appDelegate;
//
@property (nonatomic,retain) UIImage                        *attachImage;
// 
@property (nonatomic,assign) BOOL                isLike;
@property (nonatomic,assign) NSInteger           isLikeClick;

- (void)countDownComplete;

@end
