//
//  FacebookCommentViewController.m
//  AGKGlobal
//
//  Created by Jeff Jolley on 5/29/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "FacebookCommentViewController.h"

@interface FacebookCommentViewController ()

@end

@implementation FacebookCommentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
