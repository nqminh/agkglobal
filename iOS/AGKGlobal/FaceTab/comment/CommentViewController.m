//
//  CommentViewController.m
//  AGKGlobal
//
//  Created by ThuyDao on 6/30/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "CommentViewController.h"
#import <QuartzCore/QuartzCore.h>

#define kStatusBarHeight 20
#define kDefaultToolbarHeight 40
#define kKeyboardHeightPortrait 216
#define kKeyboardHeightLandscape 140

@interface CommentViewController ()

- (IBAction)handleRefresh:(id)sender;

@end

@implementation CommentViewController
@synthesize wvComment;
@synthesize refreshControl;
@synthesize inputToolbar;
@synthesize activePostID;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Comment";
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveTestNotification:)
                                                     name:@"refresh data"
                                                   object:nil];
    }
    return self;
}



#pragma mark - Main

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupWebView];
    [self setupHeaderView];
    
    [FacebookUtils addDelegate:self];
    [[ControlObj shared] addDelegate:self];
    
    listObj = [[NSMutableArray alloc] init];
    
    activeCommentID = @"";
    
    btnPost.userInteractionEnabled = NO;
    [btnPost setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    listID = [[NSMutableArray alloc] init];
    
    filterComment = [[NSMutableDictionary alloc] init];
    arrJS = [[NSMutableArray alloc] init];
    
    comment_list = [[NSMutableArray alloc] init];
    
    comment_list = self.post.comment_list;
    
    numberCommentList = 0;
    
    keyboardIsVisible = NO;
    
    /* Calculate screen size */
    CGRect screenFrame = [[UIScreen mainScreen] applicationFrame];
    
    /* Create toolbar */
    self.inputToolbar = [[UIInputToolbar alloc] initWithFrame:CGRectMake(0, screenFrame.size.height-92-kDefaultToolbarHeight, screenFrame.size.width, kDefaultToolbarHeight)];
    [self.view addSubview:self.inputToolbar];
    inputToolbar.inputDelegate = self;
    inputToolbar.textView.placeholder = @"Write a comment...";
    
    // set pull
    //-- check version ios
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    __weak typeof(self) weakSelf = self;
    
    if (sysVer < 6.0) {
        // setup pull-to-refresh
        [self.wvComment.scrollView addPullToRefreshWithActionHandler:^{
            [weakSelf handleRefresh:nil];
        }];
    }
    else {
        //-- add refresh control to tableview
        refreshControl = [[UIRefreshControl alloc] init];
        [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
        refreshControl.tintColor = [UIColor colorWithRed:162/256.0f green:20/256.0f blue:18/256.0f alpha:1];
        
        [self.wvComment.scrollView addSubview:refreshControl];
    }    
    
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    isSrolltoTop = FALSE;
    
    //// set shadow uiwebview header .
    [self.wvTopHeader.layer setShadowColor:[[UIColor grayColor] CGColor]];
    [self.wvTopHeader.layer setShadowOffset:CGSizeMake(3.0, 3.0)];
    [self.wvTopHeader.layer setShadowOpacity:0.8];
    [self.wvTopHeader.layer setShadowRadius:2.0];
    //
    // fix 191
    [self.inputToolbar  becomeFirstResponder];
    // fix 196
    [self hideCommandBar];
}


- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    //-- change title color
    CGRect frame = CGRectMake(0, 150, 100, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    
    isSrolltoTop = FALSE;
    
	/* Listen for keyboard */
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    // add scroll faster optimize speed scroll uiwebview content bug #87.
    wvComment.scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
    // fix #191
     iskeyboardShow = NO;
}


- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [FacebookUtils removeDelegate:self];
    [[ControlObj shared] removeDelegate:self];
    isSrolltoTop = FALSE;
	/* No longer listen for keyboard */
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)setupHeaderView
{
    
    NSString *html = [self sendImageName];
    [self.wvTopHeader loadHTMLString:html baseURL:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}



#pragma mark - Hide Command bar 

-(void) hideCommandBar
{
    self.wvTopHeader.hidden = YES;
    if (isIphone5) {
        self.wvComment.frame = CGRectMake(0, 0, 320, 520);
    }
    else
    {
        self.wvComment.frame = CGRectMake(0, 0, 320, 431);
    }
    
}


-(void) hideRange
{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:nil];
    if(isIphone5)
    {
        [UIView animateWithDuration:1.5
                         animations:^{
                             [Utility setFrameOfView:self.wvComment withHeightView:480 andYCordinate:0];
                             
                         }
                         completion:^(BOOL finished){
                         }];
        
    }
    else{
        [UIView animateWithDuration:1.5
                         animations:^{
                             [Utility setFrameOfView:self.wvComment withHeightView:387 andYCordinate:0];
                             
                         }
                         completion:^(BOOL finished){
                         }];
        
    }
    [ UIView commitAnimations];    
}



#pragma mark - send image name

- (NSString*) sendImageName
{
    NSString *imageSend = @"";
    NSString *temp = [NSString stringWithFormat:@"<div class=\"async_composer\"><div><div class=\"composerLinksArea\" data-sigil=\"marea\"><table class=\"composerLinksTable\"><tbody><tr><td><div class=\"imgDung\"><div><a href=\"http://null/new_status\" class=\"composerLinkText1\" role=\"button\"><img class =\"imgDung\" src=\"%@\" width=\"20\" height=\"20\"/></div></div>Status</a><div class=\"vertical-line\" style=\"height: 16px\"></div></td><td><div><a href=\"http://null/new_photo\" class=\"composerLinkText1\" role=\"button\"><div class=\"imgDung\"><img src=\"%@\" width=\"20\" height=\"20\"/></div><a href=\"http://null/new_photo\" class=\"composerLinkText2\" role=\"button\">Photo</a><div class=\"vertical-line\" style=\"height: 16px\"></div></div></td><td><div><a href=\"http://null/checkin\" class=\"composerLinkText1\" role=\"button\"><div class=\"imgDung\"><img  src=\"%@\" width=\"17\" height=\"17\"/></div>Check In</a></div></td></tr></tbody></table></div></div></div></body></html>", [self getIConStatus],[self getIConPhoto], [self getIConCheckin]];
    NSString* abc = [self templateHeaderBegin];
    imageSend = [NSString stringWithFormat:@"%@%@",abc,temp];
    return imageSend;
    
}



#pragma mark - Delegate TutoViewController .

- (void) showPrivacyTip
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    //NSString* url = @"https://www.facebook.com/settings/?tab=privacy";
    NSString* url = @"https://m.facebook.com/legal/terms";

    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
//    User *user = [[Utility shareProfileDict] objectForKey:[[NSUserDefaults standardUserDefaults] objectForKey:facebookID]];
//    if (user == nil) {
//        user = [[User alloc] initWithDictinary:nil];
//    }
    
    ShowLinkViewController *wv = [[ShowLinkViewController alloc] initWithNibName:@"ShowLinkViewController" bundle:nil];
    wv.strLinkUrl = url;
    //wv.post = user;
    [self.navigationController pushViewController:wv animated:YES];    
}



#pragma mark - Click slide photo 

- (void)photoclickRange
{
    PostObj *postObj = [ControlObj getPostWithKey:self.activePostID];
    NSMutableArray *arr = postObj.media_all_array;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480) {
        ImageViewController *ivc = [[ImageViewController alloc]initWithNibName:@"ImageViewController" bundle:nil];
        [ivc setArrData:arr];
        [ivc setActivePostid:self.activePostID];
        [ivc setIsShowButtonShare:postObj.isShared];
        [ivc setIsShowButtonShareSubPost:postObj.subPost.isShared];
        [ivc setPost:postObj];
        [ivc setDelegate:(id)self];
        DLOG(@"\nshare dc k0 %d\n",postObj.subPost.isShared);
        [self.navigationController presentViewController:ivc animated:YES completion:nil];
    }
    else {
        ImageViewController *ivc = [[ImageViewController alloc]initWithNibName:@"ImageViewController-568" bundle:nil];
        [ivc setArrData:arr];
        [ivc setActivePostid:self.activePostID];
        [ivc setIsShowButtonShare:postObj.isShared];
        [ivc setIsShowButtonShareSubPost:postObj.subPost.isShared];
        [ivc setPost:postObj];
        [ivc setDelegate:(id)self];
        [self.navigationController presentViewController:ivc animated:YES completion:nil];
    }    
}



#pragma mark - Get icon photo

- (NSString*)getIConStatus
{
    if (self.iconStatus == nil) {
        NSString *iconStatus = [[NSBundle mainBundle] pathForResource:@"groupsPost@2x" ofType:@"png"];
        self.iconStatus = [NSURL fileURLWithPath:iconStatus];
    }
    return self.iconStatus;
}


- (NSString*)getIConCheckin
{
    if (self.iconCheckin == nil) {
        NSString *iconCheckin = [[NSBundle mainBundle] pathForResource:@"facecheckin@2x" ofType:@"png"];
        self.iconCheckin = [NSURL fileURLWithPath:iconCheckin];
    }
    
    return self.iconCheckin;
}


- (NSString*)getIConPhoto
{
    if (self.iconPhoto == nil) {
        NSString *iconPhoto = [[NSBundle mainBundle] pathForResource:@"groupsPhoto@2x" ofType:@"png"];
        self.iconPhoto = [NSURL fileURLWithPath:iconPhoto];
    }
    return self.iconPhoto;
}



#pragma mark - UIActionSheetDelegate methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([actionSheet isEqual:self.popupQuery]) {
        switch (buttonIndex) {
            case 0:
                NSLog(@"hide post");
                [FacebookUtils hidenPost:self.activePostID];
                break;
            default: break;
        };
    }
    else {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            switch (buttonIndex) {
                case 0:[self openCamera];break;
                case 1: [self openGallery];break;
                default: break;
            };
        }else {
            switch (buttonIndex) {
                case 0: [self openGallery];break;
                default: break;
            };
        }
        
    }    
}


- (void)openCamera
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
}


- (void)openGallery
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePicker.allowsEditing = YES;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
    
}



#pragma UIImagePickerControllerDelegate methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    //NSLog(@"imagePicker did return");
    self.attachImage = image;
    [self dismissModalViewControllerAnimated:YES];
    
    // NEW WAY: //
    SendFacebookImageViewController *sfivc = [[SendFacebookImageViewController alloc] initWithNibName:@"SendFacebookImageViewController" bundle:nil];
    //    sfivc.imagePost.image = image;
    [sfivc setImage:image];
    [sfivc setDelegate:(id)self];
    [self.navigationController pushViewController:sfivc animated:YES];
}



#pragma mark Notifications

- (void)keyboardWillShow:(NSNotification *)notification
{
    /* Move the toolbar to above the keyboard */
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	CGRect frame = self.inputToolbar.frame;
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        //frame.origin.y = self.view.frame.size.height - frame.size.height - kKeyboardHeightPortrait + 50;// + 50
        // code ngu - no comment.
        if (isIphone5) {
            frame.origin.y = 249;
        }
        else{
            frame.origin.y = 161;
        }
        
        NSLog(@"frame.origin.y show %f",frame.origin.y);
    }
    else {
        frame.origin.y = self.view.frame.size.width - frame.size.height - kKeyboardHeightLandscape - kStatusBarHeight;
    }
	self.inputToolbar.frame = frame;
	[UIView commitAnimations];
    keyboardIsVisible = YES;
}


- (void)keyboardWillHide:(NSNotification *)notification
{
    /* Move the toolbar back to bottom of the screen */
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	CGRect frame = self.inputToolbar.frame;
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        // frame.origin.y = self.view.frame.size.height - frame.size.height ;
        // code ngu - no comment.
        if (isIphone5) {
            frame.origin.y = 415;
        }
        else{
            frame.origin.y = 327;
        }
        
    }
    else {
        frame.origin.y = self.view.frame.size.width - frame.size.height;
    }
    NSLog(@"frame.origin.y hide %f",frame.origin.y);
	self.inputToolbar.frame = frame;
	[UIView commitAnimations];
    keyboardIsVisible = NO;
}



#pragma mark - process html

//get html

- (NSString*)templateHeaderBegin {
    if (self._templateHeaderBegin == nil) {
        NSError *error;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"templateHeaderBegin" ofType:@"txt"];
        self._templateHeaderBegin = [NSString stringWithContentsOfFile:filePath encoding:NSASCIIStringEncoding error:&error];
        if (error) {
            NSLog(@"some sort of error:%@",error);
        }
    }
    return self._templateHeaderBegin;
}


- (void)setupWebView
{
    isLoadedPost = FALSE;
    NSString* contentHtml = @"";
    contentHtml = [[Template templateHTMLBegin] copy];
    contentHtml = [contentHtml stringByAppendingString:@"<div id=\"comment\"></div>"];
    contentHtml = [contentHtml stringByAppendingString:[Template templateHTMLEnd]];
    DLOG(@"contentHtml comment %@",contentHtml);
    [self.wvComment loadHTMLString:contentHtml baseURL:nil];
}


- (void)viewPost
{
    NSString *content= @"";
    //    PostObj *subPost = self.post.subPost;
    //    if (subPost != nil) {
    //        content = [content stringByAppendingString:subPost.strHtml];
    //    }
    //    else {
    if (self.post.strHtml == nil) {
        return;
    }
    content = [content stringByAppendingString:self.post.strHtml];
    
    content  = [content stringByReplacingOccurrencesOfString:@"</article></div>" withString:@"<div id=\"keyAllComment\"></div></article>"];
    
    content = [content stringByReplacingOccurrencesOfString:@"</div></div></div>                    </div>                </div>            </div>        </div>    </div><div id=\"feedback_inline_10200543620946300\" class=\"feedbackInlineWrap inlineShare\">    <div data-sigil=\"uficontainer\">" withString:@"<div id=\"feedback_inline_10200543620946300\" class=\"feedbackInlineWrap inlineShare\">    <div data-sigil=\"uficontainer\">"];
    //comment icon expand
    content  = [content stringByReplacingOccurrencesOfString:@"<div id=\"customPostDiv\">" withString:@"<!--<div id=\"customPostDiv\">"];
    content = [content stringByReplacingOccurrencesOfString:@"<div class=\"caption mfsm fcg\" style=\"margin-right: 13px\">" withString:@" --><div class=\"caption mfsm fcg\" style=\"margin-right: 10px;margin-left:10px\">"];
    content  = [content stringByReplacingOccurrencesOfString:@"</a>           </div>        <div class=\"ib title\">        <a class=\"darkTouch l\" aria-hidden=\"true\"" withString:@"</a>           </div>  -->      <div class=\"ib title\">        <a class=\"darkTouch l\" aria-hidden=\"true\""];
    
    content = [Utility convertTextToHTMLText:content];
    
    NSString *js =[NSString stringWithFormat:@"document.getElementById('comment').innerHTML = '%@%@%@';",
                   [Utility getHeadConfigForPost],
                   [Utility convertTextToHTMLText:content],
                   [Utility getEndConfigForPost]];
    [self.wvComment stringByEvaluatingJavaScriptFromString:js];
    if (isLoadedPost) {
        [self loadComment];
    }
    
    isLoadedPost = TRUE;
}


- (void)loadComment
{
    for (NSInteger ii = numberCommentList ; ii < [comment_list count]; ii ++) {
        
        if ([filterComment objectForKey:[[comment_list objectAtIndex:ii] objectForKey:@"id"]] != nil) {
            continue;
        }
        CommentObj *cmtObj = [[CommentObj alloc] initWithDictinary:[comment_list objectAtIndex:ii]];
        [listObj addObject:cmtObj];
        [filterComment setObject:cmtObj forKey:cmtObj.comment_id];
        NSString* otherComment = @"";
        
        otherComment = [NSString stringWithFormat:@"<div id=\"post_%@\">%@</div>",cmtObj.comment_id,cmtObj.strHTML];
        
        otherComment  = [Utility convertTextToHTMLText:otherComment];
        // run js
        NSString *js =[NSString stringWithFormat:@"document.getElementById('keyAllComment').innerHTML += '%@';",
                       otherComment];
         dispatch_async(dispatch_get_main_queue(), ^{
             [self.wvComment stringByEvaluatingJavaScriptFromString:js];
         });
        
//        [arrJS addObject:js];
    }
//    [self performSelector:@selector(runJS) withObject:nil afterDelay:2];
    [self reloadName];
    [self reloadPlace];
}


- (void)reloadName
{
    NSString *list = [ControlObj getStringListUser];
    NSLog(@"reloadName :\n%@",list);
    
    if ([list isEqualToString:@""]) {
        // fix 196
        [self hideRange];
        return;
        
    }
    
    [FacebookUtils getProfilesList:list];    
}


- (void)reloadPlace
{
    NSString *list = [ControlObj getStringListPlace];
    NSLog(@"%@",list);
    
    if ([list isEqualToString:@""]) {
        return;
    }
    [FacebookUtils getPlace:list];
}


-(void)inputButtonPressed:(NSString *)inputText
{
    /* Called when toolbar button is pressed */
    NSLog(@"Pressed button with text: '%@'", inputText);
    
    NSString* text = inputText;
    
    // get data from banner words , banned addresses.
    
    textSrc_comment = text;
    bannedWordArr_comment = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_WORDS"] mutableCopy];
    bannedAddressArr_comment = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_ADDRESSES"] mutableCopy];
    
    showBannedWordStr_comment = [Utility showBanned:textSrc_comment withArr:bannedWordArr_comment];
    showBannedAddressStr_comment = [Utility showBanned:textSrc_comment withArr:bannedAddressArr_comment];
    
    if(showBannedWordStr_comment.length > 0 )
    {
        if (showBannedAddressStr_comment.length > 0)
        {
            NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_comment);
            alv_comment = [[UIAlertView alloc]initWithTitle:@"Banned Word and Address!" message:[NSString stringWithFormat:@"You have typed Banned Word and Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_comment show];
        }
        else{
            NSLog(@"\n\n banned word %@\n\n",showBannedWordStr_comment);
            alv_comment = [[UIAlertView alloc]initWithTitle:@"Banned Word!" message:[NSString stringWithFormat:@"You have typed Banned Word \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_comment show];
        }
        
    }
    else if (showBannedAddressStr_comment.length> 0)
    {
        NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_comment);
        alv_comment = [[UIAlertView alloc]initWithTitle:@"Banned Address!" message:[NSString stringWithFormat:@"You have typed Banned Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alv_comment show];
    }
    else
    {
        commentText = text;
        [self countDownForPost];        
    }
}


- (NSString*)getIConExpand
{
    if (self.iconExpand == nil) {
        NSString *iconComment = [[NSBundle mainBundle] pathForResource:@"fbc_bookmarks_arrowdownglyph_white" ofType:@"png"];
        self.iconExpand = [NSURL fileURLWithPath:iconComment];
    }
    return self.iconExpand;
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    webView.scrollView.scrollsToTop = YES;
    
    if (APP_DEBUG) NSLog(@"webView shouldStartLoadWithRequest:%@",request);
    NSLog(@"webView shouldStartLoadWithRequest:");
    NSLog(@"...navigationType=%d",navigationType);
    NSLog(@"...request=%@",request);
    
    NSString *urlString = request.URL.absoluteString;
    
    NSLog(@"come on: %@",urlString);
    
    NSRange likeRange = [urlString rangeOfString:@"null/like1"];
    NSRange unlikeRange = [urlString rangeOfString:@"http://null/unlike1/"];
    NSRange commentRange = [urlString rangeOfString:@"null/comment"];
    NSRange newStatusRange = [urlString rangeOfString:@"null/new_status"];
    NSRange newPhotoRange = [urlString rangeOfString:@"null/new_photo"];
    NSRange checkInRange = [urlString rangeOfString:@"null/checkin"];
    NSRange shareLinkRange = [urlString rangeOfString:@"http://null/shareLink/"];
    NSRange photoclickRange = [urlString rangeOfString:@"http://null/clickPhoto/"];
    NSRange shareRange= [urlString rangeOfString:@"http://null/share/"];
    NSRange likePostRange = [urlString rangeOfString:@"null/like/"];
    NSRange unlikePostRange = [urlString rangeOfString:@"http://null/unlike/"];
    
    if (shareRange.location != NSNotFound) {
        self.activePostID = [urlString substringFromIndex:18];
        NSLog(@"shareRange: \n\t urlstring:%@ \n\t id: %@",urlString,self.activePostID);
        NSLog(@"doing share post");
//        NSMutableDictionary *dic = [[self postDict] objectForKey:self.activePostID];
        //        [FacebookUtils sharePost:[dic objectForKey:@"permalink"]];
        ShareViewController *svc;
        PostObj *postObj = [ControlObj getPostWithKey:self.activePostID];
        [postObj resetName];
        
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height == 480) {
            svc = [[ShareViewController alloc]initWithNibName:@"ShareViewController" bundle:nil];
        }
        else
        {
            svc = [[ShareViewController alloc]initWithNibName:@"ShareViewController-568" bundle:nil];
        }
//        [svc setPost:dic];
        [svc setPostObj:postObj];
        [svc setIsView:@"CommentView"];        
        [self.navigationController pushViewController:svc animated:YES];
        return NO;
    }
    
    if (photoclickRange.location != NSNotFound) {
        NSLog(@"clickPhoto");
//        NSString* postID = [urlString substringFromIndex:23];
//        NSDictionary *dict = [[self postDict] objectForKey:postID];
//        NSMutableArray *arr = [dict objectForKey:@"media_all_array"];
        
        NSString* postID = [urlString substringFromIndex:23];
        self.activePostID = postID;
        [self photoclickRange];
        
        /*
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height == 480) {
            ImageViewController *ivc = [[ImageViewController alloc]initWithNibName:@"ImageViewController" bundle:nil];
//            [ivc setArrData:arr];
            
            [self.navigationController presentViewController:ivc animated:YES completion:nil];
        }
        else {
            ImageViewController *ivc = [[ImageViewController alloc]initWithNibName:@"ImageViewController-568" bundle:nil];
//            [ivc setArrData:arr];
            
            [self.navigationController presentViewController:ivc animated:YES completion:nil];
        }
        */
        
        return NO;
    }
    
    if (shareLinkRange.location != NSNotFound) {
        //http://null/shareLink/
        NSString* urlString1 = [urlString substringFromIndex:22];
        
        NSURL *newurl = [NSURL URLWithString:urlString1];
        
        [[UIApplication sharedApplication] openURL:newurl];
        
        return NO;
    }
    
    if (checkInRange.location != NSNotFound) {
        SendFacebookCheckinViewController *sfmvc = [[SendFacebookCheckinViewController alloc]
                                                    initWithNibName:@"SendFacebookCheckinViewController" bundle:nil];
        [sfmvc setDelegate:(id)self];
        [self.navigationController pushViewController:sfmvc animated:YES];
        
        return NO;
    }
    
    if (newStatusRange.location != NSNotFound) {
        SendFacebookMessageViewController *sfmvc = [[SendFacebookMessageViewController alloc]
                                                    initWithNibName:@"SendFacebookMessageViewController" bundle:nil];
        [sfmvc setDelegate:(id)self];
        [self.navigationController pushViewController:sfmvc animated:YES];
        
        return NO;
    }
    
    if (newPhotoRange.location != NSNotFound) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:nil
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:nil];
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [actionSheet addButtonWithTitle:@"Take Photo"];
        }
        
        [actionSheet addButtonWithTitle:@"Choose Existing"];
        
        //ADD CANCEL BUTTON TO ACTION SHEET
        [actionSheet addButtonWithTitle:@"Cancel"];
        [actionSheet setCancelButtonIndex:[actionSheet numberOfButtons]-1];
        
        [actionSheet showInView:self.wvComment];
        
        return NO;
    }
    
    NSString *postID = @"";
    activePostID = @"";
    activeCommentID = @"";
    if (likePostRange.location != NSNotFound) {
        @try {
            /*
            postID = [urlString substringFromIndex:17];
            self.activePostID = postID;
            [FacebookUtils like:self.activePostID HTTPMethod:@"POST"];
            
            return NO;
             */
            postID = [urlString substringFromIndex:17];
            self.activePostID = postID;
            if ([[NSUserDefaults standardUserDefaults] boolForKey:warningControllFacebook] || [[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"] == 0 ) {
                self.isLike = TRUE;
                self.activePostID = postID;
                [FacebookUtils like:self.activePostID HTTPMethod:@"POST"];
                return NO;
            }
            TutorViewController *tuto = nil;
            // check only click Like
            if (self.isLikeClick == 0)
            {
                self.isLikeClick = 1;
                if (isIphone5)
                    tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController-568h" bundle:nil];
                else
                    tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
                
                tuto.delegate = (id)self;
                tuto.type = typeLike;
                [self presentPopupViewController:tuto animationType:MJPopupViewAnimationSlideBottomTop bckClickable:NO];
                return  NO;
            }

        }
        @catch (NSException *exception) {
            // like status process
        }
        @finally {
        }
    }
    if (commentRange.location != NSNotFound) {
        @try {
            NSLog(@"Comment in comment!");            
            //[self.inputToolbar becomeFirstResponder];
            // fix #191
            if (iskeyboardShow) {
                [self.inputToolbar resignFirstResponder];
                iskeyboardShow = NO;
            }
            else{
                [self.inputToolbar becomeFirstResponder];
                iskeyboardShow = YES;
            }

            return NO;
        }
        @catch (NSException *exception) {
            // DO NOTHING //
            postID = nil;
        }
        @finally {
            // DO NOTHING //
        }
    }
    
    if (unlikePostRange.location != NSNotFound) {
        @try {
            self.isLike = NO;
            postID = [urlString substringFromIndex:19];
            NSLog(@"unlike :%@",postID);
            self.activePostID = postID;
            [FacebookUtils like:self.activePostID HTTPMethod:@"DELETE"];
            
            return NO;
            
        }
        @catch (NSException *exception) {
            // like status process
        }
        @finally {
        }
    }
    // like post comment . 
    if (likeRange.location != NSNotFound) {
        @try {
            /*
            postID = [urlString substringFromIndex:18];
            self.activePostID = postID;
            [FacebookUtils like:self.activePostID HTTPMethod:@"POST"];
            
            return NO;
             */
            postID = [urlString substringFromIndex:18];
            self.activePostID = postID;
            activeCommentID = postID;
            if ([[NSUserDefaults standardUserDefaults] boolForKey:warningControllFacebook] || [[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"] == 0 ) {
                self.isLike = TRUE;
                self.activePostID = postID;
                [FacebookUtils like:self.activePostID HTTPMethod:@"POST"];
                return NO;
            }
            
            TutorViewController *tuto = nil;
            // check only click Like
            if (self.isLikeClick == 0)
            {
                self.isLikeClick = 1;
                if (isIphone5)
                    tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController-568h" bundle:nil];
                else
                    tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
                
                tuto.delegate = (id)self;
                tuto.type = typeLike;
                [self presentPopupViewController:tuto animationType:MJPopupViewAnimationSlideBottomTop bckClickable:NO];
                return  NO;
            }

        }
        @catch (NSException *exception) {
            // like status process
        }
        @finally {
        }
    }
    if (commentRange.location != NSNotFound) {
        @try {
            NSLog(@"Comment in comment!");
            return NO;
        }
        @catch (NSException *exception) {
            // DO NOTHING //
            postID = nil;
        }
        @finally {
            // DO NOTHING //
        }
    }
    
    if (unlikeRange.location != NSNotFound) {
        @try {
            self.isLikeClick = 0;
            postID = [urlString substringFromIndex:20];
            activeCommentID = postID;
            NSLog(@"unlike :%@",postID);
            self.isLike = FALSE;
            self.activePostID = postID;
            [FacebookUtils like:self.activePostID HTTPMethod:@"DELETE"];
            
            return NO;
        }
        @catch (NSException *exception) {
        }
        @finally {
        }
    }
    
    NSLog(@"urlString:%@",urlString);
    NSLog(@"postID:%@",postID);
    if (postID == nil) {
        NSLog(@"GOING TO COMMENT PAGE...");
        FacebookCommentViewController *fcvc = [[FacebookCommentViewController alloc] initWithNibName:@"FacebookCommentViewController" bundle:nil];
        [self.navigationController pushViewController:fcvc animated:YES];
        return NO;
    }
    
    return YES;
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    //
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //"Dung" set scroll to top webViews.
    if (!isSrolltoTop) {
        CGPoint topOffset = CGPointMake(0, 0);
        [webView.scrollView setContentOffset:topOffset animated:NO];
    }
    // fix content view hien thi k0 dung vi tri,bi troi xuong duoi.
    if (isIphone5) {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 470)];
    }
    else{
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, 390)];
    }
    // [self.wvComment.scrollView setContentSize:CGSizeMake(320, 800)];
    //fix do not scroll uiwebview header.
    self.wvTopHeader.scrollView.scrollEnabled = FALSE;
    // add scroll faster optimize speed scroll uiwebview content bug #87.
    wvComment.scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
    
    
    if ([webView isEqual:wvComment]) {
        [FacebookUtils getCommentForPost:self.post.postID];
        //        [self performSelector:@selector(runJS) withObject:nil afterDelay:1];
        // setup infinite scrolling
        __weak typeof(self) weakSelf = self;
        [self.wvComment.scrollView addInfiniteScrollingWithActionHandler:^{
            [weakSelf insertRowAtBottom];
        }];
        // end set pull
//        [self viewPost];
        [self performSelector:@selector(viewPost) withObject:nil afterDelay:2];
    }
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //
}



#pragma mark - facebookUtils delegate

- (void)facebook:(FacebookUtils *)fbU didCommentFinish:(NSDictionary *)result
{
    NSLog(@"result: %@",result);
    
    [self.view makeToast:@"Comment Success!"
                duration:3.0
                position:@"top"
                   title:nil];
    [FacebookUtils getCommentForPost:self.post.postID];
}


- (void)facebook:(FacebookUtils *)fbU didCommentFail:(NSError *)error
{
    NSLog(@"facebook didCommentFail:%@",[error description]);
}


- (void)facebook:(FacebookUtils *)fbU didGetCommentForPostFinish:(NSDictionary *)result
{
    [comment_list removeAllObjects];
    [comment_list addObjectsFromArray:[result objectForKey:@"data"]];
    numberCommentList = 0;
    if (isLoadedPost) {
        [self loadComment];
    }
    isLoadedPost = TRUE;
}


- (void)facebook:(FacebookUtils *)fbU didGetCommentForPostFail:(NSError *)error
{
    NSLog(@"facebook didGetCommentForPostFail:%@",[error description]);
}


- (void)facebook:(FacebookUtils *)fbU didGetCommentForIDFinish:(NSDictionary *)result
{
    [comment_list removeAllObjects];
    [comment_list addObjectsFromArray:[result objectForKey:@"data"]];
}


- (void)facebook:(FacebookUtils *)fbU didGetCommentForIDFail:(NSError *)error
{
    NSLog(@"facebook didGetCommentForIDFail:%@",[error description]);
}


- (void)facebook:(FacebookUtils *)fbU didGetPostFinish:(NSDictionary *)result
{
    NSLog(@"result:%@",result);

    NSMutableArray *arr = [result objectForKey:@"data"];
    
    for (NSInteger ii = 0; ii < [arr count]  ; ii ++) {
        NSDictionary* postDict;
        NSInteger pt = [[postDict objectForKey:@"postType"] integerValue];
        
        if (pt == typePost_event ||
            pt == Likes_a_Link1  ||
            pt == 376) {
            continue;
        }
        if (pt == typePost_Likes_a_photo_or_Link ||
            pt == typePost_Comment_created ) {
            
            int idxsub = ii + 1;
            if (idxsub < [arr count]) {
                ii++;
            }
        }
        else {
        }
    }
    
    [refreshControl endRefreshing];
}


- (void)facebook:(FacebookUtils *)fbU didGetPostFail:(NSError *)error
{
    NSLog(@"facebook didGetPostFail:%@",[error description]);
}


- (void)facebook:(FacebookUtils *)fbU didGetUserInfoFinish:(NSDictionary *)result
{
    NSArray *arr = [result objectForKey:@"data"];
   [ControlObj processFriendList:arr];
}


- (void)facebook:(FacebookUtils *)fbU didGetUserInfoFail:(NSError *)error
{
    NSLog(@"didGetUserInfoFail error: %@",[error description]);
}


- (void)facebook:(FacebookUtils *)fbU didGetCommentSeeMoreFinish:(NSDictionary *)result
{
    [comment_list removeAllObjects];
    [comment_list addObjectsFromArray:[result objectForKey:@"data"]];
    [self loadComment];
    
    [self.wvComment.scrollView.infiniteScrollingView stopAnimating];
    [self.wvComment.scrollView.pullToRefreshView stopAnimating];
}


- (void)facebook:(FacebookUtils *)fbU didGetCommentSeeMoreFail:(NSError *)error
{
    NSLog(@"didGetCommentSeeMoreFail error: %@",[error description]);
    [self.wvComment.scrollView.infiniteScrollingView stopAnimating];
    [self.wvComment.scrollView.pullToRefreshView stopAnimating];
}


// like

- (void)facebook:(FacebookUtils *)fbU didLikeFinish:(NSDictionary *)result
{
    NSLog(@"result: %@",result);
    if ([activeCommentID isEqualToString:@""]) {
        if (self.isLike) {
            [self.post liked];
        }
        else {
            [self.post unliked];
        }
        [self viewPost];
        [comment_list removeAllObjects];
        [filterComment removeAllObjects];
        numberCommentList = 0;
        [FacebookUtils getCommentForPost:self.post.postID];
    }
    else {
        // like subpost comment.
        CommentObj *comObj = [filterComment objectForKey:self.activePostID];
        if (self.isLike) {
            [comObj liked];
        }
        else
        {
            [comObj unliked];
        }
        NSString *js = [NSString stringWithFormat:@"document.getElementById('post_%@').innerHTML = '%@';",activePostID,comObj.strHTML];
        [self.wvComment stringByEvaluatingJavaScriptFromString:js];
    }
}


- (void)facebook:(FacebookUtils *)fbU didLikeFail:(NSError *)error
{
    NSLog(@"didLikeFail error: %@",[error description]);
}



#pragma mark uiscrollview

- (IBAction)handleRefresh:(id)sender
{
    [FacebookUtils getPost:self.post.postID];
}


//-- insert row at bottom when UITableView infinite scrolling
- (void)insertRowAtBottom
{
    numberCommentList = [comment_list count];
    [FacebookUtils getComment:self.post.postID seeMore:numberCommentList + 10];
    isSrolltoTop = TRUE;
}



#pragma mark -  Delegate Besafetip Tuto

- (void)cancelLike
{
    self.isLikeClick = 0;
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}


// click Like 
- (void)acceptLike:(warningType)type
{
    NSLog(@"\n ACTIVE POST ID %@ \n",self.activePostID);
    switch (type) {
        self.isLikeClick = 0;
        case typeLike: {
            self.isLike = TRUE;            
            [FacebookUtils like:self.activePostID HTTPMethod:@"POST"];
            break;
        }
        case typeUnlike: {
            self.isLike = FALSE;
            [FacebookUtils like:self.activePostID HTTPMethod:@"DELETE"];
            break;
        }
        default:
            break;
    }
    NSLog(@"acceptLike");
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    //fix blank bottom webview
    [Utility setFrameOfView:self.view withHeightView:416.0 andYCordinate:0.0];
}



#pragma mark - notification

- (void) receiveTestNotification:(NSNotification *) notification
{
    NSLog (@"Successfully received the test notification!");
    
    [FacebookUtils getCommentForPost:self.post.postID];
}


#pragma mark - JS

- (void)runJS
{
    dispatch_async(dispatch_get_main_queue(), ^{
        while (arrJS.count > 0) {
            DLOG(@"arrJS Count: \n ------\t%d\t--------",arrJS.count);
            NSString* js = [arrJS objectAtIndex:0];
            [self.wvComment stringByEvaluatingJavaScriptFromString:js];
            [arrJS removeObjectAtIndex:0];
        }
    });
}


- (void)showAllFriend
{
    for (NSString *newID in [[Utility shareProfileDict] allKeys]) {
        User *user = [[Utility shareProfileDict] objectForKey:newID];
        [self.wvComment stringByEvaluatingJavaScriptFromString:user.jspic];
        [self.wvComment stringByEvaluatingJavaScriptFromString:user.jsname];
    }
}


- (void)refreshMissAvatar
{
    for (CommentObj *cmtObj in listObj) {
        if ([cmtObj isFailName]) {
            NSString *js =[NSString stringWithFormat:@"document.getElementById('post_%@').innerHTML = '%@';",
                           cmtObj.comment_id,
                           cmtObj.strHTML];
            
            [wvComment stringByEvaluatingJavaScriptFromString:js];
        }
    }
}


- (void)ControlObj:(ControlObj *)controlObj jsforFacebookUser:(NSString *)js
{
    DLOG(@"");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.wvComment stringByEvaluatingJavaScriptFromString:js];
        [arrJS addObject:js];
    });
}


- (void)countDownForPost
{
    CountDownViewController *controller = nil;
    if (isIphone5)
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController-568h" bundle:nil];
    else
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController" bundle:nil];
    
    controller.messageText = commentText;
    controller.recipientText = self.post.name;
    controller.sendMessageDelegate = (id)self;
    controller.imagePath = self.post.pic;
    controller.sendType = FBcomment;
    
    [self addCountDownView:controller];
}


- (void)addCountDownView:(CountDownViewController*)controller
{
    UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromRight;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
    [UIView setAnimationDuration:1];
    [UIView setAnimationDelegate:controller];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    [self presentViewController:controller animated:YES completion:nil];
    [UIView commitAnimations];
}


- (void)countDownComplete
{
    //-- fix #253 BeSafe post comment frames
    //-- lost delegate CommentViewController
    [FacebookUtils addDelegate: self];
    [FacebookUtils comment:self.post.postID withText:[Utility decodeHTMLEntities:commentText]];
}


@end
