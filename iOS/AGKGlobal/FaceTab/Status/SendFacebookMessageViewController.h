//
//  SendFacebookMessageViewController.h
//  AGKGlobal
//
//  Created by Jeff Jolley on 5/28/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <QuartzCore/QuartzCore.h>
#import "FacebookWebViewController.h"
#import "SSTextView.h"
#import "Utility.h"
#import "AudienceViewController.h"
#import "FacebookUtils.h" 
#import "AppDelegate.h"

@class SendFacebookMessageViewController;
@protocol sendFacebookMessageDelegate <NSObject>

@optional

- (void)sendFacebookMessageFinished:(SendFacebookMessageViewController*)sendFacebookMessage result:(NSDictionary*)result;
- (void)callActionSheetPhoto;

@end

@interface SendFacebookMessageViewController : AudienceViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    
    NSString *textSrc;   
    NSString *showBannedWordStr;
    NSString *showBannedAddressStr;
    
    NSArray *bannedWordArr;
    NSArray *bannedAddressArr;
    UIAlertView *alv;
    
}
@property (nonatomic,retain) AppDelegate        *appDelegate;
@property (nonatomic,retain) IBOutlet UILabel *postingHeaderLabel;
@property (nonatomic,retain) IBOutlet UIButton *postingSubmitButton;
@property (nonatomic, assign)           id<sendFacebookMessageDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *arrBuddys;

- (IBAction)performPostingSubmit:(id)sender;
- (void)countDownComplete;

@end

