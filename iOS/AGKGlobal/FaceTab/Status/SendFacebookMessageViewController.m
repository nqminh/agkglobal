//
//  SendFacebookMessageViewController.m
//  AGKGlobal
//
//  Created by Jeff Jolley on 5/28/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "SendFacebookMessageViewController.h" 
#import "BesafeAPI.h"
#import "VMUsers.h" 
#import "NSDate+TimeAgo.h"

@interface SendFacebookMessageViewController ()

@end

@implementation SendFacebookMessageViewController

@synthesize postingHeaderLabel,postingTextView,postingSubmitButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];      
       
    //--navigation title
    self.title = @"Status";
    
    self.postingTextView.layer.cornerRadius = 5;
    self.postingTextView.clipsToBounds = YES;
    [self.postingTextView becomeFirstResponder];
    [self.postingTextView setPlaceholder:@"What's on your mind?"];
    [self.postingTextView.layer setBorderColor:[[[UIColor darkGrayColor] colorWithAlphaComponent:0.5] CGColor]];
    [self.postingTextView.layer setBorderWidth:1.5];
    
    // custom navigation bar button
    // left
    UIBarButtonItem *leftbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel:)];
    self.navigationItem.leftBarButtonItem = leftbarBtnItem;
    
    // right
    
    UIBarButtonItem *rightbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Post" style:UIBarButtonItemStyleBordered target:self action:@selector(postText:)];
    self.navigationItem.rightBarButtonItem = rightbarBtnItem;      
    
}

- (void)viewWillAppear:(BOOL)animated
{
    //-- change title color
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    
    [self configureView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - configure view 
- (void)configureView
{
    float y = 0;
    CGSize result = [[UIScreen mainScreen] bounds].size;
    
    if (result.height == 480) {
        y = 164; //305
    }
    else {
        y = 250;//self.view.frame.size.height - 205 ; // -44 250
    }
    [self.viewAudience setFrame:CGRectMake(0, y, 320, 36)];
    [self.view addSubview:self.viewAudience];
    [self.myTableView setFrame:CGRectMake(0,y, 320, self.view.frame.size.height - self.viewAudience.frame.origin.y)];
    [self.view addSubview:self.myTableView];
    self.myTableView.hidden = YES;
}

#pragma mark - navigationbar button

- (void)cancel:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)postText:(UIButton*)sender
{
    if ([self.postingTextView.text isEqualToString:@"What's on your mind?"] && [self.postingTextView.textColor isEqual:[UIColor grayColor]]) {
        return;
    }
    
    if (self.postingTextView.text.length == 0) {
        return;
    }
    // get data from banner words , banned addresses.
    textSrc = self.postingTextView.text;
    bannedWordArr = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_WORDS"] mutableCopy];
    bannedAddressArr = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_ADDRESSES"] mutableCopy];
    
    showBannedWordStr = [Utility showBanned:textSrc withArr:bannedWordArr];
    showBannedAddressStr = [Utility showBanned:textSrc withArr:bannedAddressArr];
    
       if(showBannedWordStr.length > 0 )
        {
            if (showBannedAddressStr.length > 0)
            {
                NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr);
                alv = [[UIAlertView alloc]initWithTitle:@"Banned Word and Address!" message:[NSString stringWithFormat:@"You have typed Banned Word and Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alv show];
            }
            else{
                NSLog(@"\n\n banned word %@\n\n",showBannedWordStr);
                alv = [[UIAlertView alloc]initWithTitle:@"Banned Word!" message:[NSString stringWithFormat:@"You have typed Banned Word \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alv show];
            }
           
        }
        else if (showBannedAddressStr.length > 0)
        {
            NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr);
            alv = [[UIAlertView alloc]initWithTitle:@"Banned Address!" message:[NSString stringWithFormat:@"You have typed Banned Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv show];
        }        
        else
        {                     
            [self countDownForPost];
        }        
    
}


- (IBAction)performPostingSubmit:(id)sender
{
    if ([self.postingTextView.text isEqualToString:@"What's on your mind?"] && [self.postingTextView.textColor isEqual:[UIColor grayColor]]) {
        return;
    }
    
    if (self.postingTextView.text.length == 0) {
        return;
    }
    
    FBRequest *req = [FBRequest requestForPostStatusUpdate:self.postingTextView.text];
    [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (result) {
            if (APP_DEBUG) {
                NSLog(@"--- requestForPostStatusUpdate --- SUCCESS ---");
                NSLog(@"%@",result);
                NSLog(@"----------------------------------------------");
            }
            if (self.delegate && [self.delegate respondsToSelector:@selector(sendFacebookMessageFinished:result:)]) {
                [self.delegate sendFacebookMessageFinished:self result:result];
            }
        } else {
            NSLog(@"--- requestForPostStatusUpdate --- FAILURE ---");
            NSLog(@"%@",error);
            NSLog(@"----------------------------------------------");
        }
    }];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)countDownForPost
{
    CountDownViewController *controller = nil;
    if (isIphone5)
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController-568h" bundle:nil];
    else
        controller = [[CountDownViewController alloc] initWithNibName:@"CountDownViewController" bundle:nil];
    
    controller.messageText = self.postingTextView.text;
    controller.recipientText = TimeLinetext;
    controller.sendMessageDelegate = (id)self;
    controller.imagePath = [[NSUserDefaults standardUserDefaults] objectForKey:facebookUrl];
    controller.sendType = FBPost;
    
    [self addCountDownView:controller];
}

- (void)addCountDownView:(CountDownViewController*)controller
{
    UIViewAnimationTransition trans = UIViewAnimationTransitionFlipFromRight;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationTransition:trans forView:self.view.window cache:YES];
    [UIView setAnimationDuration:1];
    [UIView setAnimationDelegate:controller];
    [UIView setAnimationDidStopSelector:@selector(animationFinished:finished:context:)];
    [self presentViewController:controller animated:YES completion:nil];
    [UIView commitAnimations];
}


- (void)countDownComplete
{
    
    self.arrBuddys = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:ARR_BUDDY_LOGIN]];
    NSString *date_post = [NSString stringWithFormat:@"%li",[Utility getTimeIntervalSince1970s]];
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSLog(@"buddy_id_update %@",delegate.buddy_id_update);  
        
    DLOG(@"COUNT BUDDYS %d",[self.arrBuddys count]);
    if ([self.arrBuddys count] > 0 || [delegate.buddy_id_update length] > 0)
    {
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setObject:self.postingTextView.text forKey:STT_MESSAGE_POST];
        
        NSString* audienceStr = [FacebookUtils getAudienceValue:self.audienceActiveDict];
        DLOG(@"privacy:\n \t\t\t %@",audienceStr);
        [param setObject:audienceStr forKey:STT_PRIVACY_POST];
        [param setObject:@"status"   forKey:STT_POST_TYPE];
        [param setObject:date_post   forKey:STT_DATE_POST];
        
        [NetworkActivity show];
        SBJsonWriter *jsonWriter = [[SBJsonWriter alloc] init];
        NSString *jsonString = [jsonWriter stringWithObject:param ];
        
        NSLog(@"jsonString param %@",jsonString);
        [self postDataPendingBuddy:jsonString];
        [self.navigationController popToRootViewControllerAnimated:YES];

    }
    else
    {
        FBRequest *req = [FBRequest requestForPostStatusUpdate:self.postingTextView.text];
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        
        [param setObject:@"{'value':'EVERYONE'}" forKey:@"privacy"];
        
        [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (result) {
                if (APP_DEBUG) {
                    NSLog(@"--- requestForPostStatusUpdate --- SUCCESS ---");
                    NSLog(@"%@",result);
                    NSLog(@"----------------------------------------------");
                }
                if (self.delegate && [self.delegate respondsToSelector:@selector(sendFacebookMessageFinished:result:)]) {
                    [self.delegate sendFacebookMessageFinished:self result:result];
                }
            } else {
                NSLog(@"--- requestForPostStatusUpdate --- FAILURE ---");
                NSLog(@"%@",error);
                NSLog(@"----------------------------------------------");
            }
        }];
        FBRequest *request = [FBRequest requestWithGraphPath:@"me/feed" parameters:param HTTPMethod:@"POST"];
        
        
        FBRequestConnection *connection = [[FBRequestConnection alloc] init];
        
        [connection addRequest:request completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            
            [NetworkActivity hide];
            
            
        }];
        [connection start];
    }     
        
}


- (void) showAlert: (NSString*) msg
{
	[[[UIAlertView alloc] initWithTitle: nil
                                message: msg
                               delegate: nil
                      cancelButtonTitle: @"OK"
                      otherButtonTitles: nil] show];
}


- (void) postDataPendingBuddy:(NSString*)strJson
{
    NSError *error = nil;
    NSDictionary *dict_response = nil;
    VMUsers *user = [VMUsers readData];
    DLOG(@"USER ID %@",user.userId_login);
    
    dict_response = [BesafeAPI postDataToPendingBuddy:@"facebook" userID:user.userId_login post_id:@"0" data:strJson error:&error];
    if (error)
    {
        [self showAlert:[error localizedDescription]];
    }
    else
    {
        DLOG(@"RESPONSE %@",dict_response);
        [Utility showAlert:MESSAGE_SEND_BUDDY];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self hideKeyBroad];
    return YES;
}


#pragma mark - UIActionsheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        switch (buttonIndex) {
            case 0:[self openCamera];break;
            case 1: [self openGallery];break; 
            default:
                
                break;
        };
    }else {
        switch (buttonIndex) {
            case 0: [self openGallery];break;
            default: break;
        };
    }
}


- (void) callActionSheetPhoto
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [actionSheet addButtonWithTitle:@"Take Photo"];
    }
    
    [actionSheet addButtonWithTitle:@"Choose Existing"];
    
    //ADD CANCEL BUTTON TO ACTION SHEET
    [actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet setCancelButtonIndex:[actionSheet numberOfButtons]-1];
    
    [actionSheet showInView:self.appDelegate.window];

}


- (IBAction)clickCamera:(id)sender
{
//    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//    {
//        [self openCamera];
//    }
//    else
//    {
//        [self openGallery];
//    }
    [self.navigationController popViewControllerAnimated:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(callActionSheetPhoto)])
    {
        [self.delegate callActionSheetPhoto];
    }
}

- (void)openCamera
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
}


- (void)openGallery
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
    
}


#pragma UIImagePickerControllerDelegate methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
       [self dismissModalViewControllerAnimated:YES];    
   
    SendFacebookImageViewController *sfivc = [[SendFacebookImageViewController alloc] initWithNibName:@"SendFacebookImageViewController" bundle:nil];
    //    sfivc.imagePost.image = image;
    sfivc.textStt = self.postingTextView.text;
    
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        sfivc.typeSouceImage = 1;
    }
    //otherwise, show a modal for taking a photo
    else
    {
        sfivc.typeSouceImage = 0;
    }
    
    [sfivc setImage:image];
    if ([self.postingTextView.text length] > 0)
    {
        //[sfivc setTextStt:self.postingTextView.text];
    }    
    [sfivc setDelegate:(id)self];
    [self.navigationController pushViewController:sfivc animated:YES];
}


@end
