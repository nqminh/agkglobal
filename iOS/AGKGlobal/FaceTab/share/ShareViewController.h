//
//  ShareViewController.h
//  AGKGlobal
//
//  Created by Thuy Dao on 8/28/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <QuartzCore/QuartzCore.h>
#import "SSTextView.h"
#import "FacebookUtils.h"
#import "TutorViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "Obj.h"
#import "CountDownViewController.h"
//#import "ImageViewController.h"

@class ShareViewController;
@protocol sharePostDelegate1 <NSObject>
    - (void)dismissViewsharePost;    
    - (void) addGestureRecognizertoImage:(BOOL)animated;
    
@end


@interface ShareViewController : UIViewController < UITableViewDataSource,
                                                    UITableViewDelegate,
                                                    UITextViewDelegate,
                                                    FacebookUtilsDelegate>
{
    NSMutableDictionary* dataSourceDict;
    NSMutableDictionary* audienceActiveDict;
    NSString           * textInput;
    NSString            * link;
    
    // check banned word , address
    NSString *textSrc_share;
    NSString *showBannedWordStr_share;
    NSString *showBannedAddressStr_share;
    
    NSArray *bannedWordArr_share;
    NSArray *bannedAddressArr_share;
    UIAlertView *alv_share;    
    NSInteger typeCoutDown;
}
//fix crash khi click xem besafe tip va click button back tro lai man hinh share . 
@property (nonatomic,retain) PostObj                       *postObj;
@property (nonatomic,retain) IBOutlet SSTextView            *postingTextView;
@property (nonatomic,retain) IBOutlet UIImageView           *imgAvatar;
@property (nonatomic,retain) IBOutlet UILabel               *nameUser;
@property (nonatomic,retain) IBOutlet UIView                *viewShare;
@property (nonatomic,retain) IBOutlet UIView                *viewAudience;
@property (nonatomic,retain) IBOutlet UITableView           *myTableView;
@property (nonatomic,retain) IBOutlet UIButton              *btnAudience;
@property (nonatomic,retain) IBOutlet UILabel               *lbReason;
@property (weak, nonatomic) IBOutlet UILabel *lblMobileUpload;



@property (strong, nonatomic) IBOutlet UINavigationBar *shareNav;
// phan biet kieu hien thi
@property (nonatomic,retain) NSString                       *isView;

@property (nonatomic,assign) id <sharePostDelegate1>      delegate;
//@property (assign) id<sharePostDelegate> delegate;

- (IBAction)clickExpand:(id)sender;

- (IBAction)clickCancel:(id)sender;
- (IBAction)clickShare:(id)sender;

@end
