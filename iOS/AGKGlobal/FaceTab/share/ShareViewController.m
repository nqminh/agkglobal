//
//  ShareViewController.m
//  AGKGlobal
//
//  Created by Thuy Dao on 8/28/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "ShareViewController.h"

typedef void (^load_avatar_done_block)(UIImage* image,NSError* error);

@interface ShareViewController ()

@end

@implementation ShareViewController
@synthesize shareNav,isView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [FacebookUtils addDelegate:self];
        
        self.title = @"Share";
    }
    return self;
}



#pragma mark - Main

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [FacebookUtils getALLFriendsList];
    
    textInput = @"";
    
    NSDictionary* Public = [[NSDictionary alloc] initWithObjectsAndKeys:@"Public",@"name",
                            @"AudienceCellPublic@2x.png",@"icon",
                            @"0",@"flid",
                            nil];
    
    audienceActiveDict = [[NSMutableDictionary alloc] initWithDictionary:Public];
    [self reloadAudience];
    
    NSDictionary* Friends = [[NSDictionary alloc] initWithObjectsAndKeys:@"Friends",@"name",
                             @"AudienceCellFriends@2x.png",@"icon",
                             @"1",@"flid",
                             nil];
    
    NSDictionary* Only_Me = [[NSDictionary alloc] initWithObjectsAndKeys:@"Only Me",@"name",
                             @"AudienceCellOnlyMe@2x.png",@"icon",
                             @"2",@"flid",
                             nil];
    
    NSArray *arr = [[NSArray alloc] initWithObjects:Public,Friends,Only_Me, nil];
    dataSourceDict = [[NSMutableDictionary alloc] init];
    [dataSourceDict setObject:arr forKey:@"audience"];
    
    [self.postingTextView setPlaceholder:@"Write something..."];

    // custom navigation bar button
    // left
    UIBarButtonItem *leftbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel:)];
    self.navigationItem.leftBarButtonItem = leftbarBtnItem;
    
    // right
    
    UIBarButtonItem *rightbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Post" style:UIBarButtonItemStyleBordered target:self action:@selector(postText:)];
    self.navigationItem.rightBarButtonItem = rightbarBtnItem;
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //-- change title color
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    
    [self.postingTextView becomeFirstResponder];
    [self configureView];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)setAvatar:(NSString*)url doneBlock:(load_avatar_done_block)block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        UIImage* myImage = [UIImage imageWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString: url]]];
        
        if (block) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                block(myImage,nil);
            });
        }
    });
}


- (void)configureView
{
    NSString *name = self.postObj.name;
    NSString *picURL = self.postObj.pic;
    link = @"";
    /*
    NSDictionary *dictMedia = [self.postObj.media_all_array objectAtIndex:0];    
    if ([[dictMedia objectForKey:@"href"] length] > 0)
    {
        link = [dictMedia objectForKey:@"href"];
    }
    else
    {
        link = self.postObj.permalink;
    }
    */
    link = self.postObj.permalink;
    if ([name isEqualToString:@"Facebook User"]) {
        NSString * userid = self.postObj.userID;
        User *profile = [ControlObj getUserWithID:userid];
        name = profile.name;
        picURL = profile.pic;
    }    
    
    float y = 0;
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if([isView isEqualToString:@"FacebookWeb"])
    {
        if (result.height == 480) {
            y = 164; //305
            //
            [self.viewAudience setFrame:CGRectMake(0, y, 320, 35)];
            [self.view addSubview:self.viewAudience];
            //
            [self.viewShare setFrame:CGRectMake(0, self.viewAudience.frame.origin.y - 80 , 320, 80)];//-80
            [self.view addSubview:self.viewShare];
            
        }
        else {
            y = self.view.frame.size.height -205 ; // -44 250
            //y = 164;
            [self.viewAudience setFrame:CGRectMake(0, y, 320, 36)];
            [self.view addSubview:self.viewAudience];
            //
            [self.viewShare setFrame:CGRectMake(0, self.viewAudience.frame.origin.y - 80 , 320, 80)];//-80  160
            [self.view addSubview:self.viewShare];
            
        }
        //
        [self.postingTextView setFrame:CGRectMake(0, 0, 320,self.viewShare.frame.origin.y  )];
        [self.view addSubview:self.postingTextView];
        
    }
    else if([isView isEqualToString:@"ShowLink"] || [isView isEqualToString:@"CommentView"])
    {
        if (result.height == 480) {
            y = 164; //305
            //
            [self.viewAudience setFrame:CGRectMake(0, y, 320, 35)];
            [self.view addSubview:self.viewAudience];
            //
            [self.viewShare setFrame:CGRectMake(0, self.viewAudience.frame.origin.y - 80 , 320, 80)];//-80
            [self.view addSubview:self.viewShare];
            
        }
        else {
            y = self.view.frame.size.height -205 ; // -44 250
            //y = 164;
            [self.viewAudience setFrame:CGRectMake(0, y, 320, 36)];
            [self.view addSubview:self.viewAudience];
            
            //
            [self.viewShare setFrame:CGRectMake(0, self.viewAudience.frame.origin.y - 80 , 320, 80)];//-80  160
            [self.view addSubview:self.viewShare];
            
        }
        
        //
        [self.postingTextView setFrame:CGRectMake(0, 0, 320,self.viewShare.frame.origin.y  )];
        [self.view addSubview:self.postingTextView];
        
        
    }
    else{ // share in slide photo.
        
        if (result.height == 480) {
            y = self.view.frame.size.height - 35 - 305 + 44; // - 35 - 305
            [self.viewAudience setFrame:CGRectMake(0, y, 320, 35)];
            [self.view addSubview:self.viewAudience];
            //
            [self.viewShare setFrame:CGRectMake(0, self.viewAudience.frame.origin.y - 80 , 320, 80)];//-80
            [self.view addSubview:self.viewShare];
            //
            [self.postingTextView setFrame:CGRectMake(0, 88, 320,self.viewShare.frame.origin.y - 35 - 53 )];
            [self.view addSubview:self.postingTextView];
            //
            [self.shareNav setFrame:CGRectMake(0, 44, 320 ,44)];
            [self.view addSubview:self.shareNav];
            
        }
        else {
            y = self.view.frame.size.height - 35 - 215  ; // -44
            [self.viewAudience setFrame:CGRectMake(0, y, 320, 35)];
            [self.view addSubview:self.viewAudience];
            //
            [self.viewShare setFrame:CGRectMake(0, self.viewAudience.frame.origin.y - 80 , 320, 80)];//-80
            [self.view addSubview:self.viewShare];
            //
            [self.postingTextView setFrame:CGRectMake(0, 44, 320,self.viewShare.frame.origin.y - 44 )];
            [self.view addSubview:self.postingTextView];
            //
            [self.shareNav setFrame:CGRectMake(0, 0, 320 ,44)];
            [self.view addSubview:self.shareNav];            
        }
    }
    
    [self.myTableView setFrame:CGRectMake(0,self.viewAudience.frame.origin.y, 320, self.view.frame.size.height - self.viewAudience.frame.origin.y)];
    [self.view addSubview:self.myTableView];
    
    __weak ShareViewController *svc = self;
    switch ([self.postObj.postType integerValue])
    {
        case 46:
        {
            self.lbReason.text = [self getReason];
            self.lblMobileUpload.text = self.postObj.name;
            self.nameUser.text = self.postObj.message;
            if ([self.postObj.postPhotoURL length] > 0)
            {
                picURL = self.postObj.postPhotoURL;
            }
            else
            {
                picURL = self.postObj.pic;
            }        

        }
            break;
        case 80:
        {
            self.lbReason.text = self.postObj.attach_name;
            self.lblMobileUpload.text = self.postObj.attachCaption;
            self.nameUser.text = self.postObj.attach_descriptionn;
            picURL = self.postObj.postPhotoURL;
            link  = self.postObj.linkURL;
        }
            break;
        case 247:
        {
            //--type post photo
            self.lbReason.text = self.postObj.message;
            self.lblMobileUpload.text = @"Timeline Photo";
            self.nameUser.text = self.postObj.name;
            picURL = self.postObj.pic;

        }
            break;
        case 128:
        {
            //--type share video
            self.lbReason.text        = self.postObj.message;
            self.lblMobileUpload.text = self.postObj.attach_name;
            self.nameUser.text        = self.postObj.name;
            picURL                    = self.postObj.postPhotoURL;
        }
            break;
        default:
        {
            [self.nameUser setText:name];
            [self.lbReason setText:[self getReason]];
            self.lblMobileUpload.text = @"";
            picURL = self.postObj.pic;

        }
            break;
    }
        
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [self setAvatar:picURL doneBlock:^(UIImage *image, NSError *error) {
        [svc.imgAvatar setImage:image];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }];
    
    
        
}


- (void)reloadAudience
{
    if (audienceActiveDict == nil) {
        return;
    }
    NSString *imageName = [audienceActiveDict objectForKey:@"icon"];
    [self.btnAudience setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
    [self.postingTextView becomeFirstResponder];
}


- (NSString*)getReason
{
    NSString *reason = @"";
    NSInteger postType = [self.postObj.postType integerValue];
    switch (postType) {
        case 8:
            reason = @"Share Post";
            break;
        case 56:
            reason = @"Share Post";
            break;
        case 65:
            reason = @"Share Photo";
            break;
        case 80:
        {
            reason = @"Share Post";
            break;
        }
        case 161:
            reason = @"Share Post";
            break;
        case 245:
        {
            reason = @"Share Post";
            break;
        }
        case 247:
            reason = @"Share Photo";
            break;
        case 257:
        {
            reason = @"Share Post";
            
            break;
        }
        case 272:
            reason = @"Share Post";
            break;
        case 285:
            reason = @"Share Chekin";
            break;
        case typePost_Post_in_Group:
            reason = @"Share Photo";
            break;
        case 373:
            reason = @"Share photo";
            break;
        case 46:
            reason = @"Status Update";
            break;
        default:
            reason = @"Share Post";
            break;
    }
    
    return reason;    
}



#pragma mark - navigationbar button

- (void)cancel:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) post2Text
{
    // comment tam thoi. 
//    if ([self.postingTextView.text isEqualToString:@"Write something..."] && [self.postingTextView.textColor isEqual:[UIColor grayColor]]) {
//        return;
//    }
//    
//    if (self.postingTextView.text.length == 0) {
//        return;
//    }
    
    //NSString *text = self.postingTextView.text;
    // get data from banner words , banned addresses.
    
    textSrc_share = self.postingTextView.text;
    bannedWordArr_share = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_WORDS"] mutableCopy];
    bannedAddressArr_share = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_ADDRESSES"] mutableCopy];
    
    showBannedWordStr_share = [Utility showBanned:textSrc_share withArr:bannedWordArr_share];
    showBannedAddressStr_share = [Utility showBanned:textSrc_share withArr:bannedAddressArr_share];
    
    if(showBannedWordStr_share.length > 0 )
    {
        if (showBannedAddressStr_share.length > 0)
        {
            NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_share);
            alv_share = [[UIAlertView alloc]initWithTitle:@"Banned Word and Address!" message:[NSString stringWithFormat:@"You have typed Banned Word and Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_share show];
        }
        else{
            NSLog(@"\n\n banned word %@\n\n",showBannedWordStr_share);
            alv_share = [[UIAlertView alloc]initWithTitle:@"Banned Word!" message:[NSString stringWithFormat:@"You have typed Banned Word \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_share show];
        }
        
    }
    else if (showBannedAddressStr_share.length > 0)
    {
        NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_share);
        alv_share = [[UIAlertView alloc]initWithTitle:@"Banned Address!" message:[NSString stringWithFormat:@"You have typed Banned Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alv_share show];
    }
    else
    {
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:warningControllFacebook] integerValue] == 1 || [[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"] == 0) {
            link = self.postObj.permalink;
            textInput = self.postingTextView.text;
            [FacebookUtils sharePost:link text:textInput audience:audienceActiveDict];
            if (self.delegate && [self.delegate respondsToSelector:@selector(dismissViewsharePost)]) {
                [self.delegate dismissViewsharePost];
            }
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else {
            
            textInput = self.postingTextView.text;
            [self.postingTextView resignFirstResponder];
            
            TutorViewController *tuto = nil;
            if (isIphone5)
                tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController-568h" bundle:nil];
            else
                tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
            
            tuto.delegate = (id)self;
            tuto.type = typeShare;
            [self presentPopupViewController:tuto animationType:MJPopupViewAnimationSlideBottomTop bckClickable:YES];
        }
    }
    
}


- (void)postText:(UIButton*)sender
{
    [self post2Text];
}



#pragma mark - ibaction

- (IBAction)clickExpand:(id)sender
{
    DLOG(@"");
    
    self.myTableView.hidden = NO;
    self.viewAudience.hidden = YES;
    [self.postingTextView resignFirstResponder];
    [self.myTableView reloadData];
}


- (IBAction)clickCancel:(id)sender {
    DLOG(@"");
    // add gesture after remove when click call view share
    if (_delegate && [_delegate respondsToSelector:@selector(addGestureRecognizertoImage:)]) {
        [_delegate addGestureRecognizertoImage:YES];
    }
    [self.postingTextView resignFirstResponder];
    if (self.delegate && [self.delegate respondsToSelector:@selector(dismissViewsharePost)]) {
        [self.delegate dismissViewsharePost];        
    }
}


- (IBAction)clickShare:(id)sender {
    DLOG(@"");
    // add gesture after remove when click call view share
    if (_delegate && [_delegate respondsToSelector:@selector(addGestureRecognizertoImage:)]) {
        [_delegate addGestureRecognizertoImage:YES];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(dismissViewsharePost)]) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:warningControllFacebook] integerValue] == 1) {
            [self.delegate dismissViewsharePost];
        }
        else {
            [self post2Text];
        }
        
    }
}



#pragma mark - getViewFromNib

- (UIView*)getHeaderFromTable
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"CustomShareView" owner:nil options:nil];
    return [(id) views objectAtIndex:0];
}


- (UITableViewCell*)getCellFromTable
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"CustomShareView" owner:nil options:nil];
    return [(id) views objectAtIndex:1];
}



#pragma mark - tableviewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    [self.myTableView setBackgroundColor:CELL_COLOR];
    //    return [[dataSourceDict allKeys] count];
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0: {
            NSArray *arr = [dataSourceDict objectForKey:@"audience"];
            return arr.count;
            break;
        }
        case 1: {
            NSArray *arr = [dataSourceDict objectForKey:@"FriendsList"];
            return arr.count;
            break;
        }
        default:
            return 0;
    }
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (cell == nil) {
        cell = [self getCellFromTable];
    }
    
    switch (indexPath.section) {
        case 0: {
            NSArray *arr = [dataSourceDict objectForKey:@"audience"];
            NSDictionary* dic = [arr objectAtIndex:indexPath.row];
            [((UIImageView*)[cell viewWithTag:1]) setImage:[UIImage imageNamed:[dic objectForKey:@"icon"]]];
            ((UILabel*)[cell viewWithTag:2]).text = [dic objectForKey:@"name"];
            [cell setBackgroundColor:CELL_COLOR];
            if ([[dic objectForKey:@"name"] isEqualToString:[audienceActiveDict objectForKey:@"name"]]) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            break;
        }
        case 1: {
            NSArray *arr = [dataSourceDict objectForKey:@"FriendsList"];
            NSDictionary* dic = [arr objectAtIndex:indexPath.row];
            [((UIImageView*)[cell viewWithTag:1]) setImage:[UIImage imageNamed:[dic objectForKey:@"icon"]]];
            ((UILabel*)[cell viewWithTag:2]).text = [dic objectForKey:@"name"];
            [cell setBackgroundColor:CELL_COLOR];
            
            if ([[dic objectForKey:@"flid"] isEqualToString:[audienceActiveDict objectForKey:@"flid"]]) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            break;
        }
            
        default:
            break;
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = nil;
    if (view == nil) {
        view = [self getHeaderFromTable];
    }
    switch (section) {
        case 0:
            ((UILabel*)[view viewWithTag:1]).text = @"Audience";
            break;
            
        case 1:
            ((UILabel*)[view viewWithTag:1]).text = @"Friends List";
            break;
            
        default:
            break;
    }
    [view setBackgroundColor:HEADER_COLOR];
    
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0: {
            NSMutableArray *arr = [dataSourceDict objectForKey:@"audience"];
            audienceActiveDict = [arr objectAtIndex:indexPath.row];
            [self reloadAudience];
            break;
        }
            
        case 1:  {
            NSMutableArray *arr = [dataSourceDict objectForKey:@"FriendsList"];
            audienceActiveDict = [arr objectAtIndex:indexPath.row];
            [self reloadAudience];
            break;
        }
            
        default:
            break;
    }
}



#pragma mark - Delegate TutoViewController

- (void) showPrivacyTip
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    NSString* url = @"https://m.facebook.com/legal/terms";
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
//    User *user = [[Utility shareProfileDict] objectForKey:[[NSUserDefaults standardUserDefaults] objectForKey:facebookID]];
//    if (user == nil) {
//        user = [[User alloc] initWithDictinary:nil];
//    }
    
    ShowLinkViewController *wv = [[ShowLinkViewController alloc] initWithNibName:@"ShowLinkViewController" bundle:nil];
    wv.strLinkUrl = url;
    //wv.post = user;
    [self.navigationController pushViewController:wv animated:YES];
    
}



#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    self.myTableView.hidden = YES;
    self.viewAudience.hidden = NO;
    return YES;
}



#pragma mark - facebookUtils delegate

- (void)facebook:(FacebookUtils *)fbU didGetAllFriendListFinish:(NSDictionary *)result
{
    DLOG(@"%@",result);
    NSMutableArray* friendlist = [[NSMutableArray alloc] init];
    NSArray *arr = [result objectForKey:@"data"];
    for (NSMutableDictionary *dic in arr) {
        [dic setObject:@"AudienceCellFriendList@2x.png" forKey:@"icon"];
        [friendlist addObject:dic];
    }
    [dataSourceDict setObject:friendlist forKey:@"FriendsList"];
    [self.myTableView reloadData];
}


- (void)facebook:(FacebookUtils *)fbU didGetAllFriendListFail:(NSError *)error
{
    DLOG(@"%@",[error description]);
}


- (void)acceptLike:(warningType)type
{
    // NSString *linkUrl = link;
    if ([link length] == 0) {
        link = @" ";
    }
    switch (type) {
        case typeShare: {
            [FacebookUtils sharePost:link text:textInput audience:audienceActiveDict];
            if (self.delegate && [self.delegate respondsToSelector:@selector(dismissViewsharePost)]) {
                [self.delegate dismissViewsharePost];
            }
            break;
        }
        default:
            break;
    }
    NSLog(@"acceptLike");
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    [self.delegate dismissViewsharePost];
    //return to imageviewcontroller
    if (self.delegate && [self.delegate respondsToSelector:@selector(dismissViewsharePost)]) {
        [self.delegate dismissViewsharePost];
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)cancelLike
{
    NSLog(@"cancelLike");
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    [self.postingTextView becomeFirstResponder];
}



@end