

//
//  FacebookWebViewController.m
//  AGKGlobal
//
//  Created by Jeff Jolley on 5/28/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "FacebookWebViewController.h"
#import "SendFacebookMessageViewController.h"
#import "SendFacebookImageViewController.h"
#import "FacebookCommentViewController.h"
#import "AppDelegate.h"
#import <Accounts/Accounts.h>
#import "Constants.h"
#import "UIView+HidingView.h"
#import "Utility.h"
#import "AppDelegate.h" 
#import "BesafeAPI.h" 
#import "VMUsers.h" 
#import "PostUploaderObj.h" 


#define TABBAR_HEIGHT (49)


@interface FacebookWebViewController ()

@end

@implementation FacebookWebViewController
@synthesize refreshControl;
@synthesize facebooWebView;
@synthesize lastContentOffset;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        [[ControlObj shared] addDelegate:self];
        self.title = NSLocalizedString(@"Post", @"Post");
        self.navigationItem.title = @"BeSafe Post";
        self.tabBarItem.image = [UIImage imageNamed:@"icon_post_selected.png"];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveTestNotification:)
                                                     name:@"refresh data"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                              selector:@selector(autoRefreshNewfeed)
                                              name:AUTO_REFRESH_NEWFEED_DATA
                                              object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiverPostBuddyConfirm:) name:RECEIVE_NOTIFICATION object:nil];
    }
    
    return self;
}



#pragma mark - Main

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.newsArray = [[NSMutableArray alloc] init];
    
    arrJS = [[NSMutableArray alloc] init];
    
    strJsForPost = @"";
    
    self.facebooWebView.scrollView.delegate = (id)self;
    
    [self Login];
    typeCoutDown = 0;
    
    
    self.isLike = FALSE;
    
    // set pull
    //-- check version ios
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    __weak FacebookWebViewController *myself = self;
    
    if (sysVer < 6.0) {
        // setup pull-to-refresh
        [self.facebooWebView.scrollView addPullToRefreshWithActionHandler:^{
            [myself handleRefresh:nil];
        }];
    }
    else {
        //-- add refresh control to tableview
        refreshControl = [[UIRefreshControl alloc] init];
        [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
        refreshControl.tintColor = [UIColor colorWithRed:162/256.0f green:20/256.0f blue:18/256.0f alpha:1];
        
        [self.facebooWebView.scrollView addSubview:refreshControl];
    }
    
    // setup infinite scrolling
    [self.facebooWebView.scrollView addInfiniteScrollingWithActionHandler:^{
        [myself insertRowAtBottom];
    }];
    // end set pull
    
    // set shadow uiwebview header .
    [self.wvTopHeader.layer setShadowColor:[[UIColor grayColor] CGColor]];
    [self.wvTopHeader.layer setShadowOffset:CGSizeMake(3.0, 3.0)];
    [self.wvTopHeader.layer setShadowOpacity:0.8];
    [self.wvTopHeader.layer setShadowRadius:2.0];
    
    // optimize speed scroll uiwebview content bug #87.
    facebooWebView.scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void) viewWillAppear:(BOOL)animated
{
    //-- change title color
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    
    //fix blank bottom webview
    if (isIphone5) {
        [Utility setFrameOfView:self.facebooWebView withHeightView:496.0 andYCordinate:0.0];
    }
    else{
        [Utility setFrameOfView:self.facebooWebView withHeightView:426.0 andYCordinate:0.0];
    }
    NSLog(@"\n\ntoa do y %f - height %f \n\n",self.view.frame.origin.y ,self.view.frame.size.height);
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //[self showTabBar:self.tabBarController];
    [self performSelector:@selector(showTabar) withObject:nil afterDelay:2];
    //fix blank bottom webview
    [Utility setFrameOfView:self.facebooWebView withHeightView:416.0 andYCordinate:0.0];
    [[NSNotificationCenter defaultCenter] removeObserver:AUTO_REFRESH_NEWFEED_DATA];     
}

- (void)showTabar
{
     [self.tabBarController setTabBarHidden:NO animated:NO];
}


- (void)setupHeaderView
{
    NSString *html = [self sendImageName];
    [self.wvTopHeader loadHTMLString:html baseURL:nil];
}


- (void)setupWebFacebook
{
    dispatch_async(dispatch_get_main_queue(),^{
        NSString *html = [Template templateHTMLBegin];
        html = [html stringByAppendingString:[Template templateHTMLEnd]];
        DLOG(@"STR HTML %@",html);
        [self.facebooWebView loadHTMLString:html baseURL:nil];
    });
}


- (void)Login
{
    [FacebookUtils initWithDelegate:(id)self];
    [FacebookUtils logInFacebook];
}

#pragma mark - Upload data Buddy confirm

- (void) receiverPostBuddyConfirm:(NSNotification*)ntf
{
    NSDictionary *info = ntf.userInfo;
    NSDictionary *jsondict = [info objectForKey:@"aps"];
    NSString *jsonStr = [jsondict valueForKey:@"json"];
    
    NSError* error = nil;
    NSData *data = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:&error]; 
  
    NSString *post_id = [jsonResponse valueForKey:@"id"];
    
    
    NSDictionary *dict = nil;
    
    VMUsers *user = [VMUsers readData];
    dict = [BesafeAPI getDetailPostByID:post_id userID:user.userId_login error:&error];
    
    if (error)
    {
        [Utility showAlert:[error localizedDescription]];
    }
    else
    {
        DLOG(@"dict nhan ve %@",dict);
        PostUploaderObj *post = [[PostUploaderObj alloc] initWithDictinary:[dict objectForKey:@"data"]];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:[jsondict objectForKey:@"alert"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [self postBuddyConfirmToFacebook:post];
    }
}

- (void) postBuddyConfirmToFacebook:(PostUploaderObj*)post
{
    if ([post.pul_checkin length] > 0) {
        
        FBRequest *req = [FBRequest requestForPostStatusUpdate:post.pul_message place:post.pul_checkin tags:nil];
        [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (result) {
                NSLog(@"ok: result: %@",result);
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [FacebookUtils getNewFeedsMultiQuery];
                });            
            } else {
                NSLog(@"--- requestForPostStatusUpdate --- FAILURE ---");
                NSLog(@"%@",error);
                NSLog(@"----------------------------------------------");
            }
        }];

    }
    else
    {
        FBRequest *req = [FBRequest requestForPostStatusUpdate:post.pul_message];
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        NSString *valueFormat = [NSString stringWithFormat:@"{'value':'%@'}",post.pul_privacy];
        
        [param setObject:valueFormat forKey:@"privacy"];
        
        [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (result) {
                if (APP_DEBUG) {
                    NSLog(@"--- requestForPostStatusUpdate --- SUCCESS ---");
                    NSLog(@"%@",result);
                    NSLog(@"----------------------------------------------");
                }
                DLOG(@"%@",result);
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [FacebookUtils getNewFeedsMultiQuery];
                });
            } else {
                NSLog(@"--- requestForPostStatusUpdate --- FAILURE ---");
                NSLog(@"%@",error);
                NSLog(@"----------------------------------------------");
            }
        }];
        
        FBRequest *request = [FBRequest requestWithGraphPath:@"me/feed" parameters:param HTTPMethod:@"POST"];
        
        
        FBRequestConnection *connection = [[FBRequestConnection alloc] init];
        
        [connection addRequest:request completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            
            [NetworkActivity hide];
            
        }];
        [connection start];
 
    }
}

#pragma mark - IBAction

- (IBAction)clickNewStories:(id)sender
{
    [self hidePopView];
//    
//    if (newStoriesJS == nil) {
//        return;
//    }
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        NSString *js = [NSString stringWithFormat:@"document.getElementById('newstories').innerHTML = '%@'",newStoriesJS];
//        [self.facebooWebView stringByEvaluatingJavaScriptFromString:js];
//    });
//    
//    int64_t delayInSeconds = 1;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        self.facebooWebView.scrollView.scrollsToTop = YES;
//    });
    loop = FALSE;
    [ControlObj resetCount];
    [FacebookUtils getNewFeedsMultiQuery];
}



#pragma mark - funtion

- (void)undoHideRange
{
    NSLog(@"doing undo_hide post");
    //remove and re-show
    
    NSMutableArray *arr = [Utility getCachedFile];    
    
    [arr removeObject:self.activePostID];
    [Utility saveCachedFile:arr];
    
    PostObj* postObj = [ControlObj getPostWithKey:self.activePostID];
    NSString* html = [postObj resetHTML];
//    NSLog(@"%@",html);
//    NSString *text = @"'";
//    NSString *replaceText = @"\\'";
//    
//    html  = [html stringByReplacingOccurrencesOfString:text withString:replaceText];
    html = [Utility convertTextToHTMLText:html];
    
    NSString* replace1 = [NSString stringWithFormat:@"post_%@",self.activePostID];
    NSString* replace2 = @"</article></div></div></section></div></div>";
    
    html  = [html stringByReplacingOccurrencesOfString:replace1 withString:@""];
    html  = [html stringByReplacingOccurrencesOfString:replace2 withString:@"</article></div></section></div></div>"];
    
    NSString *js =[NSString stringWithFormat:@"document.getElementById('post_%@').innerHTML = '%@';",
                   self.activePostID,
                   html];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *result = [self.facebooWebView stringByEvaluatingJavaScriptFromString:js];
        if (result == nil || [result isEqualToString:@""]) {
            DLOG(@"js fails 1");
        }
        User *user = [ControlObj getUserWithID:postObj.userID];
        [self.facebooWebView stringByEvaluatingJavaScriptFromString:user.jsname];
        [self.facebooWebView stringByEvaluatingJavaScriptFromString:user.jspic];
        if (postObj.subPost != nil) {
            User *user1 = [ControlObj getUserWithID:postObj.subPost.userID];
            [self.facebooWebView stringByEvaluatingJavaScriptFromString:user1.jsname];
            [self.facebooWebView stringByEvaluatingJavaScriptFromString:user1.jspic];
        }
    });
}


- (void)shareRange
{
    NSLog(@"doing share post");    
    
    PostObj *postObj = [ControlObj getPostWithKey:self.activePostID]; 
    [postObj resetName];
    ShareViewController *svc;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480) {
        svc = [[ShareViewController alloc]initWithNibName:@"ShareViewController" bundle:nil];
    }
    else
    {
        svc = [[ShareViewController alloc]initWithNibName:@"ShareViewController-568" bundle:nil];
    }
    [svc setPostObj:postObj];
    [svc setIsView:@"FacebookWeb"];
    [self.navigationController pushViewController:svc animated:YES];
}


- (void)expandRange
{
    NSString* myID = [[NSUserDefaults standardUserDefaults] objectForKey:facebookID];
    PostObj *postObj = [ControlObj getPostWithKey:self.activePostID];
    NSString *app_id = postObj.app_id;
    if (app_id == nil) {
        app_id = @"xxx";
    }
    // my_app_id = 539623596100993
    if ([app_id isEqualToString:@"539623596100993"] && [myID isEqualToString:postObj.userID]) {
        self.popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Delete",@"Hide", nil];
        self.popupQuery.destructiveButtonIndex = 0;
    }
    else
    {
        self.popupQuery = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Hide", nil];
    }
    
    
    [self.popupQuery setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [self.popupQuery showInView:self.facebooWebView];
}


- (void)photoclickRange
{
    PostObj *postObj = [ControlObj getPostWithKey:self.activePostID];
    NSMutableArray *arr = postObj.media_all_array;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480) {
        ImageViewController *ivc = [[ImageViewController alloc]initWithNibName:@"ImageViewController" bundle:nil];
        [ivc setArrData:arr];
        [ivc setActivePostid:self.activePostID];
        [ivc setIsShowButtonShare:postObj.isShared];
        [ivc setIsShowButtonShareSubPost:postObj.subPost.isShared];
        [ivc setPost:postObj];
        [ivc setDelegate:(id)self];
         DLOG(@"\nshare dc k0 %d\n",postObj.subPost.isShared);
        [self.navigationController presentViewController:ivc animated:YES completion:nil];
    }
    else {
        ImageViewController *ivc = [[ImageViewController alloc]initWithNibName:@"ImageViewController-568" bundle:nil];
        [ivc setArrData:arr];
        [ivc setActivePostid:self.activePostID];
        [ivc setIsShowButtonShare:postObj.isShared];
        [ivc setIsShowButtonShareSubPost:postObj.subPost.isShared];
        [ivc setPost:postObj];
        [ivc setDelegate:(id)self];
        [self.navigationController presentViewController:ivc animated:YES completion:nil];
    }
   
}



#pragma mark - UIWebViewDelegate Methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSLog(@"webView shouldStartLoadWithRequest:");
    NSLog(@"...navigationType=%d",navigationType);
    NSLog(@"...request=%@",request);
    
    NSString *urlString = request.URL.absoluteString;
    
    NSLog(@"come on: %@",urlString);
    
    NSRange likeRange = [urlString rangeOfString:@"null/like"];
    NSRange unlikeRange = [urlString rangeOfString:@"http://null/unlike/"];
    NSRange commentRange = [urlString rangeOfString:@"null/comment"];
    NSRange newStatusRange = [urlString rangeOfString:@"null/new_status"];
    NSRange newPhotoRange = [urlString rangeOfString:@"null/new_photo"];
    NSRange checkInRange = [urlString rangeOfString:@"null/checkin"];
    NSRange shareLinkRange = [urlString rangeOfString:@"http://null/shareLink/"];
    NSRange photoclickRange = [urlString rangeOfString:@"http://null/clickPhoto/"];
    NSRange expandRange = [urlString rangeOfString:@"http://null/expand/"];
    NSRange shareRange= [urlString rangeOfString:@"http://null/share/"];
    NSRange undoHideRange = [urlString rangeOfString:@"http://null/Undohide/"];
    NSRange check = [urlString rangeOfString:@"http"];    
    NSRange continueReading = [urlString rangeOfString:@"http://null/readmore/"];
    
    if (undoHideRange.location != NSNotFound) {
        self.activePostID = [urlString substringFromIndex:21];
        PostObj *obj = [ControlObj getPostWithKey:self.activePostID];
        NSLog(@"undoHideRange: \n\t urlstring:%@ \n\t id: %@ \n\t post-type :%@",urlString,self.activePostID,[obj.postType stringValue]);
        
        [self undoHideRange];
        return NO;
    }
    
    if (shareRange.location != NSNotFound) {
        self.activePostID = [urlString substringFromIndex:18];
        NSLog(@"shareRange: \n\t urlstring:%@ \n\t id: %@",urlString,self.activePostID);
        
        [self shareRange];
        return NO;
    }
    
    if (expandRange.location != NSNotFound) {
        self.activePostID = [urlString substringFromIndex:19];
        PostObj *obj = [ControlObj getPostWithKey:self.activePostID];
        NSLog(@"expandRange: \n\t urlstring:%@ \n\t id: %@ \n\t post-type :%@",urlString,self.activePostID,[obj.postType stringValue]);
        
        [self expandRange];
        return NO;
    }
    
    if (photoclickRange.location != NSNotFound) {
        NSLog(@"clickPhoto");
        NSString* postID = [urlString substringFromIndex:23];
        self.activePostID = postID;
                
        [self photoclickRange];
        return NO;
    }
    
    if (shareLinkRange.location != NSNotFound) {
        //http://null/shareLink/
        NSString* urlString1 = [urlString substringFromIndex:22];
        
        User *user = [ControlObj getUserWithID:[[NSUserDefaults standardUserDefaults] objectForKey:facebookID]];
        if (user == nil) {
            user = [[User alloc] initWithDictinary:nil];
            user.name = [[NSUserDefaults standardUserDefaults] objectForKey:facebookName];
            user.pic = [[NSUserDefaults standardUserDefaults] objectForKey:facebookUrl];
            user.userID = [[NSUserDefaults standardUserDefaults] objectForKey:facebookID];
        }
        //--fix #278: BeSafe post share
        PostObj *post  = [ControlObj getPostHrefWithKey:urlString1];
        ShowLinkViewController *wv = [[ShowLinkViewController alloc] initWithNibName:@"ShowLinkViewController" bundle:nil];
        wv.strLinkUrl = urlString1;
        wv.post    = user;
        wv.postObj = post;
        
        [self.navigationController pushViewController:wv animated:YES];
        
        return NO;
    }
    
    if (checkInRange.location != NSNotFound) {
        SendFacebookCheckinViewController *sfmvc = [[SendFacebookCheckinViewController alloc]initWithNibName:@"SendFacebookCheckinViewController" bundle:nil];
        [sfmvc setDelegate:(id)self];
        [self.navigationController pushViewController:sfmvc animated:YES];
        
        return NO;
    }
    
    if (newStatusRange.location != NSNotFound) {
        
        if (isIphone5) {
            SendFacebookMessageViewController *sfmvc = [[SendFacebookMessageViewController alloc]
                                                        initWithNibName:@"SendFacebookMessageViewController-568h" bundle:nil];
            [sfmvc setDelegate:(id)self];
            [self.navigationController pushViewController:sfmvc animated:YES];

        }
        else{
            SendFacebookMessageViewController *sfmvc = [[SendFacebookMessageViewController alloc]
                                                        initWithNibName:@"SendFacebookMessageViewController" bundle:nil];
            [sfmvc setDelegate:(id)self];
            [self.navigationController pushViewController:sfmvc animated:YES];

        }              
        
        return NO;
    }
    
    if (newPhotoRange.location != NSNotFound) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:self
                                                        cancelButtonTitle:nil
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:nil];
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [actionSheet addButtonWithTitle:@"Take Photo"];
        }
        
        [actionSheet addButtonWithTitle:@"Choose Existing"];
        
        //ADD CANCEL BUTTON TO ACTION SHEET
        [actionSheet addButtonWithTitle:@"Cancel"];
        [actionSheet setCancelButtonIndex:[actionSheet numberOfButtons]-1];
        
        [actionSheet showInView:self.appDelegate.window];
        
        return NO;
    }
    
    NSString *postID = @"";
    
    if (continueReading.location != NSNotFound)
    {
        
        postID = [urlString substringFromIndex:21];
        NSLog(@"should load readmore postID:%@",postID);
        
        PostObj *post = [ControlObj getPostWithKey:postID];
        post.isContinueReading = 1;
        [post resetName];
        [post resetHTML];
        
        CommentViewController *CVC = nil;
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height == 480) {
            CVC = [[CommentViewController alloc] initWithNibName:@"CommentViewController" bundle:nil];
        }else{
            CVC = [[CommentViewController alloc] initWithNibName:@"CommentViewController-568h" bundle:nil];
        }
        
        [CVC setPost:post];
        [CVC setFacebookWebViewController:self];
        
        [self showTabBar:self.tabBarController];
        [self.navigationController pushViewController:CVC animated:YES];
        
        return NO;
        
    }

    if (likeRange.location != NSNotFound) {
        
        self.activePostID = [urlString substringFromIndex:17];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:warningControllFacebook] ) {
            self.isLike = TRUE;
            [FacebookUtils like:self.activePostID HTTPMethod:@"POST"];
            return NO;
        }
         TutorViewController *tuto = nil;
        // check only click Like 
        if (self.isLikeClick == 0)
        {
            self.isLikeClick = 1;
            if (isIphone5)
                tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController-568h" bundle:nil];
            else
                tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
            
            tuto.delegate = (id)self;
            tuto.type = typeLike;          
            [self presentPopupViewController:tuto animationType:MJPopupViewAnimationSlideBottomTop bckClickable:NO];
            return  NO;
        }
                
    }
    if (commentRange.location != NSNotFound) {
        postID = [urlString substringFromIndex:20];
        NSLog(@"should load postID:%@",postID);
        
        PostObj *post = [ControlObj getPostWithKey:postID];
        post.isContinueReading = 1;
        [post resetName];
        [post resetHTML];
        
        CommentViewController *CVC = nil;
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height == 480) {
            CVC = [[CommentViewController alloc] initWithNibName:@"CommentViewController" bundle:nil];
        }else{
            CVC = [[CommentViewController alloc] initWithNibName:@"CommentViewController-568h" bundle:nil];
        }
        
        [CVC setPost:post];
        [CVC setFacebookWebViewController:self];
        
        [self showTabBar:self.tabBarController];
        [self.navigationController pushViewController:CVC animated:YES];
        
        return NO;
    }
    
    if (unlikeRange.location != NSNotFound) {
        self.activePostID = [urlString substringFromIndex:19];
        // #fix bug 130
        self.isLike = FALSE;
         [FacebookUtils like:self.activePostID HTTPMethod:@"DELETE"];
        return NO;
        
    }
    
    if (check.location != NSNotFound) {
        return NO;
    }
    
    NSLog(@"urlString:%@",urlString);
    NSLog(@"postID:%@",postID);
    
    if (nil == postID) {
        NSLog(@"GOING TO COMMENT PAGE...");
        FacebookCommentViewController *fcvc = [[FacebookCommentViewController alloc] initWithNibName:@"FacebookCommentViewController" bundle:nil];
        [self.navigationController pushViewController:fcvc animated:YES];
        return NO;
    }
    return YES;
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidStartLoad:%d",webView.tag);
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    if ([webView isEqual:self.facebooWebView]) {
        NSLog(@"push webview");
        [FacebookUtils getMe];
        [FacebookUtils refreshFriends];
        [FacebookUtils getNewFeedsMultiQuery];
    }
    
    // optimize speed scroll uiwebview content bug #87.
    webView.scrollView.decelerationRate = UIScrollViewDecelerationRateNormal;
    //fix do not scroll uiwebview header.
    self.wvTopHeader.scrollView.scrollEnabled = FALSE;
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"webView didFailLoadWithError:%d",webView.tag);
}



#pragma mark - UIActionSheetDelegate methods 
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([actionSheet isEqual:self.popupQuery]) {
        switch (buttonIndex) {
            case 0: {
                if (actionSheet.numberOfButtons == 2) {
                    NSLog(@"hide post: \t %@",self.activePostID);
                    
                    //                [FacebookUtils hidenPost:self.activePostID]; // can't implement
                    //new solution
                    
                    NSString *html = [NSString stringWithFormat:[Template templateArticleForUndo],
                                      textUndo,
                                      newsfeed,
                                      self.activePostID,
                                      [Template getIconUndo]];
                    
                    NSString *js =[NSString stringWithFormat:@"document.getElementById('post_%@').innerHTML = '%@';",
                                   self.activePostID,
                                   html];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.facebooWebView stringByEvaluatingJavaScriptFromString:js];
                    });
                    
                    
                    NSMutableArray *arr = [Utility getCachedFile];
                    [arr addObject:self.activePostID];
                    [Utility saveCachedFile:arr];
                    
                    
                    self.activePostID = @"";
                }
                else
                {
                    // delete post
                    
                    DLOG(@"delete post: \n\t %@",self.activePostID);
                    [FacebookUtils deletePost:self.activePostID];
                }
                break;
            }
                
            case 1:
            {
                if (actionSheet.numberOfButtons == 2) {
                    DLOG(@"cancel post: \n\t %@",self.activePostID);
                    break;
                }
                NSLog(@"hide post: \t %@",self.activePostID);
                
                //                [FacebookUtils hidenPost:self.activePostID]; // can't implement
                //new solution
                
                NSString *html = [NSString stringWithFormat:[Template templateArticleForUndo],
                                  textUndo,
                                  newsfeed,
                                  self.activePostID,
                                  [Template getIconUndo]];
                
                NSString *js =[NSString stringWithFormat:@"document.getElementById('post_%@').innerHTML = '%@';",
                               self.activePostID,
                               html];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.facebooWebView stringByEvaluatingJavaScriptFromString:js];
                });
                
                
                NSMutableArray *arr = [Utility getCachedFile];
                [arr addObject:self.activePostID];
                [Utility saveCachedFile:arr];
                
                
                self.activePostID = @"";
            
                break;
            }
            default: {
                DLOG(@"cancel post: \n\t %@",self.activePostID);
                break;
            }
        };
    }
    else {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            switch (buttonIndex) {
                case 0:[self openCamera];break;
                case 1: [self openGallery];break;
                default: break;
            };
        }else {
            switch (buttonIndex) {
                case 0: [self openGallery];break;
                default: break;
            };
        }
    }
}


- (void)openCamera
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
}


- (void)openGallery
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePicker.allowsEditing = YES;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    //-- change title color
    CGRect frame = CGRectMake(0, 150, 150, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = viewController.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    
    viewController.navigationItem.titleView = label;
}



#pragma UIImagePickerControllerDelegate methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    //NSLog(@"imagePicker did return");
    self.attachImage = image;
    [self dismissModalViewControllerAnimated:YES];
    
    // NEW WAY: //
    SendFacebookImageViewController *sfivc = [[SendFacebookImageViewController alloc] initWithNibName:@"SendFacebookImageViewController" bundle:nil];
    //    sfivc.imagePost.image = image;   
    
    //checks source type a camera   
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {    
        sfivc.typeSouceImage = 1;
    }
    //otherwise, show a modal for taking a photo
    else {
        sfivc.typeSouceImage = 0;
    }
    
    [sfivc setImage:image];
    [sfivc setDelegate:(id)self];
    [self.navigationController pushViewController:sfivc animated:YES];
}



#pragma mark - Delegate TutoViewController

- (void) showPrivacyTip
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    NSString* url = @"https://m.facebook.com/legal/terms";
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
//    User *user = [[Utility shareProfileDict] objectForKey:[[NSUserDefaults standardUserDefaults] objectForKey:facebookID]];
//    if (user == nil) {
//        user = [[User alloc] initWithDictinary:nil];
//    }
    
    ShowLinkViewController *wv = [[ShowLinkViewController alloc] initWithNibName:@"ShowLinkViewController" bundle:nil];
    wv.strLinkUrl = url;
    //wv.post = user;
    [self.navigationController pushViewController:wv animated:YES];    

}



#pragma mark - SendFacebookMessageDelegate

- (void)sendFacebookMessageFinished:(SendFacebookMessageViewController *)sendFacebookMessage result:(NSDictionary *)result
{
    NSLog(@"dlog %@",result);
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [FacebookUtils getNewFeedsMultiQuery];
    });
}



#pragma mark - SendFacebookImageDelegate

- (void) sendFacebookImageFinished:(SendFacebookImageViewController *)sendFacebookImage
{
    NSLog(@"refreshWebView");
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [FacebookUtils getNewFeedsMultiQuery];
    });
}


- (void)sendFacebookImageStart:(SendFacebookImageViewController*)sendFacebookImage
{
}



#pragma mark - sendFacebookCheckinDelegate

- (void)sendFacebookCheckinFinished:(SendFacebookCheckinViewController *)checkin
{
    NSLog(@"refreshWebView");
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [FacebookUtils getNewFeedsMultiQuery];
    });
}



#pragma mark - FacebookUtilsDelegate

// login delegate
- (void)facebook:(FacebookUtils*)fbU didLoginFinish:(NSDictionary*)response
{
    NSLog(@"");
    [self setupHeaderView];
    [self setupWebFacebook];
    
//    [self performSelector:@selector(startRequestData) withObject:nil afterDelay:1];
    
    NSLog(@"call setupWebView");
}


- (void)facebook:(FacebookUtils*)fbU didLoginFail:(NSError*)error
{
    NSLog(@"didLoginFail error :%@",[error description]);
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                      message:@"Login Fail!"
                                                     delegate:self
                                            cancelButtonTitle:@"Re-Login"
                                            otherButtonTitles:nil, nil];
    [message show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Re-Login"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [FacebookUtils resetFacebookSession];
            [FacebookUtils logInFacebook];
        });
    }
}


- (void)facebook:(FacebookUtils*)fbU didLogout:(BOOL)success
{
    NSLog(@"didLogout success: %d",success);
    [self setupWebFacebook];
    [self.tabBarController setSelectedIndex:0];
    [FacebookUtils logInFacebook];
}

// newFeed delegate

- (void)facebook:(FacebookUtils *)fbU didRefeshNewFeedFinish:(NSDictionary *)result
{
    NSLog(@"didRefeshNewFeedFinish: \n %@", result);
    
    NSArray *arrTemp = [result objectForKey:@"data"];
    NSArray *tmpary = [NSArray new];
    NSArray *nameArr = [NSArray new];
    if ([arrTemp count] > 1) {
        NSDictionary *dic0 = [arrTemp objectAtIndex:0];
        NSDictionary *dic1 = [arrTemp objectAtIndex:1];
        
        if ([[dic0 objectForKey:@"name"] isEqualToString:@"post"]) {
            tmpary = [dic0 objectForKey:@"fql_result_set"];
            nameArr = [dic1 objectForKey:@"fql_result_set"];
        }
        
        else
        {
            tmpary = [dic1 objectForKey:@"fql_result_set"];
            nameArr = [dic0 objectForKey:@"fql_result_set"];
        }
         [ControlObj processFriendList:nameArr];
    }
    else {
        NSDictionary *dic0 = [arrTemp objectAtIndex:0];
        tmpary = [dic0 objectForKey:@"fql_result_set"];
    }
    [self.newsArray removeAllObjects];
    self.newsArray = [[NSMutableArray alloc] initWithArray:tmpary];
    
    [self reloadData];
    
    [self.refreshControl endRefreshing];
    [self hidePopView];
}


- (void)facebook:(FacebookUtils *)fbU didRefeshNewFeedFail:(NSError *)error
{
    NSLog(@"didRefeshNewFeedFail error :%@",[error description]);
    [self.refreshControl endRefreshing];
    [self hidePopView];
    [NetworkActivity hide];
}


- (void)facebook:(FacebookUtils *)fbU didLikeFinish:(NSDictionary *)result
{
    DLOG(@"didLikeFinish: \n %@",result);
    PostObj *post = [ControlObj getPostWithKey:self.activePostID];
    if (self.isLike) {
        [post liked];
    }
    else {
        [post unliked];
    }
    
    NSString *html = [Utility convertTextToHTMLText:post.rangeLikeComment];
    
    NSString *js =[NSString stringWithFormat:@"document.getElementById('rangeLikeComment_%@').innerHTML = '%@';",
                   post.postID,
                   html];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.facebooWebView stringByEvaluatingJavaScriptFromString:js];
    });
    
    self.activePostID = @"";
    //dont show local notification.
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"IS_SENDING_MESSGE"];
}


- (void)facebook:(FacebookUtils *)fbU didLikeFail:(NSError *)error
{
    NSLog(@"didLikeFail error :%@",[error description]);
    self.activePostID = @"";
}


// get userinfo delegate
- (void)facebook:(FacebookUtils *)fbU didGetUserInfoFinish:(NSDictionary *)result
{
    NSArray *arr = [result objectForKey:@"data"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [ControlObj processFriendList:arr];
    });
}


- (void)facebook:(FacebookUtils *)fbU didGetUserInfoFail:(NSError *)error
{
    NSLog(@"didGetUserInfoFail error: %@",[error description]);
}


// getme delegate
- (void)facebook:(FacebookUtils *)fbU didGetMeFinish:(NSDictionary *)result
{
    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"id"] forKey:facebookID];
    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"name"] forKey:facebookName];
    
    NSDictionary *data = [[result objectForKey:@"picture"] objectForKey:@"data"];
    NSString *url = [[data objectForKey:@"url"] stringByReplacingOccurrencesOfString:@"_q.jpg" withString:@"_n.jpg"];
     [[NSUserDefaults standardUserDefaults] setObject:url forKey:facebookUrl];
    
    
    [Utility createCachedFile:nil];
    
    NSLog(@"getMe:%@",result);
    self.facebooWebView.delegate = self;
    
    [NetworkActivity hide];
}


- (void)facebook:(FacebookUtils *)fbU didGetMeFail:(NSError *)error
{
    NSLog(@"didGetMeFail error: %@",error);
}


//refesh friend delegate
- (void)facebook:(FacebookUtils *)fbU didGetReFeshFriendFinish:(NSDictionary *)result {
     DLOG(@"didGetReFeshFriendFinish result: %@",result);
    NSArray *arr = [result objectForKey:@"data"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [ControlObj processFriendList:arr];
    });
}


- (void)facebook:(FacebookUtils *)fbU didGetReFeshFriendFail:(NSError *)error
{
    NSLog(@"didGetReFeshFriendFail error: %@",error);
}


- (void)facebook:(FacebookUtils *)fbU didCommentFinish:(NSDictionary *)result
{
//    [FacebookUtils getNewFeedsMultiQuery];
    NSLog(@"comment finish with result: %@",result);
}


- (void)facebook:(FacebookUtils *)fbU didCommentFail:(NSError *)error
{
    NSLog(@"didCommentFail error : %@",error);
}


- (void) facebook:(FacebookUtils *)fbU didGetPostFinish:(NSDictionary *)result
{
    DLOG(@"%@",result);
}


- (void)facebook:(FacebookUtils *)fbU didGetPostFail:(NSError *)error
{
    NSLog(@"didGetPostFail error : %@",[error description]);    
}


- (void)facebook:(FacebookUtils *)fbU didGetNewsFeedSeeMoreFinish:(NSDictionary *)result
{
    NSArray *arrTemp = [result objectForKey:@"data"];
    NSArray *tmpary = [NSArray new];
    NSArray *nameArr = [NSArray new];
    if ([arrTemp count] > 1) {
        NSDictionary *dic0 = [arrTemp objectAtIndex:0];
        NSDictionary *dic1 = [arrTemp objectAtIndex:1];
        
        if ([[dic0 objectForKey:@"name"] isEqualToString:@"post"]) {
            tmpary = [dic0 objectForKey:@"fql_result_set"];
            nameArr = [dic1 objectForKey:@"fql_result_set"];
        }
        
        else
        {
            tmpary = [dic1 objectForKey:@"fql_result_set"];
            nameArr = [dic0 objectForKey:@"fql_result_set"];
        }
        [ControlObj processFriendList:nameArr];
    }
    else {
        NSDictionary *dic0 = [arrTemp objectAtIndex:0];
        tmpary = [dic0 objectForKey:@"fql_result_set"];
    }
    
    [self.newsArray removeAllObjects];
    self.newsArray = [[NSMutableArray alloc] initWithArray:tmpary];
        
    [self seeMoreData];
}


- (void)facebook:(FacebookUtils *)fbU didGetNewsFeedSeeMoreFail:(NSError *)error
{
    NSLog(@"didGetNewsFeedSeeMoreFail error : %@",[error description]);
    [self.facebooWebView.scrollView.infiniteScrollingView stopAnimating];
    [self.facebooWebView.scrollView.pullToRefreshView stopAnimating];
    [NetworkActivity hide];
}


// place
- (void)facebook:(FacebookUtils *)fbU didGetPlaceFinish:(NSDictionary *)result
{
    NSArray *arr = [result objectForKey:@"data"];
    for (NSDictionary *data in arr) {
        NSDictionary *geometry = [data objectForKey:@"geometry"];
        NSLog(@"%@",geometry);
        NSArray *coordinates = [geometry objectForKey:@"coordinates"];
        NSString* lon = [coordinates objectAtIndex:0];
        NSString* lat = [coordinates objectAtIndex:1];
        NSString* page_id = [data objectForKey:@"page_id"];
        NSString* name = [data objectForKey:@"name"];
        NSString* pic_small = [data objectForKey:@"pic_small"];
        
        [self showMapCheckIn:name lat:lat lon:lon placeID:page_id icon:pic_small];
    }
}


- (void)facebook:(FacebookUtils *)fbU didGetPlaceFail:(NSError *)error
{
    NSLog(@"didGetPlaceFail error : %@",[error description]);
}


- (void)facebook:(FacebookUtils*)fbU didShareFinish:(NSDictionary *)result
{
    NSLog(@"didShareFinish: \n\t result: %@",result);
    [FacebookUtils getNewFeedsMultiQuery];
}


- (void)facebook:(FacebookUtils *)fbU didShareFail:(NSError *)error
{
    NSLog(@"didShareFail error : %@",[error description]);
}


- (void)facebook:(FacebookUtils *)fbU didDeletePostFinish:(NSDictionary *)result
{
    [FacebookUtils getNewFeedsMultiQuery];
}


- (void)facebook:(FacebookUtils *)fbU didDeletePostFail:(NSError *)error
{
    NSLog(@"didDeletePostFail error : %@",[error description]);
}


- (void)facebook:(FacebookUtils *)fbU didGetProfileListFinish:(NSDictionary *)result
{
    
    NSLog(@"didGetProfileListFinish:\n %@",result);
    NSMutableArray* arr = [result objectForKey:@"data"];
    [ControlObj processFriendList:arr];
}


- (void)facebook:(FacebookUtils *)fbU didGetProfileListFail:(NSError *)error
{
    NSLog(@"didGetProfileListFail error : %@",[error description]);
}



#pragma mark - post control

- (void)reloadData
{
    strJsForPost = @"";
    //--"Dung" comment 
    //[self.facebooWebView stringByEvaluatingJavaScriptFromString:@"scrollTop();"];
    [ControlObj resetCount];
    [ControlObj analysisData:self.newsArray withType:newsfeedr];
}


- (void)seeMoreData
{
    strJsForPost = @"";
    DLOG(@"\nseeMoreData count %d\n",[self.newsArray count]);
    if ([self.newsArray count] == 0) {
        //fix blank bottom webview
        [Utility setFrameOfView:self.view withHeightView:416.0 andYCordinate:0.0];
    }
    [ControlObj analysisData:self.newsArray withType:seemore];
}



#pragma mark uiscrollview

- (IBAction)handleRefresh:(id)sender
{
    loop = FALSE;
    [ControlObj resetCount];
    [FacebookUtils getNewFeedsMultiQuery];
}


//-- insert row at bottom when UITableView infinite scrolling
- (void)insertRowAtBottom
{
    loop = FALSE;
    dispatch_async(dispatch_get_main_queue(), ^ {
         [FacebookUtils getSeeMoreNewFeedsMultiQuery:[NSString stringWithFormat:@"%d",[ControlObj shared].count + 10]];
    });   
}



#pragma mark - show / hide tabbar

// Method implementations
- (void)hideTabBar:(UITabBarController *) tabbarcontroller
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    self.tabBarController.tabBar.hidden = YES;
    [self.tabBarController.tabBar setAlpha:0.0];
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height == 480) {
        for(UIView *view in tabbarcontroller.view.subviews)
        {
            if([view isKindOfClass:[UITabBar class]])
            {
                [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
                //view.hidden = YES;
            }
            
            else
            {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,480)];
                // view.hidden = YES;
            }
        }
    }
    if (result.height == 568) {
        for(UIView *view in tabbarcontroller.view.subviews)
        {
            if([view isKindOfClass:[UITabBar class]])
            {
                [view setFrame:CGRectMake(view.frame.origin.x, 568, view.frame.size.width, view.frame.size.height)];
                //view.hidden = YES;
            }
            
            else
            {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,568)];
                // view.hidden = YES;
            }
        }
    }
    
    self.tabBarController.tabBar.hidden = YES;
    [UIView commitAnimations];
}


- (void)showTabBar:(UITabBarController *) tabbarcontroller
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    self.tabBarController.tabBar.hidden = NO;
    [tabbarcontroller.tabBar setAlpha:1.0];
    
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        NSLog(@"%@", view);
        
        if([view isKindOfClass:[UITabBar class]])
        {
            CGSize result = [[UIScreen mainScreen] bounds].size;
            if (result.height == 480) {
                [view setFrame:CGRectMake(view.frame.origin.x, 431, view.frame.size.width, view.frame.size.height)];
            }
            if (result.height == 568) {
                [view setFrame:CGRectMake(view.frame.origin.x, 519, view.frame.size.width, view.frame.size.height)];
            }
            //view.hidden= NO;
        }
    }
    [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    self.tabBarController.tabBar.hidden = NO;
    
    [UIView commitAnimations];
}



#pragma mark - Even when Scroll UIWebView

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.lastContentOffset > scrollView.contentOffset.y)
    {
        //            NSLog(@"\n\nScroll up\n\n");
        self.wvTopHeader.hidden = NO;
        if (isIphone5) {
            self.facebooWebView.frame = CGRectMake(0, 39, 320, 509);
        }
        else{
            self.facebooWebView.frame = CGRectMake(0, 39, 320, 426);
        }
        self.wvTopHeader.userInteractionEnabled = YES;
       // [self hideTabBar:self.tabBarController];
        [self.tabBarController setTabBarHidden:YES animated:NO];
    }
    else if (self.lastContentOffset < scrollView.contentOffset.y)
    {
        //            NSLog(@"\n\nScroll down\n\n");
        self.wvTopHeader.hidden = YES;
        if (isIphone5) {
            self.facebooWebView.frame = CGRectMake(0, 0, 320, 460);
        }
        else{
            self.facebooWebView.frame = CGRectMake(0, 0, 320, 377);
        }
        
        //self.facebooWebView.frame = CGRectMake(0, 0, 320, 426);
        self.wvTopHeader.userInteractionEnabled = NO;
        //[self showTabBar:self.tabBarController];
        [self.tabBarController setTabBarHidden:NO animated:NO];
    }
    //fix blank bottom webview
    [Utility setFrameOfView:self.view withHeightView:416 andYCordinate:0];
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    lastContentOffset = scrollView.contentOffset.y;
}



#pragma mark - notification

- (void) receiveTestNotification:(NSNotification *) notification
{
    NSLog (@"Successfully received the test notification!");
    dispatch_async(dispatch_get_main_queue(), ^ {
        // thuy comment fix crash here. 
        //[FacebookUtils getSeeMoreNewFeedsMultiQuery:[NSString stringWithFormat:@"%d",[ControlObj shared].count + 10]];
        //[self autoRefreshNewfeed];
    });
}

- (void) autoRefreshNewfeed
{
    // set pull    
   // __weak FacebookWebViewController *myself = self;
    //[myself handleRefresh:nil];
   
}




#pragma mark - anoimation

- (void)displayPopView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:nil];
    
    [UIView animateWithDuration:0.75
                     animations:^{
                         [self.facebooWebView addSubview:self.popView];
                         [self.popView setFrame:CGRectMake(0, 0, 320, -44)];
                         [self.popView setFrame:CGRectMake(0, 0, 320, 44)];
                     }
                     completion:^(BOOL finished){
                     }];
    [UIView commitAnimations];
}


- (void)hidePopView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:nil];
    
    [UIView animateWithDuration:0.75
                     animations:^{
                         [self.popView setFrame:CGRectMake(0, 0, 320, -44)];
                     }
                     completion:^(BOOL finished){
                         [self.popView removeFromSuperview];
                     }];
    [UIView commitAnimations];
}



#pragma mark - thuy Pass data

// this work: rename user name and avatar (default: Facebook User + avatar)
- (void)showAllFriend
{
    for (NSString *newID in [[Utility shareProfileDict] allKeys]) {
        User *user = [[Utility shareProfileDict] objectForKey:newID];
        [self.facebooWebView stringByEvaluatingJavaScriptFromString:user.jspic];
        [self.facebooWebView stringByEvaluatingJavaScriptFromString:user.jsname];
        
    }
}



#pragma mark - send image name

- (NSString*) sendImageName
{
    NSString *imageSend = @"";
    NSString *temp = [NSString stringWithFormat:@"<div class=\"async_composer\"><div><div class=\"composerLinksArea _2v9s\" data-sigil=\"marea\"><table class=\"composerLinksTable\"><tbody><tr><td><div class=\"imgDung\"><div><a href=\"http://null/new_status\" class=\"composerLinkText1\" role=\"button\"><img class =\"imgDung\" src=\"%@\" width=\"20\" height=\"20\"/></div></div>Status</a><div class=\"vertical-line\" style=\"height: 16px\"></div></td><td><div><a href=\"http://null/new_photo\" class=\"composerLinkText1\" role=\"button\"><div class=\"imgDung\"><img src=\"%@\" width=\"20\" height=\"20\"/></div><a href=\"http://null/new_photo\" class=\"composerLinkText2\" role=\"button\">Photo</a><div class=\"vertical-line\" style=\"height: 16px\"></div></div></td><td><div><a href=\"http://null/checkin\" class=\"composerLinkText1\" role=\"button\"><div class=\"imgDung\"><img  src=\"%@\" width=\"17\" height=\"17\"/></div>Check In</a></div></td></tr></tbody></table></div></div></div></body></html>", [Template getIConStatus],[Template getIConPhoto], [Template getIConCheckin]];
    NSString* abc = [Template templateHeaderBegin];
    imageSend = [NSString stringWithFormat:@"%@%@",abc,temp];
    
    return imageSend;    
}


- (void) showMapCheckIn:(NSString*)name lat:(NSString*)lat lon:(NSString*)lon placeID:(NSString*)placeID icon:(NSString*)icon
{
    NSString *idmap = [NSString stringWithFormat:@"map_%@",placeID];
    NSString* js = [NSString stringWithFormat:@"google.maps.event.addDomListener(window, 'load', initialized(\"%@\",\"%@\",\"%@\",\"%@\",\"%@\"));",lat,lon,idmap,name,icon];
    NSLog(@"abcabcabc:%@",js);
    
    NSString* name_place_js = [NSString stringWithFormat:@"$('div.name_place_%@').html('- at <a href=\"nil\" style=\"text-decoration:none;\">%@.</a>');",placeID,name];
    
    [self.facebooWebView stringByEvaluatingJavaScriptFromString:js];
    [self.facebooWebView stringByEvaluatingJavaScriptFromString:name_place_js];
}


- (void)implementJs
{
    while (arrJS.count > 0) {
        NSString *js = [arrJS objectAtIndex:0];
        NSLog(@"implementJs : %@", js);
        [self.facebooWebView stringByEvaluatingJavaScriptFromString:js];
        [arrJS removeObjectAtIndex:0];
    }
}



#pragma mark - tutordelegate

- (void)acceptLike:(warningType)type
{
    self.isLikeClick = 0;
    switch (type) {
        case typeLike: {
            self.isLike = TRUE;
            [FacebookUtils like:self.activePostID HTTPMethod:@"POST"];
            break;
        }
        case typeUnlike: {
            self.isLike = FALSE;
             [FacebookUtils like:self.activePostID HTTPMethod:@"DELETE"];
            break;
        }
        default:
            break;
    }
    NSLog(@"acceptLike");
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    //fix blank bottom webview
    [Utility setFrameOfView:self.view withHeightView:416.0 andYCordinate:0.0];
}


- (void)cancelLike
{
    NSLog(@"cancelLike");
    self.activePostID = @"";
    self.isLikeClick = 0;
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    //fix blank bottom webview
    [Utility setFrameOfView:self.view withHeightView:416.0 andYCordinate:0.0];
}



#pragma mark - html

- (NSString*)getHeadConfigForPost
{
    return @"<div class=\"storyStream _2v9s\"><div id=\"m_newsfeed_stream\"><section class=\"_7k7 storyStream _2v9s\">";
}


- (NSString*)getEndConfigForPost
{
    return  @"</div></section></div></div>";
}


- (void)reloadName
{
    NSString *listID = [ControlObj getStringListUser];
    NSLog(@"reloadName :\n%@",listID);
    
    if ([listID isEqualToString:@""]) {
        //fix blank bottom webview
        [Utility setFrameOfView:self.view withHeightView:416.0 andYCordinate:0.0];
      return;        
    }
       
    [FacebookUtils getProfilesList:listID];
}


- (void)reloadPlace
{
    NSString *listID = [ControlObj getStringListPlace];
    NSLog(@"%@",listID);
    
    if ([listID isEqualToString:@""]) {
        return;
    }
    [FacebookUtils getPlace:listID];
}



#pragma mark - ControlObjDelegate

- (void)reloadJs
{
    NSString *js =[NSString stringWithFormat:@"document.getElementById('data').innerHTML = '%@';",
                   strJsForPost];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *result = [self.facebooWebView stringByEvaluatingJavaScriptFromString:js];
        if (([result isEqualToString:@""] || result == nil) && loop) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            CLEAR( @"eee");
            NSLog(@"js fails 2");
            [self performSelector:@selector(reloadJs) withObject:nil afterDelay:2];
        }
        else {
            [self reloadName];
            [self reloadPlace];
            [self.facebooWebView.scrollView.infiniteScrollingView stopAnimating];
            [self.facebooWebView.scrollView.pullToRefreshView stopAnimating];
           [NetworkActivity hide];
        }
    });
}


- (void)ControlObj:(ControlObj *)controlObj finishedSeemore:(NSMutableArray *)arr
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"push newsfeed :%d",arr.count);
        
        if (arr.count == 0) {
            return;
        }
         NSString *js = @"";
        if (controlObj.count <= 10 && ![strJsForPost isEqualToString:@""]) {
            js =[NSString stringWithFormat:@"document.getElementById('data').innerHTML = '%@';",
                           strJsForPost];
        }
        else {
            DLOG(@"strJsForPost nil");
            js =[NSString stringWithFormat:@"document.getElementById('data').innerHTML += '%@';",
                           strJsForPost];
        }
        NSString *result = [self.facebooWebView stringByEvaluatingJavaScriptFromString:js];
        if ([result isEqualToString:@""] || result == nil) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            CLEAR(@"eee");
            NSLog(@"js fails 3");
            loop = YES;
            [self performSelector:@selector(reloadJs) withObject:nil afterDelay:4];
        }
        else {
            [self reloadName];
            [self reloadPlace];
            [self.facebooWebView.scrollView.infiniteScrollingView stopAnimating];
            [self.facebooWebView.scrollView.pullToRefreshView stopAnimating];
            [NetworkActivity hide];
        }
    });
}


- (void)ControlObj:(ControlObj *)controlObj seemore:(PostObj *)PostObj
{
    strJsForPost = [NSString stringWithFormat:@"%@ %@%@%@",
                    strJsForPost,
                    [self getHeadConfigForPost],
                    [Utility convertTextToHTMLText:PostObj.strHtml],
                    [self getEndConfigForPost]];
}


- (void)ControlObj:(ControlObj *)controlObj newsfeeds:(PostObj *)PostObj
{
    strJsForPost = [NSString stringWithFormat:@"%@ %@%@%@",
                    strJsForPost,
                    [self getHeadConfigForPost],
                    [Utility convertTextToHTMLText:PostObj.strHtml],
                    [self getEndConfigForPost]];
}


- (void)ControlObj:(ControlObj *)controlObj jsforFacebookUser:(NSString *)js
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.facebooWebView stringByEvaluatingJavaScriptFromString:js];
    });
}
 

- (void)ControlObj:(ControlObj *)controlObj newStories:(NSString *)text
{
    [self displayPopView];
}


#pragma mark - Status post delegate 

- (void)callActionSheetPhoto
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [actionSheet addButtonWithTitle:@"Take Photo"];
    }
    
    [actionSheet addButtonWithTitle:@"Choose Existing"];
    
    //ADD CANCEL BUTTON TO ACTION SHEET
    [actionSheet addButtonWithTitle:@"Cancel"];
    [actionSheet setCancelButtonIndex:[actionSheet numberOfButtons]-1];
    
    [actionSheet showInView:self.appDelegate.window];   
   
}
@end
