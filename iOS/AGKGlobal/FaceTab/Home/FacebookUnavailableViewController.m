//
//  FacebookUnavailableViewController.m
//  BeSafe
//
//  Created by Jeff Jolley on 11/9/12.
//
//

#import "FacebookUnavailableViewController.h"

@interface FacebookUnavailableViewController ()

@end

@implementation FacebookUnavailableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Post", @"Post");
        self.navigationItem.title = @"BeSafe Post";
        self.tabBarItem.image = [UIImage imageNamed:@"tab_facebook.png"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fb-ios-5.png"]];
//    [self.view addSubview:img];
    
    UIImage *img = [UIImage imageNamed:@"fb-ios-5x2.png"];
    UIImageView *ivw = [[UIImageView alloc] initWithFrame:self.view.bounds];
    ivw.image = img;
    
    [self.view addSubview:ivw];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
