//
//  FacebookWebViewController.h
//  AGKGlobal
//
//  Created by Jeff Jolley on 5/28/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "SendFacebookMessageViewController.h"
#import "SendFacebookImageViewController.h"
#import "SendFacebookCheckinViewController.h"
#import "MBProgressHUD.h"
#import "FacebookUtils.h"
#import "CommentViewController.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "ImageViewController.h"
#import "ShareViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "TutorViewController.h"
#import "ShowLinkViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Utility.h"
#import "Defines.h"
#import "Obj.h"
#import "CountDownViewController.h"

@class AppDelegate;

@interface FacebookWebViewController : UIViewController < UIWebViewDelegate,
UIActionSheetDelegate,
UINavigationControllerDelegate,
UIImagePickerControllerDelegate,
FacebookUtilsDelegate,
tutorDelegate,
ControlObjDelegate>
{
    NSMutableArray  *arrJS;
    NSString        *strJsForPost;
    NSString        *newStoriesJS;
    BOOL            loop;
    NSInteger       typeCoutDown;
}

@property(nonatomic,retain) UIRefreshControl    *refreshControl;

@property (nonatomic,retain) AppDelegate        *appDelegate;

@property (nonatomic,strong) IBOutlet UIWebView *facebooWebView;
@property (nonatomic,strong) IBOutlet UIWebView *wvTopHeader;
@property (nonatomic,strong) IBOutlet UIView    *popView;

@property (nonatomic,retain) UIImage             *attachImage;

@property (nonatomic,retain) NSMutableArray      *newsArray;

@property (nonatomic,retain) UIActionSheet       *popupQuery;
@property (nonatomic,retain) NSString            *activePostID;
@property (nonatomic,assign) BOOL                isLike;
@property (nonatomic,assign) NSInteger           isLikeClick;

// Dung add icon Status and Checkin
// get direction of scrollview
@property (nonatomic, assign) NSInteger          lastContentOffset;

- (IBAction)clickNewStories:(id)sender;

@end
