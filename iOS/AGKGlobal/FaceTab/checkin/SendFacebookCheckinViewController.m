//
//  SendFacebookCheckinViewController.m
//  AGKGlobal
//
//  Created by ThuyDao on 6/25/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "SendFacebookCheckinViewController.h"
#import "AppDelegate.h"  
#import "SBJSON.h" 
#import "BesafeAPI.h"
#import "VMUsers.h" 
#import "Utility.h"


@interface UINavigationController (Extensions)

- (void)bringUpViewController:(UIViewController*)viewController;

@end
@implementation UINavigationController (Extension)

- (void)bringUpViewController:(UIViewController*)viewController
{
    CGRect newRect = viewController.view.frame;
    newRect.origin.y = 20;
    
    [viewController.view setFrame:newRect];
    [self.view addSubview:viewController.view];
    [viewController.view setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.75]];
    [viewController viewWillAppear:NO];
    
    if ([viewController respondsToSelector:@selector(willBringUpView)]) {
        [viewController performSelector:@selector(willBringUpView)];
    }
    
    if ([viewController respondsToSelector:@selector(shouldTapForClosing:)]) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:viewController
                                       action:@selector(shouldTapForClosing:)];
        [tap setCancelsTouchesInView:NO];
        [viewController.view addGestureRecognizer:tap];
    }
}

@end

@interface SendFacebookCheckinViewController ()

@end

typedef void (^load_avatar_done_block)(UIImage* image,NSError* error);

@implementation SendFacebookCheckinViewController
@synthesize tbListCheckIn;
@synthesize mapView;

- (void)setAvatar:(NSString*)url doneBlock:(load_avatar_done_block)block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        UIImage* myImage = [UIImage imageWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString: url]]];
        
        if (block) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                block(myImage,nil);
            });
        }
    });
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    textf = @"";
//    [self.tbListCheckIn setBackgroundView:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_ct_fb_568.jpg"]]];
    
    [self setTitle:@"Where Are You?"];
    
    arrDataSource = [[NSMutableArray alloc] init];
    
//    locationManager = [[CLLocationManager alloc] init];
//    currentLocation = [[CLLocation alloc] init];
//    
//    locationManager.delegate = self;
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    [locationManager startUpdatingLocation];
    
    [self lookupLocationDelegate];
    
    idCheck = @"";
    dictCheck = [[NSDictionary alloc] init];

    [self displayMYMap];
    
    self.tbListCheckIn.layer.masksToBounds = NO;
    self.tbListCheckIn.layer.cornerRadius = 8; // if you like rounded corners
    self.tbListCheckIn.layer.shadowOffset = CGSizeMake(-15, 20);
    self.tbListCheckIn.layer.shadowRadius = 5;
    self.tbListCheckIn.layer.shadowOpacity = 0.5;
    
    
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self setMapView:nil];
    
    [[RCLocationManager sharedManager] stopUpdatingLocation];
    [[RCLocationManager sharedManager] stopMonitoringAllRegions];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //-- change title color
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
    
}

#pragma mark - Setup Location Manager
- (void) lookupLocationDelegate
{
    // Create location manager with filters set for battery efficiency.
    locationManager = [[RCLocationManager alloc] initWithUserDistanceFilter:kCLLocationAccuracyHundredMeters
                                                        userDesiredAccuracy:kCLLocationAccuracyBest purpose:@"..." delegate:self];
    [locationManager startUpdatingLocation];}


-(void)displayMYMap
{
    mapView.showsUserLocation = YES;
    mapView.delegate = self;
    //[self.mapView setCenterCoordinate:centerCoord zoomLevel:ZOOM_LEVEL animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) downloadImage
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    for (NSInteger index = 0; index <arrDataSource.count; index ++) {
        
        NSMutableDictionary *dict = [arrDataSource objectAtIndex:index];
        UIImage* myImage = [UIImage imageWithData:
                            [NSData dataWithContentsOfURL:
                             [NSURL URLWithString: [dict objectForKey:@"pic"]]]];
        
        if (myImage != nil) {
            [dict setObject:myImage forKey:@"img"];
        }
        
    
    [arrDataSource replaceObjectAtIndex:index withObject:dict];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"download success imge : %d",index);
            NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:index inSection:0];
            NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
            [self.tbListCheckIn reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
        });
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - get cell for table

- (UITableViewCell*)createCellFromNib
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"CustomCellPlace" owner:nil options:nil];
    return [(id) views objectAtIndex:0];
}

#pragma mark - uitableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrDataSource count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (cell == nil) {
        cell = [self createCellFromNib];
    }
    
    NSMutableDictionary* dict = [arrDataSource objectAtIndex:indexPath.row];
    
    ((UILabel*)[cell viewWithTag:1]).text = [dict objectForKey:@"name"];
    ((UILabel*)[cell viewWithTag:3]).text = [dict objectForKey:@"display_subtext"];
    
//    UIImage* myImage = [UIImage imageNamed:@"Places.jpg"];
    
    if ([dict objectForKey:@"img"] == nil) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [self setAvatar: [dict objectForKey:@"pic"] doneBlock:^(UIImage *image, NSError *error) {
            [((UIImageView*)[cell viewWithTag:2]) setImage:image];
            [dict setObject:image forKey:@"img"];
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }];
    }
    else {
        [((UIImageView*)[cell viewWithTag:2]) setImage:[dict objectForKey:@"img"]];
    }
    

    
    
    if ([[dict objectForKey:@"id"] isEqualToString:idCheck]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* dict = [arrDataSource objectAtIndex:indexPath.row];
    idCheck = [dict objectForKey:@"id"];
    dictCheck = dict;
    
    if (isIphone5) {
        PCVC = [[PostCheckinViewController alloc] initWithNibName:@"PostCheckinViewController-568h" bundle:nil];
        PCVC.delegate = (id)self;
        
        [PCVC setLblText:[dict objectForKey:@"name"]];
        [PCVC setCheckinText:[dict objectForKey:@"name"]];
        [PCVC setUrl:[dict objectForKey:@"pic"]];
    }
    else
    {
        PCVC = [[PostCheckinViewController alloc] initWithNibName:@"PostCheckinViewController" bundle:nil];
        PCVC.delegate = (id)self;
        
        [PCVC setLblText:[dict objectForKey:@"name"]];
        [PCVC setCheckinText:[dict objectForKey:@"name"]];
        [PCVC setUrl:[dict objectForKey:@"pic"]];
    }
    if (self.type == typeOther && self.textmsg != nil) {
        [PCVC setTextmsg:self.textmsg];
    }
    [self.navigationController pushViewController:PCVC  animated:YES];
}

#pragma mark - update current location

/*
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    
    [FacebookUtils addDelegate:self];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [FacebookUtils getNearbyPlaces:newLocation];
    
    //[locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"update current location failse");
}
*/

#pragma mark - RCLocationManagerDelegate

- (void)locationManager:(RCLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // Work around a bug in MapKit where user location is not initially zoomed to.
    
    if (oldLocation == nil) {
        MKCoordinateRegion userLocation = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 1500.0, 1500.0);
        [self.mapView setRegion:userLocation animated:YES];
    }
    currentLocation = newLocation;
    
    [FacebookUtils addDelegate:self];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [FacebookUtils getNearbyPlaces:newLocation];
    
     //--stop update location
     [locationManager stopUpdatingLocation];
     [locationManager stopMonitoringAllRegions];
     locationManager.delegate = nil;
     locationManager = nil;

}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"update current location failse");
}

#pragma mark - Delegate TutoViewController .
- (void) showPrivacyTip
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    NSString* url = @"https://m.facebook.com/legal/terms";
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
//    User *user = [[Utility shareProfileDict] objectForKey:[[NSUserDefaults standardUserDefaults] objectForKey:facebookID]];
//    if (user == nil) {
//        user = [[User alloc] initWithDictinary:nil];
//    }
    
    ShowLinkViewController *wv = [[ShowLinkViewController alloc] initWithNibName:@"ShowLinkViewController" bundle:nil];
    wv.strLinkUrl = url;
    //wv.post = user;
    [self.navigationController pushViewController:wv animated:YES];
    
}

#pragma mark - PostCheckInDelegate

-(void)sendPostCheckInDelegate:(PostCheckinViewController *)PostCheckin postTtext:(NSString *)text
{
    textf = text;
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:warningControllFacebook] integerValue] == 1 || [[NSUserDefaults standardUserDefaults] integerForKey:@"timerSecs"] == 0)
    {
        [self countDownComplete];
    }
    else {
        TutorViewController *tuto = nil;
        if (isIphone5)
            tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController-568h" bundle:nil];
        else
            tuto = [[TutorViewController alloc] initWithNibName:@"TutorViewController" bundle:nil];
        
        tuto.delegate = (id)self;
        tuto.type = typePostCheckin;
        [self presentPopupViewController:tuto animationType:MJPopupViewAnimationSlideBottomTop bckClickable:YES];
    }

}

#pragma mark - facebookutil delegate

- (void)facebook:(FacebookUtils *)fbU didGetNearbyPlacesFinish:(NSDictionary *)result
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        arrDataSource = [result objectForKey:@"data"];
        [self.tbListCheckIn reloadData];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    });
    
//    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//        [self downloadImage];
//    });

}

- (void)facebook:(FacebookUtils *)fbU didGetNearbyPlacesFail:(NSError *)error
{
    NSLog(@"--- didGetNearbyPlacesFail --- FAILURE ---");
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - mapview delegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation: (MKUserLocation *)userLocation
{
    [self.mapView setCenterCoordinate:userLocation.location.coordinate zoomLevel:13 animated:YES];
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    MKMapItem *location = (MKMapItem*)view.annotation;
    
    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
    [location openInMapsWithLaunchOptions:launchOptions];
}


- (void)countDownComplete
{
    self.arrBuddys = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:ARR_BUDDY_LOGIN] copyItems:YES];
    DLOG(@"ARR_BUDDYS_ID %@",self.arrBuddys);
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if ([self.arrBuddys count] > 0 || [delegate.buddy_id_update length] > 0)
    {
        [self savePostCheckinToBuddyConfirm];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        //FBRequest *req = [FBRequest requestForPostStatusUpdate:textf place:@"260389527326526" tags:nil]; //@"260389527326526";
        FBRequest *req = [FBRequest requestForPostStatusUpdate:textf place:[dictCheck objectForKey:@"page_id"] tags:nil];
        [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (result) {
                NSLog(@"ok: result: %@",result);
                if (self.delegate && [self.delegate respondsToSelector:@selector(sendFacebookCheckinFinished:)]) {
                    [self.delegate sendFacebookCheckinFinished:self];
                }
            } else {
                NSLog(@"--- requestForPostCheckinUpdate --- FAILURE ---");
                NSLog(@"%@",error);
                NSLog(@"----------------------------------------------");
            }
        }];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
        
    
}

- (void)acceptLike:(warningType)type
{
    NSLog(@"acceptLike");
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    [self countDownComplete];
}

- (void)cancelLike
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showKeyboardPostCheckin"  object:nil];
    
}


#pragma mark - Call api save data to server 

- (void) savePostCheckinToBuddyConfirm
{
   // NSString *date_post = [Utility stringFromDatewithTime:[NSDate date]];
    NSString *date_post = [NSString stringWithFormat:@"%li",[Utility getTimeIntervalSince1970s]];
    NSString *place = [NSString stringWithFormat:@"%@",[dictCheck objectForKey:@"page_id"]];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:textf forKey:STT_MESSAGE_POST];
    [params setObject:place forKey:STT_PLACE_CHECKIN];
    [params setObject:@"checkin" forKey:STT_POST_TYPE];
    [params setObject:date_post forKey:STT_DATE_POST];
    
    SBJsonWriter  *jsonWriter   = [[SBJsonWriter alloc] init];
    NSString *jsonStr           = [jsonWriter stringWithObject:params];
    NSLog(@" jsonStr Params %@",jsonStr);
    [self callAPISavePostCheckin:jsonStr];    
}


- (void) callAPISavePostCheckin:(NSString*)strJson
{
    NSError *error = nil;
    NSDictionary *dict_response = nil;
    VMUsers *user = [VMUsers readData];
    DLOG(@"USER ID %@",user.userId_login);
    
    dict_response = [BesafeAPI postDataToPendingBuddy:@"facebook" userID:user.userId_login post_id:@"0" data:strJson error:&error];
    if (error)
    {
        [Utility showAlert:[error localizedDescription]];
    }
    else
    {
        DLOG(@"RESPONSE %@",dict_response);
        [Utility showAlert:MESSAGE_SEND_BUDDY];
    }

}


@end
