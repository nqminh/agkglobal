//
//  SendFacebookCheckinViewController.h
//  AGKGlobal
//
//  Created by ThuyDao on 6/25/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "PostCheckinViewController.h"
#import "FacebookUtils.h"
#import "MKMapView+ZoomLevel.h"
#import "CountDownViewController.h" 
#import <MapKit/MapKit.h>
#import "RCLocationManager.h"

@class SendFacebookCheckinViewController;

enum  {
    typeCheckin = 16,
    typeOther = 17
    };
typedef NSInteger checkintype;

@protocol sendFacebookCheckinDelegate <NSObject>

- (void)sendFacebookCheckinFinished:(SendFacebookCheckinViewController*)checkin;
- (void) showKeyboardPostCheckin;
- (void)selectLocation:(SendFacebookCheckinViewController*)checkin with:(NSDictionary*)dict;


@end

@interface SendFacebookCheckinViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,PostCheckInDelegate, FacebookUtilsDelegate, MKMapViewDelegate,CLLocationManagerDelegate,RCLocationManagerDelegate>
{
    IBOutlet UITableView *tbListCheckIn;
    
    NSMutableArray* arrDataSource;
    
    //CLLocationManager *locationManager;
    
    CLLocation* currentLocation;
    
    NSString *idCheck;
    
    PostCheckinViewController *PCVC;
    
    NSDictionary *dictCheck;
    
    MKMapView *mapView;
    
    NSString* textf;
    
    NSDictionary* authienectdic;
    RCLocationManager *locationManager;
}

@property (nonatomic,retain) IBOutlet UITableView *tbListCheckIn;
@property (nonatomic,assign) id <sendFacebookCheckinDelegate> delegate;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic,assign) checkintype type;
@property (nonatomic,retain) NSString* textmsg;
@property (nonatomic,retain) NSMutableArray *arrBuddys;


- (void)countDownComplete;

@end
