//
//  PostCheckinViewController.h
//  AGKGlobal
//
//  Created by ThuyDao on 6/25/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "AudienceViewController.h"
#import "SSTextView.h"

@class PostCheckinViewController;
@protocol PostCheckInDelegate <NSObject>

- (void)sendPostCheckInDelegate:(PostCheckinViewController*)PostCheckin postTtext:(NSString*)text;

@end

@interface PostCheckinViewController : UIViewController
{
    // check banned word , address
    NSString *textSrc_sendcheckin;
    
    NSString *showBannedWordStr_sendcheckin;
    NSString *showBannedAddressStr_sendcheckin;
   
    NSArray *bannedWordArr_sendcheckin;
    NSArray *bannedAddressArr_sendcheckin;
   
    UIAlertView *alv_sendcheckin;

}

@property (strong) NSString                             *checkinText;
@property (nonatomic,retain) IBOutlet UILabel           *lblText;
@property (nonatomic,retain) IBOutlet UIImageView       *avatar;
@property (nonatomic,retain) NSString                   *url;
@property (nonatomic,retain) IBOutlet UIButton          *postingSubmitButton;
@property(nonatomic,assign) id <PostCheckInDelegate>    delegate;
@property (nonatomic,retain) IBOutlet SSTextView        *postingTextView;
@property (nonatomic,retain) NSString                   *textmsg;

-(IBAction)posttowall:(id)sender;

@end
