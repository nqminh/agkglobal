//
//  PostCheckinViewController.m
//  AGKGlobal
//
//  Created by ThuyDao on 6/25/13.
//  Copyright (c) 2013 fountaindew. All rights reserved.
//

#import "PostCheckinViewController.h"

@interface PostCheckinViewController ()

@end

@implementation PostCheckinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)configureView
{
//    float y = 0;
//    CGSize result = [[UIScreen mainScreen] bounds].size;
//    
//    if (result.height == 480) {
//        y = 164; //305
//    }
//    else {
//        y = self.view.frame.size.height -205 ; // -44 250
//    }
//    [self.viewAudience setFrame:CGRectMake(0, y, 320, 36)];
//    [self.view addSubview:self.viewAudience];
//    [self.myTableView setFrame:CGRectMake(0,y, 320, self.view.frame.size.height - self.viewAudience.frame.origin.y)];
//    [self.view addSubview:self.myTableView];
//    self.myTableView.hidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Status";
    
    // custom navigation bar button
    // left
    UIBarButtonItem *leftbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel:)];
    self.navigationItem.leftBarButtonItem = leftbarBtnItem;
    
    // right
    
    UIBarButtonItem *rightbarBtnItem = [[UIBarButtonItem alloc]initWithTitle:@"Post" style:UIBarButtonItemStyleBordered target:self action:@selector(postText:)];
    self.navigationItem.rightBarButtonItem = rightbarBtnItem;
    
    [self.lblText setText:self.checkinText];
    [self.postingTextView becomeFirstResponder];
    [self.postingTextView setText:self.textmsg];
    
    [self.avatar setImage:[UIImage imageNamed:@"Places.jpg"]];
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self getAvatar];
    });
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showKeyboardPostCheckin) name:@"showKeyboardPostCheckin" object:nil];
   
}


- (void)viewWillAppear:(BOOL)animated
{
    //-- change title color
    CGRect frame = CGRectMake(0, 0, 320, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor yellowColor];
    label.text = self.navigationItem.title;
    [label setShadowColor:[UIColor darkGrayColor]];
    [label setShadowOffset:CGSizeMake(0, -0.5)];
    self.navigationItem.titleView = label;
}


-(void) showKeyboardPostCheckin
{    
    [self.postingTextView becomeFirstResponder];
}


-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"showKeyboardPostCheckin" object:nil];
}


- (void)getAvatar {
    UIImage* myImage = [UIImage imageWithData:
                        [NSData dataWithContentsOfURL:
                         [NSURL URLWithString: self.url]]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
       [self.avatar setImage:myImage];
    });
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)cancel:(UIButton*)sender
{
    [self.navigationController popViewControllerAnimated:YES];
} 


- (void)postText:(UIButton*)sender
{
    // get data from banner words , banned addresses.
    // check banned word , banned address .
    textSrc_sendcheckin = self.postingTextView.text;
    bannedWordArr_sendcheckin = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_WORDS"] mutableCopy];
    bannedAddressArr_sendcheckin = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"BANNED_ADDRESSES"] mutableCopy];
    
    showBannedWordStr_sendcheckin = [Utility showBanned:textSrc_sendcheckin withArr:bannedWordArr_sendcheckin];
    showBannedAddressStr_sendcheckin = [Utility showBanned:textSrc_sendcheckin withArr:bannedAddressArr_sendcheckin];
    
    if(showBannedWordStr_sendcheckin.length > 0 )
    {
        if (showBannedAddressStr_sendcheckin.length > 0)
        {
            NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_sendcheckin);
            alv_sendcheckin = [[UIAlertView alloc]initWithTitle:@"Banned Word and Address!" message:[NSString stringWithFormat:@"You have typed Banned Word and Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_sendcheckin show];
        }
        else{
            NSLog(@"\n\n banned word %@\n\n",showBannedWordStr_sendcheckin);
            alv_sendcheckin = [[UIAlertView alloc]initWithTitle:@"Banned Word!" message:[NSString stringWithFormat:@"You have typed Banned Word \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alv_sendcheckin show];
        }
        
    }
    else if (showBannedAddressStr_sendcheckin.length > 0)
    {
        NSLog(@"\n\n banned word %@\n\n",showBannedAddressStr_sendcheckin);
        alv_sendcheckin = [[UIAlertView alloc]initWithTitle:@"Banned Address!" message:[NSString stringWithFormat:@"You have typed Banned Address \nPlease retry again!"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alv_sendcheckin show];
    }
    else
    {

        if (self.delegate && [self.delegate respondsToSelector:@selector(sendPostCheckInDelegate:postTtext:)]) {
            [self.postingTextView resignFirstResponder];
            [self.delegate sendPostCheckInDelegate:self postTtext:self.postingTextView.text];
        }
    }
}



#pragma mark - IBAction

- (IBAction)posttowall:(id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(sendPostCheckInDelegate:postTtext:)]) {
        [self.delegate sendPostCheckInDelegate:self postTtext:self.postingTextView.text];
    }
}



@end
